<?php
require_once('bdd.php');

$sql = "SELECT dtl.* FROM tbl_kalender kal 
		JOIN tbl_kalender_dtl dtl ON kal.`kd_kalender` = dtl.`kd_kalender`
		WHERE kal.`flag` = 2";

$req = $bdd->prepare($sql);
$req->execute();

$events = $req->fetchAll();

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
	<!-- FullCalendar -->
	<link href='css/fullcalendar.css' rel='stylesheet' />
	<link href='css/fullcalendar.min.css' rel='stylesheet' />

    <!-- Custom CSS -->
    <style>
    body {
        padding-top: 70px;
        /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
    }
	#calendar {
		max-width: 800px;
	}
	.col-centered{
		float: none;
		margin: 0 auto;
	}
    </style>

    <div class="container">

        <div class="col-lg-12 text-center" style="margin-top:-70px">
            <div id="calendar" class="col-centered">
            </div>
        </div>
			
		<!-- Modal -->
		<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal" method="POST" action="editEventTitle.php">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Detail Kegitan</h4>
			  </div>
			  <div class="modal-body">
				
				  <div class="form-group">
					<label for="title" class="col-sm-2 control-label">Kegiatan</label>
					<div class="col-sm-10">
					  <textarea name="title" class="form-control" id="title" placeholder="Title"></textarea>
					</div>
				  </div>
				  <div class="form-group">
					<label for="title" class="col-sm-2 control-label">Waktu Pelaksanaa</label>
					<div class="col-sm-5">
					  <input type="text" name="start" class="form-control col-sm-5" id="start">
					</div>
					<div class="col-sm-5">
					  <input type="text" name="end" class="form-control col-sm-5" id="end" >
					</div>
					
				  </div>

				  <input type="hidden" name="id" class="form-control" id="id">				
			  </div>
			</form>
			</div>
		  </div>
		</div>

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	
	<!-- FullCalendar -->
	<script src="js/moment.min.js"></script>
	<script src="js/fullcalendar.min.js"></script>
	<script src="js/fullcalendar.js"></script>
	
	<script>

	$(document).ready(function() {
		
		$("#calendar").fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title'
			},
			views: {
				month: { // name of view
				  titleFormat: 'MMMM YYYY'
				  // other view-specific options here
				}
			},
			defaultDate: '<?= date("Y-m-d") ?>',
			eventLimit: true, // allow "more" link when too many events
			selectable: true,
			selectHelper: true,

			select: function(start, end) {
				
				$('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
				$('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
				$('#ModalAdd').modal('show');
			},
			eventRender: function(event, element) {
				element.bind('dblclick', function() {
					$('#ModalEdit #id').val(event.id);
					$('#ModalEdit #title').val(event.title);
					$('#ModalEdit #start').val(moment(event.start).format('DD-MM-YYYY'));
					$('#ModalEdit #end').val(moment(event.end).format('DD-MM-YYYY'));
					$('#ModalEdit').modal('show');
				});
			},
			eventDrop: function(event, delta, revertFunc) { // si changement de position

				edit(event);

			},
			eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur

				edit(event);

			},
			
			events: [
			<?php foreach($events as $event):
				if ($event["kelompok"] == 1) {
					$warna = '#40E0D0';
				}elseif ($event["kelompok"] == 2) {
					$warna = '#0071C5';
				}elseif ($event["kelompok"] == 3) {
					$warna = '#FF1A1A';
				}
				$keg	= $event["kegiatan"];
				$keg 	= preg_replace('/[^A-Za-z0-9\-]/', ' ', $keg); // Removes special chars.
				$keg 	= str_replace('&', 'dan', $keg); // Removes special chars.
				$keg 	= str_replace('/', 'atau', $keg); // Removes special chars.
			?>
				{	
					id: '<?= $event["id"]; ?>',
					title: '<?= $keg; ?>',
					start: '<?= $event["mulai"]; ?>',
					end: '<?= $event["ahir"]; ?>',
					color: '<?= $warna; ?>'
				},
			<?php endforeach; ?>
			]
		});

		function edit(event){
			start = event.start.format('YYYY-MM-DD HH:mm:ss');
			if(event.end){
				end = event.end.format('YYYY-MM-DD HH:mm:ss');
			}else{
				end = start;
			}
			
			id =  event.id;
			
			Event = [];
			Event[0] = id;
			Event[1] = start;
			Event[2] = end;
			
			$.ajax({
			 url: 'editEventDate.php',
			 type: "POST",
			 data: {Event:Event},
			 success: function(rep) {
					if(rep == 'OK'){
						alert('Saved');
					}else{
						alert('Could not be saved. try again.'); 
					}
				}
			});
		}

	});

</script>

</body>

</html>
