<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temph_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function pendaftar_remid($kd,$thn)
	{
		$src = $this->db->query("SELECT * from tbl_transaksi_nilai where kd_transaksi_nilai like '".$kd."%' and NLAKHTRLNM IN ('D','E')")->result();
		foreach ($src as $key) {
			return $this->db->query("SELECT * from tbl_krs_sp where npm_mahasiswa = '".$key->NIMHSTRLNM."' and kd_krs like concat(npm_mahasiswa,'".$thn."%') and kd_matakuliah = '".$key->KDKMKTRLNM."' ")->row();
		}	
	}

	function load_pst_remid($prodi,$tahun)
	{
		return $this->db->query("SELECT DISTINCT a.`kelas`,d.`nama`,a.`kd_matakuliah`,c.nama_matakuliah,c.sks_matakuliah,a.kd_jadwal,a.id_jadwal
								FROM tbl_jadwal_matkul a 
								JOIN tbl_matakuliah c ON c.`kd_matakuliah` = a.`kd_matakuliah`
								JOIN tbl_karyawan d ON d.`nid` = a.`kd_dosen`
								WHERE c.`kd_prodi` = '".$prodi."' AND a.`kd_tahunajaran` = '".$tahun."' and a.kd_jadwal like '".$prodi."%'
								order by a.kd_matakuliah");
	}

	function jml_remid($tahun,$prodi,$jadwal)
	{
		return $this->db->query("SELECT count(NIMHSTRLNM) as jums FROM tbl_transaksi_nilai WHERE (NLAKHTRLNM = 'D' OR NLAKHTRLNM = 'E') 
								AND THSMSTRLNM = '".$tahun."' AND KDPSTTRLNM = '".$prodi."' and kd_transaksi_nilai like '".$jadwal."%'");
	}

	function list_remid_detil($tahun,$prodi,$kode,$jdl)
	{
		return $this->db->query("SELECT b.NIMHSMSMHS,b.NMMHSMSMHS,a.nilai_akhir,a.NLAKHTRLNM,a.KDKMKTRLNM FROM tbl_transaksi_nilai a JOIN tbl_mahasiswa b 
								ON a.NIMHSTRLNM = b.NIMHSMSMHS WHERE a.THSMSTRLNM = '".$tahun."' AND a.KDKMKTRLNM = '".$kode."' 
								AND a.kd_transaksi_nilai LIKE '".$jdl."%'
								AND (a.NLAKHTRLNM  = 'E' OR a.NLAKHTRLNM  = 'D') AND a.KDPSTTRLNM = '".$prodi."'");
	}

	function list_improve($prodi,$tahun)
	{
		return $this->db->query("SELECT DISTINCT a.`kelas`,d.`nama`,a.`kd_matakuliah`,c.nama_matakuliah,c.sks_matakuliah,a.kd_jadwal,a.id_jadwal
								FROM tbl_jadwal_matkul_sp a 
								JOIN tbl_matakuliah c ON c.`kd_matakuliah` = a.`kd_matakuliah`
								JOIN tbl_karyawan d ON d.`nid` = a.`kd_dosen`
								WHERE c.`kd_prodi` = '".$prodi."' AND a.`kd_tahunajaran` = '".$tahun."' and a.kd_jadwal like '".$prodi."%'
								and a.open = '1'
								order by a.kd_matakuliah");
	}

	function list_reg($kd)
	{
		return $this->db->query("SELECT count(x.npm_mahasiswa) as jum from tbl_krs_sp x join tbl_sinkronisasi_renkeu rk on rk.npm_mahasiswa = x.npm_mahasiswa where x.kd_jadwal = '".$kd."'
								 and rk.tahunajaran = SUBSTRING(x.kd_krs FROM 13 FOR 5)")->row();	
	}

	function load_mhs_sp($kd)
	{
		$kode = substr($kd, 0,5);
		return $this->db->query("SELECT distinct a.kd_jadwal,c.nama_matakuliah,c.kd_matakuliah,b.NIMHSMSMHS,
								b.NMMHSMSMHS,d.kelas,d.kd_tahunajaran,d.id_jadwal,a.kd_krs 
								from tbl_verifikasi_krs_sp x
								join tbl_krs_sp a on x.kd_krs = a.kd_krs
								join tbl_jadwal_matkul_sp d on d.kd_jadwal = a.kd_jadwal
								JOIN tbl_mahasiswa b on a.npm_mahasiswa = b.NIMHSMSMHS
								join tbl_matakuliah c on c.kd_matakuliah = a.kd_matakuliah 
								join tbl_sinkronisasi_renkeu rk on rk.npm_mahasiswa = x.npm_mahasiswa
								where a.kd_jadwal = '".$kd."' and c.kd_prodi = '".$kode."' and x.status_verifikasi = '1' and rk.tahunajaran = SUBSTRING(x.kd_krs FROM 13 FOR 5)");
	}

	function count_absen($npm,$kode)
	{
		return $this->db->query("SELECT count(npm_mahasiswa) as jums from tbl_absensi_mhs where npm_mahasiswa = '".$npm."' and kd_jadwal = '".$kode."'");
	}

	function load_nilai($npm,$krs,$jdl,$typ)
	{
		return $this->db->select('*')
						->from('tbl_nilai_detail_sp')
						->where('npm_mahasiswa',$npm)
						->where('kd_krs',$krs)
						->where('kd_jadwal',$jdl)
						->where('tipe',$typ)
						->get();
	}

	function cek_avbl($kd)
	{
		$kode = get_kd_jdl_remid($kd);
		return $this->db->query("SELECT * from tbl_nilai_detail_sp where kd_jadwal = '".$kode."'")->num_rows();
	}

	function compare_pst($kd)
	{
		$kode = get_kd_jdl_remid($kd);
		return $this->db->query("SELECT count(distinct npm_mahasiswa) as jums from tbl_nilai_detail_sp where kd_jadwal = '".$kode."'")->row()->jums;
	}

	function count_krs($kd)
	{
		$kode = get_kd_jdl_remid($kd);
		return $this->db->query("SELECT count(x.npm_mahasiswa) as jumlah from tbl_krs_sp x join tbl_sinkronisasi_renkeu rk on rk.npm_mahasiswa = x.npm_mahasiswa where x.kd_jadwal = '".$kode."'
								 and rk.tahunajaran = SUBSTRING(x.kd_krs FROM 13 FOR 5)")->row()->jumlah;
	}

	function sudah($kd)
	{
		$kode = get_kd_jdl_remid($kd);
		return $this->db->query("SELECT * from tbl_nilai_detail_sp where kd_jadwal = '".$kode."'")->row()->flag_publikasi;
	}

	function get_kdkrs($usr,$smt)
	{
		$a = $this->db->query("SELECT * from tbl_krs where npm_mahasiswa = '".$usr."' and semester_krs = '".$smt."'");
		return $a;
	}

	function get_ta($kd)
	{
		return $this->db->query("SELECT * from tbl_verifikasi_krs where kd_krs = '".$kd."'");
	}

	function get_mksp($log)
	{
		$max = $this->db->query("SELECT max(THSMSTRLNM) as th, KDPSTTRLNM from tbl_transaksi_nilai where NIMHSTRLNM = '".$log."'")->row();
		
		$now = yearImprove();
		// var_dump($max->th.'-'.$max->kd_jurusan.'-'.$log);exit();
		$has = $this->db->query("SELECT distinct c.kd_ruangan,e.nid,e.nama,a.NLAKHTRLNM,a.THSMSTRLNM,a.KDPSTTRLNM,b.kd_matakuliah,b.nama_matakuliah,b.sks_matakuliah,c.id_jadwal,c.kd_jadwal,c.kelas,c.hari,c.waktu_mulai,c.kd_ruangan,c.kd_dosen 
									from tbl_transaksi_nilai a 
									join tbl_matakuliah b on a.KDKMKTRLNM = b.kd_matakuliah 
									left join tbl_jadwal_matkul_sp c on b.kd_matakuliah = c.kd_matakuliah
									join tbl_karyawan e on e.nid = c.kd_dosen
									where NIMHSTRLNM = '".$log."' and THSMSTRLNM = '".$max->th."' and NLAKHTRLNM IN ('B-','C','C+','D','E')
									and b.kd_prodi = '".$max->KDPSTTRLNM."' and c.kd_jadwal like '".$max->KDPSTTRLNM."%'
									and c.kd_tahunajaran = '".$now."'");

		return $has;
	}

	function getMkSpWhenEdit($log, $arr)
	{
		$num = count($arr);
		$val = array_values($arr);

		$wrap = '';

		for ($i = 0; $i < $num; $i++) {
			
				$wrap = $wrap.'"'.$val[$i].'",';
		}

		$merge = substr($wrap, 0,-1);

		// var_dump($merge);

		$max = $this->db->query("SELECT max(THSMSTRLNM) as th, KDPSTTRLNM from tbl_transaksi_nilai where NIMHSTRLNM = '".$log."'")->row();
		
		$now = yearImprove();
		// var_dump($max->th.'-'.$max->kd_jurusan.'-'.$log);exit();
		$has = $this->db->query("SELECT distinct c.kd_ruangan,e.nid,e.nama,a.NLAKHTRLNM,a.THSMSTRLNM,a.KDPSTTRLNM,b.kd_matakuliah,b.nama_matakuliah,b.sks_matakuliah,c.id_jadwal,c.kd_jadwal,c.kelas,c.hari,c.waktu_mulai,c.kd_ruangan,c.kd_dosen 
									from tbl_transaksi_nilai a 
									join tbl_matakuliah b on a.KDKMKTRLNM = b.kd_matakuliah 
									left join tbl_jadwal_matkul_sp c on b.kd_matakuliah = c.kd_matakuliah
									join tbl_karyawan e on e.nid = c.kd_dosen
									where NIMHSTRLNM = '".$log."' and THSMSTRLNM = '".$max->th."' and NLAKHTRLNM IN ('B-','C','C+','D','E')
									and c.id_jadwal NOT IN (".$merge.")
									and b.kd_prodi = '".$max->KDPSTTRLNM."' and c.kd_jadwal like '".$max->KDPSTTRLNM."%'
									and c.kd_tahunajaran = '".$now."'");
		// var_dump($has->result());exit();
		return $has;
	}

	function ceksmt($nim,$thn)
	{
		return $this->db->select('b.semester_krs')
						->from('tbl_verifikasi_krs a')
						->join('tbl_krs b','a.kd_krs = b.kd_krs')
						->where('a.tahunajaran',$thn)
						->where('a.npm_mahasiswa',$nim)
						->get();
	}

	function generateRandomString() 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function get_ruang($kd)
	{
		return $this->db->where('id_ruangan', $kd)
						->get('tbl_ruangan');
	}
 
	function add_krs($nim,$kdjd)
	{
		$thn = $this->db->query("SELECT max(tahunajaran) as th, kd_jurusan from tbl_verifikasi_krs where npm_mahasiswa = '".$nim."'")->row()->th;
		$tahunmax = $thn+2; //$thn+2;
		$smt = $this->db->query("SELECT semester_krs from tbl_krs where kd_krs = (SELECT max(kd_krs) from  tbl_krs where npm_mahasiswa = '".$nim."')")->row();
		$dsnpa = $this->db->query("SELECT * from tbl_verifikasi_krs where npm_mahasiswa = '".$nim."' and kd_krs = (SELECT max(kd_krs) from  tbl_verifikasi_krs where npm_mahasiswa = '".$nim."')")->row();
		$nama = $this->db->query("SELECT * from tbl_mahasiswa where NIMHSMSMHS = '".$nim."'")->row();

		$jsks = 0;
		for ($x=0; $x < count($kdjd); $x++) { 
			$mk = $this->db->query("SELECT kd_matakuliah from tbl_jadwal_matkul_sp where kd_jadwal = '".$kdjd[$x]."'")->row()->kd_matakuliah;
			$sks = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$mk."' and kd_prodi = '".substr($kdjd[$x], 0,5)."'")->row()->sks_matakuliah;
			$jsks = $jsks + $sks;
		}

		$kode = $nim.$tahunmax;
		$hitung = strlen($kode);
		$jumlah = 0;
		for ($i=0; $i <= $hitung; $i++) {
			$char = substr($kode,$i,1);
			$jumlah = $jumlah + $char;
		}
		$mod = $jumlah%10;
		$kodkrs = $kode.$mod;

		$cek = $this->db->select('*')
						->from('tbl_verifikasi_krs_sp')
						->like('kd_krs',$kode,'after')
						->get()->result();

		if (count($cek) == 0) {
			$kodver = $kodkrs.$jsks.$this->generateRandomString();
			$verif = array(
				'kd_krs'			=> $kodkrs,
				'id_pembimbing'		=> $dsnpa->id_pembimbing,
				'npm_mahasiswa'		=> $nim,
				'kd_jurusan'		=> $nama->KDPSTMSMHS,
				'tahunajaran'		=> $tahunmax,
				'keterangan_krs'	=> '-',
				'status_verifikasi'	=> 1,
				'tgl_bimbingan'		=> date('Y-m-d H:i:s'),
				'key'				=> $this->generateRandomString(),
				'kd_krs_verifikasi'	=> md5(md5($kodver).key_verifikasi),
				'jumlah_sks'		=> $jsks,
				'slug_url'			=> url_title($nama->NMMHSMSMHS, '_', TRUE),
				'kelas'				=> $dsnpa->kelas
				);
			// var_dump($verif);exit();
			$this->db->insert('tbl_verifikasi_krs_sp', $verif);

			$hitungjd = count($kdjd);
			for ($i=0; $i < $hitungjd; $i++) { 
				$matkul = $this->db->query("SELECT kd_matakuliah from tbl_jadwal_matkul_sp where kd_jadwal = '".$kdjd[$i]."'")->row()->kd_matakuliah;
				$krs = array(
					'npm_mahasiswa' => $nim,
					'kd_matakuliah' => $matkul,
					'semester_krs'	=> $smt->semester_krs,
					'kd_krs'		=> $kodkrs,
					'kd_jadwal'		=> $kdjd[$i],
					'flag_open'		=> 0
					);
				$this->db->insert('tbl_krs_sp', $krs);
			}

		} else {
			if (count($kdjd) == 0 or is_null($kdjd)) {
				$this->db->where('kd_krs', $kodkrs);
				$this->db->delete('tbl_verifikasi_krs_sp');
				$this->db->where('kd_krs', $kodkrs);
				$this->db->delete('tbl_krs_sp');
				// die('hahaha');

				// delete on tbl sinkron renkeu
				$this->db->where('status', 1);
				$hey = $this->db->get('tbl_tahunakademik')->row();
				$fixyear = $hey->kode+2;

				$this->db->where('npm_mahasiswa', substr($kd, 0,12));
				$this->db->where('tahunajaran', $fixyear);
				$huu = $this->db->get('tbl_sinkronisasi_renkeu')->result();

				if ($huu) {
					$this->db->where('npm_mahasiswa', substr($kd, 0,12));
					$this->db->where('tahunajaran', $fixyear);
					$this->db->delete('tbl_sinkronisasi_renkeu');
				}

			} else {
				$kodver = $kodkrs.$jsks.$this->generateRandomString();
				$verif = array(
					'status_verifikasi'	=> 1,
					'tgl_bimbingan'		=> date('Y-m-d H:i:s'),
					'key'				=> $this->generateRandomString(),
					'kd_krs_verifikasi'	=> md5(md5($kodver).key_verifikasi),
					'jumlah_sks'		=> $jsks
					);
				// var_dump($verif);exit();
				$this->db->where('kd_krs', $kodkrs);
				$this->db->update('tbl_verifikasi_krs_sp', $verif);

				$this->db->where('kd_krs', $kodkrs);
				$this->db->delete('tbl_krs_sp');

				$hitungjd = count($kdjd);
				for ($i=0; $i < $hitungjd; $i++) { 
					$matkul = $this->db->query("SELECT kd_matakuliah from tbl_jadwal_matkul_sp where kd_jadwal = '".$kdjd[$i]."'")->row()->kd_matakuliah;
					$krs = array(
						'npm_mahasiswa' => $nim,
						'kd_matakuliah' => $matkul,
						'semester_krs'	=> $smt->semester_krs,
						'kd_krs'		=> $kodkrs,
						'kd_jadwal'		=> $kdjd[$i],
						'flag_open'		=> 0
						);
					$this->db->insert('tbl_krs_sp', $krs);
				}

				// delete on tbl sinkron renkeu
				$this->db->where('status', 1);
				$hey = $this->db->get('tbl_tahunakademik')->row();
				$fixyear = $hey->kode+2;

				$this->db->where('npm_mahasiswa', substr($kd, 0,12));
				$this->db->where('tahunajaran', $fixyear);
				$huu = $this->db->get('tbl_sinkronisasi_renkeu')->result();

				if ($huu) {
					$this->db->where('npm_mahasiswa', substr($kd, 0,12));
					$this->db->where('tahunajaran', $fixyear);
					$this->db->delete('tbl_sinkronisasi_renkeu');
				}
			}
		}
		
	}

	function cek_thsp($npm)
	{
		$a = $this->db->select('max(tahunajaran) as tahun')
						->from('tbl_verifikasi_krs')
						->where('npm_mahasiswa',$npm)
						->get()->row()->tahun;
		$end = $a+2;
		return $end;
	}

	function list_krs_sp($npm)
	{
		$thn = $this->db->query("SELECT max(tahunajaran) as th from tbl_verifikasi_krs where npm_mahasiswa = '".$npm."'")->row()->th;
		$tahunmax = $thn+2; //$thn+2;
		$prodi = $this->db->where('NIMHSMSMHS', $npm)->get('tbl_mahasiswa')->row();
		
		return $this->db->select('b.kd_krs,c.id_jadwal,b.kd_matakuliah,f.nama_matakuliah,f.sks_matakuliah,c.hari,c.kelas,c.waktu_mulai,d.nama,c.kd_ruangan,c.open')
						->from('tbl_verifikasi_krs_sp a')
						->join('tbl_krs_sp b','a.kd_krs = b.kd_krs')
						->join('tbl_jadwal_matkul_sp c','c.kd_jadwal = b.kd_jadwal')
						->join('tbl_matakuliah f','f.kd_matakuliah = c.kd_matakuliah')
						->join('tbl_karyawan d','d.nid = c.kd_dosen')
						->like('c.kd_jadwal',$prodi->KDPSTMSMHS,'after')
						->where('a.npm_mahasiswa',$npm)
						->where('a.tahunajaran',$tahunmax)
						->where('c.kd_tahunajaran',$tahunmax)
						->where('f.kd_prodi',$prodi->KDPSTMSMHS)
						->get();
	}

	function cek_verif_krs($kode)
	{
		return $this->db->select('*')
						->from('tbl_verifikasi_krs_sp a')
						->join('tbl_krs_sp b','a.kd_krs = b.kd_krs')
						->like('a.kd_krs',$kode,'after')
						->get();
	}

	function cek_on_edit_sp($krs,$id)
	{
		return $this->db->distinct('npm_mahasiswa')
						->from('tbl_krs_sp')
						->where('kd_krs',$krs)
						->where('kd_jadwal',$id)
						->get();
	}

	function change_krs($log)
	{
		$max = $this->db->query("SELECT tahunajaran, kd_jurusan from tbl_verifikasi_krs_sp where kd_krs = '".$log."'")->row();
		$has = $this->db->query("SELECT distinct c.kd_ruangan,e.nid,e.nama,b.kd_matakuliah,b.nama_matakuliah,b.sks_matakuliah,c.id_jadwal,c.kd_jadwal,c.kelas,c.hari,c.waktu_mulai,c.kd_ruangan,c.kd_dosen 
									from tbl_verifikasi_krs_sp q
									join tbl_krs_sp w on q.kd_krs = w.kd_krs
									join tbl_matakuliah b on w.kd_matakuliah = b.kd_matakuliah 
									left join tbl_jadwal_matkul_sp c on b.kd_matakuliah = c.kd_matakuliah
									join tbl_karyawan e on e.nid = c.kd_dosen
									where q.kd_krs = '".$log."' and tahunajaran = '".$max->tahunajaran."'
									and b.kd_prodi = '".$max->kd_jurusan."' and c.kd_jadwal like '".$max->kd_jurusan."%'
									and c.kd_tahunajaran = '".$max->tahunajaran."'");
		// var_dump($has->result());exit();
		return $has;
	}

	function cek_prasyarat($mk,$npm)
	{
		// var_dump($mk);
		$nilai = array('D','E');
		$data = $this->db->select('KDKMKTRLNM,NLAKHTRLNM')
						->from('tbl_transaksi_nilai')
						->where_in('KDKMKTRLNM',$mk)
						->where_not_in('NLAKHTRLNM',$nilai)
						->where('NIMHSTRLNM',$npm)
						->get();
						// var_dump($data);exit();
		return $data;
	}

	function cek_prasyarat_konversi($mk,$npm)
	{
		// var_dump($mk);
		$data = $this->db->select('KDKMKTRLNM,NLAKHTRLNM')
						->from('tbl_transaksi_nilai_konversi')
						->where_in('KDKMKTRLNM',$mk)
						->where('NIMHSTRLNM',$npm)
						->get();
						// var_dump($data);exit();
		return $data;
	}

	function get_ipk($npm)
	{
		return $this->db->query('SELECT nl.`KDKMKTRLNM`,
								IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
									(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
								IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
									(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
								MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,nl.`THSMSTRLNM`,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai nl 
								WHERE nl.`NIMHSTRLNM` = "'.$npm.'" and NLAKHTRLNM != "T"
								GROUP BY nl.`KDKMKTRLNM` ORDER BY nl.`THSMSTRLNM` ASC');
	}

	function get_krs_mahasiswa($kd)
	{
		return $this->db->select('*')
						//tes
						->from('tbl_verifikasi_krs_tes a')

						//live
						// ->from('tbl_verifikasi_krs a')

						->join('tbl_krs_tes b','a.kd_krs = b.kd_krs')
						->where('a.kd_krs',$kd)
						->get();
	}

	function loadkrssatuan($id,$pst)
	{
		return $this->db->query("SELECT * from tbl_mahasiswa where (NMMHSMSMHS like '".$id."%' or NIMHSMSMHS like '".$id."%') and KDPSTMSMHS = '".$pst."'");
	}

	function notif_krs($kd)
	{
		return $this->db->select('*')

						//tes
						// ->from('tbl_verifikasi_krs_tes')

						//live
						->from('tbl_verifikasi_krs')

						->like('kd_krs',$kd,'after')
						->get();
	}

	function notif_khs($kd)
	{
		return $this->db->select('*')
						->from('tbl_nilai_detail')
						->like('kd_krs',$kd,'after')
						->get();
	}

	function notif_for_pa($nid,$thn)
	{
		return $this->db->where('id_pembimbing', $nid)
						->where('tahunajaran', $thn)
						->where('status_verifikasi', 4)

						//tes
						->get('tbl_verifikasi_krs');

						//live
						// ->get('tbl_verifikasi_krs');
	}

	function loadnewkrs($usr,$thn)
	{
		if ($usr == 'baa') {
			return $this->db->where('tahunajaran', $thn)
							//tes
							->get('tbl_verifikasi_krs');
							//live
							// ->get('tbl_verifikasi_krs');
		} else {
			return $this->db->where('kd_jurusan', $usr)
							->where('tahunajaran', $thn)
							//tes
							// ->get('tbl_verifikasi_krs_tes');
							//live
							->get('tbl_verifikasi_krs');
		}
	}

	function loadapvkrs($usr,$thn)
	{
		if ($usr == 'baa') {
			return $this->db->where('tahunajaran', $thn)
							->where('status_verifikasi', 1)
							//tes
							->get('tbl_verifikasi_krs');
							//live
							// ->get('tbl_verifikasi_krs');
		} else {
			return $this->db->where('kd_jurusan', $usr)
							->where('tahunajaran', $thn)
							->where('status_verifikasi', 1)
							//tes
							->get('tbl_verifikasi_krs');
							//live
							// ->get('tbl_verifikasi_krs');	
		}
	}

	function load_notif_baa($thn)
	{
		return $this->db->where('tahunajaran', $thn)
						->get('tbl_verifikasi_krs');
	}

	function match_pa($usr)
	{
		return $this->db->query("SELECT kd_dosen from tbl_pa where npm_mahasiswa = '".$usr."'");
	}

	function countimpvclass($usr,$thn)
	{
		return $this->db->select('count(kd_jadwal) as jum')
						->from('tbl_jadwal_matkul_sp')
						->like('kd_jadwal',$usr,'after')
						->where('kd_tahunajaran',$thn)
						->get();
	}

	function mhsimpclass($usr,$thn)
	{
		return $this->db->select('count(distinct a.npm_mahasiswa) as mhs')
						->from('tbl_verifikasi_krs_sp a')
						->join('tbl_krs_sp b', 'a.kd_krs = b.kd_krs')
						->where('a.kd_jurusan',$usr)
						->where('a.tahunajaran',$thn)
						->get();
	}

	function nonactivemhs($usr,$thn)
	{
		return $this->db->query("SELECT DISTINCT KDPSTMSMHS, COUNT(NIMHSMSMHS) as jum FROM tbl_mahasiswa 
								WHERE NIMHSMSMHS NOT IN (SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = ".$thn.") 
								AND NIMHSMSMHS NOT IN (SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = ".$thn." AND validate = 1 AND tahunajaran = ".$thn.")
								AND KDPSTMSMHS = ".$usr." AND BTSTUMSMHS >= ".$thn." AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K' AND TAHUNMSMHS < '2017'");
	}

	function cutimhs($usr,$thn)
	{
		return $this->db->query("SELECT COUNT(DISTINCT npm) AS jum FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa b ON a.`npm`= b.`NIMHSMSMHS`
                                WHERE a.`status`= 'C' AND a.`tahunajaran` = '".$thn."' and a.validate >= '1' AND b.`KDPSTMSMHS` = '".$usr."'
                                ORDER BY b.`KDPSTMSMHS` ASC");
	}

	function load_stmhs_prd_actv($usr,$thn)
	{
		return $this->db->query('SELECT DISTINCT d.`NIMHSMSMHS`,d.`NMMHSMSMHS`,a.`semester_krs`,a.kd_krs,SUM(c.`sks_matakuliah`) AS tot
								FROM tbl_krs a JOIN tbl_matakuliah c ON a.`kd_matakuliah` = c.`kd_matakuliah`
								JOIN tbl_mahasiswa d ON a.`npm_mahasiswa` = d.`NIMHSMSMHS`
								WHERE a.kd_krs LIKE CONCAT (a.npm_mahasiswa,"'.$thn.'%") AND d.KDPSTMSMHS = "'.$usr.'"
								AND c.`kd_prodi` = "'.$usr.'" GROUP BY a.`kd_krs`');
	}

	function load_stmhs_prd_nctv($usr,$thn)
	{
		$aa = substr($thn, 0,4);
        $bb = $aa+1;
		return $this->db->query("SELECT NIMHSMSMHS,NMMHSMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS NOT IN (SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = '".$thn."') 
								AND NIMHSMSMHS NOT IN (SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = '".$thn."' AND validate = 1 AND tahunajaran = ".$thn.")
								AND KDPSTMSMHS = '".$usr."' AND BTSTUMSMHS >= '".$thn."' AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K' AND TAHUNMSMHS < ".$bb."");
	}

	function load_stmhs_prd_cuti($usr,$thn)
	{
		return $this->db->query('SELECT distinct a.NIMHSMSMHS,a.NMMHSMSMHS FROM tbl_mahasiswa a JOIN tbl_status_mahasiswa b ON a.NIMHSMSMHS = b.npm
								WHERE b.status = "C" AND a.KDPSTMSMHS = "'.$usr.'" AND b.tahunajaran = "'.$thn.'" and validate >= 1');
	}

	function loadmkrps($usr,$thn)
	{
		return $this->db->query("SELECT distinct b.kd_prodi,b.kd_matakuliah,b.nama_matakuliah,b.sks_matakuliah FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											WHERE a.`kd_dosen` = '".$usr."' and a.kd_tahunajaran = '".$thn."'
											group by a.kd_jadwal");
	}

	function load_data_for_rps($kd)
	{
		return $this->db->select('*')->from('tbl_matakuliah')->where('kd_matakuliah',$kd)->get();
	}

	function load_nilai_bfore($usr,$thn,$kdmk)
	{
		return $this->db->select('*')
						->from('tbl_transaksi_nilai')
						->where('NIMHSTRLNM', $usr)
						->where('THSMSTRLNM', $thn)
						->where('KDKMKTRLNM', $kdmk)
						->get();
	}

	function load_modal_rps($kd)
	{
		return $this->db->select('*')
						->from('tbl_matakuliah')
						->where('kd_matakuliah', $kd)
						->get();
	}

	function getdetail($table,$pk,$value,$key,$order)

	{

		$this->db->where_in($pk,$value);

		$this->db->order_by($key, $order);

		$q = $this->db->get($table);

		return $q;

	}

	function getdetailmkonprodi($table,$pk,$value,$key,$order,$prodi)
	{
		$this->db->where_in($pk,$value);

		$this->db->where('kd_prodi', $prodi);

		$this->db->order_by($key, $order);

		$q = $this->db->get($table);

		return $q;
	}

	function cek_available_rps($kdmk,$prd,$usr)
	{
		return $this->db->select('*')
					->from('tbl_file_rps')
					->where('kd_matakuliah',$kdmk)
					->where('prodi',$prd)
					->where('userfile',$usr)
					->order_by('id','desc')
					->limit(1)
					->get();
	}

	function loaddosenrps($usr,$thn)
	{
		$this->db->distinct();
		$this->db->select('a.kd_matakuliah,a.nama_matakuliah,a.sks_matakuliah,b.kd_dosen,c.nama,a.kd_prodi');
		$this->db->from('tbl_matakuliah a');
		$this->db->join('tbl_jadwal_matkul b', 'a.kd_matakuliah = b.kd_matakuliah');
		$this->db->join('tbl_karyawan c', 'c.nid = b.kd_dosen');
		$this->db->where('a.kd_prodi', $usr);
		$this->db->where('b.kd_tahunajaran', $thn);
		return $this->db->get();
	}

	function load_pkg_for_mk($usr)
	{
		$this->db->distinct();
		return $this->db->select('c.kd_matakuliah,c.nama_matakuliah,c.sks_matakuliah,b.semester_kd_matakuliah')
						->from('tbl_kurikulum_new a')
						->join('tbl_kurikulum_matkul_new b','a.kd_kurikulum = b.kd_kurikulum')
						->join('tbl_matakuliah c','c.kd_matakuliah = b.kd_matakuliah')
						->where('a.kd_prodi', $usr)
						->where('a.status', 1)
						->where('c.kd_prodi',$usr)
						->where('b.semester_kd_matakuliah', 1)
						->get();
	}

	function get_all_khs_mahasiswa($npm){		
		$kd_prodi = $this->db->query('SELECT KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="'.$npm.'"')->row();
		
		//ditambahin like -
		return $this->db->query('SELECT distinct b.THSMSTRLNM,b.KDPSTTRLNM from tbl_transaksi_nilai_sp b
		WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="'.$npm.'" ORDER BY THSMSTRLNM ASC');

	}

	function load_list_kelas($u,$y)
	{
		return $this->db->distinct()
						->select('kelas,waktu_kelas')
						->from('tbl_jadwal_matkul')
						->where('kelas !=', '')
						->like('kd_jadwal',$u,'after')
						->not_like('kd_matakuliah','MKDU', 'after')
						->not_like('kd_matakuliah','MKU','after')
						->where('kd_tahunajaran',$y)
						->order_by('kelas', 'asc')
						->get();
	}

	function emptyroom($h,$o,$y,$d)
	{
		return $this->db->query("SELECT a.*,b.`lantai`,c.`gedung` FROM tbl_ruangan a
							JOIN tbl_lantai b ON a.`id_lantai` = b.`id_lantai`
							JOIN tbl_gedung c ON b.`id_gedung` = c.`id_gedung`
							WHERE id_ruangan NOT IN 
							(
								SELECT kd_ruangan FROM tbl_jadwal_matkul WHERE kd_tahunajaran = '".$y."' 
								AND ((waktu_mulai >= '".$h."' AND waktu_mulai <= '".$o."') OR (waktu_selesai >= '".$h."' AND waktu_selesai <= '".$o."')) AND hari = ".$d.")");

	}

	function load_krs_pasca($usr)
	{
		return $this->db->query("SELECT b.NIMHSMSMHS,b.NMMHSMSMHS,a.ID_registrasi,b.KDPSTMSMHS,(CASE WHEN (a.jenis_pmb = 'MB') THEN 'MAHASISWA BARU' WHEN (a.jenis_pmb = 'KV') THEN 'KONVERSI' ELSE 'READMISI' END) AS jenis_pmb,
								(CASE WHEN (a.kampus = 'bks') THEN 'BEKASI' ELSE 'JAKARTA' END) AS kampus, 
								(CASE WHEN (a.kelas = 'KR') THEN 'KARYAWAN' END) AS kelas FROM tbl_pmb_s2 a JOIN tbl_mahasiswa b ON a.npm_baru = b.NIMHSMSMHS 
								where opsi_prodi_s2 = '".$usr."' ORDER BY b.NIMHSMSMHS");
	}

	function load_krs_pasca_newpmb($usr, $typcls, $angkatan)
	{
		$this->reg = $this->load->database('regis', TRUE);
		if ($typcls == 'PK') {
			$typ = 'KY';
		} else {
			$typ = $typcls;
		}

		$this->reg->from('loadfixmaba');
		$this->reg->where('prodi', $usr);
		$this->reg->where('kelas', $typ);
		$this->reg->where('status >', 2);
		$this->reg->where('SUBSTRING(npm_baru, 1, 4) =', $angkatan);
		$this->reg->order_by('npm_baru', 'asc');
		return $this->reg->get();
	}

	function load_room()
	{
		return $this->db->select('a.*, b.lantai, c.id_gedung, c.gedung')
						->from('tbl_ruangan a')
						->join('tbl_lantai b','a.id_lantai = b.id_lantai')
						->join('tbl_gedung c','c.id_gedung = b.id_gedung')
						->get();
	}

	function load_edit_room($id)
	{
		return $this->db->select('a.*, b.lantai, c.id_gedung, c.gedung')
						->from('tbl_ruangan a')
						->join('tbl_lantai b','a.id_lantai = b.id_lantai')
						->join('tbl_gedung c','c.id_gedung = b.id_gedung')
						->where('a.id_ruangan',$id)
						->get();
	}

	function mhs_konv($u)
	{
		$this->db->select('mhs.*,nl.id');
		$this->db->from('tbl_mahasiswa mhs');
		$this->db->join('tbl_transaksi_nilai_konversi nl','nl.NIMHSTRLNM = mhs.NIMHSMSMHS','left');
		$this->db->where('mhs.TAHUNMSMHS', date('Y'));
		//$this->db->where(substr('mhs.NIMHSMSMHS', 8, 1), '7');
		$this->db->where('mhs.KDPSTMSMHS',$u);

		//pasca beda digit pengkodean
		if ($u == 74101 || $u == 61101) {
			$this->db->where('SUBSTRING(mhs.NIMHSMSMHS,8,1)',7);
		}else{
			$this->db->where('SUBSTRING(mhs.NIMHSMSMHS,9,1)',7);
		}

		$this->db->group_by('mhs.NIMHSMSMHS');
		$this->db->order_by('nl.id','desc');
		$data = $this->db->get()->result();
		return $data;
	}

	function getDetailKaryawan($id)
	{
		$this->$this->db->select('*');
		$this->db->from('tbl_karyawan a');
		$this->db->join('tbl_biodata_dosen b', 'a.nid = b.nid', 'left');
		$this->db->where('a.nid', $nid);
		return $this->db->get();
	}

	function exportdosen($tipedosen,$jabfung,$home,$prodi)
	{
		if ($tipedosen == 'ALL') {
			if ($jabfung == 'ALL') {
				if ($home == 'ALL') {
					$this->db->select('*');
					$this->db->from('tbl_karyawan');
					$this->db->where('jabatan_id', $prodi);
					return $this->db->get();
				} else {
					$this->db->select('*');
					$this->db->from('tbl_karyawan');
					$this->db->where('homebase', $home);
					$this->db->where('jabatan_id', $prodi);
					return $this->db->get();
				}
			} else {
				if ($home == 'ALL') {
					$this->db->select('*');
					$this->db->from('tbl_karyawan');
					$this->db->where('jabfung', $jabfung);
					$this->db->where('jabatan_id', $prodi);
					return $this->db->get();
				}else{
					$this->db->select('*');
					$this->db->from('tbl_karyawan');
					$this->db->where('jabfung', $jabfung);
					$this->db->where('homebase', $home);
					$this->db->where('jabatan_id', $prodi);
					return $this->db->get();
				}				
			}
		} else {
			if ($jabfung == 'ALL') {
				if ($home == 'ALL') {
					$this->db->select('*');
					$this->db->from('tbl_karyawan');
					$this->db->where('jabatan_id', $prodi);
					$this->db->where('tetap', $tipedosen);
					return $this->db->get();
				} else {
					$this->db->select('*');
					$this->db->from('tbl_karyawan');
					$this->db->where('homebase', $home);
					$this->db->where('jabatan_id', $prodi);
					$this->db->where('tetap', $tipedosen);
					return $this->db->get();
				}
			} else {
				if ($home == 'ALL') {
					$this->db->select('*');
					$this->db->from('tbl_karyawan');
					$this->db->where('jabfung', $jabfung);
					$this->db->where('jabatan_id', $prodi);
					$this->db->where('tetap', $tipedosen);
					return $this->db->get();
				} else {
					$this->db->select('*');
					$this->db->from('tbl_karyawan');
					$this->db->where('jabfung', $jabfung);
					$this->db->where('homebase', $home);
					$this->db->where('jabatan_id', $prodi);
					$this->db->where('tetap', $tipedosen);
					return $this->db->get();
				}				
			}
		}
	}

	function getBebanDosen($log,$kd)
	{
		return $this->db->query("SELECT DISTINCT SUM(c.`sks_matakuliah`) AS jums FROM tbl_jadwal_matkul b
											JOIN tbl_matakuliah c ON c.`kd_matakuliah` = b.`kd_matakuliah`
											WHERE b.kd_dosen = '".$log."' AND b.kd_tahunajaran <= '".$kd."' AND c.kd_prodi = SUBSTR(b.kd_jadwal,1,5)
											AND (c.nama_matakuliah NOT LIKE 'skripsi%' AND c.nama_matakuliah NOT LIKE 'kerja praktek%' AND c.nama_matakuliah NOT LIKE 'tesis%' AND c.nama_matakuliah NOT LIKE 'magang%'
											AND c.nama_matakuliah NOT LIKE 'kuliah kerja%') AND b.`kd_jadwal` IN (SELECT kd_jadwal FROM tbl_krs WHERE kd_jadwal IS NOT NULL)");
	}

	function getParamByTopic($code)
	{
		$CI =& get_instance();
		$CI->edom = $CI->load->database('eval', TRUE);
		return $CI->edom->where('kd_topik',$code)->order_by('id_parameter','asc')->get('tbl_parameter');
	}

	function moreWhere($tbl,$arr)
	{
		for ($i = 0; $i < count($arr); $i++) {

			$this->db->where(array_keys($arr)[$i], array_values($arr)[$i]);

		}

		return $this->db->get($tbl);
	}

	function belumdaftarulang($pro,$yrs,$acd)
	{
		return $this->db->query('SELECT DISTINCT a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,a.KDPSTMSMHS 
								FROM tbl_mahasiswa a 
								JOIN tbl_jurusan_prodi b ON a.`KDPSTMSMHS`=b.`kd_prodi` 
								WHERE b.`kd_prodi`="'.$pro.'"
								AND a.`TAHUNMSMHS`="'.$yrs.'" AND (a.STMHSMSMHS != "K" AND a.STMHSMSMHS != "L")
								AND a.`NIMHSMSMHS` NOT IN (SELECT npm_mahasiswa FROM tbl_sinkronisasi_renkeu WHERE tahunajaran = "'.$acd.'")
								ORDER BY a.`NIMHSMSMHS` ASC');
	}

	function notPayButStudy($pro,$yrs,$acd)
	{
		return $this->db->query('SELECT DISTINCT a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,a.KDPSTMSMHS 
								FROM tbl_mahasiswa a 
								JOIN tbl_jurusan_prodi b ON a.`KDPSTMSMHS`=b.`kd_prodi` 
								WHERE b.`kd_prodi`="'.$pro.'"
								AND a.`TAHUNMSMHS`="'.$yrs.'" AND (a.STMHSMSMHS != "K" AND a.STMHSMSMHS != "L")
								AND a.`NIMHSMSMHS` NOT IN (SELECT npm_mahasiswa FROM tbl_sinkronisasi_renkeu WHERE tahunajaran = "'.$acd.'")
								AND a.NIMHSMSMHS IN (SELECT npm_mahasiswa from tbl_verifikasi_krs where tahunajaran = "'.$acd.'")
								ORDER BY a.`NIMHSMSMHS` ASC');
	}
}

/* End of file Temph_model.php */
/* Location: ./application/models/Temph_model.php */