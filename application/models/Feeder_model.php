<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feeder_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function isthisngulang($code,$uid,$own,$year)
	{
		$loo = $this->db->where('kd_lama', $code)
						->where('kd_prodi', $own)
						->where('flag', '1')
						->get('tbl_konversi_matkul_temp');

		if ($loo->num_rows() > 0) {

			$this->db->where('KDKMKTRLNM', $loo->row()->kd_baru);
			$this->db->where('NIMHSTRLNM', $uid);
			$this->db->where('THSMSTRLNM <', $year);
			return $this->db->get('tbl_transaksi_nilai');

		} else {

			$this->db->where('KDKMKTRLNM', $code);
			$this->db->where('NIMHSTRLNM', $uid);
			$this->db->where('THSMSTRLNM <', $year);
			return $this->db->get('tbl_transaksi_nilai');

		}
	}

}

/* End of file Feeder_model.php */
/* Location: ./application/models/Feeder_model.php */