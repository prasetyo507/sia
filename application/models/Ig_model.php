<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ig_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->dbreg = $this->load->database('regis', TRUE);
	}

	function load_change($f)
	{
		$min = date('Y')-8;
		$now = date('Y');

		$this->db->select('KDPSTMSMHS,TAHUNMSMHS,COUNT(NIMHSMSMHS) AS jums');
		$this->db->from('tbl_mahasiswa b');
		$this->db->where('KDPSTMSMHS', $f);
		$this->db->where('TAHUNMSMHS <=', $now);
		$this->db->where('TAHUNMSMHS >=', $min);
		$this->db->group_by('TAHUNMSMHS');
		return $this->db->get();

		/*
		return $this->db->query("SELECT b.`TAHUNMSMHS`,a.kd_prodi,a.prodi,COUNT(NIMHSMSMHS) AS jums FROM tbl_mahasiswa b  JOIN tbl_jurusan_prodi a ON a.kd_prodi = b.KDPSTMSMHS 
								 WHERE a.kd_prodi = '".$f."' AND b.TAHUNMSMHS <= '".$now."' AND b.TAHUNMSMHS > '".$min."' GROUP BY b.`TAHUNMSMHS`");
		*/
	}

	function getjumlahmhs($id)
	{
		$min = date('Y')-11;
		if ($id == '0') {
			$q = $this->db->query("SELECT DISTINCT TAHUNMSMHS,COUNT(DISTINCT NIMHSMSMHS) as jml FROM tbl_mahasiswa
								where TAHUNMSMHS < '".date('Y')."' and TAHUNMSMHS > '".$min."'
								GROUP BY TAHUNMSMHS
								ORDER BY TAHUNMSMHS ASC
								LIMIT 10")->result();
		} else {
			$q = $this->db->query("SELECT DISTINCT TAHUNMSMHS,COUNT(DISTINCT NIMHSMSMHS) as jml FROM tbl_mahasiswa
								where TAHUNMSMHS < 2017 and TAHUNMSMHS > 2006 and KDPSTMSMHS = '".$id."'
								GROUP BY TAHUNMSMHS
								ORDER BY TAHUNMSMHS ASC
								LIMIT 10")->result();
		}
		return $q;
	}

	function getdosen()
	{
		$q = $this->db->query("SELECT DISTINCT tetap,COUNT(DISTINCT nid) as jml FROM tbl_karyawan
								GROUP BY tetap
								ORDER BY tetap DESC")->result();
		return $q;
	}

	function getlulusan($id)
	{
		if ($id == '0') {
			$q = $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) AS tahun,COUNT(DISTINCT NIMHSMSMHS) as jml FROM tbl_mahasiswa
									GROUP BY YEAR(TGLLSMSMHS)
									ORDER BY YEAR(TGLLSMSMHS) DESC
									LIMIT 5")->result();
			return $q;
		} else {
			$q = $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) AS tahun,COUNT(DISTINCT NIMHSMSMHS) as jml FROM tbl_mahasiswa
									where KDPSTMSMHS = '".$id."'
									GROUP BY YEAR(TGLLSMSMHS)
									ORDER BY YEAR(TGLLSMSMHS) DESC
									LIMIT 5")->result();
			return $q;
		}
	}

	function getjumlahmabadaftar($tahun)
	{
		if ($tahun < 2017) {
			$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_".$tahun."")->row()->jml;
			$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_".$tahun."")->row()->jml;
		} elseif ($tahun > 2017) {
			$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb")->row()->jml;
		} else {
			$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba")->row()->jml;
			$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2")->row()->jml;
		}
		return $q+$q2;
	}

	/* diganti getAmountRegProdi
	function getjumlahmabadaftarprodi($tahun,$prodi)
	{
		if ($tahun < 2017) {
			$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_".$tahun." where prodi = '".$prodi."'")->row()->jml;
			$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_".$tahun." where opsi_prodi_s2 = '".$prodi."'")->row()->jml;
		} elseif ($tahun > 2017) {
			$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb where prodi = '".$prodi."'")->row()->jml;
			$q2 = $q;
		} else {
			$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba where prodi = '".$prodi."'")->row()->jml;
			$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 where opsi_prodi_s2 = '".$prodi."'")->row()->jml;
		}
		return $q+$q2;
	}
	*/

	function pesertates($year, $prodi)
	{
		if ($year < 2017) {

			if ($prodi == 74101 or $prodi == 61101) {
				$lulus = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_" . $year . " 
										where status IS NOT NULL and opsi_prodi_s2 = '" . $prodi . "'")->row()->jml;
				$tidak = 0;
			} else {
				$lulus = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $year . " 
										where status IS NOT NULL and prodi = '" . $prodi . "'")->row()->jml;	
				$tidak = 0;
			}
			
		} elseif ($year == 2017) {

			if ($prodi == 74101 or $prodi == 61101) {
				$lulus = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2
										where status IS NOT NULL and opsi_prodi_s2 = '" . $prodi . "'")->row()->jml;
				$tidak = 0;
			} else {
				$lulus = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba
										where status IS NOT NULL and prodi = '" . $prodi . "'")->row()->jml;	
				$tidak = 0;
			}

		} else {
			
			$lulus = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb
											where status_lulus = 1 and prodi = '" . $prodi . "' and SUBSTRING(nomor_registrasi,1,2) = substr('".$year."', 3,4)")->row()->jml;
			$tidak = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb
											where status_lulus = 2 and prodi = '" . $prodi . "' and SUBSTRING(nomor_registrasi,1,2) = substr('".$year."', 3,4)")->row()->jml;

		}

		return $lulus + $tidak;
		
	}

	function getAmountRegProdi($year, $prodi)
	{
		// kondisi untuk pertahun
		if ($year < 2017) {
			
			// jika program S2
			if ($prodi == 74101 or $prodi == 61101) {
				$amount = $this->db->query("SELECT count(DISTINCT ID_registrasi) as amt from tbl_pmb_s2_".$year." 
											where opsi_prodi_s2 = '".$prodi."'")->row()->amt;
			// jika program S1
			} else {
				$amount = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as amt from tbl_form_camaba_".$year."
											where prodi = '".$prodi."'")->row()->amt;
			}
			

		} elseif ($year == 2017) {
			
			// jika program S2
			if ($prodi == 74101 or $prodi == 61101) {
				$amount = $this->db->query("SELECT count(DISTINCT ID_registrasi) as amt from tbl_pmb_s2
											where opsi_prodi_s2 = '".$prodi."'")->row()->amt;
			// jika program S1
			} else {
				$amount = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as amt from tbl_form_camaba
											where prodi = '".$prodi."'")->row()->amt;
			}

		} else {

			$amount = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as amt from tbl_form_pmb
											where prodi = '".$prodi."' and SUBSTRING(nomor_registrasi,1,2) = substr('".$year."', 3,4)")->row()->amt;

		}

		return $amount;
		
	}

	function getjumlahmabastatus($tahun,$status)
	{
		if ($tahun < 2017) {
			if ($status == 1) {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_".$tahun." where status = 1")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_".$tahun." where status = 1")->row()->jml;
			} else {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_".$tahun." where status > 2")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_".$tahun." where status > 2")->row()->jml;
			}
		} elseif ($tahun > 2017) {
			if ($status == 1) {
				$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb where status = 1")->row()->jml;
				$q2 = $q;
			} else {
				$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb where status > '".$status."'")->row()->jml;
				$q2 = $q;
			}
		} else {
			if ($status == 1) {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba where status = 1")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 where status = 1")->row()->jml;
			} else {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba where status > 2")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 where status > 2")->row()->jml;
			}
		}
		return $q+$q2;
	}

	function getReRegist($year, $prodi)
	{
		if ($year <  2017) {
			
			if ($prodi == 74101 or $prodi == 61101) {
				$amt = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jum from tbl_pmb_s2_".$year." 
										where status > 2 and opsi_prodi_s2 = '".$prodi."'")->row()->jum;
			} else {
				$amt = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jum from tbl_form_camaba_".$year." 
										where status > 2 and prodi = '".$prodi."'")->row()->jum;
			}
			

		} elseif ($year == 2017) {
			
			if ($prodi == 74101 or $prodi == 61101) {
				$amt = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jum from tbl_pmb_s2
										where status > 2 and opsi_prodi_s2 = '".$prodi."'")->row()->jum;
			} else {
				$amt = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jum from tbl_form_camaba
										where status > 2 and prodi = '".$prodi."'")->row()->jum;
			}
			

		} else {
			
			$amt = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jum from tbl_form_pmb
										where status > 2 and prodi = '".$prodi."' and SUBSTRING(nomor_registrasi,1,2) = substr('".$year."', 3,4)")->row()->jum;

		}

		return $amt;
		
	}

	function getjumlahmabastatusprodi($tahun,$status,$prodi)
	{
		if ($tahun < 2017) {
			if ($status == 1) {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_".$tahun." where status = 1 and prodi = '".$prodi."'")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_".$tahun." where status = 1 and opsi_prodi_s2 = '".$prodi."'")->row()->jml;
			} else {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_".$tahun." where status > 2 and prodi = '".$prodi."'")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_".$tahun." where status > 2 and opsi_prodi_s2 = '".$prodi."'")->row()->jml;
			}
		} elseif ($tahun > 2017) {
			if ($status == 1) {
				$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb where status = 1 and prodi = '".$prodi."'")->row()->jml;
				$q2 = $q;
			} else {
				$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb where status > 2 and prodi = '".$prodi."'")->row()->jml;
				$q2 = $q;
			}
		} else {
			if ($status == 1) {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba where status = 1 and prodi = '".$prodi."'")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 where status = 1 and opsi_prodi_s2 = '".$prodi."'")->row()->jml;
			} else {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba where status > 2 and prodi = '".$prodi."'")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 where status > 2 and opsi_prodi_s2 = '".$prodi."'")->row()->jml;
			}
		}
		return $q+$q2;
	}

	function getLulusTes($year, $user)
	{
		if ($year < 2017) {

			if ($user == 74101 or $user == 61101) {
				$grade 	= $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_" . $year . " 
										where (status = 1 or status > 2) and opsi_prodi_s2 = '" . $user . "'")->row()->jml;	
			} else {
				$grade 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $year . " 
										where (status = 1 or status > 2) and prodi = '" . $user . "'")->row()->jml;	
			}
			
		} elseif ($year == 2017) {

			if ($user == 74101 or $user == 61101) {
				$grade 	= $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2
										where (status = 1 or status > 2) and opsi_prodi_s2 = '" . $user . "'")->row()->jml;	
			} else {
				$grade 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba
										where (status = 1 or status > 2) and prodi = '" . $user . "'")->row()->jml;	
			}

		} else {
			
			$grade 	= $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb
											where status_lulus = 1 and prodi = '" . $user . "' and SUBSTRING(nomor_registrasi,1,2) = substr('".$year."', 3,4)")->row()->jml;

		}

		return $grade;
		
	}

	function getjumlahmabaskl($tahun,$sch)
	{
		if ($tahun < 2017) {
			$q = $this->db->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba_".$tahun." WHERE jenis_sch_maba = '".$sch."'")->row()->jml;
		} else {
			$q = $this->db->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba WHERE jenis_sch_maba = '".$sch."'")->row()->jml;
		}
		return $q;
	}

	function getjumlahmabasklprodi($tahun,$sch,$prodi="")
	{
		if ($tahun < 2017) {
			$q = $this->db->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba_".$tahun." WHERE jenis_sch_maba = '".$sch."' and prodi = '".$prodi."'")->row()->jml;
		} elseif ($tahun > 2017) {
			$q = $this->dbreg->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_pmb WHERE jenis_sch_maba = '".$sch."' and prodi = '".$prodi."' and SUBSTRING(nomor_registrasi,1,2) = substr('".$tahun."', 3,4)")->row()->jml;
		} else {
			$q = $this->db->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba WHERE jenis_sch_maba = '".$sch."' and prodi = '".$prodi."'")->row()->jml;
		}
		return $q;
	}

	function getjumlahmabapenghasilan($tahun,$id)
	{
		if ($tahun < 2017) {
			$q = $this->db->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba_".$tahun." WHERE penghasilan = '".$id."'")->row()->jml;
		} else {
			$q = $this->db->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba WHERE penghasilan = '".$id."'")->row()->jml;
		}
		return $q;
	}

	function getjumlahmabapenghasilanprodi($tahun,$id,$prodi)
	{
		if ($tahun < 2017) {
			$q = $this->db->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba_".$tahun." WHERE penghasilan = '".$id."' and prodi = '".$prodi."'")->row()->jml;
		} elseif ($tahun > 2017) {
			$q = $this->dbreg->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_pmb WHERE penghasilan = '".$id."' and prodi = '".$prodi."' and SUBSTRING(nomor_registrasi,1,2) = substr('".$tahun."', 3,4)")->row()->jml;
		} else {
			$q = $this->db->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba WHERE penghasilan = '".$id."' and prodi = '".$prodi."'")->row()->jml;
		}
		return $q;
	}

	function getStatusMhsCut($kode)
	{
		return $this->db->query("SELECT tahunajaran,STATUS,COUNT(npm) AS mhs FROM tbl_status_mahasiswa where status = 'C' and tahunajaran <= '".$kode."' group by tahunajaran")->result();
	}

	function getStatusMhsOff($kode)
	{
		return $this->db->query("SELECT tahunajaran,STATUS,COUNT(npm) AS mhs FROM tbl_status_mahasiswa where status = 'N' and tahunajaran <= '".$kode."' group by tahunajaran")->result();
	}

	function getStatusMhsOff2($prodi,$tahun)
	{
		if ($prodi == '0') {
			if ($tahun <= 20152) {
				return $this->db->query("SELECT count(a.npm) as juml,a.status,a.tahunajaran,b.STMHSMSMHS FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa b ON a.npm = b.NIMHSMSMHS
										 WHERE a.status = 'N' AND (BTSTUMSMHS >= '20151' AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K') 
										 where tahunajaran = '".$tahun."' ORDER BY tahunajaran DESC LIMIT 2")->row()->juml;
			} else {
				return $this->db->query("SELECT DISTINCT KDPSTMSMHS, COUNT(NIMHSMSMHS) AS juml FROM tbl_mahasiswa WHERE NIMHSMSMHS NOT IN (
										SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = '".$tahun."'
										) AND NIMHSMSMHS NOT IN (
										SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = '".$tahun."' AND validate = 1 AND tahunajaran = '".$tahun."'
										) AND BTSTUMSMHS >= '".$tahun."' AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K' AND TAHUNMSMHS < 2017'")->row()->juml;
			}
		} else {
			if ($tahun <= 20152) {
				return $this->db->query("SELECT count(a.npm) as juml,a.status,a.tahunajaran,b.STMHSMSMHS FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa b ON a.npm = b.NIMHSMSMHS
								 		 WHERE b.KDPSTMSMHS = '".$prodi."' and a.status = 'N' AND (BTSTUMSMHS >= '20151' AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K') 
									 	 where tahunajaran = '".$tahun."' ORDER BY tahunajaran DESC LIMIT 2")->row()->juml;
			} else {
				return $this->db->query("SELECT DISTINCT KDPSTMSMHS, COUNT(NIMHSMSMHS) as juml FROM tbl_mahasiswa WHERE NIMHSMSMHS NOT IN (
									    SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = '".$tahun."'
										) AND NIMHSMSMHS NOT IN (
									    SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = '".$tahun."' AND validate = 1 AND tahunajaran = '".$tahun."'
										) AND KDPSTMSMHS = '".$prodi."' AND BTSTUMSMHS >= '".$tahun."' AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K' AND TAHUNMSMHS < 2017")->row()->juml;
			}
		}
	}	

	function getStatusMhsOut($kode)
	{
		return $z = $this->db->query("SELECT tahunajaran,STATUS,COUNT(npm) AS mhs FROM tbl_status_mahasiswa where status = 'K' and tahunajaran <= '".$kode."' group by tahunajaran")->result();
	}

	function getStatusMhsOn($user,$kode)
	{
		if ($user == 0) {
			return $this->db->query("SELECT COUNT(DISTINCT npm_mahasiswa) AS jml,tahunajaran FROM tbl_verifikasi_krs where tahunajaran <= '".$kode."' AND tahunajaran > '20141'
									 GROUP BY tahunajaran order by tahunajaran")->result();
		} else {
			return $this->db->query("SELECT COUNT(DISTINCT npm_mahasiswa) AS jml,tahunajaran FROM tbl_verifikasi_krs where tahunajaran <= '".$kode."' AND tahunajaran > '20141'
									 and kd_jurusan = '".$user."' GROUP BY tahunajaran order by tahunajaran")->result();
		}
	}

	function getStatusMhsGrd()
	{
		return $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) AS tahun,COUNT(DISTINCT NIMHSMSMHS) AS jml FROM tbl_mahasiswa WHERE SUBSTR(YEAR(TGLLSMSMHS),1,4) <= substr('".$kode."', 1,4)
								 GROUP BY YEAR(TGLLSMSMHS) ORDER BY YEAR(TGLLSMSMHS) DESC LIMIT 4")->result();
	}

	function gettahun($kode)
	{
		return $this->db->query("SELECT * from tbl_tahunakademik where kode <= '".$kode."'")->result();
	}

	function gettahun2($kode)
	{
		return $this->db->query("SELECT * from tbl_tahunakademik where kode = '".$kode."'")->row();
	}

	function load_jml_sts()
	{

	}

	//Get Status Mahasiswa load js page
	function getstatusaktif1($prodi,$ta)
	{
		if ($ta >= 20151) {
			$qq = $this->db->query('SELECT COUNT(DISTINCT kd_krs) AS jml, SUBSTR(kd_krs,13,5) as tahunajaran 
									FROM tbl_krs a 
									JOIN tbl_mahasiswa d ON a.`npm_mahasiswa` = d.`NIMHSMSMHS`
									WHERE (npm_mahasiswa IS NOT NULL OR npm_mahasiswa != "" OR npm_mahasiswa != 0)
									AND a.`kd_krs` LIKE CONCAT(npm_mahasiswa,"'.$ta.'%")
									AND d.KDPSTMSMHS = "'.$prodi.'"')->row()->jml;
		}
		return $qq;
	}

	function getstatuscuti1($prodi,$ta)
	{
		if ($ta >= 20151) {
			$qq = $this->db->query('SELECT COUNT(DISTINCT npm) AS jum FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa b ON a.`npm`=b.`NIMHSMSMHS`
                                                            WHERE a.`status`="C" AND a.`tahunajaran` = "'.$ta.'" and a.validate >= 1 AND b.`KDPSTMSMHS` = "'.$prodi.'"
                                                            ORDER BY b.`KDPSTMSMHS` ASC')->row()->jum;
		}
		return $qq;
	}

	function getstatusnonaktif1($prodi,$ta)
	{
		$now  = date('Y');
		$last = date('Y')-7;
		if ($ta < 20151) {
			$zz = $this->db->query("SELECT count(a.npm) as jum,a.status,a.tahunajaran,b.STMHSMSMHS FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa b ON a.npm = b.NIMHSMSMHS
									WHERE a.status = 'N' AND (BTSTUMSMHS >= '20151' AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K') 
									AND tahunajaran = '".$ta."' AND KDPSTMSMHS = '".$prodi."' ORDER BY tahunajaran DESC LIMIT 2")->row()->jum;
		} else {
			$zz = $this->db->query("SELECT DISTINCT KDPSTMSMHS, COUNT(NIMHSMSMHS) as jum FROM tbl_mahasiswa WHERE NIMHSMSMHS NOT IN (
									    SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = ".$ta."
									) AND NIMHSMSMHS NOT IN (
									    SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = ".$ta." AND validate = 1 
									) 
									AND KDPSTMSMHS = '".$prodi."' AND BTSTUMSMHS >= ".$ta." 
									AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' 
									AND STMHSMSMHS != 'K' AND STMHSMSMHS != 'C' 
									AND STMHSMSMHS != 'CA' 
									AND TAHUNMSMHS < '".$now."' ")->row()->jum;
		}
		return $zz;
	}

	//Get Jml lulusan
	function getlulusan1($id)
	{
		$q = $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) AS tahun,COUNT(DISTINCT NIMHSMSMHS) as jml FROM tbl_mahasiswa
						where KDPSTMSMHS = '".$id."'
						GROUP BY YEAR(TGLLSMSMHS)
						ORDER BY YEAR(TGLLSMSMHS) DESC
						LIMIT 5")->result();
		return $q;
	}

	function getmasalulusan1($id)
	{
		if (($id == '61101') or ($id == '74101')) {
				$q = $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) as thn,AVG(YEAR(TGLLSMSMHS)-SUBSTR(SMAWLMSMHS,1,4)) AS rata FROM tbl_mahasiswa 
									WHERE STMHSMSMHS = 'L' AND KDPSTMSMHS = '".$id."' AND SUBSTR(NIMHSMSMHS,8,1) = 5 GROUP BY YEAR(TGLLSMSMHS) ORDER BY YEAR(TGLLSMSMHS) desc limit 5")->result();
		} else {
				$q = $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) as thn,AVG(YEAR(TGLLSMSMHS)-SUBSTR(SMAWLMSMHS,1,4)) AS rata FROM tbl_mahasiswa 
									WHERE STMHSMSMHS = 'L' AND KDPSTMSMHS = '".$id."' AND SUBSTR(NIMHSMSMHS,9,1) = 5 GROUP BY YEAR(TGLLSMSMHS) ORDER BY YEAR(TGLLSMSMHS) desc limit 5")->result();
		
		}
		return $q;
	}

	function gotipklulus($tahun,$user,$ta)
	{
		return $this->db->query("SELECT AVG(a.`NLIPKTRAKM`) AS ipk FROM tbl_aktifitas_kuliah_mahasiswa a JOIN tbl_mahasiswa b ON a.`NIMHSTRAKM` = b.`NIMHSMSMHS` 
									 WHERE b.`STMHSMSMHS` = 'L' AND YEAR(b.`TGLLSMSMHS`) = '".$tahun."' AND a.`THSMSTRAKM` = '".$ta."'
									 and b.KDPSTMSMHS = '".$user."'")->row()->ipk;
			
	}

	function gotipkon($ta,$user)
	{
		return $this->db->query("SELECT AVG(a.`NLIPKTRAKM`) AS ipk FROM tbl_aktifitas_kuliah_mahasiswa a JOIN tbl_mahasiswa b ON a.`NIMHSTRAKM` = b.`NIMHSMSMHS`
								 WHERE a.`THSMSTRAKM` = '".$ta."' and b.KDPSTMSMHS = '".$user."'")->row()->ipk;
	}

	


}

/* End of file Info_model.php */
/* Location: ./application/models/Info_model.php */