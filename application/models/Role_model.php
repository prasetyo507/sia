<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_model extends CI_Model {

	function cekakses($menu)
	{
		$sess = $this->session->userdata('sess_login');
		$user_group = $sess['id_user_group'];
		$this->db->distinct();
		$this->db->select('menu_id');
		$this->db->from('view_role');
		$this->db->where('menu_id', $menu);
		$pecah = explode(',', $user_group);
		$jmlh = count($pecah);
		$grup = array();
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		//$this->db->where('a.parent_menu', $id);
		$this->db->where_in('user_group_id',$grup);

		return $this->db->get();
	}

	function getparentmenu()
	{
		$sess = $this->session->userdata('sess_login'); 
		$user_group = $sess['id_user_group'];
		$this->db->distinct('a.id_menu');
		$this->db->select('a.id_menu,a.menu,a.url,a.icon');
		$this->db->from('tbl_menu a');
		$this->db->join('tbl_role_access b', 'a.id_menu = b.menu_id');
		$pecah = explode(',', $user_group);
		$jmlh = count($pecah);
		$grup = array();
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		$this->db->where('a.parent_menu', 0);
		$this->db->where_in('user_group_id',$grup);
		$this->db->order_by('a.menu', 'asc');

		$q = $this->db->get();
		return $q;
	}

	function getmenu($id)
	{
		$sess = $this->session->userdata('sess_login'); 
		$user_group = $sess['id_user_group'];
		$this->db->distinct();
		$this->db->select('a.id_menu,a.menu,a.url,a.icon');
		$this->db->from('tbl_menu a');
		$this->db->join('tbl_role_access b', 'a.id_menu = b.menu_id');
		$pecah = explode(',', $user_group);
		$jmlh = count($pecah);
		$grup = array();
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		$this->db->where('a.parent_menu', $id);
		$this->db->where_in('user_group_id',$grup);
		$this->db->order_by('a.menu', 'asc');
		$q = $this->db->get();
		return $q;	
	}

	function getallmenu()
	{
		$sess = $this->session->userdata('sess_login'); 
		$user_group = $sess['id_user_group'];
		$this->db->distinct('a.id_menu');
		$this->db->select('a.id_menu,a.menu,a.url,a.icon');
		$this->db->from('tbl_menu a');
		$this->db->join('tbl_role_access b', 'a.id_menu = b.menu_id');
		$pecah = explode(',', $user_group);
		$jmlh = count($pecah);
		$grup = array();
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		$this->db->where('a.url !=', '-');
		$this->db->where('a.url !=', '#');
		$this->db->where_in('user_group_id',$grup);
		$this->db->order_by('a.menu', 'asc');

		$q = $this->db->get();
		return $q;
	}

	function cek_abs($id)
	{
		$this->db->select('*');
		$this->db->from('tbl_jadwal_matkul');
		$this->db->where('kd_jadwal', $id);
		$q = $this->db->get();
		return $q;
	}
}

/* End of file role_model.php */
/* Location: ./application/models/role_model.php */