<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Krs_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function getactvkrs($kd)
	{
		$this->db->like('kd_krs', $kd, 'after');
		$get = $this->db->get('tbl_verifikasi_krs', 1);
		return $get;
	}

	function statVerify($kd, $stat)
	{
		$this->db->like('kd_krs', $kd, 'after');
		$this->db->where('status_verifikasi', $stat);
		$get = $this->db->get('tbl_verifikasi_krs', 1);
		return $get;
	}

	function krsPayment($uid, $year, $stat)
	{
		$this->db->where('npm_mahasiswa', $uid);
		$this->db->where('tahunajaran', $year);
		$this->db->where('status >=', $stat);
		$get = $this->db->get('tbl_sinkronisasi_renkeu', 1);
		return $get;
	}

	function getPa($uid)
	{
		$this->db->where('npm_mahasiswa', $uid);
		$get = $this->db->get('tbl_pa', 1);
		return $get;
	}

	function countips($prodi, $uid, $year)
	{
		$this->db->select('a.NIMHSTRLNM,a.KDKMKTRLNM,a.NLAKHTRLNM,a.BOBOTTRLNM,b.sks_matakuliah');
		$this->db->from('tbl_transaksi_nilai a');
		$this->db->join('tbl_matakuliah b', 'a.KDKMKTRLNM = b.kd_matakuliah', 'left');
		$this->db->where('a.kd_transaksi_nilai is NOT NULL', NULL, false);
		$this->db->where('b.kd_prodi', $prodi);
		$this->db->where('a.NIMHSTRLNM', $uid);
		$this->db->where('a.THSMSTRLNM', $year);
		$this->db->where('a.NLAKHTRLNM <>', 'T');
		$get = $this->db->get()->result();
		
		$st=0;
        $ht=0;
        foreach ($get as $iso) {
            $h = 0;

            $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
            $ht=  $ht + $h;

            $st=$st+$iso->sks_matakuliah;
        }

        $ips_nr = $ht/$st;

        return $ips_nr;
	}
	function countipk($prodi, $uid)
	{
		$this->db->select('a.NIMHSTRLNM,a.KDKMKTRLNM,a.NLAKHTRLNM,a.BOBOTTRLNM,b.sks_matakuliah');
		$this->db->from('tbl_transaksi_nilai a');
		$this->db->join('tbl_matakuliah b', 'a.KDKMKTRLNM = b.kd_matakuliah', 'left');
		$this->db->where('a.kd_transaksi_nilai is NOT NULL', NULL, false);
		$this->db->where('b.kd_prodi', $prodi);
		$this->db->where('a.NIMHSTRLNM', $uid);
		$this->db->where('a.NLAKHTRLNM <>', 'T');
		$get = $this->db->get()->result();

		$st=0;
        $ht=0;
        foreach ($get as $iso) {
            $h = 0;

            $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
            $ht=  $ht + $h;

            $st=$st+$iso->sks_matakuliah;
        }

        $ips_nr = $ht/$st;

        return $ips_nr;
	}

	function detailkhs($uid, $smt, $prd)
	{
		$this->db->select('a.*, b.nama_matakuliah, b.sks_matakuliah');
		$this->db->from('tbl_krs a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');
		$this->db->where('a.npm_mahasiswa', $uid);
		$this->db->where('semester_krs', $smt);
		$this->db->where('b.kd_prodi', $prd);
		$get = $this->db->get();
		return $get;
	}

	function emaildosen($kdkrs)
	{
		$this->db->where('kd_krs', $kdkrs);
		$get = $this->db->get('tbl_verifikasi_krs', 1)->row();

		$this->db->where('nid', $get->id_pembimbing);
		$parse = $this->db->get('tbl_biodata_dosen', 1);

		return $parse;
	}
}

/* End of file Krs_model.php */
/* Location: ./application/models/Krs_model.php */