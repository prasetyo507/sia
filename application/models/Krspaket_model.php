<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Krspaket_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function getKurikulum($prodi)
	{
		$this->db->select('kd_kurikulum');
		$this->db->from('tbl_kurikulum_new');
		$this->db->where('kd_prodi', $prodi);
		$this->db->order_by('id_kurikulum', 'desc');
		$this->db->limit(1);
		return $this->db->get();
	}

	function getKdMk($kdkur)
	{
		$this->db->select('kd_matakuliah');
		$this->db->from('tbl_kurikulum_matkul_new');
		$this->db->where('kd_kurikulum', $kdkur);
		$this->db->where('semester_kd_matakuliah', 1);
		return $this->db->get();
	}

	function getJadwal($uid, $year, $kdmk, $cls)
	{
		$this->db->select('kd_jadwal, kd_ruangan');
		$this->db->from('tbl_jadwal_matkul');
		$this->db->like('kd_jadwal', $uid, 'after');
		$this->db->where('kd_tahunajaran', $year);
		$this->db->where('kd_matakuliah', $kdmk);
		$this->db->where('kelas', $cls);
		return $this->db->get();
	}

	function amountSks($kdmk, $uid)
	{
		$this->db->select('sks_matakuliah');
		$this->db->from('tbl_matakuliah');
		$this->db->where('kd_matakuliah', $kdmk);
		$this->db->where('kd_prodi', $uid);
		return $this->db->get();
	}

}

/* End of file Krspaket_model.php */
/* Location: ./application/models/Krspaket_model.php */