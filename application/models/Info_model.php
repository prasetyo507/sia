<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info_model extends CI_Model {

	function getjumlahmhs($id) 
	{
		$activeRange = date('Y')-7;
		$activeYear  = date('Y');

		if ($id == 0) {
			$this->db->select('TAHUNMSMHS, count(DISTINCT NIMHSMSMHS) as jml');
			$this->db->from('tbl_mahasiswa');
			$this->db->where('TAHUNMSMHS <=', $activeYear);
			$this->db->where('TAHUNMSMHS >=', $activeRange);
			$this->db->order_by('TAHUNMSMHS', 'asc');
			$this->db->group_by('TAHUNMSMHS');
			$this->db->limit(10);
			$q = $this->db->get()->result();

			/*
			$q 	= $this->db->query("SELECT DISTINCT TAHUNMSMHS,COUNT(DISTINCT NIMHSMSMHS) as jml FROM tbl_mahasiswa
									where TAHUNMSMHS < 2018 and TAHUNMSMHS > 2007
									GROUP BY TAHUNMSMHS
									ORDER BY TAHUNMSMHS ASC
									LIMIT 10")->result();
			*/
		/* } elseif ($id > 0 and $id < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$id,'kd_fakultas','asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh."'".$key->kd_prodi."',"; 
			}
			$akhir = substr($jumlh, 0, -1);
			$q = $this->db->query("SELECT DISTINCT TAHUNMSMHS,COUNT(DISTINCT NIMHSMSMHS) as jml FROM tbl_mahasiswa
								where TAHUNMSMHS < 2018 and TAHUNMSMHS > 2007 and KDPSTMSMHS IN (".$akhir.")
								GROUP BY TAHUNMSMHS
								ORDER BY TAHUNMSMHS ASC
								LIMIT 10")->result();
		*/
		} else {
			$this->db->distinct();
			$this->db->select('TAHUNMSMHS, COUNT(DISTINCT NIMHSMSMHS) as jml');
			$this->db->from('tbl_mahasiswa');
			$this->db->where('TAHUNMSMHS <=', $activeYear);
			$this->db->where('TAHUNMSMHS >=', $activeRange);
			$this->db->where('KDPSTMSMHS', $id);
			$this->db->order_by('TAHUNMSMHS', 'asc');
			$this->db->group_by('TAHUNMSMHS');
			$this->db->limit(10);
			$q = $this->db->get()->result();
			
			/*
			$q = $this->db->query("SELECT DISTINCT TAHUNMSMHS,COUNT(DISTINCT NIMHSMSMHS) as jml FROM tbl_mahasiswa
								where TAHUNMSMHS < 2018 and TAHUNMSMHS > 2007 and KDPSTMSMHS = '" . $id . "'
								GROUP BY TAHUNMSMHS
								ORDER BY TAHUNMSMHS ASC
								LIMIT 10")->result();
			*/
		}

		return $q;
	}

	function getdosen() 
	{
		$q = $this->db->query("SELECT DISTINCT tetap,COUNT(DISTINCT nid) as jml FROM tbl_karyawan a
								JOIN tbl_jurusan_prodi b on a.jabatan_id = b.kd_prodi
								GROUP BY tetap
								ORDER BY tetap DESC")->result();
		return $q;
	}

	function getdosenprodi() 
	{
		$q = $this->db->query("SELECT kd_prodi,b.prodi,COUNT(DISTINCT nid) as jml FROM tbl_karyawan a
								JOIN tbl_jurusan_prodi b ON a.jabatan_id = b.kd_prodi
								WHERE tetap = 1 GROUP BY kd_prodi ORDER BY kd_fakultas,kd_prodi ASC")->result();
		return $q;
	}

	function getdosenprodinidn($id) {
		$q = $this->db->query("SELECT kd_prodi,prodi,COUNT(DISTINCT nid) as jml FROM tbl_karyawan a
								JOIN tbl_jurusan_prodi b ON a.jabatan_id = b.kd_prodi
								WHERE tetap = 1 AND ((a.nidn IS NOT NULL OR a.nidn != '') OR (a.nupn IS NOT NULL OR a.nupn != '')) and kd_prodi = '" . $id . "'")->row()->jml;
		return $q;
	}

	function getlulusan($id) 
	{
		if ($id == 0) {
			$q = $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) AS tahun,COUNT(DISTINCT NIMHSMSMHS) as jml FROM tbl_mahasiswa
									GROUP BY YEAR(TGLLSMSMHS)
									ORDER BY YEAR(TGLLSMSMHS) DESC
									LIMIT 5")->result();
			return $q;
		} elseif ($id > 0 and $id < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$id,'kd_fakultas','asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh."'".$key->kd_prodi."',"; 
			}
			$akhir = substr($jumlh, 0, -1);
			$q = $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) AS tahun,COUNT(DISTINCT NIMHSMSMHS) as jml FROM tbl_mahasiswa
									where KDPSTMSMHS IN (".$akhir.")
									GROUP BY YEAR(TGLLSMSMHS)
									ORDER BY YEAR(TGLLSMSMHS) DESC
									LIMIT 5")->result();
			return $q;
		} else {
			$q = $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) AS tahun,COUNT(DISTINCT NIMHSMSMHS) as jml FROM tbl_mahasiswa
									where KDPSTMSMHS = '" . $id . "'
									GROUP BY YEAR(TGLLSMSMHS)
									ORDER BY YEAR(TGLLSMSMHS) DESC
									LIMIT 5")->result();
			return $q;
		}
	}

	function getjumlahmabadaftar($tahun) {
		if ($tahun < 2017) {
			$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $tahun . "")->row()->jml;
			$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_" . $tahun . "")->row()->jml;
		} elseif ($tahun > 2017) {
			$this->dbreg = $this->load->database('regis', TRUE);
			$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb where SUBSTRING(nomor_registrasi,1,2) = substr('".$tahun."', 3,4)")->row()->jml;
			$q2 = 0;
		} else {
			$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba")->row()->jml;
			$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2")->row()->jml;
		}
		return $q + $q2;
	}

	function getjumlahmabadaftarprodi($tahun, $prodi) {
		if ($prodi < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$prodi,'kd_fakultas','asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh."'".$key->kd_prodi."',"; 
			}
			$akhir = substr($jumlh, 0, -1);
			if ($tahun < 2017) {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $tahun . " 
										where prodi IN (" . $akhir . ")")->row()->jml;

				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_" . $tahun . " 
										where opsi_prodi_s2 IN (" . $akhir . ")")->row()->jml;
			} elseif ($tahun > 2017) {
				$this->dbreg = $this->load->database('regis', TRUE);
				$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb 
											where prodi IN (" . $akhir . ") and nomor_registrasi like '".substr($tahun, 2,4)."%'")->row()->jml;
				$q2 = 0;
			} else {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba 
										where prodi IN (" . $akhir . ")")->row()->jml;

				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 
										where opsi_prodi_s2 IN (" . $akhir . ")")->row()->jml;
			}
		} else {
			if ($tahun < 2017) {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $tahun . " 
										where prodi = '" . $prodi . "'")->row()->jml;

				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_" . $tahun . " 
										where opsi_prodi_s2 = '" . $prodi . "'")->row()->jml;
			} elseif ($tahun > 2017) {
				$this->dbreg = $this->load->database('regis', TRUE);
				$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb 
											where prodi = '" . $prodi . "' and nomor_registrasi like '".substr($tahun, 2,4)."%'")->row()->jml;
				$q2 = 0;
			} else {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba 
										where prodi = '" . $prodi . "'")->row()->jml;

				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 
										where opsi_prodi_s2 = '" . $prodi . "'")->row()->jml;
			}
		}
		return $q + $q2;
	}

	function getLulusTes($year, $user="")
	{
		// jika login bukan sebagai prodi
		if ($user == "") {
			
			if ($year < 2017) {
			
				$grade 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $year . " 
											where status = 1 or status > 2")->row()->jml;
				$reregs	= 0;
			} elseif ($year == 2017) {
				
				$grade 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba 
											where status = 1 or status > 2")->row()->jml;
				$reregs	= 0;
			} else {
				
				$grade 	= $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb 
												where status_lulus = 1 and SUBSTRING(nomor_registrasi,1,2) = substr('".$year."', 3,4)")->row()->jml;
				$reregs	= 0;
			}

		// jika login sebagai prodi
		} else {
			
			if ($year < 2017) {

				$grade 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $year . " 
											where (status = 1 or status > 2) and prodi = '" . $user . "'")->row()->jml;
				$reregs = 0;
				
			} elseif ($year == 2017) {
				
				$grade 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba
											where (status = 1 or status > 2) and prodi = '" . $user . "'")->row()->jml;
				$reregs = 0;

			} else {
				
				$grade 	= $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb
												where status_lulus = 1 and prodi = '" . $user . "' and SUBSTRING(nomor_registrasi,1,2) = substr('".$year."', 3,4)")->row()->jml;
				$reregs = 0;

			}

		}

		return $grade + $reregs;
		
	}

	function getjumlahmabastatus($tahun, $status) {
		if ($tahun < 2017) {
			if ($status == 1) {
				$q 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $tahun . " 
										where status = 1")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_" . $tahun . " 
										where status = 1")->row()->jml;
			} else {
				$q 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $tahun . " 
										where status > 2")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_" . $tahun . " 
										where status > 2")->row()->jml;
			}
		} elseif ($tahun > 2017) {
			$this->dbreg = $this->load->database('regis', TRUE);
			if ($status == 1) {
				$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb where status_lulus = 1 and SUBSTRING(nomor_registrasi,1,2) = substr('".$tahun."', 3,4)")->row()->jml;
				$q2 = 0;
			} else {
				$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb where status > 2 and SUBSTRING(nomor_registrasi,1,2) = substr('".$tahun."', 3,4)")->row()->jml;
				$q2 = 0;
			}
		} else {
			if ($status == 1) {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba where status = 1")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 where status = 1")->row()->jml;
			} else {
				$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba where status > 2")->row()->jml;
				$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 where status > 2")->row()->jml;
			}
		}
		return $q + $q2;
	}

	function getjumlahmabastatusprodi($tahun, $status, $prodi) 
	{
		if ($prodi < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$prodi,'kd_fakultas','asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh."'".$key->kd_prodi."',"; 
			}
			$akhir = substr($jumlh, 0, -1);
			if ($tahun < 2017) {
				if ($status == 1) {
					$q 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $tahun . "
											where status = 1 and prodi IN (" . $akhir . ")")->row()->jml;
					$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_" . $tahun . " 
											where status = 1 and opsi_prodi_s2 IN (" . $akhir . ")")->row()->jml;

				} else {
					$q 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $tahun . " 
											where status > 2 and prodi IN (" . $akhir . ")")->row()->jml;
					$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_" . $tahun . " 
											where status > 2 and opsi_prodi_s2 IN (" . $akhir . ")")->row()->jml;
				}

			} elseif ($tahun > 2017) {
				$this->dbreg = $this->load->database('regis', TRUE);
				if ($status == 1) {
					$q 	= $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb 
												where status_lulus = 1 and prodi IN (" . $akhir . ")")->row()->jml;
					$q2 = 0;

				} else {
					$q 	= $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb 
												where status > 2 and prodi IN (" . $akhir . ")")->row()->jml;
					$q2 = 0;
				}

			} else {
				if ($status == 1) {
					$q 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba 
											where status = 1 and prodi IN (" . $akhir . ")")->row()->jml;
					$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 
											where status = 1 and opsi_prodi_s2 IN (" . $akhir . ")")->row()->jml;

				} else {
					$q 	= $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba 
											where status > 2 and prodi IN (" . $akhir . ")")->row()->jml;
					$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 
											where status > 2 and opsi_prodi_s2 IN (" . $akhir . ")")->row()->jml;
				}
			}
		} else {
			if ($tahun < 2017) {
				if ($status == 1) {
					$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $tahun . " where status = 1 and prodi = '" . $prodi . "'")->row()->jml;
					$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_" . $tahun . " where status = 1 and opsi_prodi_s2 = '" . $prodi . "'")->row()->jml;
				} else {
					$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $tahun . " where status > 2 and prodi = '" . $prodi . "'")->row()->jml;
					$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2_" . $tahun . " where status > 2 and opsi_prodi_s2 = '" . $prodi . "'")->row()->jml;
				}
			} elseif ($tahun > 2017) {
				$this->dbreg = $this->load->database('regis', TRUE);
				if ($status == 1) {
					$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb where status_lulus = 1 and prodi = '" . $prodi . "' and nomor_registrasi like '".substr($tahun, 2,4)."%'")->row()->jml;
					$q2 = 0;
				} else {
					$q = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb where status > 2 and prodi = '" . $prodi . "' and nomor_registrasi like '".substr($tahun, 2,4)."%'")->row()->jml;
					$q2 = 0;
				}
			} else {
				if ($status == 1) {
					$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba where status = 1 and prodi = '" . $prodi . "'")->row()->jml;
					$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 where status = 1 and opsi_prodi_s2 = '" . $prodi . "'")->row()->jml;
				} else {
					$q = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba where status > 2 and prodi = '" . $prodi . "'")->row()->jml;
					$q2 = $this->db->query("SELECT count(DISTINCT ID_registrasi) as jml from tbl_pmb_s2 where status > 2 and opsi_prodi_s2 = '" . $prodi . "'")->row()->jml;
				}
			}
		}
		return $q + $q2;
	}

	function pesertates($year, $prodi='')
	{
		// jika login bukan sebagai prodi
		if ($prodi == '') {
			
			if ($year < 2017) {
				
				$lulus = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $year . " 
											where status IS NOT NULL")->row()->jml;
				$tidak = 0;

			} elseif ($year == 2017) {
				
				$lulus = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba 
											where status IS NOT NULL")->row()->jml;
				$tidak = 0;

			} else {
				
				$lulus = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb 
												where status_lulus = 1 and SUBSTRING(nomor_registrasi,1,2) = substr('".$year."', 3,4)")->row()->jml;
				$tidak = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb 
												where status_lulus = 2 and SUBSTRING(nomor_registrasi,1,2) = substr('".$year."', 3,4) ")->row()->jml;

			}
			

		// jika login sebagai prodi
		} else {

			if ($year < 2017) {

				$lulus = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba_" . $year . " 
											where status IS NOT NULL and prodi = '" . $prodi . "'")->row()->jml;
				$tidak = 0;
				
			} elseif ($year == 2017) {
				
				$lulus = $this->db->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_camaba
											where status IS NOT NULL and prodi = '" . $prodi . "'")->row()->jml;
				$tidak = 0;

			} else {
				
				$lulus = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb
												where status_lulus = 1 and prodi = '" . $prodi . "' and nomor_registrasi like '".substr($year, 2,4)."%' ")->row()->jml;
				$tidak = $this->dbreg->query("SELECT count(DISTINCT nomor_registrasi) as jml from tbl_form_pmb
												where status_lulus = 2 and prodi = '" . $prodi . "' and nomor_registrasi like '".substr($year, 2,4)."%' ")->row()->jml;

			}

		}

		return $lulus + $tidak;
		
	}

	function getjumlahmabaskl($tahun, $sch) {
		if ($tahun < 2017) {
			$q = $this->db->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba_" . $tahun . " WHERE jenis_sch_maba = '" . $sch . "'")->row()->jml;
		} elseif ($tahun > 2017) {
			$this->dbreg = $this->load->database('regis', TRUE);
			$q = $this->dbreg->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_pmb WHERE jenis_sch_maba = '" . $sch . "' and SUBSTRING(nomor_registrasi,1,2) = substr('".$tahun."', 3,4)")->row()->jml;
		} else {
			$q = $this->db->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba WHERE jenis_sch_maba = '" . $sch . "'")->row()->jml;
		}
		return $q;
	}

	function getjumlahmabasklprodi($tahun, $sch, $prodi) {
		if ($prodi < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$prodi,'kd_fakultas','asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh."'".$key->kd_prodi."',"; 
			}
			$akhir = substr($jumlh, 0, -1);
			if ($tahun < 2017) {
				$q = $this->db->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba_" . $tahun . " WHERE jenis_sch_maba = '" . $sch . "' and prodi IN (" . $akhir . ")")->row()->jml;
			} elseif ($tahun > 2017) {
				$this->dbreg = $this->load->database('regis', TRUE);
				$q = $this->dbreg->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_pmb WHERE jenis_sch_maba = '" . $sch . "' and prodi IN (" . $akhir . ") and status > 2 and nomor_registrasi like '".substr($tahun, 2,4)."%'")->row()->jml;
			} else {
				$q = $this->db->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba WHERE jenis_sch_maba = '" . $sch . "' and prodi IN (" . $akhir . ")")->row()->jml;
			}
		} else {
			if ($tahun < 2017) {
				$q = $this->db->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba_" . $tahun . " WHERE jenis_sch_maba = '" . $sch . "' and prodi = '" . $prodi . "'")->row()->jml;
			} elseif ($tahun > 2017) {
				$this->dbreg = $this->load->database('regis', TRUE);
				$q = $this->dbreg->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_pmb WHERE jenis_sch_maba = '" . $sch . "' and prodi = '" . $prodi . "' and status > 2 and nomor_registrasi like '".substr($tahun, 2,4)."%'")->row()->jml;
			} else {
				$q = $this->db->query("SELECT DISTINCT jenis_sch_maba,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba WHERE jenis_sch_maba = '" . $sch . "' and prodi = '" . $prodi . "'")->row()->jml;
			}
		}
		return $q;
	}

	function getjumlahmabapenghasilan($tahun, $id) {
		if ($tahun < 2017) {
			$q = $this->db->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba_" . $tahun . " WHERE penghasilan = '" . $id . "'")->row()->jml;
		} elseif ($tahun > 2017) {
			$this->dbreg = $this->load->database('regis', TRUE);
			$q = $this->dbreg->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_pmb WHERE penghasilan = '" . $id . "' and SUBSTRING(nomor_registrasi,1,2) = substr('".$tahun."', 3,4)")->row()->jml;
		} else {
			$q = $this->db->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba WHERE penghasilan = '" . $id . "'")->row()->jml;
		}
		return $q;
	}

	function getjumlahmabapenghasilanprodi($tahun, $id, $prodi) {
		if ($prodi < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$prodi,'kd_fakultas','asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh."'".$key->kd_prodi."',"; 
			}
			$akhir = substr($jumlh, 0, -1);
			if ($tahun < 2017) {
				$q = $this->db->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba_" . $tahun . " WHERE penghasilan = '" . $id . "' and prodi IN (" . $akhir . ")")->row()->jml;
			} elseif ($tahun > 2017) {
				$this->dbreg = $this->load->database('regis', TRUE);
				$q = $this->dbreg->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_pmb WHERE penghasilan = '" . $id . "' and prodi IN (" . $akhir . ") and status > 2 and nomor_registrasi like '".substr($tahun, 2,4)."%'")->row()->jml;
			} else {
				$q = $this->db->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba WHERE penghasilan = '" . $id . "' and prodi IN (" . $akhir . ")")->row()->jml;
			}
		} else {
			if ($tahun < 2017) {
				$q = $this->db->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba_" . $tahun . " WHERE penghasilan = '" . $id . "' and prodi = '" . $prodi . "'")->row()->jml;
			} elseif ($tahun > 2017) {
				$this->dbreg = $this->load->database('regis', TRUE);
				$q = $this->dbreg->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_pmb WHERE penghasilan = '" . $id . "' and prodi = '" . $prodi . "' and status > 2 and nomor_registrasi like '".substr($tahun, 2,4)."%'")->row()->jml;
			} else {
				$q = $this->db->query("SELECT DISTINCT penghasilan,COUNT(DISTINCT nomor_registrasi) as jml FROM tbl_form_camaba WHERE penghasilan = '" . $id . "' and prodi = '" . $prodi . "'")->row()->jml;
			}
		}
		return $q;
	}

	function getmasalulusan($id) 
	{
		if ($id == 0) {
			$q 	= $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) as thn,AVG(YEAR(TGLLSMSMHS)-SUBSTR(SMAWLMSMHS,1,4)) AS rata 
									FROM tbl_mahasiswa
									WHERE STMHSMSMHS = 'L' 
									AND SUBSTR(NIMHSMSMHS,9,1) = 5 
									AND TGLLSMSMHS != '".date('Y')."' 
									GROUP BY YEAR(TGLLSMSMHS) 
									ORDER BY YEAR(TGLLSMSMHS) desc limit 5")->result();

		} elseif ($id > 0 and $id < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$id,'kd_fakultas','asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh."'".$key->kd_prodi."',"; 
			}
			$akhir = substr($jumlh, 0, -1);
			if (($id == '6')) {
				$q 	= $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) as thn,AVG(YEAR(TGLLSMSMHS)-SUBSTR(SMAWLMSMHS,1,4)) AS rata 
										FROM tbl_mahasiswa
										WHERE STMHSMSMHS = 'L' 
										AND KDPSTMSMHS IN (".$akhir.") 
										AND SUBSTR(NIMHSMSMHS,8,1) = 5 
										GROUP BY YEAR(TGLLSMSMHS) 
										ORDER BY YEAR(TGLLSMSMHS) desc limit 5")->result();

			} else {
				$q 	= $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) AS thn,AVG(YEAR(TGLLSMSMHS)-SUBSTR(SMAWLMSMHS,1,4)) AS rata 
										FROM tbl_mahasiswa
										WHERE STMHSMSMHS = 'L' 
										AND KDPSTMSMHS IN (".$akhir.") 
										AND SUBSTR(NIMHSMSMHS,9,1) = 5 
										GROUP BY YEAR(TGLLSMSMHS) 
										ORDER BY YEAR(TGLLSMSMHS) DESC LIMIT 5")->result();
			}
		} else {
			if (($id == '61101') or ($id == '74101')) {
				$q 	= $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) as thn,AVG(YEAR(TGLLSMSMHS)-SUBSTR(SMAWLMSMHS,1,4)) AS rata 
										FROM tbl_mahasiswa
										WHERE STMHSMSMHS = 'L' 
										AND KDPSTMSMHS = '" . $id . "' 
										AND SUBSTR(NIMHSMSMHS,8,1) = 5 
										GROUP BY YEAR(TGLLSMSMHS) 
										ORDER BY YEAR(TGLLSMSMHS) desc limit 5")->result();

			} else {
				$q 	= $this->db->query("SELECT DISTINCT YEAR(TGLLSMSMHS) AS thn,AVG(YEAR(TGLLSMSMHS)-SUBSTR(SMAWLMSMHS,1,4)) AS rata 
										FROM tbl_mahasiswa
										WHERE STMHSMSMHS = 'L' 
										AND KDPSTMSMHS = '" . $id . "' 
										AND SUBSTR(NIMHSMSMHS,9,1) = 5 
										GROUP BY YEAR(TGLLSMSMHS) 
										ORDER BY YEAR(TGLLSMSMHS) DESC LIMIT 5")->result();
			}
		}
		return $q;
	}

	function getstatusaktif($prodi, $ta)
	{
		if ($prodi == 0) {
			if ($ta >= 20151) {
				$qq = $this->db->query('SELECT COUNT(DISTINCT kd_krs) AS jml, SUBSTR(kd_krs,13,5) as tahunajaran 
										FROM tbl_krs
										WHERE (npm_mahasiswa IS NOT NULL OR npm_mahasiswa != "" OR npm_mahasiswa != 0)
										AND kd_krs LIKE CONCAT(npm_mahasiswa,"' . $ta . '%") ')->row()->jml;
			}

		} elseif ($prodi > 0 and $prodi < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$prodi,'kd_fakultas','asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh."'".$key->kd_prodi."',"; 
			}
			$akhir = substr($jumlh, 0, -1);
			if ($ta >= 20151)  {
				$qq = $this->db->query('SELECT COUNT(DISTINCT kd_krs) AS jml, b.prodi, b.kd_prodi, c.fakultas, 
										SUBSTR(kd_krs,13,5) as tahunajaran FROM tbl_krs a
										JOIN tbl_mahasiswa d ON a.`npm_mahasiswa` = d.`NIMHSMSMHS`
										JOIN tbl_jurusan_prodi b ON d.`KDPSTMSMHS`=b.kd_prodi
										JOIN tbl_fakultas c ON b.kd_fakultas=c.kd_fakultas 
										WHERE (npm_mahasiswa IS NOT NULL OR npm_mahasiswa != "" OR npm_mahasiswa != 0)
										AND a.`kd_krs` LIKE CONCAT(npm_mahasiswa,"' . $ta . '%") 
										AND b.kd_prodi IN ('.$akhir.')')->row()->jml;
			}

		} else {
			if ($ta >= 20151)  {
				$qq = $this->db->query('SELECT COUNT(DISTINCT kd_krs) AS jml, SUBSTR(kd_krs,13,5) as tahunajaran FROM tbl_krs a
										JOIN tbl_mahasiswa d ON a.`npm_mahasiswa` = d.`NIMHSMSMHS`
										WHERE (npm_mahasiswa IS NOT NULL OR npm_mahasiswa != "" OR npm_mahasiswa != 0)
										AND a.`kd_krs` LIKE CONCAT(npm_mahasiswa,"' . $ta . '%") 
										AND d.KDPSTMSMHS = "' . $prodi . '"')->row()->jml;
			}
		}

		return $qq;
	}

	function getstatuscuti($prodi, $ta) {
		if ($prodi == 0) {
			if ($ta >= 20151) {
				$qq = $this->db->query('SELECT COUNT(DISTINCT npm) AS jum FROM tbl_status_mahasiswa a 
										JOIN tbl_mahasiswa b ON a.`npm`=b.`NIMHSMSMHS`
                                        WHERE a.`status`="C" AND a.`tahunajaran` = "' . $ta . '" and a.validate >= 1
                                        ORDER BY b.`KDPSTMSMHS` ASC')->row()->jum;
			}
		} elseif ($prodi > 0 and $prodi < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$prodi,'kd_fakultas','asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh."'".$key->kd_prodi."',"; 
			}
			$akhir = substr($jumlh, 0, -1);
			if ($ta >= 20151) {
				$qq = $this->db->query('SELECT COUNT(DISTINCT npm) AS jum FROM tbl_status_mahasiswa a 
										JOIN tbl_mahasiswa b ON a.`npm`=b.`NIMHSMSMHS`
                                        WHERE a.`status`="C" AND a.`tahunajaran` = "' . $ta . '" and a.validate >= 1 
                                        AND b.`KDPSTMSMHS` IN ('.$akhir.')
                                        ORDER BY b.`KDPSTMSMHS` ASC')->row()->jum;
			}
		} else {
			if ($ta >= 20151) {
				$qq = $this->db->query('SELECT COUNT(DISTINCT npm) AS jum FROM tbl_status_mahasiswa a 
										JOIN tbl_mahasiswa b ON a.`npm`=b.`NIMHSMSMHS`
                                        WHERE a.`status`="C" AND a.`tahunajaran` = "' . $ta . '" and a.validate >= 1 
                                        AND b.`KDPSTMSMHS` = "' . $prodi . '"
                                        ORDER BY b.`KDPSTMSMHS` ASC')->row()->jum;
			}
		}
		return $qq;
	}

	function getstatusnonaktif($prodi, $ta) 
	{
		// AS BAA
		if ($prodi == 0) {
			if ($ta < 20151) {
				$zz = $this->db->query("SELECT count(a.npm) as jum,a.status,a.tahunajaran,b.STMHSMSMHS FROM tbl_status_mahasiswa a 
										JOIN tbl_mahasiswa b ON a.npm = b.NIMHSMSMHS
										WHERE a.status = 'N' 
										AND (BTSTUMSMHS >= '20151' AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K')
										AND tahunajaran = '" . $ta . "' ORDER BY tahunajaran DESC LIMIT 2")->row()->jum;
			} else {
				$zz = $this->db->query("SELECT DISTINCT KDPSTMSMHS, COUNT(NIMHSMSMHS) as jum FROM tbl_mahasiswa 
										WHERE NIMHSMSMHS NOT IN (
										    SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = ".$ta."
										) AND NIMHSMSMHS NOT IN (
										    SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = ".$ta." AND validate = 1
										    and status != 'N'
										) 
										AND BTSTUMSMHS >= ".$ta." AND STMHSMSMHS != 'L' 
										AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K' 
										AND STMHSMSMHS != 'C' AND STMHSMSMHS != 'CA' 
										AND TAHUNMSMHS < '".date('Y')."' AND TAHUNMSMHS >= '".(date('Y')-7)."'")->row()->jum;
			}

		// AS FACULTY
		} elseif ($prodi > 0 and $prodi < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$prodi,'kd_fakultas','asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh."'".$key->kd_prodi."',"; 
			}
			$akhir = substr($jumlh, 0, -1);
			if ($ta < 20151) {
				$zz = $this->db->query("SELECT count(a.npm) as jum,a.status,a.tahunajaran,b.STMHSMSMHS FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa b ON a.npm = b.NIMHSMSMHS
										WHERE a.status = 'N' AND (BTSTUMSMHS >= '20151' AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K')
										AND tahunajaran = '" . $ta . "' AND KDPSTMSMHS IN (".$akhir.") ORDER BY tahunajaran DESC LIMIT 2")->row()->jum;
			} else {
				$zz = $this->db->query("SELECT DISTINCT KDPSTMSMHS, COUNT(NIMHSMSMHS) as jum FROM tbl_mahasiswa WHERE NIMHSMSMHS NOT IN (
										    SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = " . $ta . "
										) AND NIMHSMSMHS NOT IN (
										    SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = " . $ta . " AND validate = 1 AND tahunajaran = " . $ta . "
										) AND KDPSTMSMHS IN (".$akhir.") AND BTSTUMSMHS >= " . $ta . " AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K' AND TAHUNMSMHS >= '".(date('Y')-7)."' AND TAHUNMSMHS < '".date('Y')."'")->row()->jum;
			}

		// AS PRODI
		} else {
			if ($ta < 20151) {
				$zz = $this->db->query("SELECT count(a.npm) as jum,a.status,a.tahunajaran,b.STMHSMSMHS FROM tbl_status_mahasiswa a 
										JOIN tbl_mahasiswa b ON a.npm = b.NIMHSMSMHS
										WHERE a.status = 'N' 
										AND (BTSTUMSMHS >= '20151' AND STMHSMSMHS != 'L' AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K')
										AND tahunajaran = '" . $ta . "' AND KDPSTMSMHS = '" . $prodi . "' 
										ORDER BY tahunajaran DESC LIMIT 2")->row()->jum;
			} else {
				$zz = $this->db->query("SELECT DISTINCT KDPSTMSMHS, COUNT(NIMHSMSMHS) as jum FROM tbl_mahasiswa 
										WHERE NIMHSMSMHS NOT IN (
										    SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = " . $ta . "
										) AND NIMHSMSMHS NOT IN (
										    SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = " . $ta . " AND validate = 1
										    and status != 'N'
										) 
										AND KDPSTMSMHS = " . $prodi . " 
										AND BTSTUMSMHS >= " . $ta . " AND STMHSMSMHS != 'L' 
										AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K'
										AND STMHSMSMHS != 'C' AND STMHSMSMHS != 'CA' 
										AND TAHUNMSMHS < '".date('Y')."' ")->row()->jum;
			}
		}
		return $zz;
	}

}

/* End of file Info_model.php */
/* Location: ./application/models/Info_model.php */