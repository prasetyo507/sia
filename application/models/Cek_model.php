<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cek_model extends CI_Model {

	function cekdb_kuisioner($kd_krs)
	{
		$actyear = getactyear();
		$this->db2 = $this->load->database('eval', TRUE);
		$this->db2->select('count(distinct kd_jadwal) as jml');
		$this->db2->where('npm_mahasiswa', substr($kd_krs, 0,12));
		$this->db2->where('tahunajaran', substr($kd_krs, 12,5));
		$this->db2->where('kd_jadwal !=', '');
		$this->db2->where('kd_jadwal is NOT NULL', NULL, FALSE);
		$q = $this->db2->get('tbl_pengisian_kuisioner_'.$actyear.'');
		return $q->row()->jml;
	}

	function cekjumlahkrs($kd_krs)
	{
		$this->db->select('count(distinct kd_jadwal) as jml');
		$this->db->from('tbl_krs b');
		$this->db->join('tbl_matakuliah a', 'a.kd_matakuliah = b.kd_matakuliah');

		$where = "a.nama_matakuliah NOT LIKE 'skripsi%' AND a.nama_matakuliah NOT LIKE 'tesis%' AND a.nama_matakuliah NOT LIKE 'kerja praktek%' AND a.nama_matakuliah NOT LIKE 'magang%' AND a.nama_matakuliah NOT LIKE 'kuliah kerja%'";
		
		$this->db->where($where);
		$this->db->where('kd_krs', $kd_krs);
		$q = $this->db->get();
		return $q->row()->jml;

		// return $this->db->query("SELECT count(distinct kd_jadwal) as jml from tbl_krs b join tbl_matakuliah a
		// 							on a.kd_matakuliah = b.kd_matakuliah where kd_krs = '".$kd_krs."' 
		// 							and a.nama_matakuliah NOT LIKE 'skripsi%' AND a.nama_matakuliah NOT LIKE 'tesis%' AND a.nama_matakuliah NOT LIKE 'kerja praktek%' AND a.nama_matakuliah NOT LIKE 'magang%' AND a.nama_matakuliah NOT LIKE 'kuliah kerja%")->row()->jml;
	}

}

/* End of file Cek_model.php */
/* Location: ./application/models/Cek_model.php */