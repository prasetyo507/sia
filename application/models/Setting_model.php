<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Setting_model extends CI_Model {



	function getaktivasi($kode)

	{

		$this->db->where('kode_cpanel', $kode);

		$this->db->where('status', 1);

		$q = $this->db->get('tbl_cpanel');

		return $q;

	}	



	function getmhsnonakun()

	{
		$tahun = date('Y');

		$batas = $tahun - 7;//var_dump($batas);exit();

		$this->db->select('a.NIMHSMSMHS, a.NMMHSMSMHS, b.userid , b.username , b.user_type , b.status');

		$this->db->from('tbl_mahasiswa a');

		$this->db->join('tbl_user_login b', 'a.NIMHSMSMHS = b.userid', 'left');

		$this->db->where('a.TAHUNMSMHS >=', $batas);

		$this->db->where('b.userid is null', null, false);

		return $this->db->get();

	}

	function getmhsnews1($gel)
	{
		$tahun = date('Y');

		$batas = $tahun - 7;//var_dump($batas);exit();

		$this->db->select('*');

		$this->db->from('tbl_form_camaba a');

		$this->db->join('tbl_mahasiswa b', 'b.NIMHSMSMHS = a.npm_baru', 'left');

		$this->db->where('a.status', 1);

		$this->db->where('a.gelombang', $gel);

		$this->db->where('b.NIMHSMSMHS is null', null, false);

		return $this->db->get();
	}

	function getnimnews1($prodi,$jenis)
	{
		$tahun = '2019';

		if ($jenis == 'MB') {
			
			$this->db->select('b.NIMHSMSMHS');

			//$this->db->from('tbl_form_camaba a');

			$this->db->from('tbl_mahasiswa b');

			//$this->db->where('a.status', 1);

			$this->db->where('b.KDPSTMSMHS', $prodi);

			$this->db->where('b.TAHUNMSMHS', $tahun);

			$this->db->where('SUBSTR(NIMHSMSMHS,9,1)', 5);

			$this->db->order_by('b.NIMHSMSMHS', 'DESC');
		
			$this->db->limit('1');

			return $this->db->get();

		} else {

			$this->db->select('b.NIMHSMSMHS');

			//$this->db->from('tbl_form_camaba a');

			$this->db->from('tbl_mahasiswa b');

			//$this->db->where('a.status', 1);

			$this->db->where('b.KDPSTMSMHS', $prodi);

			$this->db->where('b.TAHUNMSMHS', $tahun);

			$this->db->where('SUBSTR(NIMHSMSMHS,9,1)', 7);

			$this->db->order_by('b.NIMHSMSMHS', 'DESC');
		
			$this->db->limit('1');

			return $this->db->get();
			
		}

		// $this->db->select('b.NIMHSMSMHS');

		// //$this->db->from('tbl_form_camaba a');

		// $this->db->from('tbl_mahasiswa b');

		// //$this->db->where('a.status', 1);

		// $this->db->where('b.KDPSTMSMHS', $prodi);

		// $this->db->where('b.TAHUNMSMHS', $tahun);

		// if ($jenis == 'MB') {
		// 	$this->db->where('SUBSTR(NIMHSMSMHS,9,1)', 5);
		// 	//$this->db->where('a.jenis_pmb', $jenis);
		// } else {
		// 	$this->db->where('SUBSTR(NIMHSMSMHS,9,1)', 7);
		// 	//$this->db->where('a.jenis_pmb !=', 'MB');
		// }

		// //$this->db->where('a.gelombang', $gel);

		// $this->db->order_by('b.NIMHSMSMHS', 'DESC');
		
		// $this->db->limit('1');

		// return $this->db->get();
	}

	function getmhsnews2($gel)
	{
		$tahun = date('Y');

		$batas = $tahun - 7;//var_dump($batas);exit();

		$this->db->select('*');

		$this->db->from('tbl_pmb_s2 a');

		$this->db->join('tbl_mahasiswa b', 'b.NIMHSMSMHS = a.npm_baru', 'left');

		$this->db->where('a.status', 1);

		$this->db->where('a.gelombang', $gel);

		$this->db->where('b.NIMHSMSMHS is null', null, false);

		return $this->db->get();
	}

	function getnimnews2($prodi,$jenis)
	{
		$tahun = '20191';

		$this->db->select('b.NIMHSMSMHS');

		//$this->db->from('tbl_pmb_s2 a');

		$this->db->from('tbl_mahasiswa b');

		//$this->db->where('a.status', 1);

		$this->db->where('b.KDPSTMSMHS', $prodi);

		$this->db->where('b.SMAWLMSMHS', $tahun);

		if ($jenis == 'MB') {
			$this->db->where('SUBSTR(NIMHSMSMHS,8,1)', 5);
			//$this->db->where('a.jenis_pmb', $jenis);
		} else {
			$this->db->where('SUBSTR(NIMHSMSMHS,8,1)', 7);
			//$this->db->where('a.jenis_pmb !=', 'MB');
		}

		//$this->db->where('a.jenis_pmb', $jenis);

		//$this->db->where('a.gelombang', $gel);

		$this->db->order_by('b.NIMHSMSMHS', 'DESC');
		
		$this->db->limit('1');

		return $this->db->get();
	}

	function getkarynonakun()
	{

		$this->db->select('a.nid, a.nama, a.status');

		$this->db->from('tbl_karyawan a');

		//$this->db->join('tbl_user_login b', 'a.nik = b.userid', 'left');

		$this->db->join('tbl_user_login d', 'a.nid = d.userid', 'left');

		//$this->db->join('tbl_jabatan c', 'a.jabatan_id = c.id_jabatan');

		$this->db->where('d.userid is null', null, false);

		$this->db->where('a.status', '1');

		return $this->db->get();

	}



}



/* End of file Setting_model.php */

/* Location: .//C/Users/danum246/AppData/Local/Temp/fz3temp-2/Setting_model.php */