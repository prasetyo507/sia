<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function runWS($data,$type)
{
	//$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
	$url = 'http://172.16.0.58:8082/ws/live2.php'; // gunakan live bila sudah yakin
	//global $url;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, 1);
	$headers=array("content-type: application/json");
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	if ($data) {
		$data = json_encode($data);
		//var_dump($data);echo '<br/>';
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	}
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$response = curl_exec($ch);
	//if ($response === false) $response = curl_error($ch);
 	//var_dump($response) .'<br>';
	curl_close($ch);
	return $response;
}