<?php

	function nama_dsn($kd_dsn){
		$CI = &get_instance();
		
		$CI->db->where('nid', $kd_dsn);
		$kary=$CI->db->get('tbl_karyawan', 1)->row();
		
		return $kary->nama;
	} 

	function nama_dsn_nidn($kd_dsn){
		$CI = &get_instance();
		
		$CI->db->where('nidn', $kd_dsn);
		$kary=$CI->db->get('tbl_karyawan', 1)->row();
		
		return $kary->nama;
	} 



	function PopulateForm(){

		$CI = &get_instance();

		$post = array();

		foreach(array_keys($_POST) as $key){

			$post[$key] = $CI->input->post($key);

		}

		return $post;

	}

	function ymdDate($date)
	{
		$exp = explode('/', $date);
		$com = $exp[2].'-'.$exp[1].'-'.$exp[0];
		return $com;
	}

	function TanggalIndo($date, $format = ''){
		$BulanIndo = array("","Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	 
		
		$split = explode('-', $date);

		if($format == 'd M Y')
			return $split[0]. ' '. $BulanIndo[ (int)$split[1] ] . ' ' . $split[2];
		
		return $split[2] . ' ' . $BulanIndo[ (int)$split[1] ] . ' ' . $split[0];
	}

	function TanggalIndoRange($date1,$date2){
		$BulanIndo = array("","Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	 
		$split1 = explode('-', $date1);
		$split2 = explode('-', $date2);

		if ($split1[1] == $split2[1]) {
			return $split1[2] . ' ' . ' - ' . $split2[2] . ' ' . $BulanIndo[ (int)$split2[1] ] . ' ' . $split2[0];
		}else{
			return $split1[2] . ' ' . $BulanIndo[ (int)$split1[1] ] . ' ' . $split1[0] . ' - ' . $split2[2] . ' ' . $BulanIndo[ (int)$split2[1] ] . ' ' . $split2[0];
		}
		
	}

	function toIdnDay($day)
	{
		switch ($day) {
			case 'Sunday':
				$hari = "Minggu";
				break;
			
			case 'Monday':
				$hari = "Senin";
				break;

			case 'Tuesday':
				$hari = "Selasa";
				break;

			case 'Wednesday':
				$hari = "Rabu";
				break;

			case 'Thursday':
				$hari = "Kamis";
				break;

			case 'Friday':
				$hari = "Jum'at";
				break;

			case 'Saturday':
				$hari = "Sabtu";
				break;
		}
		return $hari;
		
	}

	function dayToNumber($myday)
	{
		switch ($myday) {
			case 'Sunday':
				$res = "7";
				break;
			
			case 'Monday':
				$res = "1";
				break;

			case 'Tuesday':
				$res = "2";
				break;

			case 'Wednesday':
				$res = "3";
				break;

			case 'Thursday':
				$res = "4";
				break;

			case 'Friday':
				$res = "5";
				break;

			case 'Saturday':
				$res = "6";
				break;
		}
		return $res;		
	}

	function notohari($key){

		if ($key == 1) {

			$hari = 'Senin';

		}elseif ($key == 2) {

			$hari = 'Selasa';

		}elseif ($key == 3) {

			$hari = 'Rabu';

		}elseif ($key == 4) {

			$hari = 'Kamis';

		}elseif ($key == 5) {

			$hari = 'Jumat';

		}elseif ($key == 6) {

			$hari = 'Sabtu';

		}elseif ($key == 7) {

			$hari = 'Minggu';

		}else{
			$hari = '';
		}



		return $hari;

	}

	function get_fak($key){
		if ($key == '1') {

			$fakultas = 'EKONOMI';

		}elseif ($key == '2') {

			$fakultas = 'TEKNIK';

		}elseif ($key == '3') {

			$fakultas = 'HUKUM';

		}elseif ($key == '4') {

			$fakultas = 'ILMU KOMUNIKASI';

		}elseif ($key == '5') {

			$fakultas = 'PSIKOLOGI';

		}elseif ($key == '7') {

			$fakultas = 'ILMU PENDIDIKAN';

		}else{
			$fakultas = '';
		}

		return $fakultas;
	}

	function get_jur($key)
	{
		if ($key == '24201') {

			$prodi = 'TEKNIK KIMIA';

		}elseif ($key == '25201') {

			$prodi = 'TEKNIK LINGKUNGAN';

		}elseif ($key == '26201') {

			$prodi = 'TEKNIK INDUSTRI';

		}elseif ($key == '32201') {

			$prodi = 'TEKNIK PERMINYAKAN';

		}elseif ($key == '55201') {

			$prodi = 'TEKNIK INFORMATIKA';

		}elseif ($key == '61101') {

			$prodi = 'MAGISTER MANAJEMEN';

		}elseif ($key == '61201') {

			$prodi = 'MANAJEMEN';

		}elseif ($key == '62201') {

			$prodi = 'AKUNTANSI';

		}elseif ($key == '70201') {

			$prodi = 'ILMU KOMUNIKASI';

		}elseif ($key == '73201') {

			$prodi = 'PSIKOLOGI';

		}elseif ($key == '74101') {

			$prodi = 'MAGISTER HUKUM';

		}elseif ($key == '74201') {

			$prodi = 'ILMU HUKUM';

		}elseif ($key == '86206'){

			$prodi = 'PENDIDIKAN GURU SEKOLAH DASAR';

		}elseif ($key == '85202') {

			$prodi = 'PENDIDIKAN KEPELATIHAN OLAHRAGA';
			
		}

		return $prodi;
	}

	function zerotodash($key){
		if ($key == '' || $key == NULL) {
			$a =  ' - '; 
		}elseif ($key != '' || $key != NULL) {
			$a =  $key; 
		}

		return $a; 
	}

	function lulus($key){
		if ($key == '-' || $key == NULL) {
			$a =  ' - '; 
		}elseif ($key == 0) {
			$a =  'TIDAK LULUS'; 
		}elseif ($key == 1) {
			$a =  'LULUS'; 
		}

		return $a;
	}

	function get_thnajar($key){

		$ak  = substr($key, 4,1);
		$thn = substr($key, 0,4);

		if ($ak == 1) {
			$data = $thn.' / Ganjil';
		}elseif ($ak == 2) {
			$data = $thn.' / Genap';
		}

		return $data;
	}

	function get_mhs_jur($npm){
		$CI = &get_instance();
		
		$CI->db->where('NIMHSMSMHS', $npm);
		$jur=$CI->db->get('tbl_mahasiswa', 1)->row();
		$jurusan = $jur->KDPSTMSMHS;

		return $jurusan;
	}

	function get_nm_mhs($npm){
		$CI = &get_instance();
		
		$CI->db->where('NIMHSMSMHS', $npm);
		$q=$CI->db->get('tbl_mahasiswa', 1)->row();
		$data = $q->NMMHSMSMHS;

		return $data;
	}

	function get_nama_mk($kd,$prodi){
		$CI = &get_instance();
		
		$CI->db->where('kd_matakuliah', $kd);
		$CI->db->where('kd_prodi', $prodi);
		$nama=$CI->db->get('tbl_matakuliah', 1)->row();
		
		return $nama->nama_matakuliah;		
	}

	function get_jur_mhs($npm){
		$CI = &get_instance();
		
		$CI->db->where('NIMHSMSMHS', $npm);
		$jur=$CI->db->get('tbl_mahasiswa', 1)->row();
		$jurusan = get_jur($jur->KDPSTMSMHS);

		return $jurusan;
	}

	function get_kd_jdl($id){
		$CI = &get_instance();

		$CI->db->where('id_jadwal', $id);
		$q=$CI->db->get('tbl_jadwal_matkul')->row();

		return $q->kd_jadwal;
	}
	function get_id_jdl($id){
		$CI = &get_instance();

		$CI->db->where('kd_jadwal', $id);
		$q=$CI->db->get('tbl_jadwal_matkul')->row();

		return $q->id_jadwal;
	}

	function get_kd_jdl_feed($id){
		$CI = &get_instance();

		$CI->db->where('id_jadwal', $id);
		$q=$CI->db->get('tbl_jadwal_matkul_feeder')->row();

		return $q->kd_jadwal;
	}

	function get_dtl_jadwal($kdjdl)
	{
		$CI =& get_instance();
		return $CI->db->where('kd_jadwal', $kdjdl)->get('tbl_jadwal_matkul')->row_array();
	}

	function get_kd_jdl_remid($id){
		$CI = &get_instance();

		$CI->db->where('id_jadwal', $id);
		$q=$CI->db->get('tbl_jadwal_matkul_sp')->row();

		return $q->kd_jadwal;
	}

	function get_nm_pa($nid){
		$CI = &get_instance();
		
		$CI->db->where('nid', $nid);
		$q=$CI->db->get('tbl_karyawan', 1)->row();
		$jurusan = $q->nama;

		return $jurusan;
	}

	function get_fak_byprodi($id){

		$CI = &get_instance();


		$CI->db->where('kd_prodi', $id);
		$q=$CI->db->get('tbl_jurusan_prodi')->row();

		$key = $q->kd_fakultas;

		if ($key == '1') {

			$fakultas = 'EKONOMI';

		}elseif ($key == '2') {

			$fakultas = 'TEKNIK';

		}elseif ($key == '3') {

			$fakultas = 'HUKUM';

		}elseif ($key == '4') {

			$fakultas = 'ILMU_KOMUNIKASI';

		}elseif ($key == '5') {

			$fakultas = 'PSIKOLOGI';

		}elseif ($key == '6') {

			$fakultas = 'PASCASARJANA';
			
		}elseif ($key == '7') {

			$fakultas = 'ILMU PENDIDIKAN';

		}else{
			$fakultas = '';
		}

		return $fakultas;
	}



	function del_ms($jam) {
		$hasil=substr($jam,0,-3);
		return $hasil;
	}

	function myUrlEncode($string) {
    $entities = array('%21','%20', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
    $replacements = array('!',' ', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
    return str_replace($entities, $replacements, urlencode($string));
}

	function jenis_kelas($kls,$uid)
	{
		$CI = &get_instance();
		
		$CI->db->where('kelas', $kls);
		// $CI->db->where('kd_tahunajaran',getactyear());
		$CI->db->like('kd_jadwal',$uid,'after');
		$CI->db->not_like('kd_matakuliah','MKDU','after');
		$CI->db->not_like('kd_matakuliah','MKU','after');
		$cls = $CI->db->get('tbl_jadwal_matkul')->row();
		$kelas = $cls->waktu_kelas;

		if ($kelas == 'PG') {

			$waktu = 'A';

		}elseif ($kelas == 'SR') {

			$waktu = 'B';

		}else{
			$waktu = 'C';
		}

		return $waktu;
	}

	function classType($id)
	{
		if ($id == 'PG') {
			$type = 'A';
		} elseif ($id == 'SR') {
			$type = 'B';
		} else {
			$type = 'C';
		}

		return $type;
		

	}

	function typeofclass_byName($kls, $uid)
	{
		$CI = &get_instance();
		
		$CI->db->where('kelas', $kls);
		// $CI->db->where('kd_tahunajaran',getactyear());
		$CI->db->like('kd_jadwal', $uid, 'after');
		$CI->db->not_like('kd_matakuliah','MKDU','after');
		$CI->db->not_like('kd_matakuliah','MKU','after');
		$cls = $CI->db->get('tbl_jadwal_matkul')->row();
		$kelas = $cls->waktu_kelas;

		return $kelas;
	}

	function getKonvMk($kd,$ta,$usr)
	{
		$CI = &get_instance();

		if ($ta < '20151') {
			$ret = $CI->db->select('*')
						->from('tbl_konversi_matkul_temp a')
						->join('tbl_matakuliah_copy b', 'a.kd_lama = b.kd_matakuliah', 'left')
						->where('a.tahunakademik',$ta)
						->where('a.kd_lama',$kd)
						->where('b.kd_prodi',$usr)
						->get()->row();
		} else {
			$ret = $CI->db->select('*')
						->from('tbl_konversi_matkul_temp a')
						->join('tbl_matakuliah b', 'a.kd_lama = b.kd_matakuliah', 'left')
						->where('a.kd_lama',$kd)
						->where('b.kd_prodi',$usr)
						->get()->row();	
		}
		
		return $ret;
	}

	function giveColorForFile($u,$t,$k)
	{
		$CI = &get_instance();
		$CI->dbreg = $CI->load->database('regis', TRUE);

		$colr = $CI->dbreg->query("SELECT valid from tbl_file where userid = '".$u."' and tipe = '".$t."' and key_booking = '".$k."'")->row()->valid;
		if ($colr == 1) {
			$w = '';
		} elseif ($colr == 2) {
			$w = 'style="background:#F0E68C"';
		} elseif ($colr == 0) {
			$w = 'style="background:#F08080"';
		}

		return $w;
	}

	function getNoPmb($u)
	{
		$CI = &get_instance();
		$CI->regdb = $CI->load->database('regis',true);
		$usr = $CI->regdb->where('user_input', $u)->get('tbl_form_pmb')->row();
        return $usr->nomor_registrasi;
	}

	function getNamePmb($u)
	{
		$CI = &get_instance();
		$CI->regdb = $CI->load->database('regis',true);
		$usr = $CI->regdb->where('userid', $u)->get('tbl_regist')->row();
		if ($usr->nm_belakang == '' || is_null($usr->nm_belakang)) {
            $nav_name = $usr->nm_depan;
        } else {
            $nav_name = $usr->nm_depan.' '.$usr->nm_belakang;
        }
        return $nav_name;
	}

	function getNameBank($n)
	{
		$CI =& get_instance();
		$CI->dbreg = $CI->load->database('regis', TRUE);

		$get = $CI->dbreg->where('kd_bank',$n)->get('tbl_bank');
		$bnk = $get->row()->bank;

		return $bnk;
	}

	function get_thajar($key)
	{
		$CI =& get_instance();

		$CI->db->where('kode', $key);
		return $CI->db->get('tbl_tahunakademik')->row()->tahun_akademik;
	}

	function get_room($key)
	{
		$CI =& get_instance();
		$CI->db->where('id_ruangan', $key);
		return $CI->db->get('tbl_ruangan')->row()->ruangan;
	}

	function getSemesterMk($kd,$pr)
	{
		$CI =& get_instance();
		$CI->db->where('kd_matakuliah',$kd);
		$CI->db->like('kd_kurikulum',$pr,'after');
		return $CI->db->get('tbl_kurikulum_matkul_new')->row()->semester_kd_matakuliah;
	}

	function tridarma($log,$logtyp)
	{
		if ($log == 'rsc') {
			if ($logtyp == 'MDR') {
				$fee = 'MANDIRI';
			} elseif ($logtyp == 'BSW') {
				$fee = 'BEASISWA';
			} elseif ($logtyp == 'INT') {
				$fee = 'INTERNASIONAL';
			} elseif ($logtyp == 'NSL') {
				$fee = 'NASIONAL';
			} elseif ($logtyp == 'REG') {
				$fee = 'LOKAL/REGIONAL';
			} elseif ($logtyp == 'KET') {
				$fee = 'KETUA';
			} else {
				$fee = 'ANGGOTA';
			}

			return $fee;

		} else {
			if ($logtyp == 'INT') {
				$hey = 'INTERNASIONAL';
			} elseif ($logtyp == 'NSL') {
				$hey = 'Nasional Terakreditasi';
			} else {
				$hey = 'Tidak Terakreditasi';
			}

			return $hey;
		}
	}

	function getNameMhs($u)
	{
		$CI =& get_instance();
		$usr = $CI->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$u,'NIMHSMSMHS','asc')->row()->NMMHSMSMHS;
		return $usr;
	}

	function get_amount_mhs($kd)
	{
		$CI =& get_instance();

		$count = $CI->app_model->getdetail('tbl_krs','kd_jadwal',$kd,'id_krs','asc')->num_rows();
		return $count;
	}

	function getactyear()
	{
		$CI =& get_instance();
		$get = $CI->db->where('status',1)->get('tbl_tahunakademik')->row()->kode;
		return $get;
	}

	function getCodeProdiMhs($nim)
	{
		$CI =& get_instance();
		$code = $CI->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim,'NIMHSMSMHS','asc')->row()->KDPSTMSMHS;
		return $code;
	}

	function yearBefore()
	{
		$CI =& get_instance();
		$data = getactyear();
		$year = substr($data, 0,4);
		$code = substr($data, 4,1);
		if ($code == 1) {
			$val = ($year-1).'2';
		} else {
			$val = $year.'1';
		}
		return $val;
		
	}

	function getReligion($id)
	{
		switch ($id) {
			case 'ISL':
				$reg = 'ISLAM';
				break;

			case 'KTL':
				$reg = 'KATOLIK';
				break;

			case 'PRT':
				$reg = 'PROTESTAN';
				break;

			case 'HND':
				$reg = 'HINDU';
				break;

			case 'BDH':
				$reg = 'BUDHA';
				break;

			case 'KHC':
				$reg = 'KONGHUCU';
				break;

		}

		return $reg;
	}

	function getDescYearbyCode($code)
	{
		$CI =& get_instance();

		$get = $CI->app_model->getdetail('tbl_tahunakademik','kode',$code,'kode','asc')->row()->tahun_akademik;

		return $get;
	}

	function getFirstStudy($val)
	{
		$CI =& get_instance();

		$dats = $CI->db->query("SELECT * from tbl_kalender_dtl where kegiatan = 'kuliah perdana' and kd_kalender like '".getactyear()."%'")->row()->mulai;

		return $dats;
	}

	function getTipeUjian($id,$year,$tipe)
	{
		$CI =& get_instance();
		$CI->load->model('ujian_model');
		$get = $CI->ujian_model->getjadwaluji($id,$year,$tipe);
		return $get;
	}

	function getDataUjian($id,$type)
	{
		$CI =& get_instance();
		$CI->load->model('hilman_model');
		$get = $CI->hilman_model->moreWhere(array('id_jadwal' => $id, 'tipe_uji' => $type), 'tbl_jadwaluji');
		return $get;
	}

	function studyStart($year)
	{
		$front = substr($year, 0,4);
		$back = substr($year, 4,1);

		$newyear = $front-7;
		$fixyear = $newyear.$back;

		return $fixyear;
	}

	function amountStudent($uid,$typ,$year)
	{
		$CI =& get_instance();
		$CI->load->model('akademik/amountstd_model','amount');
		$get = $CI->amount->getAmount($uid,$typ,$year);
		return $get;
	}

	function romawi($val)
	{
		switch ($val) {
			case '1':
				$res = 'I';
				break;

			case '2':
				$res = 'II';
				break;

			case '3':
				$res = 'III';
				break;

			case '4':
				$res = 'IV';
				break;

			case '5':
				$res = 'V';
				break;

			case '6':
				$res = 'VI';
				break;

			case '7':
				$res = 'VII';
				break;

			case '8':
				$res = 'VIII';
				break;

			case '9':
				$res = 'IX';
				break;

			case '10':
				$res = 'X';
				break;

			case '11':
				$res = 'XI';
				break;

			case '12':
				$res = 'XII';
				break;
		}
		return $res; 
	}

	function getNextYear()
	{
		$CI =& get_instance();

		// ganjil atau genap
		$actyear = getactyear();
		if ($actyear%2 == 0) {
			$subs = substr($actyear, 0, 4);
			$addconst = ($subs+1);
			$nextyear = $addconst.'1';
		} else {
			$nextyear = $actyear+1;
		}
		return $nextyear;
	}

	function getEmailPmb($uid)
	{
		$CI =& get_instance();
		$CI->dbreg = $CI->load->database('regis', TRUE);

		$email = $CI->dbreg->query('SELECT email from tbl_regist where userid = "'.$uid.'"')->row()->email;
		return $email;
	}

	function yearImprove()
	{
		$act = getactyear();
		$year = $act+2;
		return $year;		
	}

	function provinceName($id)
	{
		$CI =& get_instance();
		$CI->db->where('id', $id);
		return $CI->db->get('tbl_provinsis', 1)->row()->name;
	}

	function kokabName($id)
	{
		$CI =& get_instance();
		$CI->db->where('id', $id);
		return $CI->db->get('tbl_kokab', 1)->row()->name;
	}

	function kecamatanName($id)
	{
		$CI =& get_instance();
		$CI->db->where('id', $id);
		return $CI->db->get('tbl_kecamatans', 1)->row()->name;
	}

	function kelurahanName($id)
	{
		$CI =& get_instance();
		$CI->db->where('id', $id);
		return $CI->db->get('tbl_kelurahans', 1)->row()->name;
	}

	/*Get Nama Kelas*/
	function getNamaKelas($idJadwal='')
	{
		if(!empty($idJadwal)) {
			$CI =& get_instance();
			$CI->db->where('id_jadwal', $idJadwal);
			$CI->db->select('kelas');
			return $CI->db->get('tbl_jadwal_matkul', 1)->row()->kelas;
		}else{
			echo "Missing parameter";
		}
	}

	
	// Debug
	function dd($value='', $type = 1, $isExit = '')
	{
		switch ($type) {
			case '1':
				echo "<pre>";
				print_r ($value);
				echo "</pre>";
				if (!empty($isExit)) {
					exit();
				}
				break;
			
			case '2':
				var_dump ($variable);
				die();
				break;
		}
		
	}

	function getKaprodi($kodeprodi)
	{
		$CI =& get_instance();
		return $CI->db->select('a.nama as nama')
						->from('tbl_jurusan_prodi b')
						->join('tbl_karyawan a','a.nid = b.kaprodi')
						->where('b.kd_prodi',$kodeprodi)
						->get()->row()->nama;
	}

	function getValueRange($poin)
	{	
		$this->db->select('nilai_bawah, nilai_atas, nilai_huruf');
		$this->db->where('deleted_at');
		$data = $this->db->get('tbl_index_nilai')->result();

		foreach ($data as $key) {
			if (in_array($poin, range($key->nilai_bawah, $key->nilai_atas))) {
				$nilai = $key->nilai_huruf;
			}
		}

		return $nilai;
	}