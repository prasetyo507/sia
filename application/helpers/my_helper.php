<?php 


	function get_prodi($kd)
	{
		$CI = &get_instance();
		$CI->dbsia = $CI->load->database('sia', TRUE);
		$a = $CI->dbsia->query("SELECT prodi from tbl_jurusan_prodi where kd_prodi = '".$kd."'")->row()->prodi;
		return $a;
	}

	function getName($u)
	{
		$CI = &get_instance();
		$usr = $CI->db->where('userid', $u)->get('tbl_regist')->row();
		if ($usr->nm_belakang == '' || is_null($usr->nm_belakang)) {
            $nav_name = $usr->nm_depan;
        } else {
            $nav_name = $usr->nm_depan.' '.$usr->nm_belakang;
        }
        return $nav_name;
	}

	function dateIdn($date)
	{
		$BulanIndo = array("","Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	 	$split = explode('-', $date);
		return $split[2] . ' ' . $BulanIndo[ (int)$split[1] ] . ' ' . $split[0];
	}

	function dateRmw($date)
	{
		$BulanIndo = array("","I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
	 	$split = explode('-', $date);
		return $split[2] . ' ' . $BulanIndo[ (int)$split[1] ] . ' ' . $split[0];
	}

	function getEmail($u)
	{
		$CI = &get_instance();
		$cek = $CI->db->where('userid',$u)->get('tbl_regist')->row()->email;
		return $cek;
	}

	function complete($tbl,$fld,$key,$fldcom,$keycom)
	{
		$CI = &get_instance();
		$cek = $CI->db->where($fld, $key)->where($fldcom, $keycom)->get($tbl)->num_rows();
		if ($cek > 0) {
			$res = 'Lengkap';
		} else {
			$res = 'Belum Lengkap';
		}
		return $res;
	}

	function getCamp($c)
	{
		if ($c == 'bks') {
			$camp = 'BEKASI';
		} else {
			$camp = 'JAKARTA';
		}
		return $camp;
	}

	function regType($r)
	{
		if ($r == 'MB') {
			$reg = 'Mahasiswa Baru';
		} elseif ($r == 'RM') {
			$reg = 'Readmisi';
		} else {
			$reg = 'Konversi';
		}
		return $reg;
	}

	function programType($p)
	{
		if ($p == '1') {
			$pro = 'Strata satu';
		} else {
			$pro = 'Strata dua';
		}
		return $pro;
	}

	function myEncode($e,$type)
	{
		$en = array('0','1','2','3','4','5','6','7','8','9');
		$re = array('N','o','P','L','B','l','m','X','y','z');
		if ($type) {
			return str_replace($en, $re, $e);
		} elseif (!$type) {
			return str_replace($re, $en, $e);
		}
	}

	function validHumas($u,$t)
	{
		$CI = &get_instance();
		$val = $CI->db->select('valid')->from('tbl_file')->where('userid',$u)->where('tipe',$t)->get();

		if ($val->result() == TRUE) {
			if ($val->row()->valid == 1) {
				$prt = 'Sesuai';
			} elseif ($val->row()->valid == 0) {
				$prt = 'Tidak Sesuai';
			} elseif ($val->row()->valid == 2) {
				$prt = 'Belum Divalidasi';
			}
			return $prt;
		} else {
			$prt = '-';
			return $prt;
		}
	}

 ?>