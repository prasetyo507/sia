<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_maba.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 1px solid black;
}



th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr> 
            <th rowspan="2">Fakultas</th>
            <th colspan="3">Gelombang I</th>
            <th colspan="3">Gelombang II</th>
            <th colspan="3">Gelombang III</th>
            <th colspan="3">Gelombang IV & tambahan</th>
            <th colspan="3">Jumlah</th>
            <th rowspan="2">Jumlah Total</th>
            <th rowspan="2">%</th>
        </tr>
       
        <tr>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
        </tr>
    </thead>
    <tbody>
        
         <?php
            $t_pg1 = 0;
            $t_sr1 = 0;
            $t_ky1 = 0;
            $jml_by_prodi_all= 0;

        foreach ($fakultas as $isi) { ?>
            
        <tr>
            <td style="color:#000000; background-color:#ffff00"><b><?php echo $isi->fakultas; ?></b></td>
                <td colspan="17" style="background-color:#ffff00"></td>
        </tr>

        <?php $q = $this->db->query('select * from tbl_jurusan_prodi where kd_fakultas = '.$isi->kd_fakultas.'')->result(); ?>
        
            <?php 
           

            foreach ($q as $rows) { ?>
                 <tr>
                    <td><b><?php echo $rows->prodi; ?></b></td>
                    <?php if ($ss == 'S1') { ?>
                        <?php if ($kampus == 'bks'){ ?>
                            <?php $pg1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                            <?php $sr1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                            <?php $ky1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky"')->row();?>
                        <?php }elseif ($kampus == 'jkt') { ?>
                            <?php $pg1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                            <?php $sr1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                            <?php $ky1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky"')->row();
                        } elseif ($kampus == 'ALL') { ?>
                            <?php $pg1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                            <?php $sr1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                            <?php $ky1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky"')->row();
                        } ?>
                    <?php } elseif($ss == 'S2') { ?>
                        <?php if ($kampus == 'bks'){ ?>
                            <?php $pg1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                            <?php $sr1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                            <?php $ky1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "ky"')->row();?>
                        <?php }elseif ($kampus == 'jkt') { ?>
                            <?php $pg1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                            <?php $sr1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                            <?php $ky1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "ky"')->row();
                        } elseif ($kampus == 'ALL') { ?>
                            <?php $pg1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                            <?php $sr1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                            <?php $ky1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "ky"')->row();
                        } ?>
                    <?php } elseif($ss == 'ALL') { ?>

                            <?php if ($isi->kd_fakultas == 6){ ?>
                                <?php $pg1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                <?php $sr1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                <?php $ky1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_pmb_s2 WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "ky"')->row();
                            }else { ?>
                                <?php $pg1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg"')->row();?>
                                <?php $sr1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr"')->row();?>
                                <?php $ky1 = $this->db->query('SELECT COUNT(*) AS jml FROM tbl_form_camaba WHERE kampus = "'.$kampus.'" AND jenis_pmb ="'.$jenis.'" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky"')->row();?>
                            } ?>
                           
                   <?php } ?>
                    

                        
                        
                        <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                        <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                        

                        <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                        <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                        <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td><?php echo $jml_by_prodi; ?></td>
                        <td></td>
                    
                </tr>

               
            <?php } ?>
            
            
            
        <?php } ?> 
        <tr>
            <td><b>JUMLAH</b></td>
                        <td><?php echo $t_pg1; ?></td>
                        <td><?php echo $t_sr1; ?></td>
                        <td><?php echo $t_ky1; ?></td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td><?php echo $jml_by_prodi_all; ?></td>
                        <td>100%</td>
        </tr> 
    </tbody>
</table>