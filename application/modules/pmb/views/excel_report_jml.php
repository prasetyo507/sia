<?php
header("Content-Type: application/xls");    

    header("Content-Disposition: attachment; filename=data_mhs_xx.xls");
 
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
                    
<table>
	<thead>
        <tr> 
        	<th rowspan="3">No</th>
            <th rowspan="3">Prodi</th>
            <th rowspan="3">Gelombang</th>
            <th rowspan="3">Jenis Pendaftar</th>
            <th rowspan="3">Status</th>
            <?php 
                if ($this->session->userdata('kate') == 'edu' || $this->session->userdata('kate') == 'wrk') 
                { 
                    $col = 4; 
                } else {
                    $col = 2; 
            } ?>
            <th style="text-align:center" colspan="<?php echo $col; ?>">Kategori</th>
        </tr>
        <tr>
            <?php if ($this->session->userdata('kate') == 'edu') {
                    $q = 'Pendidikan Orang Tua';
                } elseif ($this->session->userdata('kate') == 'wrk') {
                    $q = 'Pekerjaan Orang Tua';
                } elseif ($this->session->userdata('kate') == 'sch') {
                    $q = 'Asal Sekolah';
                } elseif ($this->session->userdata('kate') == 'cty') {
                    $q = 'Asal Kota';
                } elseif ($this->session->userdata('kate') == 'grd') {
                    $q = 'Tahun Lulus';
                }
            ?>
            <th colspan="2"><?php echo $q; ?></th>
        </tr>
        <tr>
            <th>Kategori</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;
        foreach ($wow as $key) {  ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td rowspan="<?php count($key->prodi) ?>"><?php echo $key->prodi; ?></td>
                <td><?php echo $key->gelombang; ?></td>
                <td><?php echo $key->jenis_pmb; ?></td>
                    <?php 
                        if ($key->status < 1) {
                            $kadal =  "Registrasi";
                        } elseif($key->status == 1) {
                            $kadal =  "Lulus Tes";
                        } elseif ($key->status > 1) {
                            $kadal =  "Daftar Ulang";
                    }?>
                <td><?php echo $kadal; ?></td>
                <td>
                    <?php 
                        if ($this->session->userdata('kate') == 'sch') {
                            if ($key->jenis_sch_maba == 'MA') {
                                echo "MA";
                            } elseif($key->jenis_sch_maba == 'SMA') {
                                echo "SMA";
                            } elseif ($key->jenis_sch_maba == 'SMK') {
                                echo "SMK";
                            } elseif ($key->jenis_sch_maba == 'LAIN') {
                                echo "LAIN-LAIN";
                            } elseif ($key->jenis_sch_maba == 'SMTB') {
                                echo "SMTB";
                            }
                        } elseif ($this->session->userdata('kate') == 'cty') {
                            echo $key->kota_sch_maba;
                        } elseif ($this->session->userdata('kate') == 'grd') {
                            echo $key->lulus_maba;
                        }
                    ?>
                </td>
                <td><?php echo $key->hitung; ?></td>
            </tr>
        <?php $no++; } ?>
    </tbody>
</table>