<?php
error_reporting(0);
header("Content-Type: application/xls");    
if ($sess == 8) {
    header("Content-Disposition: attachment; filename=DATA_MHS_".$prod->prodi.".xls");
} else {
    header("Content-Disposition: attachment; filename=data_mhs_s1.xls");
}   
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
                    
<table>
	<thead>
        <tr> 
        	<th>No</th>
            <th>ID Registrasi</th>
            <th>Nama</th>
            <th>Keterangan</th>
            <th>NIK</th>
            <th>Tanggal Lahir</th>
            <th>Jenis Kelamin</th>
            <th>Jenis Pendaftaran</th>
            <th>Kelas</th>
            <th>Telepon</th>
            <th>Telepon wali</th>
            <th>Alamat</th>
            <th>Pekerjaan</th>
            <th>Nama Ibu</th>
            <th>Agama</th>
            <th>Kota Asal Sekolah</th>
            <th>Pendidikan Ayah</th>
            <th>Pendidikan Ibu</th>
            <th>Pekerjaan Ayah</th>
            <th>Pekerjaan Ibu</th>
            <th>Asal Sekolah</th>
            <th>Kategori Sekolah</th>
            <th>Jenis Sekolah</th>
            <th>Penghasilan</th>
            <th>Prodi</th>
            <th>Kampus</th>
            <th width="120">Gelombang</th>
            <th>Status</th>
            <th>NPM</th>
            <th>Tahun Lulus</th>
            <?php if ($year > 2017) { ?>
                <th>Email</th>
            <?php } ?>
            
        </tr>
    </thead>
    <tbody>
		<?php $no = 1; foreach($wow as $row) { ?>
        <tr>
        	<td><?php echo $no; ?></td>
            <td><?php echo $row->nomor_registrasi; ?></td>
        	<td><?php echo $row->nama; ?></td>

            <?php if ($row->keterangan == 1) {
                $anggota = 'POLISI / PNS POLRI';
            } elseif ($row->keterangan == 2){
                $anggota = 'KELUARGA POLISI';
            } else{
                $anggota = 'Umum';
			}
             ?>
            <td><?= $anggota ?></td>
            <td><?php echo $row->nik; ?></td>
            <td><?php echo $row->tgl_lahir; ?></td>
            <td><?php echo $row->kelamin; ?></td>
            <?php switch ($row->jenis_pmb) {
                case 'MB':
                    $jnis = 'Mahasiswa Baru';
                    break;
                case 'KV':
                    $jnis = 'Konversi';
                    break;
                case 'RM':
                    $jnis = 'Readmisi';
                    break;
            } ?>
            <td><?php echo $jnis; ?></td>
            <?php 
                if ($year > '2017') {
                    switch ($row->kelas) {
                        case 'PG':
                            $kls = 'PAGI';
                            break;
                        case 'SR':
                            $kls = 'SORE';
                            break;
                        case 'KY':
                            $kls = 'P2K';
                            break;
                    }
                } else {
                    switch ($row->kelas) {
                        case 'pg':
                            $kls = 'PAGI';
                            break;
                        case 'sr':
                            $kls = 'SORE';
                            break;
                        case 'ky':
                            $kls = 'P2K';
                            break;
                    }
                }
            ?>
            <td><?php echo $kls; ?></td>
            <td><?php echo $row->tlp; ?></td>
            <td>
                <?php if ($year == '2016') {
                    echo '-';
                } else {
                    echo $row->tlp2;
                } ?>
            </td>
            <td><?php echo $row->alamat; ?></td>

            <?php if ($year > '2017') { ?>
                <td><?php if($row->pekerjaan == '' || is_null($row->pekerjaan)) { echo "-"; } else { echo $row->pekerjaan; } ?> </td>
            <?php } else { ?>
                <td><?php if($row->perkerjaan == '' || is_null($row->perkerjaan)) { echo "-"; } else { echo $row->perkerjaan; } ?> </td>
            <?php } ?>

            <td><?php echo $row->nm_ibu; ?></td>
            
            <td>
                <?php 
                    if ($row->agama == 'ISL') {
                        echo 'Islam';
                    } elseif ($row->agama == 'PRT') {
                        echo 'Protestan';
                    } elseif ($row->agama == 'HND') {
                        echo 'Hindu';
                    } elseif ($row->agama == 'KTL') {
                        echo 'Katholik';
                    } elseif ($row->agama == 'BDH') {
                        echo 'Budha';  
                    }
                 ?>
            </td>
            
            <td><?php echo $row->kota_sch_maba; ?></td>
            <td>
                <?php 
                    if ($year > '2017') {
                        if ($row->didik_ayah == 'NSD') {
                            echo "Tidak Tamat SD";
                        } elseif($row->didik_ayah == 'YSD') {
                            echo "Tamat SD";
                        } elseif ($row->didik_ayah == 'SMP') {
                            echo "SLTP";
                        } elseif ($row->didik_ayah == 'SMA') {
                            echo "SLTA";
                        } elseif ($row->didik_ayah == 'DPL') {
                            echo "Diploma";
                        } elseif ($row->didik_ayah == 'SMD') {
                            echo "Sarjana Muda";
                        } elseif ($row->didik_ayah == 'SRJ') {
                            echo "Sarjana";
                        } elseif ($row->didik_ayah == 'PSC') {
                            echo "Pasca Sarjana";
                        } else {
                            echo "Doctor";
                        }
                    } else {
                        if ($row->didik_ayah == 'TSD') {
                            echo "Tidak Tamat SD";
                        } elseif($row->didik_ayah == 'SD') {
                            echo "Tamat SD";
                        } elseif ($row->didik_ayah == 'SLTP') {
                            echo "SLTP";
                        } elseif ($row->didik_ayah == 'SLTA') {
                            echo "SLTA";
                        } elseif ($row->didik_ayah == 'D') {
                            echo "Diploma";
                        } elseif ($row->didik_ayah == 'SM') {
                            echo "Sarjana Muda";
                        } elseif ($row->didik_ayah == 'S') {
                            echo "Sarjana";
                        } elseif ($row->didik_ayah == 'PSC') {
                            echo "Pasca Sarjana";
                        } else {
                            echo "Doctor";
                        }
                    }
                    
                ?>
            </td>
            <td>
                <?php 
                    if ($year > '2017') {
                        if ($row->didik_ibu == 'NSD') {
                            echo "Tidak Tamat SD";
                        } elseif($row->didik_ibu == 'YSD') {
                            echo "Tamat SD";
                        } elseif ($row->didik_ibu == 'SMP') {
                            echo "SLTP";
                        } elseif ($row->didik_ibu == 'SMA') {
                            echo "SLTA";
                        } elseif ($row->didik_ibu == 'DPL') {
                            echo "Diploma";
                        } elseif ($row->didik_ibu == 'SMD') {
                            echo "Sarjana Muda";
                        } elseif ($row->didik_ibu == 'SRJ') {
                            echo "Sarjana";
                        } elseif ($row->didik_ibu == 'PSC') {
                            echo "Pasca Sarjana";
                        } else {
                            echo "Doctor";
                        }
                    } else {
                        if ($row->didik_ibu == 'TSD') {
                            echo "Tidak Tamat SD";
                        } elseif($row->didik_ibu == 'SD') {
                            echo "Tamat SD";
                        } elseif ($row->didik_ibu == 'SLTP') {
                            echo "SLTP";
                        } elseif ($row->didik_ibu == 'SLTA') {
                            echo "SLTA";
                        } elseif ($row->didik_ibu == 'D') {
                            echo "Diploma";
                        } elseif ($row->didik_ibu == 'SM') {
                            echo "Sarjana Muda";
                        } elseif ($row->didik_ibu == 'S') {
                            echo "Sarjana";
                        } elseif ($row->didik_ibu == 'PSC') {
                            echo "Pasca Sarjana";
                        } else {
                            echo "Doctor";
                        }
                    }
                    
                ?>
            </td>
            <td>
                <?php 
                    if ($year > '2017') {
                        if ($row->workdad == 'PN') {
                            echo "Pegawai Negeri";
                        } elseif($row->workdad == 'TP') {
                            echo "TNI / POLRI";
                        } elseif ($row->workdad == 'PS') {
                            echo "Pegawai Swasta";
                        } elseif ($row->workdad == 'WU') {
                            echo "Wirausaha";
                        } elseif ($row->workdad == 'PE') {
                            echo "Pensiun";
                        } elseif ($row->workdad == 'TK') {
                            echo "Tidak Bekerja";
                        } elseif ($row->workdad == 'LL') {
                            echo "Lain-lain";
                        } 
                    } else {
                        if ($row->workdad == 'PN') {
                            echo "Pegawai Negeri";
                        } elseif($row->workdad == 'TP') {
                            echo "TNI / POLRI";
                        } elseif ($row->workdad == 'PS') {
                            echo "Pegawai Swasta";
                        } elseif ($row->workdad == 'WU') {
                            echo "Wirausaha";
                        } elseif ($row->workdad == 'PSN') {
                            echo "Pensiun";
                        } elseif ($row->workdad == 'TK') {
                            echo "Tidak Bekerja";
                        } elseif ($row->workdad == 'LL') {
                            echo "Lain-lain";
                        } 
                    }
                ?>
            </td>
            <td>
                <?php 
                    if ($year > '2017') {
                        if ($row->workmom == 'PN') {
                            echo "Pegawai Negeri";
                        } elseif($row->workmom == 'TP') {
                            echo "TNI / POLRI";
                        } elseif ($row->workmom == 'PS') {
                            echo "Pegawai Swasta";
                        } elseif ($row->workmom == 'WU') {
                            echo "Wirausaha";
                        } elseif ($row->workmom == 'PE') {
                            echo "Pensiun";
                        } elseif ($row->workmom == 'TK') {
                            echo "Tidak Bekerja";
                        } elseif ($row->workmom == 'LL') {
                            echo "Lain-lain";
                        } 
                    } else {
                        if ($row->workmom == 'PN') {
                            echo "Pegawai Negeri";
                        } elseif($row->workmom == 'TP') {
                            echo "TNI / POLRI";
                        } elseif ($row->workmom == 'PS') {
                            echo "Pegawai Swasta";
                        } elseif ($row->workmom == 'WU') {
                            echo "Wirausaha";
                        } elseif ($row->workmom == 'PSN') {
                            echo "Pensiun";
                        } elseif ($row->workmom == 'TK') {
                            echo "Tidak Bekerja";
                        } elseif ($row->workmom == 'LL') {
                            echo "Lain-lain";
                        } 
                    }
                ?>
            </td>
            <td><?php echo $row->asal_sch_maba; ?></td>
            <?php if ($row->kategori_skl == 'NGR') {
                $jadi = 'Negeri';
            } else {
                $jadi = 'Swasta';
            }
             ?>
            <td><?php echo $jadi;?></td>
            <?php if ($row->jenis_sch_maba == 'MDA') {
                $jenis_sekolah = 'MA';
            } else {
                $jenis_sekolah = $row->jenis_sch_maba;
            }
             ?>
            <th><?php echo $jenis_sekolah ?></th>
            <td>
                <?php if ($row->penghasilan == 1) {
                    echo "Rp 1,000,000 - 2,000,000";
                } elseif($row->penghasilan == 2) {
                    echo "Rp 2,100,000 - 4,000,000";
                } elseif ($row->penghasilan == 3) {
                    echo "Rp 4,100,000 - 5,999,000";
                } elseif ($row->penghasilan == 4) {
                    echo ">= Rp 6,000,000";
                } else {
                    echo "0";
                }
                ?>
            </td>
            <td>
                <?php
                switch ($row->prodi) {
                    case '55201':
                        $nama_prodi = 'Teknik Informatika'; 
                        break;
                    case '25201':
                        $nama_prodi = 'Teknik Lingkungan'; 
                        break;
                    case '26201':
                        $nama_prodi = 'Teknik Industri'; 
                        break;
                    case '24201':
                        $nama_prodi = 'Teknik Kimia'; 
                        break;
                    case '32201':
                        $nama_prodi = 'Teknik Perminyakan'; 
                        break;
                    case '62201':
                        $nama_prodi = 'Akuntansi'; 
                        break;
                    case '61201':
                        $nama_prodi = 'Manajemen'; 
                        break;
                    case '73201':
                        $nama_prodi = 'Psikologi'; 
                        break;
                    case '74201':
                        $nama_prodi = 'Ilmu Hukum'; 
                        break;
                    case '70201':
                        $nama_prodi = 'Ilmu Komunikasi'; 
                        break;
                    case '86206':
                        $nama_prodi = 'PENDIDIKAN GURU SEKOLAH DASAR'; 
                        break;
                    case '85202':
                        $nama_prodi = 'PENDIDIKAN KEPELATIHAN OLAHRAGA'; 
                        break;
                }
                echo $nama_prodi;
                ?>
            </td>
            <?php if ($row->kampus == 'bks') {
                $camps = 'Bekasi';
            } else {
                $camps = 'Jakarta';
            }
             ?>
            <td><?php echo $camps; ?></td>
            <td><?php echo $row->gelombang; ?></td>
            <td>
                <?php
                    if ($year > '2017') {
                        if (is_null($row->status_lulus)) {
                            echo "Registrasi";
                        } elseif($row->status_lulus == 1) {
                            echo "Lulus Tes";
                        } elseif ($row->status_lulus > 1) {
                            echo "Daftar Ulang";
                        }
                    } else {
                        if ($row->status < 1) {
                            echo "Registrasi";
                        } elseif($row->status == 1) {
                            echo "Lulus Tes";
                        } elseif ($row->status > 1) {
                            echo "Daftar Ulang";
                        }
                    }
                ?>
            </td>
            <td><?php echo $row->npm_baru; ?></td>
            <td><?php echo $row->lulus_maba; ?></td>

            <?php if ($year > 2017) { ?>
                <td><?php echo getEmailPmb($row->user_input); ?></td>
            <?php } ?>
            
        </tr>
		<?php $no++; } ?>
    </tbody>
</table>