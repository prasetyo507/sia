<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_daftar_ulang_maba.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 1px solid black;
}



th {
    background-color: blue;
    color: black;
}
</style>

<table border="3">
    <thead>
        <tr> 
            <th rowspan="2">Fakultas</th>
            <th colspan="3">Gelombang I</th>
            <th colspan="3">Gelombang II</th>
            <th colspan="3">Gelombang III</th>
            <th colspan="3">Gelombang IV</th>
            <th colspan="3">Gelombang Ekstra</th>
            <th colspan="3">Jumlah</th>
            <th rowspan="2">Jumlah Total</th>
        </tr>
       
        <tr>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
        </tr>
    </thead>
    <tbody>
        
         <?php
            $t_pg1 = 0;
            $t_sr1 = 0;
            $t_ky1 = 0;

            $t_pg2 = 0;
            $t_sr2 = 0;
            $t_ky2 = 0;

            $jml_by_prodi_all= 0;

        foreach ($fakultas as $isi) { ?>
            
        <tr>
            <td style="color:#000000; background-color:#ffff00"><b><?php echo $isi->fakultas; ?></b></td>
            <td colspan="19" style="background-color:#ffff00"></td>
        </tr>

        <?php $q = $this->db->query('select * from tbl_jurusan_prodi where kd_fakultas = '.$isi->kd_fakultas.'')->result(); ?>
        
            <?php 

            $this->dbreg = $this->load->database('regis', TRUE);

            $years = substr($tahun, 2,4);

            if ($tahun == '2016') {
                $park = [$this->db,'tbl_form_camaba_2016','tbl_pmb_s2_2016','nomor_registrasi','ID_registrasi','opsi_prodi_s2','status > 1','ID_registrasi like "'.$years.'%"'];
                if ($ss == 'S2') {
                    $kels = ['pg','sr','KR'];
                } else {
                    $kels = ['pg','sr','ky'];
                }
            } elseif ($tahun == '2017') {
                $park = [$this->db,'tbl_form_camaba','tbl_pmb_s2','nomor_registrasi','ID_registrasi','opsi_prodi_s2','status > 1','nomor_registrasi like "'.$years.'%"'];
                if ($ss == 'S2') {
                    $kels = ['pg','sr','KR'];
                } else {
                    $kels = ['pg','sr','ky'];
                }
            } elseif ($tahun > '2017') {
                $park = [$this->dbreg,'tbl_form_pmb','tbl_form_pmb','nomor_registrasi','nomor_registrasi','prodi','status > 2','nomor_registrasi like "'.$years.'%"'];
                $kels = [strtoupper('pg'),strtoupper('sr'),strtoupper('ky')];
            }
           
            foreach ($q as $rows) { ?>

                

                <tr>
                    <td><b><?php echo $rows->prodi; ?></b></td>
                    <?php if ($ss == 'S1') { ?>
                        <?php if ($kampus == 'bks'){ ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].' ')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].' ')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].' ')->row();
                                        } else {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].' ')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].' ')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].' ')->row();
                                        }
                                    $b++;}
                                
                            } else { ?>
                            
                                    <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                } else { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                }
                            } 
                        
                        } elseif ($kampus == 'jkt') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        } else {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        }
                                    $b++;}
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                } else { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].' ')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].' ')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                }
                                
                            } 
                            
                        } elseif ($kampus == 'ALL') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        } else {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        }
                                    $b++;}
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                } else { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                }
                            }
                        } 
                        
                    } elseif($ss == 'S2') { ?>
                        <?php if ($kampus == 'bks'){ ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        } else {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        }
                                    $b++;}
                                
                            } else { ?>
                            
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                } else { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                }
                            }
                        } elseif ($kampus == 'jkt') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        } else {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        }
                                    $b++;}
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                } else { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                }
                                
                            }
                            
                        } elseif ($kampus == 'ALL') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        } else {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        }
                                    $b++;}
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                } else { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                }
                            }
                        }

                    } elseif($ss == 'ALL') { ?>

                        <?php if ($isi->kd_fakultas == 6) { ?>

                            <?php if ($kampus == 'bks') { ?>
                                <?php if ($gel == 'ALL') { $b = 0;
                                        for ($i=1; $i <= $jumlahgel; $i++) { 
                                            if ($jenis == 'ALL') {
                                                $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                                $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                                $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            } else {
                                                $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                                $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                                $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            }
                                            $b++;
                                        }
                                    
                                    } else { ?>
                        
                                        <?php if ($jenis == 'ALL') { ?>
                                            <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                            <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                            <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                        } else { ?>
                                            <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                            <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                            <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "bks" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                        }
                                    }

                        } elseif ($kampus == 'jkt') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        } else {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        }
                                    $b++;}
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                } else { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE kampus = "jkt" AND '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                }
                                
                            }
                            
                        } elseif ($kampus == 'ALL') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        } else {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        }
                                    $b++;}
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                } else { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[4].') AS jml FROM '.$park[2].' WHERE '.$park[5].' = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                }
                            }
                        }
                                
                            } else { ?>
                                <?php if ($kampus == 'bks'){ ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        } else {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        }
                                    $b++;}
                                
                            } else { ?>
                            
                                    <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                } else { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                }
                            } 
                        
                        } elseif ($kampus == 'jkt') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        } else {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        }
                                    $b++;}
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                } else { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                }
                                
                            } 
                            
                        } elseif ($kampus == 'ALL') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        } else {
                                            $pg[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $sr[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                            $ky[$b] = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'" and '.$park[6].' and '.$park[7].'')->row();
                                        }
                                    $b++;}
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and '.$park[6].' and '.$park[7].'')->row();
                                } else { ?>
                                    <?php $pg1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[0].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $sr1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[1].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();?>
                                    <?php $ky1 = $park[0]->query('SELECT count('.$park[3].') AS jml FROM '.$park[1].' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "'.$kels[2].'" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" and '.$park[6].' and '.$park[7].'')->row();
                                }
                            }
                        } 
                            }
                    } ?>
                    

                        
                        
                        <?php if ($gel == '1') { ?>
                            <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                            <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php } elseif($gel == '2') { ?>
                            <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                            <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php } elseif($gel == '3') { ?>
                            <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                            <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php } elseif($gel == '4') { ?>
                            <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                            <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php } elseif($gel == '5') { ?>
                            <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                            <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php } elseif ($gel == 'ALL') { ?>
                            <?php $jml_by_prodi = 0; $z=0; $b = 0; $pgall = 0; $srall = 0; $kyall = 0; for ($i=1; $i <= $jumlahgel; $i++) { ?>
                                <td><?php echo $pg[$b]->jml; $t_pg[$b]=$t_pg[$b]+$pg[$b]->jml; ?></td>
                                <td><?php echo $sr[$b]->jml; $t_sr[$b]=$t_sr[$b]+$sr[$b]->jml; ?></td>
                                <td><?php echo $ky[$b]->jml; $t_ky[$b]=$t_ky[$b]+$ky[$b]->jml; ?></td>
                            <?php $z = $pg[$b]->jml + $sr[$b]->jml + $ky[$b]->jml; $jml_by_prodi = $jml_by_prodi+$z; $pgall = $pgall + $pg[$b]->jml; $srall = $srall + $sr[$b]->jml; $kyall = $kyall + $ky[$b]->jml; $b++; } ?>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pgall; ?></td>
                            <td><?php echo $srall; ?></td>
                            <td><?php echo $kyall; ?></td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php }?>
                    
                </tr>

               
            <?php } ?>
            
            
            
        <?php } ?> 
        <tr>
            <td><b>JUMLAH</b></td>
                <?php if ($gel == '1') { ?>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } elseif($gel == '2') { ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } elseif($gel == '3') { ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } elseif($gel == '4') { ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } elseif($gel == '5') { ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } elseif($gel == 'ALL') { ?>
                   <?php $jml_by_prodi = 0; $z=0; $b = 0; $pgall = 0; $srall = 0; $kyall = 0; for ($i=1; $i <= $jumlahgel; $i++) { ?>
                        <td><?php echo $t_pg[$b]; ?></td>
                        <td><?php echo $t_sr[$b]; ?></td>
                        <td><?php echo $t_ky[$b]; ?></td>
                    <?php $z = $t_pg[$b] + $t_sr[$b] + $t_ky[$b]; $jml_by_prodi = $jml_by_prodi+$z; $pgall = $pgall + $t_pg[$b]; $srall = $srall + $t_sr[$b]; $kyall = $kyall + $t_ky[$b]; $b++; } ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $pgall; ?></td>
                    <td><?php echo $srall; ?></td>
                    <td><?php echo $kyall; ?></td>
                    <td><?php echo $jml_by_prodi; ?></td>
                <?php } ?>
                        
        </tr> 
    </tbody>
</table>