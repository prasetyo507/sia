<script type="text/javascript">

	/* ajax untuk cek email */
	function emailOnBlur() {
		var email = $("#email").val();
		$.ajax({
			url: '<?= base_url('pmb/forms1/cek_email/'); ?>',
			type: 'POST',
			data: {email:email},
			success: function(e){
				if (e == 1) {
					$("#email").val('');
					$("#foralert").html('<div id="showalert" class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>E-Mail Sudah Pernah Didaftarkan! Gunakan E-Mail Lain!</div>');
				} else {
					$("#showalert").remove();
				}
			}
		});
	}

$(document).ready(function() {

$('#kwn').hide();
$('#stsb_txt').hide();
$('#bpjs-yes').hide();

// $('#readmisi1').hide();
// $('#readmisi2').hide();

// $('#new1').hide();
// $('#new2').hide();
// $('#new3').hide();

// $('#konversi1').hide();
// $('#konversi2').hide();
// $('#konversi3').hide();
// $('#konversi4').hide();
// $('#konversi5').hide();

$('#konversi').hide();
$('#new0').hide();
$('#readmisi').hide();


$('#spd').hide();
$('#sak').hide();
$('#tkr').hide(); 
$('#baa').hide();
$('#ketren').hide();

$('#k').click(function () {
	$('#spd').show();
	$('#sak').show();
	$('#tkr').show();
	$('#skhun').hide();
	$('#skl').hide();
	$('#rpt').hide();
});

$('#r').click(function () {
	$('#baa').show();
	$('#tkr').show(); 
	$('#ketren').show();
	$('#ijz').hide();
	$('#skhun').hide();
	$('#skl').hide();
	$('#rpt').hide();
});

$('#new').click(function () {
	$('#spd').hide();
	$('#sak').hide();
	$('#tkr').hide();
	$('#baa').hide();
	$('#ketren').hide();
	$('.dda').show();
});



$('#r').click(function () {
    $('#konversi').hide();
	$('#new0').hide();
	$('#readmisi').show();
});
$('#new').click(function () {
	$('#konversi').hide();
	$('#new0').show();
	$('#readmisi').hide();
});
$('#k').click(function () {
	$('#konversi').show();
	$('#new0').hide();
	$('#readmisi').hide();
});



$('#tgl_lahir').datepicker({
	dateFormat: "yy-mm-dd",

	yearRange: "1945:2016",

	changeMonth: true,

	changeYear: true

});



$('#bpjs-y').click(function () {
	$('#bpjs-yes').show();
});	

$('#bpjs-n').click(function () {
	$('#bpjs-yes').hide();
});	

$('#wni').click(function () {
	$('#kwn').hide();
});

$('#wna').click(function () {
	$('#kwn').show();
});

$('#stsb_n').click(function () {
	$('#stsb_txt').hide();
});

$('#stsb_y').click(function () {
	$('#stsb_txt').show();
});
});

</script>

<div class="row">
	<div class="span12" id="form_pmb">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Form Mahasiswa Baru</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/forms1/simpan_form" method="post">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Jenis Form</label>
							<div class="controls">
								<select class="form-control span2"  name="jenis" required>
									<option disabled="" selected="">--Pilih Form--</option>
									<!-- <option value="RM"  id="r">Readmisi</option> -->
									<option value="MB" id="new">Mahasiswa Baru</option>
									<option value="KV"  id="k">Mahasiswa Konversi</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pilihan Kampus</label>
							<div class="controls">
								<select class="form-control span2"  name="kampus" required>
									<option disabled="" selected="">--Pilih Kampus--</option>
									<option value="jkt">Jakarta</option>
									<option value="bks">Bekasi</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pilihan Kelas</label>
							<div class="controls">
								<select class="form-control span2"  name="kelas" required>
									<option disabled selected>--Pilih Kelas--</option>
									<option value="PG">Pagi</option>
									<option value="SR">Sore</option>
									<option value="KY">Karyawan</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Keterangan</label>
							<div class="controls">
								<select class="form-control span2"  name="ket" required>
									<option value="1">POLISI / PNS POLRI</option>
									<option value="2">KELUARGA POLISI</option>
									<option value="0">UMUM</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">NIK</label>
							<div class="controls">
								<input type="text" class="form-control span6" placeholder="Nomor Induk Kependudukan" name="nik" maxlength=16 minlength=16 required><br>
								<small>*sesuai ktp / kartu keluarga</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama</label>
							<div class="controls">
								<input type="text" class="form-control span6" placeholder="Isi dengan nama calon mahasiswa"  name="nama" required><br>
								<small>*nama sesuai ijazah/akte kelahiran</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Jenis Kelamin</label>
							<div class="controls">
								<input type="radio" name="jk" value="L" required> Laki - Laki <br>
								<input type="radio" name="jk" value="P"> Perempuan
							</div>
						</div>

						
						<div class="control-group">
							<label class="control-label">Kewarganegaraan</label>
							<div class="controls">
								<input type="radio" name="wn" id="wni" onclick="wni()" value="WNI" required> WNI <br>
								<input type="radio" name="wn" id="wna" onclick="wna()" value="WNA"> WNA  &nbsp;&nbsp; 
								<input  type="text" class="form-control span3" id="kwn"  name="wn_txt" placeholder="Kewarganegaraan Calon Mahasiswa">
							</div>
						</div>
				
						<div class="control-group">
							<label class="control-label">Kelahiran</label>
							<div class="controls">
								<input type="text" class="form-control span2" placeholder="Tempat Lahir" name="tpt_lahir"  required>
								<input type="text" class="form-control span2" placeholder="Tanggal Lahir" id="tgl_lahir" name="tgl_lahir"  required>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Agama</label>
							<div class="controls">
								<select class="form-control span2"  name="agama" required>
									<option disabled="" selected="">-- PILIH AGAMA --</option>
									<option value="ISL">Islam</option>
									<option value="KTL">Katolik</option>
									<option value="PRT">Protestan</option>
									<option value="BDH">Budha</option>
									<option value="HND">Hindu</option>
									<option value="OTH">Lainnya</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Status Nikah</label>
							<div class="controls">
								<input type="radio" name="stsm" value="Y" > Menikah  <br>
								<input type="radio" name="stsm" value="N"> Belum Menikah
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label">Status Kerja</label>
							<div class="controls">
								<input type="radio" name="stsb" id="stsb_n" value="N" required> Belum Bekerja <br>
								<input type="radio" name="stsb" id="stsb_y" value="Y"> Bekerja &nbsp;&nbsp; 
								<input type="text" class="form-control span3" id="stsb_txt"  name="stsb_txt" placeholder="Profesi pekerjaan anda">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Alamat</label>
							<div class="controls">
								<!-- <textarea class="form-control span4" type="text"  name="alamat" required></textarea> -->
								
								<input class="form-control span3" placeholder="Jalan" type="text"  name="jalan" required>
								<input class="form-control span2" placeholder="RT" type="text"  name="rt" required>
								<input class="form-control span2" placeholder="RW" type="text"  name="rw" required>
								<input class="form-control span3" placeholder="Perumahan" type="text"  name="perum" required>

							</div>
						</div>
						<div class="control-group">
							<label class="control-label"></label>
							<div class="controls">
								<!-- <textarea class="form-control span4" type="text"  name="alamat" required></textarea> -->
								<input class="form-control span4" placeholder="Kelurahan" type="text"  name="lurah" required>
								<input class="form-control span4" placeholder="Kecamatan" type="text"  name="camat" required>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kode Pos</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan Kode Pos" type="text"  name="kdpos" required>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">No. Telpon / HP</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan Nomer Telpon" type="text"  name="tlp" maxlength=13 minlength=10 required>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">No. Telpon / HP Wali</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan Nomer Telpon" type="text"  name="tlp2" maxlength=13 minlength=10>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">E-Mail</label>
							<div class="controls">
								<input class="form-control span3" id="email" onblur="emailOnBlur()" placeholder="Isi dengan E-Mail aktif" type="email"  name="email" required="">
								<br><br>
								<div id="foralert"></div>
							</div>
						</div>

						<!-- readmisi START -->
						<div id="readmisi">
						<div class="control-group" id="readmisi1">
							<label class="control-label">NPM Lama</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan Nomor Pokok Mahasiswa Lama"  type="text"  name="npm_readmisi">
							</div>
						</div>
						<div class="control-group" id="readmisi2">
							<label class="control-label">Tahun Masuk di UBJ</label>
							<div class="controls">
								<input class="form-control span1" type="number"  name="thmasuk_readmisi"> &nbsp; Sampai dengan semester &nbsp;
								<input class="form-control span1" type="text"  name="smtr_readmisi">
							</div>
						</div>
						</div>
						<!-- readmisi END -->

						<!-- NEW START -->
						<div id="new0">
						<div class="control-group" id="new1">
							<label class="control-label">Asal Sekolah</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan Asal Sekolah Calon Mahasiswa"  type="text"  name="asal_sch" >
							</div>
						</div>
						<div class="control-group" id="new1">
							<label class="control-label">NISN</label>
							<div class="controls">
								<input class="form-control span3" placeholder="NISN Calon Mahasiswa"  type="text"  name="nisn">
							</div>
						</div>
						<div class="control-group" id="new2">
							<label class="control-label">Kota Asal Sekolah</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan kota Asal Sekolah "  type="text"  name="kota_sch">
							</div>
						</div>
						<div class="control-group" id="new2">
							<label class="control-label">Kelurahan Asal Sekolah</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan Kelurahan Asal Sekolah "  type="text"  name="daerah_sch" >
							</div>
						</div>
						<div class="control-group" id="new4">
							<label class="control-label">Jenis Sekolah</label>
							<div class="controls">
								<input type="radio" name="jenis_skl" value="NGR" > NEGERI &nbsp;&nbsp;
								<input type="radio" name="jenis_skl" value="SWT" > SWASTA &nbsp;&nbsp; 
							</div>
						</div>
						<div class="control-group" id="new4">
							<label class="control-label"></label>
							<div class="controls">
								<input type="radio" name="jenis_sch_maba" value="SMA" > SMA &nbsp;&nbsp;
								<input type="radio" name="jenis_sch_maba" value="SMK" > SMK &nbsp;&nbsp; 
								<input type="radio" name="jenis_sch_maba" value="MDA" > MA  &nbsp;&nbsp;
								<input type="radio" name="jenis_sch_maba" value="SMB" > SMTB  &nbsp;&nbsp;
								<input type="radio" name="jenis_sch_maba" value="OTH" > Lainnya  
							</div>
						</div>
						<div class="control-group" id="new3">
							<label class="control-label">Jurusan</label>
							<div class="controls">
								<input class="form-control span4" type="text"  name="jur_sch"> &nbsp;&nbsp; Tahun Lulus &nbsp;&nbsp;
								<input class="form-control span1" type="text"  name="lulus_sch">
							</div>
						</div>
						</div>
						<!-- NEW END -->

						<!-- KONVERSI START -->
						<div id="konversi">
						<div class="control-group" id="konversi1">
							<label class="control-label">Nama Perguruan Tinggi</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan Asal Sekolah Calon Mahasiswa"  type="text"  name="asal_pts">
							</div>
						</div>
						<div class="control-group" id="konversi4">
							<label class="control-label">Kota PTN/PTS</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan kota PTS/PTN "  type="text"  name="kota_pts">
							</div>
						</div>
						<div class="control-group" id="konversi2">
							<label class="control-label">Program Studi</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan Program Studi "  type="text"  name="prodi_pts" >
							</div>
						</div>
						<div class="control-group" id="konversi3">
							<label class="control-label">Tahun Lulus / Semester</label>
							<div class="controls">
								<input class="form-control span1" placeholder="Tahun" type="text"  name="lulus_pts">  / 
								<input class="form-control span1" placeholder="Semester " type="text"  name="smtr_pts">
							</div>
						</div>
						
						<div class="control-group" id="konversi5">
							<label class="control-label">NPM/NIM Asal</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan kota PTS/PTN "  type="text"  name="npm_pts">
							</div>
						</div>
						</div>
						<!-- KONVERSI END -->

						<div class="control-group">
							<label class="control-label">Pilih Program Studi</label>
							<div class="controls">
								<select class="form-control span3"  name="prodi" required>
									<option disabled="" selected="">-- PILIH PROGRAM STUDI--</option>
									 <?php foreach ($fakultas as $isi) { ?>
			                            <optgroup label="<?php echo $isi->fakultas ?>"></optgroup>
			                            <?php $jur = $this->db->where('kd_fakultas', $isi->kd_fakultas)->get('tbl_jurusan_prodi')->result();
			                            foreach ($jur as $key) { ?>
			                              <option value="<?php echo $key->kd_prodi ?>"><?php echo $key->prodi ?></option>
			                            <?php }?>
			                          <?php } ?>
									
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Pilih Program Studi Peminatan I</label>
							<div class="controls">
								<select class="form-control span3"  name="prodi2" required>
									<option disabled="" selected="">-- PILIH PROGRAM STUDI--</option>
									 <?php foreach ($fakultas as $isi) { ?>
			                            <optgroup label="<?php echo $isi->fakultas ?>"></optgroup>
			                            <?php $jur = $this->db->where('kd_fakultas', $isi->kd_fakultas)->get('tbl_jurusan_prodi')->result();
			                            foreach ($jur as $key) { ?>
			                              <option value="<?php echo $key->kd_prodi ?>"><?php echo $key->prodi ?></option>
			                            <?php }?>
			                          <?php } ?>
									
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Pilih Program Studi Peminatan II</label>
							<div class="controls">
								<select class="form-control span3"  name="prodi3" required>
									<option disabled="" selected="">-- PILIH PROGRAM STUDI--</option>
									 <?php foreach ($fakultas as $isi) { ?>
			                            <optgroup label="<?php echo $isi->fakultas ?>"></optgroup>
			                            <?php $jur = $this->db->where('kd_fakultas', $isi->kd_fakultas)->get('tbl_jurusan_prodi')->result();
			                            foreach ($jur as $key) { ?>
			                              <option value="<?php echo $key->kd_prodi ?>"><?php echo $key->prodi ?></option>
			                            <?php }?>
			                          <?php } ?>
									
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Pilih Program Studi Peminatan III</label>
							<div class="controls">
								<select class="form-control span3"  name="prodi4" required>
									<option disabled="" selected="">-- PILIH PROGRAM STUDI--</option>
									 <?php foreach ($fakultas as $isi) { ?>
			                            <optgroup label="<?php echo $isi->fakultas ?>"></optgroup>
			                            <?php $jur = $this->db->where('kd_fakultas', $isi->kd_fakultas)->get('tbl_jurusan_prodi')->result();
			                            foreach ($jur as $key) { ?>
			                              <option value="<?php echo $key->kd_prodi ?>"><?php echo $key->prodi ?></option>
			                            <?php }?>
			                          <?php } ?>
									
								</select>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label">Nama Ayah</label>
							<div class="controls">
								<input class="form-control span4" type="text" placeholder="Isi Nama Ayah Calon Mahasiswa"  name="nm_ayah" required>
								<select class="form-control span2"  name="didik_ayah">
									<option disabled="" selected="">-- Pendidikan Ayah --</option>
									<option value="NSD">Tidak tamat SD</option>
									<option value="YSD">Tamat SD</option>
									<option value="SMP">Tamat SLTP</option>
									<option value="SMA">Tamat SLTA</option>
									<option value="DPL">Diploma</option>
									<option value="SMD">Sarjana Muda</option>
									<option value="SRJ">Sarjana</option>
									<option value="PSC">Pascasarjana</option>
									<option value="DTR">Doctor</option>
									
								</select>
								<select class="form-control span2"  name="workdad">
									<option disabled="" selected="">-- Pekerjaan Ayah --</option>
									<option value="PN">Pegawai Negeri</option>
									<option value="TP">TNI / POLRI</option>
									<option value="PS">Pegawai Swasta</option>
									<option value="WU">Wirausaha</option>
									<option value="PE">Pensiun</option>
									<option value="TK">Tidak Bekerja</option>
									<option value="LL">Lain-lain</option>
								</select>
								<select class="form-control span2"  name="life_statdad">
									<option disabled="" selected="">-- Status Hidup --</option>
									<option value="MH">Masih Hidup</option>
									<option value="SM">Sudah Meninggal</option>
									
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama Ibu</label>
							<div class="controls">
								<input class="form-control span4" type="text" placeholder="Isi Nama Ibu Calon Mahasiswa"  name="nm_ibu" required>
								<select class="form-control span2"  name="didik_ibu">
									<option disabled="" selected="">-- Pendidikan Ibu --</option>
									<option value="NSD">Tidak tamat SD</option>
									<option value="YSD">Tamat SD</option>
									<option value="SMP">Tamat SLTP</option>
									<option value="SMA">Tamat SLTA</option>
									<option value="DPL">Diploma</option>
									<option value="SMD">Sarjana Muda</option>
									<option value="SRJ">Sarjana</option>
									<option value="PSC">Pascasarjana</option>
									<option value="DTR">Doctor</option>
								</select>
								<select class="form-control span2"  name="workmom">
									<option disabled="" selected="">-- Pekerjaan Ibu --</option>
									<option value="PN">Pegawai Negeri</option>
									<option value="TP">TNI / POLRI</option>
									<option value="PS">Pegawai Swasta</option>
									<option value="WU">Wirausaha</option>
									<option value="PE">Pensiun</option>
									<option value="TK">Tidak Bekerja</option>
									<option value="LL">Lain-lain</option>
								</select>
								<select class="form-control span2"  name="life_statmom">
									<option disabled="" selected="">-- Status Hidup --</option>
									<option value="MH">Masih Hidup</option>
									<option value="SM">Sudah Meninggal</option>
									
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Penghasilan Orang Tua</label>
							<div class="controls">
								<input type="radio" value="1" name="gaji" required>
								Rp 1,000,000 - 2,000,000 &nbsp;&nbsp;
							
								<input type="radio" value="2" name="gaji">
								Rp 2,100,000 - 4,000,000 <br>
							
								<input type="radio" value="3" name="gaji">
								Rp 4,100,000 - 5,999,000  &nbsp;&nbsp;
						
								<input type="radio" value="4" name="gaji">
								>= Rp 6,000,000
							</div>
						</div>
						<div class="control-group" id="">
							<label class="control-label">Pengguna BPJS </label>
							<div class="controls">
								<input type="radio" id="bpjs-y" name="bpjs" value="y" required> Ya &nbsp;&nbsp;
								<input type="radio" id="bpjs-n" name="bpjs" value="n" > Tidak &nbsp;&nbsp; 
							</div>
						</div>
						<div class="control-group" id="bpjs-yes">
							<label class="control-label">NO. BPJS</label>
							<div class="controls">
								<input class="form-control span3" placeholder="No. BPJS Calon Mahasiswa"  type="text"  name="nobpjs">
							</div>
						</div>
						<div class="control-group" id="bpjs-yes">
							<label class="control-label">Transportasi</label>
							<div class="controls">
								<select class="form-control span2"  name="transport">
									<option disabled="" selected="">-- Alat Transportasi --</option>
									<option value="MBL">Mobil</option>
									<option value="MTR">Motor</option>
									<option value="AKT">Angkutan Umum</option>
									<option value="SPD">Sepeda</option>
									<option value="JKK">Jalan Kaki</option>
									<option value="ADG">Andong</option>
									<option value="KRT">Kereta</option>
								</select>
							</div>
						</div>
						<div class="control-group" id="konversi5">
							<label class="control-label">Referensi </label>
							<div class="controls">
								<input class="form-control span6" placeholder="informasi mengenai ubhara diperoleh dari"  type="text"  name="refer">
							</div>
						</div>
						<div class="control-group" id="new4">
							<label class="control-label">Ukuran Almamater</label>
							<div class="controls">
								<select class="form-control span2"  name="sizealmet">
									<option disabled="" selected="">-- Pilih Ukuran --</option>
									<option value="S">S</option>
                                    <option value="M">M</option>
                                    <option value="L">L</option>
                                    <option value="X">XL</option>
                                    <option value="2">XXL</option>
                                    <option value="3">XXXL</option>
								</select> 
							</div>
						</div>
						<div class="control-group" id="new4">
							<label class="control-label">Kelengkapan Berkas</label>
							<div class="controls">
								<div class="dda" id="akt"><input type="checkbox" name="lengkap[]" value="AKT"> Akte Kelahiran &nbsp;&nbsp;</div>
								<div class="dda" id="kk"><input type="checkbox" name="lengkap[]" value="KK"> Kartu Keluarga (KK) &nbsp;&nbsp; </div>
								<div class="dda" id="ktp"><input type="checkbox" name="lengkap[]" value="KTP"> Kartu Tanda Penduduk (KTP)  &nbsp;&nbsp;</div>
								<div class="dda" id="rpt"><input type="checkbox" name="lengkap[]" value="RP"> Rapot  &nbsp;&nbsp;</div>
								<div class="dda" id="skhun"><input type="checkbox" name="lengkap[]" value="SKHUN"> SKHUN  </div>
								<div class="dda" id="foto"><input type="checkbox" name="lengkap[]" value="FT"> Foto (3x4 dan 4x6) &nbsp;&nbsp;</div>
								<div class="dda" id="ijz"><input type="checkbox" name="lengkap[]" value="IJZ"> Ijazah &nbsp;&nbsp; </div>
								<div class="dda" id="skl"><input type="checkbox" name="lengkap[]" value="SKL"> Surat Kelulusan  &nbsp;&nbsp;<br></div>
								
								<div id="spd"><input type="checkbox" name="lengkap[]" value="SPD"> Surat Pindah &nbsp;&nbsp;</div>
								<div id="sak"><input type="checkbox" name="lengkap[]" value="SAK"> Sertifikat Akreditasi &nbsp;&nbsp;</div>

								<div id="tkr"><input type="checkbox" name="lengkap[]" value="TKR"> Transkrip  &nbsp;&nbsp;</div>
								<div id="baa"><input type="checkbox" name="lengkap[]" value="LBAA"> Laporan BAA  &nbsp;&nbsp;</div>
								<div id="ketren"><input type="checkbox" name="lengkap[]" value="KTREN"> Keterangan RENKEU </div>

								<div class="dda" id="lkp"><input type="checkbox" name="lengkap[]" value="LLKP"> Lengkap Administratif  &nbsp;&nbsp;</div>
							</div>
						</div>
						<div class="form-actions">
							<input class="btn btn-large btn-primary" type="submit" value="Submit">
							<input class="btn btn-large btn-default" type="reset" value="Clear">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
