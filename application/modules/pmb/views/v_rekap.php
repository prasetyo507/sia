<script type="text/javascript">
	$(document).ready(function(){
		$('#prog').change(function(){
	    	$.post('<?php echo base_url()?>pmb/rekap/changejur/'+$(this).val(),{},function(get){
	      		$('#jurs').html(get);
	    	});
	 	});
	});
</script>

<?php 
	$tgl1 = '2016';
	$tgl2 = date('Y')+1;
 ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Rekapitulasi</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/rekap/load" method="post" enctype="multipart/form-data">
					<fieldset>

						<div class="control-group">
							<label class="control-label">Tahun</label>
							<div class="controls">
								<select class="form-control span4" name="tahun" required/>
									<option disabled="" selected="">--Pilih Tahun--</option>
									<?php for ($i = $tgl1; $i <= $tgl2 ; $i++) { ?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Jenis</label>
							<div class="controls">
								<select class="form-control span4" name="jns" required/>
									<option disabled="" selected="">--Pilih Jenis--</option>
									<option value="MB">Mahasiswa Baru</option>
									<option value="RM">Readmisi</option>
									<option value="KV">Konversi</option>
									<option value="AL">Semua</option>
								</select>
							</div>
						</div>

						<?php $sesi = $this->session->userdata('sess_login'); if ($sesi['id_user_group'] != 8) { ?>
							<div class="control-group">
								<label class="control-label">Program</label>
								<div class="controls">
									<select class="form-control span4" name="program" id="prog" />
										<option disabled="" selected="">--Pilih Program--</option>
										<option value="S1">S1</option>
										<option value="S2">S2</option>
									</select>
								</div>
							</div>
						<?php } ?>

						<?php $sesi = $this->session->userdata('sess_login');
						if ($sesi['id_user_group'] == 8) {
							if ($sesi['userid'] == '74101' || $sesi['userid'] == '62101') { ?>
							 	<input type="hidden" name="program" value="S2">
							<?php } else { ?>
							 	<input type="hidden" name="program" value="S1">
							<?php } 
						} else { ?>
							
						<?php } ?>
						
						

						<div class="control-group">
							<label class="control-label">Gelombang</label>
							<div class="controls">
								<select class="form-control span4" name="gelombang" required/>
									<option disabled="" selected="">--Pilih Gelombang--</option>
									<option value="ALL">Semua Gelombang</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">Ekstra</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Status</label>
							<div class="controls">
								<select class="form-control span4" name="status" required/>
									<option disabled="" selected="">--Pilih Status--</option>
									<option value="IS NULL">Daftar / Registrasi</option>
									<option value="= 1">Lulus Tes</option>
									<option value="= 2">Tidak Lulus Tes</option>
									<option value="> 2">Daftar Ulang</option>
									<option value="ALL">Semua</option>
								</select>
							</div>
						</div>

						<?php 
							$sesi = $this->session->userdata('sess_login');
							if ($sesi['id_user_group'] == 8) { ?>
								<input type="hidden" name="prodi" value="<?php echo $prods; ?>">
							<?php } elseif ($sesi['id_user_group'] == 9) { ?>
								<div class="control-group">
									<label class="control-label">Prodi</label>
									<div class="controls">
										<select class="form-control span4" id="jurs" name="prodi" required/>
											<option disabled="" selected="">--Pilih Prodi--</option>
											
										</select>
									</div>
								</div>
							<?php } else { ?>
								<div class="control-group">
									<label class="control-label">Prodi</label>
									<div class="controls">
										<select class="form-control span4" id="jurs" name="prodi" required/>
											<option disabled="" selected="">--Pilih Prodi--</option>
											
										</select>
									</div>
								</div>
							<?php }
						?>
						

						<div class="form-actions">
							<input class="btn btn-large btn-success" type="submit" value="Submit">
							<!-- <a class="btn btn-large btn-success" href="<?php echo base_url(); ?>pmb/data_maba/load">Submit</a> -->
						</div>

					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

