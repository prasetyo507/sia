<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Laporan Data Mahasiswa Baru</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/data_maba/load" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Tahun</label>
							<div class="controls">
								<select class="form-control span4" name="tahun" required/>
									<option disabled="" selected="">--Pilih Tahun--</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Kampus</label>
							<div class="controls">
								<select class="form-control span4" name="kampus" required/>
									<option disabled="" selected="">--Pilih Kampus--</option>
									<option value="ALL">Semua Kampus</option>
									<option value="jkt">Jakarta</option>
									<option value="bks">Bekasi</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Program</label>
							<div class="controls">
								<select class="form-control span4" name="program" required/>
									<option disabled="" selected="">--Pilih Program--</option>
									<option value="ALL">Semua Program</option>
									<option value="S1">S1</option>
									<option value="S2">S2</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Gelombang</label>
							<div class="controls">
								<select class="form-control span4" name="gelombang" required/>
									<option disabled="" selected="">--Pilih Gelombang--</option>
									<option value="ALL">Semua Gelombang</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">Ekstra</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Status</label>
							<div class="controls">
								<select class="form-control span4" name="status" required/>
									<option disabled="" selected="">--Pilih Status--</option>
									<option value="IS NULL">Daftar / Registrasi</option>
									<option value="=1">Lulus Tes</option>
									<option value=">2">Daftar Ulang</option>
									<option value="ALL">Semua</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Prodi</label>
							<div class="controls">
								<select class="form-control span4" name="prodi" required/>
									<option disabled="" selected="">--Pilih Prodi--</option>
									<option value="ALL">Semua Prodi</option>
									<?php foreach ($prods as $key) { ?>
										<option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="form-actions">
							<input class="btn btn-large btn-success" type="submit" value="Unduh">
							<!-- <a class="btn btn-large btn-success" href="<?php echo base_url(); ?>pmb/data_maba/load">Submit</a> -->
						</div>

					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

