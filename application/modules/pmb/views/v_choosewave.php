<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Daftar Mahasiswa Baru</h3>
			</div>

			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/peserta_pmb/pass_sess" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Tahun</label>
							<div class="controls">
								<select class="form-control span4" name="tahun" required/>
									<option disabled="" selected="">--Pilih Tahun--</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Program</label>
							<div class="controls">
								<select class="form-control span4" name="program" required/>
									<option disabled="" selected="">--Pilih Program--</option>
									<option value="S1">S1</option>
									<option value="S2">S2</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Gelombang</label>
							<div class="controls">
								<select class="form-control span4" name="gelombang" required/>
									<option disabled="" selected="">--Pilih Gelombang--</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">Ekstra</option>
								</select>
							</div>
						</div>

						<div class="form-actions">
							<input class="btn btn-large btn-success" type="submit" value="Submit">
							<!-- <a class="btn btn-large btn-success" href="<?php //echo base_url(); ?>pmb/data_maba/load">Submit</a> -->
						</div>

					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

