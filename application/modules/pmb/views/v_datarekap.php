<script type="text/javascript">
    function loads(id) {
        $("#konten").load('<?php echo base_url()?>pmb/regist/kelengkapan/'+id);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-file"></i>
  				<h3>Kelengkapan Data Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url(); ?>keuangan/status_ujian/input_data" method="post">
                        <!-- <a class="btn btn-success btn-large" href=""><i class="btn-icon-only icon-print"> Print</i></a> -->
                        <a class="btn btn-md btn-warning" href="<?php echo base_url(); ?>pmb/regist/destByPass"><i class="icon-chevron-left"></i> Kembali</a>
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th width="30">No</th>
                                    <th>No. Registrasi</th>
                                    <th>NPM</th>
                                    <th>Nama</th>
                                    <th>Prodi</th>
                                    <th width="50">Kelengkapan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($load as $row) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->nomor_registrasi; ?></td>
                                    <td><?php echo $row->npm_baru; ?></td>
                                    <td><?php echo $row->nama; ?></td>
                                    <td><?php echo get_jur($row->prodi); ?></td>
                                    <td style="text-align: center"><a href="#myModal" onclick="loads('<?php echo $row->nomor_registrasi; ?>')" data-toggle="modal" class="btn btn-sm btn-success" title=""><i class="icon-list"></i></a></td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="konten">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>