<?php
error_reporting(0);
header("Content-Type: application/xls");    
if ($sess == 8) {
    header("Content-Disposition: attachment; filename=DATA_MHS_".$prod->prodi.".xls");
} else {
    header("Content-Disposition: attachment; filename=data_mhs_s2.xls");
}  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
                    
<table>
	<thead>
        <tr> 
        	<th>No</th>
            <th>ID Registrasi</th>
            <th>Nama</th>
            <th>NIK</th>
            <th>Tanggal Lahir</th>
            <th>Jenis Kelamin</th>
            <th>Jenis Pendaftaran</th>
            <th>Agama</th>
            <th>Alamat</th>
            <th>Asal Universitas</th>
            <th>Pekerjaan</th>
            <th>Prodi</th>
            <th>Kampus</th>
            <th width="120">Gelombang</th>
            <th>Status</th>
            <th>NPM</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
		<?php $no = 1; foreach($wew as $row){?>
        <tr>
        	<td><?php echo $no; ?></td>
        	<td>
                <?php
                    if ($year > '2017') {
                        echo $row->nomor_registrasi;
                    } else {
                        echo $row->ID_registrasi;
                    }
                ?>
            </td>
            <td><?php echo $row->nama; ?></td>
            <td><?php echo $row->nik; ?></td>
            <td><?php echo $row->tgl_lahir; ?></td>
            <td><?php echo $row->kelamin; ?></td>
            <?php switch ($row->jenis_pmb) {
                case 'MB':
                    $jnis = 'Mahasiswa Baru';
                    break;
                case 'KV':
                    $jnis = 'Konversi';
                    break;
                case 'RM':
                    $jnis = 'Readmisi';
                    break;
            } ?>
            <td><?php echo $jnis; ?></td>
            <td><?php echo $row->agama; ?></td>
            <td><?php echo $row->alamat; ?></td>
            <td>
                <?php
                    if ($year > '2017') {
                        echo $row->asal_sch_maba; 
                    } else {
                        echo $row->nm_univ; 
                    }
                 ?>
            </td>
            <td>
                <?php 
                    if ($year > '2017') {
                        if ($row->pekerjaan == 'PN') {
                            echo "Pegawai Negeri";
                        } elseif($row->pekerjaan == 'TP') {
                            echo "TNI / POLRI";
                        } elseif ($row->pekerjaan == 'PS') {
                            echo "Pegawai Swasta";
                        } elseif ($row->pekerjaan == 'WU') {
                            echo "Wirausaha";
                        } elseif ($row->pekerjaan == 'PE') {
                            echo "Pensiun";
                        } elseif ($row->pekerjaan == 'TK') {
                            echo "Tidak Bekerja";
                        } elseif ($row->pekerjaan == 'LL') {
                            echo "Lain-lain";
                        }
                    } else {
                        if ($row->pekerjaan == 'PN') {
                            echo "Pegawai Negeri";
                        } elseif($row->pekerjaan == 'TP') {
                            echo "TNI / POLRI";
                        } elseif ($row->pekerjaan == 'PS') {
                            echo "Pegawai Swasta";
                        } elseif ($row->pekerjaan == 'WU') {
                            echo "Wirausaha";
                        } elseif ($row->pekerjaan == 'PSN') {
                            echo "Pensiun";
                        } elseif ($row->pekerjaan == 'TK') {
                            echo "Tidak Bekerja";
                        } elseif ($row->pekerjaan == 'LL') {
                            echo "Lain-lain";
                        }   
                    } 
                ?>
            </td>
            <td>
                <?php
                switch ($row->prodi) {
                    case '61101':
                        $nama_prodi = 'Magister Manajemen'; 
                        break;
                    case '74101':
                        $nama_prodi = 'Magister Hukum'; 
                        break;
                }
                echo $nama_prodi;
                ?>
            </td>
            <?php if ($row->kampus == 'bks') {
                $jadi = 'Bekasi';
            } else {
                $jadi = 'Jakarta';
            }
             ?>
            <td><?php echo $jadi; ?></td>
            <td><?php echo $row->gelombang; ?></td>
            <td>
                <?php 
                    if ($year > '2017') {
                        if (is_null($row->status_lulus)) {
                            echo "Registrasi";
                        } elseif($row->status_lulus == 1) {
                            echo "Lulus Tes";
                        } elseif ($row->status_lulus > 2) {
                            echo "Daftar Ulang";
                        }
                    } else {
                        if ($row->status < 1) {
                            echo "Registrasi";
                        } elseif($row->status == 1) {
                            echo "Lulus Tes";
                        } elseif ($row->status > 2) {
                            echo "Daftar Ulang";
                        }
                    }
                ?>
            </td>
            <td><?php echo $row->npm_baru; ?></td>
            <td><?php echo $row->email; ?></td>
        </tr>
		<?php $no++; } ?>
    </tbody>
</table>