<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data Hasil Tes Calon Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url('pmb/hasil_tes/updatelulus'); ?>" method="post" class="form-horizontal">
                        <button class="btn btn-success btn-sm" type="submit"><i class="btn-icon-only icon-ok"></i> submit</button>

                        <!-- checked all -->
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $("#RemoveAll").click(function() {
                                    if ($(this).is(':checked'))
                                        $(".lulus").attr("checked", "checked");
                                    else
                                        $(".lulus").removeAttr("checked");
                                });
                            });
                        </script>
                        <div class="control-group pull-right label label-warning">
                            <label class="control-label" style="margin-left: -23px; color:white;"><b>Lulus Semua</b></label>
                            <div class="controls" >
                                <input type="checkbox" style="margin-left: -35px; margin-top: 8px;" id="RemoveAll" name="absenall"/>
                            </div>
                        </div>
                        <!-- end checked all -->

                        <hr>
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th rowspan="2" style="text-align: center; vertical-align: middle;">No</th>
                                    <th rowspan="2" style="text-align: center; vertical-align: middle;">Gelombang</th>
                                    <th rowspan="2" style="text-align: center; vertical-align: middle;">Nomor Registrasi</th>
                                    <th rowspan="2" style="text-align: center; vertical-align: middle;">Peserta</th>
                                    <th colspan="2" style="text-align: center">Status</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center">Lulus</th>
                                    <th style="text-align: center">Tidak Lulus</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($raws->result() as $val => $row) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->gelombang; ?></td>
                                    <td><?php echo $row->nomor_registrasi; ?></td>
                                    <td><?php echo $row->nama; ?></td>
                                    <?php if ($row->status_lulus == 1) { ?>
                                        <td style="text-align: center"><i class="icon icon-ok"></i></td>
                                        <td style="text-align: center"> - </td>
                                    <?php } elseif ($row->status_lulus == 2) { ?>
                                        <td style="text-align: center"> - </td>
                                        <td style="text-align: center"><i class="icon icon-ok"></i></td>
                                    <?php } else { ?>
                                        <td style="text-align: center"><input type="radio" class="lulus" name="y[][<?php echo $row->user_input; ?>]" value="1"></td>
                                        <td style="text-align: center"><input type="radio" name="y[][<?php echo $row->user_input; ?>]" value="2"></td>
                                        <input type="hidden" name="id[]" value="<?php echo $row->user_input; ?>">
                                    <?php } ?>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>
