<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forms1 extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('setting_model');
		error_reporting(0);
		if ($this->session->userdata('sess_login') != TRUE) {
		// 	$cekakses = $this->role_model->cekakses(96)->result();
		// 	$aktif = $this->setting_model->getaktivasi('forms1')->result();
		// 	if ((count($aktif) != 0)) {
		// 		if ($cekakses != TRUE) {
		// 			echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
		// 			//echo "gabisa";
		// 		}
		// 	} else {
		// 		echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
		// 		//echo "gabisa";
		// 	}
		// } else {
			redirect('auth','refresh');
		}
		$this->dbreg = $this->load->database('regis', TRUE);
		date_default_timezone_set('Asia/Jakarta'); 
	}

	function index()
	{
		$a = array('74101','61101');

		$this->db->select('*');
		$this->db->from('tbl_jurusan_prodi');
		$this->db->where_not_in('kd_prodi', $a);
		$data['rows'] = $this->db->get()->result();
		$data['fakultas'] = $this->db->query("select * from tbl_fakultas where kd_fakultas != 6")->result();
		$data['page'] = "v_s1";
		$this->load->view('template', $data);
	}

	function simpan_form()
	{
		$emailRegister = $this->input->post('email', TRUE);
		
		// cek email apabila sudah terdaftar
		$cekemail = $this->dbreg->query('SELECT * from tbl_regist where email = "'.$emailRegister.'"')->result();
		if (count($cekemail) > 0) {
			echo '<script>alert("E-Mail yang sama telah terdaftar sebelumnya! Silahkan gunakan E-Mail lainnya!");history.go(-1);</script>';	
		}

		$wave = $this->db->query("SELECT * from tbl_gelombang_pmb where status = 1")->row();
		$gelombang = $wave->gelombang;
		$logged = $this->session->userdata('sess_login');

		$form = $this->input->post('jenis');

		$addr = $this->input->post('rt').','.$this->input->post('rw').','.strtoupper($this->input->post('lurah')).','.strtoupper($this->input->post('camat')).','.strtoupper($this->input->post('jalan').','.$this->input->post('perum'));

		if ($form == 'RM') {
			$b=date('Y');
			//$c=substr($b, 2,4);
			$c = '19';

			$q = $this->dbreg->query('SELECT nomor_registrasi from tbl_form_pmb where nomor_registrasi like "%'.$c.'.RS1%" order by nomor_registrasi desc limit 1')->row()->nomor_registrasi;
			$ha = (int)substr($q, -4);
			$no = $ha+1;

				if($no <= 9){
	                $doc = $c.".RS1.".$gelombang.".000".$no;
	            }elseif($no <= 99){
	                $doc = $c.".RS1.".$gelombang.".00".$no;
	            }elseif($no <= 999){
	                $doc = $c.".RS1.".$gelombang.".0".$no;
	            }elseif($no <= 9999){
	                $doc = $c.".RS1.".$gelombang.".".$no;
	            }

		}elseif ($form == 'MB') {
			$b=date('Y');
			//$c=substr($b, 2,4);
			$c = '19';

			$q = $this->dbreg->query('SELECT nomor_registrasi from tbl_form_pmb where nomor_registrasi like "'.$c.'.S1%" order by nomor_registrasi desc limit 1')->row()->nomor_registrasi;
			$ha = (int)substr($q, -4);
			$no = $ha+1;


				if($no <= 9){
	                $doc = $c.".S1.".$gelombang.".000".$no;
	            }elseif($no <= 99){
	                $doc = $c.".S1.".$gelombang.".00".$no;
	            }elseif($no <= 999){
	                $doc = $c.".S1.".$gelombang.".0".$no;
	            }elseif($no <= 9999){
	                $doc = $c.".S1.".$gelombang.".".$no;
	            }

		}elseif ($form == 'KV') {
			$b=date('Y');
			//$c=substr($b, 2,4);
			$c = '19';

			$q = $this->dbreg->query('SELECT nomor_registrasi from tbl_form_pmb where nomor_registrasi like "%'.$c.'.KS1%" order by nomor_registrasi desc limit 1')->row()->nomor_registrasi;
			$ha = (int)substr($q, -4);
			$no = $ha+1;

				if($no <= 9){
	                $doc = $c.".KS1.".$gelombang.".000".$no;
	            }elseif($no <= 99){
	                $doc = $c.".KS1.".$gelombang.".00".$no;
	            }elseif($no <= 999){
	                $doc = $c.".KS1.".$gelombang.".0".$no;
	            }elseif($no <= 9999){
	                $doc = $c.".KS1.".$gelombang.".".$no;
	            }
		}

		$jumlah = count($this->input->post('lengkap', TRUE));
		$hasil = '';
		for ($i=0; $i < $jumlah; $i++) { 
			if ($hasil == '') {
				$hasil = $this->input->post('lengkap['.$i.']', TRUE).',';
			} else {
				$hasil = $hasil.$this->input->post('lengkap['.$i.']', TRUE).',';
			}	
		}


		// buat user_input
		$tg = '19';
		// $pecah = substr($tg, 2,4);
		$no = $this->dbreg->query("SELECT userid from tbl_regist where userid like '".$tg."%' order by id desc limit 1")->row()->userid; 
		$ce = (int)substr($no, -4);
		$c = ($ce+1); 
		
		if($c <= 9){
            $here = $tg.$gelombang."0000".$c;
        }elseif($c <= 99){
            $here = $tg.$gelombang."000".$c;
        }elseif($c <= 999){
            $here = $tg.$gelombang."00".$c;
        }elseif($c <= 9999){
            $here = $tg.$gelombang."0".$c;
        }elseif($c <= 99999){
            $here = $tg.$gelombang.$c;
        }

        // var_dump($here); exit();



        // buat regis
		$toregis = array(
			'userid'	=> $here,
			'nm_depan'	=> strtoupper($this->input->post('nama')),
			'email'		=> $emailRegister,
			'password'	=> 'Ubharaj4y4',
			'tlp'		=> $this->input->post('tlp'),
			'regis_date'=> date('Y-m-d H:i:s'),
			'gate'		=> 2
		);
		$this->dbreg->insert('tbl_regist',$toregis);

		// key to tbl_aktivasi
		$hash = NULL;
        $n = 20; // jumlah karakter yang akan di bentuk.
        $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";
        for ($i = 0; $i < $n; $i++) {
            $rIdx = rand(1, strlen($chr));
            $hash .=substr($chr, $rIdx, 1);
        }
        $key = $hash.date('ymdHis');

        // buat aktivasi
		$toaktivasi = array(
			'userid'	=> $here,
			'email'		=> $emailRegister,
			'key'		=> $key,
			'flag'		=> 1,
			'send_date'	=> date('Y-m-d H:i:s'),
			'active_date'	=> date('Y-m-d H:i:s')
		);
		$this->dbreg->insert('tbl_aktivasi',$toaktivasi);

		// buat akun login
		$tologin = array(
			'email'		=> $emailRegister,
			'password'	=> md5('Ubharaj4y4regis'),
			'password_plain' => 'Ubharaj4y4',
			'userid'	=> $here
		);
		$this->dbreg->insert('tbl_user_login',$tologin);

		// buat key booking
		$salt = NULL;
        $juml = 16; // jumlah karakter yang akan di bentuk.
        $char = "0123456789";
        for ($i = 0; $i < $juml; $i++) {
            $rIdx = rand(1, strlen($char));
            $salt .=substr($char, $rIdx, 1);
        }
        $kunc = $salt;

		$tobook = array(
			'userid'		=> $here,
			'key'			=> $kunc,
			'prodi'			=> $this->input->post('prodi'),
			'gel'			=> $gelombang,
			'camp'			=> $this->input->post('kampus'),
			'bookdate'		=> date('ymd'),
			'ready'			=> 1,
			'valid'			=> 2,
			'valid_admin'	=> 'pmsrn',
			'valid_adm_date'=> date('ymd'),
			'program'		=> 1,
			'jenisreg'		=> $this->input->post('jenis')
		);
		$this->dbreg->insert('tbl_booking',$tobook);

		// buat payment
		$topayment = array(
			'userid'		=> $here,
			'datepay'		=> date('ymd'),
			'paytipe'		=> 3,
			'key_booking'	=> $kunc
		);
		$this->dbreg->insert('tbl_payment',$topayment);

		$dust = array(
				'program'				=> 1,
				'nomor_registrasi'		=> $doc,
				'nik'					=> $this->input->post('nik'),
				'jenis_pmb'				=> $this->input->post('jenis'),
				'kampus'				=> $this->input->post('kampus'),
				'keterangan'			=> $this->input->post('ket',TRUE),
				'kelas'					=> $this->input->post('kelas'),
				'nama'					=> strtoupper($this->input->post('nama')),
				'kelamin'				=> $this->input->post('jk'),
				'status_wn'				=> $this->input->post('wn'),
				'kewarganegaraan'		=> strtoupper($this->input->post('wn_txt')),
				'tempat_lahir'			=> strtoupper($this->input->post('tpt_lahir')),
				'tgl_lahir'				=> $this->input->post('tgl_lahir'),
				'agama'					=> $this->input->post('agama'),
				'status_nikah'			=> $this->input->post('stsm'),
				'status_kerja'			=> $this->input->post('stsb'),
				'pekerjaan'				=> strtoupper($this->input->post('stsb_txt')),
				'alamat'				=> $addr,
				'kd_pos'				=> $this->input->post('kdpos'),
				'tlp'					=> $this->input->post('tlp'),
				'tlp2'					=> $this->input->post('tlp2'),

				'npm_lama_readmisi'		=> $this->input->post('npm'),
				'tahun_masuk_readmisi'	=> $this->input->post('thmasuk_readmisi'),
				'smtr_keluar_readmisi'	=> $this->input->post('smtr_readmisi'),

				'asal_sch_maba'			=> strtoupper($this->input->post('asal_sch')),
				'daerah_sch_maba'		=> strtoupper($this->input->post('daerah_sch')),
				'kota_sch_maba'			=> strtoupper($this->input->post('kota_sch')),
				'jenis_sch_maba'		=> $this->input->post('jenis_sch_maba'),
				'jur_maba'				=> strtoupper($this->input->post('jur_sch')),
				'lulus_maba'			=> $this->input->post('lulus_sch'),

				'asal_pts_konversi'		=> strtoupper($this->input->post('asal_pts')),
				'kota_pts_konversi'		=> strtoupper($this->input->post('kota_pts')),
				'prodi_pts_konversi'	=> strtoupper($this->input->post('prodi_pts')),
				'npm_pts_konversi'		=> $this->input->post('npm_pts'),
				'tahun_lulus_konversi'	=> $this->input->post('lulus_pts'),
				'smtr_lulus_konversi'	=> $this->input->post('smtr_pts'),

				'prodi'					=> $this->input->post('prodi'),
				'prodi2'				=> $this->input->post('prodi2'),
				'prodi3'				=> $this->input->post('prodi3'),
				'prodi4'				=> $this->input->post('prodi4'),
				'nm_ayah'				=> strtoupper($this->input->post('nm_ayah')),
				'didik_ayah'			=> $this->input->post('didik_ayah'),
				'workdad'				=> $this->input->post('workdad'),
				'life_statdad'			=> $this->input->post('life_statdad'),

				'nm_ibu'				=> strtoupper($this->input->post('nm_ibu')),
				'didik_ibu'				=> $this->input->post('didik_ibu'),
				'workmom'				=> $this->input->post('workmom'),
				'life_statmom'			=> $this->input->post('life_statmom'),

				'penghasilan'			=> $this->input->post('gaji'),
				'referensi'				=> $this->input->post('refer'),
				'tanggal_regis'			=> date('Y-m-d H:i:s'),
				'gelombang'				=> $gelombang,
				'status'				=> '',
				'gelombang_du'			=> '',
				'npm_baru'				=> '',
				'foto'					=> '',
				'status_kelengkapan'	=> '',
				'status_feeder'			=> '',
				'nisn'					=> $this->input->post('nisn'),
				'kategori_skl'			=> $this->input->post('jenis_skl'),
				'bpjs'					=> $this->input->post('bpjs'),
				'no_bpjs'				=> $this->input->post('nobpjs'),
				'user_input'			=> $here,
				'user_renkeu'			=> '',
				'user_baa'				=> '',
				'transport'				=> $this->input->post('transport'),
				'user_pemasaran'		=> $logged['userid'],
				// 'npm_lama_s2'			=> $this->input->post('npm'),
				// 'th_masuk_s2'			=> $this->input->post('thmasuk'),
				'status_form'			=> 1,
				'key_booking'			=> $kunc,
				'almet'					=> $this->input->post('sizealmet')
				);
		$datax = $this->security->xss_clean($dust);
		$this->dbreg->insert('tbl_form_pmb', $datax);

		$this->load->library('ciqrcode');
		$params['data'] = $doc.str_replace('.', '', $doc);
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'QRImage/'.str_replace('.', '', $doc).'.png';
		$this->ciqrcode->generate($params);

		// $datax = array(
		// 		'status_kelengkapan' => $hasil,
		// 		'nomor_registrasi'	=>	$doc,
		// 		'jenis_pmb'		=>	$this->input->post('jenis'),
		// 		'kampus'		=>	$this->input->post('kampus'),
		// 		'kelas'			=>	$this->input->post('kelas'),
		// 		'nama'			=>	$this->input->post('nama'),
		// 		'nik'			=>	$this->input->post('nik'),
		// 		'kelamin'		=>	$this->input->post('jk'),
		// 		'status_wn'		=>	$this->input->post('wn'),
		// 		'kewaganegaraan'=>	$this->input->post('wn_txt'),
		// 		'tempat_lahir'	=>	$this->input->post('tpt_lahir'),
		// 		'tgl_lahir'		=>	$this->input->post('tgl_lahir'),
		// 		'agama'			=>	$this->input->post('agama'),
		// 		'status_nikah'	=>	$this->input->post('stsm'),
		// 		'status_kerja'	=>	$this->input->post('stsb'),
		// 		'perkerjaan'	=>	$this->input->post('stsb_txt'),
		// 		'alamat'		=>	$this->input->post('alamat'),
		// 		'kd_pos'		=>	$this->input->post('kdpos'),
		// 		'tlp'			=>	$this->input->post('tlp'),
		// 		'tlp2'			=>	$this->input->post('tlp2'),

		// 		'npm_lama_readmisi'		=>	$this->input->post('npm_readmisi'),
		// 		'tahun_masuk_readmisi'	=>	$this->input->post('thmasuk_readmisi'),
		// 		'smtr_keluar_readmisi'	=>	$this->input->post('smtr_readmisi'),

		// 		'asal_sch_maba'			=>	$this->input->post('asal_sch'),
		// 		'kota_sch_maba'			=>	$this->input->post('kota_sch'),
		// 		'daerah_sch_maba'		=>	$this->input->post('daerah_sch'),
		// 		'jur_maba'				=>	$this->input->post('jur_sch'),
		// 		'lulus_maba'			=>	$this->input->post('lulus_sch'),
		// 		'jenis_sch_maba'		=> 	$this->input->post('jenis_sch_maba'),

		// 		'asal_pts_konversi'		=>	$this->input->post('asal_pts'),
		// 		'kota_pts_konversi'		=>	$this->input->post('kota_pts'),
		// 		'prodi_pts_konversi'	=>	$this->input->post('prodi_pts'),
		// 		'npm_pts_konversi'		=>	$this->input->post('npm_pts'),
		// 		'tahun_lulus_konversi'	=>	$this->input->post('lulus_pts'),
		// 		'smtr_lulus_konversi'	=>	$this->input->post('smtr_pts'),
		// 		'keterangan'	=>	$this->input->post('ket',TRUE),
		// 		'gelombang'		=>  $gelombang,
		// 		'prodi'			=>	$this->input->post('prodi'),
		// 		'nm_ayah'		=>	$this->input->post('nm_ayah'),
		// 		'didik_ayah'	=>	$this->input->post('didik_ayah'),
		// 		'workdad'		=>	$this->input->post('workdad'),
		// 		'life_statdad'	=>	$this->input->post('life_statdad'),
		// 		'nm_ibu'		=>	$this->input->post('nm_ibu'),
		// 		'didik_ibu'		=>	$this->input->post('didik_ibu'),
		// 		'workmom'		=>	$this->input->post('workmom'),
		// 		'life_statmom'	=>	$this->input->post('life_statmom'),
		// 		'tanggal_regis'	=>	date('Y-m-d'),
		// 		'penghasilan'	=>	$this->input->post('gaji'),
		// 		'referensi'		=>	$this->input->post('refer'),
		// 		'kategori_skl'	=> $this->input->post('jenis_skl'),
		// 		'nisn'			=> $this->input->post('nisn'),
		// 		'bpjs'			=> $this->input->post('bpjs'),
		// 		'user_input'	=> $logged['userid'],
		// 		'no_bpjs'		=> $this->input->post('nobpjs'),
		// 		'transport'		=> $this->input->post('transport')
				
		// 	);
		// $datax = $this->security->xss_clean($datax);
		// //var_dump($datax);exit();
		// $this->app_model->insertdata('tbl_form_camaba', $datax);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."pmb/forms1';</script>";
	}

	function cek_email()
	{
		$email = $this->input->post('email');
		$cek = $this->dbreg->query("SELECT * from tbl_regist where email = '".$email."'")->num_rows();
		echo json_encode($cek);
	}

}

/* End of file Forms1.php */
/* Location: ./application/controllers/Forms1.php */