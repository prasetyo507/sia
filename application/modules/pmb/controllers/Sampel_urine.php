<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sampel_urine extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$sesi = $this->session->userdata('sess_login');
		if ($this->session->userdata('sess_login') != TRUE) {
			redirect('auth');
		}
		if($sesi['id_user_group'] != 13){
				echo "<script>alert('Maaf, Anda tidak memiliki akses di menu ini');
		document.location.href='".base_url()."';</script>";
			}
		$this->dbreg = $this->load->database('regis', TRUE);
	}

	public function index()
	{
		$this->load->model('pmb/model_urine');
		$data['prodi'] = $this->model_urine->getdata('tbl_jurusan_prodi','prodi','ASC')->result();
		$data['page'] = "v_urine";
		$this->load->view('template', $data);
	}

	function load()
	{
		$this->load->model('pmb/model_urine');
		$this->load->library('Cfpdf');

		$year 			= substr($this->input->post('tahun'),2,4);
		$gelombang 		= $this->input->post('gelombang');
		$data['prodd']	= $this->input->post('prodi');

		$data['query'] 	= $this->model_urine->getPDF($year,$gelombang,$data['prodd'])->result();
		
		$this->load->view('v_urine_pdf', $data);

		
	}


}

/* End of file Sampel_urine.php */
/* Location: ./application/modules/pmb/controllers/Sampel_urine.php */