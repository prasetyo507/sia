<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pmb_model');
		
	}

	public function index()
	{
		$data['fak'] =$this->db->order_by('fakultas','asc')->get('tbl_fakultas')->result(); 
		$data['page']  ='pmb/v_nilai_select';
		$this->load->view('template', $data);
	}

	function list_nilai(){

		$form = $this->session->userdata('sess_form');

		$data['camaba'] = $this->Pmb_model->get_camaba($form['ta'],$form['gel'],$form['prodi'])->result();

		//var_dump($data['camaba']);die();

		$data['page']='pmb/v_nilai_list';
		$this->load->view('template', $data);	
	}

	function create_session(){

		$ta = $this->input->post('ta');
		$gel = $this->input->post('gel');
		$prodi = $this->input->post('prodi');

		$array = array(
			'ta' => $ta,
			'gel' => $gel,
			'prodi' => $prodi
		);
		
		$this->session->set_userdata('sess_form',$array);

		redirect(base_url("pmb/nilai/list_nilai"),'refresh');
	}

	function dwld_excel(){
		$this->load->library('excel');

		$form = $this->session->userdata('sess_form');

		$data['prodi'] = $form['prodi'];
		$data['ta'] = $form['ta'];
		$data['gel'] = $form['gel'];

		$data['camaba'] = $this->Pmb_model->get_camaba($form['ta'],$form['gel'],$form['prodi'])->result();


		$this->load->view('phpexcel_nilai_pmb',$data);
	}

	function upload_excel(){
		$user = $this->session->userdata('sess_login');

		if ($_FILES['userfile']) {
			$ta = $this->input->post('ta');
			$gel = $this->input->post('gel');
			$prodi = $this->input->post('prodi');
			$peserta = $this->input->post('peserta');



			//get ip address
			if (getenv('HTTP_CLIENT_IP'))
		        $ipaddress = getenv('HTTP_CLIENT_IP');
		    else if(getenv('HTTP_X_FORWARDED_FOR'))
		        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		    else if(getenv('HTTP_X_FORWARDED'))
		        $ipaddress = getenv('HTTP_X_FORWARDED');
		    else if(getenv('HTTP_FORWARDED_FOR'))
		        $ipaddress = getenv('HTTP_FORWARDED_FOR');
		    else if(getenv('HTTP_FORWARDED'))
		        $ipaddress = getenv('HTTP_FORWARDED');
		    else if(getenv('REMOTE_ADDR'))
		        $ipaddress = getenv('REMOTE_ADDR');
		    else
		        $ipaddress = 'UNKNOWN';

		    //bersihkan karakter sampah pada nama file
			$this->load->helper('inflector');
			$nama = underscore(str_replace('/', '_', $this->security->sanitize_filename($_FILES['userfile']['name'])));
			$nama = str_replace(' ', '_', $nama);
			$cek_dot = explode('.', $nama);

			//cek keaslian file
			if (count($cek_dot) > 2) {
				echo "<script>alert('FORMAT FILE MENCURIGAKAN!');history.go(-1)';</script>";exit();
			}

			//create nama file
			$nama = "".$prodi.'_GEL'.$gel.'_'.date('dmY').'_'.date('H:i:s').'.'.$cek_dot['1']."";

			//cek and create folder
			if (!file_exists('./upload/nilai_pmb'.$ta.'')) {
			    mkdir('./upload/nilai_pmb'.$ta.'', 0777, true);
			}

			//config upload library
			$config['upload_path'] = './upload/nilai_pmb'.$ta.'/';
			$config['allowed_types'] = 'xls|xlsx';
			$config['file_name'] = $nama;
			$config['max_size'] = 1000000;

			$this->load->library('upload', $config);

			//upload excel
			if (!$this->upload->do_upload("userfile")) {
				$data = array('error', $this->upload->display_errors());
				var_dump($data);exit();
				echo "<script>alert('Gagal');
				document.location.href='".base_url()."form/formnilai/';</script>";
			} else {
				//insert to file log table
				$log_excel = array('kd_file' => $prodi.'.'.$gel.'.'.date('dmYHis'), 'file_name' => $nama, 'waktu' => date('Y-m-d H:i:s'), 'userid' => $user['userid'], 'ip_address' => $ipaddress );
				$this->db->insert('tbl_nilai_pmb_log', $log_excel);

				//reading excel
				$upload_data = $this->upload->data(); 
	   			$this->load->library('excel_reader');
	   			$this->excel_reader->setOutputEncoding('CP1251');
				$file=$upload_data['full_path'];
				$this->excel_reader->read($file);
				$dataexcel=$this->excel_reader->sheets[0];
				
				$a = 0;

				//var_dump($dataexcel);die()
				//if (count($dataexcel) == $peserta) {
					for ($x=0; $x < count($dataexcel) ; $x++) { 
						$i=8;
						$this->db->where('nomor_registrasi', $dataexcel['cells'][$i][2]);
						$this->db->delete('tbl_nilai_pmb');

						if ($dataexcel['cells'][$i][8] >= 50) {
							$lls = 1;
						}else{
							$lls = 0;
						}

						$object = array(
								'nomor_registrasi' => $dataexcel['cells'][$i][2],
								'tpa' => $dataexcel['cells'][$i][4], 
								'mtk' => $dataexcel['cells'][$i][5], 
								'ind' => $dataexcel['cells'][$i][6],
								'ing' => $dataexcel['cells'][$i][7],
								'total' => $dataexcel['cells'][$i][8],
								'kelulusan' => $lls, 
								'kd_file' => $log_excel['kd_file']
								);

						$this->db->insert('tbl_nilai_pmb', $object);
						$i++;
					}

					die();
				echo "<script>alert('Sukses');
							document.location.href='".base_url()."pmb/nilai/list_nilai/;</script>";
				//}else{
				//	echo "<script>alert('gagal upload check kembali jumlah peserta');
				//		document.location.href='".base_url()."pmb/nilai/list_nilai/;</script>";
				//}
				


				
			}




		}
	}

	function insert_nilai($data){
		

		for ($i=0; $i < count($data); $i++) { 

			$this->db->where('nomor_registrasi', $data[$i]['nomor_registrasi']);
			$this->db->delete('tbl_nilai_pmb');

			$object = array(
					'nomor_registrasi' => $data[$i]['nomor_registrasi'],
					'tpa' => $data[$i]['tpa'], 
					'mtk' => $data[$i]['mtk'], 
					'ind' => $data[$i]['ind'],
					'ing' => $data[$i]['ing'],
					'total' => $data[$i]['total'],
					'kelulusan' => $data[$i]['kelulusan'], 
					'kd_file' => $data[$i]['kd_file']
					);

			$this->db->insert('tbl_nilai_pmb', $object);
		}
	}

	function test(){
		$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);

		echo $hostname;
	}

}

/* End of file Nilai.php */
/* Location: ./application/modules/pmb/controllers/Nilai.php */