<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta_pmb extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('Cfpdf');
		//error_reporting(0);
		// if ($this->session->userdata('sess_login') == TRUE) {
		// 	$cekakses = $this->role_model->cekakses(118)->result();
		// 	if ($cekakses != TRUE) {
		// 		echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
		// 	}
		// } else {
		// 	redirect('auth','refresh');
		// }
		$this->load->helper('main_helper');
	}

	public function index()
	{
		$data['page'] = "v_choosewave";
		$this->load->view('template', $data);
	}

	function pass_sess()
	{
		$th = $this->input->post('tahun');
		$pr = $this->input->post('program');
		$gl = $this->input->post('gelombang');
		$array = array(
			'tahun' => $th,
			'progs' => $pr,
			'gels' => $gl
		);
		
		$this->session->set_userdata('sess4',$array);
		redirect('pmb/peserta_pmb/load_pstpmb');
	}

	function load_pstpmb()
	{
		$log = $this->session->userdata('sess4');
		$data['tahun'] = $log['tahun'];
		$data['progr'] = $log['progs'];
		$data['gelms'] = $log['gels'];
		$data['list'] = $this->app_model->pstpmb($log['tahun'],$log['progs'],$log['gels']);
		$data['page'] = "v_listpst";
		$this->load->view('template', $data);
	}

	function breaksess()
	{
		$this->session->unset_userdata('sess4');
		redirect('pmb/peserta_pmb');
	}

	function dwld()
	{
		$logg = $this->session->userdata('sess4');
		$data['progr'] = $logg['progs'];
		$data['query'] = $this->app_model->dwld_pstpmb($logg['tahun'],$logg['progs'],$logg['gels']);
		$this->load->view('Excel_update_sts', $data);
	}

	function update_byupload()
	{
		if ($_FILES['userfile']) {
			$g = $this->input->post('glm');
			$p = $this->input->post('prg');
			if ($p == 'S1') {
				$cekjum = $this->db->query("SELECT count(nomor_registrasi) as jums from tbl_form_camaba where gelombang = '".$g."'")->row()->jums;
			} else {
				$cekjum = $this->db->query("SELECT count(ID_registrasi) as jums from tbl_pmb_s2 where gelombang = '".$g."'")->row()->jums;
			}
			$this->load->helper('inflector');
			$nama = rand(1,10000000).underscore(str_replace('/', '', $_FILES['userfile']['name']));
			$kadal = explode('.', $nama);
			if (count($kadal) > 2) {
				echo "<script>alert('FORMAT FILE MENCURIGAKAN!');history.go(-1)';</script>";exit();
			}
			$config['upload_path'] = './temp_uploadpmb/';
			$config['allowed_types'] = 'xls|xlsx';
			$config['file_name'] = $nama;
			$config['max_size'] = 1000000;
            
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload("userfile")) {
				$data = array('error', $this->upload->display_errors());
				//var_dump($data);exit();
				die('false');
				echo "<script>alert('Gagal');
				document.location.href='".base_url()."pmb/peserta_pmb/';</script>";
			} else {
				$upload_data = $this->upload->data(); 
	   			$this->load->library('excel_reader');
	   			$this->excel_reader->setOutputEncoding('CP1251');
				$file=$upload_data['full_path'];
				$this->excel_reader->read($file);
				$data=$this->excel_reader->sheets[0];
				$dataexcel = array();

				for ($sq = 0 ; $sq < $cekjum; $sq++) {
	                $dataexcel[$sq]['idregis'] = $data['cells'][$sq+6][2];
	                if ($p == 'S1') {
	                	$dataexcel[$sq]['status'] = number_format($data['cells'][$sq+6][8]);
	                } else {
	                	$dataexcel[$sq]['status'] = number_format($data['cells'][$sq+6][7]);
	                }
			    }
			    $this->updatests($dataexcel,$p);	
			}
	    }
	}

	function updatests($dataarray,$pr)
	{
        for($i=0;$i<count($dataarray);$i++){
            $datax = array(
                'status' => $dataarray[$i]['status']
                    );
            $dataxx = $this->security->xss_clean($datax);
            if ($pr == 'S1') {
	            $this->db->where('nomor_registrasi', $dataarray[$i]['idregis']);
	            $this->db->update('tbl_form_camaba_test', $dataxx);
            } else {
            	$this->db->where('ID_registrasi', $dataarray[$i]['idregis']);
            	$this->db->update('tbl_pmb_s2_test', $dataxx);
            }
            
    	}
    	echo "<script>alert('Sukses');document.location.href='".base_url()."pmb/peserta_pmb/load_pstpmb;</script>";
        
	}

}

/* End of file Peserta_pmb.php */
/* Location: ./application/modules/pmb/controllers/Peserta_pmb.php */