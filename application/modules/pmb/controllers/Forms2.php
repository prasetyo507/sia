<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forms2 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('setting_model');
		error_reporting(0);
		if ($this->session->userdata('sess_login') != TRUE) {
		// 	$cekakses = $this->role_model->cekakses(95)->result();
		// 	$aktif = $this->setting_model->getaktivasi('forms2')->result();
		// 	if ((count($aktif) != 0)) {
		// 		if ($cekakses != TRUE) {
		// 			echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
		// 			//echo "gabisa";
		// 		}
		// 	} else {
		// 		echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
		// 		//echo "gabisa";
		// 	}
		// } else {
			redirect('auth','refresh');
		}
		$this->dbreg = $this->load->database('regis', TRUE);
		date_default_timezone_set('Asia/Jakarta'); 
	}

	function index()
	{
		//$data['prodi'] = $this->db->query('SELECT * from tbl_jurusan_prodi')->row();
		$data['page'] = "v_s2";
		$this->load->view('template', $data);
	}

	function inputdata()
	{
		// cek email apabila sudah terdaftar
		$cekemail = $this->dbreg->query('SELECT * from tbl_regist where email = "'.$this->input->post('email', TRUE).'"')->result();
		if ((bool)$cekemail) {
			echo '<script>alert("E-Mail yang sama telah terdaftar sebelumnya! Silahkan gunakan E-Mail lainnya!");history.go(-1);</script>';	
		}

		$this->load->helper('inflector');
		$nama = underscore($_FILES['img']['name']);
		$config['upload_path'] = './upload/imgpmb/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['file_name'] = $nama;
		$config['max_size'] = 1000000;
		$this->load->library('upload', $config);
		$patt = $config['upload_path'].basename($nama);
		$namm = strtoupper($this->input->post('nama'));
		$ayah = strtoupper($this->input->post('nm_ayah'));
		$ibu = strtoupper($this->input->post('nm_ibu'));
		$kota = strtoupper($this->input->post('kota_asal'));
		$ambil = $this->input->post('jenis');

		$wave = $this->db->query("SELECT * from tbl_gelombang_pmb where status = 1")->row();
		$gelombang = $wave->gelombang;

		$addr = 'RT '.$this->input->post('rt').', RW '.$this->input->post('rw').', Kel. '.$this->input->post('lurah').', Kec. '.$this->input->post('camat').', Jl. '.$this->input->post('jalan').', Perum. '.$this->input->post('perum');
		
		$logged = $this->session->userdata('sess_login');
		if ($ambil == 'RM') {
			$t = '2019';
			$y = substr($t, 2,4);
			$q = $this->dbreg->query('SELECT nomor_registrasi from tbl_form_pmb where nomor_registrasi like "%'.$y.'.RS2%" order by nomor_registrasi desc limit 1')->row()->nomor_registrasi;
			$j = (int)substr($q, -4);
			$m = $j+1;
			
			if($m<=9){
                $doc = $y.".RS2.".$gelombang.".000".$m;
            }elseif($m<=99){
                $doc = $y.".RS2.".$gelombang.".00".$m;
            }elseif($m<=999){
                $doc = $y.".RS2.".$gelombang.".0".$m;
            }elseif($m<=9999) {
                $doc = $y.".RS2.".$gelombang.".".$m;
            }
		}elseif($ambil == 'MB') {
			$t = '2019';
			$y = substr($t, 2,4);
			$q = $this->dbreg->query('SELECT nomor_registrasi from tbl_form_pmb where nomor_registrasi like "%'.$y.'.S2%" order by nomor_registrasi desc limit 1')->row()->nomor_registrasi;
			$j = (int)substr($q, -4);
			$m = $j+1;
			
			if($m<=9){
                $doc = $y.".S2.".$gelombang.".000".$m;
            }elseif($m<=99){
                $doc = $y.".S2.".$gelombang.".00".$m;
            }elseif($m<=999){
                $doc = $y.".S2.".$gelombang.".0".$m;
            }elseif($m<=9999) {
                $doc = $y.".S2.".$gelombang.".".$m;
            }
		}elseif($ambil == 'KV') {
			$t = '2019';
			$y = substr($t, 2,4);
			$q = $this->dbreg->query('SELECT nomor_registrasi from tbl_form_pmb where nomor_registrasi like "%'.$y.'.KS2%" order by nomor_registrasi desc limit 1')->row()->nomor_registrasi;
			$j = (int)substr($q, -4);
			$m = $j+1;
			
			if($m<=9){
                $doc = $y.".KS2.".$gelombang.".000".$m;
            }elseif($m<=99){
                $doc = $y.".KS2.".$gelombang.".00".$m;
            }elseif($m<=999){
                $doc = $y.".KS2.".$gelombang.".0".$m;
            }elseif($m<=9999) {
                $doc = $y.".KS2.".$gelombang.".".$m;
            }
		}

		$jumlah = count($this->input->post('lengkap', TRUE));
		//var_dump($reg.' - '.$jumlah);exit();
		$hasil = '';
		for ($i=0; $i < $jumlah; $i++) { 
			if ($hasil == '') {
				$hasil = $this->input->post('lengkap['.$i.']', TRUE).',';
			} else {
				$hasil = $hasil.$this->input->post('lengkap['.$i.']', TRUE).',';
			}	
		}

		if ($this->input->post('nm_pt') == '' or is_null($this->input->post('nm_pt'))) {
			$asal_universitas = $this->input->post('nm_pt_konv');
		} else {
			$asal_universitas = $this->input->post('nm_pt');
		}
		

		// buat user_input
		$tg = '19';
		$no = $this->dbreg->query("SELECT userid from tbl_regist where userid like '".$tg."%' order by userid desc limit 1")->row()->userid;
		$fo = (int)substr($no, -5);		
		$c = $fo+1;
		
		if($c <= 9){
            $here = $tg.$gelombang."0000".$c;
        }elseif($c <= 99){
            $here = $tg.$gelombang."000".$c;
        }elseif($c <= 999){
            $here = $tg.$gelombang."00".$c;
        }elseif($c <= 9999){
            $here = $tg.$gelombang."0".$c;
        }elseif($c <= 99999){
            $here = $tg.$gelombang.$c;
        }

		$toregis = array(
			'userid'	=> $here,
			'nm_depan'	=> strtoupper($this->input->post('nama')),
			'password'	=> 'Ubharaj4y4',
			'email'		=> $this->input->post('email', TRUE),
			'tlp'		=> $this->input->post('tlp', TRUE),
			'regis_date'=> date('Y-m-d H:i:s'),
			'gate'		=> 2
		);
		$this->dbreg->insert('tbl_regist',$toregis);

		// key to tbl_aktivasi
		$hash = NULL;
        $n = 20; // jumlah karakter yang akan di bentuk.
        $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";
        for ($i = 0; $i < $n; $i++) {
            $rIdx = rand(1, strlen($chr));
            $hash .=substr($chr, $rIdx, 1);
        }
        $key = $hash.date('ymdHis');

		$toaktivasi = array(
			'userid'	=> $here,
			'email'		=> $this->input->post('email'),
			'key'		=> $key,
			'flag'		=> 1,
			'send_date'	=> date('Y-m-d H:i:s'),
			'active_date'	=> date('Y-m-d H:i:s')
		);
		$this->dbreg->insert('tbl_aktivasi',$toaktivasi);

		$tologin = array(
			'email'		=> $this->input->post('email'),
			'password'	=> md5('Ubharaj4y4regis'),
			'password_plain' => 'Ubharaj4y4',
			'userid'	=> $here
		);
		$this->dbreg->insert('tbl_user_login',$tologin);

		// buat key booking
		$salt = NULL;
        $juml = 16; // jumlah karakter yang akan di bentuk.
        $char = "0123456789";
        for ($i = 0; $i < $juml; $i++) {
            $rIdx = rand(1, strlen($char));
            $salt .=substr($char, $rIdx, 1);
        }
        $kunc = $salt;

		$tobook = array(
			'userid'		=> $here,
			'key'			=> $kunc,
			'prodi'			=> $this->input->post('prodi'),
			'gel'			=> $gelombang,
			'camp'			=> $this->input->post('kampus'),
			'bookdate'		=> date('ymd'),
			'ready'			=> 1,
			'valid'			=> 2,
			'valid_admin'	=> 'pmsrn',
			'valid_adm_date'=> date('ymd'),
			'program'		=> 1,
			'jenisreg'		=> $ambil
		);
		$this->dbreg->insert('tbl_booking',$tobook);

		// buat payment
		$topayment = array(
			'userid'		=> $here,
			'datepay'		=> date('ymd'),
			'paytipe'		=> 3,
			'key_booking'	=> $kunc
		);
		$this->dbreg->insert('tbl_payment',$topayment);

		$dust = array(
					'program'				=> 2,
					'nomor_registrasi'		=> $doc,
					'nik'					=> $this->input->post('nik'),
					'jenis_pmb'				=> $ambil,
					'kampus'				=> $this->input->post('kampus'),
					'keterangan'			=> $this->input->post('ket',TRUE),
					'kelas'					=> 'KY',
					'nama'					=> $namm,
					'kelamin'				=> $this->input->post('jk'),
					'status_wn'				=> $this->input->post('wn'),
					'kewarganegaraan'		=> $this->input->post('jns_wn'),
					'tempat_lahir'			=> strtoupper($this->input->post('tpt')),
					'tgl_lahir'				=> $this->input->post('tgl'),
					'agama'					=> $this->input->post('agama'),
					'status_nikah'			=> $this->input->post('stsm'),
					'status_kerja'			=> $this->input->post('stsb'),
					'pekerjaan'				=> strtoupper($this->input->post('tpt_kerja')),
					'alamat'				=> $addr,
					'kd_pos'				=> $this->input->post('kdpos'),
					'tlp'					=> $this->input->post('tlp'),
					'tlp2'					=> $this->input->post('tlp2'),

					// 'npm_lama_readmisi'		=> $this->input->post('npm_readmisi'),
					// 'tahun_masuk_readmisi'	=> $this->input->post('thmasuk_readmisi'),
					// 'smtr_keluar_readmisi'	=> $this->input->post('smtr_readmisi'),

					'asal_sch_maba'			=> $this->input->post('nm_pt'),
					// 'daerah_sch_maba'		=> '',
					// 'kota_sch_maba'			=> '',
					// 'jenis_sch_maba'		=> $this->input->post('jenis_sch_maba'),
					// 'jur_maba'				=> '',
					// 'lulus_maba'			=> '',

					'asal_pts_konversi'		=> $this->input->post('nm_pt_konv'),
					'kota_pts_konversi'		=> $this->input->post('kota_asal'),
					'prodi_pts_konversi'	=> $this->input->post('prodi_lm'),
					'npm_pts_konversi'		=> $this->input->post('npmnim'),
					'tahun_lulus_konversi'	=> $this->input->post('thlulus'),
					'smtr_lulus_konversi'	=> $this->input->post('smtlulus'),

					'prodi'					=> $this->input->post('prodi'),
					'prodi2'				=> $this->input->post('prodi2'),
					'nm_ayah'				=> strtoupper($this->input->post('nm_ayah')),
					'didik_ayah'			=> $this->input->post('didik_ayah'),
					'workdad'				=> $this->input->post('workdad'),
					'life_statdad'			=> $this->input->post('life_statdad'),

					'nm_ibu'				=> strtoupper($this->input->post('nm_ibu')),
					'didik_ibu'				=> $this->input->post('didik_ibu'),
					'workmom'				=> $this->input->post('workmom'),
					'life_statmom'			=> $this->input->post('life_statmom'),

					'penghasilan'			=> $this->input->post('gaji'),
					'referensi'				=> $this->input->post('refer'),
					'tanggal_regis'			=> date('Y-m-d H:i:s'),
					'gelombang'				=> $gelombang,
					'status'				=> '',
					'gelombang_du'			=> '',
					'npm_baru'				=> '',
					'foto'					=> '',
					'status_kelengkapan'	=> '', // $lengkap,
					'status_feeder'			=> '',
					'nisn'					=> $this->input->post('nisn'),
					'kategori_skl'			=> strtoupper($this->input->post('jenispt')),
					'bpjs'					=> $this->input->post('bpjs'),
					'no_bpjs'				=> $this->input->post('nobpjs'),
					'user_input'			=> $here,
					'user_renkeu'			=> '',
					'user_baa'				=> '',
					'user_pemasaran'		=> $logged['userid'],
					'transport'				=> $this->input->post('transport'),
					'npm_lama_s2'			=> '',
					'th_masuk_s2'			=> '',
					'status_form'			=> 1,
					'key_booking'			=> $kunc,
					'almet'					=> $this->input->post('sizealmet')
				);

		$datax = $this->security->xss_clean($dust);
		$this->dbreg->insert('tbl_form_pmb', $dust);

		$this->load->library('ciqrcode');
		$params['data'] = $doc.str_replace('.', '', $doc);
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'QRImage/'.str_replace('.', '', $doc).'.png';
		$this->ciqrcode->generate($params);

		echo "<script>alert('Sukses');
		document.location.href='".base_url()."pmb/forms2';</script>";

		// $datax = array(
		// 		'status_kelengkapan' => $hasil,
		// 		'jenis_pmb'		=>	$ambil,
		// 		'kampus'		=>	$this->input->post('kampus'),
		// 		'kelas'			=>	'KR',
		// 		'nama'			=>	$namm,
		// 		'nik'			=>	$this->input->post('nik'),
		// 		'kelamin'		=>	$this->input->post('jk'),
		// 		'negara'		=>	$this->input->post('wn'),
		// 		'tempat_lahir'	=>	$this->input->post('tpt'),
		// 		'tgl_lahir'		=>	$this->input->post('tgl'),
		// 		'agama'			=>	$this->input->post('agama'),
		// 		'status_nikah'	=>	$this->input->post('stsm'),
		// 		'status_kerja'	=>	$this->input->post('stsb'),
		// 		'alamat'		=>	$this->input->post('alamat'),
		// 		'kd_pos'		=>	$this->input->post('kdpos'),
		// 		'tlp'			=>	$this->input->post('tlp'),
		// 		'asaluniv'		=>	$this->input->post('asaluniv'),
		// 		'npm_lama'		=>	$this->input->post('npm'),
		// 		'thmsubj'		=>	$this->input->post('thmasuk'),
		// 		'nm_univ'		=>	$asal_universitas,
		// 		'prodi'			=>	$this->input->post('prodi_lm'),
		// 		'th_lulus'		=>	$this->input->post('thlulus'),
		// 		'kota_asl_univ'	=>	$kota,
		// 		'npm_nim'		=>	$this->input->post('npmnim'),
		// 		'opsi_prodi_s2'	=>	$this->input->post('prodi'),
		// 		'pekerjaan'		=>	$this->input->post('pekerjaan'),
		// 		'nm_ayah'		=>	$ayah,
		// 		'nm_ibu'		=>	$ibu,
		// 		'penghasilan'	=>	$this->input->post('gaji'),
		// 		'informasi_from'=>	$this->input->post('infrom'),
		// 		'foto'			=>	$patt,
		// 		'negara_wna'	=>	$this->input->post('jns_wn'),
		// 		'tempat_kerja'	=>	$this->input->post('tpt_kerja'),
		// 		'ID_registrasi'	=>	$doc,
		// 		'tanggal_regis'	=>	date('Y-m-d'),
		// 		'gelombang'		=>  $gelombang,
		// 		'kategori_univ'	=> $this->input->post('jenispt'),
		// 		'bpjs'			=> $this->input->post('bpjs'),
		// 		'user_input'	=> $logged['userid'],
		// 		'no_bpjs'		=> $this->input->post('nobpjs'),
		// 		'keterangan'	=>	$this->input->post('ket',TRUE),
		// 		'transport'		=> $this->input->post('transport')
		// 	);

		// //var_dump($datax);exit();
		// $datax = $this->security->xss_clean($datax);
		// $this->app_model->insertdata('tbl_pmb_s2', $datax);
		// echo "<script>alert('Sukses');
		// document.location.href='".base_url()."pmb/forms2';</script>";
	}

}

/* End of file Forms2.php */
/* Location: .//tmp/fz3temp-1/Forms2.php */