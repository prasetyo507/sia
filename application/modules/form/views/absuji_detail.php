
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Absensi Ujian</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a href="<?php echo base_url(); ?>form/cetakabsensiujian" class="btn btn-primary"><< Kembali</a>
                    <hr>
					<table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 

                                <th>Kode MK</th>

                                <th>Mata Kuliah</th>

                                <th>Hari</th>

                                <th>Dosen</th>

                                <th>Kelas</th>

                                <th>Peserta</th>

                                <th width="40">UTS</th>

                                <!--th width="40">UTS Susulan</th-->

                                <th width="40">UAS</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no=1; foreach ($rows as $isi): ?>

                                <tr>

                                    <td><?php echo $isi->kd_matakuliah; ?></td>

                                    <td><?php echo $isi->nama_matakuliah; ?></td>

                                    <td><?php echo notohari($isi->hari); ?></td>

                                    <td><?php echo $isi->nama; ?></td>

                                    <td><?php echo $isi->kelas; ?></td>

                                    <?php $kode = $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$isi->id_jadwal,'id_jadwal','asc')->row(); $anak = $this->db->query("select count(distinct npm_mahasiswa) as jumlah from tbl_krs where kd_jadwal = '".$kode->kd_jadwal."' ")->row(); ?>

                                    <td><?php echo $anak->jumlah; ?></td>

                                    <td class="td-actions">

                                        <a href="<?php echo base_url(); ?>form/cetakabsensiujian/cetakpdf/<?php echo $isi->id_jadwal; ?>" class="btn btn-success btn-small"><i class="btn-icon-only icon-print"> </i></a>

                                    </td>

                                    <td class="td-actions">

                                        <a href="<?php echo base_url(); ?>form/cetakabsensiujian/cetakpdfuas/<?php echo $isi->id_jadwal; ?>" class="btn btn-success btn-small"><i class="btn-icon-only icon-print"> </i></a>

                                    </td> 

                                </tr>    

                            <?php $no++; endforeach ?>

                        </tbody>

                    </table>
				</div>
			</div>
		</div>
	</div>
</div>