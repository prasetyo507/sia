<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">

jQuery(document).ready(function($) {

    $('input[name^=dosen]').autocomplete({

        source: '<?php echo base_url('form/krspaket/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.dosen.value = ui.item.value;

            this.form.dsn.value = ui.item.nid;

        }

    });

});

</script>
<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Form KRS</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <b><center>FORM KRS</center></b><br>
          <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>form/krspaket/save_sess">
            <fieldset>

              <div class="control-group">                     
                <label class="control-label">NID</label>
                <div class="controls">
                  <input type="text" class="span3" name="dosen" placeholder="Masukkan NID Pembimbing" required autofocus>
                  <input type="hidden" name="dsn" value="" placeholder="">
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->

              <div class="control-group">                     
                <label class="control-label">Angkatan</label>
                <div class="controls">
                  <select name="tahun" class="span3" required>
                    <option value="<?= date('Y') ?>"><?= date('Y') ?></option>
                  </select>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->

              <div class="control-group">                     
                <label class="control-label">Kelompok Kelas</label>
                <div class="controls">
                  <select name="kelas" class="span3" required>
                    <option selected="" disabled="">-- Pilih Kelompok Kelas --</option>
                    <?php foreach ($kelas as $val) { ?>
                      <option value="<?php echo $val->kelas; ?>"><?php echo $val->kelas.' ('.classType($val->waktu_kelas).')'; ?></option>
                    <?php } ?>
                  </select>
                </div> <!-- /controls -->       
              </div>

              <div class="form-actions">
                <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                <input type="reset" class="btn btn-warning" value="Reset"/>
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>