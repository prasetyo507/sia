<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>



<div class="row">

	<div class="span12">      		  		

  		<div class="widget">

  			<div class="widget-header">

  				<i class="icon-user"></i>

  				<h3>Data Kartu Hasil Studi</h3>

			</div> <!-- /widget-header -->

			

			<div class="widget-content">

				<div class="span11">

					<a href="#" class="btn btn-success"onclick="window.history.back();return false;"><< Kembali</a>

					<?php 
                    $logged = $this->session->userdata('sess_login');
                    $pecah = explode(',', $logged['id_user_group']);
                    $jmlh = count($pecah);
                    for ($i=0; $i < $jmlh; $i++) { 
                        $grup[] = $pecah[$i];
                    }
                    if ((in_array(5, $grup))) {
                        $prodi = $mhs->KDPSTMSMHS;
                    } else {
                        $prodi = $logged['userid'];
                    }
                    $hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
                    JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
                    WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$mhs->KDPSTMSMHS.'" AND NIMHSTRLNM = "'.$mhs->NIMHSMSMHS.'" and THSMSTRLNM = "'.substr($kode->kd_krs, 12,5).'" ')->result();

                    $st=0;
                    $ht=0;
                    foreach ($hitung_ips as $iso) {
                        $h = 0;


                        $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
                        $ht=  $ht + $h;

                        $st=$st+$iso->sks_matakuliah;
                    }

                    $ips_nr = $ht/$st;
                    $ipk = $this->app_model->get_ipk_mahasiswa($q->NIMHSMSMHS)->row()->ipk;
                    for ($i=0; $i < $jmlh; $i++) { 
                        $grup[] = $pecah[$i];
                    }

                    $cek = $this->db->query("select * from tbl_sinkronisasi_renkeu where npm_mahasiswa = '".substr($kode->kd_krs, 0,12)."' and (status = 4 or status = 5) and tahunajaran = '".substr($kode->kd_krs, 12,5)."'")->result();
                    $krsan = $this->app_model->getkrsanmhs($npm,'20152')->row();
                    if ( (in_array(8, $grup)) and ($krsan == true) ) { ?>
                        
                        <a href="<?php echo base_url(); ?>akademik/khs/printkhs/<?php echo $npm ?>/<?php echo $semester;?>" target="_blank" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print KHS</a>
                     
                    <?php } ?>

					<!--a href="<?php echo base_url(); ?>akademik/khs/printkhs/<?php echo $npm ?>" target="_blank" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print KHS</a-->

					<hr>

					<table>

                        <tr>

                            <td>NPM</td>

                            <td>:</td>

                            <td><?php echo $mhs->NIMHSMSMHS;?></td>

                        </tr>

                        <tr>

                            <td>Nama</td>

                            <td>:</td>

                            <td><?php echo $mhs->NMMHSMSMHS;?></td>

                            <td width="100"></td>

                            <td>Semester</td>

                            <td>:</td>
                            <?php $a = $this->app_model->get_semester_khs($mhs->SMAWLMSMHS,substr($kode->kd_krs, 12,5)); ?>
                            <td><?php echo $a;?></td>
                            <!-- <td><?php //echo $semester;?></td> -->

                        </tr>

                        <tr>

                            <td>IPS</td>

                            <td>:</td>

                            <td><?php echo number_format($ips_nr, 2); ?></td>

                        </tr>

                    </table>

                    <hr>

					<table id="example1" class="table table-bordered table-striped">

	                	<thead>

	                        <tr> 

	                        	<th>No</th>

	                        	<th>Kode MK</th>

	                        	<th>Mata Kuliah</th>

	                        	<th>SKS</th>

	                        	<!--th>Dosen</th-->

	                        	<th>Nilai</th>

	                        	<!--th width="40">Detail Nilai</th-->

	                        </tr>

	                    </thead>

	                    <tbody>

							<?php $no = 1; foreach ($detail_khs as $row) { ?>

	                        <tr>

	                        	<td><?php echo $no; ?></td>

	                        	<td><?php echo $row->kd_matakuliah; ?></td>

                                <td><?php echo $row->nama_matakuliah; ?></td>

                                <td><?php echo $row->sks_matakuliah ?></td>

                                <!--td></td-->
                                <?php   $logged = $this->session->userdata('sess_login');
                                        if (($logged['userid'] == '61101') or ($logged['userid'] == '74101')) {
                                            $nilai = $this->db->query("select * from tbl_transaksi_nilai where NIMHSTRLNM = '".$npm."' and KDKMKTRLNM = '".$row->kd_matakuliah."' and THSMSTRLNM = '".substr($kode->kd_krs, 12,5)."' ")->row();
                                            //die("select * from tbl_transaksi_nilai where NIMHSTRLNM = '".$row->npm_mahasiswa."' and KDKMKTRLNM = '".$row->kd_matakuliah."' and THSMSTRLNM = '".substr($kode->kd_krs, 12,5)."' ");
                                        } else {
                                            $nilai = $this->db->query("select * from tbl_transaksi_nilai where NIMHSTRLNM = '".$npm."' and KDKMKTRLNM = '".$row->kd_matakuliah."' and THSMSTRLNM = '".substr($row->kd_krs, 12,5)."' ")->row();
                                        }
                                 ?>
                                <!--td></td-->
                                <?php  ?>

                                <td><?php echo $nilai->NLAKHTRLNM; ?></td>
                                <!-- <td><?php //echo $row->NLAKHTRLNM; ?></td> -->

                                <!--td class="td-actions">

                                	<a data-toggle="modal" class="btn btn-success btn-small" href="#detil"><i class="btn-icon-only icon-ok"> </i></a>

                                </td-->

	                        </tr>

							<?php $no++; } ?>

	                    </tbody>

	               	</table>

				</div>

			</div>

		</div>

	</div>

</div>



<div class="modal fade" id="detil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Data Detail Nilai Mata Kuliah ...</h4>

            </div>

            <form class ='form-horizontal' action="#" method="post">

                <div class="modal-body">    

                    <table id="example2" class="table table-bordered table-striped">

	                  <thead>

	                        <tr> 

	                          <th>Absensi</th>

	                          <th>Tugas</th>

	                          <th>UTS</th>

							  <th>UAS</th>

							  <th>Catatan</th>

	                        </tr>

	                    </thead>

	                    <tbody>

	                    	<tr>

	                    		<td></td>

	                    		<td></td>

	                    		<td></td>

	                    		<td></td>

	                    		<td></td>

	                    	</tr>

	                    </tbody>

	                </table>

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->