<?php //echo $id_jadwal;die(); ?>

<script>
    function edit(id){
        $('#edit').load('<?= base_url();?>data/divisi/view_edit/'+id);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-book"></i>
  				<h3>Daftar Pengajaran</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Kode MK</th>
                                <th>Mata Kuliah</th>
                                <th>Hari/Ruang</th>
                                <th>Waktu</th>
                                <th>Kelas</th>
                                <th width="50">Peserta</th>
                                <th width="40">Lihat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($rows as $isi): ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $isi->kd_matakuliah; ?></td>
                                    <td><?= $isi->nama_matakuliah; ?></td>
                                    <td><?= notohari($isi->hari); ?></td>
                                    <td><?= del_ms($isi->waktu_mulai).' - '.del_ms($isi->waktu_selesai); ?></td>
                                    <td><?= $isi->kelas;?></td>
                                    <?php
                                        if ($isi->gabung > 0) {
                                            $jml_peserta = $this->db->query('SELECT 
                                                                                count(distinct npm_mahasiswa) as jml 
                                                                            from tbl_krs 
                                                                            where kd_jadwal = "'.$isi->kd_jadwal.'" 
                                                                            or kd_jadwal = "'.$isi->referensi.'" 
                                                                            or kd_jadwal IN 
                                                                                (SELECT DISTINCT kd_jadwal 
                                                                                FROM tbl_jadwal_matkul 
                                                                                WHERE referensi = "'.$isi->referensi.'")
                                                                            ')->row()->jml;
                                        } else {
                                            $jml_peserta = $this->db->query('SELECT 
                                                                                count(distinct npm_mahasiswa) as jml 
                                                                            from tbl_krs 
                                                                            where kd_jadwal = "'.$isi->kd_jadwal.'" 
                                                                            or kd_jadwal IN 
                                                                                (SELECT DISTINCT kd_jadwal 
                                                                                FROM tbl_jadwal_matkul 
                                                                                WHERE referensi = "'.$isi->kd_jadwal.'")
                                                                            ')->row()->jml;
                                        }
                                    ?>

                                    <td><?= $jml_peserta; ?></td>

                                    <td class="td-actions">

                                        <a 
                                            target="_blank" 
                                            href="<?= base_url('form/formnilai/view/'.$isi->id_jadwal);?>"  
                                            class="btn btn-primary btn-small">
                                            <i class="btn-icon-only icon-ok"> </i>
                                        </a>

                                    </td>

                                </tr>    

                            <?php $no++; endforeach ?>

                            

                        </tbody>

                    </table>
				</div>
			</div>
            <p>Page rendered in {elapsed_time} seconds.</p>
		</div>
	</div>
</div>