<?php
//var_dump(nama_dsn($kaprodi));die();


error_reporting(0);
//var_dump($rows);die();

$pdf = new FPDF("L","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetMargins(3, 3 ,0);

$pdf->SetFont('Arial','B',10); 


$pdf->Ln(0);

$pdf->Cell(200,5,strtoupper($title->prodi),0,0,'L');

$pdf->Ln(4);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(200,5,''.strtoupper($title->fakultas).' - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,0,'L');

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Dharmawangsa III No.1,Kebayoran Baru, Jakarta Selatan ',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS II',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Raya Perjuangan, Bekasi Barat',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(290,0,'',1,0,'C');


$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(80,5,$rows->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

if (substr($rows->kd_tahunajaran, 4,1) == 1) {
	$gj = 'Ganjil';
} else {
	$gj = 'Genap';
}


$pdf->Cell(50,5,$rows->semester_matakuliah.' / '.substr($rows->kd_tahunajaran, 0,4).' - '.$gj,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->nama,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(80,5,$rows->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NID',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kd_dosen,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(80,5,'Bekasi',0,0,'L');

$pdf->Cell(15,5,'KELAS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kelas,0,0,'L');



$pdf->ln(10);

$pdf->SetLeftMargin(3);

$pdf->SetFont('Arial','',12);
$pdf->SetLeftMargin(8);
$pdf->Cell(280,8,'DAFTAR NILAI PESERTA KULIAH '.strtoupper($rows->nama_matakuliah),1,0,'C');

$pdf->ln(8);

$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');

$pdf->Cell(80,10,'NAMA','L,T,R,B',0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(29,5,'KEHADIRAN','L,T,R,B',0,'C');

$pdf->Cell(55,5,'NILAI TUGAS','L,T,R,B',0,'C');

$pdf->Cell(12,10,'ABSEN','L,T,R,B',0,'C');

$pdf->Cell(13,5,'RATA RATA','L,T,R',0,'C');

$pdf->Cell(12,10,'UTS','L,T,R,B',0,'C');

$pdf->Cell(12,10,'UAS','L,T,R,B',0,'C');


$pdf->Cell(34,5,'JENIS NILAI','L,T,R,B',0,'C');

$pdf->ln(5);

$pdf->Cell(8,0,'',0,0,'C');

$pdf->Cell(25,0,'',0,0,'C');

$pdf->Cell(80,0,'',0,0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(14,5,'DOSEN','1',0,'C');
$pdf->Cell(15,5,'MAHASISWA','1',0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(11,5,'TUGAS 1','1',0,'C');
$pdf->Cell(11,5,'TUGAS 2','1',0,'C');
$pdf->Cell(11,5,'TUGAS 3','1',0,'C');
$pdf->Cell(11,5,'TUGAS 4','1',0,'C');
$pdf->Cell(11,5,'TUGAS 5','1',0,'C');

$pdf->Cell(12,0,'','',0,'C');

$pdf->Cell(13,5,'TUGAS','L,R,B',0,'C');

$pdf->Cell(12,0,'',0,0,'C');

$pdf->Cell(12,0,'',0,0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(17,5,'NILAI','1',0,'C');
$pdf->Cell(17,5,'HURUF','1',0,'C');

$no=1;
$noo=1;
if ($rows->kd_tahunajaran >= '20171') {
	$tblabsen = 'tbl_absensi_mhs_new_20171';
	$tblnilai = 'tbl_nilai_detail';
} elseif ($rows->kd_tahunajaran == '20162') {
	$tblabsen = 'tbl_absensi_mhs_new';
	$tblnilai = 'tbl_nilai_detail';
} else {
	$tblabsen = 'tbl_absensi_mhs';
	$tblnilai = 'tbl_nilai_detail_20161';
}

foreach ($mhs as $key) {
	
	$pdf->ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(8,5,$no,1,0,'C');

	$pdf->Cell(25,5,$key->NIMHSMSMHS,1,0,'C');

	$pdf->Cell(80,5,$key->NMMHSMSMHS,1,0,'L');
	//$absendosen = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs where kd_jadwal = '".$rows->kd_jadwal."'")->row();
	$absenmhs = $this->db->query("SELECT COUNT(npm_mahasiswa) as dua FROM ".$tblabsen." where kd_jadwal = '".$rows->kd_jadwal."' and npm_mahasiswa = '".$key->NIMHSMSMHS."' and (kehadiran = 'H')")->row();
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(14,5,$absendosen->satu,'1',0,'C');
	$pdf->Cell(15,5,$absenmhs->dua,'1',0,'C');

	// $absensi = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,2,$rows->kd_matakuliah)->row()->nilai;
	// $tugas = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,1,$rows->kd_matakuliah)->row()->nilai;
	// $tugas1 = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,5,$rows->kd_matakuliah)->row()->nilai;
	// $tugas2 = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,6,$rows->kd_matakuliah)->row()->nilai;
	// $tugas3 = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,7,$rows->kd_matakuliah)->row()->nilai;
	// $tugas4 = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,8,$rows->kd_matakuliah)->row()->nilai;
	// $tugas5 = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,9,$rows->kd_matakuliah)->row()->nilai;
	// $uts = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,3,$rows->kd_matakuliah)->row()->nilai;
	// $uas = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,4,$rows->kd_matakuliah)->row()->nilai;
	$qqq = $this->db->query("select distinct tipe,nilai from ".$tblnilai." 
							where npm_mahasiswa = '".str_replace(' ', '', $key->NIMHSMSMHS)."' and kd_jadwal = '".$rows->kd_jadwal."' ")->result();
	foreach ($qqq as $key) {
		switch ($key->tipe) {
			case '2':
				$absensi = $key->nilai;
				break;
			case '1':
				$tugas = $key->nilai;
				break;
			case '3':
				$uts = $key->nilai;
				break;
			case '4':
				$uas = $key->nilai;
				break;
			case '5':
				$tugas1 = $key->nilai;
				break;
			case '6':
				$tugas2 = $key->nilai;
				break;
			case '7':
				$tugas3 = $key->nilai;
				break;
			case '8':
				$tugas4 = $key->nilai;
				break;
			case '9':
				$tugas5 = $key->nilai;
				break;
			case '10':
				$rt = $key->nilai;
				break;
		}
	}
	$pdf->SetFont('Arial','',6);
	if (is_null($tugas1) or $tugas1 == '') {
		$tugas1 = '-';
	}
	if (is_null($tugas2) or $tugas2 == '') {
		$tugas2 = '-';
	} 
	if (is_null($tugas3) or $tugas3 == '') {
		$tugas3 = '-';
	}
	if (is_null($tugas4) or $tugas4 == '') {
		$tugas4 = '-';
	}
	if (is_null($tugas5) or $tugas5 == '') {
		$tugas5 = '-';
	}
	$pdf->Cell(11,5,$tugas1,'1',0,'C');
	$pdf->Cell(11,5,$tugas2,'1',0,'C');
	$pdf->Cell(11,5,$tugas3,'1',0,'C');
	$pdf->Cell(11,5,$tugas4,'1',0,'C');
	$pdf->Cell(11,5,$tugas5,'1',0,'C');

	$pdf->Cell(12,5,$absensi,'L,R,B',0,'C');

	$pdf->SetFont('Arial','',6);
	$pdf->Cell(13,5,$tugas,'1',0,'C');
	$pdf->Cell(12,5,$uts,'1',0,'C');
	$pdf->Cell(12,5,$uas,'1',0,'C');


	//fungsi ambil data nilai dari database
	//$rt = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,10,$rows->kd_matakuliah)->row()->nilai;

	$logged = $this->session->userdata('sess_login');
	if (($logged['userid'] == '74101') or ($rows->kd_tahunajaran >= '20171')) {
		$rtnew = number_format($rt,2);
		if (($rtnew >= 80) and ($rtnew <= 100)) {
			$rw = "A";
		} elseif (($rtnew >= 76) and ($rtnew <= 79.99)) {
			$rw = "A-";
		} elseif (($rtnew >= 72) and ($rtnew <= 75.99)) {
			$rw = "B+";
		} elseif (($rtnew >= 68) and ($rtnew <= 71.99)) {
			$rw = "B";
		} elseif (($rtnew >= 64) and ($rtnew <= 67.99)) {
			$rw = "B-";
		} elseif (($rtnew >= 60) and ($rtnew <= 63.99)) {
			$rw = "C+";
		} elseif (($rtnew >= 56) and ($rtnew <= 59.99)) {
			$rw = "C";
		} elseif (($rtnew >= 45) and ($rtnew <= 55.99)) {
			$rw = "D";
		} elseif (($rtnew >= 0) and ($rtnew <= 44.99)) {
			$rw = "E";
		} elseif ($rtnew >100) { 
			$rw = "T";
		}
	} else {
		$rtnew = number_format($rt,2);
		if (($rtnew >= 80) and ($rtnew <= 100)) {
			$rw = "A";
		} elseif (($rtnew >= 65) and ($rtnew <= 79.99)) {
			$rw = "B";
		} elseif (($rtnew >= 55) and ($rtnew <= 64.99)) {
			$rw = "C";
		} elseif (($rtnew >= 45) and ($rtnew <= 54.99)) {
			$rw = "D";
		} elseif (($rtnew >= 0) and ($rtnew <= 44.99)) {
			$rw = "E";
		}
	}

	$pdf->SetFont('Arial','',6);
	$pdf->Cell(17,5,$rtnew,'1',0,'C');
	$pdf->Cell(17,5,$rw,'1',0,'C');

	// for ($i=0; $i < 16; $i++) { 

	// 	$pdf->Cell(11,5,'',1,0,'C');	

	// }

	$no++;
	$noo++;

	if ($noo == 21) {
		$noo = 1;

		$pdf->ln(8);
		
		$pdf->Cell(110,5,'',0,0,'C');
		$pdf->Cell(30,5,'Kepala Program Studi',0,0,'C');
		$x = $pdf->GetX();
		$y = $pdf->GetY();
		//$pdf->image('http://172.16.2.42:801/assets/ttd_baa.png',($x-30),($y+2),27);
		$pdf->Cell(50,5,'',0,0,'C');
		$pdf->Cell(113,5,'Dosen Pengajar',0,0,'C');

		$pdf->ln(16);
		$pdf->Cell(110,5,'',0,0,'C');
		$pdf->Cell(30,5,nama_dsn($kaprodi),0,0,'C');
		$pdf->Cell(50,5,'',0,0,'C');
		$pdf->Cell(113,5,$rows->nama,0,0,'C');

$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetMargins(3, 3 ,0);

$pdf->SetFont('Arial','B',10); 

//$pdf->Image(''.base_url().'assets/img/logo-albino.png',60,30,90);

$pdf->Ln(0);

$pdf->Cell(200,5,strtoupper($title->prodi),0,0,'L');

$pdf->Ln(4);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(200,5,strtoupper($title->fakultas),0,0,'L');

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Dharmawangsa III No.1,Kebayoran Baru, Jakarta Selatan ',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS II',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Raya Perjuangan, Bekasi Barat',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(280,0,'',1,0,'C');



$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->semester_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->nama,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NID',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kd_dosen,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,'Bekasi',0,0,'L');

$pdf->Cell(15,5,'KELAS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kelas,0,0,'L');


$pdf->ln(10);
$pdf->SetLeftMargin(3);

$pdf->SetFont('Arial','',12);

$pdf->Cell(280,8,'DAFTAR NILAI PESERTA KULIAH '.strtoupper($rows->nama_matakuliah),1,0,'C');

$pdf->ln(8);

$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');

$pdf->Cell(80,10,'NAMA','L,T,R,B',0,'C');

$pdf->SetFont('Arial','',6);

$pdf->Cell(29,5,'KEHADIRAN','L,T,R,B',0,'C');

$pdf->Cell(55,5,'NILAI TUGAS','L,T,R,B',0,'C');

$pdf->Cell(12,10,'ABSEN','L,T,R,B',0,'C');

$pdf->Cell(13,5,'RATA RATA','L,T,R',0,'C');

$pdf->Cell(12,10,'UTS','L,T,R,B',0,'C');

$pdf->Cell(12,10,'UAS','L,T,R,B',0,'C');


$pdf->Cell(34,5,'JENIS NILAI','L,T,R,B',0,'C');

$pdf->ln(5);

$pdf->Cell(8,0,'',0,0,'C');

$pdf->Cell(25,0,'',0,0,'C');

$pdf->Cell(80,0,'',0,0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(14,5,'DOSEN','1',0,'C');
$pdf->Cell(15,5,'MAHASISWA','1',0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(11,5,'TUGAS 1','1',0,'C');
$pdf->Cell(11,5,'TUGAS 2','1',0,'C');
$pdf->Cell(11,5,'TUGAS 3','1',0,'C');
$pdf->Cell(11,5,'TUGAS 4','1',0,'C');
$pdf->Cell(11,5,'TUGAS 5','1',0,'C');

$pdf->Cell(12,0,'','',0,'C');

$pdf->Cell(13,5,'TUGAS','L,R,B',0,'C');

$pdf->Cell(12,0,'',0,0,'C');

$pdf->Cell(12,0,'',0,0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(17,5,'NILAI','1',0,'C');
$pdf->Cell(17,5,'HURUF','1',0,'C');

	}

	//ending header

}

$pdf->ln(8);
		
$pdf->Cell(110,5,'',0,0,'C');
$pdf->Cell(30,5,'Kepala Program Studi',0,0,'C');
$x = $pdf->GetX();
$y = $pdf->GetY();
//$pdf->image('http://172.16.2.42:801/assets/ttd_baa.png',($x-30),($y+2),27);
$pdf->Cell(50,5,'',0,0,'C');
$pdf->Cell(113,5,'Dosen Pengajar',0,0,'C');

$pdf->ln(16);
$pdf->Cell(110,5,'',0,0,'C');
$pdf->Cell(30,5,nama_dsn($kaprodi),0,0,'C');
$pdf->Cell(50,5,'',0,0,'C');
$pdf->Cell(113,5,$rows->nama,0,0,'C');


$pdf->Output('DAFTAR_NILAI.PDF','I');



?>

