<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Penugasan Bimbingan</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>NIM</th>
	                        	<th>NAMA MAHASISWA</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($mhs as $row) {  $pecah = explode(';',$row->penugasan); $jumlah = count($pecah); for ($i=1; $i < $jumlah; $i++) { ?>
	                        <tr>
	                        	<td><?php echo number_format($no); ?></td>
	                        	<td><?php echo $pecah[$i]; ?></td>
	                        	<?php $mahasiswa = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$pecah[$i],'NIMHSMSMHS','asc')->row(); ?>
                                <td><?php echo $mahasiswa->NMMHSMSMHS; ?></td>
	                        	<td class="td-actions">
									<a class="btn btn-success btn-small" href="#"><i class="btn-icon-only icon-print"> </i></a>
                                    <!--a class="btn btn-primary btn-small" href="#"><i class="btn-icon-only icon-pencil"> </i></a>
									<a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="#"><i class="btn-icon-only icon-remove"> </i></a-->
								</td>
	                        </tr>
	                        <?php $no++; }} ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>