<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Form KRS</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <b><center>FORM KRS</center></b><br>
          <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url();?>form/formkrsmhs/verify">
            <fieldset>
              <div class="control-group">                     
                <label class="control-label">ID Pembimbing</label>
                <div class="controls">
                  <input type="text" class="span3" name="username" placeholder="ID Pembimbing" required>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->
              <div class="control-group">                     
                <label class="control-label">Kelas</label>
                <div class="controls">
                  <select name="kelas" required class="span3">
                    <option value="PG">Reguler Pagi</option>
                    <option value="SR">Reguler Sore</option>
                    <option value="PK">P2K / Karyawan</option>
                  </select>
                </div> <!-- /controls -->   
              </div> <!-- /control-group -->
              <!-- <div class="control-group">                     
                <label class="control-label">Verifikasi</label>
                <div class="controls">
                  <div class="g-recaptcha" data-sitekey="6LeLLAoUAAAAAPFxEQf5C2sIkhPKj2zNj2C9gaOp"></div>     
                </div>  
              </div> <!-- /control-group --> -->    
              
              <div class="form-actions">
                <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                <input type="reset" class="btn btn-warning" value="Reset"/>
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>