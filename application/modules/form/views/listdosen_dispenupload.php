<div class="row">
	<div class="span12">      		  		
  		<div class="widget">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Daftar dispensasi unggah nilai</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a href="<?= base_url('form/excepupload') ?>" class="btn btn-success">Tambah dosen</a>
                    <a onclick="return confirm('Yakin?')" href="<?= base_url('form/excepupload/endAllDispen') ?>" class="btn btn-danger pull-right">Hapus semua dispensasi</a>
                    <hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>NID</th>
	                        	<th>Nama dosen</th>
                                <th>Tahun akademik</th>
	                        	<th>Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach ($data as $row) { ?>
	                        <tr>
	                        	<td><?= $no; ?></td>
	                        	<td><?= $row->nid; ?></td>
                                <td><?= nama_dsn($row->nid); ?></td>
                                <td><?= $row->tahunakademik ?></td>
                                <td><a href="<?= base_url('form/excepupload/endDispen/'.$row->nid.'/'.$row->tahunakademik) ?>" title="hapus data dispensasi" class="btn btn-danger"><i class="icon icon-remove"></i></a>
                                </td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>