<?php

ob_start();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();





$pdf->SetMargins(3, 5 ,0);

 



//$pdf->image('http://172.16.2.42:801/assets/logo.gif',10,10,20);
$pdf->ln(1);
$pdf->ln(1);
$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(25,5,'',0,10);
$pdf->Cell(25,5,'',0,10);
$pdf->Cell(25,5,'',0,10);
$pdf->Cell(25,5,'',0,10);

$pdf->Ln(5);
$pdf->SetFont('Arial','U',14);
$pdf->Ln(3);
$pdf->Cell(200,5,'SURAT KETERANGAN CUTI AKADEMIK',0,3,'C');

$pdf->Ln(0);
$pdf->SetFont('Arial','',12); 
$pdf->Cell(200,10,'NOMOR : KET/C.A/....../....../'.date("Y").'/BAA-UBJ',0,5,'C');


$pdf->ln(5);
$pdf->SetFont('Arial','',11); 
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(0.1,5,'Kepala Biro Administrasi Akademik Universitas Bhayangkara Jakarta Raya dengan ini',0,0);
$pdf->Cell(10,20,'menerangkan bahwa :',0,1);


//$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(25,5,'',0,0);
$pdf->Cell(37,5,'NAMA',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(10,5,$idd->NMMHSMSMHS,0,1);

$pdf->ln(4);
$pdf->SetFont('Arial','','12');
$pdf->Cell(25,10,'',0,0);
$pdf->Cell(37,0,'NPM',0,0);
$pdf->Cell(5,0,':',0,'L');
$pdf->Cell(10,0,$idd->NIMHSMSMHS,0,1);

$pdf->ln(2);
$pdf->SetFont('Arial','','12');
$pdf->Cell(25,10,'',0,0);
$pdf->Cell(37,10,'SEMESTER',0,0);
$pdf->Cell(5,10,':',0,'L');
$pdf->Cell(5,10,$semester,0,1,'L');

$pdf->ln(2);
$pdf->SetFont('Arial','','12');
$pdf->Cell(25,10,'',0,0);
$pdf->Cell(37,0,'PRODI',0,0);
$pdf->Cell(5,0,':',0,'L');
$pdf->Cell(27,0,$idd->prodi,0,0);
$pdf->Cell(5,5,'',0,1,'L');

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(25,5,'',0,0);
$pdf->Cell(37,5,'ALAMAT',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Multicell(120,5,trim($idd->alamat),0,'L');

$a = explode('-', $idd->transaksi_terakhir);
$thn = $a[0];
$bln = $a[1];
$tgl = $a[2];

if ($bln == '01') {
	$blnn = 'Januari';
} elseif ($bln == '02') {
	$blnn = 'Februari';
} elseif ($bln == '03') {
	$blnn = 'Maret';
} elseif ($bln == '04') {
	$blnn = 'April';
} elseif ($bln == '05') {
	$blnn = 'Mei';
} elseif ($bln == '06') {
	$blnn = 'Juni';
} elseif ($bln == '07') {
	$blnn = 'Juli';
} elseif ($bln == '08') {
	$blnn = 'Agustus';
} elseif ($bln == '09') {
	$blnn = 'September';
} elseif ($bln == '10') {
	$blnn = 'Oktober';
} elseif ($bln == '11') {
	$blnn = 'November';
} elseif ($bln == '12') {
	$blnn = 'Desember';
} 

$tahh = $this->app_model->getdetail('tbl_tahunakademik','kode',$this->session->userdata('tahunajaran'),'kode','asc')->row();
$pecahtahun = explode(' - ', $tahh->tahun_akademik);

$pdf->ln(5);
$pdf->SetFont('Arial','',12); 
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(0,5,'Berdasarkan pembayaran mahasiswa tanggal '.$tgl.' '.$blnn.' '.$thn.' tentang cuti akademik,',0,0);


$pdf->ln(5);
$pdf->SetFont('Arial','','12');
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(10,10,'bahwa mahasiswa yang bersangkutan disetujui untuk cuti akademik semester '.$pecahtahun[1].' T.A',0,0);


$pdf->ln(8);
$pdf->SetFont('Arial','','12');
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(10,10,''.$pecahtahun[0].', selanjutnya mahasiswa yang bersangkutan sudah menyelesaikan administrasi',0,0);
$pdf->Cell(10,10,'',0,0);

$pdf->ln(8);
$pdf->SetFont('Arial','','12');
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(10,10,'keuangan di BPAK. Apabila mahasiswa yang bersangkutan akan aktif kembali diharapkan',0,0);
$pdf->Cell(10,10,'',0,0);

$pdf->ln(8);
$pdf->SetFont('Arial','','12');
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(10,10,'mengisi formulir pengajuan aktif kembali.',0,0);
$pdf->Cell(10,10,'',0,0);

$pdf->ln(8);
$pdf->SetFont('Arial','','12');
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(10,10,'Demikian surat keterangan ini dibuat untuk dapat dipergunakan seperlunya.',0,0);
$pdf->Cell(10,10,'',0,1);



$pdf->ln(8);

$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',12);
$pdf->Cell(5,10,'',0,0);

date_default_timezone_set('Asia/Jakarta'); 

$pdf->Cell(170,5,'Jakarta, '.date('d-m-Y').'',0,1,'R');

$pdf->Ln(1);
$pdf->SetFont('Arial','',12);
$pdf->Cell(137,10,'',0,0);
$pdf->Cell(10,7,'A.n Wakil Rektor I',0,0);

$pdf->Ln();
$pdf->SetFont('Arial','',12);
$pdf->Cell(120,10,'',0,0);
$pdf->Cell(10,7,'Kepala Biro Administrasi Akademik',0,0);

$pdf->Ln();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(10,7,'',0,1);
$pdf->image('http://172.16.2.42:801/assets/ttd_baa.png',140,195,40);

$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','U',12);
$pdf->Cell(123,5,'',0,0);
$pdf->Cell(65,5,'ROULY G. RATNA. S, ST., MM',0,1);

$pdf->Ln(1);
$pdf->SetFont('Arial','',12);
$pdf->Cell(140,5,'',0,0);
$pdf->Cell(65,5,'NIP: 0607108',0,1);

$pdf->Ln();

$pdf->ln(5);
$pdf->SetFont('Arial','U','12');
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(35,0,'Tembusan yth:',0,0);
$pdf->Cell(5,5,'',0,1,'L');

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(35,0,'1. Program Studi '.$idd->prodi,0,0);
$pdf->Cell(5,5,'',0,1,'L');

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(35,0,'2. BPAK',0,0);
$pdf->Cell(5,5,'',0,1,'L');

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(35,0,'3. Arsip',0,0);
$pdf->Cell(5,5,'',0,0,'L');

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(35,0,'',0,0);
$pdf->Cell(5,5,'',0,0); 

$pdf->Ln(1);
$pdf->SetFont('Arial','I',9);
$pdf->Cell(133,10,'',0,0);
$pdf->Cell(65,5,'Tanggal cetak '.date('d-m-Y H:i:s'),0,1);

$pdf->Output();


ob_end_flush();
?>

