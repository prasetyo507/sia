<?php 

$logged = $this->session->userdata('sess_login');
$prodi = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$logged['userid'],'kd_prodi','asc')->row()->prodi;

$mons = array(1 => "Januari", 2 => "Februari", 3 => "Maret", 4 => "April", 5 => "Mei", 6 => "Juni", 7 => "Juli", 8 => "Agustus", 9 => "September", 10 => "Oktober", 11 => "November", 12 => "Desember");

$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
//border
$excel->getActiveSheet()->getStyle('B3:C3')->applyFromArray($BStyle);
//$excel->getActiveSheet()->getStyle('E3:F3')->applyFromArray($BStyle);

$excel->setActiveSheetIndex(0);

$excel->getActiveSheet()->setTitle('FORM MONITORING DOSEN');
//header
$excel->getActiveSheet()->setCellValue('B1', 'FORM MONITORING DOSEN - PRODI '.$prodi.'');
$excel->getActiveSheet()->setCellValue('B2', 'Bulan '.$mons[$bulan].' - 2017/2018 - GANJIL');
$excel->getActiveSheet()->setCellValue('B3', 'NID / Nama');
$nama = $this->app_model->getdetail('tbl_karyawan','nid',$nid,'nid','asc')->row()->nama;
$excel->getActiveSheet()->setCellValue('C3', $nid.' - '.$nama);
// $excel->getActiveSheet()->setCellValue('E3', 'Jumlah SKS');
// $excel->getActiveSheet()->setCellValue('F3', '');

//isi mahasiswa
$excel->getActiveSheet()->setCellValue('A6', 'NO');
$excel->getActiveSheet()->setCellValue('B6', 'KODE MK');
$excel->getActiveSheet()->setCellValue('C6', 'MATA KULIAH');
$excel->getActiveSheet()->setCellValue('D6', 'SKS');
$excel->getActiveSheet()->setCellValue('E6', 'KELAS');
$excel->getActiveSheet()->setCellValue('F6', 'JUMLAH MHS');
$excel->getActiveSheet()->setCellValue('G6', 'JUMLAH HADIR');
$excel->getActiveSheet()->setCellValue('H6', 'JUMLAH TIDAK HADIR');
$excel->getActiveSheet()->setCellValue('I6', 'TANGGAL');
$excel->getActiveSheet()->setCellValue('J6', 'PERTEMUAN');

$x = 7; $no = 1;foreach ($getmonitoring as $value) {
	$excel->getActiveSheet()->setCellValue('A'.$x.'', $no);
	$excel->getActiveSheet()->setCellValue('B'.$x.'', $value->kd_matakuliah);
	$excel->getActiveSheet()->setCellValue('C'.$x.'', $value->nama_matakuliah);
	$excel->getActiveSheet()->setCellValue('D'.$x.'', $value->sks_matakuliah);
	$excel->getActiveSheet()->setCellValue('E'.$x.'', $value->kelas);
	$excel->getActiveSheet()->setCellValue('F'.$x.'', $value->total);
	$excel->getActiveSheet()->setCellValue('G'.$x.'', $value->hadir);
	$excel->getActiveSheet()->setCellValue('H'.$x.'', ($value->ijin+$value->sakit+$value->alfa));
	$excel->getActiveSheet()->setCellValue('I'.$x.'', $value->tanggal);
	$excel->getActiveSheet()->setCellValue('J'.$x.'', $value->pertemuan);
	$x++;$no++;
}
$sampe = $x - 1;
$excel->getActiveSheet()->getStyle('A6:J'.$sampe.'')->applyFromArray($BStyle);
$xy = $x+1;
$a = $xy+1;
$d = $a+2;
$excel->getActiveSheet()->setCellValue('B'.$a.'', 'Wakil Dekan 2');
$excel->getActiveSheet()->setCellValue('B'.$d.'', '...');
$excel->getActiveSheet()->setCellValue('G'.$xy.'', 'Jakarta/Bekasi,'.date('d-m-Y').'');
$excel->getActiveSheet()->setCellValue('G'.$a.'', 'Ketua Program Studi');
$excel->getActiveSheet()->setCellValue('G'.$d.'', '...');

$filename = 'Form_Monitoring_Dosen_'.$nid.'_'.$nama.'_'.$prodi.'.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>