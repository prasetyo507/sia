<?php
//var_dump($mhs);die();

error_reporting(0);
//var_dump($rows);die();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();





$pdf->SetMargins(3, 3 ,0);

$pdf->SetFont('Arial','B',10); 



//$pdf->Image(''.base_url().'assets/img/logo-albino.png',60,30,90);

$pdf->Ln(0);

$pdf->Cell(200,5,strtoupper($title->prodi),0,0,'L');

$pdf->Ln(4);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(200,5,''.strtoupper($title->fakultas).' - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,0,'L');

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Dharmawangsa III No.1,Kebayoran Baru, Jakarta Selatan ',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS II',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Raya Perjuangan, Bekasi Barat',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(200,0,'',1,0,'C');



$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->semester_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->nama,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NIDN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kd_dosen,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,'Bekasi',0,0,'L');

$pdf->Cell(15,5,'KELAS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kelas,0,0,'L');



$pdf->ln(10);

$pdf->SetLeftMargin(35);

$pdf->SetFont('Arial','',12);

$pdf->Cell(133,8,'DAFTAR NILAI PESERTA KULIAH',1,0,'C');

$pdf->ln(8);



$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');
$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');
$pdf->Cell(60,10,'NAMA','L,T,R,B',0,'C');
$pdf->Cell(20,10,'NILAI','1',0,'C');
$pdf->Cell(20,10,'HURUF','1',0,'C');

$no=1;
$noo=1;
$pdf->ln(5);

foreach ($mhs as $key) {
	
	$pdf->ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(8,5,$no,1,0,'C');

	$pdf->Cell(25,5,$key->NIMHSMSMHS,1,0,'C');

	$pdf->Cell(60,5,$key->NMMHSMSMHS,1,0,'L');

	$rt = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,10,$rows->kd_matakuliah)->row()->nilai;
	//var_dump($rt);exit();
	$logged = $this->session->userdata('sess_login');
	if (($logged['userid'] == '74101') or ($rows->kd_tahunajaran >= '20171')) {
		$rtnew = number_format($rt,2);
		if (($rtnew >= 80) and ($rtnew <= 100)) {
			$rw = "A";
		} elseif (($rtnew >= 76) and ($rtnew <= 79.99)) {
			$rw = "A-";
		} elseif (($rtnew >= 72) and ($rtnew <= 75.99)) {
			$rw = "B+";
		} elseif (($rtnew >= 68) and ($rtnew <= 71.99)) {
			$rw = "B";
		} elseif (($rtnew >= 64) and ($rtnew <= 67.99)) {
			$rw = "B-";
		} elseif (($rtnew >= 60) and ($rtnew <= 63.99)) {
			$rw = "C+";
		} elseif (($rtnew >= 56) and ($rtnew <= 59.99)) {
			$rw = "C";
		} elseif (($rtnew >= 45) and ($rtnew <= 55.99)) {
			$rw = "D";
		} elseif (($rtnew >= 0) and ($rtnew <= 44.99)) {
			$rw = "E";
		} elseif ($rtnew >100) { 
			$rw = "T";
		}
	} else {
		$rtnew = number_format($rt,2);
		if (($rtnew >= 80) and ($rtnew <= 100)) {
			$rw = "A";
		} elseif (($rtnew >= 65) and ($rtnew <= 79.99)) {
			$rw = "B";
		} elseif (($rtnew >= 55) and ($rtnew <= 64.99)) {
			$rw = "C";
		} elseif (($rtnew >= 45) and ($rtnew <= 54.99)) {
			$rw = "D";
		} elseif (($rtnew >= 0) and ($rtnew <= 44.99)) {
			$rw = "E";
		} elseif ($rtnew == 101) {
			$rw = "T";
			$rtnew = '-';
		}
	}

	$pdf->SetFont('Arial','',6);
	$pdf->Cell(20,5,$rtnew,'1',0,'C');
	$pdf->Cell(20,5,$rw,'1',0,'C');

	$no++;
	$noo++;

	if ($noo == 31) {
		$noo = 1;

		$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetMargins(3, 3 ,0);

$pdf->SetFont('Arial','B',10); 


$pdf->Ln(0);

$pdf->Cell(200,5,strtoupper($title->prodi),0,0,'L');

$pdf->Ln(4);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(200,5,strtoupper($title->fakultas),0,0,'L');

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Dharmawangsa III No.1,Kebayoran Baru, Jakarta Selatan ',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS II',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Raya Perjuangan, Bekasi Barat',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(200,0,'',1,0,'C');



$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->semester_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->nama,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NIDN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kd_dosen,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(20,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,'Bekasi',0,0,'L');




$pdf->ln(10);

$pdf->SetLeftMargin(35);

$pdf->SetFont('Arial','',12);

$pdf->Cell(133,8,'DAFTAR NILAI PESERTA KULIAH',1,0,'C');

$pdf->ln(8);



$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');
$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');
$pdf->Cell(60,10,'NAMA','L,T,R,B',0,'C');
$pdf->Cell(20,10,'NILAI','1',0,'C');
$pdf->Cell(20,10,'HURUF','1',0,'C');
$pdf->ln(5);

	}

	//ending header

}


$pdf->Output('DAFTAR_NILAI.PDF','I');



?>

