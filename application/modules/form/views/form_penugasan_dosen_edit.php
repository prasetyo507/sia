<script type="text/javascript">

jQuery(document).ready(function($) {

    $('input[name^=dosen1]').autocomplete({

        source: '<?php echo base_url('perkuliahan/jdl_kuliah/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.dosen1.value = ui.item.value;

            this.form.kd_dosen1.value = ui.item.nid;

        }

    });

    $('input[name^=dosen2]').autocomplete({

        source: '<?php echo base_url('perkuliahan/jdl_kuliah/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.dosen2.value = ui.item.value;

            this.form.kd_dosen2.value = ui.item.nid;

        }

    });

});

</script>

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Form Penugasan Dosen Pembimbing</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>form/penugasandosen/savedata" method="post" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="control-group" id="">

                        <label class="control-label">Judul Skripsi</label>

                        <div class="controls">

                            <textarea name="judul_skripsi"><?php echo $mhsces->judul_skripsi; ?></textarea>

                        </div>

                    </div>

                    <?php
                        $nama1 = $this->db->query("SELECT * from tbl_karyawan where nid = '".$mhsces->pembimbing1."'")->row();
                        $nama2 = $this->db->query("SELECT * from tbl_karyawan where nid = '".$mhsces->pembimbing2."'")->row();
                    ?>

                    <div class="control-group" id="">

                        <label class="control-label">Pembimbing 1</label>

                        <div class="controls">

                            <input type="hidden" name="id" value="" >

                            <input type="text" id='dosen1' name="dosen1" value="<?php echo $nama1->nama; ?>">

                            <input type="hidden" id='kd_dosen1' name="kd_dosen1" value="<?php echo $mhsces->pembimbing1; ?>" >

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Pembimbing 2</label>

                        <div class="controls">

                            <input type="text" id='dosen2' name="dosen2" value="<?php echo $nama2->nama; ?>">

                            <input type="hidden" id='kd_dosen2' name="kd_dosen2" value="<?php echo $mhsces->pembimbing1; ?>" >

                        </div>

                    </div>

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>