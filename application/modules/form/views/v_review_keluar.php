<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>form/formcuti/looad/'+edc);
    }
</script>

<?php if ($tipp == 'P') {
    $type = 'Pindah';
} else {
    $type = 'Keluar';
}
 ?>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-home"></i>
                <h3>Daftar Permohonan <?= $type; ?> Mahasiswa</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <a class="btn btn-success" href="<?= base_url('form/formpindah/cetak_review') ?>">
                        <i class="btn-icon-only icon-print"> Print Daftar Keluar</i>
                    </a>
                    <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Angkatan</th>
                                <th>Prodi</th>
                                <th>Print</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($list->result() as $row) { ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <td><?= $row->npm; ?></td>
                                <td><?= get_nm_mhs($row->npm); ?></td>
                                <td><?= substr($row->npm, 0, 4); ?></td>
                                <td><?= get_jur($row->prodi); ?></td>
                                <td>
                                    <a class="btn btn-success btn-small" href="<?= base_url(); ?>form/formpindah/cetak_surat_pindah/<?= $row->npm.'.'.$row->tahunakademik; ?>">
                                        <i class="btn-icon-only icon-print"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>