<script type="text/javascript">
  jQuery(document).ready(function($) {
    $("#conditional").hide();
    $('input[name^=npm]').autocomplete({
      source: '<?php echo base_url('form/formpindah/autocomplete');?>',
      minLength: 4,
      select: function (evt, ui) {
        this.form.npm.value = ui.item.value;
      }
    });
  });
</script>

<script type="text/javascript">

  function hidecon() {
    $("#conditional").hide();
  }

  function showcon() {
    $("#conditional").show();
  }

</script>

<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-user"></i>
        <h3>Data Pengajuan Pindah dan Pengunduran Diri</h3>
      </div> <!-- /widget-header -->

      <div class="widget-content">
        <div class="tabbable">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#banyak" data-toggle="tab">Review Aktifitas</a></li>
            <li><a href="#satuan" data-toggle="tab">Cetak Form Pindah</a></li>
          </ul>           
          <br>         
          <div class="tab-content">
            <div class="tab-pane active" id="banyak">
              <div class="span11">
                <form method="post" class="form-horizontal" action="<?=base_url(); ?>form/formpindah/savesess">
                  <fieldset>

                    <script>
                      $(document).ready(function(){
                        $('#faks').change(function(){
                          $.post('<?= base_url()?>form/formpindah/get_jurusan/'+$(this).val(),{},function(get){
                            $('#jurs').html(get);
                          });
                        });
                      });
                    </script>

                    <div class="control-group">
                      <label class="control-label">Review</label>
                      <div class="controls">
                        <select class="form-control span6" name="tipe" id="">
                          <option selected="" disabled="">--Pilih Jenis Review--</option>
                          <option value="K">Keluar</option>
                          <option value="P">Pindah</option>
                        </select>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Tahun Akademik</label>
                      <div class="controls">
                        <select class="form-control span6" name="tahunakademik" id="">
                          <option>--Pilih Tahun Akademik--</option>
                          <?php foreach ($year as $row) { ?>
                          <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>  
                    <br/>

                    <div class="form-actions">
                      <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
                    </div> <!-- /form-actions -->

                  </fieldset>
                </form>
              </div>
            </div>
            <div class="tab-pane" id="satuan">
              <div class="span11">
                <form class="form-horizontal" action="<?= base_url(); ?>form/formpindah/addData" method="post">
                  <fieldset>

                    <div class="control-group">
                      <label class="control-label">NPM Mahasiswa </label>
                      <div class="controls">
                        <input class="form-control span3" placeholder="Isi dengan NPM Mahasiswa" type="text" name="npm" required="">
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Tipe</label>
                      <div class="controls">
                        <select name="tipe" class="form-control span3" required>
                            <option>-- Pilih Tipe Pengunduran Diri --</option>
                            <option onclick="showcon()" value="P">Pindah</option>
                            <option onclick="hidecon()" value="K">Keluar</option>
                        </select>
                      </div>
                    </div>

                    <div class="control-group" id="conditional">
                      <label class="control-label">Universitas tujuan</label>
                      <div class="controls">
                        <input class="form-control span3" placeholder="Universitas Tujuan" type="text" name="destinasi">
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Kelas  </label>
                      <div class="controls">
                        <select name="kelas" class="form-control span3" required>
                            <option>-- Pilih Kelas --</option>
                            <option value="PG">Pagi</option>
                            <option value="SR">Sore</option>
                            <option value="KY">Karyawan</option>
                        </select>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">No.Tlp / Hp  </label>
                      <div class="controls">
                        <input class="form-control span3" type="text"  name="hp" required="">
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Alamat </label>
                      <div class="controls">
                        <textarea class="form-control span4" name="alamat" required>
                        </textarea>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Alasan pindah </label>
                      <div class="controls">
                        <textarea class="form-control span4" name='alasan' required>
                        </textarea>
                      </div>
                    </div>

                    <div class="form-actions">
                      <input class="btn btn-large btn-primary" type="submit" value="Submit">
                      <input class="btn btn-large btn-default" type="reset" value="Clear">
                    </div>

                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

