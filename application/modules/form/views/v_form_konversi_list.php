<?php 
//var_dump($mhs);die();
 ?>
<script>
function edit(id){
$('#absen').load('<?php echo base_url();?>akademik/ajar/view_absen/'+id);
}
</script>

<style>
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}

.ada_nilai{
    background-color: #D9534F;
}

</style>


<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-list"></i>

                <h3>Data Mahasiswa Konversi - <?php echo $this->session->userdata('angkatan'); ?></h3>

            </div> <!-- /widget-header -->

            

            <div class="widget-content">

                <div class="span11">

                    <table id="example1" class="table">

                        <thead>

                            <tr> 

                                <th>No</th>

                                <th>NPM</th>

                                <th>Nama</th>

                                <th>SKS Konversi</th>

                                <th width="80">Aksi</th>

                            </tr>

                        </thead>

                        <tbody>
                            <?php $no=1; foreach ($mhs as $isi) { ?>
                                <tr  <?php if(is_null($isi->id)){echo 'class="ada_nilai"';} 
                                        $user = $this->session->userdata('sess_login');
                                        $sks = $this->app_model->sum_sks_konversi($isi->NIMHSMSMHS,$user['userid'],$isi->SMAWLMSMHS)->sks;
                                        ?> >

                                    <td><?php echo $no; ?></td>

                                    <td><?php echo $isi->NIMHSMSMHS; ?></td>

                                    <td><?php echo $isi->NMMHSMSMHS; ?></td>

                                    <td><?php echo $sks; ?></td>

                                    <td>
                                        <a href="<?php echo base_url();?>form/formnilai_konversi/save_session_mhs/<?php echo $isi->NIMHSMSMHS; ?>" 
                                         class="btn btn-primary btn-small" data-toggle="tooltip" title="Input Nilai Konversi">
                                            <i class="btn-icon-only icon-pencil"></i>
                                        </a>
                                        <a href="<?php echo base_url();?>form/formnilai_konversi/cetak_transkip/<?php echo $isi->NIMHSMSMHS; ?>" 
                                            target='_blank' class="btn btn-warning btn-small" data-toggle="tooltip" title="Print Transkip">
                                            <i class="btn-icon-only icon-print"></i>
                                        </a>
                                    </td>

                               </tr>
                            <?php $no++; } ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>