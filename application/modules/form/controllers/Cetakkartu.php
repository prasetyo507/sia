<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetakkartu extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('Cfpdf');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(76)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
		$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'form/cetakkartu_view';
		$this->load->view('template',$data);	
	}

	function get_jurusan($id){

		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Prodi--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function save_session()
	{
		$fakultas = explode('-',$this->input->post('fakultas'));
		$jurusan = explode('-',$this->input->post('jurusan'));
        $tahunajaran = $this->input->post('tahunajaran');
        $this->session->set_userdata('tahunajaran', $tahunajaran);
		$this->session->set_userdata('id_fakultas_prasyarat', $fakultas[0]);
		$this->session->set_userdata('nama_fakultas_prasyarat', $fakultas[1]);
		$this->session->set_userdata('id_jurusan_prasyarat', $jurusan[0]);
		$this->session->set_userdata('nama_jurusan_prasyarat', $jurusan[1]);
		redirect(base_url('form/cetakkartu/view_mhs'));
	}

	function view_mhs()
	{
		$data['page'] = 'form/cetakkartu_mhs';
		$this->load->view('template',$data);
	}

}

/* End of file Cetakkartu.php */
/* Location: ./application/modules/form/controllers/Cetakkartu.php */