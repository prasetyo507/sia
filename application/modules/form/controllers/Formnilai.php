<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formnilai extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		error_reporting(0);
		$this->load->model('setting_model');
		if ($this->session->userdata('sess_login') == TRUE) {
			
			$user = $this->session->userdata('sess_login');
			$akses = $this->role_model->cekakses(58)->result();
			$aktif = $this->setting_model->getaktivasi('nilai')->result();
			
		} else {
			redirect('auth', 'refresh');
		}
		//$this->load->library('excel');
	}

	function index() {
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i = 0; $i < $jmlh; $i++) {
			$grup[] = $pecah[$i];
		}
		if ((in_array(8, $grup))) {
			$this->session->unset_userdata('sess_dosen');
			redirect('form/formnilai/authdosen', 'refresh');
		} else {
			$this->session->unset_userdata('sess_dosen');
			redirect('form/formnilai/authdosenlog', 'refresh');
		}
	}

	function view($id) 
	{
		ini_set('memory_limit', '256M');
		$this->session->set_userdata('id_jadwal', $id);

		$jadwal = $this->db->select('kd_jadwal')->where('id_jadwal', $id)->get('tbl_jadwal_matkul')->row();
		$user = $this->session->userdata('sess_dosen');

		$data['mk'] = $this->app_model->get_mk_by_jadwal($jadwal->kd_jadwal)->row();
		$data['kode_jadwal'] = $jadwal->kd_jadwal;

		$data['id_jadwal'] = $id;
		$data['referensi'] = $jadwal->referensi;
		$data['logged'] = $this->session->userdata('sess_login');
		$data['tahunpilih'] = $user['tahun'];
		$data['look'] = $this->db->query("SELECT distinct a.NIMHSMSMHS,a.NMMHSMSMHS,b.kd_matakuliah,b.kd_jadwal
											FROM tbl_mahasiswa a
											JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
											WHERE b.`kd_jadwal` = '" . $data['kode_jadwal'] . "'
											ORDER BY NIMHSMSMHS")->result();
		if ($data['tahunpilih'] < 20171) {
			$data['page'] = 'form/nilai_lihat_lama';
		} else {
			$data['page'] = 'form/nilai_lihat';
		}
		$this->load->view('template', $data);
	}

	function dwld_excel($id) 
	{
		$data['tayang_nilai'] = 'tidak';
		$data['rows'] 	= $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
											JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
											JOIN tbl_kurikulum_matkul_new d ON a.`kd_matakuliah` = d.`kd_matakuliah`
											WHERE id_jadwal = ' . $id . ' 
											AND b.kd_prodi = SUBSTR(a.kd_jadwal,1,5)')->row();

		$data['ping'] 	= $this->db->query('SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_mahasiswa
											WHERE NIMHSMSMHS IN 
												(SELECT distinct npm_mahasiswa FROM tbl_krs
												WHERE kd_jadwal = "' . $data['rows']->kd_jadwal . '")
											')->result();

		$kdprodi = substr($data['rows']->kd_jadwal, 0, 5);
		$data['prodi'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$kdprodi,'kd_prodi','asc')->row();
		$data['absendosen'] = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs_new_20171
												where kd_jadwal = '" . $data['rows']->kd_jadwal . "'")->row();

		$data['jmlh'] = $this->db->query("SELECT count(distinct npm_mahasiswa) as jumlah from tbl_krs
										where kd_jadwal = '" . $data['rows']->kd_jadwal . "' ")->row();

		$this->load->library('excel');
		if ((strtoupper($data['rows']->nama_matakuliah) == 'SKRIPSI')
			OR (strtoupper($data['rows']->nama_matakuliah) == 'SEMINAR DAN TUGAS AKHIR')
			OR (strtoupper($data['rows']->nama_matakuliah) == 'KULIAH KERJA MAHASISWA')
			OR (strtoupper($data['rows']->nama_matakuliah) == 'SKRIPSI DAN SEMINAR TUGAS AKHIR')
			OR ($data['rows']->nama_matakuliah == 'Seminar dan Tugas Akhir')
			OR ($data['rows']->nama_matakuliah == 'KULIAH KERJA NYATA')
			OR ($data['rows']->nama_matakuliah == 'Kuliah Kerja Nyata (Magang Kerja)')
			OR ($data['rows']->nama_matakuliah == 'Kuliah Kerja Praktek')
			OR (strtoupper($data['rows']->nama_matakuliah) == 'MAGANG KERJA')
			OR (strtoupper($data['rows']->nama_matakuliah) == 'KERJA PRAKTEK')
			OR ($data['rows']->nama_matakuliah == 'Tesis')
			OR ($data['rows']->nama_matakuliah == 'TESIS')) {

			$this->load->view('welcome/phpexcel_nilai_skripsi', $data);

		} else {

			$this->load->view('welcome/phpexcel_nilai', $data);

		}
	}

	function upload_excel() 
	{
		$testype = $this->input->post('tipeuji', TRUE);
		$session = $this->session->userdata('sess_dosen');
		$user = $session['userid'];
		$tahun = $session['tahun'];

		if ($_FILES['userfile']) {
			$t = $this->input->post('tipe');
			$kd = $this->input->post('kode_jdl');
			//$id_jadwal = $this->input->post('id_jadwal');
			$mk = $this->db->query('SELECT distinct a.kd_matakuliah,b.nama_matakuliah,b.semester_matakuliah
									FROM tbl_jadwal_matkul a
									JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
									WHERE a.`kd_jadwal` = "' . $kd . '" AND SUBSTR(a.kd_jadwal,1,5) = b.kd_prodi')->row();

			$kd_jadwal = $this->db->query('SELECT * from tbl_jadwal_matkul where kd_jadwal = "' . $kd . '"')->row();

			$data['jmlh'] = $this->db->query('SELECT count(distinct npm_mahasiswa) as jumlah from tbl_krs
                								where kd_jadwal = "' . $kd_jadwal->kd_jadwal . '"')->row();

			$this->load->helper('inflector');

			$sanitize = $this->security->sanitize_filename($_FILES['userfile']['name']);
			$nama = rand(1, 10000000) . underscore(str_replace('/', '', $sanitize));
			$kadal = explode('.', $nama);
			if (count($kadal) > 2) {
				echo "<script>alert('FORMAT FILE MENCURIGAKAN!');history.go(-1)';</script>";exit();
			}

			$jumlah = $data['jmlh']->jumlah + 8;
			$config['upload_path'] = './upload/score/';
			$config['allowed_types'] = 'xls|xlsx';
			$config['file_name'] = $nama;
			$config['max_size'] = 1000000;

			if (getenv('HTTP_CLIENT_IP')) {
				$ipaddress = getenv('HTTP_CLIENT_IP');
			} else if (getenv('HTTP_X_FORWARDED_FOR')) {
				$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			} else if (getenv('HTTP_X_FORWARDED')) {
				$ipaddress = getenv('HTTP_X_FORWARDED');
			} else if (getenv('HTTP_FORWARDED_FOR')) {
				$ipaddress = getenv('HTTP_FORWARDED_FOR');
			} else if (getenv('HTTP_FORWARDED')) {
				$ipaddress = getenv('HTTP_FORWARDED');
			} else if (getenv('REMOTE_ADDR')) {
				$ipaddress = getenv('REMOTE_ADDR');
			} else {
				$ipaddress = 'UNKNOWN';
			}

			$ip2 = $_SERVER['HTTP_X_REAL_IP'];

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('userfile')) {
				$data = array('error', $this->upload->display_errors());
				var_dump($data);exit();
				echo "<script>alert('Gagal');
				document.location.href='" . base_url() . "form/formnilai/';</script>";
			} else {

				// insert file upload
				$dfuser = $this->session->userdata('sess_login');
				$datf['nama_file'] = $config['file_name'];
				$datf['waktu'] = date('Y-m-d H:i:s');
				$datf['tipe'] = 1;
				$datf['kode'] = $kd;
				$datf['path'] = $config['upload_path'] . $nama;
				$datf['userid'] = $dfuser['userid'];
				$datf['mac_addr'] = $_HASIL;
				$datf['ip_addr'] = $ipaddress;
				$datf['ip2'] = $ip2;
				$this->app_model->insertdata('tbl_file_upload', $datf);

				$upload_data = $this->upload->data();

				$this->load->library('excel_reader');
				$this->excel_reader->setOutputEncoding('CP1251');
				$file = $upload_data['full_path'];
				$this->excel_reader->read($file);
				$data = $this->excel_reader->sheets[0];

				$dataexcel = array();

				if ((strtoupper($mk->nama_matakuliah) == 'SKRIPSI')
					OR (strtoupper($mk->nama_matakuliah) == 'SEMINAR DAN TUGAS AKHIR')
					OR ($mk->nama_matakuliah == 'KULIAH KERJA MAHASISWA')
					OR ($mk->nama_matakuliah == 'Kuliah Kerja Mahasiswa')
					OR ($mk->nama_matakuliah == 'KULIAH KERJA NYATA')
					OR ($mk->nama_matakuliah == 'Kuliah Kerja Nyata (Magang Kerja)')
					OR ($mk->nama_matakuliah == 'Kuliah Kerja Praktek')
					OR (strtoupper($mk->nama_matakuliah) == 'MAGANG KERJA')
					OR (strtoupper($mk->nama_matakuliah) == 'KERJA PRAKTEK')
					OR (strtoupper($mk->nama_matakuliah) == 'TESIS')) {

					for ($i = 8; $i < $jumlah; $i++) {

						$dataexcel[$i - 8]['npm'] = substr($data['cells'][$i][2], 0, 12);

						$dataexcel[$i - 8]['nilai_10'] = number_format($data['cells'][$i][4], 2);

						$kodeasli = $this->db->query("SELECT * from tbl_krs where kd_krs
		                								like concat('".$dataexcel[$i-8]['npm']."','".$tahun."%')
		                								and kd_matakuliah = '".$kd_jadwal->kd_matakuliah."'")->row();

						$dataexcel[$i - 8]['code'] = $kodeasli->kd_jadwal;

						$dataexcel[$i - 8]['tran'] = $v;

						$ceknpm = $this->db->query("SELECT * from tbl_krs
		                							where npm_mahasiswa = '" . $dataexcel[$i - 8]['npm'] . "'
		                							and kd_jadwal = '" . $kd . "'")->result();

						if (count($ceknpm) < 1) {
							echo "Gagal Upload, Inputan Data Kelas Tidak Sesuai.<a href='javascript:history.go(-1)'><< Kembali</a>";
							echo $kd.'--'.$tahun.'--'.$dataexcel[$i-8]['npm'].'--'.$kd_jadwal->kd_matakuliah;
							exit();
						}

						if ((($dataexcel[$i - 8]['nilai_10']) < 0) or (ctype_alpha($dataexcel[$i - 8]['nilai_10']))) {
							echo "Gagal Upload, Inputan Nilai Tidak Sesuai . <a href='javascript:history.go(-1)'><< Kembali</a>";
							exit();
						}

					}

					$data['nama_dokumen'] = $config['file_name'];

					$this->db->where('id_jadwal', $id_jadwal);
					$qq = $this->db->get('tbl_jadwal_matkul')->row();

					$this->tambahnilaiskripsi($dataexcel, $kd);

				} else {
					for ($i = 8; $i < $jumlah; $i++) {

						$dataexcel[$i - 8]['npm'] = substr($data['cells'][$i][2], 0, 12);

						// absen
						$dataexcel[$i - 8]['nilai_2'] = number_format($data['cells'][$i][11], 2);

						// average tugas
						$dataexcel[$i - 8]['nilai_1'] = number_format($data['cells'][$i][12], 2);

						// UTS
						$dataexcel[$i - 8]['nilai_3'] = number_format($data['cells'][$i][13], 2);

						// if dispensasi
						if ($testype == 'all') {
							$dataexcel[$i - 8]['nilai_4'] = number_format($data['cells'][$i][14], 2);
						} else {
							// prevent charging abnormal values (for UAS)
							$isAbsenFull = $this->app_model->bisa_ujiankah($kd_jadwal->kd_jadwal,$dataexcel[$i - 8]['npm']);
							if ($isAbsenFull >= 75) {
								// UAS
								$dataexcel[$i - 8]['nilai_4'] = number_format($data['cells'][$i][14], 2);
							} else {
								// UAS
								$dataexcel[$i - 8]['nilai_4'] = 0;
							}
						}
						
						

						// cell tugas
						$dataexcel[$i - 8]['nilai_5'] = number_format($data['cells'][$i][6], 2);
						$dataexcel[$i - 8]['nilai_6'] = number_format($data['cells'][$i][7], 2);
						$dataexcel[$i - 8]['nilai_7'] = number_format($data['cells'][$i][8], 2);
						$dataexcel[$i - 8]['nilai_8'] = number_format($data['cells'][$i][9], 2);
						$dataexcel[$i - 8]['nilai_9'] = number_format($data['cells'][$i][10], 2);
						// cell tugas end

						// guna menghalangi nilai akhir yg tidak sesuai
						// masa UAS
						if ($testype == 'uas') {
							$utsscore = $this->db->query('SELECT * from tbl_nilai_detail where kd_jadwal = "' . $kd . '" and tipe = "3"')->num_rows();

							if ($utsscore < 1 or is_null($utsscore)) {
								$finalscore = (0.1 * $dataexcel[$i - 8]['nilai_2']) + (0.2 * $dataexcel[$i - 8]['nilai_1']) + (0.4 * $dataexcel[$i - 8]['nilai_4']);

							} else {

								$ceknilaiuts = $this->db->query("SELECT nilai from tbl_nilai_detail where tipe = 3 and npm_mahasiswa = '".$dataexcel[$i - 8]['npm']."' and kd_jadwal = '".$kd ."'")->row()->nilai;

								if ($ceknilaiuts != $dataexcel[$i - 8]['nilai_3']) {
									$finalscore = (0.1 * $dataexcel[$i - 8]['nilai_2']) + (0.2 * $dataexcel[$i - 8]['nilai_1']) + $ceknilaiuts + (0.4 * $dataexcel[$i - 8]['nilai_4']);
								} else {
									$finalscore = (0.1 * $dataexcel[$i - 8]['nilai_2']) + (0.2 * $dataexcel[$i - 8]['nilai_1']) + (0.3 * $dataexcel[$i - 8]['nilai_3']) + (0.4 * $dataexcel[$i - 8]['nilai_4']);
								}
								
							}

							$tofloat = number_format($finalscore, 2);

							// masa UTS
						} elseif ($testype == 'uts') {
							$finalscore = (0.1 * $dataexcel[$i - 8]['nilai_2']) + (0.2 * $dataexcel[$i - 8]['nilai_1']) + (0.3 * $dataexcel[$i - 8]['nilai_3']);

							$tofloat = number_format($finalscore, 2);

							// dispensasi dosen
						} elseif ($testype == 'all') {
							$tofloat = number_format($data['cells'][$i][15], 2);
						} else {
							$tofloat = number_format($data['cells'][$i][4], 2);
						}

						// nilai akhir
						$dataexcel[$i - 8]['nilai_10'] = $tofloat;

						$dataexcel[$i - 8]['code'] = $kd_jadwal->kd_jadwal;
						$dataexcel[$i - 8]['tran'] = $v;

						if ($dataexcel[$i - 8]['nilai_5'] == '-') {
							$dataexcel[$i - 8]['nilai_5'] = NULL;
						} elseif ($dataexcel[$i - 8]['nilai_6'] == '-') {
							$dataexcel[$i - 8]['nilai_6'] = NULL;
						} elseif ($dataexcel[$i - 8]['nilai_7'] == '-') {
							$dataexcel[$i - 8]['nilai_7'] = NULL;
						} elseif ($dataexcel[$i - 8]['nilai_8'] == '-') {
							$dataexcel[$i - 8]['nilai_8'] = NULL;
						} elseif ($dataexcel[$i - 8]['nilai_9'] == '-') {
							$dataexcel[$i - 8]['nilai_9'] = NULL;
						}
						//var_dump($dataexcel[$i-8]['nilai_6']);exit();
						$ceknpm = $this->db->query("SELECT * from tbl_krs
		                							where npm_mahasiswa = '" . $dataexcel[$i - 8]['npm'] . "'
		                							and kd_jadwal = '" . $kd_jadwal->kd_jadwal . "'")->result();

						if (count($ceknpm) < 1) {
							echo "Gagal Upload, Inputan Data Kelas Tidak Sesuai . <a href='javascript:history.go(-1)'><< Kembali</a>";
							exit();
						}

						$datas[] = $dataexcel[$i - 8]['npm'];

						if ((($dataexcel[$i - 8]['nilai_1']) < 0)
							or (($dataexcel[$i - 8]['nilai_2']) < 0)
							or (($dataexcel[$i - 8]['nilai_3']) < 0)
							or (($dataexcel[$i - 8]['nilai_4']) < 0)
							or (($dataexcel[$i - 8]['nilai_1']) > 101)
							or (($dataexcel[$i - 8]['nilai_2']) > 101)
							or (($dataexcel[$i - 8]['nilai_3']) > 101)
							or (($dataexcel[$i - 8]['nilai_4']) > 101)
							or (ctype_alpha(($dataexcel[$i - 8]['nilai_1'])))
							or (ctype_alpha(($dataexcel[$i - 8]['nilai_3'])))
							or (ctype_alpha(($dataexcel[$i - 8]['nilai_4'])))) {

							echo "Gagal Upload, Inputan Nilai Tidak Sesuai . <a href='javascript:history.go(-1)'><< Kembali</a>";
							exit();

						}

						if ((($dataexcel[$i - 8]['nilai_10']) < 0) or (ctype_alpha($dataexcel[$i - 8]['nilai_10']))) {
							echo "Gagal Upload, Inputan Nilai Tidak Sesuai . <a href='javascript:history.go(-1)'><< Kembali</a>";
							exit();

						}

					}
					// var_dump(count(array_unique($datas)));exit();
					$data['nama_dokumen'] = $config['file_name'];
					$kd_jadwal = $this->db->query('SELECT * from tbl_jadwal_matkul where kd_jadwal = "'.$kd.'"')->row();
					//if ($kd_jadwal->gabung > 0) {
					//$data['jmlh'] = $this->db->query('select count(distinct npm_mahasiswa) as jumlah from tbl_krs where kd_jadwal = "'.$kd_jadwal->kd_jadwal.'" or kd_jadwal = "'.$kd_jadwal->referensi.'" or kd_jadwal IN
					//(SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$kd_jadwal->referensi.'")')->row();
					//} else {
					$data['jmlh'] = $this->db->query('SELECT count(distinct npm_mahasiswa) as jumlah from tbl_krs where kd_jadwal = "' . $kd_jadwal->kd_jadwal . '" ')->row();
					//}
					//$hitungpeserta = $this->db->query("SELECT count(distinct kd_krs) as jmlmhs from tbl_krs where kd_jadwal = '".$kd."'")->row()->jmlmhs;
					if ($data['jmlh']->jumlah != count(array_unique($datas))) {
						echo "Gagal Upload, Mohon cek data mahasiswa. <a href='javascript:history.go(-1)'><< Kembali</a>";
						var_dump($data['jmlh']);exit();
					}

					//$this->db->where('id_jadwal', $id_jadwal);
					//$qq=$this->db->get('tbl_jadwal_matkul')->row();
					if ((strtoupper($data['title']->nama_matakuliah) == 'SKRIPSI')
						OR (strtoupper($mk->nama_matakuliah) == 'SEMINAR DAN TUGAS AKHIR')
						OR (strtoupper($mk->nama_matakuliah) == 'SKRIPSI DAN SEMINAR TUGAS AKHIR')
						OR (strtoupper($data['title']->nama_matakuliah) == 'KULIAH KERJA MAHASISWA')
						OR ($data['title']->nama_matakuliah == 'Seminar dan Tugas Akhir')
						OR ($data['title']->nama_matakuliah == 'KULIAH KERJA NYATA')
						OR ($data['title']->nama_matakuliah == 'Kuliah Kerja Nyata (Magang Kerja)')
						OR ($data['title']->nama_matakuliah == 'Kuliah Kerja Praktek')
						OR (strtoupper($data['title']->nama_matakuliah) == 'MAGANG KERJA')
						OR (strtoupper($data['title']->nama_matakuliah) == 'KERJA PRAKTEK')
						OR ($data['title']->nama_matakuliah == 'Tesis')
						OR ($data['title']->nama_matakuliah == 'TESIS')) {

						$this->tambahnilaiskripsi($dataexcel, $kd);

					} else {

						$this->tambahnilai($dataexcel, $kd, $testype);

					}

				}

			}

		}

	}

	function tambahnilai($dataarray, $kd, $ttype) {
		$userr = $this->session->userdata('sess_dosen');
		$nikk = $userr['userid'];
		$tahunn = $userr['tahun'];

		/**
		 * jika masa UAS, data nilai UTS tidak dapat diganti
		 * maka hanya nilai UTS yang tidak di delete
		 */
		if ($ttype == 'uas') {
			$this->db->where('tipe !=', 3);
			$this->db->where('kd_jadwal', $kd);
			$this->db->delete('tbl_nilai_detail');
		} else {
			$this->db->where('kd_jadwal', $kd);
			$this->db->delete('tbl_nilai_detail');
		}

		for ($i = 0; $i < count($dataarray); $i++) {
			$mk = $this->db->query('SELECT distinct a.kd_matakuliah,b.nama_matakuliah,b.semester_matakuliah
        							FROM tbl_jadwal_matkul a
									JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
									WHERE a.`kd_jadwal` = "' . $kd . '"
									AND SUBSTR(a.kd_jadwal,1,5) = b.kd_prodi')->row();

			$data['lala'] = $this->db->query('SELECT * from tbl_verifikasi_krs a join tbl_mahasiswa b
    											on a.`npm_mahasiswa`= b.`NIMHSMSMHS`
    											where a.`npm_mahasiswa`="' . $dataarray[$i]['npm'] . '"
    											and tahunajaran = "' . $tahunn . '"')->row();

			for ($z = 1; $z < 11; $z++) {

				$a = $dataarray[$i]['nilai_' . $z . ''];
				$b = $a[0];
				$c = $a[1];

				if ($a == '' or is_null($a)) {
					$a = NULL;
				}

				if ($ttype == 'uas') {
					if ($z != 3) {
						$datax[] = [
							'npm_mahasiswa' => $dataarray[$i]['npm'],
							'kd_krs' => $data['lala']->kd_krs,
							'kd_matakuliah' => $mk->kd_matakuliah,
							'tahun_ajaran' => $tahunn,
							'tipe' => $z,
							'kd_prodi' => $data['lala']->KDPSTMSMHS,
							'nid' => $nikk,
							'audit_date' => date('Y-m-d H:i:s'),
							'kd_transaksi_nilai' => $kd . 'zzz' . $dataarray[$i]['npm'], //$dataarray[$i]['tran'],
							'nilai' => $a,
							'flag_publikasi' => 1,
							'kd_jadwal' => $dataarray[$i]['code'],
						];
					}
				} elseif ($ttype == 'uts') {
					if ($z != 4) {
						$datax[] = [
							'npm_mahasiswa' => $dataarray[$i]['npm'],
							'kd_krs' => $data['lala']->kd_krs,
							'kd_matakuliah' => $mk->kd_matakuliah,
							'tahun_ajaran' => $tahunn,
							'tipe' => $z,
							'kd_prodi' => $data['lala']->KDPSTMSMHS,
							'nid' => $nikk,
							'audit_date' => date('Y-m-d H:i:s'),
							'kd_transaksi_nilai' => $kd . 'zzz' . $dataarray[$i]['npm'], //$dataarray[$i]['tran'],
							'nilai' => $a,
							'flag_publikasi' => 1,
							'kd_jadwal' => $dataarray[$i]['code'],
						];
					}
				} elseif ($ttype == 'all') {
					$datax[] = [
						'npm_mahasiswa' => $dataarray[$i]['npm'],
						'kd_krs' => $data['lala']->kd_krs,
						'kd_matakuliah' => $mk->kd_matakuliah,
						'tahun_ajaran' => $tahunn,
						'tipe' => $z,
						'kd_prodi' => $data['lala']->KDPSTMSMHS,
						'nid' => $nikk,
						'audit_date' => date('Y-m-d H:i:s'),
						'kd_transaksi_nilai' => $kd . 'zzz' . $dataarray[$i]['npm'], //$dataarray[$i]['tran'],
						'nilai' => $a,
						'flag_publikasi' => 1,
						'kd_jadwal' => $dataarray[$i]['code'],
					];
				}

			}

		}
		$datax = $this->security->xss_clean($datax);
		$this->db->insert_batch('tbl_nilai_detail', $datax);
		$idjadwal = $this->app_model->getdetail('tbl_jadwal_matkul', 'kd_jadwal', $kd, 'kd_jadwal', 'asc')->row()->id_jadwal;
		echo "<script>alert('Sukses');
		document.location.href='" . base_url() . "form/formnilai/view/" . $idjadwal . "';</script>";

	}

	function tambahnilaiskripsi($dataarray, $kd) {

		$userr = $this->session->userdata('sess_dosen');
		$nikk = $userr['userid'];
		$tahunn = $userr['tahun'];
		$this->db->query('delete from tbl_nilai_detail where kd_jadwal like "%' . $kd . '%"');
		for ($i = 0; $i < count($dataarray); $i++) {
			$mk = $this->db->query('SELECT distinct a.kd_matakuliah,b.nama_matakuliah,b.semester_matakuliah FROM tbl_jadwal_matkul a
									JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
									WHERE a.`kd_jadwal` = "' . $kd . '" AND SUBSTR(a.kd_jadwal,1,5) = b.kd_prodi')->row();

			$data['lala'] = $this->db->query('SELECT * from tbl_verifikasi_krs a join tbl_mahasiswa b
    										on a.`npm_mahasiswa`=b.`NIMHSMSMHS`
    										where a.`npm_mahasiswa`="' . $dataarray[$i]['npm'] . '"
    										and tahunajaran = "' . $tahunn . '"')->row();

			//for ($z=1; $z < 9; $z++) {
			$a = $dataarray[$i]['nilai_10'];
			$b = $a[0];
			$c = $a[1];

			$datax[] = array(
				'npm_mahasiswa' => $dataarray[$i]['npm'],
				'kd_krs' => $data['lala']->kd_krs,
				'kd_matakuliah' => $mk->kd_matakuliah,
				'tahun_ajaran' => $data['lala']->tahunajaran,
				'tipe' => 10,
				'kd_prodi' => $data['lala']->KDPSTMSMHS,
				'nid' => $nikk,
				'audit_date' => date('Y-m-d H:i:s'),
				'kd_transaksi_nilai' => $kd . 'zzz' . $dataarray[$i]['npm'], //$dataarray[$i]['tran'],
				'nilai' => $a,
				'flag_publikasi' => 1,
				'kd_jadwal' => $dataarray[$i]['code'],
			);

		}

		// var_dump($datax);exit();
		$datax = $this->security->xss_clean($datax);
		$this->db->insert_batch('tbl_nilai_detail', $datax);
		$idjadwal = $this->app_model->getdetail('tbl_jadwal_matkul', 'kd_jadwal', $kd, 'kd_jadwal', 'asc')->row()->id_jadwal;
		echo "<script>alert('Sukses'); document.location.href='" . base_url() . "form/formnilai/view/" . $idjadwal . "';</script>";

	}

	function cetak_nilai($id) {
		$this->load->library('Cfpdf');

		//$user = $this->session->userdata('sess_dosen');

		$data['id_jadwal'] = $id;

		//$nik = $user['userid'];

		$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
											JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
											WHERE id_jadwal = "' . $id . '" AND SUBSTR(a.kd_jadwal,1,5) = b.kd_prodi')->row();

		//var_dump($data['rows']);exit();

		if ($data['rows']->kd_tahunajaran >= '20171') {
			$tblabsen = 'tbl_absensi_mhs_new_20171';
		} elseif ($data['rows']->kd_tahunajaran == '20162') {
			$tblabsen = 'tbl_absensi_mhs_new';
		} else {
			$tblabsen = 'tbl_absensi_mhs';
		}

		$data['absendosen'] = $this->db->query("SELECT count(distinct pertemuan) as satu FROM " . $tblabsen . "
												where kd_jadwal = '" . $data['rows']->kd_jadwal . "'")->row();

		$data['title'] = $this->db->query('SELECT * FROM tbl_matakuliah a
											JOIN tbl_jurusan_prodi b ON a.`kd_prodi` = b.`kd_prodi`
											JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
											WHERE a.`kd_matakuliah` = "' . $data['rows']->kd_matakuliah . '"
											and a.kd_prodi = "' . $data['rows']->kd_prodi . '"')->row();
		$data['kaprodi']=$this->db->query('SELECT * FROM tbl_jurusan_prodi
											WHERE kd_prodi = "' . $data['rows']->kd_prodi . '"')->row()->kaprodi;
		// $data['mhs'] = $this->db->query('SELECT distinct mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS`,b.`kd_jadwal`,b.`kd_matakuliah` FROM tbl_krs b
		// 									JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
		// 									WHERE kd_jadwal = "'.$data['rows']->kd_jadwal.'" ORDER BY mhs.`NIMHSMSMHS` asc')->result();

		$data['mhs'] = $this->db->query('SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_mahasiswa
										WHERE NIMHSMSMHS IN (SELECT npm_mahasiswa FROM tbl_krs
										WHERE kd_jadwal = "' . $data['rows']->kd_jadwal . '")')->result();

		if ((strtoupper($data['title']->nama_matakuliah) == 'SKRIPSI')
			OR (strtoupper($mk->nama_matakuliah) == 'SEMINAR DAN TUGAS AKHIR')
			OR ($data['title']->nama_matakuliah == 'KULIAH KERJA MAHASISWA')
			OR ($data['title']->nama_matakuliah == 'Kuliah Kerja Mahasiswa')
			OR ($data['title']->nama_matakuliah == 'KULIAH KERJA NYATA')
			OR ($data['title']->nama_matakuliah == 'Kuliah Kerja Nyata (Magang Kerja)')
			OR ($data['title']->nama_matakuliah == 'Kuliah Kerja Praktek')
			OR (strtoupper($data['title']->nama_matakuliah) == 'MAGANG KERJA')
			OR (strtoupper($data['title']->nama_matakuliah) == 'KERJA PRAKTEK')
			OR ($data['title']->nama_matakuliah == 'Tesis')
			OR ($data['title']->nama_matakuliah == 'TESIS')) {

			$this->load->view('form/cetak_nilai_matkul_skripsi', $data);

		} else {

			$this->load->view('form/cetak_nilai_matkul', $data);

		}

	}

	function authdosen() {
		if ($this->session->userdata('sess_dosen') != TRUE) {
			$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'form_auth_nilai';
			$this->load->view('template', $data);
		} else {
			$user = $this->session->userdata('sess_dosen');
			$nik = $user['userid'];
			$tahun = $user['tahun'];
			$bro = $this->session->userdata('sess_login');
			$prodi = $bro['userid'];
			$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
												JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
												WHERE a.`kd_dosen` = "' . $nik . '" AND a.`kd_jadwal` like "' . $prodi . '%" and a.kd_tahunajaran = "' . $tahun . '" and b.kd_prodi = "' . $prodi . '"
												and (a.gabung IS NULL or a.gabung = 0)
												GROUP BY a.id_jadwal')->result();
			//}

			$data['page'] = 'form/nilai_view';
			$this->load->view('template', $data);
		}
	}

	function authdosenlog() 
	{
		if ($this->session->userdata('sess_dosen') != TRUE) {
			$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'form_auth_nilai';
			$this->load->view('template', $data);
		} else {
			$user = $this->session->userdata('sess_dosen');
			$nik = $user['userid'];
			$tahun = $user['tahun'];
			$data['rows'] 	= $this->db->query('SELECT distinct a.*,b.nama_matakuliah FROM tbl_jadwal_matkul a
												JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
												WHERE a.`kd_dosen` = "' . $nik . '" 
												and a.kd_tahunajaran = "' . $tahun . '"
												GROUP BY a.id_jadwal')->result();

			$data['page'] = 'form/nilai_view';
			$this->load->view('template', $data);
		}
	}

	function load_dosen_autocomplete() 
	{
		$bro = $this->session->userdata('sess_login');
		$prodi = $bro['userid'];
		$this->db->distinct();
		$this->db->select("a.id_kary,a.nid,a.nama");
		$this->db->from('tbl_karyawan a');
		$this->db->join('tbl_jadwal_matkul b', 'a.nid = b.kd_dosen');
		$this->db->like('b.kd_jadwal', $prodi, 'after');
		$this->db->like('a.nama', $_GET['term'], 'both');
		$this->db->or_like('a.nid', $_GET['term'], 'both');
		$sql = $this->db->get();
		$data = array();

		foreach ($sql->result() as $row) {
			$data[] = array(
				'id_kary' => $row->id_kary,
				'nid' => $row->nid,
				'value' => $row->nid . ' - ' . $row->nama,
			);
		}

		echo json_encode($data);
	}

	function load_mhs_autocomplete() 
	{

		$sql = $this->db->query('SELECT a.*,b.kd_krs FROM tbl_mahasiswa a
								JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
								JOIN tbl_jadwal_matkul c ON b.`kd_jadwal` = c.`kd_jadwal`
								WHERE c.`id_jadwal` = ' . $this->session->userdata('id_jadwal') . ' and (b.npm_mahasiswa like "%' . $_GET['term'] . '%" OR a.`NMMHSMSMHS` like "%' . $_GET['term'] . '%") ORDER BY NIMHSMSMHS');

		$data = array();

		foreach ($sql->result() as $row) {

			$data[] = array(

				'npm' => $row->NIMHSMSMHS,

				'value' => $row->NIMHSMSMHS . '-' . $row->NMMHSMSMHS,

				'kd_krs' => $row->kd_krs,

			);

		}

		echo json_encode($data);

	}

	function histori_nilai() {
		//cek ada atau nggak

		//tab1
		$id = $this->input->post('id_jadwal');

		$data['id_jadwal'] = $id;

		$data['kode_jadwal_bro'] = $this->db->select('kd_jadwal')->where('id_jadwal', $id)->get('tbl_jadwal_matkul')->row()->kd_jadwal;

		$data['ding'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul WHERE `id_jadwal` = "' . $id . '"')->row();

		$data['look'] = $this->db->query('SELECT * FROM tbl_mahasiswa a
											JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
											JOIN tbl_jadwal_matkul c ON b.`kd_jadwal` = c.`kd_jadwal`
											WHERE c.`id_jadwal` = "' . $id . '"
											AND b.kd_matakuliah = "' . $data['ding']->kd_matakuliah . '"
											ORDER BY NIMHSMSMHS')->result();

		$data['jdl_mk'] = $id;

		$this->session->set_userdata('idj', $id);
		$this->session->set_userdata('kdj', $data['kode_jadwal_bro']);

		//end tab1

		$mhs = explode('-', $this->input->post('npm'));

		$data['npm'] = $mhs[0];
		$data['nama'] = $mhs[1];

		$npm = $this->input->post('npm_mhs');
		$kd_jadwal = $this->input->post('kd_jadwal');

		$cek = $this->db->query("SELECT distinct npm_mahasiswa from tbl_nilai_detail where npm_mahasiswa = '" . $npm . "'
    								AND kd_jadwal = '" . $kd_jadwal . "' ")->result();

		$data['ctgs_1'] = $this->db->query("SELECT count(npm_mahasiswa) as byk_nol FROM tbl_nilai_detail WHERE tipe = 5 AND kd_jadwal = '" . $kd_jadwal . "' and nilai = 0")->row()->byk_nol;
		$data['ctgs_2'] = $this->db->query("SELECT count(npm_mahasiswa) as byk_nol FROM tbl_nilai_detail WHERE tipe = 6 AND kd_jadwal = '" . $kd_jadwal . "' and nilai = 0")->row()->byk_nol;
		$data['ctgs_3'] = $this->db->query("SELECT count(npm_mahasiswa) as byk_nol FROM tbl_nilai_detail WHERE tipe = 7 AND kd_jadwal = '" . $kd_jadwal . "' and nilai = 0")->row()->byk_nol;
		$data['ctgs_4'] = $this->db->query("SELECT count(npm_mahasiswa) as byk_nol FROM tbl_nilai_detail WHERE tipe = 8 AND kd_jadwal = '" . $kd_jadwal . "' and nilai = 0")->row()->byk_nol;
		$data['ctgs_5'] = $this->db->query("SELECT count(npm_mahasiswa) as byk_nol FROM tbl_nilai_detail WHERE tipe = 9 AND kd_jadwal = '" . $kd_jadwal . "' and nilai = 0")->row()->byk_nol;

		// $data['pembagi'] = $this->db->query("SELECT COUNT(tipe) AS bill FROM tbl_nilai_detail WHERE tipe IN(5,6,7,8,9) AND kd_jadwal = '".$kd_jadwal."' AND nilai = 0 AND npm_mahasiswa = '".$npm."'")->row()->bill;
		$data['ada'] = $this->db->query("SELECT COUNT(tipe) AS pop FROM tbl_nilai_detail WHERE tipe IN(5,6,7,8,9)
    										AND kd_jadwal = '" . $kd_jadwal . "' AND nilai > 0 AND npm_mahasiswa = '" . $npm . "'")->row()->pop;
		//var_dump($data['ada']);exit();
		$data['byk_mhs'] = $this->db->query('select count(npm_mahasiswa) as banyak from tbl_krs where kd_jadwal = "' . $kd_jadwal . '"')->row()->banyak;

		//die($ctgs_1.'---'.$ctgs_2.'---'.$ctgs_3.'---'.$byk_mhs);

		$data['nilai_edit'] = $this->db->query("SELECT DISTINCT a.`NMMHSMSMHS`,a.`NIMHSMSMHS`,b.`kd_jadwal`,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 1 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 2 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS absen,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 3 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS uts,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 4 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS uas,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 5 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas1,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 6 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas2,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 7 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas3,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 8 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas4,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 9 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS tugas5,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = 10 AND npm_mahasiswa = '" . $npm . "' and kd_jadwal = '" . $kd_jadwal . "' order by id_nilai desc limit 1) AS nilai_ahir
				FROM tbl_mahasiswa a
				JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
				WHERE b.`kd_jadwal` = '" . $kd_jadwal . "' AND b.npm_mahasiswa = '" . $npm . "' GROUP BY b.`kd_jadwal`")->row();
		//var_dump($data['nilai_edit']->tugas3);exit();
		$data['task1'] = $data['nilai_edit']->tugas1;
		$data['task2'] = $data['nilai_edit']->tugas2;
		$data['task3'] = $data['nilai_edit']->tugas3;
		$data['task4'] = $data['nilai_edit']->tugas4;
		$data['task5'] = $data['nilai_edit']->tugas5;
		$data['nama'] = $data['nilai_edit']->NMMHSMSMHS;

		$data['absenmhs'] = $this->db->query("SELECT COUNT(npm_mahasiswa) as dua FROM tbl_absensi_mhs_new_20171
												where kd_jadwal = '" . $kd_jadwal . "' and npm_mahasiswa = '" . $npm . "'
												and (kehadiran IS NULL or kehadiran = 'H')")->row()->dua;

		$data['absendosen'] = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs_new_20171
												where kd_jadwal = '" . $kd_jadwal . "'")->row()->satu;
		//var_dump($data['nilai']);die();
		$data['tayang_nilai'] = 'ada';
		if ($cek == TRUE) {
			$data['page'] = 'form/nilai_lihat';
			$this->load->view('template', $data);
		} else {
			echo "<script>alert('Nilai Belum Diinput');history.go(-1);</script>";
		}

	}

	function histori_nilai22() {

		//die($this->input->post('npm_mhs').''.$this->input->post('kd_jadwal'));

		$to = explode('-', $this->input->post('npm'));

		$sa = $to[0];

		$jdl_mk = $this->session->userdata('idj');

		$kdmk = $this->app_model->get_mk_by_jadwal($jdl_mk)->row();

		$hasil_kd = $kdmk->kd_matakuliah;

		$this->db->select('*');

		$this->db->where('id_jadwal', $jdl_mk);

		$qq = $this->db->get('tbl_jadwal_matkul')->row();

		$kd = $qq->kd_jadwal;

		$userr = $this->session->userdata('sess_dosen');

		$tahunn = $userr['tahun'];

		$cari = $this->db->query("SELECT DISTINCT a.`NMMHSMSMHS`,a.`NIMHSMSMHS`,b.`kd_jadwal`,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '1' AND npm_mahasiswa = '" . $sa . "') AS tugas,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '2' AND npm_mahasiswa = '" . $sa . "') AS absen,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '3' AND npm_mahasiswa = '" . $sa . "') AS uts,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '4' AND npm_mahasiswa = '" . $sa . "') AS uas,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '5' AND npm_mahasiswa = '" . $sa . "') AS tugas1,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '6' AND npm_mahasiswa = '" . $sa . "') AS tugas2,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '7' AND npm_mahasiswa = '" . $sa . "') AS tugas3,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '9' AND npm_mahasiswa = '" . $sa . "') AS tugas5,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '8' AND npm_mahasiswa = '" . $sa . "') AS tugas4,
				(SELECT nilai FROM tbl_nilai_detail WHERE tipe = '10' AND npm_mahasiswa = '" . $sa . "') AS akhir
				FROM tbl_mahasiswa a
				JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
				WHERE b.`kd_jadwal` = '" . $kd . "' AND b.npm_mahasiswa = '" . $sa . "' GROUP BY b.`kd_jadwal`")->result();

		$data = array();

		foreach ($sql->result() as $row) {

			$data[] = array(

				'npm' => $row->NIMHSMSMHS,

				'value' => $row->NIMHSMSMHS . '-' . $row->NMMHSMSMHS,

				'absen' => $row->absen,

				'tugas' => $row->tugas,

				'nilai_t1' => $row->tugas1,

				'nilai_t2' => $row->tugas2,

				'nilai_t4' => $row->tugas4,

				'nilai_t5' => $row->tugas5,

				'nilai_t3' => $row->tugas3,

				'uts' => $row->uts,

				'uas' => $row->uas,

				'akhir' => $row->akhir,

			);

		}

		echo json_encode($data);
	}

	function update_nilai() {

		$npm = $this->input->post('npm2');

		$kd_krs = $this->input->post('kd_krs');

		for ($i = 1; $i < 9; $i++) {
			$a = $this->input->post('tugas[' . $i . ']');
			//var_dump($a);die();

			$dataxx = array(
				'nilai' => $a,
			);

			$this->db->where('npm_mahasiswa', $npm);
			$this->db->where('kd_krs', $kd_krs);
			$this->db->where('tipe', $i);
			$this->db->update('tbl_nilai_detail', $dataxx);
		}

		echo "<script>alert('Sukses');
			document.location.href='" . base_url() . "form/formnilai/view/" . $this->session->userdata('id_jadwal') . "';</script>";
	}

	function update_nilai22() {
		$userr = $this->session->userdata('sess_dosen');

		$nikk = $userr['userid'];
		$tahunn = $userr['tahun'];
		$npm = $this->input->post('nim_mhs');
		$nim = explode('-', $npm);
		$no = $npm;
		$nilai = $this->input->post('tugas');
		$jdl_mk = $this->session->userdata('idj');
		$kdmk = $this->app_model->get_mk_by_jadwal($jdl_mk)->row();
		$hasil_kd = $kdmk->kd_matakuliah;

		$this->db->where('id_jadwal', $jdl_mk);
		$qq = $this->db->get('tbl_jadwal_matkul')->row();

		// var_dump($no.'---'.$qq->kd_jadwal);exit();

		$data['lala'] = $this->db->query('SELECT * from tbl_verifikasi_krs a join tbl_mahasiswa b
	    									on a.`npm_mahasiswa`=b.`NIMHSMSMHS`
	    									where a.`npm_mahasiswa`="' . $no . '"
	    									and tahunajaran = "' . $tahunn . '"')->row();

		for ($z = 1; $z < 11; $z++) {
			$a = $this->input->post('tugas[' . $z . ']');
			//var_dump($a);exit();
			$b = $a[0];
			$c = $a[1];

			$dataxx = array(
				'npm_mahasiswa' => $no,
				'kd_krs' => $data['lala']->kd_krs,
				'kd_matakuliah' => $qq->kd_matakuliah,
				'tahun_ajaran' => $data['lala']->tahunajaran,
				'tipe' => $z,
				'kd_prodi' => $data['lala']->KDPSTMSMHS,
				'nid' => $nikk,
				'audit_date' => date('Y-m-d H:i:s'),
				'kd_transaksi_nilai' => $qq->kd_jadwal . 'zzz' . $no,
				'nilai' => $a,
				'flag_publikasi' => 1,
				'kd_jadwal' => $qq->kd_jadwal,
			);

			// var_dump($dataxx);exit();

			$this->db->where('npm_mahasiswa', $no);
			$this->db->where('tipe', $z);
			$this->db->where('kd_jadwal', $qq->kd_jadwal);
			$this->db->where('tahun_ajaran', $tahunn);
			//var_dump($dataxx); exit();
			$this->db->update('tbl_nilai_detail', $dataxx);
		}

		echo "<script>alert('Sukses');
		document.location.href='" . base_url() . "form/formnilai/view/" . $this->session->userdata('id_jadwal') . "';</script>";
	}

}

/* End of file Formnilai.php */
/* Location: .//C/Users/danum246/AppData/Local/Temp/fz3temp-1/Formnilai.php */