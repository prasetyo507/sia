<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembimbing extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			/*$cekakses = $this->role_model->cekakses(63)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}*/
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$logged = $this->session->userdata('sess_login');
		$data['getData'] = $this->app_model->getpembimbingdsn($logged['userid'])->result();
		$data['page'] = 'form/pembimbing_view';
		$this->load->view('template',$data);
	}

	function view($id)
	{
		$logged = $this->session->userdata('sess_login');
		$data['mhs'] = $this->app_model->getpembimbingdsnall($id,$logged['userid'])->result();
		//var_dump($data['mhs']);exit();
		$data['page'] = 'form/pembimbing_detail';
		$this->load->view('template',$data);	
	}

}

/* End of file Pembimbing.php */
/* Location: ./application/modules/form/controllers/Pembimbing.php */