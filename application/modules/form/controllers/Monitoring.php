<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(161)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$data['page']='form_monitoring';
		$this->load->view('template', $data);
	}

	function cetak_monitoring()
	{
		$logged = $this->session->userdata('sess_login');
		$this->load->model('monitoring_model');
		$data['nid'] = $this->input->post('kd_dosen', TRUE);
		//$data['ta'] = $this->input->post('ta', TRUE);
		$data['bulan'] = $this->input->post('bulan', TRUE);
		$data['tahun'] = $this->input->post('tahun', TRUE);
		$data['getmonitoring'] = $this->monitoring_model->getmonitoring($data['nid'],$data['bulan'],$data['tahun'],$logged['userid']);
		//var_dump($data['getmonitoring']);exit();
		$this->load->library('excel');
		$this->load->view('phpexcel_monitoring', $data);
	}

	function cetak_rekap()
	{
		$logged = $this->session->userdata('sess_login');
		$this->load->model('monitoring_model');
		$data['nid'] = $this->input->post('kd_dosen', TRUE);
		$data['ta'] = $this->input->post('tahun', TRUE);
		$data['getmonitoring'] = $this->monitoring_model->getmonitoring_rekap($data['nid'],$data['ta'],$logged['userid']);
		//var_dump($data['getmonitoring']);exit();
		$this->load->library('excel');
		$this->load->view('phpexcel_monitoring_rekap', $data);
	}

}

/* End of file Monitoring.php */
/* Location: ./application/modules/form/controllers/Monitoring.php */