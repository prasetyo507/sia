<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beasiswa extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
		error_reporting(0);
		//$id_menu = 55 (database); cek apakah user memiliki akses
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(79)->result();
			if ($akses != TRUE) {
				redirect('home','refresh');
			}
		} else {
			redirect('auth','refresh');
		}
		$this->load->library('Cfpdf');
	}

	function index()
	{
		$user = $this->session->userdata('sess_login');

		$nik   = $user['userid'];
		
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ((in_array(20, $grup))) {
			

	        $data['page']='v_beasiswa_mhs';

	    }elseif ((in_array(5, $grup))){


	    	$data['page']='v_beasiswa';

	    }
		$this->load->view('template', $data);
	}

}
