<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formsidyud extends CI_Controller {

	function index()
	{
		$data['page'] = 'form/form_view';
		$this->load->view('template',$data);	
	}

	function form()
	{
		$logged = $this->session->userdata('sess_login');

		$npm=$logged['userid'];
		//die($npm);

		$data['rows'] = $this->db->where('nomor_pokok', $npm)->get('tbl_biodata')->row();
		
		
		if ($this->input->post('kelas', TRUE) == 1) {
			$data['page'] = 'form/form_sidang';
		} else {
			$data['page'] = 'form/form_yudisium';
		}


		$this->load->view('template',$data);	
	}

	// function sidang()
	// {
	// 	$data['page'] = 'form/form_sidang';
	// 	$this->load->view('template',$data);	
	// }

	// function yudisium()
	// {
	// 	$data['page'] = 'form/form_yudisium';
	// 	$this->load->view('template',$data);	
	// }

	public function simpan() {
		

		$npm = $this->input->post('npm_mhs');
		
		
	
	 
	    $files = $_FILES;
	    $cpt = count($_FILES['userfile']['name']);


	    for($i=0; $i<$cpt; $i++){

	    	if ($files['userfile']['name'][$i] != '') {
	    		//echo $files['userfile']['name'][$i].'-';

	    		// if ($i == 0) {
	    		// 	$tag = 'akta';		    		
	    		// }elseif ($i == 1) {
	    		// 	$tag = 'ijazah';
	    		// }elseif ($i == 2) {
	    		// 	$tag = 'bab1';
	    		// }elseif ($i == 3) {
	    		// 	$tag = 'bab2';
	    		// }elseif ($i == 4) {
	    		// 	$tag = 'bab3';
	    		// }elseif ($i == 5) {
	    		// 	$tag = 'bab4';
	    		// }elseif ($i == 6) {
	    		// 	$tag = 'bab5';
	    		// }
	    	
	    		$_FILES['userfile']['name']		= $files['userfile']['name'][$i];
		        $_FILES['userfile']['type']		= $files['userfile']['type'][$i];
		        $_FILES['userfile']['tmp_name']	= $files['userfile']['tmp_name'][$i];
		        $_FILES['userfile']['error']	= $files['userfile']['error'][$i];
		        $_FILES['userfile']['size']		= $files['userfile']['size'][$i];    
		 		
		        echo $_FILES['userfile']['name'].'-';

			     $config['upload_path']   = './arsip/'; 
		         $config['allowed_types'] = 'pdf'; 
		         $config['max_size']      = 4096; 
		         $this->load->library('upload', $config);
		         $this->upload->do_upload('userfile');

		        $datax = $this->upload->data();

		        $data = array('npm_mahasiswa' => $npm,'file' => $datax['full_path'],'tipe' => $i,'user_date' => date('Y-m-d H:i:s'));
		        $this->db->insert('tbl_arsip', $data);

	    	}
	 
		}

		if ($this->input->post('data_ubah') == 'F') {
				$data_edit = array('nama' => strtoupper($this->input->post('namaedit')),
									'ibu' => strtoupper($this->input->post('namaibuedit')),
									'tempat_lahir' => $this->input->post('tempatlahiredit'), 
									'tanggal_lahir' => $this->input->post('tanggallahiredit')  
								);

				$this->db->where('nomor_pokok', $npm);
				$this->db->update('tbl_biodata', $data_edit);
			}
 
}
private function set_upload_options(){   
	//  upload an image options
    $config = array();
    $config['upload_path'] 	 = base_url().'arsip';
    $config['allowed_types'] = 'pdf';
    $config['max_size']      = '4096';
    $config['overwrite']     = FALSE;
 
 
    return $config;
}
		
		// if(count($_FILES['upload']['name']) > 0){
  //       //Loop through each file
  //       for($i=0; $i<count($_FILES['upload']['name']); $i++) {
  //         //Get the temp file path
  //           $tmpFilePath = $_FILES['upload']['tmp_name'][$i];

  //           //Make sure we have a filepath
  //           if($tmpFilePath != ""){
            
  //               //save the filename
  //               $shortname = $_FILES['upload']['name'][$i];

  //               //save the url and the file
  //               $filePath = "uploaded/" . date('d-m-Y-H-i-s').'-'.$_FILES['upload']['name'][$i];

  //               //Upload the file into the temp dir
  //               if(move_uploaded_file($tmpFilePath, $filePath)) {

  //                   $files[] = $shortname;
  //                   //insert into db 
  //                   //use $shortname for the filename
  //                   //use $filePath for the relative url to the file

  //               }
  //             }
  //       }
       

}

/* End of file Formsidyud.php */
/* Location: ./application/modules/form/controllers/Formsidyud.php */