<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transkrip_wisudawan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Cfpdf');
		$session = $this->session->userdata('sess_login');

		if(!$session) {
			redirect('auth/logout','refresh');
		}

		$this->load->language('all_lang', 'english');
	}

	public function index()
	{	
		$string = 'Program Pendidikan Tinggi';

		$data = [
			'title' => 'Transkrip Wisudawan',
			'page' => 'transkrip-wisudawan/view_transkrip',
		];


		$this->load->view('template', $data);
	}

	public function findNpm($npm='')
	{
		$this->db->distinct();
        $this->db->select("NIMHSMSMHS,NMMHSMSMHS");
        $this->db->from('tbl_mahasiswa');
        $this->db->like('NIMHSMSMHS', $_GET['term'], 'both');
        $this->db->or_like('NMMHSMSMHS', $_GET['term'], 'both');
        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {
            $data[] = array(
                    'nama'          => $row->NMMHSMSMHS,
                    'value'         => $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS,
                    'nid'          => $row->NIMHSMSMHS,
                    );
        }
        echo json_encode($data);
	}

	function view_transkrip($npm = NULL) {	
		
		$npm = '201210215158';
		// $npm = '201210225045';
		// $npm = $this->session->userdata('npm-transkrip');
		$ijazahNumber = '622012019000051';

		$mhs = $this->db->query('SELECT * from tbl_mahasiswa a join tbl_jurusan_prodi b
									on a.`KDPSTMSMHS`=b.`kd_prodi` join tbl_fakultas c
									on b.`kd_fakultas`=c.`kd_fakultas`
									where a.`NIMHSMSMHS` = "'.$npm.'"')->row();

		
		$matkul = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah_en FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah_en FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah_en,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
										MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,
										MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM,
										deletedAt 
										FROM tbl_transaksi_nilai nl 
										WHERE  nl.`NIMHSTRLNM` = "'.$npm.'" and deletedAt is null or deletedAt = ""
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();

		$matkul_konversi = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM`  ORDER BY id_matakuliah DESC LIMIT 1) , 
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
										MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,
										MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM,
										deletedAt 
										FROM tbl_transaksi_nilai_konversi nl 
										WHERE nl.`NIMHSTRLNM` = "'.$npm.'"  and deletedAt is null or deletedAt = ""
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();
		
		$epro = 'S'.substr($npm, 4, 1);
		
		$kd_prodi = $mhs->KDPSTMSMHS;
		$prodi = get_jur($mhs->KDPSTMSMHS);

		$namaDekan = getInfoDekan($kd_prodi, 4);
		$nidnDekan = getInfoDekan($kd_prodi, 2);

		$nama = $mhs->NMMHSMSMHS;
		$ttl = $mhs->TPLHRMSMHS .', '.$mhs->TGLHRMSMHS;
		$tglLulus = $mhs->TGLLSMSMHS == '' ? '' : date('d F Y', strtotime($mhs->TGLLSMSMHS));

		$actYear = getActYear();

		$kdFakultas = $this->db->query('select kd_fakultas from tbl_jurusan_prodi where kd_prodi = "'.$kd_prodi.'"')->row()->kd_fakultas; 
		$fakultas = get_fak($kdFakultas);
		$fakultas = strtolower($fakultas);
		$fakultas = 'Fakultas '.ucfirst($fakultas);
		$judulSkripsi = $this->db->get_Where('tbl_skripsi', ['npm_mahasiswa' => $npm]);
		// $judulSkripsi = $this->db->get_Where('tbl_skripsi', ['npm_mahasiswa' => '201310115059']);

		if ($judulSkripsi->num_rows() > 0) 
			$judulSkripsi = $judulSkripsi->row()->judul_skripsi;
		else
			$judulSkripsi = '';

		$data= [
			'head'         => $mhs,
			'epro'         => $epro,
			'prodi'        => $prodi,
			'nama'         => $nama,
			'ttl'          => $ttl,
			'npm'          => $npm,
			'tglLulus'     => $tglLulus,
			'ijazahNumber' => $ijazahNumber,
			'matkul'       => $matkul,
			'matkul_konversi'       => $matkul_konversi,
			'judulSkripsi' => $judulSkripsi,
			'namaDekan'  => $namaDekan,		  
			'nidnDekan' => $nidnDekan,
			'fakultas' => trim($fakultas),
		];

		

		$html = $this->load->view('transkrip-wisudawan/tbl_transkrip-wisudawan', $data, true);
		// $this->load->view('transkrip-wisudawan/tbl_transkrip-wisudawan', $data);
		$mpdf = new \Mpdf\Mpdf();
		// satuan mm 
		$mpdf->AddPageByArray([
		    'margin-left' => 15,
		    'margin-right' => 15,
		    'margin-top' => 4,
		    'margin-bottom' => 0,
		]);
		$mpdf->WriteHTML($html);

		$mpdf->Output();
		
	}
	function view_trans($npm = NULL) {	
		
		$npm = '201210215158';
		// $npm = '201210225045';
		// $npm = $this->session->userdata('npm-transkrip');
		$ijazahNumber = '622012019000051';

		$mhs = $this->db->query('SELECT * from tbl_mahasiswa a join tbl_jurusan_prodi b
									on a.`KDPSTMSMHS`=b.`kd_prodi` join tbl_fakultas c
									on b.`kd_fakultas`=c.`kd_fakultas`
									where a.`NIMHSMSMHS` = "'.$npm.'"')->row();

		
		$matkul = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,

										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
										MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,
										MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM,
										deletedAt 
										FROM tbl_transaksi_nilai nl 
										WHERE nl.`NIMHSTRLNM` = "'.$npm.'" and deletedAt is null or deletedAt = ""
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();

		$matkul_konversi = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM`  ORDER BY id_matakuliah DESC LIMIT 1) , 
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
										MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,
										MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM,
										deletedAt 
										FROM tbl_transaksi_nilai_konversi nl 
										WHERE nl.`NIMHSTRLNM` = "'.$npm.'"  and deletedAt is null or deletedAt = ""
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();
		
		$epro = 'S'.substr($npm, 4, 1);
		
		$kd_prodi = $mhs->KDPSTMSMHS;
		$prodi = get_jur($mhs->KDPSTMSMHS);

		$namaDekan = getInfoDekan($kd_prodi, 4);
		$nidnDekan = getInfoDekan($kd_prodi, 2);

		$nama = $mhs->NMMHSMSMHS;
		$ttl = $mhs->TPLHRMSMHS .', '.$mhs->TGLHRMSMHS;
		$tglLulus = $mhs->TGLLSMSMHS == '' ? '' : date('d F Y', strtotime($mhs->TGLLSMSMHS));

		$actYear = getActYear();

		$kdFakultas = $this->db->query('select kd_fakultas from tbl_jurusan_prodi where kd_prodi = "'.$kd_prodi.'"')->row()->kd_fakultas; 
		$fakultas = get_fak($kdFakultas);
		$fakultas = strtolower($fakultas);
		$fakultas = 'Fakultas '.ucfirst($fakultas);
		$judulSkripsi = $this->db->get_Where('tbl_skripsi', ['npm_mahasiswa' => $npm]);
		// $judulSkripsi = $this->db->get_Where('tbl_skripsi', ['npm_mahasiswa' => '201310115059']);

		if ($judulSkripsi->num_rows() > 0) 
			$judulSkripsi = $judulSkripsi->row()->judul_skripsi;
		else
			$judulSkripsi = '';

		$data= [
			'head'         => $mhs,
			'epro'         => $epro,
			'prodi'        => $prodi,
			'nama'         => $nama,
			'ttl'          => $ttl,
			'npm'          => $npm,
			'tglLulus'     => $tglLulus,
			'ijazahNumber' => $ijazahNumber,
			'matkul'       => $matkul,
			'matkul_konversi'       => $matkul_konversi,
			'judulSkripsi' => $judulSkripsi,
			'namaDekan'  => $namaDekan,		  
			'nidnDekan' => $nidnDekan,
			'fakultas' => trim($fakultas),
		];



		$this->load->view('transkrip-wisudawan/tbl_transkrip-wisudawan_tyo', $data);
	}

	public function info($value='')
	{
		phpinfo();
	}
}

/* End of file Transkrip.php */
/* Location: ./application/controllers/Transkrip.php */