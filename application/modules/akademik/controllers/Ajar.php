<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajar extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->output->cache(60);
		$this->load->library('Cfpdf');
		$this->load->model('akademik/ajar_model','ajarmod');
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(71)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{	
	
		$user = $this->session->userdata('sess_login');

		$nik   = $user['userid'];
		
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		

		if ( (in_array(10, $grup)) or (in_array(1, $grup)) ) {
			
			$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();

	        $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

	        $data['page']='ajar_select';

	        $this->load->view('template', $data);

	    }elseif ((in_array(9, $grup))){

			$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

			$data['prodi'] = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas',$nik,'kd_fakultas', 'ASC')->result();

	        $data['page']='ajar_select_fak';

	        $this->load->view('template', $data);    	

	    }elseif ((in_array(8, $grup)) or (in_array(19, $grup))) {
	    	    	
	    	$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
	    	//$data['dosen'] = $this->app_model->getdata('tbl_karyawan', '', 'ASC')->result();

	        $data['page']='ajar_select_prodi';

	        $this->load->view('template', $data);

		}else{
			//redirect('absensi','refresh');
			$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page'] = 'akademik/ajar_select_ta';
			$this->load->view('template',$data);
		
		}
	}

	function save_session_dosen()
	{
		$user = $this->session->userdata('sess_login');
		$nik = $user['userid'];
		$tahunajaran = $this->input->post('tahunajaran');
		$this->session->set_userdata('tahunajaran', $tahunajaran);
		$data['rows'] 	= $this->db->query('SELECT distinct a.*,b.nama_matakuliah,b.sks_matakuliah FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											JOIN tbl_kurikulum_matkul_new c on c.kd_matakuliah = b.kd_matakuliah
											JOIN tbl_kurikulum_new d on d.kd_kurikulum = c.kd_kurikulum
											WHERE a.`kd_dosen` = "'.$nik.'" and a.kd_tahunajaran = "'.$this->session->userdata('tahunajaran').'"
											AND d.status = "1"
											AND b.kd_prodi = SUBSTR(a.kd_jadwal,1,5)
											group by a.kd_jadwal')->result();

		$data['nm_dosen']  = $this->db->where('nid', $nik)
						 ->get('tbl_karyawan')->row();

		$data['page'] = 'akademik/ajar_view';
		$this->load->view('template',$data);
	}

	function save_session(){

		$user = $this->session->userdata('sess_login');

		$nik   = $user['userid'];
		
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ( (in_array(10, $grup)) or (in_array(1, $grup)) ) {
			
			$fakultas = explode('-',$this->input->post('fakultas'));

			$jurusan = explode('-',$this->input->post('jurusan'));

	        $tahunajaran = $this->input->post('tahunajaran');
	       
	        $this->session->set_userdata('tahunajaran', $tahunajaran);

			$this->session->set_userdata('id_fakultas_prasyarat', $fakultas[0]);

			$this->session->set_userdata('nama_fakultas_prasyarat', $fakultas[1]);

			$this->session->set_userdata('id_jurusan_prasyarat', $jurusan[0]);

			$this->session->set_userdata('nama_jurusan_prasyarat', $jurusan[1]);

	    }elseif ((in_array(9, $grup))){

			$jurusan = explode('-',$this->input->post('jurusan'));

	        $tahunajaran = $this->input->post('tahunajaran');
	       
	        $this->session->set_userdata('tahunajaran', $tahunajaran);

			$this->session->set_userdata('id_fakultas_prasyarat', $nik);

			$this->session->set_userdata('id_jurusan_prasyarat', $jurusan[0]);

			$this->session->set_userdata('nama_jurusan_prasyarat', $jurusan[1]);

	    }elseif ((in_array(8, $grup)) or (in_array(19, $grup))) {

	        $tahunajaran = $this->input->post('tahunajaran');

	        $this->session->set_userdata('tahunajaran', $tahunajaran);

			$this->session->set_userdata('id_jurusan_prasyarat', $nik);

		}

		redirect(base_url('akademik/ajar/load_dosen_mengajar'));

	}

	function load_dosen_mengajar()
	{
		$user = $this->session->userdata('sess_login');

		$nik   = $user['userid'];
		
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		$data['dosen'] 	= $this->db->query('SELECT DISTINCT 
													jdl.`kd_dosen`,
													kry.`nama`,
													kry.`nid`,
													(SELECT SUM(mk.`sks_matakuliah`) AS sks 
													FROM tbl_jadwal_matkul jdl
		                                    		JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
		                                    		WHERE jdl.`kd_dosen` = kry.`nid` 
	                                    			and (mk.nama_matakuliah not like "tesis%" 
	                                    				and mk.nama_matakuliah not like "skripsi%" 
	                                    				and mk.nama_matakuliah not like "kerja praktek%" 
	                                    				and mk.nama_matakuliah not like "magang%" 
	                                    				and mk.nama_matakuliah not like "kuliah kerja%") 
	                                    			and jdl.kd_tahunajaran = "'.$this->session->userdata('tahunajaran').'" 
	                                    			and mk.kd_prodi = "'.$this->session->userdata('id_jurusan_prasyarat').'" 
	                                    			and jdl.kd_jadwal like "'.$this->session->userdata('id_jurusan_prasyarat').'%") AS sks 
                                    		FROM tbl_jadwal_matkul jdl
											JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
											JOIN tbl_karyawan kry ON jdl.`kd_dosen` = kry.`nid`
											where jdl.kd_tahunajaran = "'.$this->session->userdata('tahunajaran').'" 
											AND mk.kd_prodi = "'.$this->session->userdata('id_jurusan_prasyarat').'"
											AND jdl.kd_jadwal like "'.$this->session->userdata('id_jurusan_prasyarat').'%" 
											AND (jdl.`kd_dosen` != "" OR kry.`nama` != "" OR kry.`nid` != "")
											ORDER BY kry.`nama` ASC ')->result();

		//print_r($data['dosen']);die();

		$data['prodi'] = $this->session->userdata('id_jurusan_prasyarat');
		$data['page'] = 'akademik/ajar_listdosen_baa';
		$this->load->view('template',$data);
	}

	function save_session_prodi()
	{

        $tahunajaran = $this->input->post('tahunajaran');
        $semester = $this->input->post('semester', TRUE);

       	$user = $this->session->userdata('sess_login');
		$jurusan = $user['userid'];

        $this->session->set_userdata('tahunajaran', $tahunajaran);
        $this->session->set_userdata('semester', $semester);
		$this->session->set_userdata('id_jurusan_prasyarat', $jurusan);

		redirect(base_url('akademik/ajar/load_list_dosen'));
	}

	function load_list_dosen()
	{

		$user = $this->session->userdata('sess_login');
		$data['crud'] = $this->db->query('SELECT * from tbl_role_access where menu_id = 71 and user_group_id = '.$user['id_user_group'].' ')->row();
		$sess   = $user['userid'];

		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);

		$smt = $this->session->userdata('semester');
		$ta = $this->session->userdata('tahunajaran');
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		

		if (in_array(8, $grup) or in_array(19, $grup)) {
			
			$data['dosen'] = $this->ajarmod->list_dosen($sess,$smt,$ta);

		} elseif (in_array(9, $grup)) {

			$data['dosen'] = $this->ajarmod->list_dosen_fak($this->session->userdata('id_jurusan_prasyarat'));

		} elseif (in_array(10, $grup)) {
			
			$data['dosen'] = $this->ajarmod->list_dosen_baa();

		}

		/*
		foreach ($data['dosen'] as $key) {
            $data['maksper'][$key->kd_jadwal] = $this->ajarmod->meetingAmount($ta,$key->kd_jadwal);
		}
		*/

		$data['page'] = 'akademik/ajar_listdosen';
		$this->load->view('template',$data);

	}

	function loadMeetAmount($id)
	{
		$kdjdl = get_kd_jdl($id);
		$ta = $this->session->userdata('tahunajaran');
		$data['maksper'] = $this->ajarmod->meetingAmount($ta,$kdjdl);
		$this->load->view('v_meetamount', $data);	
	}

	function load_baa(){

		$user = $this->session->userdata('sess_login');
		$tahunajaran = $this->input->post('tahunajaran');
        $this->session->set_userdata('tahunajaran', $tahunajaran);

		$tahun = $this->session->userdata('tahunajaran');

		$nik   = $user['userid'];

		
		// $this->db->distinct('jdl.kd_dosen');
		// $this->db->from('tbl_jadwal_matkul jdl');
		// $this->db->join('tbl_karyawan kry', 'jdl.kd_dosen = kry.nid');
		// $this->db->join('tbl_matakuliah mk', 'jdl.id_matakuliah = mk.id_matakuliah');
		// $this->db->where('mk.kd_prodi =',$nik);
		// $this->db->where('jdl.kd_dosen !=', NULL);
		// $this->db->where('jdl.kd_dosen !=', '');
		// $data['dosen']=$this->db->get()->result();

		$data['dosen'] = $this->db->query('SELECT DISTINCT jdl.`kd_dosen` ,kry.`nama`,kry.`nid`,
									(SELECT SUM(mk.`sks_matakuliah`) AS sks FROM tbl_jadwal_matkul jdl
                                    LEFT JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
                                    WHERE jdl.`kd_dosen` = kry.`nid`) AS sks 
                                    
                                    FROM tbl_jadwal_matkul jdl

								JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
								JOIN tbl_karyawan kry ON jdl.`kd_dosen` = kry.`nid`
								ORDER BY kry.`nama` ASC ')->result();

		//print_r($data['dosen']);die();

		$data['prodi'] = $this->session->userdata('id_jurusan_prasyarat');
		$data['page'] = 'akademik/ajar_listdosen_baa';
		$this->load->view('template',$data);

	}

	function load_kegiatan_mengajar(){
		
		$user = $this->session->userdata('sess_login');

		$nik   = $user['userid'];
		
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		

		if ( (in_array(1, $grup)) ) {
			
			$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											LEFT JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											WHERE b.`kd_prodi` = "'.$this->session->userdata('id_jurusan_prasyarat').'"
                                			AND a.`kd_tahunajaran` = "'.$this->session->userdata('tahunajaran').'"')->result();

			$data['page'] = 'akademik/ajar_view';
			$this->load->view('template',$data);

		}elseif ((in_array(8, $grup)) or (in_array(9, $grup)) or (in_array(19, $grup))) {
			
			$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											LEFT JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											WHERE b.`kd_prodi` = "'.$this->session->userdata('id_jurusan_prasyarat').'"
                                			AND a.`kd_tahunajaran` = "'.$this->session->userdata('tahunajaran').'" AND a.kd_jadwal LIKE "'.$this->session->userdata('id_jurusan_prasyarat').'%"')->result();

			$user = $this->session->userdata('sess_login');

			$nik = $user['userid'];

			$data['nm_dosen']  = $this->db->where('nid', $nik)
							 ->get('tbl_karyawan')->row();

			$data['page'] = 'akademik/ajar_view';
			$this->load->view('template',$data);

		}else{

			$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											LEFT JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											WHERE a.`kd_dosen` = '.$nik.'')->result();

			$user = $this->session->userdata('sess_login');

			$nik = $user['userid'];

			$data['nm_dosen']  = $this->db->where('nid', $nik)
							 ->get('tbl_karyawan')->row();

			$data['page'] = 'akademik/ajar_view';
			$this->load->view('template',$data);
		
		}


		

		// $data['haha'] = $this->db->query('SELECT * from tbl_jadwal_matkul a
		// 									left join tbl_matakuliah b on a.`kd_matakuliah`=b.`kd_matakuliah`
		// 									left join tbl_krs c on c.`kd_matakuliah`=b.`kd_matakuliah`
		// 									left join tbl_mahasiswa d on d.`NIMHSMSMHS`=c.`npm_mahasiswa`
		// 									where id_jadwal=id_jadwal')->result();

		

	}

	function show_dosen_by_prodi($nid){


		$data['nid_dosen'] = $nid;

		$data['nm_dosen']  = $this->db->where('nid', $nid)
							 ->get('tbl_karyawan')->row();
								  
		$user = $this->session->userdata('sess_login');
		
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ((in_array(8, $grup)) or (in_array(19, $grup))) {
			$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											WHERE b.`kd_prodi` = "'.$this->session->userdata('id_jurusan_prasyarat').'"
                                			AND a.`kd_tahunajaran` = "'.$this->session->userdata('tahunajaran').'"
                                			AND a.`kd_dosen` = "'.$nid.'" AND a.`kd_jadwal` LIKE "'.$this->session->userdata('id_jurusan_prasyarat').'%"')->result();
		}elseif((in_array(10, $grup)) or (in_array(1, $grup))){
			$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											WHERE b.`kd_prodi` = "'.$this->session->userdata('id_jurusan_prasyarat').'"
                                			AND a.`kd_tahunajaran` = "'.$this->session->userdata('tahunajaran').'"
                                			AND a.`kd_dosen` = "'.$nid.'" AND a.`kd_jadwal` LIKE "'.$this->session->userdata('id_jurusan_prasyarat').'%"')->result();	
		}					 


		// $data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
		// 									JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
		// 									WHERE b.`kd_prodi` = "'.$this->session->userdata('id_jurusan_prasyarat').'"
  //                               			AND a.`kd_tahunajaran` = "'.$this->session->userdata('tahunajaran').'"
  //                               			AND a.`kd_dosen` = "'.$nid.'" AND a.`kd_jadwal` LIKE "'.$this->session->userdata('id_jurusan_prasyarat').'%"')->result();

		$data['page'] = 'akademik/ajar_view';
		$this->load->view('template',$data);
	}

	function load_ajr($ldr)
	{
		$data['haha'] = $this->db->query('SELECT * from tbl_jadwal_matkul a
											left join tbl_matakuliah b on a.`kd_matakuliah`=b.`kd_matakuliah`
											left join tbl_krs c on c.`kd_matakuliah`=b.`kd_matakuliah`
											left join tbl_mahasiswa d on d.`NIMHSMSMHS`=c.`npm_mahasiswa`
											where id_jadwal='.$ldr.'')->result();

		$data['kok'] = $this->db->query('SELECT * from tbl_jadwal_matkul a
											left join tbl_matakuliah b on a.`kd_matakuliah`=b.`kd_matakuliah`
											left join tbl_krs c on c.`kd_matakuliah`=b.`kd_matakuliah`
											left join tbl_mahasiswa d on d.`NIMHSMSMHS`=c.`npm_mahasiswa`
											where id_jadwal='.$ldr.'')->row();

		$this->load->view('akademik/load_ajar', $data);
	}

	function cetak_absensi($id)
	{
		//ini_set('max_execution_time', 1000);
		$user = $this->session->userdata('sess_login');

		$activeYear = getactyear();

		if ($user['userid'] == 'BAA') {
			$optyear = $this->session->userdata('ta');
		} else {
			$optyear = $this->session->userdata('tahunajaran');
		}

		$nik = $user['userid'];

		$data['rows'] 	= $this->db->query('SELECT a.*,b.kd_matakuliah,b.sks_matakuliah,b.nama_matakuliah,
											b.semester_matakuliah,d.kode_ruangan,d.kuota FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
											LEFT JOIN tbl_ruangan d ON d.`id_ruangan` = a.`kd_ruangan`
											WHERE a.`id_jadwal` = '.$id.' AND SUBSTR(a.kd_jadwal,1,5) = b.kd_prodi')->row();
		

		$data['kodeprodi'] = substr($data['rows']->kd_jadwal, 0,5);

		$data['jumlah'] = $this->db->query('SELECT count(distinct npm_mahasiswa) as mhs from tbl_krs 
											where kd_jadwal = "'.$data['rows']->kd_jadwal.'" ')->row()->mhs;

		$data['mhs'] 	= $this->db->query('SELECT distinct b.npm_mahasiswa FROM tbl_krs b
											JOIN tbl_sinkronisasi_renkeu r ON r.npm_mahasiswa = b.npm_mahasiswa
											WHERE r.tahunajaran = "'.$optyear.'"
											AND b.kd_jadwal = "'.$data['rows']->kd_jadwal.'" 
											
											ORDER BY b.npm_mahasiswa ASC')->result();

		
		// OLD
		// $data['maxper'] = $this->ajarmod->meetingAmountForPrint($optyear,$data['rows']->kd_jadwal);


		// NEW
		$maxper = $this->ajarmod->meetingAmountForPrint($optyear,$data['rows']->kd_jadwal);

		for ($i=1; $i <= $maxper ; $i++) { 
			$arrayPertemuan[] = $i;
		}

		$data['arrayPertemuan'] = $arrayPertemuan;
		$data['maxper'] = $maxper;

		$this->load->view('cetak_absensi_pdf__',$data);
		
	}

	function cetak_absensi2($id)
	{
		//ini_set('max_execution_time', 1000);
		$user = $this->session->userdata('sess_login');

		$activeYear = getactyear();

		if ($user['userid'] == 'BAA') {
			$optyear = $this->session->userdata('ta');
		} else {
			$optyear = $this->session->userdata('tahunajaran');
		}

		$nik = $user['userid'];

		$data['rows'] 	= $this->db->query('SELECT a.*,b.kd_matakuliah,b.sks_matakuliah,b.nama_matakuliah,
											b.semester_matakuliah,d.kode_ruangan,d.kuota FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
											LEFT JOIN tbl_ruangan d ON d.`id_ruangan` = a.`kd_ruangan`
											WHERE a.`id_jadwal` = '.$id.' AND SUBSTR(a.kd_jadwal,1,5) = b.kd_prodi')->row();
		

		$data['kodeprodi'] = substr($data['rows']->kd_jadwal, 0,5);

		$data['jumlah'] = $this->db->query('SELECT count(distinct npm_mahasiswa) as mhs from tbl_krs 
											where kd_jadwal = "'.$data['rows']->kd_jadwal.'" ')->row()->mhs;

		$data['mhs'] 	= $this->db->query('SELECT distinct b.npm_mahasiswa FROM tbl_krs b
											JOIN tbl_sinkronisasi_renkeu r ON r.npm_mahasiswa = b.npm_mahasiswa
											WHERE r.tahunajaran = "'.$optyear.'"
											AND b.kd_jadwal = "'.$data['rows']->kd_jadwal.'" 
											
											ORDER BY b.npm_mahasiswa ASC')->result();

		$maxper = $this->ajarmod->meetingAmountForPrint($optyear,$data['rows']->kd_jadwal);

		for ($i=1; $i <= $maxper ; $i++) { 
			$arrayPertemuan[] = $i;
		}

		$data['arrayPertemuan'] = $arrayPertemuan;
		$data['maxper'] = $maxper;


		$this->load->view('cetak_absensi_pdf__2',$data);
		
	}

	function cetak_absensi_polos($id) 
	{
		$optyear 	= $this->session->userdata('tahunajaran');
		$user 		= $this->session->userdata('sess_login');

		$nik = $user['userid'];

		$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
											LEFT JOIN tbl_ruangan d ON d.`id_ruangan` = a.`kd_ruangan`
											WHERE a.`id_jadwal` = '.$id.'
											AND b.kd_prodi = "'.$nik.'"')->row();

		$kodeprodi = substr($data['rows']->kd_jadwal, 0,5);

		//$data['mk'] = $this->db->query('SELECT * FROM tbl_matakuliah WHERE `kd_prodi` = "'.$nik.'" and kd_matakuliah =  "'.$data['rows']->kd_matakuliah.'"')->row();		

		// $data['title'] = $this->db->query('SELECT * FROM tbl_matakuliah a 
		// 									LEFT JOIN tbl_jurusan_prodi b ON a.`kd_prodi` = b.`kd_prodi`
		// 									LEFT JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
		// 									WHERE a.`kd_matakuliah` = "'.$data['rows']->kd_matakuliah.'"')->row();

		$data['titles'] = $this->db->query('SELECT * FROM tbl_jurusan_prodi b
											JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
											WHERE b.`kd_prodi` = "'.$kodeprodi.'"')->row();

		if ($data['rows']->gabung > 0) {
			$data['mhs'] = $this->db->query('SELECT distinct mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS` FROM tbl_krs b
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											JOIN tbl_sinkronisasi_renkeu r ON r.npm_mahasiswa = b.npm_mahasiswa
											WHERE r.tahunajaran = "'.$optyear.'"
											AND b.kd_jadwal = "'.$data['rows']->kd_jadwal.'" OR kd_jadwal = "'.$data['rows']->referensi.'"
											OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$data['rows']->referensi.'")
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		} else {
			$data['mhs'] = $this->db->query('SELECT distinct mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS` FROM tbl_krs b
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											JOIN tbl_sinkronisasi_renkeu r ON r.npm_mahasiswa = b.npm_mahasiswa
											WHERE r.tahunajaran = "'.$optyear.'"
											AND b.kd_jadwal = "'.$data['rows']->kd_jadwal.'" 
											OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$data['rows']->kd_jadwal.'")
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		}
		$this->load->view('cetak_absensi_pdf',$data);
	}

	function cetak_gelondongan($id,$day)
	{
		error_reporting(0);
		$this->load->model('temph_model');
		$data['jadwal'] = $this->db->query("SELECT jdl.*,mk.`nama_matakuliah`,mk.semester_matakuliah,mk.`sks_matakuliah`,r.`kode_ruangan`,r.`kuota` FROM tbl_jadwal_matkul jdl
											JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
											LEFT JOIN tbl_ruangan r ON jdl.`kd_ruangan` = r.`id_ruangan`
											WHERE jdl.`kd_jadwal` LIKE '".$id."%' AND jdl.`kd_tahunajaran` = '20172' AND mk.`kd_prodi` = '".$id."' and jdl.hari = ".$day."
											AND jdl.`kd_jadwal` IN (SELECT kd_jadwal FROM tbl_krs WHERE kd_krs LIKE CONCAT(npm_mahasiswa,'20172%'))
											-- limit 1
											")->result();
		
		$this->load->view('cetak_absensi_gelondongan',$data);
		
	}

	function cetak_daftar_mahasiswa($id){
		$data['www'] = $this->db->query('SELECT * from tbl_jadwal_matkul a
											join tbl_matakuliah b on a.`kd_matakuliah`=b.`kd_matakuliah`
											join tbl_krs c on c.`kd_matakuliah`=b.`kd_matakuliah`
											join tbl_mahasiswa d on d.`NIMHSMSMHS`=c.`npm_mahasiswa`
											where id_jadwal='.$id.'')->result();

		$data['lah'] = $this->db->query('SELECT * from tbl_jadwal_matkul a
											join tbl_matakuliah b on a.`kd_matakuliah`=b.`kd_matakuliah`
											join tbl_jurusan_prodi c on b.`kd_prodi`=c.`kd_prodi`
											join tbl_fakultas d on c.`kd_fakultas`=d.`kd_fakultas`
											left join tbl_ruangan e on a.`kd_ruangan`=e.`kode_ruangan`
											left join tbl_gedung f on e.`id_gedung`=f.`id_gedung`
											where id_jadwal='.$id.'')->row();

		$this->load->view('cetak_mahasiswa_pdf',$data);
	}

	#edit danu
	function print_surat_tugas($nid)
	{
		$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											LEFT JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											WHERE b.`kd_prodi` = "'.$this->session->userdata('id_jurusan_prasyarat').'"
                                			AND a.`kd_tahunajaran` = "'.$this->session->userdata('tahunajaran').'"
                                			AND a.`kd_dosen` = "'.$nid.'"')->result();		
		$data['karyawan'] = $this->app_model->getdetail('tbl_karyawan','nid',$nid,'nid','asc')->row();
		$data['fak'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$this->session->userdata('id_jurusan_prasyarat'),'kd_prodi','asc')->row();
		$data['fakultas'] = $this->app_model->getdetail('tbl_fakultas','kd_fakultas',$data['fak']->kd_fakultas,'kd_fakultas','asc')->row();
		$this->load->view('cetak_surat_tugas', $data);
	}

	function view_absen($id)
	{
		$data['kelas'] = $this->ajarmod->loadAbsen($id);
		$data['id'] = $id;
		$this->load->view('ajar_absen_harian', $data);
	}

	function view_absen_dosen($id)
	{
		$user = $this->session->userdata('sess_login');
		$data['kelas'] = $this->app_model->get_data_jadwal($id,$user['userid'])->row();
		$data['max'] = $this->db->query("select count(npm_mahasiswa) as mhs from tbl_krs where kd_jadwal = '".$data['kelas']->kd_jadwal."'")->row()->mhs;
		$data['id'] = $id;
		$this->load->view('ajar_absen_dosen', $data);
	}

	function kelas_absen($id)
	{
		//$datenow = date('yy-mm-dd H:i:s');
		$jumlahabsen=$this->input->post('jumlah', TRUE);
		$kode = $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$id,'id_jadwal','asc')->row();
		$cek = $this->db->query("select count(distinct tanggal) as jml from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$kode->kd_jadwal."'")->row()->jml;
		$tanggal = $this->db->query("select distinct tanggal,max(pertemuan) AS pertemuan from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$kode->kd_jadwal."' order by id_absen desc limit 1")->row();
		//var_dump($cek);exit();
		$masuk = 0; $ijin = 0; $alfa = 0; $sakit = 0;
		if ($cek > 0) {
			if ($tanggal->tanggal == $this->input->post('tgl',TRUE)) {
				//$pertemuan =  1;
				echo "<script>alert('Absensi Sudah Dilakukan Pada Tanggal Tersebut');
				history.go(-1);</script>";
			} else {
				$a = 1;
				for ($i=0; $i < $jumlahabsen-1; $i++) { 
					$hasil = $this->input->post('absen'.$a.'[0]', TRUE);
					//echo 'absen'.$a.'['.$i.']';
					$pecah = explode('-', $hasil);
					//var_dump($hasil);
					$datax[] = array(
						'npm_mahasiswa' => $pecah[1],
						'kd_jadwal' => $pecah[2],
						'tanggal' => $this->input->post('tgl',TRUE),
						'pertemuan' => $tanggal->pertemuan+1,
						'kehadiran' => $pecah[0]
						);
					if (($tanggal->pertemuan+1) > 16) {
						echo "<script>alert('Absen Melebihi Jumlah 16 Pertemuan');
						document.location.href='".base_url()."akademik/ajar/load_list_dosen';</script>";
						exit();
					}
					if ($pecah[0] == 'H') {
						$masuk++;
					} elseif ($pecah[0] == 'I') {
						$ijin++;
					} elseif ($pecah[0] == 'S') {
						$sakit++;
					} else {
						$alfa++;
					}
					$a++;
				}
				$this->db->insert_batch('tbl_absensi_mhs_new_20171',$datax);
				$datas = array(
						'kd_jadwal' => $kode->kd_jadwal,
						'tanggal' => $this->input->post('tgl',TRUE),
						'pertemuan' => 1,
						'jumlah_hadir' => $masuk,
						'jumlah_sakit' => $sakit,
						'jumlah_izin' => $ijin,
						'jumlah_alfa' => $alfa,
						//'pembahasan' => $this->input->post('pembahasan',TRUE),
						'date_input' => date('yy-mm-dd H:i:s')
						);
				$this->app_model->insertdata('tbl_absensi_dosen',$datas);
				echo "<script>alert('Sukses');
				document.location.href='".base_url()."akademik/ajar/load_list_dosen';</script>";
				
			}
		} else {
			$a = 1;
			for ($i=0; $i < $jumlahabsen-1; $i++) { 
				$hasil = $this->input->post('absen'.$a.'[0]', TRUE);
				//echo 'absen'.$a.'['.$i.']';
				$pecah = explode('-', $hasil);
				//var_dump($hasil);
				$datax[] = array(
					'npm_mahasiswa' => $pecah[1],
					'kd_jadwal' => $pecah[2],
					'tanggal' => $this->input->post('tgl',TRUE),
					'pertemuan' => $tanggal->pertemuan+1,
					'kehadiran' => $pecah[0]
					);
				if ($pecah[0] == 'H') {
					$masuk++;
				} elseif ($pecah[0] == 'I') {
					$ijin++;
				} elseif ($pecah[0] == 'S') {
					$sakit++;
				} else {
					$alfa++;
				}
				$a++;
			}
			$this->db->insert_batch('tbl_absensi_mhs_new_20171',$datax);
			$datas = array(
					'kd_jadwal' => $kode->kd_jadwal,
					'tanggal' => $this->input->post('tgl',TRUE),
					'pertemuan' => 1,
					'jumlah_hadir' => $masuk,
					'jumlah_sakit' => $sakit,
					'jumlah_izin' => $ijin,
					'jumlah_alfa' => $alfa,
					//'pembahasan' => $this->input->post('pembahasan',TRUE),
					'date_input' => date('yy-mm-dd H:i:s')
					);
			$this->app_model->insertdata('tbl_absensi_dosen',$datas);
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."akademik/ajar/load_list_dosen';</script>";
		}
		
	}


	function dosen_absen($id)
	{
		$kode = $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$id,'id_jadwal','asc')->row();
		$cek = $this->db->query("select count(distinct tanggal) as jml from tbl_absensi_dosen where kd_jadwal = '".$kode->kd_jadwal."'")->row()->jml;
		$tanggal = $this->db->query("select distinct tanggal,pertemuan from tbl_absensi_dosen where kd_jadwal = '".$kode->kd_jadwal."' order by id desc limit 1")->row();
		//var_dump($cek);exit();
		if ($cek > 0) {
			if ($tanggal->tanggal == $this->input->post('tgl',TRUE)) {
				//$pertemuan =  1;
				echo "<script>alert('Absensi Sudah Dilakukan Pada Tanggal Tersebut');
				history.go(-1);</script>";
			} else {
				$data['materi'] = $this->input->post('materi', TRUE);
				$data['jumlah_mhs'] = $this->input->post('jumlah', TRUE);
				$data['kd_jadwal'] = $kode->kd_jadwal;
				$data['tanggal'] = $this->input->post('tgl',TRUE);
				$data['pertemuan'] = 1;
				//var_dump($data);exit();
				$q = $this->app_model->insertdata('tbl_absensi_dosen',$data);
				echo "<script>alert('Sukses');
				document.location.href='".base_url()."akademik/ajar/load_list_dosen';</script>";
			}
		} else {
			$data['materi'] = $this->input->post('materi', TRUE);
			$data['jumlah_mhs'] = $this->input->post('jumlah', TRUE);
			$data['kd_jadwal'] = $kode->kd_jadwal;
			$data['tanggal'] = $this->input->post('tgl',TRUE);
			$data['pertemuan'] = 1;
			//var_dump($data);exit();
			$q = $this->app_model->insertdata('tbl_absensi_dosen',$data);
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."akademik/ajar/load_list_dosen';</script>";
		}
		
	}

	function edit_absen($id){
		$this->session->unset_userdata('kd_jadwal');
		$q = $this->db->query('select kd_jadwal from tbl_jadwal_matkul where id_jadwal = '.$id.'')->row()->kd_jadwal;

		$this->session->set_userdata('kd_edit',$q);

		$data['id_jadwal'] = $id;

		$data['pertemuan'] = $this->db->query('SELECT DISTINCT pertemuan,tanggal FROM tbl_absensi_mhs_new_20171 WHERE kd_jadwal = "'.$q.'"
													GROUP BY pertemuan,tanggal')->result();

		$data['page'] = 'edit_absen';
		$this->load->view('template', $data);
	}

	function open_absen($id,$akses)
	{
		$this->db->query("update tbl_jadwal_matkul set open = ".$akses." where id_jadwal = ".$id."");
		$lacak = $this->db->query("SELECT kd_jadwal from tbl_jadwal_matkul where id_jadwal = '".$id."'")->row();
		$temu = $this->db->query("SELECT max(pertemuan) as meet from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$lacak->kd_jadwal."' order by pertemuan limit 1")->row();
		$jam = date('H:i:s');
		if ($akses == 0) {
			$this->db->query("UPDATE tbl_materi_abs set jam_keluar = '".$jam."' where kd_jadwal = '".$lacak->kd_jadwal."' and pertemuan = '".$temu->meet."'");
		} 
		
		echo "<script>alert('Berhasil');
			document.location.href='".base_url()."akademik/ajar/load_list_dosen';</script>";
	}

	function modal_edit_absen($id,$pertemuan)
	{
		clearstatcache();
		$actyear = getactyear();

		$q = $this->db->query('select * from tbl_jadwal_matkul where id_jadwal = '.$id.'')->row();

		$data['pertemuan'] = $pertemuan;
		$data['kode_jadwal'] = $q->kd_jadwal;
		$data['tanggal'] = $this->db->query('SELECT DISTINCT tanggal FROM tbl_absensi_mhs_new_20171 
											WHERE pertemuan = '.$pertemuan.' 
											AND kd_jadwal = "'.$q->kd_jadwal.'"')->row()->tanggal;
		
		$data['kelas'] 	= $this->db->query('SELECT distinct 
												b.`npm_mahasiswa`,
												mhs.`NMMHSMSMHS`,
												b.`kd_jadwal`, 
												(SELECT kehadiran FROM tbl_absensi_mhs_new_20171 
												WHERE kd_jadwal = "'.$q->kd_jadwal.'" 
												AND pertemuan = '.$pertemuan.' 
												AND npm_mahasiswa = b.`npm_mahasiswa`) as kehadiran
											FROM tbl_krs b
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											WHERE b.kd_jadwal = "'.$q->kd_jadwal.'"
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		
		$data['id'] = $id;
		$this->load->view('ajar_absen_harian_edit', $data);
	}

	function edit_kelas_absen($id)
	{

		$jumlahabsen=$this->input->post('jumlah', TRUE);
		$pertemuan=$this->input->post('pertemuan', TRUE);
		$id_jadwal2=$this->input->post('id_jadwal2', TRUE);
		$tanggal2=$this->input->post('tanggal2', TRUE);
		$tanggal = $this->input->post('tgl',TRUE);

		$user = $this->session->userdata('sess_login');
		$nik   = $user['userid'];

		//$kode = $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$id,'id_jadwal','asc')->row();
		// //if ($kode->gabung > 0) {
		// 	$where = 'kd_jadwal = "'.$kode->kd_jadwal.'" OR kd_jadwal = "'.$kode->referensi.'"
		// 									OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$kode->referensi.'")';
		// //} else {
		// 	//$where = 'kd_jadwal = "'.$kode->kd_jadwal.'" 
		// 									//OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$kode->kd_jadwal.'")';
		// //}
		$this->db->where('kd_jadwal',$id_jadwal2);
		$this->db->where('pertemuan', $pertemuan);
		$this->db->delete('tbl_absensi_mhs_new_20171');
				
				$a = 1;
				for ($i=0; $i < $jumlahabsen-1; $i++) { 
					$hasil = $this->input->post('absen'.$a.'[0]', TRUE);
					//die($hasil);
					$pecah = explode('-', $hasil);
					//var_dump($hasil);
					
					$data['sblm_update'] = $tanggal2;
					$data['pertemuan'] = $pertemuan;
					$data['kehadiran'] = $pecah[0];
					$data['npm_mahasiswa'] = $pecah[1];
					$data['kd_jadwal'] = $pecah[2];
					
					$data['tanggal'] = $tanggal;
					$data['sblm_update'] = $tanggal2;
					$data['user_update'] = $nik;
					//var_dump($data);exit();
					// $q = $this->db->where('npm_mahasiswa',$pecah[1])
					// 			  ->where('kd_jadwal',$kode->kd_jadwal)
					// 			  ->where('tanggal',$tanggal2)
					// 			  ->where('pertemuan',$pertemuan)
					// 			  ->update('tbl_absensi_mhs_new_20171',$data);

					$this->db->insert('tbl_absensi_mhs_new_20171', $data);

					$a++;
				}
				//die($pecah[1]);

				echo "<script>alert('Sukses');
			document.location.href='".base_url()."akademik/ajar/edit_absen/".$id."';</script>";
	}

	function export_excel(){

		//die('test');
		$data['dosen'] = $this->db->query('SELECT DISTINCT jdl.`kd_dosen` ,kry.`nama`,kry.`nid`,
									(SELECT SUM(mk.`sks_matakuliah`) AS sks FROM tbl_jadwal_matkul jdl
                                    JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
                                    WHERE jdl.`kd_dosen` = kry.`nid` and (mk.nama_matakuliah not like "skripsi%" and mk.nama_matakuliah not like "kerja praktek%" and mk.nama_matakuliah not like "magang%" and mk.nama_matakuliah not like "kuliah kerja%") and jdl.kd_tahunajaran = "'.$this->session->userdata('tahunajaran').'" and mk.kd_prodi = "'.$this->session->userdata('id_jurusan_prasyarat').'" and jdl.kd_jadwal like "'.$this->session->userdata('id_jurusan_prasyarat').'%") AS sks 
                                    FROM tbl_jadwal_matkul jdl

								JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
								JOIN tbl_karyawan kry ON jdl.`kd_dosen` = kry.`nid`
								where jdl.kd_tahunajaran = "'.$this->session->userdata('tahunajaran').'" and mk.kd_prodi = "'.$this->session->userdata('id_jurusan_prasyarat').'"
								and jdl.kd_jadwal like "'.$this->session->userdata('id_jurusan_prasyarat').'%"
								ORDER BY kry.`nama` ASC')->result();

		$data['detail'] = $this->db->query("select prodi,fakultas from tbl_jurusan_prodi a join tbl_fakultas b on a.kd_fakultas = b.kd_fakultas where a.kd_prodi = ".$this->session->userdata('id_jurusan_prasyarat')." ")->row();

		$this->load->view('export_list_dosen_ajar', $data);
	}

	function dwld_excel($id)
	{
		$kodeprod = $this->db->query("select kd_jadwal from tbl_jadwal_matkul where id_jadwal = ".$id."")->row()->kd_jadwal;
		$kodeprodi = substr($kodeprod, 0,5);
		$user = $this->session->userdata('sess_login');
		//$data['tayang_nilai'] = 'tidak';
		$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
											WHERE id_jadwal = "'.$id.'" and b.kd_prodi = "'.$kodeprodi.'"')->row();
		$data['ping'] = $this->db->query('SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS IN (SELECT npm_mahasiswa FROM tbl_krs WHERE kd_jadwal = "'.$data['rows']->kd_jadwal.'")')->result();
		$data['absendosen'] = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs_new_20171 where kd_jadwal = '".$kodeprod."'")->row();
		// $data['title'] = $this->db->query('SELECT a.*,b.nama_matakuliah,b.semester_matakuliah,c.prodi FROM tbl_jadwal_matkul a 
		// 									JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
		// 									join tbl_jurusan_prodi c on b.kd_prodi = c.kd_prodi
		// 									WHERE a.`id_jadwal` = "'.$id.'"')->row();
		$data['prodi'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$kodeprodi,'kd_prodi','asc')->row();
		$data['jmlh'] = $this->db->query('SELECT count(distinct npm_mahasiswa) as jumlah FROM tbl_krs
										WHERE `kd_jadwal` = "'.$data['rows']->kd_jadwal.'" ORDER BY `npm_mahasiswa` asc')->row();
		// $data['ping'] = $this->db->query('SELECT * from tbl_nilai_detail a join tbl_mahasiswa b
		// 									on a.`npm_mahasiswa`=b.`NIMHSMSMHS` join tbl_krs c
		// 									on c.`npm_mahasiswa`=b.`NIMHSMSMHS` join tbl_jadwal_matkul d
		// 									on d.`kd_jadwal`=c.`kd_jadwal` where d.`id_jadwal` = "'.$id.'"')->result();
		//var_dump($data);exit();
		$this->load->library('excel');
		if ( (strtoupper($data['title']->nama_matakuliah) == 'SKRIPSI') OR ($data['title']->nama_matakuliah == 'KULIAH KERJA MAHASISWA') OR ($data['title']->nama_matakuliah == 'Kuliah Kerja Mahasiswa') OR ($data['title']->nama_matakuliah == 'KULIAH KERJA NYATA') OR ($data['title']->nama_matakuliah == 'Kuliah Kerja Nyata (Magang Kerja)') OR ($data['title']->nama_matakuliah == 'Kuliah Kerja Praktek') OR ($data['title']->nama_matakuliah == 'MAGANG KERJA') OR ($data['title']->nama_matakuliah == 'Kerja Praktek') OR ($data['title']->nama_matakuliah == 'TESIS') OR ($data['title']->nama_matakuliah == 'Tesis') ) {
			$this->load->view('welcome/phpexcel_nilai_skripsi', $data);
		} else {
			$this->load->view('welcome/phpexcel_nilai', $data);
		}
	}

	function cetak_nilai($id)
	{
		$this->load->library('Cfpdf');

		$user = $this->session->userdata('sess_login');

		$data['id_jadwal']=$id;

		$nik = $user['userid'];

		$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`id_matakuliah` = b.`id_matakuliah` 
											JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
											WHERE id_jadwal = "'.$id.'"')->row();

		$data['absendosen'] = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs_new_20171 where kd_jadwal = '".$data['rows']->kd_jadwal."'")->row();

		$data['title'] = $this->db->query('SELECT * FROM tbl_matakuliah a 
											JOIN tbl_jurusan_prodi b ON a.`kd_prodi` = b.`kd_prodi`
											JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
											WHERE a.`kd_matakuliah` = "'.$data['rows']->kd_matakuliah.'" and a.kd_prodi = "'.$data['rows']->kd_prodi.'"')->row();


		$data['mhs'] = $this->db->query('SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS IN (SELECT npm_mahasiswa FROM tbl_krs WHERE kd_jadwal = "'.$data['rows']->kd_jadwal.'")')->result();
		if ( (strtoupper($data['title']->nama_matakuliah) == 'SKRIPSI') OR ($data['title']->nama_matakuliah == 'KULIAH KERJA MAHASISWA') OR ($data['title']->nama_matakuliah == 'Kuliah Kerja Mahasiswa') OR ($data['title']->nama_matakuliah == 'KULIAH KERJA NYATA') OR ($data['title']->nama_matakuliah == 'Kuliah Kerja Nyata (Magang Kerja)') OR ($data['title']->nama_matakuliah == 'Kuliah Kerja Praktek') OR ($data['title']->nama_matakuliah == 'MAGANG KERJA') OR ($data['title']->nama_matakuliah == 'Kerja Praktek') ) {
			$this->load->view('form/cetak_nilai_matkul_skripsi',$data);
		} else {
			$this->load->view('form/cetak_nilai_matkul',$data);
		}
		
	}

	function hapus_pertemuan($id,$pertemuan){
		
		$kd_jadwal = $this->db->query('select * from tbl_jadwal_matkul where id_jadwal = '.$id.'')->row();
		// if ($kd_jadwal->gabung > 0) {
		// 	$where = 'kd_jadwal = "'.$kd_jadwal->kd_jadwal.'" OR kd_jadwal = "'.$kd_jadwal->referensi.'"
		// 									OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$kd_jadwal->referensi.'")';
		// } else {
		// 	$where = 'kd_jadwal = "'.$kd_jadwal->kd_jadwal.'" 
		// 									OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$kd_jadwal->kd_jadwal.'")';
		// }
		$where = 'kd_jadwal = "'.$kd_jadwal->kd_jadwal.'"';
		$this->db->where($where);
		$this->db->where('pertemuan', $pertemuan);
		$this->db->delete('tbl_absensi_mhs_new_20171');
		//$this->db->where('kd_jadwal', $kd_jadwal);
		//$this->db->where('pertemuan', $pertemuan);
		//$this->db->delete('tbl_absensi_mhs_new_20171');

		echo "<script>alert('Berhasil Menghapus Jadwal !!');history.go(-1);</script>";
	}

}

/* End of file Ajar.php */
/* Location: .//C/Users/danum246/AppData/Local/Temp/fz3temp-1/Ajar.php */