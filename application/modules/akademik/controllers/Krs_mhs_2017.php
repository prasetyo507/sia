<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Krs_mhs_2017 extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$tglnow = date('Y-m-d');
		// $tgl = array(
		// 	1 => array(
		// 		'tgl_on' => '2017-08-01',
		// 		'tgl_off'=> '2017-08-20'
		// 		),
		// 	2 => array(
		// 		'tgl_on' => '2017-08-19',
		// 		'tgl_off'=> '2017-08-25'
		// 		),
		// 	3 => array(
		// 		'tgl_on' => '2017-08-23',
		// 		'tgl_off'=> '2017-08-29'
		// 		),
		// 	4 => array(
		// 		'tgl_on' => '2017-08-28',
		// 		'tgl_off'=> '2017-08-31'
		// 		)
		// 	);
		$this->load->model('setting_model');
		$this->load->model('temph_model');
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(157)->result();
			$aktif1 = $this->setting_model->getaktivasi('krs')->result();
			$aktif2 = $this->setting_model->getaktivasi('krss2')->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			} else {
				if ((count($aktif1) != 1) or (count($aktif2) != 1)) {
					echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
				}
			}
			
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$tglnow = date('Y-m-d');
		$log = $this->session->userdata('sess_login');
		$tahun = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
		$cek = $this->db->query("SELECT * from tbl_verifikasi_krs_tes where kd_krs like '".$log['userid'].$tahun->kode."%'")->result();
		$cek2 = $this->db->query("SELECT * from tbl_verifikasi_krs_tes where kd_krs like '".$log['userid'].$tahun->kode."%'")->row();
		$data['mhs'] = $this->db->where('npm_mahasiswa',$log['userid'])->get('tbl_pa',1)->row();
		if (count($cek) == 1) {
			$this->save_sesi($log['userid'],$cek2->kd_krs);
		} else {
			$data['mhs'] = $this->db->where('npm_mahasiswa',$log['userid'])->get('tbl_pa',1)->row();
			$jam = date("H:i");
			if ($jam > '07:59' and $jam < '21:01') {
				$npm = $this->session->userdata('sess_login');
				$log = $this->db->query("SELECT KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS = '".$npm['userid']."'")->row()->KDPSTMSMHS;
				if ((substr($npm['userid'], 0,4) != '2017') and ($log == '61201' or $log == '73201' or $log == '62201')) {
					if (($tglnow > '2017-08-13' and $tglnow < '2017-08-17') or ($tglnow > '2017-08-22' and $tglnow < '2017-08-25')) {
						$data['page'] = 'akademik/krs_mhs';
						$this->load->view('template', $data);
					} else {
						echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
					}
				} elseif ((substr($npm['userid'], 0,4) != '2017') and ($log == '26201' or $log == '32201' or $log == '25201' or $log == '24201' or $log == '70201')) {
					if (($tglnow > '2017-08-16' and $tglnow < '2017-08-20') or ($tglnow > '2017-08-23' and $tglnow < '2017-08-26')) {
						$data['page'] = 'akademik/krs_mhs';
						$this->load->view('template', $data);
					} else {
						echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
					}
				} elseif ((substr($npm['userid'], 0,4) != '2017') and ($log == '61101' or $log == '74101' or $log == '74201' or $log == '55201'))  {
					if (($tglnow > '2017-08-19' and $tglnow < '2017-08-23') or ($tglnow > '2017-08-24' and $tglnow < '2017-08-27')) {
						$data['page'] = 'akademik/krs_mhs';
						$this->load->view('template', $data);
					} else {
						echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
					}
				} elseif ((substr($npm['userid'], 0,4) == '2017') and ($log == '61101' or $log == '74101' or $log == '70201' or $log == '62201' or $log == '55201' or $log == '35201' or $log == '25201' or $log == '24201' or $log == '74201' or $log == '61201' or $log == '73201' or $log == '26201')) {
					if ($tglnow > '2017-08-20' and $tglnow < '2017-08-29') {
						$data['page'] = 'akademik/krs_mhs_2017';
						$this->load->view('template', $data);
					} else {
						echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
					}
				} else {
					echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
				}
			} else {
				echo "<script>alert('Akses pengisian KRS hanya diizinkan mulai pukul 08:00 s/d 21:00');document.location.href='".base_url()."home';</script>";
			}
		}
	}

	function create_session()
	{
		// var_dump($this->input->post('kd_dosen'));exit();
		$get = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
		// var_dump($get->kode);exit();
		$ceks = $this->db->query("SELECT * from tbl_verifikasi_krs_tes where kd_krs like '".$this->input->post('mhs').$get->kode."%' and status_verifikasi = 1")->result();
		if (count($ceks) > 0) {
			echo "<script>alert('KRS Anda Telah Disetujui Oleh Dosen Pembimbing!!');history.go(-1);</script>";
		} else {
			$logs = $this->session->userdata('sess_login');
			$cekrenkeu = $this->db->query("SELECT * from tbl_sinkronisasi_renkeu where npm_mahasiswa = '".$logs['userid']."' and tahunajaran = '".$get->kode."' and status >= 1 ")->result();
			if (count($cekrenkeu) > 0) {
				$this->session->set_userdata('kd_dosen', $this->input->post('kd_dosen'));
				$this->session->set_userdata('kelas', $this->input->post('kelas'));
				// cek apakah pa sesuai ?
				$akt = substr($logs['userid'], 0,4);
				if ($akt != '2017') {
					$cekpa = $this->temph_model->match_pa($logs['userid'])->row()->kd_dosen;
					if ($this->input->post('kd_dosen') != $cekpa) {
						echo "<script>alert('Dosen Pembimbing Akademik anda tidak sesuai!');history.go(-1);</script>";
					}
				}
				
				redirect(base_url().'akademik/krs_mhs_2017/pengisian_krs','refresh');
				
			} else {
				echo "<script>alert('Mohon Penuhi Biaya Administrasi!');history.go(-1);</script>";
			}
		}
	}

	function printkrs($kodebaru){
		$a=substr($kodebaru, 16,1);
		$npm=substr($kodebaru, 0,12);
		$ta=substr($kodebaru, 12,5);

		// echo $a.'--'.$npm.'--'.$ta;exit();

	    if ($a == 1) {
	      $b = 'Ganjil';
	    } else {
	      $b = 'Genap';
	    }

	    $cetak['kdver'] = $this->db->query('SELECT kd_krs_verifikasi from tbl_verifikasi_krs_tes WHERE npm_mahasiswa = "'.$npm.'" and tahunajaran = "'.$ta.'" order by kd_krs_verifikasi desc limit 1')->row();

		$cetak['footer'] = $this->db->select('npm_mahasiswa,id_pembimbing')
									->from('tbl_verifikasi_krs_tes')
									->like('npm_mahasiswa', $npm,'both')
									->get()->row();

		$cetak['kd_krs'] = $kodebaru;
		
	    $cetak['gg']  = $b;
	    $cetak['npm'] = $npm;
	    $cetak['ta'] = $ta;

		$this->load->view('welcome/print/krs_pdf',$cetak);
	}

	function load_dosen_autocomplete(){


        $this->db->distinct();

        $this->db->select("a.id_kary,a.nid,a.nama");

        $this->db->from('tbl_karyawan a');

        $this->db->like('a.nama', $_GET['term'], 'both');

        $this->db->or_like('a.nid', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

                            'id_kary'       => $row->id_kary,

                            'nid'           => $row->nid,

                            'value'         => $row->nid.' - '.$row->nama

                            );

        }

        echo json_encode($data);

    }

    function revisi_krs($id){
    	$tglnow = date('Y-m-d');
    	$kd_dosen = $this->db->where('kd_krs',$id)->get('tbl_verifikasi_krs_tes')->row();
    	
    	$this->session->set_userdata('kd_krs',$id);
    	
    	$this->session->set_userdata('kd_dosen', $kd_dosen->id_pembimbing);
    	$this->session->set_userdata('kd_kelas', $kd_dosen->kelas);
    	//die($this->session->userdata('kd_dosen'));
    	//var_dump($kd_dosen);die();
    	
    	$data['mhs'] = $this->db->where('npm_mahasiswa',$log['userid'])->get('tbl_pa',1)->row();
		$jam = date("H:i");
		if ($jam > '07:59' and $jam < '21:01') {
			$npm = $this->session->userdata('sess_login');
			$log = $this->db->query("SELECT KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS = '".$npm['userid']."'")->row()->KDPSTMSMHS;
			if ((substr($npm['userid'], 0,4) != '2017') and ($log == '61201' or $log == '73201' or $log == '62201')) {
				if (($tglnow > '2017-08-13' and $tglnow < '2017-08-17') or ($tglnow > '2017-08-22' and $tglnow < '2017-08-25')) {
					redirect(base_url().'akademik/krs_mhs/pengisian_krs');
				} else {
					echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
				}
			} elseif ((substr($npm['userid'], 0,4) != '2017') and ($log == '26201' or $log == '32201' or $log == '25201' or $log == '24201' or $log == '70201')) {
				if (($tglnow > '2017-08-16' and $tglnow < '2017-08-20') or ($tglnow > '2017-08-23' and $tglnow < '2017-08-26')) {
					redirect(base_url().'akademik/krs_mhs/pengisian_krs');
				} else {
					echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
				}
			} elseif ((substr($npm['userid'], 0,4) != '2017') and ($log == '61101' or $log == '74101' or $log == '74201' or $log == '55201'))  {
				if (($tglnow > '2017-08-19' and $tglnow < '2017-08-23') or ($tglnow > '2017-08-24' and $tglnow < '2017-08-27')) {
					redirect(base_url().'akademik/krs_mhs/pengisian_krs');
				} else {
					echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
				}
			} elseif ((substr($npm['userid'], 0,4) == '2017') and ($log == '61101' or $log == '74101' or $log == '70201' or $log == '62201' or $log == '55201' or $log == '35201' or $log == '25201' or $log == '24201' or $log == '74201' or $log == '61201' or $log == '73201' or $log == '26201')) {
				if ($tglnow > '2017-08-20' and $tglnow < '2017-08-29') {
					redirect(base_url().'akademik/krs_mhs_2017/pengisian_krs');
				} else {
					echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
				}
			} else {
				echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
			}
		} else {
			echo "<script>alert('Akses pengisian KRS hanya diizinkan mulai pukul 08:00 s/d 21:00');document.location.href='".base_url()."home';</script>";
		}
    }

	function pengisian_krs(){

		$user = $this->session->userdata('sess_login');
		$nim  = $user['userid'];
		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
		if ($nim == TRUE) {
	      	$loggedmhs = $nim;
	      	$strata = substr($nim, 4,1);
	      	if ($strata == 1) {
		      	//$aktif = $this->setting_model->getaktivasi('krs')->result();
		      	$aktif = TRUE;
		      	if ($aktif == TRUE) {
		      		// jika sudah ada KRS
		      		$loadkrs = $this->db->query("SELECT * from tbl_verifikasi_krs_tes where kd_krs like '".$loggedmhs.$tahunakademik->kode."%'")->result();
		      		if (count($loadkrs) > 0) {
			      		$data['ta'] = $tahunakademik->kode;
			      		$krsan = TRUE;
			      		if ($krsan == TRUE) {

			      			// load data mahasiswa
		      			  	$mhs 	= $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $loggedmhs, 'KDPSTMSMHS', 'ASC')->row_array();

		      			  	// cek tahun akademik
		      			  	$thn 	= $tahunakademik->kode;;
		      			  	$aa 	= substr($thn, 0,4);
							$bb 	= substr($thn, -1);
							if ($bb == 2) {
								$cc = $aa.'1';
							} else{
								$cc = ($aa-1).'2';
							}

							$hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
			                        JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
			                        WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$mhs['KDPSTMSMHS'].'" AND NIMHSTRLNM = "'.$loggedmhs.'" and THSMSTRLNM = "'.$cc.'" ')->result();

		                    $st=0;
		                    $ht=0;
		                    foreach ($hitung_ips as $iso) {
		                        $h = 0;

		                        $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
		                        $ht=  $ht + $h;

		                        $st=$st+$iso->sks_matakuliah;
		                    }

		                    $ips_nr = $ht/$st;
		      			  	$data['ipsmhs_before'] 	= $ips_nr;
		      			  	$data['seekhs'] 		= $nim.$cc;
					      	$data['smtmhs'] 		= $mhs['SMAWLMSMHS'];
					      	$data['npm'] 			= $loggedmhs;
						  	$data['prodi'] 			= $mhs['KDPSTMSMHS'];
						  	$data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
						  	$data['semester'] 		= $this->app_model->get_semester($mhs['SMAWLMSMHS']);
						  	$data['data_krs'] 		= $this->app_model->get_all_khs_mahasiswa($loggedmhs)->result();
						  	$data_krs 				= $this->app_model->get_krs_mahasiswa_tes($data['npm'], $data['semester']);  // gunakan tes jika tes


						  	if($data_krs->num_rows() > 0){
								//$data['data_matakuliah'] = $data_krs->result();
								$data['kode_krs'] = $data_krs->row()->kd_krs;
								//$data['catatan_pembimbing'] = $this->app_model->get_pembimbing_krs($data['kode_krs'])->row()->keterangan_krs;
								$data['data_matakuliah'] = $data_krs->result();
								$data['status_krs'] = 1;
							}else{
								$data['data_matakuliah'] = $this->app_model->get_matkul_krs($data['semester'], $data['prodi'], $data['npm'])->result();
								$data['data_ngulang'] = $this->app_model->get_matkul_ngulang($bb, $data['npm'])->result();
								//var_dump($data_ngulang);exit();
								//$data['data_matakuliah'] .= $data_ngulang;
								// //$data['data_matakuliah'] = $this->app_model->get_matkul_krs(3, $data['prodi'], $data['npm'])->result();
								// $data['data_matakuliah'] = $this->db->query("SELECT * FROM tbl_krs krs 
								// 											JOIN tbl_matakuliah mk ON mk.`kd_matakuliah` = krs.`kd_matakuliah`
								// 											WHERE krs.`kd_krs` like '".$mhs['NIMHSMSMHS']."20171%'
								// 											AND mk.`kd_prodi` = '".$mhs['KDPSTMSMHS']."'")->result();
								$data['status_krs'] = 0;
								//print_r($data['semester']. $data['prodi']. $data['npm']);die();
								//var_dump($data['data_matakuliah']);die();
							}
							$data['dospem'] = $this->session->userdata('kd_dosen');
							
							if ($nim == '201410315011') {
						  		$data['page'] = 'akademik/krs_view_tes';
						  	}else{
						  		$data['page'] = 'akademik/krs_view_2017';
						  	}
						    //$data['page'] = 'akademik/krs_view';
						    $this->load->view('template', $data); 
			      		} else {
			      			echo "<script>alert('Mohon Penuhi Biaya Administrasi');document.location.href='".base_url()."akademik/krs_mhs_2017';</script>";
			      		}
		      		} else {
		      			// jika pertama kali KRSan
		      			$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
			      		$data['ta'] = $tahunakademik->kode;
			      		$krsan = TRUE;
			      		
			      		if ($krsan == TRUE) {

			      			// load data mahasiswa
		      			  	$mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $loggedmhs, 'KDPSTMSMHS', 'ASC')->row_array();
		      			  	
		      			  	// cek tahun akademik
		      			  	$thn = $tahunakademik->kode;
		      			  	$aa = substr($thn, 0,4);
							$bb = substr($thn, -1);
							if ($bb == 2) {
								$cc = $aa.'1';
							} else{
								$cc = ($aa-1).'2';
							}

							$hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
			                        JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
			                        WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$mhs['KDPSTMSMHS'].'" AND NIMHSTRLNM = "'.$loggedmhs.'" and THSMSTRLNM = "'.$cc.'" ')->result();

		                    $st=0;
		                    $ht=0;
		                    foreach ($hitung_ips as $iso) {
		                        $h = 0;

		                        $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
		                        $ht=  $ht + $h;

		                        $st=$st+$iso->sks_matakuliah;
		                    }

		                    $ips_nr = $ht/$st;
		      			  	$data['ipsmhs_before'] 	= $ips_nr;
		      			  	$data['seekhs'] 		= $nim.$cc;
					      	$data['smtmhs'] 		= $mhs['SMAWLMSMHS'];
					      	$data['npm'] 			= $loggedmhs;
						  	$data['prodi'] 			= $mhs['KDPSTMSMHS'];
						  	$data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
						  	$data['semester'] 		= $this->app_model->get_semester($mhs['SMAWLMSMHS']);
						  	$data['data_krs'] 		= $this->app_model->get_all_khs_mahasiswa($loggedmhs)->result();
						  	$data_krs 				= $this->app_model->get_krs_mahasiswa_tes($data['npm'], $data['semester']);  // gunakan tes jika tes

						  	if($data_krs->num_rows() > 0){
								//$data['data_matakuliah'] = $data_krs->result();
								$data['kode_krs'] 			= $data_krs->row()->kd_krs;
								$data['catatan_pembimbing'] = $this->app_model->get_pembimbing_krs($data['kode_krs'])->row()->keterangan_krs;
								$data['status_krs'] 		= 1;
							}else{
								$data['data_matakuliah']	= $this->app_model->get_matkul_krs($data['semester'], $data['prodi'], $data['npm'])->result();
								$data['status_krs'] 		= 0;
								$data['data_ngulang'] 		= $this->app_model->get_matkul_ngulang($bb, $data['npm'])->result();
								//$data['data_matakuliah'] .= $data_ngulang;
								//var_dump($data['data_matakuliah']);exit();
								//var_dump($bb.' - '.$data['npm']);exit();
								//print_r($data['semester']. $data['prodi']. $data['npm']);die();
								//var_dump($data['data_matakuliah']);die();
							}
							$data['dospem'] = $this->session->userdata('kd_dosen');
							if ($nim == '201410315011') {
						  		$data['page'] = 'akademik/krs_view_tes';
						  	}else{
						  		$data['page'] = 'akademik/krs_view_2017';
						  	}

						    //$data['page'] = 'akademik/krs_view';
						    $this->load->view('template', $data); 
			      		} else {
			      			echo "<script>alert('Mohon Penuhi Biaya Administrasi');document.location.href='".base_url()."akademik/krs_mhs_2017';</script>";
			      		} 
		      		}
		      		
			    } else {
			        echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."akademik/krs_mhs_2017';</script>";
			    }
	    	} else {
	    		// pengisian untuk S2
		      	$data['ta'] = $tahunakademik->kode;
		      	$aktif = $this->setting_model->getaktivasi('krss2')->result();
		      	if ($aktif == TRUE) {
		      		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
		      		//$krsan = $this->app_model->getkrsanmhs($loggedmhs,$tahunakademik->kode)->row();
		      		//$krsan = $this->app_model->getkrsanmhs($loggedmhs,'20162')->row();
		      		$krsan = TRUE;
		      		//var_dump($krsan);exit();
		      		if ($krsan == TRUE) {

		      			// load data mahasiswa
			    	  	$mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $loggedmhs, 'KDPSTMSMHS', 'ASC')->row_array();

			    	  	// cek tahun akademik
			    	  	$thn = $tahunakademik->kode;
	      			  	$aa = substr($thn, 0,4);
						$bb = substr($thn, -1);
						if ($bb == 2) {
							$cc = $aa.'1';
						} else{
							$cc = ($aa-1).'2';
						}

						$hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
		                        						JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah` WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$mhs['KDPSTMSMHS'].'" AND NIMHSTRLNM = "'.$loggedmhs.'" and THSMSTRLNM = "'.$cc.'" ')->result();

	                    $st=0;
	                    $ht=0;
	                    foreach ($hitung_ips as $iso) {
	                        $h = 0;

	                        $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
	                        $ht=  $ht + $h;

	                        $st=$st+$iso->sks_matakuliah;
	                    }

	                    $ips_nr = $ht/$st;
	      			  	$data['ipsmhs_before'] 	= $ips_nr;
	      			  	$data['seekhs']			= $nim.$cc;
				      	$data['npm'] 			= $loggedmhs;
					  	$data['prodi'] 			= $mhs['KDPSTMSMHS'];
					  	$data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
					  	$data['semester'] 		= $this->app_model->get_semester($mhs['SMAWLMSMHS']);
					  	$data['data_krs'] 		= $this->app_model->get_all_khs_mahasiswa($loggedmhs)->result();
					  	$data_krs 				= $this->app_model->get_krs_mahasiswa_tes($data['npm'], $data['semester']); // gunakan tes jika tes
					  	
					  	if($data_krs->num_rows() > 0){
							$data['data_matakuliah'] = $data_krs->result();
							$data['kode_krs'] = $data_krs->row()->kd_krs;
							$data['catatan_pembimbing'] = $this->app_model->get_pembimbing_krs($data['kode_krs'])->row()->keterangan_krs;
							$data['status_krs'] = 1;
						} else {
							$data['data_matakuliah'] = $this->app_model->get_matkul_krs($data['semester'], $data['prodi'], $data['npm'])->result();
							$data['status_krs'] = 0;
					  	}

					  	$data['dospem'] = $this->session->userdata('kd_dosen');

					  	if ($nim == '201410315011') {
					  		$data['page'] = 'akademik/krs_view_tes';
					  	}else{
					  		$data['page'] = 'akademik/krs_view_2017';
					  	}
						
				      	//$data['page'] = 'akademik/krs_view';
				      	$this->load->view('template', $data);

				    } else {
		      			echo "<script>alert('Mohon Penuhi Biaya Administrasi');document.location.href='".base_url()."akademik/krs_mhs_2017 ';</script>";
		      		} 
			    } else {
			        echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."akademik/krs_mhs_2017';</script>";
			        //echo "gabisa";
			    }

	      	}
		 
	    } else {
	      echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
	    }
	}

	function cekjumlahsks()
	{
		$loggedmhs		= $this->session->userdata('sess_login');
		$npm 			= $loggedmhs['userid'];
		$mhs 			= $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $loggedmhs['userid'], 'KDPSTMSMHS', 'ASC')->row_array();
		$status 		= $this->db->query("SELECT * FROM tbl_status_mahasiswa where npm = '".$loggedmhs['userid']."' and tahunajaran = '20162'")->row_array();
	    $data['prodi']	= $mhs['KDPSTMSMHS'];
	    //$data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
	    $hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
        JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
        WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$data['prodi'].'" AND NIMHSTRLNM = "'.$npm.'" and THSMSTRLNM = "20162" ')->result();

        $st=0;
        $ht=0;
        foreach ($hitung_ips as $iso) {
            $h = 0;

            $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
            $ht=  $ht + $h;

            $st=$st+$iso->sks_matakuliah;
        }

        $ips_nr = $ht/$st;
	    $semester = $this->app_model->get_semester($mhs['SMAWLMSMHS']);
		// $sms = $semester - 1;

		$pecah = substr($npm, 0,4);
		$sks = $_POST['sks'];
		if ($sks > 24) {
			echo 0;
		} else {
			$b = $this->app_model->ketentuan_sks_2017(number_format($ips_nr,2),$sks);
			// if (($semester == 1) or ($semester == 2)) {
			// 	echo 1;
			// } elseif(($mhs['KDPSTMSMHS'] == '25201' or $mhs['KDPSTMSMHS'] == '32201') and (substr($npm, 0,4) < 2015)){
			// 	echo 1;
			// } elseif (($status['status'] == 'CA') and (number_format($ips_nr,2) == 0.00)) {
			// 	echo 1;
			// } else {
			// 	echo $b;
			// }
			echo $b;
		}
	}

	function get_matkul(){
		$semester = $_POST['semester'];
		// var_dump($semester);exit();
		
		$npm = $_POST['npm'];
		$prodi = $_POST['prodi'];
		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row()->tahun_akademik;
		
		if(($tahunakademik % 2) == 1 and $semester == 8){ 
			$data = $this->app_model->get_matkul_krs_spesial($semester, $prodi, $npm,1)->result();
		} elseif(($tahunakademik % 1) == 0 and $semester == 6) {
			$data = $this->app_model->get_matkul_krs_spesial($semester, $prodi, $npm,2)->result();
		} elseif(($tahunakademik % 2) == 0 and $semester == 7) {
			$data = $this->app_model->get_matkul_krs_spesial($semester, $prodi, $npm,2)->result();
		} else {
			$data = $this->app_model->get_matkul_krs_spesial_not_krs($semester, $prodi, $npm)->result();
		}
	                
	    $js = json_encode($data);
	    echo $js;
	 }

	function cek_prasyarat()
	{
		$log = $this->session->userdata('sess_login');
		$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$log['userid'],'NIMHSMSMHS','asc')->row();
		$sym  = array('[', ']');
		$prasyarat = array();
		$hit = array();
		$prasyarat = explode(',', str_replace($sym,'',$_POST['prasyarat']));
		
		for ($i=0; $i < count($prasyarat); $i++) {
			$c++;
			$kv = $this->db->where('kd_baru',$prasyarat[$i])->where('flag',1)->where('kd_prodi',$mhs->KDPSTMSMHS)->get('tbl_konversi_matkul_temp')->row();
				if (count($kv) > 0) {
					$hit[] = $kv->kd_lama;
					
					$kv2 = $this->db->where('kd_baru',$kv->kd_lama)->where('flag',1)->where('kd_prodi',$mhs->KDPSTMSMHS)->get('tbl_konversi_matkul_temp')->row();
						if (count($kv2) > 0) {
							$hit[] = $kv2->kd_lama;

							$kv3 = $this->db->where('kd_baru',$kv2->kd_lama)->where('flag',1)->where('kd_prodi',$mhs->KDPSTMSMHS)->get('tbl_konversi_matkul_temp')->row();
							if (count($kv3) > 0) {
								$hit[] = $kv3->kd_lama;
							}
						}
				}
		}

		$gabung = array_merge($prasyarat, $hit);
		// $gabung = array();
		if (substr($log['userid'], 8,1) == '7') {
			$data2 = $this->temph_model->cek_prasyarat_konversi($gabung,$log['userid'])->num_rows();
			$data3 = $this->temph_model->cek_prasyarat($gabung,$log['userid'])->num_rows();
			$data = $data2+$data3;
		} else {
	        $data = $this->temph_model->cek_prasyarat($gabung,$log['userid'])->num_rows();
		}
		//$data = $this->temph_model->cek_prasyarat($gabung,$log['userid'])->num_rows();
		echo $data;
	}

	function get_krs(){
		$semester = $_POST['semester'];
		$npm = $_POST['npm'];
		
	    $data = $this->app_model->get_matkul_krs_rekam($semester, $npm)->result();
	                
	    $js = json_encode($data);
	    echo $js;
	}

	function saving(){

		$user = $this->session->userdata('sess_login');
		$nim  = $user['userid'];

		$kodebaru = $this->input->post('kode_krs');	

		//die($kodebaru);

		$kd_matakuliah = $this->input->post('kd_matkuliah');
		$krs['semester_krs'] = $this->input->post('semester');
		$krs['npm_mahasiswa'] = $nim;
		//$krs['id_pembimbing'] = $this->session->userdata('kd_dosen');

		if (count($this->input->post('kd_matkuliah')) == 0) {
			echo "<script>alert('Pilih Matakuliah Terlebih Dahulu!');document.location.href='".base_url('akademik/krs_mhs_2017/pengisian_krs')."';</script>";
		} else {
			
			$this->db->order_by('kode', 'desc');
			$this->db->where('status', '1');
			$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row()->kode;
			$ta = $tahunakademik;		

			if ($kodebaru != '') { //sudah ada krs
				$mkbaru = array();
				foreach ($kd_matakuliah as $key => $n) {
					$mkbaru[] = $n;
					$getkrsbefore = $this->app_model->getkrsbefore_tes($kodebaru,$n)->row();
					if (count($getkrsbefore) == 0) {
						$krs['kd_matakuliah'] = $n;
						$krs['kd_krs'] = $kodebaru;								
						$krs = $this->security->xss_clean($krs);	
						$this->app_model->insertdata('tbl_krs_tes', $krs);
					} 
				}

				$getmkkrsbefore = $this->app_model->cekmkbarukrs_tes($kodebaru,$mkbaru)->result();		
				foreach ($getmkkrsbefore as $value) {
					$this->app_model->deletedata('tbl_krs_tes','id_krs',$value->id_krs);
				}

				// kelas
				if ($this->session->userdata('kelas') != TRUE) {
					$cls = $this->db->query("SELECT * from tbl_verifikasi_krs_tes where kd_krs like '".$nim.$tahunakademik."%'")->row();
					$kelas = $cls->kelas;
				} else {
					$kelas = $this->session->userdata('kelas');
				}

				// jika dosen pembimbing kosong
				if ($this->input->post('kodos') == '' or is_null($this->input->post('kodos'))) {
					$cekdosen = $this->db->query("SELECT * from tbl_pa where npm_mahasiswa = '".$nim."'")->row()->kd_dosen;
					$kodedosen = $cekdosen;
				} else {
					$kodedosen = $this->input->post('kodos');
				}
				

				$vkrs['id_pembimbing'] = $kodedosen;
				$vkrs['kd_krs'] = $kodebaru;
				// $vkrs['keterangan_krs'] = $this->input->post('keterangan_krs');

				$vkrs['tgl_bimbingan'] = date('Y-m-d H:i:s');
				$vkrs['key'] = $this->generateRandomString();
				
				$vkrs['tahunajaran'] = $ta;
				$vkrs['npm_mahasiswa'] = $krs['npm_mahasiswa'];

				$jurusan = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$vkrs['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();

				$vkrs['kd_jurusan'] = $jurusan->KDPSTMSMHS;
				$vkrs['jumlah_sks'] = $this->input->post('jumlah_sks', TRUE);
				$kodver = $kodebaru.$vkrs['jumlah_sks'].$vkrs['key'];
				$vkrs['kd_krs_verifikasi'] = md5(md5($kodver).key_verifikasi);

				//qr code
				$this->load->library('ciqrcode');
				$params['data'] = base_url().'welcome/welcome/cekkodeverifikasi/'.$vkrs['kd_krs_verifikasi'].'';
				$params['level'] = 'H';
				$params['size'] = 10;
				$params['savename'] = FCPATH.'QRImage/'.$vkrs['kd_krs_verifikasi'].'.png';
				$this->ciqrcode->generate($params);
				//end qr code

				$this->app_model->deletedata('tbl_verifikasi_krs_tes','kd_krs',$kodebaru);
				$nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$vkrs['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
				$slug = url_title($nama->NMMHSMSMHS, '_', TRUE);
				$vkrs['slug_url'] = $slug;
				$vkrs['kelas'] = $kelas;
				$vkrs = $this->security->xss_clean($vkrs);
				$this->app_model->insertdata('tbl_verifikasi_krs_tes', $vkrs);

				
			}else{ // belum ada ada KRS
				$kodeawal = $krs['npm_mahasiswa'].$ta;
				$hitung = strlen($kodeawal);
				$jumlah = 0;
				for ($i=0; $i <= $hitung; $i++) {
					$char = substr($kodeawal,$i,1);
					$jumlah = $jumlah + $char;
				}
				$mod = $jumlah%10;
				$kodebaru = $krs['npm_mahasiswa'].$ta.$mod;
				
				foreach ($kd_matakuliah as $key => $n) {
					$krs['kd_matakuliah'] = $n;
					$krs['kd_krs'] = $kodebaru;		
					$krs = $this->security->xss_clean($krs);
					$this->app_model->insertdata('tbl_krs_tes',$krs);
				}
				
				$vkrs['id_pembimbing'] = $this->session->userdata('kd_dosen');
				$vkrs['kd_krs'] = $kodebaru;
				$vkrs['npm_mahasiswa'] = $krs['npm_mahasiswa'];
				$jurusan = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$vkrs['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
				$vkrs['kd_jurusan'] = $jurusan->KDPSTMSMHS;
				// $vkrs['keterangan_krs'] = $this->input->post('keterangan_krs');
				$vkrs['tgl_bimbingan'] = date('Y-m-d H:i:s');
				$vkrs['key'] = $this->generateRandomString();
				$vkrs['tahunajaran'] = $ta;
				$vkrs['jumlah_sks'] = $this->input->post('jumlah_sks', TRUE);
				$kodver = $kodebaru.$vkrs['jumlah_sks'].$vkrs['key'];
				$vkrs['kd_krs_verifikasi'] = md5(md5($kodver).key_verifikasi);

				// print QRcode
				$this->load->library('ciqrcode');
				$params['data'] = base_url().'welcome/welcome/cekkodeverifikasi/'.$vkrs['kd_krs_verifikasi'].'';
				$params['level'] = 'H';
				$params['size'] = 10;
				$params['savename'] = FCPATH.'QRImage/'.$vkrs['kd_krs_verifikasi'].'.png';
				$this->ciqrcode->generate($params);
				// var_dump($vkrs);die('dua');

				$nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$vkrs['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
				$slug = url_title($nama->NMMHSMSMHS, '_', TRUE);
				$vkrs['slug_url'] = $slug;
				$vkrs['kelas'] = $this->session->userdata('kelas');
				//var_dump($vkrs);die();
				$vkrs = $this->security->xss_clean($vkrs);
				$this->app_model->insertdata('tbl_verifikasi_krs_tes', $vkrs);
			}
		}

		

		$krs['pembimbing'] = $this->app_model->get_pembimbing_krs($kodebaru)->row_array();
		$krs['detail_krs'] = $this->app_model->get_detail_print_krs_mahasiswa($kodebaru)->result();
		$krs['kd_krs'] = $kodebaru;

		// $this->jadwal_kuliah($krs['npm_mahasiswa'],$krs['kd_krs']);
		$this->save_sesi($krs['npm_mahasiswa'],$krs['kd_krs']);
		// redirect(base_url().'akademik/krs_mhs_2017/jadwal_kuliah/'.$krs['npm_mahasiswa'].'/'.$krs['kd_krs'],'refresh');
	}

	function save_sesi($npm,$kdkrs)
	{
		$array = array(
			'npm' 	=> $npm,
			'kd_krs'	=> $kdkrs
		);
		
		$this->session->set_userdata('sess_for_jdl', $array);
		redirect(base_url('akademik/krs_mhs_2017/jadwal_kuliah'),'refresh');
	}

  	function jadwal_kuliah()
  	{
  		$log_jdl = $this->session->userdata('sess_for_jdl');
  		$user = $this->session->userdata('sess_login');

  		// cek apakah krs paket atau bukan
  		// $akt = substr($user['userid'], 0,4);
  		// if ($akt == date('Y')) {
  		// 	echo "<script>alert('Akses pada menu ini ditolak untuk Anda!');history.go(-1);</script>"; exit();
  		// }  		

		$nim  = $user['userid'];
  		if ($log_jdl['npm'] != $nim) {
			$this->session->unset_userdata('sess_login');
			echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."auth';</script>";
  		} else {
  			// die($this->session->userdata('kelas'));
  			$cekdata = $this->db->query("SELECT * from tbl_krs_tes where kd_krs = '".$log_jdl['kd_krs']."'")->result();
  			if (count($cekdata) == 0) {
  				echo "<script>alert('Data Tidak Ditemukan !');document.location.href='".base_url('akademik/krs_mhs_2017')."';</script>";
  			} else {
  				$data['npm_mahasiswa'] = $log_jdl['npm'];
				$data['kd_krs']		= $log_jdl['kd_krs'];

				$data['pembimbing'] = $this->app_model->get_pembimbing_krs_tes($log_jdl['kd_krs'])->row_array();
				$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
				// $data['noteformhs'] = $this->db->query("SELECT note from tbl_bimbingan_krs where kd_krs like '".$nim.$tahunakademik->kode."%' order by id desc limit 1")->row_array();
				$data['detail_krs'] = $this->app_model->get_detail_print_krs_mahasiswa_tes($log_jdl['kd_krs'])->result();
				$data['comparemk'] = $this->db->query("SELECT count(npm_mahasiswa) as mk from tbl_krs_tes where kd_krs like '".$nim.$tahunakademik->kode."%'")->row()->mk;
				$data['comparejd'] = $this->db->query("SELECT count(npm_mahasiswa) as jd from tbl_krs_tes where kd_krs like '".$nim.$tahunakademik->kode."%' and (kd_jadwal != NULL or kd_jadwal != '') ")->row()->jd;
				//var_dump($data['detail_krs']);die();

				$data['page'] = 'akademik/krs_mhs_jdl_2017';
				$this->load->view('template',$data);
  			}   
  		}
	}

	function ajukan_krs($kd_krs){
		$npm_mahasiswa = substr($kd_krs, 0,12);

		$object = array('status_verifikasi' => 4);
		$this->db->where('kd_krs', $kd_krs);
		$this->db->update('tbl_verifikasi_krs_tes', $object);
		redirect(base_url('akademik/krs_mhs_2017/jadwal_kuliah'),'refresh');
		// $this->jadwal_kuliah($npm_mahasiswa,$kd_krs);
	}

	function generateRandomString() {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function get_jadwal(){
		//die($this->session->userdata('kelas'));
	  	$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
	  	//$loggedmhs = $this->session->userdata('sess_mhs');
		$kd_matakuliah = $_POST['kd_matakuliah'];

		$user = $this->session->userdata('sess_login');
		$nim  = $user['userid'];

		$cls = $this->db->query("SELECT * from tbl_verifikasi_krs_tes where kd_krs like '".$nim.$tahunakademik->kode."%'")->row();

		if ($this->session->userdata('kelas') != TRUE) {
			$kelas = $cls->kelas;
		} else {
			$kelas = $this->session->userdata('kelas');
		}

		$mhs  = $this->app_model->get_jurusan_mhs($nim)->row();
		// echo $kelas;
		$data = $this->app_model->get_pilih_jadwal_krs($kd_matakuliah,$mhs->KDPSTMSMHS,$tahunakademik->kode,$kelas)->result();
                
		$js = json_encode($data);
		echo $js;
	}

	function save_jadwal(){
		$data['kd_jadwal'] = $this->input->post('kd_jadwal');
		$kd_matakuliah = $this->input->post('kd_matakuliah');
		$kd_krs= $this->input->post('kd_krs');

		$npm_mahasiswa = substr($kd_krs, 0,12);
		//die($npm_mahasiswa);
		
		$this->app_model->updatedata_krs_tes($kd_matakuliah, $kd_krs, $data);
		//redirect('form/formkrs/jadwal_kuliah','refresh');

		$this->jadwal_kuliah($npm_mahasiswa,$kd_krs);

	}

	function kosongkan_jadwal($id,$id2)
	{

		$data = array('kd_jadwal' => null );
		$npm_mahasiswa = substr($id, 0,12);


		$this->db->where('kd_krs', $id);
		$this->db->where('kd_matakuliah', $id2);
		$this->db->update('tbl_krs_tes', $data);

		$q = $this->db->select('kd_krs')
						->from('tbl_krs_tes')
						->where('id_krs',$id)
						->get()->row();

		// $this->jadwal_kuliah($npm_mahasiswa,$id);

		redirect(base_url().'akademik/krs_mhs_2017/jadwal_kuliah/'.$npm_mahasiswa.'/'.$id,'refresh');

	}

	function verify()
	{
	    $id = $this->input->post('username', TRUE);
	    $cek = $this->input->post('g-recaptcha', TRUE);
	    if ($_POST['g-recaptcha-response'] != '') {
	      $secret = '6LeLLAoUAAAAAHQPTHRFKtaMGVexvRQM1_5wD4c4';
	      //get verify response data
	      $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
	      $responseData = json_decode($verifyResponse);
	      if ($responseData->success) {
	        echo 'Your contact request have submitted successfully.';;
	      } else {
	        echo "salah ces";
	      }
	    } else {
	      echo "salah ces";
	    }
	    //var_dump($responseData);
	    exit();
	}

	function view($npm, $id)
	{
		
		$data['npm'] = $npm;

		$data['ips'] = $this->app_model->get_ips_mahasiswa($npm, $id)->row()->ips;

		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'NIMHSMSMHS','asc')->row();
		
		$data['semester'] = $id;
		$data['kode'] = $this->db->query("SELECT distinct kd_krs from tbl_krs_tes where npm_mahasiswa = '".$npm."' and semester_krs = ".$id." ")->row();
		//$data['detail_khs'] = $this->app_model->get_detail_khs_mahasiswa($npm, $id)->result();
		$data['detail_khs'] = $this->db->query("SELECT a.*,b.nama_matakuliah,b.sks_matakuliah from tbl_krs_tes a join tbl_matakuliah b on a.kd_matakuliah = b.kd_matakuliah 
												where a.npm_mahasiswa = '".$npm."' and semester_krs = ".$id." and b.kd_prodi = '".$data['mhs']->KDPSTMSMHS."'")->result();
		$data['page'] = 'akademik/khs_detail2';
		$this->load->view('template',$data);	
	}

	function load_khs($kd)
	{
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',substr($kd, 0,12),'NIMHSMSMHS','asc')->row();
		$data['q'] = $this->db->query("SELECT distinct a.*,b.nama_matakuliah,b.sks_matakuliah from tbl_krs_tes a join tbl_matakuliah b on a.kd_matakuliah = b.kd_matakuliah 
										where a.kd_krs like '".$kd."%' and b.kd_prodi = '".$data['mhs']->KDPSTMSMHS."'")->result();
		$sks = 0;
		foreach ($data['q'] as $key) {
			$sks = $sks + $key->sks_matakuliah;
		}
		$data['jumsks'] = $sks;
		//$data['tahunakad'] = substr($kd, 12,17);

		$thn = substr($kd, 12,5);
		$data['tahunakad'] = $thn;
		
		$hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
                JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
                WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$data['mhs']->KDPSTMSMHS.'" AND NIMHSTRLNM = "'.substr($kd, 0,12).'" and THSMSTRLNM = "'.$thn.'" ')->result();

        $st=0;
        $ht=0;
        foreach ($hitung_ips as $iso) {
            $h = 0;

            $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
            $ht=  $ht + $h;

            $st=$st+$iso->sks_matakuliah;
        }

        $ips_nr = $ht/$st;
		$data['ipsmhs_before'] = $ips_nr;

		//$data['ipsmhs_before'] = $this->db->query("SELECT ips from tbl_ips where kd_krs like '".$kd."%'")->row()->ips;
		$data['smt'] = $this->db->query("SELECT semester_krs from tbl_krs_tes where kd_krs like '".$kd."%'")->row()->semester_krs;
		// var_dump($tahunakad);exit();
		$this->load->view('detil_khs_modal', $data);
	}


}

/* End of file bimbingan.php */
/* Location: ./application/modules/akademik/controllers/bimbingan.php */