<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(62)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$sess = $this->session->userdata('sess_login');
		$result = $sess['userid'];

		$data['query'] = $this->db->query('SELECT * from tbl_mahasiswa
											join tbl_jurusan_prodi on tbl_mahasiswa.`KDPSTMSMHS`=tbl_jurusan_prodi.`kd_prodi`
											join tbl_fakultas on tbl_fakultas.`kd_fakultas`=tbl_jurusan_prodi.`kd_fakultas`
											where NIMHSMSMHS="'.$result.'"')->row();
		//var_dump($data);die();
		$m = $data['query']->NIMHSMSMHS;
		$data['t'] = substr($m,2);
		if (substr($m, 0,4) > 2015) {
			$kode = '70207';
		} else {
			$kode = '70306';
		}
		
		$b = array($kode,$data['t']);
		$data['c'] = implode("", $b);

		$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$m,'NIMHSMSMHS','asc')->row();
		$tahunakademik = $this->db->get('tbl_tahunakademik', 1)->row();
		$selisih = ($tahunakademik->kode) - ($mhs->SMAWLMSMHS);
		$semawal = (substr($selisih, 0,1)) * 2;
		$semtambah = substr($selisih, 1,2) + 1;
		$data['semester'] = $semawal + $semtambah;
		//$a = $query->NIMHSMSMHS;
		$data['skrg'] = $tahunakademik->kode;
		$data['row'] = $this->db->query('SELECT DISTINCT smtr FROM tbl_rekap_pembayaran_mhs WHERE nim = '.$data['query']->NIMHSMSMHS.'')->result();

		$data['ta'] = $this->app_model->getdata('tbl_tahunakademik','kode','asc')->result();

		//var_dump($data['row']);die();

		$data['page'] = 'akademik/status_view';
		$this->load->view('template',$data);
	}

	function load_detail($npm){

		//die($npm);

		//$err=explode('-', $id);

		//$data['nim'] = $err[0];
		//$data['smtr']= $err[1];

		$data['row']=$this->db->query('SELECT * FROM tbl_rekap_pembayaran_mhs WHERE nim = '.$npm.'')->result();
		//var_dump($data['row']);die();
		$this->load->view('akademik/status_detail',$data);
	}

}

/* End of file viewkrs.php */
/* Location: .//C/Users/danum246/Desktop/viewkrs.php */