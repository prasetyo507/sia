<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_mkdu_schedule extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			
			if ($this->session->userdata('sess_login')['id_user_group'] != 10) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$this->changeMkduForMhs();
	}

	function changeMkduForMhs()
	{
		if ($this->session->userdata('sess_schedule')) {
			$this->session->unset_userdata('sess_schedule');
		}
		$data['page'] = "v_changemkdumhs";
		$this->load->view('template', $data);
	}

	function autocomplete_mkdu()
	{
		$actYear = getactyear();
		
		$this->db->select('a.id_jadwal, a.kd_matakuliah, a.kd_dosen, a.kelas, a.kd_jadwal, b.nama_matakuliah, b.kd_prodi');
		$this->db->from('tbl_jadwal_matkul a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');
		$this->db->where('a.kd_dosen <> ""');
		$this->db->where('a.kd_dosen IS NOT NULL');
		$this->db->where('a.kd_tahunajaran', $actYear);
		$this->db->where('b.kd_prodi = SUBSTRING(a.kd_jadwal,1,5)');
		$this->db->group_start();
		$this->db->like('a.kd_matakuliah', $_GET['term'], 'BOTH');
		$this->db->or_like('b.nama_matakuliah', $_GET['term'], 'BOTH');
		$this->db->or_like('a.kelas', $_GET['term'], 'BOTH');
		$this->db->group_end();
		$sql = $this->db->get()->result();

		$data = [];

		foreach ($sql as $key) {
			$data[] = [
				'value' => $key->kd_matakuliah.' - '.$key->nama_matakuliah.' - '.nama_dsn($key->kd_dosen).' - '.$key->kelas.' - '.get_jur($key->kd_prodi).' - '.$key->id_jadwal
			];
		}

		echo json_encode($data);

	}

	function create_sess_schedule()
	{
		$schedulebefore = $this->input->post('jdl_before');
		$scheduleafter = $this->input->post('jdl_after');

		$exp = explode(' - ', $schedulebefore);

		$amountofarray = count($exp);
		$getarray_idjdl = $amountofarray-1;

		$kd_mk = $exp[0];
		$kelas = $exp[3];
		$jadwal = $exp[$getarray_idjdl];
		$dosen = $exp[2];

		$exp2 = explode(' - ', $scheduleafter);

		$amountofarray2 = count($exp2);
		$getarray_idjdl2 = $amountofarray2-1;

		$kd_mk2 = $exp2[0];
		$kelas2 = $exp2[3];
		$jadwal2 = $exp2[$getarray_idjdl2];
		$dosen2 = $exp2[2];

		$array = array(
			'before_kode' => $kd_mk,
			'before_kelas' => $kelas,
			'before_jadwal' => $jadwal,
			'before_dosen' => $dosen,
			'after_kode' => $kd_mk2,
			'after_kelas' => $kelas2,
			'after_jadwal' => $jadwal2,
			'after_dosen' => $dosen2
		);
		
		$this->session->set_userdata('sess_schedule', $array);
		redirect(base_url('akademik/change_mkdu_schedule/loadliststudent'));
	}

	function loadliststudent()
	{
		$sess_schedule = $this->session->userdata('sess_schedule');

		$id_jadwal = $sess_schedule['before_jadwal'];
		$kd_jdl = get_kd_jdl($id_jadwal);
		$prodi = substr($kd_jdl, 0,5);

		$id_jadwal2 = $sess_schedule['after_jadwal'];
		$kd_jdl2 = get_kd_jdl($id_jadwal2);
		$prodi2 = substr($kd_jdl2, 0,5);

		// var_dump($sess_schedule['before_kode'].'=='.$prodi.'<<<<>>>>'.$sess_schedule['after_kode'].'=='.$prodi2);
		// exit();

		$data['kd_mk'] = $sess_schedule['before_kode'];
		$data['kelas'] = $sess_schedule['before_kelas'];
		$data['nm_mk'] = get_nama_mk($sess_schedule['before_kode'],$prodi);
		$data['dosen'] = $sess_schedule['before_dosen'];
		$data['idjadwal'] = $sess_schedule['before_jadwal'];

		$data['kd_mk2'] = $sess_schedule['after_kode'];
		$data['kelas2'] = $sess_schedule['after_kelas'];
		$data['nm_mk2'] = get_nama_mk($sess_schedule['after_kode'],$prodi2);
		$data['dosen2'] = $sess_schedule['after_dosen'];
		$data['idjadwal2'] = $sess_schedule['after_jadwal'];

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";

		// get schedule detail
		$data['mhs'] = $this->db->query("SELECT * from tbl_krs where kd_jadwal = '".$kd_jdl."'")->result();
		$data['page'] = "v_listmhsmkdu";
		$this->load->view('template', $data);
	}

	function updateJadwalMkdu()
	{
		error_reporting(0);
		$npm = $this->input->post('npm');
		$sess  = $this->session->userdata('sess_schedule');
		$kd_jdl_bf = get_kd_jdl($sess['before_jadwal']);
		$kd_jdl_af = get_kd_jdl($sess['after_jadwal']);

		// echo "<pre>";
		// print_r ($npm);
		// echo "</pre>"; exit();

		for ($i=0; $i < count($npm); $i++) {
			$object = ['kd_jadwal' => $kd_jdl_af]; 
			$this->db->like('kd_krs', $npm[$i], 'after');
			$this->db->where('kd_jadwal', $kd_jdl_bf);
			$this->db->update('tbl_krs', $object);
		}

		echo "<script>alert('Berhasil!');</script>";
		redirect(base_url('akademik/change_mkdu_schedule/changeMkduForMhs'),'refresh');
	}

	function backmkdu()
	{
		$this->session->unset_userdata('sess_schedule');
		redirect(base_url('akademik/change_mkdu_schedule/changeMkduForMhs'),'refresh');
	}

}

/* End of file Change_mkdu_schedule.php */
/* Location: ./application/modules/akademik/controllers/Change_mkdu_schedule.php */