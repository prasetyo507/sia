<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Void_new_student extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			echo "<script>alert('Silahkan log-in kembali untuk memulai sesi!');</script>";
			redirect(base_url('auth/logout'),'refresh');
		} else {
			$this->sess = $this->session->userdata('sess_login');
		}	
	}

	public function index()
	{
		$data['page'] = "vselect_angkatan";
		$this->load->view('template', $data);		
	}

	function savesess()
	{
		extract(PopulateForm());
		$this->session->set_userdata('newstd', $angkatan);
		redirect(base_url('akademik/void_new_student/listStudent'));
	}

	function listStudent()
	{
		$data['sess'] = $this->session->userdata('newstd');
		$data['page'] = "vnew_student";
		$this->load->view('template', $data);
	}

	function loadData($gen)
	{
		$this->load->model('hilman_model','hilman');
		$log = $this->sess['userid'];
		$arr = ['KDPSTMSMHS' => $log, 'TAHUNMSMHS' => $gen];
		$data['data'] = $this->hilman->moreWhere($arr,'tbl_mahasiswa');
		$this->load->view('table_newstd', $data);
	}

	function remove($nim)
	{
		$this->app_model->deletedata('tbl_mahasiswa','NIMHSMSMHS',$nim);
		$this->app_model->deletedata('tbl_sinkronisasi_renkeu','npm_mahasiswa',$nim);
		$this->app_model->deletedata('tbl_krs','npm_mahasiswa',$nim);
		$this->app_model->deletedata('tbl_verifikasi_krs','npm_mahasiswa',$nim);
		$this->app_model->deletedata('tbl_pa','npm_mahasiswa',$nim);
	}

}

/* End of file Void_new_student.php */
/* Location: .//tmp/fz3temp-1/Void_new_student.php */