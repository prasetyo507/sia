<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absenmhs extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			echo "<script>alert('Silahkan login kembali untuk memulai sesi!');</script>";
			redirect(base_url('auth/logout'),'refresh');
		}
		// error_reporting(0);
		$this->sess = $this->session->userdata('sess_login');
		$this->load->model('akademik/absen_model', 'absen');
	}

	public function index()
	{
		$tgl = strtotime(date('Y-m-d'));
		$day = dayToNumber(date('l', $tgl));
		$actyear = getactyear();
		$kdkrs = $this->sess['userid'].$actyear;
		$data['list'] = $this->absen->loadListJadwal($kdkrs,$day)->result();
		$data['page'] = "v_verifabsenmhs";
		$this->load->view('template', $data);
	}

	function loadDetailAbs($id)
	{
		$kdjdl = get_kd_jdl($id);
		$data['kania'] = $this->absen->loadPresence($kdjdl);
		$this->load->view('modal_detail_presence', $data);
	}

	function validasi($trk)
	{
		$this->absen->validAbsen($trk);
		echo "<string>alert('Berhasil!');history.go(-1);</string>";
	}

}

/* End of file Absenmhs.php */
/* Location: .//tmp/fz3temp-1/Absenmhs.php */