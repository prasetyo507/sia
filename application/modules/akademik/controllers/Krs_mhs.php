<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Krs_mhs extends CI_Controller {

	protected $uid;

	function __construct()
	{
		parent::__construct();

		$tglnow = date('Y-m-d');
		
		$this->load->model('setting_model');
		$this->load->model('temph_model');
		$this->load->model('krs_model');

		error_reporting(0);

		if ($this->session->userdata('sess_login') == TRUE) {

			$user_session 	= $this->session->userdata('sess_login'); 
			$this->uid 		= $user_session['userid'];

			$cekakses 	= $this->role_model->cekakses(157)->result();
			$aktif1 	= $this->setting_model->getaktivasi('krs')->result();
			$aktif2 	= $this->setting_model->getaktivasi('krss2')->result();

			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
				exit();
			} else {
				if ((count($aktif1) != 1) or (count($aktif2) != 1)) {
					echo "<script>alert('Akses Tidak Diizinkan !!');document.location.href='".base_url()."home';</script>";
					exit();
				}

				if (substr($this->uid, 0,4) == '2012') {
					echo "<script>alert('Akses Tidak Diizinkan !!');document.location.href='".base_url()."home';</script>";
					exit();
				}
			}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		// setting user session
		$log = $this->session->userdata('sess_login');

		// check whether student has filled out the bio
		$bio = $this->app_model->getdetail('tbl_bio_mhs','npm',$log['userid'],'npm','asc');
		if ($bio->num_rows() == 0) {
			echo "<script>alert('Mohon isi biodata Anda sebelum melakukan pengisian KRS!');history.go(-1);</script>";
			return;
		}

		// check whether student has filled out the e-mail
		if (is_null($bio->row()->email or empty($bio->row()->email)) ) {
			echo "<script>alert('Mohon isi e-Mail anda pada menu biodata guna pengiriman notifikasi KRS!');history.go(-1);</script>";
			return;
		}

		// check whether student has filled out the phone number
		if (is_null($bio->row()->no_hp) || $bio->row()->no_hp == '') {
			echo "<script>alert('Mohon isi nomor handphone anda terlebih dahulu!');history.go(-1);</script>";
			return;
		}

		// is student's KRS exist on this academic year ?
		$cek = $this->krs_model->getactvkrs($log['userid'].getactyear());

		// jika sudah pernah melakukan pengisian KRS
		if ($cek->num_rows() == 1) {
			redirect(base_url('akademik/krs_mhs/save_sesi/'.$log['userid'].'/'.$cek->row()->kd_krs),'refresh');
		}

		// jika belum mengisi KRS
		$this->_prefillingForm();
	}

	/**
	 * formulir pra pengisian KRS, memilih kelas dan dosen pembimbing
	 * @return void
	 */
	protected function _prefillingForm()
	{
		$data['mhs'] = $this->db->where('npm_mahasiswa',$this->uid)->get('tbl_pa',1)->row();
		$jam = date("H:i");

		/**
		 * cek waktu pengisian KRS
		 * jika diluar waktu yang ditentukan maka firut pengisian KRS tidak dapat diakses */
		if ($jam > '00:01' and $jam < '23:59') {
			$studentDepartment = get_mhs_jur($this->uid);

			// cek apakah jadwal untuk prodi terkait sudah aktif
			$isScheduleActive = $this->app_model->getdetail('tbl_jadwal_krs','kd_prodi',$studentDepartment,'kd_prodi','asc')->row();

			// 1 = jadwal pengisian aktif
			if ($isScheduleActive->status == 1) {
				$data['page'] = 'akademik/krs_mhs';
				$this->load->view('template', $data);
			} else {
				echo "<script>alert('Akses Tidak Diizinkan!');document.location.href='".base_url('home')."';</script>";
				return;
			}

		} else {
			echo "<script>alert('Akses pengisian KRS hanya diizinkan mulai pukul 00:01 s/d 23:59');document.location.href='".base_url()."home';</script>";
		}
	}

	function create_session()
	{
		// cek apakah KRS mahasiswa teah disetujui 
		$ceks 	= $this->krs_model->statVerify($this->uid.getactyear(),1)->result();

		// jika KRS telah disetujui
		if (count($ceks) > 0) {
			echo "<script>alert('KRS Anda Telah Disetujui Oleh Dosen Pembimbing!!');history.go(-1);</script>";
			return;

		// jika KRS belum disetujui
		} else {
			$logs = $this->session->userdata('sess_login');

			// cek pembayaran pada tahun ajaran aktif
			// $cekrenkeu 	= $this->krs_model->krsPayment($uid, getactyear(), 1)->result();

			// cek status verifikasi KRS pada tahun ajaran sebelumnya
			// $krsan = $this->app_model->getdetail('tbl_verifikasi_krs','kd_krs',$uid.yearBefore(),'kd_krs','asc')->row()->status_verifikasi;

			// jika pembayaran dan status verifikasi valid
			// if ((count($cekrenkeu) > 0) and ($krsan != 10)) {

				$this->session->set_userdata('kd_dosen', $this->input->post('kd_dosen'), TRUE);
				$this->session->set_userdata('kelas', $this->input->post('kelas'), TRUE);

				// cek apakah pa sesuai ?
				$akt = substr($logs['userid'], 0,4);
				if ($akt != date('Y')) {
					$cekpa = $this->temph_model->match_pa($logs['userid'])->row()->kd_dosen;
					if ($this->input->post('kd_dosen', TRUE) != $cekpa) {
						echo "<script>alert('Dosen Pembimbing Akademik anda tidak sesuai!');history.go(-1);</script>";
					}
				}
				
				redirect(base_url().'akademik/krs_mhs/pengisian_krs','refresh');
				
			// } else {
			// 	echo "<script>alert('Mohon Penuhi Biaya Administrasi!');history.go(-1);</script>";
			// }
		}
	}

	function printkrs($kodebaru)
	{
		$a 	=substr($kodebaru, 16,1);
		$npm=substr($kodebaru, 0,12);
		$ta =substr($kodebaru, 12,5);

	    if ($a == 1) {
	      $b = 'Ganjil';
	    } else {
	      $b = 'Genap';
	    }

	    $cetak['kdver'] = $this->db->select('kd_krs_verifikasi')
	    							->from('tbl_verifikasi_krs')
	    							->where('npm_mahasiswa', $npm)
	    							->where('tahunajaran', $ta)
	    							->order_by('kd_krs_verifikasi','desc')
	    							->limit(1)
	    							->get()->row();

		$cetak['footer'] = $this->db->select('npm_mahasiswa,id_pembimbing')
									->from('tbl_verifikasi_krs')
									->like('npm_mahasiswa', $npm,'both')
									->get()->row();

		$cetak['kd_krs'] = $kodebaru;
		
	    $cetak['gg']  = $b;
	    $cetak['npm'] = $npm;
	    $cetak['ta']  = $ta;

		$this->load->view('welcome/print/krs_pdf',$cetak);
	}

	function load_dosen_autocomplete()
	{

        $this->db->distinct();

        $this->db->select("a.id_kary,a.nid,a.nama");

        $this->db->from('tbl_karyawan a');

        $this->db->like('a.nama', $_GET['term'], 'both');

        $this->db->or_like('a.nid', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

	            'id_kary'       => $row->id_kary,

	            'nid'           => $row->nid,

	            'value'         => $row->nid.' - '.$row->nama

            );

        }

        echo json_encode($data);

    }

    function revisi_krs($id)
    {
    	$tglnow 	= date('Y-m-d');
    	$kd_dosen 	= $this->db->where('kd_krs',$id)->get('tbl_verifikasi_krs')->row();
    	
    	$this->session->set_userdata('kd_krs',$id);
    	$this->session->set_userdata('kd_dosen', $kd_dosen->id_pembimbing);
    	$this->session->set_userdata('kd_kelas', $kd_dosen->kelas);
    	
    	$data['mhs'] = $this->krs_model->getPa($log['userid'])->row();

		$jam = date("H:i");

		if ($jam > '00:01' and $jam < '23:59') {
			$npm = $this->session->userdata('sess_login');
			$log = get_mhs_jur($npm['userid']);
			$act = $this->app_model->getdetail('tbl_jadwal_krs','kd_prodi',$log,'kd_prodi','asc')->row()->status;

			if ($act == 1) {
				redirect(base_url().'akademik/krs_mhs/pengisian_krs');
			} else {
				echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
			}

		} else {
			echo "<script>alert('Akses pengisian KRS hanya diizinkan mulai pukul 00:01 s/d 23:59');document.location.href='".base_url()."home';</script>";
		}
    }

	function pengisian_krs()
	{
		$user = $this->session->userdata('sess_login');
		$nim  = $user['userid'];

		// ganjil genap
		$gg   = substr(getactyear(), -1);

		// jika tidak memiliki sesi
		if (!$nim) {
			echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			return;
		}

		// cek program perkuliahan S1/S2
		$strata = substr($nim, 4,1);
		if ($strata == 1) {
			$this->_forBachelor($gg, $nim);

		} else {
			$this->_forPostgraduate($nim);
		}
	}

	/**
     * untuk pengisian sarjana S1
     * @param string $gg [ganjil genap]
     * @param string $logmhs [npm]
     * @return void
     */
    function _forBachelor($gg, $logmhs)
    {
    	// pengecekan apakah mahasiswa sudah melakukan pengisian
  		$loadkrs = $this->krs_model->getactvkrs($logmhs.getactyear())->result();

  		// jika sudah ada KRS
  		if (count($loadkrs) > 0) {
  			$this->_loadKrs($gg, $logmhs);

  		// jika belum ada KRS
  		} else {
  			$this->_loadNewForm($gg, $logmhs);
  		}
    }

    function _forPostgraduate($logmhs)
    {
    	$data['ta'] = getactyear();
  		$tahunakademik = getactyear();
  		//$krsan = $this->app_model->getkrsanmhs($logmhs,$tahunakademik->kode)->row();
  		//$krsan = $this->app_model->getkrsanmhs($logmhs,'20162')->row();
  		$krsan = TRUE;

  		if ($krsan == TRUE) {
    	  	$mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $logmhs, 'KDPSTMSMHS', 'ASC')->row_array();
    	  
    	  	// hitung IPS
            $ips_nr = $this->krs_model->countips($mhs['KDPSTMSMHS'],$logmhs,yearBefore());
		  	$data['ipsmhs_before'] 	= $ips_nr;
		  	$data['seekhs'] 		= $nim.yearBefore();
	      	$data['npm'] 			= $logmhs;
		  	$data['prodi'] 			= $mhs['KDPSTMSMHS'];
		  	$data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];

		  	if ($mhs['SMAWLMSMHS']%2 == 0) {
		  		$getsemester = $this->app_model->get_smt_awal_genap($mhs['SMAWLMSMHS']);
		  	} else {
		  		$getsemester = $this->app_model->get_semester($mhs['SMAWLMSMHS']);
		  	}
		  	
		  	$data['semester'] 		= $getsemester;
		  	$data['data_krs'] 		= $this->app_model->get_all_khs_mahasiswa($logmhs)->result();
		  	$data_krs 				= $this->app_model->get_krs_mahasiswa($data['npm'], $data['semester']);

		  	if($data_krs->num_rows() > 0){
				$data['data_matakuliah'] 	= $data_krs->result();
				$data['kode_krs'] 			= $data_krs->row()->kd_krs;
				$data['catatan_pembimbing'] = $this->app_model->get_pembimbing_krs($data['kode_krs'])->row()->keterangan_krs;
				$data['status_krs'] 		= 1;
			} else {
				$data['data_matakuliah'] 	= $this->app_model->get_matkul_krs($data['semester'], $data['prodi'], $data['npm'])->result();
				$data['status_krs'] 		= 0;
		  	}

		  	$data['dospem'] = $this->session->userdata('kd_dosen');
		  	$data['page'] = 'akademik/krs_view';
	      	$this->load->view('template', $data);

	    } else {
  			echo "<script>alert('Mohon Penuhi Biaya Administrasi');document.location.href='".base_url()."akademik/krs_mhs ';</script>";
  		}
	}

	/**
	 * memuat KRS untuk mahasiswa yang sudah mengisi KRS
	 * @param string $gg [ganjil genap]
     * @param string $logmhs [npm]
     * @return void
	 */
    function _loadKrs($gg, $logmhs)
    {
    	$data['ta'] = getactyear();
  		$krsan 		= TRUE;
  		
  		if ($krsan == TRUE) {

  			// load data mahasiswa
		  	$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$logmhs,'KDPSTMSMHS','ASC')->row_array();
		  	
		  	// count IPK mahasiswa
		  	$getipk = $this->temph_model->get_ipk($mhs['NIMHSMSMHS'])->result();
			$nulsks = 0;
			$const 	= 0;
			foreach ($getipk as $row) {
				$countloop 	= ($row->sks_matakuliah * $row->BOBOTTRLNM);
				$nulsks		= $nulsks + $row->sks_matakuliah;
				$const 	  	= $const + $countloop;
			}
			$data['ipk'] = number_format(($const / $nulsks),2);

            // penghitungan IPS mahasiswa
            $ips_nr = $this->krs_model->countips($mhs['KDPSTMSMHS'],$logmhs,yearBefore());

		  	$data['ipsmhs_before'] 	= $ips_nr;
		  	$data['seekhs'] 		= $logmhs.yearBefore();
	      	$data['smtmhs'] 		= $mhs['SMAWLMSMHS'];
	      	$data['npm'] 			= $logmhs;
		  	$data['prodi'] 			= $mhs['KDPSTMSMHS'];
		  	$data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
		  	$data['semester'] 		= $this->app_model->get_semester($mhs['SMAWLMSMHS']);
		  	$data['data_krs'] 		= $this->app_model->get_all_khs_mahasiswa($logmhs)->result();

		  	# gunakan tes jika tes
		  	$data_krs = $this->app_model->get_krs_mahasiswa($data['npm'], $data['semester']);

		  	$prekodekrs = $logmhs.getactyear();

		  	if($data_krs->num_rows() > 0) {

		  		$kodekrs = $this->db->like('kd_krs', $prekodekrs, 'AFTER')->get('tbl_verifikasi_krs')->row()->kd_krs;

		  		// $data['kode_krs'] = $data_krs->row()->kd_krs;
		  		$data['kode_krs'] = $kodekrs;
				$data['data_matakuliah'] = $data_krs->result();
				$data['status_krs'] = 1;

			} else {
				$data['data_matakuliah'] = $this->app_model->get_matkul_krs($data['semester'], $data['prodi'], $data['npm'])->result();
				$data['data_ngulang'] = $this->app_model->get_matkul_ngulang($gg, $data['npm'])->result();
				$data['status_krs'] = 0;

			}

			

			$data['dospem'] = $this->session->userdata('kd_dosen');
			$data['page'] = 'akademik/krs_view';
		    $this->load->view('template', $data);

  		} else {
  			echo "<script>alert('Mohon Penuhi Biaya Administrasi');document.location.href='".base_url()."akademik/krs_mhs';</script>";
  		}
    }

    /**
     * memuat halaman baru untuk pengisian KRS
     * @param string $gg [ganjil genap]
     * @param string $logmhs [npm]
     * @return void
     */
    function _loadNewForm($gg, $logmhs)
    {
    	$data['ta'] = getactyear();
  		$krsan = TRUE;
  		
  		if ($krsan == TRUE) {

		  	$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$logmhs,'KDPSTMSMHS','ASC')->row_array();

		  	// count IPK mahasiswa
		  	$getipk = $this->temph_model->get_ipk($mhs['NIMHSMSMHS'])->result();
			$nulsks = 0;
			$const 	= 0;
			foreach ($getipk as $row) {
				$countloop 	= ($row->sks_matakuliah * $row->BOBOTTRLNM);
				$nulsks		= $nulsks + $row->sks_matakuliah;
				$const 	  	= $const + $countloop;
			}
			$data['ipk'] = number_format(($const / $nulsks),2);
			  	
			// hitung IPS 
            $ips_nr = $this->krs_model->countips($mhs['KDPSTMSMHS'],$logmhs,yearBefore());
		  	$data['ipsmhs_before'] 	= $ips_nr;
		  	$data['seekhs'] 		= $logmhs.yearBefore();
	      	$data['smtmhs'] 		= $mhs['SMAWLMSMHS'];
	      	$data['npm'] 			= $logmhs;
		  	$data['prodi'] 			= $mhs['KDPSTMSMHS'];
		  	$data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
		  	$data['semester'] 		= $this->app_model->get_semester($mhs['SMAWLMSMHS']);
		  	$data['data_krs'] 		= $this->app_model->get_all_khs_mahasiswa($logmhs)->result();
		  	$data_krs = $this->app_model->get_krs_mahasiswa($data['npm'], $data['semester']);  // gunakan tes jika tes

		  	if($data_krs->num_rows() > 0){
		  		$data['kode_krs'] = $data_krs->row()->kd_krs;
				$data['catatan_pembimbing'] = $this->app_model->get_pembimbing_krs($data['kode_krs'])->row()->keterangan_krs;
				$data['status_krs'] = 1;

			}else{
				
				$data['data_matakuliah'] = $this->app_model->get_matkul_krs($data['semester'],$data['prodi'],$data['npm'])->result();
				$data['status_krs'] = 0;
				$data['data_ngulang'] = $this->app_model->get_matkul_ngulang($gg, $data['npm'])->result();

			}

			$data['dospem'] = $this->session->userdata('kd_dosen');
			$data['page'] = 'akademik/krs_view';
		    $this->load->view('template', $data);

  		} else {
  			echo "<script>alert('Mohon Penuhi Biaya Administrasi');document.location.href='".base_url()."akademik/krs_mhs';</script>";
  		}
    }

	function cekjumlahsks()
	{
		$loggedmhs = $this->session->userdata('sess_login');
		$npm = $loggedmhs['userid'];
		$mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $loggedmhs['userid'], 'KDPSTMSMHS', 'ASC')->row_array();
		$data['prodi'] = $mhs['KDPSTMSMHS'];

        $ips_nr = $this->krs_model->countips($data['prodi'],$npm,yearBefore());
        $ips = number_format($ips_nr,2);
	    $semester = $this->app_model->get_semester($mhs['SMAWLMSMHS']);
		$sks = $_POST['sks'];
		// var_dump($sks);
		if ($sks > 24) {
			echo 0;
		} else {
			$b = $this->app_model->ketentuan_sks($ips,$sks);
			if ($semester == 1) {
				echo 1;
			} else {
				// var_dump($sks);
				echo $b;
			}
		}
	}

	function get_matkul()
	{
		$semester = $_POST['semester'];
		
		$npm = $_POST['npm'];
		$prodi = $_POST['prodi'];
		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row()->kode;
		
		if(($tahunakademik % 2) == 1 and $semester == 8){ 
			$data = $this->app_model->get_matkul_krs_spesial($semester, $prodi, $npm,1)->result();
		} elseif(($tahunakademik % 2) == 1 and $semester == 6) {
			$data = $this->app_model->get_matkul_krs_spesial($semester, $prodi, $npm,2)->result();
		} elseif(($tahunakademik % 2) == 0 and $semester == 7) {
			$data = $this->app_model->get_matkul_krs_spesial($semester, $prodi, $npm,2)->result();
		} else {
			$data = $this->app_model->get_matkul_krs_spesial_not_krs($semester, $prodi, $npm)->result();
		}
	                
	    $js = json_encode($data);
	    echo $js;
	 }

	function cek_prasyarat()
	{
		$log = $this->session->userdata('sess_login');
		$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$log['userid'],'NIMHSMSMHS','asc')->row();
		$sym  = array('[', ']');
		$prasyarat = array();
		$hit = array();
		$prasyarat = explode(',', str_replace($sym,'',$_POST['prasyarat']));
		
		for ($i=0; $i < count($prasyarat); $i++) {
			$c++;
			$kv = $this->db->where('kd_baru',$prasyarat[$i])->where('flag',1)->where('kd_prodi',$mhs->KDPSTMSMHS)->get('tbl_konversi_matkul_temp')->row();
				if (count($kv) > 0) {
					$hit[] = $kv->kd_lama;
					
					$kv2 = $this->db->where('kd_baru',$kv->kd_lama)->where('flag',1)->where('kd_prodi',$mhs->KDPSTMSMHS)->get('tbl_konversi_matkul_temp')->row();
						if (count($kv2) > 0) {
							$hit[] = $kv2->kd_lama;

							$kv3 = $this->db->where('kd_baru',$kv2->kd_lama)->where('flag',1)->where('kd_prodi',$mhs->KDPSTMSMHS)->get('tbl_konversi_matkul_temp')->row();
							if (count($kv3) > 0) {
								$hit[] = $kv3->kd_lama;
							}
						}
				}
		}

		// var_dump($prasyarat);

		$gabung = array_merge($prasyarat, $hit);
		// $gabung = array();
		if (substr($log['userid'], 8,1) == '7') {
			$data2 = $this->temph_model->cek_prasyarat_konversi($gabung,$log['userid'])->num_rows();
			$data3 = $this->temph_model->cek_prasyarat($gabung,$log['userid'])->num_rows();
			$data = $data2+$data3;
		} else {
	        $data = $this->temph_model->cek_prasyarat($gabung,$log['userid'])->num_rows();
		}
		//$data = $this->temph_model->cek_prasyarat($gabung,$log['userid'])->num_rows();
		
		// if (count($gabung) != $data) {
		// 	echo 0;
		// } else {
			echo $data;
		// }
		
		// echo "1";
	}

	function get_krs()
	{
		$semester 	= $_POST['semester'];
		$npm 		= $_POST['npm'];
		
	    $data = $this->app_model->get_matkul_krs_rekam($semester, $npm)->result();
	                
	    $js = json_encode($data);
	    echo $js;
	}

	function saving()
	{
		$user = $this->session->userdata('sess_login');
		$nim  = $user['userid'];

		$kode_krs 		= $this->input->post('kode_krs', TRUE);	
		$kd_matakuliah 	= $this->input->post('kd_matkuliah', TRUE);
		$sumsks 		= $this->input->post('jumlah_sks', TRUE);

		if (($sumsks > 24) or ($sumsks < 1)) {
			echo "<script>alert('Jumlah SKS Tidak Diizinkan');
			document.location.href='".base_url('akademik/krs_mhs/pengisian_krs')."';
			</script>";
			return;
		}

		if (count($kd_matakuliah) == 0) {
			echo "<script>alert('Pilih Matakuliah Terlebih Dahulu!');
			document.location.href='".base_url('akademik/krs_mhs/pengisian_krs')."';
			</script>";
			return;
		}

		$actyear = getactyear();

		$isKrsExist = $this->db->like('kd_krs', $nim.$actyear, 'AFTER')->get('tbl_verifikasi_krs');

		//sudah ada krs
		if($isKrsExist->num_rows() > 0) {
		// if (!empty($kode_krs)) {
			$this->_hasFillKrsForm($kd_matakuliah, $isKrsExist->row()->kd_krs, $nim, $sumsks, getactyear());

		// belum ada ada KRS
		} else {
			$this->_firstKrsFilling($kd_matakuliah, $nim, $sumsks);
		}
	}

	/**
	 * untuk handle pengisian KRS bagi mahasiswa yang telah mengisi form KRS sebelumnya
	 * @param array $kodemk
	 * @param string $kodekrs
	 * @param string $nim
	 * @param string $tahunajaran
	 * @return void
	 */
	protected function _hasFillKrsForm($kodemk, $kodekrs, $nim, $sumsks, $tahunajaran)
	{
		$kode_krs = $kodekrs;
		$kd_matakuliah = $kodemk;

		/*=========================================================
		=            Adjustment penyimpanan matakuliah            =
		=========================================================*/
		/**
		 * Ketika update matakuliah, agar tidak menghapus jadwal yang telah dipilih
		 * pada matakuliah sebelumnya, maka dilakukan proses pengecekan.
		 * Bagi matakuliah yang sebelumnya telah diisikan jadwal tidak akan terhapus.
		 */

		/*----------  ambil data KRS dan kolektifkan dalam array  ----------*/
		$getkrsbefore2 = $this->app_model->getkrsbefore2($kode_krs)->result();
		foreach ($getkrsbefore2 as $vals) {
			$bulk[] = $vals->kd_matakuliah;
		}

		/*
		* bandingkan antara KRS exist dengan KRS baru
		* jika pada KRS baru tidak terdapat salah satu data pada KRS exist
		* maka hapus data yang tidak ada tersebut dari KRS exist
		*/
		foreach ($bulk as $keys) {
			if (!in_array($keys, $kd_matakuliah)) {
				$this->db->where('kd_krs', $kode_krs);
				$this->db->where('kd_matakuliah', $keys);
				$this->db->delete('tbl_krs');
			}
		}

		/*
		* bandingkan antara KRS baru dengan KRS exist
		* jika terdapat data baru yang belum ada pada KRS exist
		* maka tambahkan data tersebut kedalam KRS exist
		*/
		foreach ($kd_matakuliah as $code) {
			if (!in_array($code, $bulk)) {
				$obj['npm_mahasiswa'] = $nim;
				$obj['kd_matakuliah'] = $code;
				$obj['kd_krs'] = $kode_krs;
				$obj['semester_krs'] = $this->input->post('semester', TRUE);
				$this->db->insert('tbl_krs', $obj);
			}
		}
				
		/*=====  End of Adjustment penyimpanan matakuliah  ======*/

		/*----------  terkadang field dosen kosong, maka dibuat fungsi untuk menanganinya  ----------*/
		// if ($this->input->post('kodos') == '' or is_null($this->input->post('kodos'))) {
		// 	$cekdosen = $this->db->query("SELECT kd_dosen from tbl_pa where npm_mahasiswa = '".$nim."'")->row()->kd_dosen;
		// 	$kodedosen = $cekdosen;
		// } else {
		// 	$kodedosen = $this->input->post('kodos');
		// }
		
		// insert to tbl_verifikasi_krs
		$this->_insertToVerifikasiKrs($kode_krs, $nim, $sumsks, $tahunajaran, TRUE);

		// redirect to schedule filling
		redirect(base_url('akademik/krs_mhs/save_sesi/'.$nim.'/'.$kode_krs));
	}

	/**
	 * untuk handle pengisian KRS baru
	 * @param string $kd_matakuliah
	 * @param strng $nim
	 * @param int $sumsks
	 * @return void
	 */
	protected function _firstKrsFilling($kd_matakuliah, $nim, $sumsks)
	{
		$tahunajaran = getactyear();

		$kodeawal 	= $nim.$tahunajaran;

		$randomSufix = rand(1,9);
		$kode_krs 	 = $nim.$tahunajaran.$randomSufix;
		
		foreach ($kd_matakuliah as $key => $value) {
			$krs[] = [
				'npm_mahasiswa' => $nim,
				'semester_krs' 	=> $this->input->post('semester', TRUE),
				'kd_matakuliah' => $value,
				'kd_krs' 		=> $kode_krs
			];
		}
		
		// remove duplicate data from multidimensional array
		$removeDuplicate = array_map("unserialize", array_unique(array_map("serialize", $krs)));
		$datakrs = $removeDuplicate;
		$this->db->insert_batch('tbl_krs', $datakrs);
		
		// insert krs transaction to tbl_verifikasi_krs
		$this->_insertToVerifikasiKrs($kode_krs, $nim, $sumsks, $tahunajaran);

		// redirect to schedule filling
		redirect(base_url('akademik/krs_mhs/save_sesi/'.$nim.'/'.$kode_krs));
	}

	/**
	 * handle proses untuk insert ke tbl_verifikasi_krs
	 * @param string $kodeKrs
	 * @param string $nim
	 * @param string $tahunajaran
	 * @param bool $condition
	 * @param string $kelas
	 * @return void
	 */
	protected function _insertToVerifikasiKrs($kodeKrs, $nim, $sumsks, $tahunajaran, $condition = FALSE)
	{
		/*----------  terkadang field kelas kosong, maka dibuat fungsi untuk menanganinya  ----------*/
		$sessionClass = $this->session->userdata('kelas');

		if (is_null($sessionClass) or empty($sessionClass) or !$sessionClass) {
			$cls 	= $this->db->query("SELECT kelas from tbl_verifikasi_krs where kd_krs like '".$nim.$tahunajaran."%'")->row()->kelas;
			$kelas 	= $cls;
		} else {
			$kelas 	= $sessionClass;
		}

		$cekdosen = $this->db->query("SELECT kd_dosen from tbl_pa where npm_mahasiswa = '".$nim."'")->row()->kd_dosen;
		$kodedosen = $cekdosen;

		$vkrs['id_pembimbing'] 	= $kodedosen;
		$vkrs['kd_krs'] 		= $kodeKrs;
		$vkrs['npm_mahasiswa'] 	= $nim;
		$vkrs['kd_jurusan'] 	= get_mhs_jur($nim);
		$vkrs['tgl_bimbingan'] 	= date('Y-m-d H:i:s');
		$vkrs['key'] 			= $this->generateRandomString();
		$vkrs['tahunajaran'] 	= $tahunajaran;
		$vkrs['jumlah_sks'] 	= $sumsks;
		
		$kodver 				= $kode_krs.$sumsks.$vkrs['key'];
		$vkrs['kd_krs_verifikasi'] = md5(md5($kodver).key_verifikasi);

		$nama 				= get_nm_mhs($nim);
		$slug 				= url_title($nama, '_', TRUE);
		$vkrs['slug_url'] 	= $slug;
		$vkrs['kelas'] 		= $kelas;
		$vkrs 				= $this->security->xss_clean($vkrs);

		// jika insert setelah proses update
		if ($condition) {
			$this->app_model->deletedata('tbl_verifikasi_krs','kd_krs',$kodeKrs);
		}

		$this->app_model->insertdata('tbl_verifikasi_krs', $vkrs);

		// generate QR Code
		$this->_QRCodeGenerator($vkrs['kd_krs_verifikasi']);
	}

	protected function _QRCodeGenerator($kodeVerifikasiKrs)
	{
		$this->load->library('ciqrcode');
		$params['data'] = 'http://sia.ubharajaya.ac.id/welcome/welcome/cekkodeverifikasi/'.$kodeVerifikasiKrs.'';
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'QRImage/'.$kodeVerifikasiKrs.'.png';
		$this->ciqrcode->generate($params);
	}

	function save_sesi($npm,$kdkrs)
	{
		$array = array(
			'npm' 	=> $npm,
			'kd_krs'	=> $kdkrs
		);
		
		$this->session->set_userdata('sess_for_jdl', $array);
		redirect(base_url('akademik/krs_mhs/jadwal_kuliah'),'refresh');
	}

  	function jadwal_kuliah()
  	{
  		$log_jdl = $this->session->userdata('sess_for_jdl');
  		$user = $this->session->userdata('sess_login');

  		if ($this->session->userdata('sessdirectlogin')) {
  			$logs  = $this->session->userdata('sessdirectlogin');
  			$kdkrs = $logs['kdkrs']; 
  		} else {
  			$kdkrs = $log_jdl['kd_krs'];
  		}


  		// cek apakah krs paket atau bukan
  		// $akt = substr($user['userid'], 0,4);
  		// if ($akt == date('Y')) {
  		// 	echo "<script>alert('Akses pada menu ini ditolak untuk Anda!');history.go(-1);</script>"; exit();
  		// }  		

		$nim  = $user['userid'];
  		// if ($nim == '201610225129') {
  		// 	echo substr($kdkrs, 0, 12).'-------------'.$nim;
  		// }
  		if (substr($kdkrs, 0, 12) != $nim) {
			$this->session->unset_userdata('sess_login');
			echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."auth';</script>";
	        //redirect('/auth', 'refresh');

  		} else {
  			// die($this->session->userdata('kelas'));
  			$cekdata = $this->db->query("SELECT * from tbl_krs where kd_krs = '".$kdkrs."'")->result();
  			if (count($cekdata) == 0) {
  				echo "<script>alert('Data Tidak Ditemukan !');document.location.href='".base_url('akademik/krs_mhs')."';</script>";
  			} else {
  				$data['npm_mahasiswa'] 	= substr($kdkrs, 0, 12);
				$data['kd_krs']			= $kdkrs;

				$data['pembimbing'] = $this->app_model->get_pembimbing_krs($kdkrs)->row_array();
				$tahunakademik 		= getactyear();
				$data['ta']			= $tahunakademik;
				$data['detail_krs'] = $this->app_model->get_detail_print_krs_mahasiswa($kdkrs)->result();
				
				/* jika fitur catatan pembimbing dibutuhkan
				$data['noteformhs'] = $this->db->query("SELECT note from tbl_bimbingan_krs 
														where kd_krs like '".$nim.$tahunakademik->kode."%' 
														order by id desc limit 1")->row_array();
				*/

				// buat komparasi jumlah matakuliah yang sudah dijadwalkan
				$data['comparemk'] 	= $this->db->query("SELECT count(npm_mahasiswa) as mk from tbl_krs 
														where kd_krs like '".$nim.$tahunakademik."%'")->row()->mk;

				$data['comparejd'] 	= $this->db->query("SELECT count(npm_mahasiswa) as jd from tbl_krs 
														where kd_krs like '".$nim.$tahunakademik."%' 
														and (kd_jadwal != NULL or kd_jadwal != '') ")->row()->jd;

				$data['page'] = 'akademik/krs_mhs_jdl';
				$this->load->view('template',$data);
  			}   
  		}
	}

	function ajukan_krs($kd_krs)
	{
		$emaildosen = $this->krs_model->emaildosen($kd_krs)->result();

		if (count($emaildosen) == 0) {
			echo "<script>alert('E-Mail dosen pembimbing anda belum terdaftar! Mohon hubungi dosen anda untuk melanjutkan pengajuan KRS!');document.location.href='".base_url()."akademik/krs_mhs/jadwal_kuliah';</script>";
		} else {

			$npm_mahasiswa = substr($kd_krs, 0,12);

			$object = array('status_verifikasi' => 4);
			$this->db->where('kd_krs', $kd_krs);
			$this->db->update('tbl_verifikasi_krs', $object);

			echo "<script>alert('Berhasil diajukan!')</script>";

			redirect(base_url('akademik/sendmessage/sendSms/'.$kd_krs));
			// redirect(base_url('akademik/krs_mhs/jadwal_kuliah'),'refresh');
		}
	}

	function generateRandomString() 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function get_jadwal()
	{
		$tahunakademik = getactyear();
		$kd_matakuliah = $_POST['kd_matakuliah'];

		$user = $this->session->userdata('sess_login');
		$nim  = $user['userid'];

		$cls = $this->krs_model->getactvkrs($nim.$tahunakademik)->row();

		if ($this->session->userdata('kelas') != TRUE) {
			$kelas = $cls->kelas;
		} else {
			$kelas = $this->session->userdata('kelas');
		}

		$mhs  = $this->app_model->get_jurusan_mhs($nim)->row();
		// echo $kelas;
		$data = $this->app_model->get_pilih_jadwal_krs($kd_matakuliah,$mhs->KDPSTMSMHS,$tahunakademik,$kelas)->result();
                
		$js = json_encode($data);
		echo $js;
	}

	function save_jadwal()
	{
		$data['kd_jadwal'] = $this->input->post('kd_jadwal');
		$kd_matakuliah = $this->input->post('kd_matakuliah');
		$kd_krs= $this->input->post('kd_krs');

		$npm_mahasiswa = substr($kd_krs, 0,12);
		//die($npm_mahasiswa);
		
		$this->app_model->updatedata_krs($kd_matakuliah, $kd_krs, $data);
		//redirect('form/formkrs/jadwal_kuliah','refresh');

		$this->jadwal_kuliah($npm_mahasiswa,$kd_krs);

	}

	function kosongkan_jadwal($id,$id2)
	{

		$data = array('kd_jadwal' => null );
		$npm_mahasiswa = substr($id, 0,12);

		$this->db->where('kd_krs', $id);
		$this->db->where('kd_matakuliah', $id2);
		$this->db->update('tbl_krs', $data);

		$q = $this->db->select('kd_krs')
						->from('tbl_krs')
						->where('id_krs',$id)
						->get()->row();

		// $this->jadwal_kuliah($npm_mahasiswa,$id);

		redirect(base_url().'akademik/krs_mhs/jadwal_kuliah/'.$npm_mahasiswa.'/'.$id,'refresh');

	}

	function verify()
	{
	    $id 	= $this->input->post('username', TRUE);
	    $cek 	= $this->input->post('g-recaptcha', TRUE);

	    if ($_POST['g-recaptcha-response'] != '') {
	      $secret = '6LeLLAoUAAAAAHQPTHRFKtaMGVexvRQM1_5wD4c4';
	      //get verify response data
	      $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
	      $responseData = json_decode($verifyResponse);
	      if ($responseData->success) {
	        echo 'Your contact request have submitted successfully.';;
	      } else {
	        echo "salah ces";
	      }
	    } else {
	      echo "salah ces";
	    }
	    //var_dump($responseData);
	    exit();
	}

	function view($npm, $id)
	{
		
		$data['npm'] = $npm;

		$data['ips'] = $this->app_model->get_ips_mahasiswa($npm, $id)->row()->ips;

		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'NIMHSMSMHS','asc')->row();
		
		$data['semester'] = $id;
		$data['kode'] = $this->db->query("SELECT distinct kd_krs from tbl_krs where npm_mahasiswa = '".$npm."' 
											and semester_krs = ".$id." ")->row();
		//$data['detail_khs'] = $this->app_model->get_detail_khs_mahasiswa($npm, $id)->result();
		$data['detail_khs'] = $this->krs_model->detailkhs($npm, $id, $data['mhs']->KDPSTMSMHS)->result();
		$data['page'] = 'akademik/khs_detail2';
		$this->load->view('template',$data);	
	}

	function load_khs($kd)
	{
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',substr($kd, 0,12),'NIMHSMSMHS','asc')->row();
		$data['q'] = $this->db->query("SELECT distinct a.*,b.nama_matakuliah,b.sks_matakuliah from tbl_krs a 
										join tbl_matakuliah b on a.kd_matakuliah = b.kd_matakuliah 
										where a.kd_krs like '".$kd."%' and b.kd_prodi = '".$data['mhs']->KDPSTMSMHS."'")->result();
		$sks = 0;
		foreach ($data['q'] as $key) {
			$sks = $sks + $key->sks_matakuliah;
		}
		$data['jumsks'] = $sks;
		// $data['tahunakad'] = substr($kd, 12,17);

		$thn = substr($kd, 12,5);
		$data['tahunakad'] = $thn;
		
		/*
		$hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
                JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
                WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$data['mhs']->KDPSTMSMHS.'" AND NIMHSTRLNM = "'.substr($kd, 0,12).'" and THSMSTRLNM = "'.$thn.'" ')->result();

        $st=0;
        $ht=0;
        foreach ($hitung_ips as $iso) {
            $h = 0;

            $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
            $ht=  $ht + $h;

            $st=$st+$iso->sks_matakuliah;
        }

        $ips_nr = $ht/$st;
        */

        $ips_nr = $this->krs_model->countips($data['mhs']->KDPSTMSMHS,substr($kd, 0,12),$thn);
		$data['ipsmhs_before'] = $ips_nr;

		//$data['ipsmhs_before'] = $this->db->query("SELECT ips from tbl_ips where kd_krs like '".$kd."%'")->row()->ips;
		$data['smt'] = $this->db->query("SELECT semester_krs from tbl_krs where kd_krs like '".$kd."%'")->row()->semester_krs;
		// var_dump($tahunakad);exit();
		$this->load->view('detil_khs_modal', $data);
	}

}

/* End of file bimbingan.php */
/* Location: ./application/modules/akademik/controllers/bimbingan.php */