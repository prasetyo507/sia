<?php //var_dump($matakuliah);die(); ?>

<style type="text/css">
#circle { width: 20px; height: 20px; background: #FFB200; -moz-border-radius: 50px; -webkit-border-radius: 50px; border-radius: 50px; }
</style>

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.css">
<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.js"></script>
<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">

<script>
	function edit(idk){
		$('#edit1').load('<?php echo base_url();?>akademik/jadwal_sp/edit_jadwal/'+idk);
	}

	function edit2(idk){
		$('#edit2').load('<?php echo base_url();?>akademik/jadwal_sp/penugasan/'+idk);
	}
</script>



<script type="text/javascript">

$(document).ready(function(){
	$('#jam_masuk').timepicker();
});
</script>



<script type="text/javascript">

jQuery(document).ready(function($) {

	$('input[name^=assign_dsn]').autocomplete({

        source: '<?php echo base_url('akademik/jadwal_sp/getdosen');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.nid_dsn.value = ui.item.nid;

            this.form.assign_dsn.value = ui.item.value;

        }

    });

});

</script>

<div class="row">

	<div class="span12">      		  		

  		<div class="widget ">

  			<div class="widget-header">

  				<i class="icon-list"></i>

  				<h3>Data Jadwal Perkuliahan Semester Perbaikan</h3>

			</div> <!-- /widget-header -->

			

			<div class="widget-content" style="padding:30px;">
				<center>
                <h4><?php echo $this->session->userdata('nama_jurusan_prasyarat').' - '.$this->session->userdata('tahunajaran'); ?> </i></u></h4>
              </center>
              <hr>
            <div class="tabbable">
            <ul class="nav nav-tabs">
            	<?php
            	$no=1; foreach ($tabs as $isi) {
            		
            		if ($no==1) { 
            			echo '<li class="active"><a href="#tab'.$isi->semester_matakuliah.'" data-toggle="tab">Semester '.$isi->semester_matakuliah.'</a></li>';
            		}else {
            			echo '<li><a href="#tab'.$isi->semester_matakuliah.'" data-toggle="tab">Semester '.$isi->semester_matakuliah.'</a></li>';
            		}
            	$no++; } ?>
            </ul>           
            <br>

                        
              <div class="tab-content">
              	
              	<?php
            	$no=1; foreach ($tabs as $isi) {
            		
            		if ($isi->semester_matakuliah==1) {
            			$act = 'active';
            		} else {
            			$act = '';
            		}
            			echo '<div class="tab-pane '.$act.'" id="tab'.$isi->semester_matakuliah.'">'; ?>
            				<div class="span11">

									<?php
									$get_mk_by_smtr = $this->app_model->jdl_matkul_by_semester_sp($isi->semester_matakuliah);

									$user = $this->session->userdata('sess_login');

										if ($user['id_user_group'] == 1 || $user['id_user_group'] == 10) { ?>

								            <a href="<?php echo base_url(); ?>akademik/jadwal_sp" class="btn btn-warning"> << Kembali</a>

								    <?php } ?>

								    <?php $this->load->model('setting_model'); $aktif = $this->setting_model->getaktivasi('kls_per')->result(); 
								    if (count($aktif) == 1)  { if ($user['id_user_group'] != 5) { ?> 
					                    <!-- <a data-toggle="modal" class="btn btn-success" href="#myModal"><i class="btn-icon-only icon-plus"> </i> Tambah Data</a> -->
					                    <a data-toggle="modal" href="#myModal" class="btn btn-success"> New Data </a>
									<?php } } ?>
				                    <!-- <a href="<?php echo base_url(); ?>akademik/jadwal_sp/cetak_jadwal_all" class="btn btn-info"> Print Jadwal</a> -->
				                    <br><hr>

									<table id="<?php echo 'example'.$isi->semester_matakuliah; ?>" class="table table-bordered table-striped">

					                	<thead>

					                        <tr> 

					                        	<th>No</th>
<!-- 
												<th  width="30">Semester</th> -->

												<th>Kelas</th>

												<th>Hari</th>

					                            <th>Kode MK</th>

					                            <th>Matakuliah</th>

												<th>SKS</th>

												<th>Waktu</th>

												<th>Ruang</th>

												<th>Kapasitas</th>

												<th>Peserta</th>

												<th>Dosen</th>

												<?php if ($user['id_user_group'] != 5) { ?>

					                            <th width="120">Aksi</th>

					                            <?php } ?>

					                        </tr>

					                    </thead>

					                    <tbody>
					                    	<?php $no=1; foreach ($get_mk_by_smtr as $isi)  { 
					                    			//$cek_kelas = $this->db->query('SELECT COUNT(z.`npm_mahasiswa`) as jml FROM tbl_krs z WHERE z.`kd_jadwal` = "'.$isi->kd_jadwal.'"')->row();

					                    			$jml_peserta = $this->db->query('select count(distinct npm_mahasiswa) as jml from tbl_krs_sp where kd_jadwal = "'.$isi->kd_jadwal.'"')->row()->jml;

					                    			
					                    			if (substr($isi->kuota, 0,2) == NULL ) {
					                    				$warna ='';
					                    			}elseif (substr($isi->kuota, 0,2) < $jml_peserta) {
					                    				$warna = 'style="background:#FFB200;"';
					                    			} else {
					                    				$warna ='';
					                    			}
					                    		?>
					                    		
					                    	<tr >

					                           		<td <?php echo $warna; ?> ><?php echo $no; ?></td>
<!-- 
						                        	<td <?php //echo $warna; ?> width="30"><?php //echo $isi->semester_matakuliah; ?></td> -->

						                        	<td <?php echo $warna; ?>><?php echo $isi->kelas; ?></td>

						                        	<td <?php echo $warna; ?>><?php echo notohari($isi->hari); ?></td>

						                        	<td <?php echo $warna; ?>><?php echo $isi->kd_matakuliah; ?></td>

						                        	<td <?php echo $warna; ?>><?php echo $isi->nama_matakuliah; ?></td>

						                        	<td <?php echo $warna; ?>><?php echo $isi->sks_matakuliah; ?></td>

						                        	<td <?php echo $warna; ?>><?php echo substr($isi->waktu_mulai,0,5).'-'.substr($isi->waktu_selesai,0,5); ?></td>

						                        	<?php if ($isi->kode_ruangan == NULL || $isi->kode_ruangan == '') { ?>
						                        		<td <?php echo $warna; ?>>-</td> 
						                        		<td <?php echo $warna; ?>>-</td> 
						                        	<?php }else{ ?>
						                        		<td <?php echo $warna; ?>><?php echo $isi->kode_ruangan; ?></td> 
						                        		<td <?php echo $warna; ?>><?php echo substr($isi->kuota, 0,2)?></td>
						                        	<?php }?>	                        
						                
						                        	<td <?php echo $warna; ?>><?php echo $jml_peserta; ?></td>
						                        	<td <?php echo $warna; ?>><?php echo $isi->nama; ?></td>
						                        	<?php if ($user['id_user_group'] != 5) { ?>
							                        	<?php if (($isi->audit_user != 'baa')){ ?>
							                        	<td <?php echo $warna; ?>>
							                        		<a class="btn btn-success btn-small" onclick="edit2(<?php echo $isi->id_jadwal;?>)" data-toggle="modal" href="#editModal2"><i class="btn-icon-only icon-user"></i></a>
						                                    <a class="btn btn-primary btn-small" onclick="edit(<?php echo $isi->id_jadwal;?>)" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-pencil"></i></a>
						                                    <a class="btn btn-warning btn-small" href="<?php echo base_url('akademik/jadwal_sp/cetak_absensi/').$isi->id_jadwal;?>" target="_blank"><i class="btn-icon-only icon-print"></i></a>
														</td>
														<?php } ?>
													<?php } ?>
												</tr>
												<?php $no++; } ?>
					                    </tbody>
					               	</table>
								</div>
            				</div>
				<?php } ?>
              </div>
			</div>
          </div> <!-- /widget-content -->









<!-- Start Modal -->




<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">FORM DATA</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>akademik/jadwal_sp/save_data" method="post">

                <div class="modal-body">

									<div class="control-group">

                                      <label class="control-label">Jurusan</label>

                                      <div class="controls">

                                        <input type='hidden' class="form-control" name="jurusan" value="<?php echo $this->session->userdata('id_jurusan_prasyarat'); ?>" readonly >

                                        <input type='text' class="form-control"  value="<?php echo $this->session->userdata('nama_jurusan_prasyarat'); ?>" readonly >

                                      </div>

                                    </div>

                                    <script type="text/javascript">

                                    	$(document).ready(function(){

											$('#semester').change(function(){

												$.post('<?php echo base_url();?>akademik/jadwal_sp/get_detail_matakuliah_by_semester/'+$(this).val(),{},function(get){

													$('#ala_ala').html(get);

												});

											});

										});

                                    </script>

                                    <div class="control-group">

                                      <label class="control-label">Semester</label>

                                      <div class="controls">

                                        <select id="semester" class="form-control" name="semester" required="">

                                          <option>--Pilih Semester--</option>

                                          <option value="1">1</option>

                                          <option value="2">2</option>

                                          <option value="3">3</option>

                                          <option value="4">4</option>

                                          <option value="5">5</option>

                                          <option value="6">6</option>

                                          <option value="7">7</option>

                                          <option value="8">8</option>

                                        </select>

                                      </div>

                                    </div>

                                    <div class="control-group">
										<label class="control-label">Kelompok Kelas</label>
										<div class="controls">
											<input type="radio" name="st_kelas" value="PG" required=""> A &nbsp;&nbsp; 
											<input type="radio" name="st_kelas" value="SR" required=""> B &nbsp;&nbsp;
											<input type="radio" name="st_kelas" value="PK" required=""> C  &nbsp;&nbsp;
										</div>
									</div>

                                    	<div class="control-group">											

											<label class="control-label" for="kelas">Nama Kelas</label>

											<div class="controls">

												<input type="text" class="form-control" placeholder="Input Kelas" id="kelas" name="kelas" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<script type="text/javascript">

										$(document).ready(function() {

											$('#ala_ala').change(function() {

												$.post('<?php echo base_url();?>akademik/jadwal_sp/get_kode_mk/'+$(this).val(),{},function(get){

													$('#kode_mk').val(get);

												});

											});

										});

										</script>



										<div class="control-group">											

											<label class="control-label" for="firstname">Hari Kuliah</label>

											<div class="controls">

												<!-- <input type="text" class="span2" name="nama_matakuliah"> -->

												<select class="form-control" name="hari_kuliah" id="hari_kuliah" required>

													<option disabled>--Hari Kuliah--</option>

													<option value="1">Senin</option>

													<option value="2">Selasa</option>

													<option value="3">Rabu</option>

													<option value="4">Kamis</option>

													<option value="5">Jum'at</option>

													<option value="6">Sabtu</option>

													<option value="7">Minggu</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->	

                                    

                						<div class="control-group">											

											<label class="control-label" for="firstname">Nama Matakuliah</label>

											<div class="controls">

												<!-- <input type="text" class="span2" name="nama_matakuliah"> -->

												<select class="form-control" name="nama_matakuliah" id="ala_ala">

													<option>--Pilih MataKuliah--</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->    

                    					

                    					<div class="control-group">										

											<label class="control-label" for="kode_mk">Kode MK</label>

											<div class="controls">

												<input type="text" class="span2" name="kd_matakuliah" placeholder="Kode Matakuliah" id="kode_mk">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<script type="text/javascript">

	                                    	$(document).ready(function(){

												$('#gedung').change(function(){

													$.post('<?php echo base_url();?>akademik/jadwal_sp/get_lantai/'+$(this).val(),{},function(get){

														$('#lantai').html(get);

													});

												});

											});

                                    	</script>

										<div class="control-group">											

											<label class="control-label" for="gedung">Gedung</label>

											<div class="controls">

												<select class="form-control" name="gedung" id="gedung">

													<option value="">--Pilih Gedung--</option>

													<?php foreach ($gedung as $isi) {?>

														<option value="<?php echo $isi->id_gedung ?>"><?php echo $isi->gedung ?></option>	

													<?php } ?>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<script type="text/javascript">

	                                    	$(document).ready(function(){

												$('#lantai').change(function(){

													$.post('<?php echo base_url();?>akademik/jadwal_sp/get_ruangan/'+$(this).val(),{},function(get){

														$('#ruangan').html(get);

													});

												});

											});

                                    	</script>

										<div class="control-group">											

											<label class="control-label" for="lantai">Lantai</label>

											<div class="controls">

												<select name="lantai" id="lantai">

													<option value="">--Pilih Lantai--</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="ruangan">Ruangan</label>

											<div class="controls">

												<select name="ruangan" id="ruangan" required>

													<option value="">--Pilih Ruangan--</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label" for="jam">Jam Masuk</label>

											<div class="controls">

												<input type="text" class="form-control" placeholder="Input Jam Masuk" id="jam_masuk" name="jam_masuk" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <input type="submit" class="btn btn-primary" value="Save"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->



<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit1">            

        </div>
    </div>
</div>

<div class="modal fade" id="editModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit2">
        	
        </div>
    </div>
</div>