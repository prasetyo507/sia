<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
				<form method="post" class="form-horizontal" action="<?php echo base_url(); ?>akademik/viewkrs/viewkrsprodi">
                        <fieldset>
                      <script>
                              $(document).ready(function(){
                                $('#faks').change(function(){
                                  $.post('<?php echo base_url()?>akademik/viewkrs/get_jurusan/'+$(this).val(),{},function(get){
                                    $('#jurs').html(get);
                                  });
                                });
                              });
                              </script>

                              <?php $logged = $this->session->userdata('sess_login'); if ($logged['id_user_group'] == 9) { ?>
                                <div class="control-group">
                                  <label class="control-label">Jurusan</label>
                                  <div class="controls">
                                    <select class="form-control span6" name="jurusan">
                                      <option disabled selected>--Pilih Jurusan--</option>
                                      <?php foreach ($jurusan as $key) { ?>
                                      <option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                              <?php } else { ?>
                                <div class="control-group">
                                  <label class="control-label">Fakultas</label>
                                  <div class="controls">
                                    <select class="form-control span6" name="fakultas" id="faks">
                                      <option>--Pilih Fakultas--</option>
                                      <?php foreach ($fakultas as $row) { ?>
                                      <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label">Jurusan</label>
                                  <div class="controls">
                                    <select class="form-control span6" name="jurusan" id="jurs">
                                      <option>--Pilih Jurusan--</option>
                                    </select>
                                  </div>
                                </div>
                              <?php } ?>      
                              <div class="control-group">
                                <label class="control-label">Angkatan</label>
                                <div class="controls">
                                  <select class="form-control span6" name="tahun">
                                    <option disabled selected>--Pilih Angkatan--</option>
                                    <?php for ($i=2010; $i < 2020; $i++) { ?>
                                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                              <div class="control-group">
                                <label class="control-label">Tahun Akademik</label>
                                <div class="controls">
                                  <select class="form-control span6" name="akad">
                                    <option disabled selected>--Pilih Tahun Akademik--</option>
                                    <?php foreach ($tahunajar as $thn) { ?>
                                      <option value="<?php echo $thn->kode; ?>"><?php echo $thn->tahun_akademik; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>    
                            <br />
                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>
					
				</div>
			</div>
		</div>
	</div>
</div>
