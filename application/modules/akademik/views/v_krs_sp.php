<script type="text/javascript">
function loadjdl(kd) {
        $("#kontenmodal").load('<?php echo base_url()?>akademik/perbaikan/get_jdl/'+kd);
    }
</script>

<?php 
if ($bayar >= 1 ) {
  $btn_krs = '<a target="_blank" href="'.base_url('akademik/perbaikan/print_formulir/'.$krs->kd_krs).'" class="btn btn-success" title=""><i class="icon icon-print"></i> Cetak KRS</a>';
} elseif ($bayar == 0) {
  $btn_krs = '<a  onclick="myFunction()" class="btn btn-success" title=""><i class="icon icon-print"></i> Cetak KRS</a>';
}


 ?>

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-file"></i>
          <h3>KRS Semester Perbaikan</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <?php 
          $aktif = $this->setting_model->getaktivasi('kls_per')->result();
          // var_dump($aktif);
          if (count($aktif) != 0) { ?>
            <a href="<?php echo base_url('akademik/perbaikan/ubah/'.$krs->kd_krs.'/'.$krs->id_jadwal); ?>" class="btn btn-primary" title=""><i class="icon icon-pencil"></i> Edit KRS</a>
          <?php } ?>
          <a target="_blank" href="<?php echo base_url('akademik/perbaikan/print_sp/'.$krs->kd_krs); ?>" class="btn btn-warning" title=""><i class="icon icon-print"></i> Cetak Formulir</a>
          <?php echo $btn_krs; ?>
          <a href="#info" class="btn btn-danger" data-toggle="modal" title=""><i class="icon icon-info"></i> Informasi</a>
          <hr>
          <table id="" class="table table-bordered table-striped">
            <thead>
              <tr style="text-align:center;"> 
                <th rowspan="2" style="vertical-align:middle">No</th>
                <th rowspan="2" style="vertical-align:middle">Kode Matakuliah</th>
                <th rowspan="2" style="vertical-align:middle">Nama Matakuliah</th>
                <th rowspan="2" style="vertical-align:middle">SKS</th>
                <th width="80" colspan="5"  style="text-align:center;">Jadwal</th>
                <th rowspan="2" style="vertical-align:middle" width="70">Status Kelas</th>
              </tr>
              <tr>
                <th style="text-align:center;">Hari</th>
                <th style="text-align:center;">Kelas</th>
                <th style="text-align:center;">Jam</th>
                <th style="text-align:center;">Ruang</th>
                <th style="text-align:center;">Dosen</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; foreach ($filter as $row) { ?>
                <tr>
                  <td><?php echo number_format($no); ?></td>
                  <td><?php echo $row->kd_matakuliah; ?></td>
                  <td><?php echo $row->nama_matakuliah; ?></td>
                  <td><?php echo $row->sks_matakuliah; ?></td>
                  <td align="center"><?php echo notohari($row->hari); ?></td>
                  <td><?php echo $row->kelas; ?></td>
                  <td><?php echo $row->waktu_mulai; ?></td>
                  <?php $rg = $this->temph_model->get_ruang($row->kd_ruangan)->row(); ?>
                  <td><?php echo $rg->ruangan; ?></td>
                  <td><?php echo $row->nama; ?></td>
                  <?php if ($row->open == 1) {
                    $sts = 'OPEN';
                  } else {
                    $sts = 'CLOSE';
                  }
                   ?>
                  <td><?php echo $sts; ?></td>
                </tr>
              <?php $no++; } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="jadwal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="kontenmodal">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
function myFunction() {
    alert("Mohon penuhi Biaya Administrasi terlebih dahulu!");
}
</script>

<div class="modal fade" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="kontenmodal">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Informasi Kelas Perbaikan</h4>
          </div>
          <div class="modal-body">  
            <p>
              Pada tombol <b>cetak formulir</b> dan <b>cetak KRS</b> data tidak akan dimunculkan jika <b style="color:red">status kelas masih "CLOSE"</b>
            </p>
          </div> 
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
          </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->