<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-calendar"></i>
                <h3>Jadwal Ujian</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <form method="post" class="form-horizontal" action="<?php echo base_url()?>akademik/jadwal_ujian/save_sess">
                        <fieldset>

                            <!-- if user as BAA -->
                            <?php if ($grup == 10) { ?>
                                <div class="control-group">
                                    <label class="control-label">Prodi</label>
                                    <div class="controls">
                                        <select class="form-control span6" name="prodi">
                                            <option disabled="" selected="">--Pilih Prodi--</option>
                                            <?php foreach ($prod as $val) { ?>
                                                <option value="<?= $val->kd_prodi ?>"><?= $val->prodi ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                            <!-- if user as BAA /end-->

                            <div class="control-group">
                                <label class="control-label">Tahun Akademik</label>
                                <div class="controls">
                                    <select class="form-control span6" name="year">
                                        <option>-- Pilih Tahun Akademik --</option>
                                        <?php foreach ($date as $value) {
                                            echo "<option value='".$value->kode."'> ".$value->tahun_akademik." </option>";
                                        } ?>
                                    </select>
                              </div>
                            </div>
                            <br />
                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
