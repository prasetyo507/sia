<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_dosen_tetap_dan_tidak.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 2px solid black;
}


th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr>
            <th>NO</th>
            <th>Status Dosen</th>
            <th>Jumlah Dosen</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Dosen Tetap</td>
            <td><?php echo $dosen_tetap->jumlh_dsn; ?></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Dosen Tidak Tetap</td>
            <td><?php echo $dosen_tidak->juml_dsn; ?></td>
        </tr>
    </tbody>
</table>