<style type="text/css">
.lunas{
background-color:#99ff66 !important;
}
.blm_lunas{
background-color:#cc3333 !important;
}
</style>

<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">DATA PEMBAYARAN KULIAH SEMESTER 1</h4>
            </div>
            <form class ='form-horizontal' action="#" method="post">
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>NO</th>
                                <th>KETERANGAN</th>
                                <th>TAGIHAN</th>
                                <th>TERBAYAR</th>
                                <th>STATUS</th>
                                <th width="40">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php $no=1; foreach ($row as $key): ?>
                                <?php if ($key->flag == 4){
                                    $ket = 'LUNAS';
                                } else{
                                    $ket = 'BELUM LUNAS';
                                }
                                ?>    
                                <tr class="<?php if ($key->flag == 4 ) {
                                        echo "lunas";
                                    }else{
                                        echo "blm_lunas";
                                    }?>">
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $key->keterangan; ?></td>
                                    <td><?php echo $key->credit; ?></td>
                                    <td><?php echo $key->debit; ?></td>
                                    <td><?php echo $ket; ?> </td>
                                    <td class="td-actions">
                                        <input type="checkbox" checked disabled/>
                                    </td>
                                </tr>
                                <?php $no++; endforeach ?>
                        </tbody>
                    </table>            
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <input type="submit" class="btn btn-primary" value="Print"/>
                </div>
            </form>