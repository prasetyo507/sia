<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var table = $('#tabel_krs');
	var oTable = table.dataTable({
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
		
	var hari = [];
		hari[1] = 'Senin'; 
		hari[2] = 'Selasa';
		hari[3] = 'Rabu';
		hari[4] = 'Kamis';
		hari[5] = 'Jumat';
		hari[6] = 'Sabtu';
		hari[7] = 'Minggu';
	
		
	var table_jadwal = $('#tabel_jadwal');	
	var oTable_jadwal = table_jadwal.dataTable({
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [5,6] }],
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
		oTable.on('click', 'tbody tr .edit', function() {
			var nRow = $(this).parents('tr')[0];
			var aData = oTable.fnGetData(nRow);
			var kd_matakuliah = aData[1];
			$.ajax({
				url: "<?php echo base_url('akademik/bimbingan/get_jadwal'); ?>",
				type: "post",
				data: {kd_matakuliah: kd_matakuliah},
				success: function(d) {
					var parsed = JSON.parse(d);
                    var arr = [];
                    for (var prop in parsed) {
                        arr.push(parsed[prop]);
                    }
                    oTable_jadwal.fnClearTable();
                    oTable_jadwal.fnDeleteRow(0);
                    for (var i = 0; i < arr.length; i++) {
                        oTable_jadwal.fnAddData([hari[arr[i]['hari']],arr[i]['kelas'],arr[i]['waktu_mulai'].substring(0, 5)+'/'+arr[i]['waktu_selesai'].substring(0, 5), arr[i]['kode_ruangan'],arr[i]['nama'],arr[i]['kd_jadwal'],arr[i]['kd_matakuliah'],arr[i]['kuota'],arr[i]['jumlah']]);
                    }
					$('#jadwal').modal('show');
				}
			});	
		});
		
		oTable_jadwal.on( 'click', 'tr', function () {
        if ( $(this).hasClass('active') ) {
            $(this).removeClass('active');
        }
        else {
            table_jadwal.$('tr.active').removeClass('active');
            $(this).addClass('active');
			var aData = oTable_jadwal.fnGetData(this);
			$('#kd_jadwal').val(aData[5]);
			$('#kd_matakuliah').val(aData[6]);
        }
    } );
	});
</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi - <?php echo $nmmhs->NMMHSMSMHS.' ( '.$nmmhs->NIMHSMSMHS.' )'; ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a onclick="history.go(-1)" class="btn btn-success"><< Kembali</a>
					<?php 
						$aktif = $this->setting_model->getaktivasi('krs')->result();
						$aktif2 = $this->setting_model->getaktivasi('krss2')->result();
						$rev = $this->setting_model->getaktivasi('revisikrs')->result();
						$rev2 = $this->setting_model->getaktivasi('revisikrss')->result();
      					//if (($aktif == TRUE || $aktif2 ==  TRUE) or (count($rev) != 0 || count($rev) != 0)) { ?>
							<a href="#verif" data-toggle="modal" class="btn btn-primary">Verifikasi</a>
					<?php //} ?>
					<a href="<?php echo base_url(); ?>akademik/bimbingan/dp_view/<?php echo $this->session->userdata('npm'); ?>" target="blank" class="btn btn-warning">KHS Mahasiswa</a>
					
					<hr>
					<table>
						<tr>
							<td>Status</td>
								<?php if ($sts->status_verifikasi == 1) {
									$stats = 'TERIMA';
								} elseif ($sts->status_verifikasi == 2) {
									$stats = 'TUNDA';
								} elseif ($sts->status_verifikasi == 3) {
									$stats = 'TOLAK';
								} else {
									$stats = '-';
								}
							?>
							<td>&nbsp&nbsp</td>
							<td> : <?php echo $stats; ?></td>
							<td></td>
						</tr>
						<tr>
							<td>Pembimbing Akademik</td>
							<td>&nbsp&nbsp</td>
							<td> : <?php echo $pembimbing['nama']; ?></td>
							<td></td>
						</tr>
						<tr>
							<td>Catatan dari mahasiswa</td>
							<td>&nbsp&nbsp</td>
							<td> : <?php echo $pembimbing['keterangan_krs']; ?></td>
							<td></td>
						</tr>
					</table>
					<table id="tabel_krs" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Kode MK</th>
	                        	<th>Mata Kuliah</th>
	                        	<th>SKS</th>								
	                        	<th>Dosen</th>
								<th>Hari/Waktu</th>
								<th>Ruangan</th>
								<!-- <th width="40">Pilih Jadwal</th>
								<th width="40">Kosongkan Jadwal</th> -->
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($detail_krs as $row) { ?>
	                        <tr>
								<?php
									switch ($row->hari){
										case 1 :
											$hari = 'Senin';
										break;
										case 2 :
											$hari = 'Selasa';
										break;
										case 3 :
											$hari = 'Rabu';
										break;
										case 4 :
											$hari = 'Kamis';
										break;
										case 5 :
											$hari = 'Jumat';
										break;
		
										case 6 :
											$hari = 'Sabtu';
										break;
										case 7 :
											$hari = 'Minggu';
										break;
										default:
										$hari = '';
									}
								?>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->kd_matakuliah; ?></td>
                                <td><?php echo $row->nama_matakuliah; ?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $hari.' / '.substr($row->waktu_mulai, 0,5).'-'.substr($row->waktu_selesai, 0,5); ?></td>
                                <td><?php echo $row->kode_ruangan; ?></td>
                                <?php $this->load->model('setting_model');?>
                               	<!--td class="td-actions">
                                	<a data-toggle="modal" class="btn btn-success btn-small edit"><i class="btn-icon-only icon-ok"> </i></a>
                                	
                                </td>
                                <td>
                                	<a href="<?php //echo base_url(); ?>akademik/bimbingan/kosongkan_jadwal/<?php //echo $row->id_krs; ?>" class="btn btn-danger btn-small edit"><i class="btn-icon-only icon-remove"> </i></a>
                                </td-->
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
	               	<hr>
	               	<h3>Riwayat Perwalian</h3>
	               	<br>
					<table id="tabel_krs" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Catatan Pembimbing</th>
	                        	<th>Tanggal</th>
	                        	<th>Tahun Ajaran</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($histori as $kew) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $kew->note; ?></td>
                                <td><?php echo $kew->tgl; ?></td>
                                <td><?php echo substr($kew->kd_krs, 12,5); ?></td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="jadwal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">PILIH JADWAL MATAKULIAH</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/viewkrs/save_jadwal" method="post">
                <div class="modal-body">    
					<input type="hidden" id="kd_jadwal" name="kd_jadwal" value=""/>
					<input type="hidden" id="kd_matakuliah" name="kd_matakuliah" value=""/>
					<input type="hidden" id="kd_krs" name="kd_krs" value="<?php echo $kd_krs; ?>"/>
                    <table id="tabel_jadwal" class="table table-bordered table-striped">
	                  <thead>
	                        <tr> 
	                          <th>Hari</th>
	                          <th>Kelas</th>
	                          <th>Jam</th>
	                          <th>Ruang</th>
							  <th>Dosen</th>
							  <th></th>
							  <th></th>
							  <th>Kuota</th>
							  <th>Jumlah</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<tr>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
	                    	</tr>
	                    </tbody>
	                </table>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="verif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Status Verifikasi</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/bimbingan/comein" method="post">
                <div class="modal-body"> 

                    <div class="control-group">
                    	<input type="hidden" name="kode" value="<?php echo $detail_krs_row->kd_krs; ?>">
                    	<!-- <input type="hidden" name="id_verif" value="<?php echo $detail_krs_row->id_verifikasi; ?>"> -->
                        <label class="control-label">Status</label>

                        <div class="controls">

                          <select class="form-control span3" name="sts" id="" required>

                            <option disabled selected>--Pilih Status--</option>

                            <option value="1">Terima</option>

                            <option value="2">Tunda</option>

                            <option value="3">Tolak</option>

                          </select>

                        </div>

                    </div>	

                    <div class="control-group">

                        <label class="control-label">Catatan</label>

                        <div class="controls">

                          <textarea name="note" class="form-control span3" required></textarea>

                        </div>

                    </div>
					
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>