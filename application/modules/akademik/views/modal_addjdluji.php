<!-- autocomplete load jadwal -->
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('input[name^=pengawas]').autocomplete({
            source: '<?= base_url('akademik/jadwal_ujian/loaddosen');?>',
            minLength: 3,
            select: function (evt, ui) {
                this.form.pengawas.value = ui.item.value;
                this.form.hiddenguide.value = ui.item.dosen;
            }
        });

        $('input[name^=ruangan]').autocomplete({
            source: '<?= base_url('akademik/jadwal_ujian/loadroom');?>',
            minLength: 1,
            select: function (evt, ui) {
                this.form.ruangan.value = ui.item.value;
                this.form.hiddenroom.value = ui.item.idroom;
            }
        });

    });

    $(document).ready(function(){
        $( "#datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2015:<?= date('Y')+1; ?>',
            dateFormat: 'dd/mm/yy'
        });
    });

    $(document).ready(function(){
        $("#timepickerr").timepicker();
    });
</script>

<!-- ajax input data -->
<script type="text/javascript">
    $(function() {
        $('#ajaxform').on('submit', function () {
            // ajax input data
            var data = $('#ajaxform').serialize();
            $.ajax({
                type: 'POST',
                url: "<?= base_url('akademik/jadwal_ujian/addjadwal') ?>",
                data: data
            });
        });
    });
</script>

<form id="" action="<?= base_url('akademik/jadwal_ujian/addjadwal') ?>" method="post" >
                
    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Tambah Data Jadwal</h4>

    </div>

    <div class="modal-body">

        <?php if ($cek1 == 1 && $cek2 == 1) {
            echo "<h3>Data UTS dan UAS sudah ditambahkan</h3>";
        } else { ?>

            <input type="hidden" name="id" value="<?= $data->id_jadwal; ?>" />

            <input type="hidden" name="tahun" value="<?= $akad; ?>" />

            <input type="hidden" name="prodi" value="<?= $data->kd_jadwal; ?>" />

            <div class="control-group">
                <label class="control-label">Nama Kelas</label>
                <div class="controls">
                    <input class="form-control span5" value="<?= $data->kelas; ?>" disabled=""/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Tipe Ujian</label>
                <div class="controls">
                    <select class="form-control span5" name="tipe">
                        <option disabled="" selected="">Pilih Tipe Ujian</option>

                        <?php if ($cek1 == 0) {
                            echo '<option value="1">UTS</option>';
                        } ?>
                        
                        <?php if($cek2 == 0) {
                            echo '<option value="2">UAS</option>';
                        } ?>
                        
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Tanggal Ujian</label>
                <div class="controls">
                    <input class="form-control span5" id="datepicker" value="" name="start" placeholder="Tanggal Ujian" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Ruangan</label>
                <div class="controls">
                    <input class="form-control span5" placeholder="Ketik Nama Ruangan" name="ruangan" />
                    <input type="hidden" value="" name="hiddenroom" />
                </div>
            </div>

            <!-- <div class="control-group">
                <label class="control-label">Pengawas</label>
                <div class="controls">
                    <input class="form-control span5" placeholder="Ketik Nama Dosen" name="pengawas" />
                    <input type="hidden" value="" name="hiddenguide" />
                </div>
            </div> -->

            <div class="control-group">
                <label class="control-label">Jam Mulai</label>
                <div class="controls">
                    <input class="form-control span5" id="timepickerr" placeholder="00:00" name="starttime" />
                </div>
            </div>

        </div>

        <div class="modal-footer">

            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        </div>
            
        <?php } ?>

</form>