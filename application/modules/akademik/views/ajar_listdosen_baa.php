<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>



<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Data Dosen Mengajar</h3>

            </div> <!-- /widget-header -->

            

            <div class="widget-content">

                <div class="span11">

                    <a href="<?php echo base_url(); ?>akademik/ajar" class="btn btn-primary "> << Kembali</a>  <a href="<?php echo base_url(); ?>akademik/ajar/export_excel" class="btn btn-success "> Export Data</a><p>* Total SKS yang dihitung hanya mata kuliah (tidak termasuk skripsi, tesis ataupun kerja praktek/magang)</p><hr>

                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 

                                <th>No</th>

                                <th>NID</th>

                                <th>Nama Dosen</th>

                                <th>Total Sks</th>

                                <th width="80">Aksi</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no=1; foreach ($dosen as $isi): ?>

                                <tr>

                                    <td><?php echo $no; ?></td>

                                    <td><?php echo $isi->nid; ?></td>

                                    <td><?php echo $isi->nama; ?></td>

                                    <td><?php echo $isi->sks; ?></td>

                                    <td class="td-actions">

                                       <a href="<?php echo base_url();?>akademik/ajar/show_dosen_by_prodi/<?php echo $isi->nid; ?>" class="btn btn-primary btn-small"><i class="btn-icon-only icon-ok"> </i></a>

                                       <a href="<?php echo base_url();?>akademik/ajar/print_surat_tugas/<?php echo $isi->nid; ?>" class="btn btn-success btn-small"><i class="btn-icon-only icon-print"> </i></a>

                                    </td>

                                </tr>    

                            <?php $no++; endforeach ?>

                            

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>



