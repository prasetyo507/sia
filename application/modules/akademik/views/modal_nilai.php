

<form id="" action="" method="post" >
                
    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Detail <?php echo get_nama_mk($general->kd_matakuliah,$general->kd_prodi)?> (<?php echo $general->kd_matakuliah ?>)</h4>

    </div>

    <div class="modal-body">
		<table border="0">
			<?php if (!empty($avg->nilai)) {?>
				<tr>
					<td style="font-weight: bold;">Rata-rata Tugas</td>
					<td style="font-weight: bold;">:</td>
					<td align="right"><?php echo $avg->nilai ?></td>
				</tr>
			<?php } ?>
			<?php if (!empty($absen->nilai)) {?>
				<tr>
					<td style="font-weight: bold;">Absensi</td>
					<td style="font-weight: bold;">:</td>
					<td align="right"><?php echo $absen->nilai ?> </td>
				</tr>
			<?php } ?>
			<?php if (!empty($uts->nilai)) {?>
				<tr>
					<td style="font-weight: bold;">Nilai UTS</td>
					<td style="font-weight: bold;">:</td>
					<td align="right"><?php echo $uts->nilai ?> </td>
				</tr>
			<?php } ?>
			<?php if (!empty($uas->nilai)) {?>
				<tr>
					<td style="font-weight: bold;">Nilai UAS</td>
					<td style="font-weight: bold;">:</td>
					<td align="right"><?php echo $uas->nilai ?> </td>
				</tr>
			<?php } ?>
			<?php if (!empty($akhir->nilai)) {?>
				<tr>
					<td style="font-weight: bold;">Nilai Akhir</td>
					<td style="font-weight: bold;">:</td>
					<td align="right"><?php echo $akhir->nilai ?></td> 
				</tr>
			<?php } ?>
		</table>
			
    </div>

            

</form>