<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Jumlah Mahasiswa Aktif</h3>
            </div> <!-- /widget-header -->           

            <div class="widget-content">
                <div class="span11">
                    <a href="<?= base_url('akademik/amount_stdn/export') ?>" class="btn btn-success">Export Excel</a>
                    <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Prodi</th>
                                <th>Kelas A</th>
                                <th>Kelas B</th>
                                <th>Kelas C</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($data as $isi): ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $isi->prodi; ?></td>
                                    <td>
                                        <a href="<?= base_url('akademik/amount_stdn/detail/'.$isi->kd_prodi.'/PG') ?>">
                                            <?= amountStudent($isi->kd_prodi,'PG',$sess) ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?= base_url('akademik/amount_stdn/detail/'.$isi->kd_prodi.'/SR') ?>">
                                            <?= amountStudent($isi->kd_prodi,'SR',$sess) ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?= base_url('akademik/amount_stdn/detail/'.$isi->kd_prodi.'/PK') ?>">
                                            <?= amountStudent($isi->kd_prodi,'PK',$sess) ?>
                                        </a>
                                    </td>
                                </tr>    
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>