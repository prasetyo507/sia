<script type="text/javascript">
$(document).ready(function() {
  $('#mulai').datepicker({dateFormat: 'yy-mm-dd',
                  yearRange: "2015:2025",
                  changeMonth: true,
                  changeYear: true
  });

  $('#ahir').datepicker({dateFormat: 'yy-mm-dd',
                  yearRange: "2015:2025",
                  changeMonth: true,
                  changeYear: true
  });

});
</script>

<script>
    function edit(idk){
        $('#edit').load('<?php echo base_url();?>akademik/kalender/edit_isi_kalender/'+idk);
    }
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-list"></i>
                <h3>Data Kalender Akademik - <?php echo $ta ?></h3>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="span11">
                    <?php 
            
                    $logged = $this->session->userdata('sess_login');

                    $pecah = explode(',', $logged['id_user_group']);
                    $jmlh = count($pecah);
                    for ($i=0; $i < $jmlh; $i++) { 
                        $grup[] = $pecah[$i];
                    }?>

                    <a href="<?php echo base_url()?>akademik/kalender" class="btn btn-warning"></i>Kembali</a>
                        
                    <a target="_blank" href="<?php echo base_url()?>akademik/kalender/cetak_isi_kalender/<?php echo $kode ?>" class="btn btn-default"><i class="icon icon-print"></i>&nbsp; Cetak</a>
                    <center><h3>Kalender Akademik Semester <?php if(substr($ta, -1) == 1){echo "Ganjil - ".$ta;}else{echo "Genap- ".$ta;}  ?>  </h3></center><br>
                    <h3>KEGIATAN PRA SEMESTER</h3>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>NO</th>
                                <th>KEGIATAN</th>
                                <th>WAKTU PELAKSANAAN</th>
                                <th>KETERANGAN</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $no=1; foreach ($pra as $isi) { 
                            if ($isi->mulai == $isi->ahir) {
                                $waktu = TanggalIndo($isi->mulai);
                            }else{
                                $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                            }

                            ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $isi->kegiatan ?></td>
                                <td><?php echo $waktu ?></td>
                                <td><?php echo $isi->keterangan ?></td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                    <h3>KEGIATAN SEMESTER</h3>
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>NO</th>
                                <th>KEGIATAN</th>
                                <th>WAKTU PELAKSANAAN</th>
                                <th>KETERANGAN</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $no=1; foreach ($acara as $isi) { 
                            if ($isi->mulai == $isi->ahir) {
                                $waktu = TanggalIndo($isi->mulai);
                            }else{
                                $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                            }

                            ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $isi->kegiatan ?></td>
                                <td><?php echo $waktu ?></td>
                                <td><?php echo $isi->keterangan ?></td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                    <h3>KEGIATAN PEMBAYARAN KULIAH SEMESTER</h3>
                    <table id="example3" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>NO</th>
                                <th>KEGIATAN</th>
                                <th>WAKTU PELAKSANAAN</th>
                                <th>KETERANGAN</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $no=1; foreach ($biaya as $isi) { 
                            if ($isi->mulai == $isi->ahir) {
                                $waktu = TanggalIndo($isi->mulai);
                            }else{
                                $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                            }

                            ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $isi->kegiatan ?></td>
                                <td><?php echo $waktu ?></td>
                                <td><?php echo $isi->keterangan ?></td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


