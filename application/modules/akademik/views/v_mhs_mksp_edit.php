<script type="text/javascript">
  function settup(elem) {
    var elems = document.getElementsByTagName("input");
    var currentState = elem.checked;
    var elemsLength = elems.length;

    for(i=0; i<elemsLength; i++)
    {
      if(elems[i].type == "checkbox" && elems[i].className == elem.className)
      {
         elems[i].checked = false;   
      }
    }

    elem.checked = currentState;
  }​
</script>
<script type="text/javascript">
  function loadjdl(kd) {
    $("#kontenmodal").load('<?php echo base_url()?>akademik/perbaikan/get_jdl/'+kd);
  }
</script>
<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-pencil"></i>
          <h3>Rubah Matakuliah Perbaikan</h3>
      </div> <!-- /widget-header -->
      <div class="widget-content">
        <form action="<?php echo base_url('akademik/perbaikan/addkrs'); ?>" method="post" accept-charset="utf-8">
          <div class="span11">
            <table id="" class="table table-bordered table-striped">
              <thead>
                <tr style="text-align:center;"> 
                  <th rowspan="2" style="vertical-align:middle">No</th>
                  <th rowspan="2" style="vertical-align:middle">Kode Matakuliah</th>
                  <th rowspan="2" style="vertical-align:middle">Nama Matakuliah</th>
                  <th rowspan="2" style="vertical-align:middle">SKS</th>
                  <th width="80" colspan="5" style="text-align:center;">Jadwal</th>
                  <th rowspan="2" width="50" style="vertical-align:middle">Pilih MK</th>
                </tr>
                <tr>
                  <th style="text-align:center;">Hari</th>
                  <th style="text-align:center;">Kelas</th>
                  <th style="text-align:center;">Jam</th>
                  <th style="text-align:center;">Ruang</th>
                  <th style="text-align:center;">Dosen</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1; foreach ($filter as $row) { ?>
                  <tr>
                    <td><?php echo number_format($no); ?></td>
                    <td><?php echo $row->kd_matakuliah; ?></td>
                    <td><?php echo $row->nama_matakuliah; ?></td>
                    <td><?php echo $row->sks_matakuliah; ?></td>
                    <td align="center"><?php echo notohari($row->hari); ?></td>
                    <td><?php echo $row->kelas; ?></td>
                    <td><?php echo $row->waktu_mulai; ?></td>
                    <?php $rg = $this->temph_model->get_ruang($row->kd_ruangan)->row(); ?>
                    <td><?php echo $rg->ruangan; ?></td>
                    <td><?php echo $row->nama; ?></td>
                    <?php 
                      $ceking = $this->temph_model->cek_on_edit_sp($krs,$row->kd_jadwal)->row();
                      if (count($ceking) == 0) { ?>
                         <td style="text-align:center;"><input type="checkbox" name="kdjdl[]" value="<?php echo $row->kd_jadwal; ?>" placeholder=""></td>
                      <?php } else { ?>
                         <td style="text-align:center;"><input type="checkbox" name="kdjdl[]" value="<?php echo $row->kd_jadwal; ?>" checked=""></td>
                      <?php } ?>
                  </tr>
                  <input type="hidden" name="nim" value="<?php echo $nim; ?>">
                <?php $no++; } ?>

                <!-- jika ada mk yg belum dipilih sebelumnya dan saat edit krs ingin menambahkan mk tsb -->
                <?php if ($unselectedmk) { 
                  foreach ($unselectedmk as $vals) { ?>

                  <tr>
                    <td><?php echo number_format($no); ?></td>
                    <td><?php echo $vals->kd_matakuliah; ?></td>
                    <td><?php echo $vals->nama_matakuliah; ?></td>
                    <td><?php echo $vals->sks_matakuliah; ?></td>
                    <td align="center"><?php echo notohari($vals->hari); ?></td>
                    <td><?php echo $vals->kelas; ?></td>
                    <td><?php echo $vals->waktu_mulai; ?></td>
                    <?php $rg = $this->temph_model->get_ruang($vals->kd_ruangan)->row(); ?>
                    <td><?php echo $rg->ruangan; ?></td>
                    <td><?php echo $vals->nama; ?></td>
                    <?php 
                      $ceking = $this->temph_model->cek_on_edit_sp($krs,$vals->kd_jadwal)->row();
                      if (count($ceking) == 0) { ?>
                         <td style="text-align:center;"><input type="checkbox" name="kdjdl[]" value="<?php echo $vals->kd_jadwal; ?>" placeholder=""></td>
                      <?php } else { ?>
                         <td style="text-align:center;"><input type="checkbox" name="kdjdl[]" value="<?php echo $vals->kd_jadwal; ?>" checked=""></td>
                      <?php } ?>
                    </tr>
                <?php } } ?>
                
              </tbody>
            </table>
            <hr>
            <button type="submit" class="btn btn-primary" title=""><i class="icon icon-ok"></i> Submit Formulir</button>
            <a href="<?php echo base_url('akademik/perbaikan/del_mksp/'.$krs); ?>" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Semua Matakuliah Yang Dipilih?')" class="btn btn-danger"><i class="icon icon-remove"></i> Hapus Semua</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="jadwal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="kontenmodal">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->