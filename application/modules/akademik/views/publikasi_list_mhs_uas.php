<script type="text/javascript">
    $(function() {
        $("#example4").dataTable();
    });
</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Daftar NIlai Mahasiswa</h4>
    <h5><?php echo $title->nama_matakuliah.' ('.$title->kelas.' )' ?></h5>
</div>
<form class ='form-horizontal' method="post" action="<?php echo base_url(); ?>akademik/publikasi/publikasi_uas" >
    <div class="modal-body">  
        <div class="control-group" id="">
            <table id="example4" class="table table-bordered table-striped">
            	<thead>
                    <tr> 
                        <th>No</th>
                        <th width="90">NIM</th>
                        <th>NAMA</th>
                        <th width="80">Nilai</th>
                    </tr>
                </thead>
                <tbody>
                	<?php $no = 1; foreach ($kelas as $value) { ?>
                	<!--input type="hidden" name="jadwal" value="<?php echo $value->kd_jadwal; ?>"/-->
                	<tr>
                		<td><?php echo number_format($no); ?></td>
                		<td><?php echo $value->NIMHSMSMHS; ?></td>
                		<td><?php echo $value->NMMHSMSMHS; ?></td>
                		<td><?php echo $value->nilai; ?></td>
                	</tr>
                	<?php $no++; } ?>
                </tbody>
                <input type='hidden' name='id_jadwal' value="<?php echo $title->id_jadwal; ?>">
                <input type='hidden' name='posisi' value="<?php echo $posisi; ?>">
            </table>
        </div>              
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <?php if ($pub == 1){ ?>
            <input type="submit" class="btn btn-primary" value="Publikasi"/>    
        <?php }elseif ($pub == 2) { ?>
            <H3>Nilai Sudah Di Publikasi</H3>
        <?php } ?>
            
    </div>
</form>