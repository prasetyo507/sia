<div class="modal-header">
    
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    
	<h4 class="modal-title">Ubah Status Upload</h4>

</div>

<form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/upload_soal/save_uts/<?php echo $id; ?>" method="post" enctype="multipart/form-data">
    
	<div class="modal-body"> 
        
		<div class="control-group" id="">
			
			<label class="control-label">Status Upload Soal UTS</label>
			
			<div class="controls">
				
				<select name="status_uts" class="span3" required>
											
					<option selected disabled>-- Pilih Status Upload Soal UTS --</option>	
					
					<option <?php if($row->status_uts=='Terima'){ echo ' selected="selected"'; }?> value="Terima">Terima</option>
					
					<option <?php if($row->status_uts=='Revisi'){ echo ' selected="selected"'; }?> value="Revisi">Revisi</option>
					
				</select>
			</div>
        </div>
        
		<div class="control-group" id="">
			
			<label class="control-label">Komentar/Alasan</label>
			
			<div class="controls">
				
				<textarea name="komentar_uts"  class="span3"><?php echo $row->komentar_uts ?></textarea>
				
			</div>
        
		</div> 	
    
	</div> 
    
	<div class="modal-footer">
        
		<button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        
		<input type="submit" class="btn btn-primary" value="Simpan"/>
    
	</div>

</form>