<?php 
//var_dump($info_mhs);die();
ob_start();

// class PDF extends FPDF
// {
// 	function Footer()
//     {
//         // Go to 1.5 cm from bottom
// 	    $this->SetY(-10);
// 	    // Select Arial italic 8
// 	    $this->ln(0);
// 		$this->SetFont('Arial','',8);
// 		$this->Cell(10,0,'Menyetujui,',0,0,'L');

// 		$this->ln(8);
// 		$this->SetFont('Arial','',8);
// 		$this->Cell(135,0,'Ka.Prodi / Ka.bag TU',0,1,'L');
		
// 		$this->ln(10);
// 		$this->SetFont('Arial','',8);
// 		$this->Cell(135,0,'1.',0,0,'L');
// 		$this->Cell(135,0,'(...................................)',0,1,'L');
// 	}
// }


$pdf = new FPDF("P","mm", "A5");
$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetMargins(3, 5 ,0);


$pdf->Ln(0);

$pdf->SetFont('Arial','B',8);

$pdf->image('http://172.16.2.42:801/assets/logo.gif',5,5,18);

$pdf->Ln();

$pdf->Ln();

$pdf->SetFont('Arial','B',12);

$pdf->Cell(50,5,'',0,'C');

$pdf->Cell(50,5,'FORM PENDAFTARAN',0,'L');

$pdf->Ln();

$pdf->Cell(48,5,'',0,'C');

$pdf->Cell(50,5,'SEMESTER PERBAIKAN ',0,'L');
$pdf->Line(4,25,142,25);
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('Arial','',8);

$pdf->Cell(20,5,'FAKULTAS',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(60,5,get_fak_byprodi($prodi),0,'C');

$pdf->Cell(10,5,'NAMA',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(60,5,$info_mhs->NMMHSMSMHS,0,'C');

$pdf->Ln();

$pdf->Cell(20,5,'Program Studi',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(60,5,get_jur($prodi),0,'C');

$pdf->Ln();

$pdf->Cell(20,5,'SEMESTER',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(60,5,$this->app_model->get_semester($smtr->SMAWLMSMHS),0,'C');

$pdf->Cell(10,5,'NPM',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(60,5,$info_mhs->NIMHSMSMHS,0,'C');

$pdf->Ln();

$pdf->Cell(20,5,'Tahun Ajaran',0,'C');

$pdf->Cell(5,5,':',0,'C');

// $pdf->Cell(60,5,$tahun.' - '.$ganjilgenap,0,'C');
$pdf->Cell(60,5,$tahun,0,'C');

$pdf->Ln();
$pdf->ln();


$pdf->SetFont('Arial','B',8);
$pdf->Cell(7,10,'NO','L,T,R,B',0,'C');
$pdf->Cell(20,10,'KODE MK','L,T,R,B',0,'C');
$pdf->Cell(64,10,'MATAKULIAH','L,T,R,B',0,'C');
$pdf->Cell(8,10,'SKS','L,T,R,B',0,'C');
$pdf->Cell(20,10,'NILAI LAMA','L,T,R,B',0,'C');
$pdf->Cell(20,10,'BIAYA','L,T,R,B',0,'C');

$pdf->ln();

$no=0;
$sks = 0;
$t_sks = 0;

	foreach ($matkul as $isi) {

		if ($info_mhs->KDPSTMSMHS == '74101' || $info_mhs->KDPSTMSMHS == '61101') {
			if ($isi->sks_matakuliah == 3) {
				$b_sks = 1500000;
			} if ($isi->sks_matakuliah == 2) {
				$b_sks = 1000000;
			}
		} else {
			if ($isi->sks_matakuliah == 3) {
				$b_sks = 500000;
			} if ($isi->sks_matakuliah == 2) {
				$b_sks = 400000;
			}
			if ($isi->sks_matakuliah == 4) {
				$b_sks = 800000;
			}
		}
		
		

		$no++;
		$this->db->select('*');
		$this->db->where('NIMHSTRLNM', $info_mhs->NIMHSMSMHS);
		$this->db->where('KDKMKTRLNM', $isi->kd_matakuliah);
		$this->db->order_by('id','desc');
		$nilai_lama=$this->db->get('tbl_transaksi_nilai',1)->row();

		$pdf->Cell(7,5,$no,'L,T,R,B',0,'C');

		$pdf->Cell(20,5,$isi->kd_matakuliah,'L,T,R,B',0,'C');

		$pdf->Cell(64,5,$isi->nama_matakuliah,'L,T,R,B',0,'C');

		$pdf->Cell(8,5,$isi->sks_matakuliah,'L,T,R,B',0,'C');

		//$pdf->Cell(20,5,'E','L,T,R,B',0,'C');

		$pdf->Cell(20,5,$nilai_lama->NLAKHTRLNM,'L,T,R,B',0,'C');

		$pdf->Cell(20,5,number_format($b_sks),'L,T,R,B',1,'C');

		$sks = $sks + $isi->sks_matakuliah;

		$t_sks = $t_sks + $b_sks;
	}

$pdf->Cell(91,5,'Total SKS yang diambil','L,T,R,B',0,'C');
$pdf->Cell(8,5,$sks,'L,T,R,B',0,'C');
$pdf->Cell(20,5,'','L,T,R,B',0,'C',1);
$pdf->Cell(20,5,'-------------(+)','L,T,R,B',1,'R');

$pdf->Cell(91,5,'Total Biaya Semester Perbaikan','L,T,R,B',0,'C');
$pdf->Cell(48,5,number_format($t_sks),'L,T,R,B',0,'R');		



$pdf->ln(6);

$pdf->SetFont('Arial','',7);

// $pdf->Cell(8,5,'Keterangan :',0,'C');

// $pdf->ln();

// $pdf->Cell(8,5,'',0,'R');

// $pdf->Cell(53,5,'1.  Maksimal 9 SKS',0,'C');


$pdf->ln(15);

$pdf->SetFont('Arial','',8);

$pdf->Cell(11,0,'',0,'L');

$pdf->Cell(200,0,'Mengetahui,',0,1,'L');

$pdf->SetX(96);
$pdf->Cell(110,0,'Bekasi,'.date('d-m-Y').'',0,'L');

//$pdf->SetY(-37);

$pdf->ln(4);



$pdf->Cell(87,0,'Dosen Pembimbing Akademik',0,'L');
//$info_mhs->NMMHSMSMHS
$pdf->SetX(96);
$pdf->Cell(110,0,'Mahasiswa Ybs,',0,'C');


$pdf->ln(20);

$pdf->SetFont('Arial','',8);

$pdf->Cell(2,0,'',0,'L');

$pdf->Cell(87,0,'( '.$dosen->nama.' )',0,'C');

$pdf->SetX(96);
$pdf->Cell(87,0,'( '.$info_mhs->NMMHSMSMHS.' )',0,'c');

$pdf->ln(15);

$pdf->SetFont('Arial','',8);

$pdf->Cell(11,0,'',0,'L');

$pdf->Cell(200,0,'Menyetujui,',0,1,'L');

$pdf->SetX(105);
$pdf->Cell(110,0,'Menyetujui',0,'L');

//$pdf->SetY(-37);

$pdf->ln(4);


$pdf->Cell(8,0,'',0,'L');
$pdf->Cell(79,0,'Kaprodi/Wadek I',0,'C');
//$info_mhs->NMMHSMSMHS
$pdf->SetX(85);
$pdf->Cell(90,0,'Biro Perencanaan dan Anggaran Keuangan',0,'C');


$pdf->ln(20);

$pdf->SetFont('Arial','',8);

$pdf->Cell(2,0,'',0,'L');

$pdf->Cell(87,0,'(.......................................)',0,'C');

$pdf->SetX(86);
$pdf->Cell(87,0,'(Adelina Suryati, SE, M. Ak., CMA., CBV)',0,'c');


// $pdf->ln(8);

// $pdf->SetFont('Arial','',8);

// $pdf->SetX(63);
// $pdf->Cell(110,0,'Menyetujui,',0,'C');
// $pdf->ln(4);
// $pdf->SetX(60);
// $pdf->Cell(110,0,'Kaprodi/Wadek I,',0,'C');

// $pdf->ln(20);
// $pdf->SetX(55);
// $pdf->Cell(110,0,'(.......................................)',0,'L');


		

$pdf->Output('SEMESTER_PENDEK_'.$tahun.'_'.$info_mhs->NMMHSMSMHS.'.PDF','I');
?>