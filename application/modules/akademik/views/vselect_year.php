<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-file"></i>
        <h3>Jumlah Mahasiswa Reguler</h3>
      </div> 

      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?= base_url(); ?>akademik/amount_stdn/saveses">
            <fieldset>
              
              <div class="control-group">
                <label class="control-label">Tahun Akademik</label>
                <div class="controls">
                  <select class="form-control span6" name="tahunajaran" id="tahunajaran" required>
                    <option disabled selected>--Pilih Tahun Akademik--</option>
                    <?php foreach ($year as $row) { ?>
                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-actions">
                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
              </div> <!-- /form-actions -->
            
            </fieldset>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>