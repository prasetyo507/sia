<?php // var_dump($nm_dosen);die(); ?>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<script>
function edit(id){
$('#absen').load('<?php echo base_url();?>akademik/upload_soal/view_soal/'+id);
}

function stat(id){
$('#status').load('<?php echo base_url();?>akademik/upload_soal/status_soal_uts/'+id);
}

function statuas(id){
$('#statusuas').load('<?php echo base_url();?>akademik/upload_soal/status_soal_uas/'+id);
}

$("#popover").popover({
                html: true,
                animation: false,
                placement: "bottom"
            });
</script>

<?php $user = $this->session->userdata('sess_login');
        $nik = $user['userid'];
        $pecah = explode(',', $user['id_user_group']);
        $jmlh = count($pecah);
        
        for ($i=0; $i < $jmlh; $i++) { 
            $grup[] = $pecah[$i];
        } ?>

<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Data Dokumen Soal Ujian</h3>

            </div> <!-- /widget-header -->
            

            <div class="widget-content">

                <div class="span11">

                    <a href="<?php echo base_url();?>akademik/upload_soal/back" class="btn btn-primary "> << Kembali</a>

                    <hr>

                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 


                                <th>Kode MK</th>

                                <th>Mata Kuliah</th>
								
                                <th>Jumlah Mahasiswa</th>

                                <th>SKS</th>

                                <th>Kelas</th>

                                <th style="text-align:center;" width="150">UTS</th>

                                <th style="text-align:center;" width="150">UAS</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no=1; foreach ($rows as $isi): ?>

                                <tr>
                                    <td><?php echo $isi->kd_matakuliah; ?></td>

                                    <td><?php echo $isi->nama_matakuliah; ?></td>
                                    <td><?php echo $isi->jml; ?></td>
                                    <td><?php echo $isi->sks_matakuliah; ?></td>

                                    <td><?php echo $isi->kelas;?></td>

									<!--------------------------------------------------------------UTS----------------------------------------------------------------------->

									<?php 
										$status 	= $isi->status_uts;
										$disable 	= 'display:none';
										$icon 		= '';
										$bg 		= '';
										if($status == 'Terima') {
											$disable 	= '';
											$icon 		= 'btn-icon-only icon-ok-sign  icon-large';
											$bg 		= 'background-color:#99FF99';
										}elseif($status == 'Revisi') {
											$disable 	= '';
											$icon 		= 'icon-exclamation-sign icon-large';
											$bg 		= 'background-color:#FFFF99';
										}?>
                                    <td data-toggle="tooltip" data-placement="right" title="<?php echo $isi->status_uts; ?> : <?php echo $isi->komentar_uts; ?>" style="<?php echo $bg ?>" class="td-actions">
										
										<center>
											
										<script>
											function ale() {
												return alert('Soal belum diupload');
											}
										</script>
									
											<?php 
												$dok1 = $this->db->query("select * from tbl_dokumen_ujian where userid = '".$nik_dosen."' and kd_jadwal = '".$isi->kd_jadwal."' and tipe = 1")->row(); 
													if ($dok1 == true and $isi->status_uts == null) {
														$url 		= base_url().'upload_soal/'.$dok1->nama_dokumen;
														$dokumen 	= $dok1->nama_dokumen;
														$id 		= $isi->id_jadwal;
														$hi 		= 'display:none';
														$aa 		= '';
														$href 		= "upload_soal/app_uts/$id";
														$al 		= "";
													}elseif ($dok1 == true and $isi->status_uts == 'Revisi') {
														$url 		= base_url().'upload_soal/'.$dok1->nama_dokumen;
														$dokumen 	= $dok1->nama_dokumen;
														$id 		= $isi->id_jadwal;
														$hi 		= '';
														$aa 		= 'disabled="disabled"';
														$href 		= "upload_soal/app_uts/$id";
														$al 		= "";
													}elseif ($dok1 == true and $isi->status_uts == 'Terima') {
														$url 		= base_url().'upload_soal/'.$dok1->nama_dokumen;
														$dokumen 	= $dok1->nama_dokumen;
														$id 		= $isi->id_jadwal;
														$hi 		= 'display:none';
														$aa 		= 'disabled="disabled"';
														$href 		= "#";
														$al 		= "";
													} else {
														$url 		= '#';
														$dokumen 		= 'belum upload';
														$id 		= $isi->id_jadwal;
														$hi 		= '';
														$aa 		= 'disabled="disabled"';
														$href 		= "#";
														$al 		= "return ale();";
													}
											?>  
											<i class="<?php echo $icon ?>" style="color:#4682b4 <?php echo $disable?>" ></i>
											<a  onclick="<?php echo $al; ?>" href="<?php echo $url; ?>"><?php echo $dokumen; ?></a>
											
											<?php  if ((in_array(6, $grup)) ) { ?>
												<br>
												<a data-toggle="modal" style="<?php echo $hi; ?>" onclick="edit(1911<?php echo $isi->id_jadwal; ?>)" href="#myModal" class="btn btn-primary btn-small"><i class="btn-icon-only icon-upload-alt"></i></a>
												
												<?php 
													$appr = $isi->approve_uts;
																$dis='';
																if($appr==1 and $dok1 == true){
																				$dis='display:none';
																			}
													echo '<a href="'.$href.'" class="btn btn-info btn-small" style="'.$dis.'" '.$aa.'><i class="btn-icon-only icon-key" > </i></a>';
												?>
											<?php } ?>
											
											
											<?php  if ((in_array(8, $grup)) ) { ?>
											<br>
											<?php 
											
												$appr 		= $isi->approve_uts;
																$prd='display:none';
																$hil='';
																$link='';
																if($appr==1 and $dok1 == true){
																				$prd='';
																			}
												$status 	= $isi->status_uts;
																if($status == 'Terima'){
																	$hil='disabled="disabled"';
																	$link_uts='#';
																}elseif ($status == 'Revisi'){
																	$hil='';
																	$link_uts='#modalprd';
																}else{
																	$hil='';
																	$link_uts='#modalprd';
																}
											?>
											<a data-toggle="modal" href="<?php echo $link_uts ?>" onclick="stat(<?php echo $isi->id_jadwal; ?>)" class="btn btn-info btn-small" style="<?php echo $prd ?>" <?php echo $hil ?>><i class="btn-icon-only icon-file-text-alt" ></i></a>
											
											<?php } ?>
											
										</center>
                                    </td>

									<!------------------------------------------------------------END UTS--------------------------------------------------------------------->
									
									
									<!--------------------------------------------------------------UAS----------------------------------------------------------------------->
									<?php 
										$status 	= $isi->status_uas;
										$disable 	= 'display:none';
										$icon 		= '';
										$bg 		= '';
										if($status == 'Terima') {
											$disable 	= '';
											$icon 		= 'btn-icon-only icon-ok-sign icon-large';
											$bg 		= 'background-color:#99FF99';
										}elseif($status == 'Revisi') {
											$disable 	= '';
											$icon 		= 'icon-exclamation-sign icon-large';
											$bg 		= 'background-color:#FFFF99';
										}
									?>
									
                                    <td data-toggle="tooltip" data-placement="right" title="<?php echo $isi->status_uas; ?> : <?php echo $isi->komentar_uas; ?>" style="<?php echo $bg ?>" class="td-actions">
									
										<center>
										
											<?php 
												$dok2 = $this->db->query("select * from tbl_dokumen_ujian where userid = '".$nik_dosen."' and kd_jadwal = '".$isi->kd_jadwal."' and tipe = 2")->row(); 
													if ($dok2 == true and $isi->status_uas == null) {
														$url 		= base_url().'upload_soal/'.$dok2->nama_dokumen;
														$dokumen 	= $dok2->nama_dokumen;
														$id 		= $isi->id_jadwal;
														$hi 		= 'display:none';
														$aa 		= '';
														$href 		= "upload_soal/app_uas/$id";
														$al 		= "";
													}elseif ($dok2 == true and $isi->status_uas == 'Revisi') {
														$url		= base_url().'upload_soal/'.$dok2->nama_dokumen;
														$dokumen 	= $dok2->nama_dokumen;
														$id 		= $isi->id_jadwal;
														$hi 		= '';
														$aa 		= '';
														$href 		= "upload_soal/app_uas/$id";
														$al 		= "";
													}elseif ($dok2 == true and $isi->status_uas == 'Terima') {
														$url 		= base_url().'upload_soal/'.$dok2->nama_dokumen;
														$dokumen 	= $dok2->nama_dokumen;
														$id 		= $isi->id_jadwal;
														$hi 		= 'display:none';
														$aa 		= 'disabled="disabled"';
														$href 		= "#";
														$al 		= "";
													} else {
														$url 		= '#';
														$dokumen 	= 'belum upload';
														$id 		= $isi->id_jadwal;
														$hi 		= '';
														$aa 		= 'disabled="disabled"';
														$href 		= "#";
														$al 		= "return ale();";
													}
											 ?>
											 
											<i class="<?php echo $icon ?>" style=" color:#4682b4 <?php echo $disable?>" ></i> 
											 <a onclick="<?php echo $al; ?>" href="<?php echo $url; ?>"><?php echo $dokumen; ?></a> 
											 
											<?php  if ((in_array(6, $grup)) ) { ?>
												<br>
												<a data-toggle="modal" style="<?php echo $hi; ?>" onclick="edit(2911<?php echo $isi->id_jadwal; ?>)"  style="<?php echo $hi; ?>" href="#myModal" class="btn btn-success btn-small"><i class="btn-icon-only icon-upload-alt"> </i></a>
												
												<?php 
													$appr2 = $isi->approve_uas;
																$id = $isi->id_jadwal;
																$dis='';
																if($appr2==1 and $dok2 == true){
																				$dis='display:none';
																			}
													echo '<a href="'.$href.'" class="btn btn-info btn-small" style="'.$dis.'" '.$aa.'><i class="btn-icon-only icon-key"> </i></a>';
												?>
											<?php } ?>
											
											<?php  if ((in_array(8, $grup)) ) { ?>
											<br>
												<?php 
													$appr2 = $isi->approve_uas;
																	$prd='display:none';
																	$hil='';
																	$link='';
																	if($appr2==1 and $dok2 == true){
																					$prd='';
																				}
													$status = $isi->status_uas;
																	if($status == 'Terima'){
																		$hil='disabled="disabled"';
																		$link='#';
																	}elseif ($status == 'Revisi'){
																		$hil='';
																		$link='#modalprduas';
																	}else{
																		$hil='';
																		$link='#modalprduas';
																	}
												?>
												<a data-toggle="modal" href="<?php echo $link ?>" onclick="statuas(<?php echo $isi->id_jadwal; ?>)" class="btn btn-info btn-small" style="<?php echo $prd ?>" <?php echo $hil ?>><i class="btn-icon-only icon-file-text-alt" ></i></a>
											<?php } ?>
										</center>
										
                                    </td>

									<!------------------------------------------------------------END UAS--------------------------------------------------------------------->
                                </tr>    

                            <?php $no++; endforeach ?>

                            

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-sm">

        <div class="modal-content" id="absen">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="modalprd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-sm">

        <div class="modal-content" id="status">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="modalprduas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-sm">

        <div class="modal-content" id="statusuas">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->