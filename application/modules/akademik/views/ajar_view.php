<?php // var_dump($nm_dosen);die(); ?>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<script>
function edit(id){
$('#absen').load('<?php echo base_url();?>akademik/ajar/view_absen/'+id);
}
</script>

<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Data Kegiatan Mengajar</h3>

            </div> <!-- /widget-header -->

            

            <div class="widget-content">

                <div class="span11">
                    
                   <!--  <?php
                    //$user = $this->session->userdata('sess_login');
        
                    //$pecah = explode(',', $user['id_user_group']);
                    //$jmlh = count($pecah);
                    
                    //for ($i=0; $i < $jmlh; $i++) { 
                    //    $grup[] = $pecah[$i];
                    //}

                    //if ((in_array(1, $grup) && (in_array(8, $grup)) {
                       //echo '<a href="<?php echo base_url();?>akademik/ajar/load_list_dosen" class="btn btn-primary "> << Kembali</a><hr>';
                    //} ?> -->

                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 


                                <th>Kode MK</th>

                                <th>Mata Kuliah</th>

                                <th>SKS</th>

                                <th>Hari/Ruang</th>

                                <th>Waktu</th>

                                <th>Kelas</th>

                                <th width="80">Jumlah Peserta</th>

                                <th width="40">Absen Dosen</th>

                                <th width="80">Lihat</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no=1; foreach ($rows as $isi): ?>

                                <tr>

                                    <td><?php echo $isi->kd_matakuliah; ?></td>

                                    <td><?php echo $isi->nama_matakuliah; ?></td>

                                    <td><?php echo $isi->sks_matakuliah; ?></td>

                                    <td><?php echo notohari($isi->hari); ?></td>

                                    <td><?php echo del_ms($isi->waktu_mulai).' - '.del_ms($isi->waktu_selesai); ?></td>

                                    <td><?php echo $isi->kelas;?></td>

                                    <?php $kode = $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$isi->id_jadwal,'id_jadwal','asc')->row(); 
                                          if ($isi->gabung > 0) {
                                              $anak = $this->db->query('select count(distinct npm_mahasiswa) as jumlah from tbl_krs where kd_jadwal = "'.$isi->kd_jadwal.'" or kd_jadwal = "'.$isi->referensi.'"')->row(); 
                                          } else {
                                              $anak = $this->db->query("select count(npm_mahasiswa) as jumlah from tbl_krs where kd_jadwal = '".$kode->kd_jadwal."' and npm_mahasiswa in (
                                                select npm_mahasiswa from tbl_sinkronisasi_renkeu where  tahunajaran = '".$this->session->userdata('tahunajaran')."'
                                              ) ")->row(); 
                                          }
                                    ?>

                                    <td><?php echo $anak->jumlah; ?></td>

                                    <td>
                                        <a href="<?php echo base_url('akademik/absenDosen/event/'.$isi->id_jadwal); ?>" target="_blank" class="btn btn-info btn-small" title="Berita Acara"><i class="icon icon-file"></i></a>
                                    </td>

                                    <td class="td-actions">

                                        <!-- <a data-toggle="modal" onclick="edit(<?php //echo $isi->id_jadwal;?>)" href="#myModal" class="btn btn-success btn-small"><i class="btn-icon-only icon-ok"> </i></a> -->

                                        <a href="<?php echo base_url();?>akademik/ajar/cetak_absensi/<?php echo $isi->id_jadwal; ?>" target='_blank' class="btn btn-primary btn-small"><i class="btn-icon-only icon-print"> </i></a>

                                        <?php if ($log['id_user_group'] == 8 OR $log['id_user_group'] == 19) { ?>
                                            
                                            <a href="<?= base_url('akademik/ajar/cetak_absensi_polos/').$isi->id_jadwal;?>" 
                                                target='_blank' class="btn btn-warning btn-small"><i class="btn-icon-only icon-print"> </i></a>

                                        <?php } ?>

                                        <!-- <a href="<?php //echo base_url(); ?>akademik/ajar/dwld_excel/<?php //echo $isi->id_jadwal; ?>" target='_blank' class="btn btn-success btn-small"><i class="btn-icon-only icon-download"> </i></a> -->


                                    </td>

                                </tr>    

                            <?php $no++; endforeach ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="absen">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->