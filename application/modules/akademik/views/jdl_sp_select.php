<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Jadwal Perkuliahan Kelas Perbaikan</h3>
        </div> <!-- /widget-header -->
      <div class="widget-content">
        <div class="span11">
        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>akademik/jadwal_sp/save_session">
              <fieldset>
            <script>
                    $(document).ready(function(){
                      $('#faks').change(function(){
                        $.post('<?php echo base_url()?>perkuliahan/jadwal/get_jurusan/'+$(this).val(),{},function(get){
                          $('#jurs').html(get);
                        });
                      });
                    });
                    </script>
                    <div class="control-group">
                      <label class="control-label">Fakultas</label>
                      <div class="controls">
                        <select class="form-control span6" name="fakultas" id="faks">
                          <option>--Pilih Fakultas--</option>
                          <?php foreach ($fakultas as $row) { ?>
                          <option value="<?php echo $row->kd_fakultas.'-'.$row->fakultas;?>"><?php echo $row->fakultas;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Jurusan</label>
                      <div class="controls">
                        <select class="form-control span6" name="jurusan" id="jurs">
                          <option>--Pilih Jurusan--</option>
                        </select>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Tahun Akademik</label>
                      <div class="controls">
                        <select class="form-control span6" name="tahunajaran" id="tahunajaran">
                          <option>--Pilih Tahun Akademik--</option>
                          <?php foreach ($select as $key) {
                                      $ta = substr($key->kode, -1);
                                      
                                      if ($ta == '1') {
                                        $ta = substr($key->kode, 0,-1);
                                        $ta_sp = $ta.'3';
                                        echo '<option value="'.$ta_sp.'">'.substr($key->tahun_akademik,0,9).' Kelas Perbaikan - Ganjil </option>';
                                      }elseif($ta == '2'){
                                        $ta = substr($key->kode, 0,-1);
                                        $ta_sp = $ta.'4';
                                        echo '<option value="'.$ta_sp.'">'.substr($key->tahun_akademik,0,9).' Kelas Perbaikan - Genap </option>';
                                      }
                                      //$ta_sp_old = $ta_sp;
                                    } 

                                    ?>
                          <?php 
                          // foreach ($tahunajar as $key) {
                          //   $ta = substr($key->kode, -1);
                          //   if ($ta == '1') {
                          //     $ta = substr($key->kode, 0,-1);
                          //     $ta_sp = $ta.'3';
                          //     echo '<option value="'.$ta_sp.'">'.substr($key->tahun_akademik,0,9).' Semester Perbaikan - '.$ta_sp.'</option>';
                          //   }elseif($ta == '2'){
                          //     $ta = substr($key->kode, 0,-1);
                          //     $ta_sp = $ta.'4';
                          //     echo '<option value="'.$ta_sp.'">'.substr($key->tahun_akademik,0,9).' Semester Perbaikan - '.$ta_sp.'</option>';
                          //   }
                          // } 
                          ?>
                        </select>
                      </div>
                    </div>  
                  <br/>
                  <div class="form-actions">
                      <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                  </div> <!-- /form-actions -->
              </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
