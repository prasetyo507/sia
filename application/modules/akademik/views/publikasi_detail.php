<?php // var_dump($rows);die(); ?>
<script>
function uts(id){
//alert('kaskus UTS');
$('#absen').load('<?php echo base_url();?>akademik/publikasi/show_nilai_uts/'+id);
}

function uas(id){
//alert('kaskus');
	$('#absen').load('<?php echo base_url();?>akademik/publikasi/show_nilai_uas/'+id);
}

function all(id){
	$('#absen').load('<?php echo base_url();?>akademik/publikasi/pub_all/'+id);
}

function jumlahpeserta(id)
{
	$.post('<?= base_url('akademik/publikasi/jumlahpeserta/') ?>'+id, function(respon){
		$('#amount').modal('show');
		var parse = JSON.parse(respon);
		$('#showhere').text(parse.jum+' peserta');
		$('#titles').text(parse.kd_matakuliah);
	});
}

</script>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">

	<div class="span12">      		  		

  		<div class="widget">

  			<div class="widget-header">

  				<i class="icon-user"></i>

  				<h3>Data Publikasi Nilai</h3>

			</div> <!-- /widget-header -->

			

			<div class="widget-content">

				<div class="span11">

					<a href="<?php echo base_url(); ?>akademik/publikasi" class="btn btn-success"><< Kembali</a>

					<hr>

					<table id="example8" class="table table-bordered table-striped">

	                	<thead>

	                        <tr> 

	                        	<th>No</th>

	                        	<th>Kelas</th>

	                        	<th>Kode Matakuliah</th>

	                        	<th>Mata Kuliah</th>

	                        	<th>SKS</th>

	                        	<th>Dosen</th>

	                        	<!-- <th>Peserta</th> -->

	                        	<th width="40">Publikasi</th> 

	                        </tr>

	                    </thead>

	                    <tbody>

	                    	<?php  $no=1;
	                    	foreach ($rows as $isi) { 
								// $all = $this->db->query('SELECT DISTINCT flag_publikasi,SUM(flag_publikasi) AS itung_publis,COUNT(DISTINCT kd_transaksi_nilai) AS mhs FROM tbl_nilai_detail 
								// 						WHERE`kd_jadwal` LIKE "'.$isi->kd_jadwal.'%"
								// 						AND `nilai` IS NOT NULL AND tipe = 10')->row();
								// if ($all->flag_publikasi == 2) {
								// 	$warna = 'style="background:#7FFFD4;"';
								// } elseif ($all->flag_publikasi == 1) {
									$warna = '';
								// } else {
								// 	$warna = 'style="background:#DC143C;"';
								// }
								
	                    		?>
	                    		<script type="text/javascript">
	                    			$.post('<?= base_url('akademik/publikasi/statuspublish/'.$isi->id_jadwal) ?>', function(res){
	                    				if (res) {
	                    					$('#stst-<?= $isi->id_jadwal ?>').empty();
	                    					$('#stst-<?= $isi->id_jadwal ?>').append(res);
	                    				}
	                    				
	                    			});

	                    		</script>
	                    	<tr>
	                    		<td <?php echo $warna; ?>><?php echo $no; ?></td>
	                    		<td <?php echo $warna; ?>><?php echo $isi->kelas; ?></td>
	                    		<td <?php echo $warna; ?>><?php echo $isi->kd_matakuliah; ?></td>
	                    		<td <?php echo $warna; ?>><?php echo $isi->nama_matakuliah; ?></td>
	                    		<td <?php echo $warna; ?>><?php echo $isi->sks_matakuliah; ?></td>
	                    		<td <?php echo $warna; ?>><?php echo $isi->nama; ?></td>
	                    		                   		
	                    		<?php                				
                				
                				$logged = $this->session->userdata('sess_login');
								$pecah = explode(',', $logged['id_user_group']);
								$jmlh = count($pecah);
								for ($i=0; $i < $jmlh; $i++) { 
									$grup[] = $pecah[$i];
								}
								if ( (in_array(8, $grup) or in_array(19, $grup)) and (count($aktif) != 0 ) and ($crud->create > 0)) { ?>
                					<td class="action" id="stst-<?= $isi->id_jadwal ?>">
                						<img src="<?= base_url('assets/img/loader.gif') ?>" style="width: 50%">
                					</td>
                				<?php } else { ?>
                					<td class="action">
		                    			-
		                    		</td>
                				<?php } ?>
                				
	                    	</tr>	

	                    	<?php $no++; } ?>
	                    	
	                    </tbody>

	               	</table>

				</div>

			</div>

		</div>

	</div>

</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="absen">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="amount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="">

            <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title" id="titles"></h4>
		    </div>
		    <div class="modal-body">
		        <h1 id="showhere"></h1>
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    </div>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->