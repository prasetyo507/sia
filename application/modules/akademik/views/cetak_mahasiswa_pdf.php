<?php

$pdf = new FPDF("P","mm", "A4");
$pdf->AliasNbPages();
$pdf->AddPage();


$pdf->SetMargins(3, 3 ,0);
$pdf->SetFont('Arial','B',10); 

//$pdf->Image(''.base_url().'assets/img/logo-albino.png',60,30,90);
$pdf->Ln(0);
$pdf->Cell(200,5,'TEKNIK INFORMATIKA',0,0,'L');
$pdf->Ln(4);
$pdf->SetFont('Arial','',10); 
$pdf->Cell(200,5,'FAKULTAS TEKNIK - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,0,'L');
$pdf->Ln(4);

$pdf->SetFont('Arial','',6);
$pdf->Cell(13,3,'KAMPUS I',0,0,'L');
$pdf->Cell(3,3,' : ',0,0,'L');
$pdf->Cell(140,3,'Jl. Dharmawangsa III No.1,Kebayoran Baru, Jakarta Selatan ',0,0,'L');
$pdf->Ln(2);

$pdf->SetFont('Arial','',6);
$pdf->Cell(13,3,'KAMPUS II',0,0,'L');
$pdf->Cell(3,3,' : ',0,0,'L');
$pdf->Cell(140,3,'Jl. Raya Perjuangan, Bekasi Barat',0,0,'L');
$pdf->Ln(4);
$pdf->Cell(200,0,'',1,0,'C');

//end header

$pdf->SetLeftMargin(10);
$pdf->ln(4);
$pdf->SetFont('Arial','',8);
$pdf->Cell(30,8,'FAKULTAS',1,0,'L');
$pdf->Cell(65,8,$lah->fakultas,1,0,'L');
$pdf->Cell(30,8,'SEMESTER',1,0,'L');
$pdf->Cell(65,8,$lah->semester_matakuliah,1,0,'L');
$pdf->ln(8);
$pdf->SetFont('Arial','',8);
$pdf->Cell(30,8,'PRODI',1,0,'L');
$pdf->Cell(65,8,$lah->prodi,1,0,'L');
$pdf->Cell(30,8,'TAHUN AKADEMIK',1,0,'L');
$pdf->Cell(65,8,'',1,0,'L');
$pdf->ln(8);
$pdf->SetFont('Arial','',8);
$pdf->Cell(95,8,'MATA KULIAH',1,0,'C');
//$pdf->Cell(65,8,'',1,0,'L');
$pdf->Cell(95,8,'DOSEN',1,0,'C');
//$pdf->Cell(65,8,'',1,0,'L');	
$pdf->ln(8);
$pdf->SetFont('Arial','',8);
$pdf->Cell(30,8,'KODE MK',1,0,'L');
$pdf->Cell(65,8,$lah->kd_matakuliah,1,0,'L');
$pdf->Cell(30,8,'NAMA',1,0,'L');
$pdf->Cell(65,8,'',1,0,'L');

$pdf->ln(8);
$pdf->SetFont('Arial','',8);
$pdf->Cell(30,8,'NAMA MK',1,0,'L');
$pdf->Cell(65,8,$lah->nama_matakuliah,1,0,'L');
$pdf->Cell(30,8,'NIDN',1,0,'L');
$pdf->Cell(65,8,'',1,0,'L');

$pdf->ln(8);
$pdf->SetFont('Arial','',8);
$pdf->Cell(30,8,'KELAS',1,0,'L');
$pdf->Cell(65,8,'',1,0,'L');
$pdf->Cell(30,8,'',1,0,'L',1);
$pdf->Cell(65,8,'',1,0,'L',1);

$pdf->ln(8);
$pdf->SetFont('Arial','',8);
$pdf->Cell(30,8,'KAMPUS',1,0,'L');
$pdf->Cell(65,8,$lah->kode_lembaga,1,0,'L');
$pdf->Cell(30,8,'JUMLAH PESERTA',1,0,'L');
$pdf->Cell(65,8,'',1,0,'L');


$pdf->ln(10);

$pdf->SetFont('Arial','',12); 
$pdf->Cell(190,8,'DAFTAR PESERTA KULIAH',1,0,'C');
$pdf->ln(8);

$pdf->SetFont('Arial','',8);
$pdf->Cell(10,5,'NO','L,T,R,B',0,'C');
$pdf->Cell(50,5,'NPM','L,T,R,B',0,'C');
$pdf->Cell(100,5,'NAMA','L,T,R,B',0,'C');
$pdf->Cell(30,5,'STATUS','L,T,R,B',0,'C');
$pdf->ln(5);

$no=1;
foreach ($www as $row) {
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(10,10,$no,'L,T,R,B',0,'C');
	$pdf->Cell(50,10,$row->npm_mahasiswa,'L,T,R,B',0,'C');
	$pdf->Cell(100,10,$row->NMMHSMSMHS,'L,T,R,B',0,'C');
	$pdf->Cell(30,10,'LOOP','L,T,R,B',0,'C');
	$pdf->ln(5);

	$no++;
}


$pdf->Output('ABSENSI.PDF','I');


?>


