<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_bimbingan.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border='1'>
    <thead>
        <tr> 
            <th>No</th>
            <th>NIM</th>
            <th>Mahasiswa</th>
            <th>Angkatan</th>
            <th>Semester KRS</th>
            <!-- <th>SKS</th> -->
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; foreach ($getData as $row) { ?>
        <tr>
            <td><?php echo number_format($no); ?></td>
            <td><?php echo $row->NIMHSMSMHS; ?></td>
            <td><?php echo $row->NMMHSMSMHS; ?></td>
            <td><?php echo $row->TAHUNMSMHS; ?></td>
            <!-- <td><?php //echo $semester->semester_krs; ?></td> -->
            <td>
                <?php 
                    $semester = $this->db->query("SELECT distinct semester_krs from tbl_krs where npm_mahasiswa = '".$row->NIMHSMSMHS."' ")->result();
                    foreach ($semester as $key) {
                        for ($i=0; $i <= count($key); $i++) { 
                            if ($i == count($key)) {
                                echo $key->semester_krs;
                            } else {
                                echo $key->semester_krs.', ';
                            }
                        } 
                    }
                ?>
            </td>
            <?php 
            // $q = $this->db->query("select distinct kd_matakuliah from tbl_krs where kd_krs = '".$row->kd_krs."'")->result();
            // $sumsks = 0;
            // foreach ($q as $key) {
            //     $sksmk = $this->db->query("select distinct sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$key->kd_matakuliah."'")->row()->sks_matakuliah; 
            //     $sumsks = $sumsks + $sksmk;
            // }
            ?>        
            <!-- <td><?php echo $sumsks; ?></td> -->
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>