<div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Detail Nilai Mata Kuliah <?php echo $mk->nama_matakuliah ; ?> - (<?php echo $mk->kd_matakuliah ; ?>)</h4>

            </div>

            <form class ='form-horizontal' action="#" method="post">

                <div class="modal-body">    

                    <table id="example2" class="table table-bordered table-striped">

	                  <thead>

	                        <tr> 

	                          <th>TUGAS</th>

	                          <th>PERSENTASI ABSEN</th>

	                          <th>UTS</th>

							  <th>UAS</th>

	                        </tr>

	                    </thead>

	                    <tbody>

	                    	<tr>
	                    	<?php foreach ($nilai as $key) { ?>
	                    		<td><?php echo $key->nilai; ?></td>
	                    	<?php } ?>
	                    	</tr>

	                    </tbody>

	                </table>

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>

            </form>