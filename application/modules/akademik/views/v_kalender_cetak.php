<?php

//var_dump($detail_khs);exit();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();


$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Times','',11); 

//$pdf->image('http://172.16.2.42:801/assets/logo.gif',8,8,16);


$pdf->Cell(50,4,'UNIVERSITAS BHAYANGKARA ',0,1,'C');
$pdf->Cell(62,4,'JAKARTA RAYA','B',0,'C');

$pdf->SetFont('Times','',10); 
$pdf->Cell(60,4,'',0,0,'C');
$pdf->Cell(23,4,'LAMPIRAN B','',0,'L');
$pdf->Cell(2,4,':','',0,'C');
$pdf->Cell(42,4,'SURAT KEPUTUSAN REKTOR UBJ','',1,'L');

$pdf->Cell(122,4,'',0,0,'C');
$pdf->Cell(23,4,'NOMOR','',0,'L');
$pdf->Cell(2,4,':','',0,'C');
$pdf->Cell(42,4,'SURAT KEPUTUSAN REKTOR UBJ','',1,'L');

$pdf->Cell(122,4,'',0,0,'C');
$pdf->Cell(23,4,'TANGGAL','',0,'L');
$pdf->Cell(2,4,':','',0,'C');
$pdf->Cell(42,4,'SURAT KEPUTUSAN REKTOR UBJ','',1,'L');

$pdf->ln(10);
$pdf->SetFont('Times','B',13); 
$pdf->Cell(200,4,'JADWAL KALENDER AKADEMIK',0,1,'C');
$pdf->Cell(200,4,'UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,1,'C');
$pdf->Cell(200,4,'SEMESTER GANJIL TAHUN AKADEMIK 2016/2017',0,1,'C');

//TABLE 1
$pdf->ln(4);
$pdf->SetFont('Times','B',10);
$pdf->Cell(10,10,'NO',1,0,'C');
$pdf->Cell(90,10,'KEGIATAN PRA SEMESTER',1,0,'C');
$pdf->Cell(65,10,'TANGGAL PELAKSANAAN',1,0,'C');
$pdf->Cell(40,10,'KETERANGAN',1,1,'C');

$pdf->SetFont('Times','',10);
$pdf->Cell(10,7,'NO',1,0,'C');
$pdf->Cell(90,7,'KEGIATAN PRA SEMESTER',1,0,'C');
$pdf->Cell(65,7,'TANGGAL PELAKSANAAN',1,0,'C');
$pdf->Cell(40,7,'KETERANGAN',1,1,'C');

//TABLE 2
$pdf->ln(4);
$pdf->SetFont('Times','B',10);
$pdf->Cell(10,10,'NO',1,0,'C');
$pdf->Cell(90,10,'KEGIATAN SEMESTER GENAP',1,0,'C');
$pdf->Cell(65,10,'TANGGAL PELAKSANAAN',1,0,'C');
$pdf->Cell(40,10,'KETERANGAN',1,1,'C');

$pdf->SetFont('Times','',10);
$pdf->Cell(10,7,'NO',1,0,'C');
$pdf->Cell(90,7,'KEGIATAN PRA SEMESTER',1,0,'C');
$pdf->Cell(65,7,'TANGGAL PELAKSANAAN',1,0,'C');
$pdf->Cell(40,7,'KETERANGAN',1,1,'C');

//TABLE 3
$pdf->ln(4);
$pdf->SetFont('Times','B',10);
$pdf->Cell(10,10,'NO',1,0,'C');
$pdf->Cell(90,10,'KEGIATAN PEMBAYARAN KULIAH SEMESTER',1,0,'C');
$pdf->Cell(65,10,'TANGGAL PELAKSANAAN',1,0,'C');
$pdf->Cell(40,10,'KETERANGAN',1,1,'C');

$pdf->SetFont('Times','',10);
$pdf->Cell(10,7,'NO',1,0,'C');
$pdf->Cell(90,7,'KEGIATAN PRA SEMESTER GENAP',1,0,'C');
$pdf->Cell(65,7,'TANGGAL PELAKSANAAN',1,0,'C');
$pdf->Cell(40,7,'KETERANGAN',1,0,'C');

//$pdf->Cell(200,4,'FORMULIR PENDAFTARAN CALON MAHASISWA BARU',1,0,'C',1);


$pdf->Output('FORMULIR.PDF','I');



?>

