<script type="text/javascript">
function loadjdl(kd) {
        $("#kontenmodal").load('<?php echo base_url()?>akademik/perbaikan/get_jdl/'+kd);
    }
</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Daftar Matakuliah Perbaikan</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url('akademik/perbaikan/back'); ?>" class="btn btn-warning" title=""><i class="icon icon-arrow-left"></i> Kembali</a>
					<hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Kode Matakuliah</th>
	                        	<th>Nama Matakuliah</th>
	                        	<th>SKS</th>
	                        	<th>Semester</th>
	                        	<th>Nilai</th>
	                            <th width="80">Pilih Jadwal</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($query as $row) { ?>
	                        <tr>
	                        	<td><?php echo number_format($no); ?></td>
                                <td><?php echo $row->kd_matakuliah; ?></td>
                                <td><?php echo $row->nama_matakuliah; ?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
                                <td><?php echo $row->semester_matakuliah; ?></td>
                                <td><?php echo $row->NLAKHTRLNM; ?></td>
                                <?php $expl = $verif->tahunajaran.'.'.$verif->kd_jurusan.'.'.$row->kd_matakuliah ?>
	                        	<td align="center">
									<a class="btn btn-success btn-small" href="#jadwal" onclick="loadjdl('<?php echo $expl; ?>')" data-toggle="modal"><i class="btn-icon-only icon-reorder"> </i></a>
								</td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="jadwal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="kontenmodal">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->