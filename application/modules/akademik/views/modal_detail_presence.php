<?php
	$kdmk = get_dtl_jadwal($kania->row()->kd_jadwal)['kd_matakuliah'];
	$kels = get_dtl_jadwal($kania->row()->kd_jadwal)['kelas'];
	$nmmk = get_nama_mk($kdmk,substr($kania->row()->kd_jadwal, 0,5));
	$keyy =	md5($kania->row()->kd_jadwal.$kania->row()->pertemuan);
 ?>


<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?php echo $nmmk.' - '.$kels.' - pertemuan '.$kania->row()->pertemuan ?></h4>
</div>

<div class="modal-body">
    <table class="table table-stripped">
    	<thead>
    		<tr>
    			<th>No</th>
    			<th>Nama</th>
    			<th>Status Kehadiran</th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php if (count($kania->result()) > 0) { ?>
    			
    			<?php $no = 1; foreach ($kania->result() as $key) { ?>
	    			<tr>
		    			<td><?php echo $no; ?></td>
		    			<td><?php echo get_nm_mhs($key->npm_mahasiswa); ?></td>
		    			<td><?php echo $key->kehadiran; ?></td>
		    		</tr>
	    		<?php $no++; } ?>

    		<?php } else {  ?>
    			<tr>
    				<td colspan="3"><b><i>Tidak Ada Data</i></b></td>
    			</tr>
    		<?php } ?>
    	</tbody>
    </table>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <a href="<?php echo base_url('akademik/absenmhs/validasi/'.$keyy) ?>" class="btn btn-success">Validasi</a>
</div>