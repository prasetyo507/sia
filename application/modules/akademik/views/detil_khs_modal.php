<?php 
    $log = $this->session->userdata('sess_login');
    $usr = $log['userid'];
    $brk = substr($usr, 0,4);
    if ($brk != '2017') { ?>
        <b>IPS terakhir : <?php echo number_format($ipsmhs_before,2); ?></b>
        <br>
        <b>Jumlah SKS &nbsp: <?php echo $jumsks; ?></b>
        <br>
        <b>Semester &nbsp&nbsp&nbsp&nbsp: <?php echo $smt; ?></b>
        <hr>
    <?php } ?>

<table id="example1" class="table table-bordered table-striped">

    <thead>

        <tr> 

            <th>No</th>

            <th>Kode MK</th>

            <th>Mata Kuliah</th>

            <th>SKS</th>

            <th>Nilai</th>

        </tr>

    </thead>

    <tbody>

        <?php $no = 1; foreach ($q as $row) { ?>

        <tr>

            <td><?php echo $no; ?></td>

            <td><?php echo $row->kd_matakuliah; ?></td>

            <td><?php echo $row->nama_matakuliah; ?></td>

            <td><?php echo $row->sks_matakuliah ?></td>

            <?php $nilai = $this->db->query("SELECT * from tbl_transaksi_nilai where NIMHSTRLNM = '".$mhs->NIMHSMSMHS."' and KDKMKTRLNM = '".$row->kd_matakuliah."' and THSMSTRLNM = '".$tahunakad."' ")->row(); ?>

            <td><?php echo $nilai->NLAKHTRLNM; ?></td>

        </tr>

        <?php $no++; } ?>

    </tbody>

</table>