<!-- autocomplete load jadwal -->
<script type="text/javascript">
    jQuery(document).ready(function($) {

        // for uts
        $('input[name^=watch]').autocomplete({
            source: '<?= base_url('akademik/jadwal_ujian/loaddosen');?>',
            minLength: 3,
            select: function (evt, ui) {
                this.form.watch.value = ui.item.value;
                this.form.hiddenwatch.value = ui.item.dosen;
            }
        });

        $('input[name^=room]').autocomplete({
            source: '<?= base_url('akademik/jadwal_ujian/loadroom');?>',
            minLength: 1,
            select: function (evt, ui) {
                this.form.room.value = ui.item.value;
                this.form.hiddenrooms.value = ui.item.idroom;
            }
        });

        // for uas
        $('input[name^=uasroom]').autocomplete({
            source: '<?= base_url('akademik/jadwal_ujian/loadroom');?>',
            minLength: 1,
            select: function (evt, ui) {
                this.form.uasroom.value = ui.item.value;
                this.form.uashiddenroom.value = ui.item.idroom;
            }
        });

        $('input[name^=uaswatch]').autocomplete({
            source: '<?= base_url('akademik/jadwal_ujian/loaddosen');?>',
            minLength: 3,
            select: function (evt, ui) {
                this.form.uaswatch.value = ui.item.value;
                this.form.uashiddenwatch.value = ui.item.dosen;
            }
        });

    });

    // for uts
    $(document).ready(function(){
        $( "#tgl" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2015:<?= date('Y'); ?>',
            dateFormat: 'yy-mm-dd'
        });
    });

    // for uas
    $(document).ready(function(){
        $( "#uastgl" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2015:<?= date('Y')+1; ?>',
            dateFormat: 'yy-mm-dd'
        });
    });

    $(document).ready(function(){
        $("#timepickert").timepicker();
        $("#timepickera").timepicker();
    });

</script>

<!-- ajax input data -->
<script type="text/javascript">
    $(function() {
        $('#utsedt').on('submit', function (e) {
            // ajax input data
            var uts = $('#utsedt').serialize();
            $.ajax({
                type: 'POST',
                url: "<?= base_url('akademik/jadwal_ujian/changeDataUts') ?>",
                data: uts
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $('#uasedt').on('submit', function (e) {
            // ajax input data
            var uas = $('#uasedt').serialize();
            $.ajax({
                type: 'POST',
                url: "<?= base_url('akademik/jadwal_ujian/changeDataUas') ?>",
                data: uas
            });
        });
    });
</script>

<script type="text/javascript">
    function removal(id) {
        $('#'+id).val('');
    }
</script>

<div class="modal-header">

    <button type="button" class="close" data-dismiss="modal">&times;</button>

    <h4 class="modal-title">Rubah Jadwal Ujian</h4>

</div>

<div class="modal-body">

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Jadwal UTS</a></li>
        <li><a data-toggle="tab" href="#menu1">Jadwal UAS</a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">

            <!-- data uts start-->
            <?php if ((bool)$uts->result()) {
                $kelas  = $uts->row()->kelas;
                $kdmk   = $uts->row()->kd_matakuliah;
                $nmmk   = get_nama_mk($uts->row()->kd_matakuliah,substr($uts->row()->kd_jadwal, 0,5));
                $dosen  = nama_dsn($uts->row()->kd_dosen);
                $tgl    = $uts->row()->start_date;
                $room   = get_room($uts->row()->kd_ruang);
                $watch  = nama_dsn($uts->row()->pengawas);
                $timez  = $uts->row()->start_time; ?>

                <form id="" method="post" action="<?= base_url('akademik/jadwal_ujian/changeDataUts') ?>">

                    <input type="hidden" name="id" value="<?= $uts->row()->id_jadwal; ?>">

                    <input type="hidden" name="typ" value="<?= $uts->row()->tipe_uji; ?>">

                    <input type="hidden" name="prodi" value="<?= $uts->row()->prodi; ?>">

                    <div class="control-group">
                        <label class="control-label">Nama Kelas</label>
                        <div class="controls">
                            <input class="form-control span5" value="<?= $kelas; ?>" disabled=""/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Mata Kuliah</label>
                        <div class="controls">
                            <input class="form-control span5" value="<?= $kdmk.' - '.$nmmk; ?>" disabled=""/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Dosen</label>
                        <div class="controls">
                            <input class="form-control span5" value="<?= $dosen; ?>" disabled=""/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Tanggal UTS</label>
                        <div class="controls">
                            <input class="form-control span5" id="tgl" onfocus="removal('tgl')" value="<?= $tgl; ?>" name="tgl" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Ruangan</label>
                        <div class="controls">
                            <input class="form-control span5" value="<?= $room; ?>" onfocus="removal('rg')" id="rg" name="room" />
                            <input type="hidden" class="form-control span5" value="<?= $uts->row()->kd_ruang; ?>" name="hiddenrooms" />
                        </div>
                    </div>

                    <!-- <div class="control-group">
                        <label class="control-label">Pengawas</label>
                        <div class="controls">
                            <input class="form-control span5" value="<?= $watch; ?>" onfocus="removal('wt')" id="wt" name="watch" />
                            <input type="hidden" class="form-control span5" value="<?= $uts->row()->pengawas; ?>" name="hiddenwatch" />
                        </div>
                    </div> -->

                    <div class="control-group">
                        <label class="control-label">Jam Mulai</label>
                        <div class="controls">
                            <input class="form-control span5" id="timepickert" value="<?= $timez; ?>" placeholder="Jam mulai ujian" name="starttimeuts" />
                        </div>
                    </div>

                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Submit</button>    

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    </div>

                </form>

            <?php } else { ?>

                <h3>Belum ada jadwal.</h3>

            <?php } ?>
            <!-- data uts end -->
            
        </div>

        <div id="menu1" class="tab-pane fade">
            
            <!-- data uas start-->
            <?php if ((bool)$uas->result()) {
                $kelas  = $uas->row()->kelas;
                $kdmk   = $uas->row()->kd_matakuliah;
                $nmmk   = get_nama_mk($uas->row()->kd_matakuliah,substr($uas->row()->kd_jadwal, 0,5));
                $dosen  = nama_dsn($uas->row()->kd_dosen);
                $tgl    = $uas->row()->start_date;
                $room   = get_room($uas->row()->kd_ruang);
                $watch  = nama_dsn($uas->row()->pengawas);
                $timec  = $uas->row()->start_time; ?>

                <form id="" method="post" action="<?= base_url('akademik/jadwal_ujian/changeDataUas') ?>">
                    
                    <input type="hidden" name="uasid" value="<?= $uas->row()->id_jadwal; ?>">

                    <input type="hidden" name="uastyp" value="<?= $uas->row()->tipe_uji; ?>">

                    <input type="hidden" name="uasprodi" value="<?= $uas->row()->prodi; ?>">

                    <div class="control-group">
                        <label class="control-label">Nama Kelas</label>
                        <div class="controls">
                            <input class="form-control span5" value="<?= $kelas; ?>" disabled=""/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Mata Kuliah</label>
                        <div class="controls">
                            <input class="form-control span5" value="<?= $kdmk.' - '.$nmmk; ?>" disabled=""/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Dosen</label>
                        <div class="controls">
                            <input class="form-control span5" value="<?= $dosen; ?>" disabled=""/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Tanggal UAS</label>
                        <div class="controls">
                            <input class="form-control span5" id="uastgl" onfocus="removal('uastgl')" value="<?= $tgl; ?>" name="uastgl" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Ruangan</label>
                        <div class="controls">
                            <input class="form-control span5" value="<?= $room; ?>" onfocus="removal('uasrg')" id="uasrg" name="uasroom" />
                            <input type="hidden" class="form-control span5" value="<?= $uas->row()->kd_ruang; ?>" name="uashiddenroom" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Jam Mulai</label>
                        <div class="controls">
                            <input class="form-control span5" id="timepickera" value="<?= $timec; ?>" placeholder="Jam mulai ujian" name="starttimeuas" />
                        </div>
                    </div>

                    <!-- <div class="control-group">
                        <label class="control-label">Pengawas</label>
                        <div class="controls">
                            <input class="form-control span5" value="<?= $watch; ?>" onfocus="removal('uaswt')" id="uaswt" name="uaswatch" />
                            <input type="hidden" class="form-control span5" value="<?= $uas->row()->pengawas; ?>" name="uashiddenwatch" />
                        </div>
                    </div> -->

                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Submit</button>    

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    </div>

                </form>

            <?php } else { ?>

                <h3>Belum ada jadwal.</h3>

            <?php } ?>
            <!-- data uas end -->

        </div>
    </div>

</div>