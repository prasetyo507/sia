<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_khs extends CI_Model {


	function khs($id)
	{
		$this->db->select('*');
		$this->db->from('view_transkrip');
		$this->db->where('NIMHSTRLNM', $id);
		return $this->db->get()->result();
	}
	function khs_konversi($id)
	{
		$this->db->select('*');
		$this->db->from('view_transkrip_kvs');
		$this->db->where('NIMHSTRLNM', $id);
		return $this->db->get()->result();
	}

	

}

/* End of file Ajar_model.php */
/* Location: ./application/models/Ajar_model.php */