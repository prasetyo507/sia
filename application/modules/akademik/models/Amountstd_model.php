<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Amountstd_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function getList()
	{
		$this->db->select('kd_prodi, prodi');
		$this->db->from('tbl_jurusan_prodi');
		return $this->db->get();
	}

	function getAmount($uid,$typ,$year)
	{
		$this->db->select('count(npm_mahasiswa) as jumlah');
		$this->db->from('tbl_verifikasi_krs');
		$this->db->where('tahunajaran', $year);
		$this->db->where('kelas', $typ);
		$this->db->where('kd_jurusan', $uid);
		return $this->db->get()->row()->jumlah;
	}

	function getDetailClass($uid,$cls,$yrs)
	{
		$this->db->select('npm_mahasiswa');
		$this->db->from('tbl_verifikasi_krs');
		$this->db->where('kd_jurusan', $uid);
		$this->db->where('kelas', $cls);
		$this->db->where('tahunajaran', $yrs);
		return $this->db->get();
	}
}

/* End of file Amountstd_model.php */
/* Location: ./application/models/Amountstd_model.php */