<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class list_mk_sp extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$data['page'] = '';
		$this->load->view('template', $data);
	}

	function view()
	{
		$user = $this->session->userdata('sess_login');

		$ta = '20153';
		$data['ta']   = $ta;
		$data['rows'] = $this->db->query('SELECT tbl_krs_sp.kd_matakuliah,tbl_matakuliah.nama_matakuliah, COUNT(*) FROM tbl_krs_sp
													JOIN tbl_matakuliah ON tbl_matakuliah.`kd_matakuliah` = tbl_krs_sp.kd_matakuliah
													WHERE kd_krs LIKE CONCAT(npm_mahasiswa,'20153%') AND tbl_matakuliah.`kd_prodi` = 55201
													GROUP BY kd_matakuliah ')->result();

		$data['page'] = 'list_mk_sp';
		$this->load->view('template', $data);
	}

}

/* End of file list_mk_sp.php */
/* Location: ./application/controllers/list_mk_sp.php */