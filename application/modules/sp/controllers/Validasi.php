<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validasi extends CI_Controller {

	function index()
	{
		$data['page'] = 'validasi_select2';
		$this->load->view('template', $data);
	}

	function view()
	{
		$user = $this->session->userdata('sess_login');

		$ta = '20153';
		$data['ta']   = $ta;
		$data['rows'] = $this->db->query('SELECT vsp.npm_mahasiswa,mhs.`NMMHSMSMHS`,SUM(mk.sks_matakuliah) as jml_sks FROM tbl_verifikasi_krs_sp vsp
							LEFT JOIN tbl_krs_sp ksp ON vsp.kd_krs = ksp.kd_krs
							JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = vsp.npm_mahasiswa
							JOIN tbl_matakuliah mk ON mk.`kd_matakuliah` = ksp.kd_matakuliah
							WHERE vsp.tahunajaran = '.$ta.'
							GROUP BY vsp.kd_krs
							')->result();

		//var_dump($data);die();

		$data['page'] = 'validasi';
		$this->load->view('template', $data);
	}

	function simpan(){
		$a = $this->session->userdata('sess_login');
		$b = $a['userid'];

		$jum = count($this->input->post('mhs_bayar', TRUE));
		
		for ($i=0; $i < $jum ; $i++) { 
			$exp = explode('911', $this->input->post('mhs_bayar['.$i.']', TRUE));

			$data['npm_mahasiswa'] = $exp[0];
			$data['briva_mhs'] = '70306'.substr($exp[0], 2);
			$data['status'] = 4;
			$data['transaksi_terakhir'] = date('Y-m-d');
			$data['userid'] = $b;
			$data['tahunajaran'] = $exp[1];

			$this->app_model->insertdata('tbl_sinkronisasi_renkeu_sp',$data);
			echo "<script>alert('Sukses');
				document.location.href='".base_url()."keuangan/status_ujian/load_uji';</script>";

		}
	}

}

/* End of file Validasi.php */
/* Location: ./application/modules/sp/controllers/Validasi.php */