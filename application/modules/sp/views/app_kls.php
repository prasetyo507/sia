            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Form Buka Kelas</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>sp/validasisp/save_open" method="post" enctype="multipart/form-data">

                <div class="modal-body" style="margin-left: -60px;">

                    <div class="control-group" id="">

                        <label class="control-label">Status</label>

                        <div class="controls">

                            <input type="hidden" name="id_jadwal" value="<?php echo $id_jadwal;?>" >

                            <select class="form-control" name="open_kelas" id="open_kelas" required>
                                <option value="1">Buka Kelas</option>
                                <option value="2">Tutup Kelas</option>
                            </select>

                        </div>

                    </div>

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>