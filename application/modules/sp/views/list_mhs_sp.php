<script>
	function edit(idk){
		$('#edit1').load('<?php echo base_url();?>sp/validasisp/app_mhs/'+idk);
	}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Semester Pendek (<?php echo $kd_mk.' - '.$nm_mk;?>)</h3>
			</div>
			
			<div class="widget-content">
				<!-- <div class="span11"> -->
					<!-- <div class="span1.5">
						<a href="<?php echo base_url(); ?>sp/validasisp/list_mk" class="btn btn-info">Kembali</a>
					</div>
					<div class="span1.5">
						<a class="btn btn-success"
						href="#sts" data-toggle="modal">
						Status Kelas</a>
					</div>
					<?php if ($id_jdl != 0): ?>
					<div class="span7">
						<a href="<?php echo base_url(); ?>sp/validasisp/cetak_absensi/<?php echo $kd_mk;?>" class="btn btn-warning"><i class="btn-icon-only icon-print"></i> Cetak Absensi</a>
					</div>
					<?php endif ?> -->
				<!-- </div> -->
				<div class="span11">
				<br>
					<table>
						<?php 
							if ($status->open == 1) {
								$sts = 'KELAS DIBUKA';
							}elseif ($status->open == 2) {
								$sts = 'KELAS DITUTUP';
							}elseif ($status->open == 0) {
								$sts = 'MENUNGGU KONFIRMASI PRODI';
							}
						 ?>
						<tr>
							<td>STATUS KELAS</td>
							<td> : </td>
							<td><?php echo $sts; ?></td>
						</tr>
					</table>					
					<p> * Mahasiswa yg ada pada cetak absensi hanya mahasiswa yang telah melunasi pembayaran semester perbaikan.</p>
					<table id="example99" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>NPM </th>
                                <th>NAMA Mahasiswa</th>
                                <th>Nilai</th>
                                <th>Absensi</th>
                                <th>Status Pembayaran</th>
                                <th>Status Persyaratan</th>
                                <th>Aksi</th>	
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php 
	                    		//OLD
	                    		/*if ($tahunajaran%2 == 1) {
	                    			$yearcode = yearBefore();
	                    		} else {
	                    			$yearcode = getactyear();
	                    		}*/
	                    		//New
	                    		if ($tahunajaran%2 == 1) {
	                    			$yearcode = getactyear();
	                    		} else {
	                    			$yearcode = yearBefore();
	                    		}
	                    	 ?>
							<?php $no = 1; foreach($rows as $isi){
								// OLD
								$query = "SELECT kd_jadwal from tbl_krs where kd_krs 
																like '".$isi->NIMHSMSMHS.$yearcode."%'
																AND kd_matakuliah = '".$kd_mk."'";
																
								$kd_jadwal 	= $this->db->query($query);

								if($kd_jadwal->num_rows() > 0) $kd_jadwal =  $kd_jadwal->row()->kd_jadwal;
								else $kd_jadwal = '';

								$this->db->select('*');
								$this->db->where('NIMHSTRLNM', $isi->NIMHSMSMHS);
								$this->db->where('KDKMKTRLNM', $kd_mk);
								$this->db->like('kd_transaksi_nilai', (string)$kd_jadwal,'after');
								$this->db->order_by('id','desc');
								$nilai_lama=$this->db->get('tbl_transaksi_nilai',1)->row();

								$absendosen = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs_new_20171 
																where kd_jadwal = '".$kd_jadwal."'")->row()->satu;

								$absenmhs 	= $this->db->query("SELECT COUNT(npm_mahasiswa) as dua FROM tbl_absensi_mhs_new_20171 
																where kd_jadwal = '".$kd_jadwal."' 
																AND npm_mahasiswa = '".$isi->NIMHSMSMHS."' 
																AND kehadiran = 'H'")->row()->dua;

								$this->db->select('*');
								$this->db->where('npm_mahasiswa', $isi->NIMHSMSMHS);
								$this->db->where('tahunajaran', $this->session->userdata('ta'));
								$q = $this->db->get('tbl_sinkronisasi_renkeu',1)->result();
								$cq = count($q);

								$fo=$this->db->where('kd_matakuliah',$kd_mk)->where('kd_krs',$isi->kd_krs)->get('tbl_krs_sp')->row()->flag_open;
								?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $isi->NIMHSMSMHS; ?></td>
	                        	<td><?php echo $isi->NMMHSMSMHS; ?></td>
	                        	<td><?php echo $nilai_lama->NLAKHTRLNM ?></td>
	                        	<td>
	                        		<?php 
	                        		$persentase = 0; 

	                        		if ($absenmhs == 0) {
		                        		echo number_format($persentase,2).'%';
	                        		}else{
		                        		$persentase = $absenmhs/$absendosen*100; 
		                        		echo number_format($persentase,2).'%';
	                        		}
	                        		?>

	                        	</td>

	                        	<?php if ($cq == 0) {
	                        		echo '<td>Belum Lunas</td>';
	                        	}elseif ($cq == 1) {
	                        		echo '<td>LUNAS</td>';
	                        	}

	                        	if ($fo == 1) {
	                        	 	echo '<td>Dikeluarkan</td>';
	                        	 	$btn_apv = '<a class="btn btn-danger btn-small" onclick="edit('.$isi->id_krs.')" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-pencil"></i></a>';
	                        	}elseif ($cq == 1) {
	                        	 	echo '<td>Memenuhi Syarat</td>';
	                        	 	$btn_apv = '<a class="btn btn-success btn-small" onclick="edit('.$isi->id_krs.')" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-pencil"></i></a>';
	                        	} else {
	                        		echo '<td>Belum Memenuhi</td>';
	                        		$btn_apv = '<a class="btn btn-warning btn-small" onclick="edit('.$isi->id_krs.')" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-pencil"></i></a>';
	                        	}
	        
	                        	?>
	                        	<td><?php echo $btn_apv ?></td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit1">            

        </div>
    </div>
</div>