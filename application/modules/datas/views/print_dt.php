<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_calon_maba.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>ID registrasi</th>
			<th>NAMA</th>
			<th>Asal Sekolah</th>
			<th>Pilihan</th>
			<th>Gelombang</th>
			<th>Status</th>
			<th>NPM</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach($cmb as $row) { ?>
		<tr>
			<td><?php echo number_format($no); ?></td>
			<td><?php echo $row->ID_registrasi; ?></td>
			<td><?php echo $row->nama; ?></td>
			<td><?php echo $row->asal_skl; ?></td>
			<td><?php echo $row->prodi; ?></td>
			<td><?php echo substr($row->ID_registrasi, 0,1); ?></td>
			<td></td>
			<td></td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>

