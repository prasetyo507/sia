<script type="text/javascript">
    function edit(idj) {
        $("#edit_jurusan").load('<?php echo base_url()?>data/jurusan/view_edit_jurs/'+idj);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Daftar Jumlah Peserta Perbaikan</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <!-- <a data-toggle="modal" href="#tambahModal" class="btn btn-primary"> Tambah Data </a><br><hr> -->
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Kelas</th>
                                <th>Dosen</th>
                                <th>Kode Matakuliah</th>
                                <th>Nama Matakuliah</th>
                                <th>SKS</th>
                                <th>Jumlah Perbaikan</th>
                                <th>Jumlah Pendaftar</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($query as $row){?>
	                        <tr>
	                        	<td><?php echo $no;?></td>
	                        	<td><?php echo $row->kelas;?></td>
	                        	<td><?php echo $row->nama;?></td>
                                <td><?php echo $row->kd_matakuliah;?></td>
                                <td><?php echo $row->nama_matakuliah;?></td>
                                <td><?php echo $row->sks_matakuliah;?></td>
                                <?php $jml = $this->temph_model->jml_remid($thn,$this->session->userdata('userid'),$row->kd_jadwal)->row(); ?>
                                <td><?php echo $jml->jums; ?></td>
                                <?php $reg = $this->temph_model->pendaftar_remid($row->kd_jadwal,substr($thn, 0,4)); //var_dump($reg); ?>
                                <td><?php echo count($reg); ?></td>
	                        	<td class="td-actions">
									<a href="<?php echo base_url('datas/list_remid/detil_pst/'.$thn.'/'.$this->session->userdata('userid').'/'.$row->kd_matakuliah.'/'.$row->id_jadwal); ?>" target="blank" class="btn btn-primary btn-small"><i class="btn-icon-only icon-eye-open"> </i></a>
								</td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- modal edit -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_jurusan">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- modal tambah -->
<div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Jurusan</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>data/jurusan/save_jurusan" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Kode Jurusan</label>
                        <div class="controls">
                            <input type="text" class="span4" name="kd_jur" placeholder="Input Kode" class="form-control" value="" required/>
                        </div>
                    </div>              
                    <div class="control-group" id="">
                        <label class="control-label">Jurusan</label>
                        <div class="controls">
                            <input type="text" class="span4" name="nm_jur" placeholder="Input Jurusan" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Fakultas</label>
                        <div class="controls">
                            <select class="form-control" name='fakuls'>
                            <option>--Pilih Fakultas--</option>
                            <?php foreach ($fakultas as $row) { ?>
                                <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->