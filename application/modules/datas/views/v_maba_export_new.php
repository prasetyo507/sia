<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=DATA_MABA_".$prod.".xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
                    
<table>
	<thead>
        <tr> 
        	<th>No</th>
            <th>Nomor Registrasi</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Asal Sekolah</th>
            <th>Penghasilan</th>
            <th>Agama</th>
            <th>Status</th>
            <th>NPM</th>
            <th>Prodi</th>
            <th width="120">Gelombang</th>
            <th>Telepon</th>
        </tr>
    </thead>
    <tbody>
		<?php $no = 1; foreach($owa as $row){?>
        <tr>
        	<td><?php echo $no; ?></td>
        	<td><?php echo $row->nomor_registrasi; ?></td>
        	<td><?php echo $row->nama; ?></td>
            <td>
                <?php if ($row->kelamin == 'L') {
                    echo 'Laki-laki';
                } else {
                    echo 'Perempuan';
                }
                 ?>
            </td>

            <?php if ($row->jenis_pmb == 'KV') { ?>
                <td><?php echo $row->asal_pts_konversi; ?></td>
            <?php } else { ?>
                <td><?php echo $row->asal_sch_maba; ?></td>
            <?php } ?>
            
            <td>
                <?php if ($row->penghasilan == 1) {
                    echo "Rp 1,000,000 - 2,000,000";
                } elseif($row->penghasilan == 2) {
                    echo "Rp 2,100,000 - 4,000,000";
                } elseif ($row->penghasilan == 3) {
                    echo "Rp 4,100,000 - 5,999,000";
                } elseif ($row->penghasilan == 3) {
                    echo ">= Rp 6,000,000";
                } else {
                    echo "0";
                }
                ?>
            </td>
            <td><?php echo getReligion($row->agama); ?></td>
            <td>
                <?php if ($row->status < 1) {
                    echo "Registrasi";
                } elseif ($row->status == 1) {
                    echo "Lulus Tes";
                } elseif ($row->status == 2) {
                    echo "Tidak Lulus";
                } elseif ($row->status > 2) {
                    echo "Daftar Ulang";
                } ?>
            </td>
            <td><?php echo $row->npm_baru; ?></td>
            <td>
                <?php
                switch ($row->prodi) {
                    case '55201':
                        $nama_prodi = 'Teknik Informatika'; 
                        break;
                    case '25201':
                        $nama_prodi = 'Teknik Lingkungan'; 
                        break;
                    case '26201':
                        $nama_prodi = 'Teknik Industri'; 
                        break;
                    case '24201':
                        $nama_prodi = 'Teknik Kimia'; 
                        break;
                    case '32201':
                        $nama_prodi = 'Teknik Perminyakan'; 
                        break;
                    case '62201':
                        $nama_prodi = 'Akuntansi'; 
                        break;
                    case '61201':
                        $nama_prodi = 'Manajemen'; 
                        break;
                    case '73201':
                        $nama_prodi = 'Psikologi'; 
                        break;
                    case '74201':
                        $nama_prodi = 'Ilmu Hukum'; 
                        break;
                    case '70201':
                        $nama_prodi = 'Ilmu Komunikasi'; 
                        break;
                    case '86206':
                        $nama_prodi = 'PENDIDIKAN GURU SEKOLAH DASAR'; 
                        break;
                    case '85202':
                        $nama_prodi = 'PENDIDIKAN KEPELATIHAN OLAHRAGA'; 
                        break;
                    case '61101':
                        $nama_prodi = 'Magister Manajemen'; 
                        break;
                    case '85202':
                        $nama_prodi = 'Magister Hukum'; 
                        break;
                }
                echo $nama_prodi;
                ?>
            </td>
            <td><?php echo $row->gelombang; ?></td>
            <td><?php echo $row->tlp; ?></td>
        </tr>
		<?php $no++; } ?>
    </tbody>
</table>