<?php
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=export.xls");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr> 
            <th>No</th>
            <th>ID Registrasi</th>
            <th>Nama</th>
            <th>Negeri/Swasta</th>
            <th>Asal Sekolah</th>
            <th>Kelas</th>
            <th>Program Pilihan</th>
            <th>Kampus</th>
            <th>Uang Pendaftaran</th>
        </tr>
    </thead>
    <tbody>
        <?php $uang = 250000; $a=0; $no=1; foreach ($look as $row) { $a = $no;?>
        
        <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $row->nomor_registrasi;?></td>
            <td><?php echo $row->nama;?></td>
            <?php if ($row->kategori_skl == 'ngr') {
                $jadi = 'Negeri';
            } else {
                $jadi = 'Swasta';
            }
             ?>
            <td><?php echo $jadi;?></td>
            <td><?php echo $row->asal_sch_maba; ?></td>
            <?php if ($row->kelas == 'sr') {
                $kls = 'SORE';
            } elseif ($row->kelas == 'ky') {
                $kls = 'KARYAWAN';
            } else {
                $kls = 'PAGI';
            }
             ?>
            <td><?php echo $kls; ?></td>
            <td><?php echo $row->prodi;?></td>
            <?php if ($row->kampus == 'bks') {
                $kps = 'BEKASI';
            } else {
                $kps = 'JAKARTA';
            }
             ?>
            <td><?php echo $kps; ?></td>
            <td><?php echo $uang;?></td>
        </tr>
        <?php $no++; } ?>
        <tr>
            <td colspan="7" >Total</td>
            <td><?php echo $uang*$a; ?></td>
        </tr>
        
    </tbody>
</table>