<script type="text/javascript">
    function edit(idf) {
        $("#edit_fakultas").load('<?php echo base_url()?>data/fakultas/get_list_faks/'+idf);
    }
</script>


<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Fakultas</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a data-toggle="modal" href="#myModal" class="btn btn-primary"> Tambah Data </a><br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Kode</th>
                                <th>Fakultas</th>
	                            <th width="120">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($fakultas->result() as $row){?>
	                        <tr>
	                        	<td><?php echo $no;?></td>
	                        	<td><?php echo $row->kd_fakultas;?></td>
	                        	<td><?php echo $row->fakultas;?></td>
	                        	<td class="td-actions">
									<a onclick="edit(<?php echo $row->id_fakultas;?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
									<a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>data/fakultas/del_fakultas/<?php echo $row->id_fakultas;?>"><i class="btn-icon-only icon-remove"> </i></a>
								</td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- modal tambah fakultas -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Fakultas</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>data/fakultas/save_fakultas" method="post">
                <div class="modal-body" style="margin-left:-20px;">    
                    <div class="control-group" id="">
                        <label class="control-label">Kode Fakultas</label>
                        <div class="controls">
                            <input type="text" class="span4" name="kd_fak" placeholder="Input Kode Fakultas" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Nama Fakultas</label>
                        <div class="controls">
                            <input type="text" class="span4" name="nama_fak" placeholder="Input Nama Fakultas" class="form-control" value="" required/>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- modal edit fakultas-->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_fakultas">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->