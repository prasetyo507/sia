<script>
function edit(id){
$('#myModal1').load('<?php echo base_url();?>datas/mahasiswapmb/load_lengkap/'+id);
}

function edit_edit(kk){
$('#absen').load('<?php echo base_url();?>datas/mahasiswapmb/load_lengkap_2016/'+kk);
}
</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-list"></i>
  				<h3>Daftar Calon Mahasiswa S1</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
				<?php 
				$logged = $this->session->userdata('sess_login');
				$pecah = explode(',', $logged['id_user_group']);
				$jmlh = count($pecah);
				for ($i=0; $i < $jmlh; $i++) { 
					$grup[] = $pecah[$i];
				}
				if ( (in_array(13, $grup))) { ?>
					<a data-toggle="modal" href="#myModal" class="btn btn-success"><i class="icon-excel"></i> Print Harian</a> 
					<a data-toggle="modal" href="#yuModal" class="btn btn-primary"><i class="icon-excel"></i> Print All</a>
					<a href="<?php echo base_url(); ?>datas/mahasiswapmb/printstatuss1" class="btn btn-warning"><i class="icon-excel"></i> Print Status Kelengkapan</a>
				<?php } else {	?>
					<a href="<?php echo base_url(); ?>datas/mahasiswapmb/cetak" class="btn btn-success">Cetak Excel</a><br>
				<?php } ?>
                    <!-- <a href="<?php //echo base_url(); ?>datas/mahasiswapmb/printexcels1" class="btn btn-primary"><i class="icon-excel"></i> Print All</a> --><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th width="50">No</th>
	                        	<th>ID Registrasi</th>
                                <th>Nama</th>
                                <th>NPM</th>
                                <th>Negeri/Swasta</th>
                                <th width="130">Asal Sekolah</th>
                                <th width="120">Program Pilihan</th>
                                <th>Kelas</th>
                                <th>Status</th>
                                <?php if ( (in_array(13, $grup)) or (in_array(8, $grup)) or (in_array(9, $grup))) { ?>
									<th>Aksi</th>
								<?php }	?>
                                <th>Berkas</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no=1; foreach ($look as $row) { ?>
	                        <tr>
                                <td><?php echo $no;?></td>
	                        	<td><?php echo $row->nomor_registrasi;?></td>
	                        	<td><?php echo $row->nama;?></td>
	                        	<td><?php echo $row->npm_baru;?></td>
	                        	<?php if ($row->kategori_skl == 'ngr') {
	                        		$jadi = 'Negeri';
	                        	} else {
	                        		$jadi = 'Swasta';
	                        	}
	                        	 ?>
	                        	<td><?php echo $jadi;?></td>
	                        	<td>
		                        	<?php if ($row->jenis_pmb == 'MB') {
		                        		echo $row->asal_sch_maba;
		                        	} elseif ($row->jenis_pmb == 'KV') {
		                        		echo $row->asal_pts_konversi;
		                        	} ?>
	                        	</td>
	                        	<td><?php echo $row->prodi;?></td>
	                        	<?php if ($row->kelas == 'pg') {
	                        		$kls = 'Pagi';
	                        	} elseif ($row->kelas == 'sr') {
	                        		$kls = 'Sore';
	                        	} else {
	                        		$kls = 'P2K';
	                        	}
	                        	 ?>
	                        	<td><?php echo $kls;?></td>
	                        	<?php if ($row->status == 1) {
	                        		$st = 'Lulus Tes';
	                        	} elseif ($row->status > 1) {
	                        		$st = 'Daftar Ulang';
	                        	} else {
	                        		$st = 'Registrasi';
	                        	}
	                        	 ?>
	                        	<td><?php echo $st; ?></td>
	                        	<?php if ( (in_array(13, $grup))) { ?>
									<td>
										<?php if ($tahu == '2016') { ?>
											<a class="btn btn-success" href="<?php echo base_url(); ?>datas/mahasiswapmb/detils1_2016/<?php echo $row->nomor_registrasi; ?>"><i class="icon-edit"></i></a>
											<a data-toggle="modal" onclick="edit_edit(<?php echo $row->id_form; ?>)" href="#myModal1" class="btn btn-primary"><i class="icon-ok"></i></a>
										<?php } else { ?>
											<a class="btn btn-success" href="<?php echo base_url(); ?>datas/mahasiswapmb/detils1/<?php echo $row->nomor_registrasi; ?>"><i class="icon-edit"></i></a>
											<a data-toggle="modal" onclick="edit(<?php echo $row->id_form; ?>)" href="#myModal1" class="btn btn-primary"><i class="icon-ok"></i></a>
										<?php }
										 ?>
										
									</td>
								<?php } elseif ( (in_array(8, $grup))) { ?>
									<td>
										<a class="btn btn-success" href="<?php echo base_url(); ?>master/print_hasil/print_hsl/<?php echo $row->nomor_registrasi; ?>"><i class="icon-print"></i></a>
									</td>
								<?php } ?>
								<?php $ask = $this->db->query("select status_kelengkapan from tbl_form_camaba where ((status_kelengkapan LIKE '%FT%' AND status_kelengkapan LIKE '%SKL%' AND status_kelengkapan LIKE '%IJZ%' AND status_kelengkapan LIKE '%SKHUN%' AND status_kelengkapan LIKE '%RP%' AND status_kelengkapan LIKE '%KTP%' AND status_kelengkapan LIKE '%KK%' AND status_kelengkapan LIKE '%AKT%') OR status_kelengkapan LIKE '%LLKP%') AND nomor_registrasi = '".$row->nomor_registrasi."'")->result(); if ($ask == true) { ?>
									<td><i class="icon-ok"></i></td>
								<?php } else { ?>
									<td> - </td>
								<?php } ?>
	                        </tr>
                            <?php $no++; } ?>
							
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui.js"></script>
<script>
   $(document).ready(function() {
     $( "#tgl" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: "yy-mm-dd"
      });
   });
      
</script>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="absen">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Print Data Pendaftaran Harian</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/printexcelharis1" method="post" enctype="multipart/form-data">
            	    
			
                <div class="modal-body" style="margin-left: 30px;">   
	                <div class="control-group" id="">
	                	<label class="control-label">Tanggal </label>
	                	<div class="controls">
		                	<input type="text" id="tgl" name="rekap_hari" class="form-control">
		                </div>
	                </div>
	                <div class="control-group" id="">
		                <label class="control-label">Fakultas </label>
	                	<div class="controls">
		                	<select name="fakultas" class="form-control">
		                		<option value="0">SEMUA FAKULTAS</option>
		                		<option value="1">EKONOMI</option>
		                		<option value="2">TEKNIK</option>
		                		<option value="3">HUKUM</option>
		                		<option value="4">ILMU KOMUNIKASI</option>
		                		<option value="5">PSIKOLOGI</option>
		                	</select>
		                </div>
                    </div>
				</div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="yuModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Print Data Pendaftaran</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/printexcels1" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                	<div class="control-group" id="">
	                	<label class="control-label">Fakultas </label>
	                	<div class="controls">
		                	<select name="fakultas" class="form-control">
		                		<option value="0">SEMUA FAKULTAS</option>
		                		<option value="1">EKONOMI</option>
		                		<option value="2">TEKNIK</option>
		                		<option value="3">HUKUM</option>
		                		<option value="4">ILMU KOMUNIKASI</option>
		                		<option value="5">PSIKOLOGI</option>
		                	</select>
		                </div>
                    </div>
				</div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->