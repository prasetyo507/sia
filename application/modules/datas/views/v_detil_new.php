<script>
$(document).ready(function() {
	$('#tgl_lahir').datepicker({
		dateFormat: "yy-mm-dd",

		yearRange: "1945:2016",

		changeMonth: true,

		changeYear: true

		});
});

</script>
<?php 
if ($cek->jenis_pmb == 'MB') {
	$jpmb = 'MAHASISWA BARU';
}elseif ($cek->jenis_pmb == 'RM') {
	$jpmb = 'READMISI';
}elseif ($cek->jenis_pmb == 'KV') {
	$jpmb = 'KONVERSI';
}
?>

<?php 

	$logged = $this->session->userdata('sess_login');
	$pecah = explode(',', $logged['id_user_group']);
	$jmlh = count($pecah);
	for ($i=0; $i < $jmlh; $i++) { 
		$grup[] = $pecah[$i];
	}

 ?>

<div class="row">
	<div class="span12" id="form_pmb">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Form Mahasiswa Baru</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>datas/mahasiswapmb/simpan_form_new" method="post">
					<fieldset>
						<?php // echo $cek->id_form; ?>
						<input type="hidden" name="tahubulat" value="<?php echo $bulat; ?>">
						<input type="hidden" name="kd_regis" value="<?php echo $cek->id_form; ?>">
						<input type="hidden" name="jenis" value="<?php echo $cek->jenis_pmb; ?>">
						<div class="control-group">
							<label class="control-label">Jenis Form</label>
							<div class="controls">
								<select class="form-control span2"  name="jenis_2" disabled>
									<option><?php echo $jpmb; ?></option>
								</select>
							</div>
						</div>

						<?php 
							$this->dbregs = $this->load->database('regis', TRUE); 
							$getem =  $this->dbregs->query("SELECT email from tbl_regist where userid = '".$cek->user_input."'")->row()->email; 
						?>

						<div class="control-group">
							<label class="control-label">E-Mail</label>
							<div class="controls">
								<input type="text" class="form-control span6" value="<?= $getem; ?>" disabled="">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pilihan Kampus</label>
							<div class="controls">
								<select class="form-control span2"  name="kampus">
									<option disabled="" >--Pilih Kampus--</option>
									<option value="jkt" <?php if ($cek->kampus == 'jkt') {echo 'selected=""';} ?>>Jakarta</option>
									<option value="bks" <?php if ($cek->kampus == 'bks') {echo 'selected=""';} ?>>Bekasi</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pilihan Kelas</label>
							<div class="controls">
								<select class="form-control span2"  name="kelas">
									<option>--Pilih Kelas--</option>
									<option value="PG" <?php if ($cek->kelas == 'PG') {echo 'selected=""';} ?>>Pagi</option>
									<option value="SR" <?php if ($cek->kelas == 'SR') {echo 'selected=""';} ?>>Sore</option>
									<option value="KY" <?php if ($cek->kelas == 'KY') {echo 'selected=""';} ?>>Karyawan</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Keterangan</label>
							<div class="controls">
								<select class="form-control span2"  name="ket" required>
									<option value="1" <?php if ($cek->keterangan == '1') {echo 'selected=""';} ?>>POLISI / PNS POLRI</option>
									<option value="2" <?php if ($cek->keterangan == '2') {echo 'selected=""';} ?>>KELUARGA POLISI</option>
									<option value="0" <?php if ($cek->keterangan == '0') {echo 'selected=""';} ?>>UMUM</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">NIK</label>
							<div class="controls">
								<input type="text" class="form-control span6" value="<?php echo $cek->nik; ?>" placeholder="Isi dengan nama calon mahasiswa"  name="nik"><br>
								<small>*sesuai ktp / kartu keluarga</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama</label>
							<div class="controls">
								<input type="text" class="form-control span6" value="<?php echo $cek->nama; ?>" placeholder="Isi dengan nama calon mahasiswa"  name="nama"><br>
								<small>*nama sesuai ijazah/akte kelahiran</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Jenis Kelamin</label>
							<div class="controls">
								<input type="radio" name="jk" value="L" <?php if ($cek->kelamin == 'L') {echo 'checked=""';} ?>> Laki - Laki <br>
								<input type="radio" name="jk" value="P" <?php if ($cek->kelamin == 'P') {echo 'checked=""';} ?>> Perempuan
							</div>
						</div>


						<div class="control-group">
							<label class="control-label">Kewarganegaraan</label>
							<div class="controls">
								<input type="radio" name="wn" id="wni" onclick="wni()" <?php if ($cek->status_wn == 'WNI') {echo 'checked=""';} ?> value="WNI" > WNI <br>
								<input type="radio" name="wn" id="wna" onclick="wna()" <?php if ($cek->status_wn == 'WNA') {echo 'checked=""';} ?> value="WNA"> WNA  &nbsp;&nbsp;
								
								<input  type="text" class="form-control span3" id="kwn" value="<?php echo $cek->kewarganegaraan ?>" name="wn_txt" placeholder="Kewarganegaraan Calon Mahasiswa">
							</div>
						</div>
				
						<div class="control-group">
							<label class="control-label">Kelahiran</label>
							<div class="controls">
								<input type="text" class="form-control span2" value="<?php echo $cek->tempat_lahir; ?>" placeholder="Tempat Lahir" name="tpt_lahir"  >
								<input type="text" class="form-control span2" value="<?php echo $cek->tgl_lahir; ?>" placeholder="Tanggal Lahir" id="tgl_lahir" name="tgl_lahir"  >
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Agama</label>
							<div class="controls">
								<select class="form-control span2"  name="agama">
									<option disabled="" >-- PILIH AGAMA --</option>
									<option value="ISL" <?php if ($cek->agama == 'ISL') {echo 'selected=""';} ?>>Islam</option>
									<option value="KTL" <?php if ($cek->agama == 'KTL') {echo 'selected=""';} ?>>Katolik</option>
									<option value="PRT" <?php if ($cek->agama == 'PRT') {echo 'selected=""';} ?>>Protestan</option>
									<option value="BDH" <?php if ($cek->agama == 'BDH') {echo 'selected=""';} ?>>Budha</option>
									<option value="HND" <?php if ($cek->agama == 'HND') {echo 'selected=""';} ?>>Hindu</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Status Nikah</label>
							<div class="controls">
								<input type="radio" name="stsm" value="Y" <?php if ($cek->status_nikah == 'Y') {echo 'checked=""';} ?> > Menikah  <br>
								<input type="radio" name="stsm" value="N" <?php if ($cek->status_nikah == 'N') {echo 'checked=""';} ?> > Belum Menikah
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label">Status Kerja</label>
							<div class="controls">
								<input type="radio" name="stsb" id="stsb_n" value="N" <?php if ($cek->status_kerja == 'N') {echo 'checked=""';} ?> > Belum Bekerja <br>
								<input type="radio" name="stsb" id="stsb_y" value="Y"<?php if ($cek->status_kerja == 'Y') {echo 'checked=""';} ?>> Bekerja &nbsp;&nbsp; 
								
								<input type="text" class="form-control span3" id="stsb_txt"  name="stsb_txt" placeholder="Tempat Bekerja Calon Mahasiswa" value="<?php echo $cek->pekerjaan ?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Alamat</label>
							<div class="controls">
								<!-- <textarea class="form-control span4" type="text" name="alamat"></textarea> -->
								<?php $exp = explode(',', $cek->alamat); $rt = $exp[0]; $rw = $exp[1]; $kel = $exp[2]; $kec = $exp[3]; $jln = $exp[4]; $prm = $exp[5]; ?>
								<div class="input-prepend input-append" style="margin-left: 20px; margin-bottom: 10px">
									<span class="add-on">Jln.</span>
									<input class="form-control span2" placeholder="Jalan" value="<?php echo $jln; ?>" type="text"  name="jln">
								</div>
								<div class="input-prepend input-append" style="margin-left: 20px; margin-bottom: 10px">
									<span class="add-on">RT</span>
									<input class="form-control span2" placeholder="RT" value="<?php echo $rt; ?>" type="text"  name="rt">
								</div>
								<div class="input-prepend input-append" style="margin-left: 20px; margin-bottom: 10px">
									<span class="add-on">RW</span>
									<input class="form-control span2" placeholder="RW" value="<?php echo $rw; ?>" type="text"  name="rw">
								</div>
								<div class="input-prepend input-append" style="margin-left: 20px; margin-bottom: 10px">
									<span class="add-on">Perum.</span>
									<input class="form-control span2" placeholder="Perumahan" value="<?php echo $prm; ?>" type="text"  name="prm">
								</div>
								<div class="input-prepend input-append" style="margin-left: 20px; margin-bottom: 10px">
									<span class="add-on">Kel.</span>
									<input class="form-control span3" placeholder="Kelurahan" value="<?php echo $kel; ?>" type="text"  name="kel">
								</div>
								<div class="input-prepend input-append" style="margin-left: 20px; margin-bottom: 10px">
									<span class="add-on">Kec.</span>
									<input class="form-control span3" placeholder="Kecamatan" value="<?php echo $kec; ?>" type="text"  name="kec">
								</div>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kode Pos</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan Kode Pos" value="<?php echo $cek->kd_pos; ?>" type="text"  name="kdpos">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">No. Telpon / HP</label>
							<div class="controls">
								<input class="form-control span3" value="<?php echo $cek->tlp; ?>" placeholder="Isi dengan Nomer Telpon" type="text"  name="tlp">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">No. Telpon / HP Wali</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan Nomer Telpon" value="<?php echo $cek->tlp2; ?>" type="text"  name="tlp2" maxlength=13 minlength=10>
							</div>
						</div>

						<!-- readmisi START -->
						<?php if ($cek->jenis_pmb == 'RM') { ?>
							<div id="readmisi">
							<div class="control-group" id="readmisi1">
								<label class="control-label">NPM Lama</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->nama; ?>" placeholder="Isi dengan Nomor Pokok Mahasiswa Lama"  type="text"  name="npm_readmisi">
								</div>
							</div>
							<div class="control-group" id="readmisi2">
								<label class="control-label">Tahun Masuk di UBJ</label>
								<div class="controls">
									<input class="form-control span1" type="text" value="<?php echo $cek->tahun_masuk_readmisi; ?>" name="thmasuk_readmisi"> &nbsp; Sampai dengan semester &nbsp;
									<input class="form-control span1" type="text" value="<?php echo $cek->smtr_keluar_readmisi; ?>" name="smtr_readmisi">
								</div>
							</div>
							</div>
						<?php } ?>
						
						<!-- readmisi END -->

						<!-- NEW START -->
						<?php if ($cek->jenis_pmb == 'MB') { ?>
							<div id="new0">
							<div class="control-group" id="new1">
								<label class="control-label">Asal Sekolah</label>
								<div class="controls">
									<input class="form-control span3" placeholder="Isi dengan Asal Sekolah Calon Mahasiswa" value="<?php echo $cek->asal_sch_maba ?>"  type="text"  name="asal_sch">
								</div>
							</div>
							<div class="control-group" id="new1">
								<label class="control-label">NISN</label>
								<div class="controls">
									<input class="form-control span3" placeholder="NISN Calon Mahasiswa"  type="text" value="<?php echo $cek->nisn ?>"  name="nisn">
								</div>
							</div>
							<div class="control-group" id="new2">
								<label class="control-label">Kota Asal Sekolah</label>
								<div class="controls">
									<input class="form-control span3" placeholder="Isi dengan kota Asal Sekolah " value="<?php echo $cek->kota_sch_maba ?>" type="text"  name="kota_sch">
								</div>
							</div>
							<div class="control-group" id="new2">
								<label class="control-label">Kelurahan Asal Sekolah</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->daerah_sch_maba ?>" placeholder="Isi dengan Kelurahan Asal Sekolah "  type="text"  name="daerah_sch" >
								</div>
							</div>
							<div class="control-group" id="new4">
								<label class="control-label">Jenis Sekolah</label>
								<div class="controls">
									<input type="radio" <?php if ($cek->kategori_skl == 'NGR') {echo 'checked=""';} ?> name="jenis_skl" value="NGR" > NEGERI &nbsp;&nbsp;
									<input type="radio" <?php if ($cek->kategori_skl == 'SWT') {echo 'checked=""';} ?> name="jenis_skl" value="SWT" > SWASTA &nbsp;&nbsp; 
								</div>
							</div>
							<div class="control-group" id="new4">
								<!-- <label class="control-label">Jenis Sekolah</label> -->
								<div class="controls">
									<input type="radio" name="jenis_sch_maba" <?php if ($cek->jenis_sch_maba == 'SMA') {echo 'checked=""';} ?> value="SMA" > SMA &nbsp;&nbsp;
									<input type="radio" name="jenis_sch_maba" <?php if ($cek->jenis_sch_maba == 'SMK') {echo 'checked=""';} ?> value="SMK" > SMK &nbsp;&nbsp; 
									<input type="radio" name="jenis_sch_maba" <?php if ($cek->jenis_sch_maba == 'MDA') {echo 'checked=""';} ?> value="MA" > MA  &nbsp;&nbsp;
									<input type="radio" name="jenis_sch_maba" <?php if ($cek->jenis_sch_maba == 'SMB') {echo 'checked=""';} ?> value="SMTB" > SMTB  &nbsp;&nbsp;
									<input type="radio" name="jenis_sch_maba" <?php if ($cek->jenis_sch_maba == 'OTH') {echo 'checked=""';} ?> value="LAIN" > Lainnya  
								</div>
							</div>
							<div class="control-group" id="new3">
								<label class="control-label">Jurusan</label>
								<div class="controls">
									<input class="form-control span1" type="text" value="<?php echo $cek->jur_maba; ?>"  name="jur_sch"> &nbsp;&nbsp; Tahun Lulus &nbsp;&nbsp;
									<input class="form-control span1" type="text" value="<?php echo $cek->lulus_maba; ?>"  name="lulus_sch">
								</div>
							</div>
							</div>
						<?php } ?>
						
						<!-- NEW END -->

						<!-- KONVERSI START -->
						<?php if ($cek->jenis_pmb == 'KV') { ?>
							<div id="konversi">
							<div class="control-group" id="konversi1">
								<label class="control-label">Nama Perguruan Tinggi</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->asal_pts_konversi; ?>" placeholder="Isi dengan Asal Sekolah Calon Mahasiswa"  type="text"  name="asal_pts">
								</div>
							</div>
							<div class="control-group" id="konversi4">
								<label class="control-label">Kota PTN/PTS</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->kota_pts_konversi; ?>" placeholder="Isi dengan kota PTS/PTN "  type="text"  name="kota_pts">
								</div>
							</div>
							<div class="control-group" id="konversi2">
								<label class="control-label">Program Studi</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->prodi_pts_konversi; ?>" placeholder="Isi dengan Program Studi "  type="text"  name="prodi_pts">
								</div>
							</div>
							<div class="control-group" id="konversi3">
								<label class="control-label">Tahun Lulus / Semester</label>
								<div class="controls">
									<input class="form-control span1" value="<?php echo $cek->tahun_lulus_konversi; ?>" placeholder="Tahun" type="text"  name="lulus_pts">  / 
									<input class="form-control span1" value="<?php echo $cek->smtr_lulus_konversi; ?>" placeholder="Semester " type="text"  name="smtr_pts">
								</div>
							</div>
							
							<div class="control-group" id="konversi5">
								<label class="control-label">NPM/NIM Asal</label>
								<div class="controls">
									<input class="form-control span3" value="<?php echo $cek->npm_pts_konversi; ?>" placeholder="Isi dengan kota PTS/PTN "  type="text"  name="npm_pts">
								</div>
							</div>
							</div>
						<?php } ?>
						
						<!-- KONVERSI END -->

						<div class="control-group">
							<label class="control-label">Pilih Program Studi</label>
							<div class="controls">
								<select class="form-control span3"  name="prodi">
									<option disabled="" >-- PILIH PROGRAM STUDI--</option>
									<?php foreach ($rows as $isi): ?>
										<option value="<?php echo $isi->kd_prodi ?>" <?php if ($cek->prodi == $isi->kd_prodi) {echo 'selected=""';} ?>><?php echo $isi->prodi ?></option>
									<?php endforeach ?>
									
								</select>
							</div>
						</div>

						
						
						<div class="control-group">
							<label class="control-label">Nama Ayah</label>
							<div class="controls">
								<input class="form-control span4" type="text" placeholder="Isi Nama Ayah Calon Mahasiswa" value="<?php echo $cek->nm_ayah; ?>"  name="nm_ayah">
								<select class="form-control span2"  name="didik_ayah">
									<option disabled="" >-- Pendidikan Ayah --</option>
									<option value="TSD" <?php if ($cek->didik_ayah == 'NSD') {echo 'selected=""';} ?>>Tidak tamat SD</option>
									<option value="SD" <?php if ($cek->didik_ayah == 'YSD') {echo 'selected=""';} ?>>Tamat SD</option>
									<option value="SLTP" <?php if ($cek->didik_ayah == 'SMP') {echo 'selected=""';} ?>>Tamat SLTP</option>
									<option value="SLTA" <?php if ($cek->didik_ayah == 'SMA') {echo 'selected=""';} ?>>Tamat SLTA</option>
									<option value="D" <?php if ($cek->didik_ayah == 'DPL') {echo 'selected=""';} ?>>Diploma</option>
									<option value="SM" <?php if ($cek->didik_ayah == 'SMD') {echo 'selected=""';} ?>>Sarjana Muda</option>
									<option value="S" <?php if ($cek->didik_ayah == 'SRJ') {echo 'selected=""';} ?>>Sarjana</option>
									<option value="PSC" <?php if ($cek->didik_ayah == 'PSC') {echo 'selected=""';} ?>>Pascasarjana</option>
									<option value="DTR" <?php if ($cek->didik_ayah == 'DTR') {echo 'selected=""';} ?>>Doctor</option>
									
								</select>
								<select class="form-control span2"  name="workdad">
									<option disabled="" >-- Pekerjaan Ayah --</option>
									<option value="PN" <?php if ($cek->workdad == 'PN') {echo 'selected=""';} ?>>Pegawai Negeri</option>
									<option value="TP" <?php if ($cek->workdad == 'TP') {echo 'selected=""';} ?> >TNI / POLRI</option>
									<option value="PS" <?php if ($cek->workdad == 'PS') {echo 'selected=""';} ?>>Pegawai Swasta</option>
									<option value="WU" <?php if ($cek->workdad == 'WU') {echo 'selected=""';} ?>>Wirausaha</option>
									<option value="PSN" <?php if ($cek->workdad == 'PE') {echo 'selected=""';} ?>>Pensiun</option>
									<option value="TK" <?php if ($cek->workdad == 'TK') {echo 'selected=""';} ?>>Tidak Bekerja</option>
									<option value="LL" <?php if ($cek->workdad == 'LL') {echo 'selected=""';} ?>>Lain-lain</option>
								</select>
								<select class="form-control span2"  name="life_statdad">
									<option disabled="" >-- Status Hidup --</option>
									<option value="MH" <?php if ($cek->life_statdad == 'MH') {echo 'selected=""';} ?>>Masih Hidup</option>
									<option value="SM" <?php if ($cek->life_statdad == 'SM') {echo 'selected=""';} ?>>Sudah Meninggal</option>
									
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Nama Ibu</label>
							<div class="controls">
								<input class="form-control span4" type="text" placeholder="Isi Nama Ibu Calon Mahasiswa" value="<?php echo $cek->nm_ibu; ?>" name="nm_ibu">
								<select class="form-control span2"  name="didik_ibu">
									<option disabled="" >-- Pendidikan Ibu --</option>
									<option value="TSD" <?php if ($cek->didik_ibu == 'NSD') {echo 'selected=""';} ?>>Tidak tamat SD</option>
									<option value="SD" <?php if ($cek->didik_ibu == 'YSD') {echo 'selected=""';} ?>>Tamat SD</option>
									<option value="SLTP" <?php if ($cek->didik_ibu == 'SMP') {echo 'selected=""';} ?>>Tamat SLTP</option>
									<option value="SLTA" <?php if ($cek->didik_ibu == 'SMA') {echo 'selected=""';} ?>>Tamat SLTA</option>
									<option value="D" <?php if ($cek->didik_ibu == 'DPL') {echo 'selected=""';} ?>>Diploma</option>
									<option value="SM" <?php if ($cek->didik_ibu == 'SMD') {echo 'selected=""';} ?>>Sarjana Muda</option>
									<option value="S" <?php if ($cek->didik_ibu == 'SRJ') {echo 'selected=""';} ?>>Sarjana</option>
									<option value="PSC" <?php if ($cek->didik_ibu == 'PSC') {echo 'selected=""';} ?>>Pascasarjana</option>
									<option value="DTR" <?php if ($cek->didik_ibu == 'DTR') {echo 'selected=""';} ?>>Doctor</option>
								</select>
								<select class="form-control span2"  name="workmom">
									<option disabled="" >-- Pekerjaan Ibu --</option>
									<option value="PN" <?php if ($cek->workmom == 'PN') {echo 'selected=""';} ?>>Pegawai Negeri</option>
									<option value="TP" <?php if ($cek->workmom == 'TP') {echo 'selected=""';} ?> >TNI / POLRI</option>
									<option value="PS" <?php if ($cek->workmom == 'PS') {echo 'selected=""';} ?>>Pegawai Swasta</option>
									<option value="WU" <?php if ($cek->workmom == 'WU') {echo 'selected=""';} ?>>Wirausaha</option>
									<option value="PSN" <?php if ($cek->workmom == 'PE') {echo 'selected=""';} ?>>Pensiun</option>
									<option value="TK" <?php if ($cek->workmom == 'TK') {echo 'selected=""';} ?>>Tidak Bekerja</option>
									<option value="LL" <?php if ($cek->workmom == 'LL') {echo 'selected=""';} ?>>Lain-lain</option>
								</select>
								<select class="form-control span2"  name="life_statmom">
									<option disabled="" >-- Status Hidup --</option>
									<option value="MH" <?php if ($cek->life_statmom == 'MH') {echo 'selected=""';} ?>>Masih Hidup</option>
									<option value="SM" <?php if ($cek->life_statmom == 'SM') {echo 'selected=""';} ?>>Sudah Meninggal</option>
									
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Penghasilan Orang Tua</label>
							<div class="controls">
								<input type="radio" value="1" <?php if ($cek->penghasilan == '1') {echo 'checked=""';} ?> name="gaji">
								Rp 1,000,000 - 2,000,000 &nbsp;&nbsp;
							
								<input type="radio" value="2" <?php if ($cek->penghasilan == '2') {echo 'checked=""';} ?> name="gaji">
								Rp 2,100,000 - 4,000,000 <br>
							
								<input type="radio" value="3" <?php if ($cek->penghasilan == '3') {echo 'checked=""';} ?> name="gaji">
								Rp 4,100,000 - 5,999,000  &nbsp;&nbsp;
						
								<input type="radio" value="4" <?php if ($cek->penghasilan == '4') {echo 'checked=""';} ?> name="gaji">
								>= Rp 6,000,000
							</div>
						</div>
						<div class="control-group" id="">
							<label class="control-label">Pengguna BPJS </label>
							<div class="controls">
								<input type="radio" id="bpjs-y" <?php if ($cek->bpjs == 'y') {echo 'checked=""';} ?> name="bpjs" value="y" required> Ya &nbsp;&nbsp;
								<input type="radio" id="bpjs-n" <?php if ($cek->bpjs == 'n') {echo 'checked=""';} ?> name="bpjs" value="n" > Tidak &nbsp;&nbsp; 
							</div>
						</div>
						<div class="control-group" id="bpjs-yes">
							<label class="control-label">NO. BPJS</label>
							<div class="controls">
								<input id="men" class="form-control span3" value="<?php echo $cek->no_bpjs ?>" placeholder="No. BPJS Calon Mahasiswa"  type="text"  name="nobpjs">
							</div>
						</div>
						<div class="control-group" id="bpjs-yes">
							<label class="control-label">Transportasi</label>
							<div class="controls">
								<select class="form-control span2"  name="transport">
									<?php 	if ($cek->transport != NULL and $cek->transport != '') {
												switch ($cek->transport) {
													case 'MBL':
														$transport = 'Mobil';
														break;
													case 'MTR':
														$transport = 'Motor';
														break;
													case 'AKT':
														$transport = 'Angkutan Umum';
														break;
													case 'SPD':
														$transport = 'Sepeda';
														break;
													case 'JKK':
														$transport = 'Jalan Kaki';
														break;
													case 'ADG':
														$transport = 'Andong';
														break;
													case 'KRT':
														$transport = 'Kereta';
														break;
												} ?>
												<option value="<?php echo $cek->transport;?>" selected><?php echo $transport; ?></option>
									<?php 	} ?>
									<option disabled="">-- Alat Transportasi --</option>
									<option value="MBL">Mobil</option>
									<option value="MTR">Motor</option>
									<option value="AKT">Angkutan Umum</option>
									<option value="SPD">Sepeda</option>
									<option value="JKK">Jalan Kaki</option>
									<option value="ADG">Andong</option>
									<option value="KRT">Kereta</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Ukuran Almamater</label>
							<div class="controls">
								<select class="form-control span2"  name="almet">
									<option disabled="" >-- Ukuran --</option>
									<option value="S" <?php if ($cek->almet == 'S') {echo 'selected=""';} ?>>S</option>
									<option value="M" <?php if ($cek->almet == 'M') {echo 'selected=""';} ?>>M</option>
									<option value="L" <?php if ($cek->almet == 'L') {echo 'selected=""';} ?>>L</option>
									<option value="X" <?php if ($cek->almet == 'X') {echo 'selected=""';} ?>>XL</option>
									<option value="2" <?php if ($cek->almet == '2') {echo 'selected=""';} ?>>XXL</option>
									<option value="3" <?php if ($cek->almet == '3') {echo 'selected=""';} ?>>XXXL</option>
								</select>
							</div>
						</div>

						<div class="control-group" id="konversi5">
							<label class="control-label">Referensi </label>
							<div class="controls">
								<input class="form-control span6" placeholder="informasi mengenai ubhara diperoleh dari" value="<?php echo $cek->referensi; ?>"  type="text"  name="refer">
							</div>
						</div>
						<div class="form-actions">
							<?php
							 if ((in_array(13, $grup))) { ?>
								<input class="btn btn-large btn-primary" type="submit" value="Update">
							<?php } ?>
							
							<!-- <input class="btn btn-large btn-default" type="reset" value="Clear"> -->
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>