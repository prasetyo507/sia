<script>
function status(idk){
$('#status').load('<?php echo base_url();?>data/iuran/load_status_bayar/'+idk);
}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Iuran Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a href="<?php echo base_url(); ?>data/iuran/back" class="btn btn-warning"> << Kembali </a><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Nomor Briva</th>
	                        	<th>NPM</th>
	                        	<th>Nama Mahasiswa</th>
                                <th>Status</th>
	                            <th width="80">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($mahasiswa as $row) { ?>
	                        <tr>
	                        	<td><?php echo number_format($no); ?></td>
                                <?php $ambil = substr($row->NIMHSMSMHS, 2); ?>
                                <td><?php echo '70306'.$ambil; ?></td>
	                        	<td><?php echo $row->NIMHSMSMHS; ?></td>
	                        	<td><?php echo $row->NMMHSMSMHS; ?></td>
                                <td></td>
	                        	<td class="td-actions">
									<a class="btn btn-primary btn-small" data-toggle="modal"  href="#myModal" onclick="status(<?php echo $row->NIMHSMSMHS; ?>)"><i class="btn-icon-only icon-ok"> </i></a>
                                    <a class="btn btn-success btn-small" href="#"><i class="btn-icon-only icon-print"> </i></a>
								</td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="status">
           
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->