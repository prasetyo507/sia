<?php // var_dump($row);die(); ?>
<script language="javascript">
	function numeric(e, decimal) { 
	var key;
	var keychar;
	 if (window.event) {
		 key = window.event.keyCode;
	 } else
	 if (e) {
		 key = e.which;
	 } else return true;
	
	keychar = String.fromCharCode(key);
	if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		return true;
	} else 
	if ((("0123456789").indexOf(keychar) > -1)) {
		return true; 
	} else 
	if (decimal && (keychar == ".")) {
		return true; 
	} else return false; 
	}
</script> 
            <script>
                $(document).ready(function(){

                    $('#tgledit').datepicker({
                        dateFormat: "yy-mm-dd",

                        yearRange: "1995:<?php echo date('Y'); ?>",

                        dateFormat: 'dd/mm/yy',

                        changeMonth: true,

                        changeYear: true
                    });

                });
               // $(document).ready(function(){
               //  $.post('<?php echo base_url();?>datas/karyawan/get_listjab/'+$(this).val(),{},function(get){
               //         $('#jabatans').html(get);
               //      });
               //  });
            </script>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Data Dosen</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>datas/karyawan/update_karyawan" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -30px;">    
                    <div class="control-group" id="">
                        <label class="control-label">NID</label>
                        <div class="controls">
                            <input 
                                type="text" 
                                class="span4" 
                                placeholder="Input Nomor Identitas" 
                                name="nid" 
                                class="form-control" 
                                value="<?php echo $row->nid;?>" 
                                required />
                            <input type="hidden" class="span4" name="old_nid" value="<?php echo $row->nid;?>" />
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">NIDN</label>
                        <div class="controls">
                            <input type="text" class="span4" name="nidn" placeholder="Input Nomor Identitas" class="form-control" value="<?php echo $row->nidn;?>" required/>
                        </div>
                    </div>
                    
                    <div class="control-group" id="">
                        <label class="control-label">NUPN / NIDK</label>
                        <div class="controls">
                            <input type="text" class="span4" name="nupn" placeholder="Input Nomor Identitas" class="form-control" value="<?php echo $row->nupn;?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Nama</label>
                        <div class="controls">
                            <input type="text" class="span4" name="nama" placeholder="Input Nama" class="form-control" value="<?php echo $row->nama;?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Jenis Kelamin</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="jk" value="P" <?php if($row->jns_kel=='P'){ echo 'checked'; } ?>/>
                                Pria
                            </label>
                            <label class="radio inline">
                                <input type="radio" name="jk" value="W" <?php if($row->jns_kel=='W'){ echo 'checked'; } ?>/>
                                Wanita
                            </label>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Telepon</label>
                        <div class="controls">
                            <input type="text" class="span4" name="telepon"  onkeypress="return numeric(event, false)" id="telepon_e" placeholder="Input Telepon" class="form-control" value="<?php echo $row->hp; ?>" required/>  
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Email</label>
                        <div class="controls">
                            <input type="text" class="span4" name="email" placeholder="Input Email" class="form-control" value="<?php echo $row->email;?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Alamat</label>
                        <div class="controls">
                            <textarea class="span4" name="alamat" placeholder="Input Alamat" class="form-control" value="" required><?php echo $row->alamat;?></textarea>
                        </div>
                    </div>
                    <!-- <div class="control-group" id="">
                        <label class="control-label">Foto</label>
                        <div class="controls">
                            <input type="file" class="span4" name="foto" class="form-control"/>
                        </div>
                    </div> -->

                    <script>
                    // $(document).ready(function(){
                    //     $('#lembaga1').change(function(){
                    //         $.post('<?php //echo base_url();?>datas/karyawan/get_listjab/'+$(this).val(),{},function(get){
                    //            //alert($(this).val());
                    //            $('#jabatan1').html(get);
                    //         });
                    //     });
                    // });
                    </script>
                    
                    <div class="control-group" id="">
                        <label class="control-label">Jabatan Fungsional</label>
                        <div class="controls">
                            <select class="span4" name="jabfung" id="lembaga" class="form-control" >
                                <option <?php if($row->jabfung==''){ echo ' selected="selected"'; }?> disabled>--</option>
								
                                <option <?php if($row->jabfung=='TPD'){ echo ' selected="selected"'; }?> value="TPD">Tenaga Pendidik</option>
													
								<option <?php if($row->jabfung=='SAH'){ echo ' selected="selected"'; }?> value="SAH">Asisten Ahli</option>
													
								<option <?php if($row->jabfung=='LKT'){ echo ' selected="selected"'; }?> value="LKT">Lektor</option>
													
								<option <?php if($row->jabfung=='LKK'){ echo ' selected="selected"'; }?> value="LKK">Lektor Kepala</option>
													
								<option <?php if($row->jabfung=='BIG'){ echo ' selected="selected"'; }?> value="BIG">Guru Besar</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">TMT Jabfung</label>

                        <div class="controls">

                            <input type="date" class="span4" name="tmt" placeholder="Input TMT Jabatan Fungsional" class="form-control" value="<?php echo $row->tmt_jabfung;?>" />

                        </div>

                    </div>

                    <div class="control-group" id="">
                        <label class="control-label">Prodi</label>
                        <div class="controls">
                            <select class="span4" name="prodi" id="lembaga1" class="form-control" required>
							<option selected disabled> -- </option>
                                <?php $su = $row->kd_prodi;
											foreach ($prodi as $isi){
												$is_selected='';
												if($isi->kd_prodi==$su){
													$is_selected='selected';
												}?>
                               
                                    <option value="<?php echo $isi->kd_prodi; ?>" <?php echo $is_selected ?>><?php echo $isi->prodi ?></option>
									<?php }?>
                            </select>
                        </div>
                    </div>

                    <!-- <div class="control-group" id="">
                        <label class="control-label">Jabatan</label>
                        <div class="controls">
                            <select class="span4" name="jabatan" id="jabatan1" class="form-control" value="" required>
                               
                            </select>
                        </div>
                    </div> -->
                    <div class="control-group" id="">
                        <label class="control-label">Status</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="status" value="1" <?php if ($row->tetap==1){ echo 'checked'; };?> />
                                Tetap
                            </label>
                            <label class="radio inline">
                                <input type="radio" name="status" value="NULL" <?php if ($row->tetap==NULL){ echo 'checked'; };?> />
                                Tidak Tetap
                            </label>
                            <label class="radio inline">
                                <input type="radio" name="status" value="2" <?php if ($row->tetap==2){ echo 'checked'; };?> />
                                MKDU
                            </label>
                        </div>
                    </div>

                    <div class="control-group" id="">
                        <label class="control-label">Status Aktif</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="stsakt" value="1" <?php if($row->aktif=='1'){ echo 'checked'; } ?>/>
                                Aktif
                            </label>
                            <label class="radio inline">
                                <input type="radio" name="stsakt" value="0" <?php if($row->aktif=='0'){ echo 'checked'; } ?>/>
                                Non aktif
                            </label>
                        </div>
                    </div>

                    <div class="control-group" id="">
                        <label class="control-label">Homebase</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="homebase" value="1" <?php if ($row->homebase==1){ echo 'checked'; };?> />
                                Ya
                            </label>
                            <label class="radio inline">
                                <input type="radio" name="homebase" value="NULL" <?php if ($row->homebase==NULL){ echo 'checked'; };?> />
                                Tidak
                            </label>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Struktural</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="struktural" value="1" <?php if ($row->struktural==1){ echo 'checked'; };?> />
                                Ya
                            </label>
                            <label class="radio inline">
                                <input type="radio" name="struktural" value="NULL" <?php if ($row->struktural==NULL){ echo 'checked'; };?> />
                                Tidak
                            </label>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>