<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswapmb extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		$this->db2 = $this->load->database('regis', TRUE);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(89)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	function index()
	{
		//$data['yes'] = $this->db->query("SELECT * from tbl_calon_mhs a join tbl_jurusan_prodi b on a.`kd_prodi`=b.`kd_prodi` where a.`status`>='1'")->result();
		$data['page'] = 'data_cmhs';
		$this->load->view('template', $data);
	}
	
	function print_dt()
	{
		$data['cmb'] = $this->db->query('SELECT * from tbl_calon_mhs a join tbl_jurusan_prodi b on a.`kd_prodi`=b.`kd_prodi` where a.`status` >= "1"')->result();
		$this->load->view('print_dt', $data);
	}

	function data_cmhs()
	{
		$data['page'] = 'data_cmhs';
		$this->load->view('template', $data);
	}

	function simpan_sesi()
	{
		$jenjang = $this->input->post('jenjang');
		$jenis = $this->input->post('jenis');
		$gel = $this->input->post('gel');
		$thn = $this->input->post('tahun');

		if ($this->input->post('prodi')) {
			$this->session->set_userdata('prodi', $this->input->post('prodi', TRUE));
		}

        $this->session->set_userdata('cuss', $jenjang);

        $this->session->set_userdata('jenis', $jenis);   

		$this->session->set_userdata('gelombang', $gel); 

		$this->session->set_userdata('tahun', $thn);        

		if ($thn < 2018) {
			redirect(base_url('datas/mahasiswapmb/load_data'));
		} else {
			redirect(base_url('datas/mahasiswapmb/load_data_new'));
		}
		
	}

	function load_data_new()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ($this->session->userdata('cuss') == 's1') {
			$program = 1;
		} else {
			$program = 2;
		}
		if ($this->session->userdata('jenis') != 'ALL') {
			$jenis = 'and jenis_pmb = "'.$this->session->userdata('jenis').'"';
		} else {
			$jenis = '';
		}
		if ($this->session->userdata('gelombang') > 0) {
			$gelombang = 'and gelombang = "'.$this->session->userdata('gelombang').'"';
		} else {
			$gelombang = '';
		}
		$thn = substr($this->session->userdata('tahun'),2,4);
		if ( (in_array(9, $grup))) {
			echo "9";
		} elseif ( (in_array(8, $grup))) {
			$data['look'] = $this->db2->query('SELECT * from tbl_form_pmb where program = '.$program.' '.$gelombang.' '.$jenis.' and nomor_registrasi like "'.$thn.'%" and prodi = "'.$this->session->userdata('prodi').'" and status_form = 1 order by nomor_registrasi ASC')->result();
			$data['page'] = "v_list_new";
			$this->load->view('template', $data);
		} elseif ( (in_array(13, $grup)) || (in_array(10, $grup)) || (in_array(12, $grup)) ) {
			$data['look'] = $this->db2->query('SELECT * from tbl_form_pmb where program = '.$program.' '.$gelombang.' '.$jenis.' and nomor_registrasi like "'.$thn.'%" and status_form = 1 order by nomor_registrasi ASC')->result();
			//var_dump('SELECT * from tbl_form_pmb where program = '.$program.' '.$gelombang.' '.$jenis.' order by nomor_registrasi ASC');exit();
			$data['page'] = "v_list_new";
			$this->load->view('template', $data);
		}
	}

	function load_data()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ( (in_array(9, $grup))) {
			$hahay = $this->session->userdata('cuss');
			if ($hahay == 's1') {
				if ($this->session->userdata('tahun') == '2016') {
					if ($this->session->userdata('gelombang') == 0) {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' order by nomor_registrasi ASC')->result();
						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					} else {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi ASC')->result();
						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					}
				} else {
					if ($this->session->userdata('gelombang') == 0) {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' order by nomor_registrasi ASC')->result();
						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					} else {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi ASC')->result();
						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					}
				}
				
			} elseif ($hahay == 's2') {
				if ($this->session->userdata('tahun') == '2016') {
					if ($this->session->userdata('gelombang') == 0) {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' order by ID_registrasi ASC')->result();
						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					} else {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					}
				} else {
					if ($this->session->userdata('gelombang') == 0) {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' order by ID_registrasi ASC')->result();
						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					} else {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					}
				}
				
			}
		} elseif ( (in_array(8, $grup))) {
			$hahay = $this->session->userdata('cuss');
			if ($hahay == 's1') {
				if ($this->session->userdata('tahun') == '2016') {
					if ($this->session->userdata('gelombang') == 0) {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.prodi = '.$logged['userid'].' order by nomor_registrasi ASC')->result();
						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					} else {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.prodi = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi ASC')->result();
						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					}
				} else {
					if ($this->session->userdata('gelombang') == 0) {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.prodi = '.$logged['userid'].' order by nomor_registrasi ASC')->result();
						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					} else {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.prodi = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi ASC')->result();
						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					}
				}
				
			} elseif ($hahay == 's2') {
				if ($this->session->userdata('tahun') == '2016') {
					if ($this->session->userdata('gelombang') == 0) {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.opsi_prodi_s2 = '.$logged['userid'].' order by ID_registrasi ASC')->result();
						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					} else {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.opsi_prodi_s2 = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					}
				} else {
					if ($this->session->userdata('gelombang') == 0) {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.opsi_prodi_s2 = '.$logged['userid'].' order by ID_registrasi ASC')->result();
						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					} else {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.opsi_prodi_s2 = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					}
				}
				
			}
		} elseif ( (in_array(13, $grup))) {
			$data['tahu'] = $this->session->userdata('tahun');
			$hahay = $this->session->userdata('cuss');
			if ($hahay == 's1') {
				if ($this->session->userdata('tahun') == '2016') {
					if ($this->session->userdata('gelombang') == 0) {
						if ($this->session->userdata('jenis') == 'KV') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "KV" 
															 order by nomor_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'MB') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "MB" 
															 order by nomor_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'RM') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "RM" 
															 order by nomor_registrasi DESC')->result();
						} elseif ($this->session->userdata('jenis') == 'ALL') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 order by nomor_registrasi DESC')->result();
						}
						
						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					} else {
						if ($this->session->userdata('jenis') == 'KV') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "KV" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by nomor_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'MB') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "MB" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by nomor_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'RM') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "RM" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by nomor_registrasi DESC')->result();
						} elseif ($this->session->userdata('jenis') == 'ALL') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi DESC')->result();
						}

						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					}
				} else {
					if ($this->session->userdata('gelombang') == 0) {
						if ($this->session->userdata('jenis') == 'KV') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "KV" 
															 order by nomor_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'MB') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "MB" 
															 order by nomor_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'RM') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "RM" 
															 order by nomor_registrasi DESC')->result();
						} elseif ($this->session->userdata('jenis') == 'ALL') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 order by nomor_registrasi DESC')->result();
						}
						
						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					} else {
						if ($this->session->userdata('jenis') == 'KV') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "KV" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by nomor_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'MB') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "MB" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by nomor_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'RM') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where jenis_pmb = "RM" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by nomor_registrasi DESC')->result();
						} elseif ($this->session->userdata('jenis') == 'ALL') {
							$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
															 where gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi DESC')->result();
						}

						$data['page'] = "v_list_s1";
						$this->load->view('template', $data);
					}
				}
				
				
			} elseif ($hahay == 's2') {
				if ($this->session->userdata('tahun') == '2016') {
					if ($this->session->userdata('gelombang') == 0) {
						if ($this->session->userdata('jenis') == 'KV') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "KV" 
															 order by ID_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'MB') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "MB" 
															 order by ID_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'RM') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "RM" 
															 order by ID_registrasi DESC')->result();
						} elseif ($this->session->userdata('jenis') == 'ALL') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															order by ID_registrasi DESC')->result();
						}
						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					} else {
						if ($this->session->userdata('jenis') == 'KV') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "KV" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by ID_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'MB') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "MB" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by ID_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'RM') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "RM" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by ID_registrasi DESC')->result();
						} elseif ($this->session->userdata('jenis') == 'ALL') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															where gelombang = '.$this->session->userdata('gelombang').'
															order by ID_registrasi DESC')->result();
						}

						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					}
				} else {
					if ($this->session->userdata('gelombang') == 0) {
						if ($this->session->userdata('jenis') == 'KV') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "KV" 
															 order by ID_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'MB') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "MB" 
															 order by ID_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'RM') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "RM" 
															 order by ID_registrasi DESC')->result();
						} elseif ($this->session->userdata('jenis') == 'ALL') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															order by ID_registrasi DESC')->result();
						}
						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					} else {
						if ($this->session->userdata('jenis') == 'KV') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "KV" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by ID_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'MB') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "MB" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by ID_registrasi DESC')->result();
						}elseif ($this->session->userdata('jenis') == 'RM') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															 where jenis_pmb = "RM" AND gelombang = '.$this->session->userdata('gelombang').'
															 order by ID_registrasi DESC')->result();
						} elseif ($this->session->userdata('jenis') == 'ALL') {
							$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
															where gelombang = '.$this->session->userdata('gelombang').'
															order by ID_registrasi DESC')->result();
						}

						$data['page'] = "v_list_s2";
						$this->load->view('template', $data);
					}
				}
			}
		}
		
	}

	function printexcels2()
	{
		if ($this->input->post('fakultas') == 0) {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi order by ID_registrasi ASC')->result();
				$this->load->view('excel_mabas2', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
				$this->load->view('excel_mabas2', $data);
			}
			
		} else {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' order by ID_registrasi ASC')->result();
				$this->load->view('excel_mabas2', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
				$this->load->view('excel_mabas2', $data);
			}
			
		}
		
	}



	function printexcels1()
	{
		if ($this->input->post('fakultas') == 0) {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			}
		} else {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			}
			
		}
	}
	
	function detilnew($id)
	{
		$data['rows'] = $this->app_model->getdata('tbl_jurusan_prodi','kd_fakultas','asc')->result();
		$data['cek'] = $this->db2->query('SELECT * from tbl_form_pmb where nomor_registrasi = "'.$id.'"')->row();
		//var_dump($data);exit();
		if ($data['cek']->program == 1) {
			$data['page'] = "v_detil_new";
		} else {
			$data['page'] = "v_detil_new2";
		}
		$this->load->view('template', $data);
	}

	function simpan_form_new()
	{
		$logged = $this->session->userdata('sess_login');
		$id = $this->input->post('kd_regis');
		$adr = $this->input->post('rt').','.$this->input->post('rw').','.$this->input->post('kel').','.$this->input->post('kec').','.$this->input->post('jln').','.$this->input->post('prm');
		$datax = array(
				'jenis_pmb'		=>	$this->input->post('jenis'),
				'kampus'		=>	$this->input->post('kampus'),
				'kelas'			=>	$this->input->post('kelas'),
				'nama'			=>	$this->input->post('nama'),
				'nik'			=>	$this->input->post('nik'),
				'kelamin'		=>	$this->input->post('jk'),
				'status_wn'		=>	$this->input->post('wn'),
				'kewarganegaraan'=>	$this->input->post('wn_txt'),
				'tempat_lahir'	=>	$this->input->post('tpt_lahir'),
				'tgl_lahir'		=>	$this->input->post('tgl_lahir'),
				'agama'			=>	$this->input->post('agama'),
				'status_nikah'	=>	$this->input->post('stsm'),
				'status_kerja'	=>	$this->input->post('stsb'),
				'pekerjaan'		=>	$this->input->post('stsb_txt'),
				'alamat'		=>	$adr,
				'kd_pos'		=>	$this->input->post('kdpos'),
				'tlp'			=>	$this->input->post('tlp'),
				'tlp2'			=>	$this->input->post('tlp2'),

				'npm_lama_readmisi'		=>	$this->input->post('npm_readmisi'),
				'tahun_masuk_readmisi'	=>	$this->input->post('thmasuk_readmisi'),
				'smtr_keluar_readmisi'	=>	$this->input->post('smtr_readmisi'),

				'asal_sch_maba'			=>	$this->input->post('asal_sch'),
				'kota_sch_maba'			=>	$this->input->post('kota_sch'),
				'jur_maba'				=>	$this->input->post('jur_sch'),
				'lulus_maba'			=>	$this->input->post('lulus_sch'),
				'jenis_sch_maba'		=> 	$this->input->post('jenis_sch_maba'),

				'asal_pts_konversi'		=>	$this->input->post('asal_pts'),
				'kota_pts_konversi'		=>	$this->input->post('kota_pts'),
				'prodi_pts_konversi'	=>	$this->input->post('prodi_pts'),
				'npm_pts_konversi'		=>	$this->input->post('npm_pts'),
				'tahun_lulus_konversi'	=>	$this->input->post('lulus_pts'),
				'smtr_lulus_konversi'	=>	$this->input->post('smtr_pts'),
				'keterangan' 			=>	$this->input->post('ket',TRUE),

				'daerah_sch_maba' => 	$this->input->post('daerah_sch'),
				
				'prodi'			=>	$this->input->post('prodi'),
				'nm_ayah'		=>	$this->input->post('nm_ayah'),
				'didik_ayah'	=>	$this->input->post('didik_ayah'),
				'workdad'		=>	$this->input->post('workdad'),
				'life_statdad'	=>	$this->input->post('life_statdad'),
				'nm_ibu'		=>	$this->input->post('nm_ibu'),
				'didik_ibu'		=>	$this->input->post('didik_ibu'),
				'workmom'		=>	$this->input->post('workmom'),
				'life_statmom'	=>	$this->input->post('life_statmom'),
				//'tanggal_regis'	=>	date('Y-m-d'),
				'penghasilan'	=>	$this->input->post('gaji'),
				'referensi'		=>	$this->input->post('refer'),
				'kategori_skl'	=> $this->input->post('jenis_skl'),
				'nisn'			=> $this->input->post('nisn'),
				'bpjs'			=> $this->input->post('bpjs'),
				//'user_input'	=> $logged['userid'],
				'no_bpjs'		=> $this->input->post('nobpjs'),
				'transport'		=> $this->input->post('transport'),
				'almet'			=> $this->input->post('almet')
			);
		//var_dump($id);exit();

		$this->db2->where('id_form', $id);
		$this->db2->update('tbl_form_pmb', $datax);
		$detailbgt = $this->db2->query('SELECT * from tbl_form_pmb where id_form = "'.$id.'"')->row()->npm_baru;
	
		if (($detailbgt != '') or (!is_null($detailbgt))) {
			$dataxxx['NMMHSMSMHS'] 	= strtoupper($this->input->post('nama'));
			$dataxxx['TPLHRMSMHS']	= strtoupper($this->input->post('tpt_lahir'));
			$dataxxx['TGLHRMSMHS']	= $this->input->post('tgl_lahir');
			$dataxxx['NMIBUMSMHS']	= strtoupper($this->input->post('nm_ibu'));
			$this->db->where('NIMHSMSMHS', $detailbgt);
			$this->db->update('tbl_mahasiswa', $dataxxx);
		}
		

		echo "<script>alert('Sukses');
		document.location.href='".base_url()."datas/mahasiswapmb/load_data_new';</script>";
	}

	function detils2($id)
	{
		$data['cek'] = $this->db->query('SELECT * from tbl_pmb_s2 where ID_registrasi = "'.$id.'"')->row();
		$data['page'] = "v_detil_mabas2";
		$this->load->view('template', $data);
	}

	function detils2_2016($id)
	{
		$data['bulat'] = $this->session->userdata('tahun');
		$data['cek'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where ID_registrasi = "'.$id.'"')->row();
		$data['page'] = "v_detil_mabas2";
		$this->load->view('template', $data);
	}

	function detils1($id)
	{
		$data['cek'] = $this->db->query('SELECT * from tbl_form_camaba where nomor_registrasi = "'.$id.'"')->row();

		$a = array('74101','61101');

		$this->db->select('*');
		$this->db->from('tbl_jurusan_prodi');
		$this->db->where_not_in('kd_prodi', $a);
		$data['rows'] = $this->db->get()->result();

		$data['page'] = "v_detil_mabas1";
		$this->load->view('template', $data);
	}

	function detils1_2016($id)
	{
		$data['bulat'] = $this->session->userdata('tahun');
		$data['cek'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where nomor_registrasi = "'.$id.'"')->row();

		$a = array('74101','61101');

		$this->db->select('*');
		$this->db->from('tbl_jurusan_prodi');
		$this->db->where_not_in('kd_prodi', $a);
		$data['rows'] = $this->db->get()->result();

		$data['page'] = "v_detil_mabas1";
		$this->load->view('template', $data);
	}

	function simpan_form(){


		$id = $this->input->post('kd_regis');
		//die($id);
		$datax = array(

				'jenis_pmb'		=>	$this->input->post('jenis'),
				'kampus'		=>	$this->input->post('kampus'),
				'kelas'			=>	$this->input->post('kelas'),
				'nama'			=>	$this->input->post('nama'),
				'kelamin'		=>	$this->input->post('jk'),
				'status_wn'		=>	$this->input->post('wn'),
				'kewaganegaraan'=>	$this->input->post('wn_txt'),
				'tempat_lahir'	=>	$this->input->post('tpt_lahir'),
				'tgl_lahir'		=>	$this->input->post('tgl_lahir'),
				'agama'			=>	$this->input->post('agama'),
				'status_nikah'	=>	$this->input->post('stsm'),
				'status_kerja'	=>	$this->input->post('stsb'),
				'perkerjaan'	=>	$this->input->post('stsb_txt'),
				'alamat'		=>	$this->input->post('alamat'),
				'kd_pos'		=>	$this->input->post('kdpos'),
				'tlp'			=>	$this->input->post('tlp'),
				'tlp2'			=>	$this->input->post('tlp2'),

				'npm_lama_readmisi'		=>	$this->input->post('npm_readmisi'),
				'tahun_masuk_readmisi'	=>	$this->input->post('thmasuk_readmisi'),
				'smtr_keluar_readmisi'	=>	$this->input->post('smtr_readmisi'),

				'asal_sch_maba'			=>	$this->input->post('asal_sch'),
				'kota_sch_maba'			=>	$this->input->post('kota_sch'),
				'jur_maba'				=>	$this->input->post('jur_sch'),
				'lulus_maba'			=>	$this->input->post('lulus_sch'),
				'jenis_sch_maba'		=> 	$this->input->post('jenis_sch_maba'),

				'asal_pts_konversi'		=>	$this->input->post('asal_pts'),
				'kota_pts_konversi'		=>	$this->input->post('kota_pts'),
				'prodi_pts_konversi'	=>	$this->input->post('prodi_pts'),
				'npm_pts_konversi'		=>	$this->input->post('npm_pts'),
				'tahun_lulus_konversi'	=>	$this->input->post('lulus_pts'),
				'smtr_lulus_konversi'	=>	$this->input->post('smtr_pts'),
				'keterangan' 			=>	$this->input->post('ket',TRUE),

				'daerah_sch_maba' => 	$this->input->post('daerah_sch'),
				
				'prodi'			=>	$this->input->post('prodi'),
				'nm_ayah'		=>	$this->input->post('nm_ayah'),
				'didik_ayah'	=>	$this->input->post('didik_ayah'),
				'workdad'		=>	$this->input->post('workdad'),
				'life_statdad'	=>	$this->input->post('life_statdad'),
				'nm_ibu'		=>	$this->input->post('nm_ibu'),
				'didik_ibu'		=>	$this->input->post('didik_ibu'),
				'workmom'		=>	$this->input->post('workmom'),
				'life_statmom'	=>	$this->input->post('life_statmom'),
				//'tanggal_regis'	=>	date('Y-m-d'),
				'penghasilan'	=>	$this->input->post('gaji'),
				'referensi'		=>	$this->input->post('refer'),
				'kategori_skl'	=> $this->input->post('jenis_skl'),
				'nisn'			=> $this->input->post('nisn'),
				'bpjs'			=> $this->input->post('bpjs'),
				'user_input'	=> $logged['userid'],
				'no_bpjs'		=> $this->input->post('nobpjs'),
				'transport'		=> $this->input->post('transport')
				
			);
		if ($this->input->post('tahubulat') == '2016') {
			$this->db->where('id_form', $id);
			$this->db->update('tbl_form_camaba_2016', $datax);
			$detailbgt = $this->app_model->getdetail('tbl_form_camaba_2016','id_form',$id,'id_form','asc')->row()->npm_baru;
		} else {
			$this->db->where('id_form', $id);
			$this->db->update('tbl_form_camaba', $datax);
			$detailbgt = $this->app_model->getdetail('tbl_form_camaba','id_form',$id,'id_form','asc')->row()->npm_baru;
		}


		if (($detailbgt != '') or (!is_null($detailbgt))) {
			$dataxxx['NMMHSMSMHS'] 	= strtoupper($this->input->post('nama'));
			$dataxxx['TPLHRMSMHS']	= strtoupper($this->input->post('tpt_lahir'));
			$dataxxx['TGLHRMSMHS']	= $this->input->post('tgl_lahir');
			$dataxxx['NMIBUMSMHS']	= strtoupper($this->input->post('nm_ibu'));
			$this->db->where('NIMHSMSMHS', $detailbgt);
			$this->db->update('tbl_mahasiswa', $dataxxx);
		}
		

		echo "<script>alert('Sukses');
		document.location.href='".base_url()."datas/mahasiswapmb/load_data';</script>";
	}

	function printexcelharis1()
	{
		if ($this->input->post('fakultas') == 0) {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.tanggal_regis = "'.$this->input->post('rekap_hari').'" order by nomor_registrasi ASC')->result();
				//var_dump($data);exit();
				$this->load->view('excel_mabas1', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.gelombang = '.$this->session->userdata('gelombang').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'" order by nomor_registrasi ASC')->result();
				//var_dump($data);exit();
				$this->load->view('excel_mabas1', $data);
			}
		} else {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'" order by nomor_registrasi ASC')->result();
				//var_dump($data);exit();
				$this->load->view('excel_mabas1', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.gelombang = '.$this->session->userdata('gelombang').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'" order by nomor_registrasi ASC')->result();
				//var_dump($data);exit();
				$this->load->view('excel_mabas1', $data);
			}
			
		}
		
	}

	function printexcelharis2()
	{
		if ($this->input->post('fakultas') == 0) {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.tanggal_regis = "'.$this->input->post('rekap_hari').'"')->result();
				$this->load->view('excel_mabas2', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.gelombang = '.$this->session->userdata('gelombang').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'"')->result();
				$this->load->view('excel_mabas2', $data);
			}
		} else {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'"')->result();
				$this->load->view('excel_mabas2', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.gelombang = '.$this->session->userdata('gelombang').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'"')->result();
				$this->load->view('excel_mabas2', $data);
			}
			
		}
		
	}

	function update_s2()
	{
		$this->load->helper('inflector');
		$nama = underscore($_FILES['img']['name']);
		$config['upload_path'] = './upload/imgpmb/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['file_name'] = $nama;
		$config['max_size'] = 1000000;
		$this->load->library('upload', $config);
		$patt = $config['upload_path'].basename($nama);

		$id = $this->input->post('ole');

		$namm = strtoupper($this->input->post('nama'));
		$ayah = strtoupper($this->input->post('nm_ayah'));
		$ibu = strtoupper($this->input->post('nm_ibu'));
		$kota = strtoupper($this->input->post('kota_asal'));

		$datas = array(
				'kampus'		=>	$this->input->post('kampus'),
				'kelas'			=>	'KR',
				'nik'			=>	$this->input->post('nik'),
				'nama'			=>	$namm,
				'kelamin'		=>	$this->input->post('jk'),
				'negara'		=>	$this->input->post('wn'),
				'tempat_lahir'	=>	$this->input->post('tpt'),
				'tgl_lahir'		=>	$this->input->post('tgl'),
				'agama'			=>	$this->input->post('agama'),
				'status_nikah'	=>	$this->input->post('stsm'),
				'status_kerja'	=>	$this->input->post('stsb'),
				'alamat'		=>	$this->input->post('alamat'),
				'kd_pos'		=>	$this->input->post('kdpos'),
				'tlp'			=>	$this->input->post('tlp'),
				'npm_lama'		=>	$this->input->post('npm'),
				'thmsubj'		=>	$this->input->post('thmasuk'),
				'nm_univ'		=>	$this->input->post('nm_pt'),
				'prodi'			=>	$this->input->post('prodi_lm'),
				'th_lulus'		=>	$this->input->post('thlulus'),
				'kota_asl_univ'	=>	$kota,
				'npm_nim'		=>	$this->input->post('npmnim'),
				'opsi_prodi_s2'	=>	$this->input->post('prodi'),
				'pekerjaan'		=>	$this->input->post('pekerjaan'),
				'nm_ayah'		=>	$ayah,
				'nm_ibu'		=>	$ibu,
				'penghasilan'	=>	$this->input->post('gaji'),
				'informasi_from'=>	$this->input->post('infrom'),
				'foto'			=>	$patt,
				'negara_wna'	=>	$this->input->post('jns_wn'),
				'tempat_kerja'	=>	$this->input->post('tpt_kerja'),
				'bpjs'			=>	$this->input->post('bpjs'),
				'no_bpjs'		=>	$this->input->post('nobpjs'),
				'kategori_univ'	=>	$this->input->post('jenispt'),
				'asaluniv'		=> 	$this->input->post('asaluniv'),
				'transport'		=> $this->input->post('transport')
			);
		//var_dump($datas);exit();
		$this->app_model->updatedata('tbl_pmb_s2','ID_registrasi',$id,$datas);

		$detailbgt = $this->app_model->getdetail('tbl_pmb_s2','ID_registrasi',$id,'ID_registrasi','asc')->row()->npm_baru;
		$dataxxx['NMMHSMSMHS'] 	= strtoupper($namm);
		$dataxxx['TPLHRMSMHS']	= strtoupper($this->input->post('tpt'));
		$dataxxx['TGLHRMSMHS']	= $this->input->post('tgl');
		$dataxxx['NMIBUMSMHS']	= strtoupper($ibu);
		$this->db->where('NIMHSMSMHS', $detailbgt);
		$this->db->update('tbl_mahasiswa', $dataxxx);
		echo "<script>alert('Sukses');history.go(-1);</script>";
	}

	function load_lengkap($id)
	{
		$data['mhspmb'] = $this->app_model->getdetail('tbl_form_camaba','id_form',$id,'id_form','asc')->row();
		$this->load->view('lengkap_view',$data);
	}

	function load_lengkap_2016($id)
	{
		$data['tahu'] = $this->session->userdata('tahun');
		$data['mhspmb'] = $this->app_model->getdetail('tbl_form_camaba_2016','id_form',$id,'id_form','asc')->row();
		$this->load->view('lengkap_view',$data);
	}

	function load_lengkap2($id)
	{
		$data['mhspmb'] = $this->app_model->getdetail('tbl_pmb_s2','id_s2',$id,'id_s2','asc')->row();
		$this->load->view('lengkap_view2',$data);
	}

	function load_lengkap_new($id,$key)
	{
		$data['tahu'] = $this->session->userdata('tahun');
		$data['idcmb'] = $id;
		$data['keybo'] = $key;
		$data['page'] = 'lengkap_view_new';
		$this->load->view('template',$data);

	}

	function save_lengkap(){
		$reg = $this->input->post('id', TRUE);
		$jumlah = count($this->input->post('lengkap', TRUE));
		//var_dump($reg.' - '.$jumlah);exit();
		$hasil = '';
		for ($i=0; $i < $jumlah; $i++) { 
			if ($hasil == '') {
				$hasil = $this->input->post('lengkap['.$i.']', TRUE).',';
			} else {
				$hasil = $hasil.$this->input->post('lengkap['.$i.']', TRUE).',';
			}	
		}
		$data['status_kelengkapan'] = $hasil;
		$this->app_model->updatedata('tbl_form_camaba','nomor_registrasi',$reg,$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."datas/mahasiswapmb/load_data';</script>";
		//var_dump($hasil);exit();
	}

	function save_lengkap_2016(){
		$reg = $this->input->post('id', TRUE);
		$jumlah = count($this->input->post('lengkap', TRUE));
		//var_dump($reg.' - '.$jumlah);exit();
		$hasil = '';
		for ($i=0; $i < $jumlah; $i++) { 
			if ($hasil == '') {
				$hasil = $this->input->post('lengkap['.$i.']', TRUE).',';
			} else {
				$hasil = $hasil.$this->input->post('lengkap['.$i.']', TRUE).',';
			}	
		}
		$data['status_kelengkapan'] = $hasil;
		$this->app_model->updatedata('tbl_form_camaba_2016','nomor_registrasi',$reg,$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."datas/mahasiswapmb/load_data';</script>";
		//var_dump($hasil);exit();
	}

	function save_lengkap2(){
		$reg = $this->input->post('id', TRUE);
		$jumlah = count($this->input->post('lengkap', TRUE));
		//var_dump($reg.' - '.$jumlah);exit();
		$hasil = '';
		for ($i=0; $i < $jumlah; $i++) { 
			if ($hasil == '') {
				$hasil = $this->input->post('lengkap['.$i.']', TRUE).',';
			} else {
				$hasil = $hasil.$this->input->post('lengkap['.$i.']', TRUE).',';
			}	
		}
		$data['status_kelengkapan'] = $hasil;
		$this->app_model->updatedata('tbl_pmb_s2','ID_registrasi',$reg,$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."datas/mahasiswapmb/load_data';</script>";
		//var_dump($hasil);exit();
	}

	function printstatuss1()
	{
		if ($this->session->userdata('gelombang') == 0) {
			// die('1');
			if ($this->session->userdata('jenis') == 'KV') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "KV" and status >1
												 order by nomor_registrasi ASC')->result();
			}elseif ($this->session->userdata('jenis') == 'MB') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "MB" and status >1
												 order by nomor_registrasi ASC')->result();
			}elseif ($this->session->userdata('jenis') == 'RM') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "RM" and status >1
												 order by nomor_registrasi ASC')->result();
			} elseif ($this->session->userdata('jenis') == 'ALL') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi 
												 WHERE status >1 								 
												 order by nomor_registrasi ASC')->result();
			}
		} else {
			
			if ($this->session->userdata('jenis') == 'KV') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "KV" and status >1 and a.gelombang = '.$this->session->userdata('gelombang').'
												 order by nomor_registrasi ASC')->result();
			}elseif ($this->session->userdata('jenis') == 'MB') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "MB" and status >1 and a.gelombang = '.$this->session->userdata('gelombang').'
												 order by nomor_registrasi ASC')->result();
			}elseif ($this->session->userdata('jenis') == 'RM') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "RM" and status >1 and a.gelombang = '.$this->session->userdata('gelombang').'
												 order by nomor_registrasi ASC')->result();
			} elseif ($this->session->userdata('jenis') == 'ALL') {
				//die('3');
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi 
												 where a.gelombang = '.$this->session->userdata('gelombang').' and status >1
												 order by nomor_registrasi ASC')->result();
				
			}
			
		}

		$this->load->library('excel');
		$this->load->view('welcome/phpexcel_status', $data);
	}

	function printstatuss2()
	{
		if ($this->session->userdata('gelombang') == 0) {
			$data['look'] = $this->db->query('SELECT ID_registrasi as noreg,nama,status_kelengkapan,th_lulus as lls from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi WHERE status >1 order by ID_registrasi ASC')->result();
		} else {
			$data['look'] = $this->db->query('SELECT ID_registrasi as noreg,nama,status_kelengkapan,th_lulus as lls from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where status >1 and a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
		}
		$this->load->library('excel');
		$this->load->view('welcome/phpexcel_status', $data);
	}

	function cetak()
	{
		$logged = $this->session->userdata('sess_login');

		$uid = $logged['userid'];

		if ($this->session->userdata('cuss') == 's1') {
			$progs = 1;
		} else {
			$progs = 2;
		}
		

		if ($uid == 'perpus') {
			$prodiCondition = '';
		} else {
			$prodiCondition = 'and prodi = "'.$uid.'"';
		}
		

		$data['prod'] = get_jur($logged['userid']);

		if ($this->session->userdata('tahun') > '2017') {
			
			if ($this->session->userdata('jenis') == 'ALL') {

				if ($this->session->userdata('gelombang') == '0') {

					if ($uid == 'perpus') {
						$data['owa'] = $this->db2->query('SELECT * from tbl_form_pmb where program = '.$progs.' ')->result();
					} else {
						$data['owa'] = $this->db2->query('SELECT * from tbl_form_pmb where program = '.$progs.' '.$prodiCondition.' ')->result();
					}

				} else {

					$data['owa'] = $this->db2->query('SELECT * from tbl_form_pmb where gelombang = "'.$this->session->userdata('gelombang').'" '.$prodiCondition.' and program = '.$progs.' ')->result();

					var_dump($data['owa']);exit();

				}
				
			} else {

				if ($this->session->userdata('gelombang') == '0') {

					$data['owa'] = $this->db2->query('SELECT * from tbl_form_pmb where jenis_pmb = "'.$this->session->userdata('jenis').'" '.$prodiCondition.' and program = '.$progs.' ')->result();

				} else {

					$data['owa'] = $this->db2->query('SELECT * from tbl_form_pmb where jenis_pmb = "'.$this->session->userdata('jenis').'" and gelombang = "'.$this->session->userdata('gelombang').'" '.$prodiCondition.' and program = '.$progs.' ')->result();
				}
					
			}

			$this->load->view('v_maba_export_new', $data);

		} else {
			
			if ($this->session->userdata('cuss') == 's1') {

				if ($this->session->userdata('jenis') == 'ALL') {

					if ($this->session->userdata('gelombang') == '0') {

						if ($this->session->userdata('tahun') == '2016') {
							$data['owa'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "'.$logged['userid'].'"')->result();
						} else {
							$data['owa'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "'.$logged['userid'].'"')->result();
						}

					} else {

						if ($this->session->userdata('tahun') == '2016') {
							$data['owa'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where gelombang = "'.$this->session->userdata('gelombang').'" and prodi = "'.$logged['userid'].'"')->result();
						} else {
							$data['owa'] = $this->db->query('SELECT * from tbl_form_camaba where gelombang = "'.$this->session->userdata('gelombang').'" and prodi = "'.$logged['userid'].'"')->result();
						}

					}
					
				} else {

					if ($this->session->userdata('gelombang') == '0') {

						if ($this->session->userdata('tahun') == '2016') {
							$data['owa'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "'.$logged['userid'].'" and jenis_pmb = "'.$this->session->userdata('jenis').'"')->result();
						} else {
							$data['owa'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "'.$logged['userid'].'" and jenis_pmb = "'.$this->session->userdata('jenis').'"')->result();
						}

					} else {

						if ($this->session->userdata('tahun') == '2016') {
							$data['owa'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where jenis_pmb = "'.$this->session->userdata('jenis').'" and gelombang = "'.$this->session->userdata('gelombang').'" and prodi = "'.$logged['userid'].'"')->result();
						} else {
							$data['owa'] = $this->db->query('SELECT * from tbl_form_camaba where jenis_pmb = "'.$this->session->userdata('jenis').'" and gelombang = "'.$this->session->userdata('gelombang').'" and prodi = "'.$logged['userid'].'"')->result();
						}
						
					}
					
				}
				
				$this->load->view('v_maba_prodis1', $data);

			} elseif ($this->session->userdata('cuss') == 's2') {

				if ($this->session->userdata('jenis') == 'ALL') {

					if ($this->session->userdata('gelombang') == '0') {

						if ($this->session->userdata('tahun') == '2016') {
							$data['owe'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$logged['userid'].'"')->result();
						} else {
							$data['owe'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "'.$logged['userid'].'"')->result();
						}

					} else {
						if ($this->session->userdata('tahun') == '2016') {
							$data['owe'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where gelombang = "'.$this->session->userdata('gelombang').'" and opsi_prodi_s2 = "'.$logged['userid'].'"')->result();
						} else {
							$data['owe'] = $this->db->query('SELECT * from tbl_pmb_s2 where gelombang = "'.$this->session->userdata('gelombang').'" and opsi_prodi_s2 = "'.$logged['userid'].'"')->result();
						}
						
						

					}
					
				} else {

					if ($this->session->userdata('gelombang') == '0') {

						if ($this->session->userdata('tahun') == '2016') {
							$data['owe'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$logged['userid'].'" and jenis_pmb = "'.$this->session->userdata('jenis').'"')->result();
						} else {
							$data['owe'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "'.$logged['userid'].'" and jenis_pmb = "'.$this->session->userdata('jenis').'"')->result();
						}

					} else {

						if ($this->session->userdata('tahun') == '2016') {
							$data['owe'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where jenis_pmb = "'.$this->session->userdata('jenis').'" and gelombang = "'.$this->session->userdata('gelombang').'" and opsi_prodi_s2 = "'.$logged['userid'].'"')->result();
						} else {
							$data['owe'] = $this->db->query('SELECT * from tbl_pmb_s2 where jenis_pmb = "'.$this->session->userdata('jenis').'" and gelombang = "'.$this->session->userdata('gelombang').'" and opsi_prodi_s2 = "'.$logged['userid'].'"')->result();
						}
						
					}
					//$data['owe'] = $this->db->query('SELECT * from tbl_pmb_s2 where jenis_pmb = "'.$this->session->userdata('jenis').'" and gelombang = "'.$this->session->userdata('gelombang').'" and opsi_prodi_s2 = "'.$logged['userid'].'"')->result();
					
				}

				$this->load->view('v_maba_prodis2', $data);
			}

		}
		
	}

	function unggahberkas()
	{
		$usr = $this->input->post('user');
		$typ = $this->input->post('tipe');
		$key = $this->input->post('key');

		// var_dump($_SERVER['SERVER_ADDR']);exit();

		$getid = $this->db2->query("SELECT * from tbl_form_pmb where user_input = '".$usr."' and status_form = '1'")->row()->id_form;

		// var_dump($getid);exit();

		if ($_FILES['userfile']) {
			$this->load->helper('inflector');
			$nama = underscore(str_replace('/', '', $_FILES['userfile']['name']));

			$kadal = explode('.', $nama);
			if (count($kadal) > 2) {
				echo "<script>alert('FORMAT FILE MENCURIGAKAN!');history.go(-1);</script>";exit();
			} 

            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_size'] = '10240';
            $config['file_name'] = $usr.$nama;
            $config['upload_path'] = '/usr/share/nginx/html/pmb/file_image/';
            // var_dump(dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR.'file_image/');exit();
            $expForPath = explode('/', $config['upload_path']);
            $pth = '/'.$expForPath[6].'/'.$usr.$nama;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload("userfile")) {
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
            } else {
                $data = array(
					'userid'	=> $usr,
					'path'		=> $pth,
					'tipe'		=> $typ,
					'tgl'		=> date('Y-m-d h:i:s'),
					'valid'		=> 2,
					'key_booking'	=> $key
					);

				// cek apakah file sudah ada
				$avb = $this->db2->where('userid', $usr)->where('tipe',$typ)->get('tbl_file')->num_rows();
				if ($avb > 0) {
						unlink(base_url().$avb->path);
						$obj = array('path' => $pth, 'tgl' => date('Y-m-d h:i:s'), 'valid' => 2);
						$this->db2->where('userid', $usr)
								->where('tipe',$typ)
								->where('key_booking', $key)
								->update('tbl_file',$obj);
					} else {
						// var_dump($data);exit();
						$this->db2->insert('tbl_file',$data);
					}	
				redirect(base_url('datas/mahasiswapmb/load_lengkap_new/'.$getid.'/'.$key));
            }
        }
	}

	function load_det_berkas($u,$t,$k)
	{
		$data['getup'] = $this->db2->query("SELECT * from tbl_file where userid = '".$u."' and tipe = '".$t."' and key_booking = '".$k."'")->row();
		$this->load->view('detail_modal_berkaspmb', $data);
	}

	function validAllFile($u,$k)
	{
		$getid = $this->db2->query("SELECT id_form from tbl_form_pmb where user_input = '".$u."' and status_form = '1'")->row()->id_form;

		$obj = array('valid' => 1);
		$eki = array('status_kelengkapan' => 1);

		$this->db2->where('userid', $u);
		$this->db2->where('key_booking', $k);
		$this->db2->update('tbl_file', $obj);

		$this->db2->where('user_input', $u);
		$this->db2->update('tbl_form_pmb', $eki);

		redirect(base_url('datas/mahasiswapmb/load_lengkap_new/'.$getid.'/'.$k));
	}

	function voidfile($u,$t,$k)
	{
		$getid = $this->db2->query("SELECT id_form from tbl_form_pmb where user_input = '".$u."' and status_form = '1'")->row()->id_form;

		$obj = array('valid' => 0);

		$this->db2->where('userid', $u);
		$this->db2->where('tipe', $t);
		$this->db2->where('key_booking', $k);
		$this->db2->update('tbl_file', $obj);

		redirect(base_url('datas/mahasiswapmb/load_lengkap_new/'.$getid.'/'.$k));
	}

}

/* End of file Mahasiswapmb.php */
/* Location: ./application/modules/datas/controllers/Mahasiswapmb.php */