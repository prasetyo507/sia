<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_remid extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('temph_model');
	}

	public function index()
	{
		$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = "v_listremid";
		$this->load->view('template', $data);
	}

	function save_session()
	{
		$var = $this->input->post('tahunajaran');
		$this->session->set_userdata('ta_remid', $var);
		redirect(base_url('datas/list_remid/load_remid'));
	}

	function load_remid()
	{
		$log = $this->session->userdata('sess_login');
		$data['user'] = $log['userid'];
		$data['thn'] = $this->session->userdata('ta_remid');
		$data['query'] = $this->temph_model->load_pst_remid($log['userid'],$this->session->userdata('ta_remid'))->result();
		$data['page'] = "v_pesertaremid";
		$this->load->view('template', $data);
	}

	function detil_pst($tahun,$prodi,$kode,$jdl)
	{
		$data['tahuns'] = $this->session->userdata('ta_remid');
		$data['load'] = $this->temph_model->list_remid_detil($tahun,$prodi,$kode,get_kd_jdl($jdl))->result();
		$data['kd'] = get_kd_jdl($jdl);
		$data['page'] = "v_detil_pstremid";
		$this->load->view('template', $data);
	}

}

/* End of file List_remid.php */
/* Location: ./application/modules/datas/controllers/List_remid.php */