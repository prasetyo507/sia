<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(49)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}


	public function index()
	{

		$data['getData']=$this->app_model->getdata('tbl_divisi','id_divisi','asc')->result();
		$data['page']= "data/divisi_view";
		$this->load->view('template', $data);
	}

	function savedata()
	{
		$data['kd_divisi'] = $this->input->post('kode', TRUE);
		$data['divisi'] = $this->input->post('divisi', TRUE);
		$this->app_model->insertdata('tbl_divisi',$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."data/divisi';</script>";
	}

	function deletedata($id)
	{
		$this->app_model->deletedata('tbl_divisi','id_divisi',$id);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."data/divisi';</script>";	
	}

}

/* End of file Divisi.php */
/* Location: ./application/modules/data/controllers/Divisi.php */