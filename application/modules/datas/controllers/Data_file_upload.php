<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_file_upload extends CI_Controller {

	public function index()
	{
		$q = $this->session->userdata('sess_login');
		$user = $q['userid'];// die($user);
		$data['pengguna'] = $this->db->query('SELECT * from tbl_jurusan_prodi where kd_prodi = "'.$user.'"')->row();

		$data['dt'] = $this->db->query('SELECT a.nama_file,d.nama_matakuliah,c.nama,b.kelas,a.waktu,a.ip_addr,a.mac_addr from tbl_file_upload a join tbl_jadwal_matkul b on a.kode=b.kd_jadwal
										join tbl_karyawan c on c.nid=b.kd_dosen
										join tbl_matakuliah d on d.kd_matakuliah=b.kd_matakuliah
										where userid = "'.$user.'" 
										order by a.waktu desc')->result();
		$data['page'] = 'v_data_upload';
		$this->load->view('template', $data);
	}

}

/* End of file Data_file_upload.php */
/* Location: ./application/modules/data/controllers/Data_file_upload.php */