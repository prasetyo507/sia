<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=Data_pertemuan_MK_.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
error_reporting(0);
?>


<style>
table, td, th {
    border: 2px solid black;
}


th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <h3>Matakuliah <?php echo $look->nama_matakuliah.'('.$look->kd_matakuliah.') - '.$look->kelas; ?></h3>
    <thead>
        <tr> 
            <th>No</th>
            <th>Pertemuan</th>
            <th>Bahasan</th>
            <th>Tanggal</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($pertemuan as $isi) { ?>
            <tr>
                <td><?php echo number_format($no); ?></td>
                <td><?php echo "Pertemuan Ke-".$isi->pertemuan; ?></td>
                <?php $mat = $this->db->query("SELECT materi from tbl_materi_abs where pertemuan = '".$isi->pertemuan."' and kd_jadwal = '".$code."'")->row(); ?>
                <td>
                    <?php 
                        if (is_null($mat->materi) || $mat->materi == '') 
                        {
                            echo "";
                        } else {
                            echo $mat->materi;
                        }
                    ?>
                </td>
                <td><?php echo $isi->tanggal; ?></td>
            </tr>    
        <?php $no++; } ?>
    </tbody>
</table>