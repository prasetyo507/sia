<script type="text/javascript">
    $(function() {
        $("#example4").dataTable();
    });
</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> Daftar Hadir Mahasiswa - Pertemuan Ke-<?php echo $pertemuan ?></h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/ajar/edit_kelas_absen/<?php echo $id; ?>" method="post">
    <div class="modal-body">  
        <div class="control-group" id="">
            <fieldset>
                <div class="control-group" style="margin-left: -20px;">
                    <label class="control-label">Tanggal Pertemuan</label>
                    <div class="controls">
                        <input class="span2" type="text" name="tgl" value="<?php echo $tanggal; ?>" id="tgl" required readonly/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="alert-danger">
                        Jumlah Kehadiran : <b>Hadir(H) = <?php echo $h->jums; ?></b> | <b>Sakit(S) = <?php echo $s->jums; ?></b> | <b>Izin(I) = <?php echo $i->jums; ?></b> |
                        <b>Alfa(A) = <?php echo $a->jums; ?></b>
                    </div>
                </div>
            </fieldset>

            <table id="example2" class="table table-bordered table-striped">
            	<thead>
                    <tr> 
                        <th>No</th>
                        <th width="90">NIM</th>
                        <th>NAMA</th>
                        <th width="20">KETERANGAN</th>
                    </tr>
                </thead>
                <tbody>
                	<?php $no = 1; foreach ($kelas as $value) { ?>
                	<!--input type="hidden" name="jadwal" value="<?php //echo $value->kd_jadwal; ?>"/-->
                	<tr>
                		<td><?php echo number_format($no); ?></td>
                		<td><?php echo $value->npm_mahasiswa; ?></td>
                		<td><?php echo $value->NMMHSMSMHS; ?></td>
                		<td><?php echo $value->kehadiran; ?></td>
                        
                	</tr>
                	<?php $no++; } ?>
                </tbody>
            </table>
            <script>
                $(function() {
                    $( "#tgl" ).datepicker({
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: "yy-mm-dd",
                        minDate: -150,
                        maxDate: 0,
                    });
                    $("#tgl").keydown(function (event) {
                        event.preventDefault();
                    });
                });
            </script>
            
        </div>              
    </div> 
    <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> -->
        <!-- <input type="submit" class="btn btn-primary" value="Simpan"/> -->
    </div>
</form>