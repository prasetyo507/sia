

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<script>
function edit(id,id2){
$('.modal-content').empty();
$('#myModal').removeData('bs.modal');
$('#absen').load('<?php echo base_url();?>absensi/modal_edit_absen_copy/'+id+'/'+id2);
}
</script>

<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Daftar Data Absensi</h3>

            </div> <!-- /widget-header -->

            

            <div class="widget-content">

                <div class="span11">
                    <a href="<?php echo base_url(); ?>absensi/cetak/" class="btn btn-success">Print Excel</a>
                    <hr>
                   <!-- <a href="<?php //echo base_url();?>akademik/ajar/cetak/" class="btn btn-success "><i class="btn-icon-only icon-print"> </i> Export Excel</a><hr> -->

                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 

                                <th>No</th>

                                <th>Pertemuan</th>

                                <th>Bahasan</th>

                                <th>Tanggal</th>

                                <th width="40">Aksi</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no=1; foreach ($pertemuan as $isi): ?>

                                <tr>

                                    <td><?php echo number_format($no); ?></td>

                                    <td><?php echo "Pertemuan Ke-".$isi->pertemuan; ?></td>
                                    <?php $act = getactyear(); $mat = $this->db->query("SELECT materi from tbl_materi_abs_".$act." where pertemuan = '".$isi->pertemuan."' and kd_jadwal = '".$code."'")->row(); ?>
                                    <td>
                                        <?php 
                                            if (is_null($mat->materi) || $mat->materi == '') 
                                            {
                                                echo "";
                                            } else {
                                                echo $mat->materi;
                                            }
                                        ?>
                                      </td>

                                    <td><?php echo $isi->tanggal; ?></td>

                                    <td class="td-actions">
                                        <a data-toggle="modal" onclick='edit(<?php echo "".$id_jadwal.",".$isi->pertemuan."";?>)' href="#myModal" class="btn btn-success btn-small"><i class="btn-icon-only icon-eye-open"> </i></a>
                                        <!-- <a data-toggle="modal" onclick='edit(<?php echo "".$id_jadwal.",".$isi->pertemuan."";?>)' href="#myModal" class="btn btn-success btn-small">Edit<i class="btn-icon-only icon-edit"> </i></a> -->
                                        <!-- <a onclick="return confirm('Are you sure you want to delete this item?');" href="<?php echo base_url();?>akademik/ajar/hapus_pertemuan/<?php echo $id_jadwal.'/'.$isi->pertemuan;?>" class="btn btn-default btn-small">Hapus <i class="btn-icon-only icon-trash"> </i></a> -->
                                    </td>

                                </tr>    

                            <?php $no++; endforeach ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="absen">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->