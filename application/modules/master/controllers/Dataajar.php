<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataajar extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		//$this->load->library('Cfpdf');
		//error_reporting(0);
		//if ($this->session->userdata('sess_login') == TRUE) {
			//$cekakses = $this->role_model->cekakses(71)->result();
			//if ($cekakses != TRUE) {
				//echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			//}
		//} else {
			//redirect('auth','refresh');
		//}
	}

	function index()
	{
		$user = $this->session->userdata('sess_login');

		$nik   = $user['userid'];
		
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ( (in_array(10, $grup)) or (in_array(1, $grup)) ) {
			
			$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();

	        $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

	        $data['page']='akademik/ajar_select';

	        $this->load->view('template', $data);

	    }elseif ((in_array(9, $grup))){

			$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

			$data['prodi'] = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas',$nik,'kd_fakultas', 'ASC')->result();

	        $data['page']='akademik/ajar_select_fak';

	        $this->load->view('template', $data);    	

	    }elseif ((in_array(8, $grup) or in_array(19, $grup))) {
	    	    	
	    	$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
	    	//$data['dosen'] = $this->app_model->getdata('tbl_karyawan', '', 'ASC')->result();

	        $data['page']='akademik/ajar_select_dosen';

	        $this->load->view('template', $data);

		}
	}

}

/* End of file Dataajar.php */
/* Location: ./application/modules/akademik/controllers/Dataajar.php */