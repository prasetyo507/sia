<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validasi_maba extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->db2 = $this->load->database('regis', TRUE);
		error_reporting(0);
		$this->load->library('slug');
		$this->load->model('setting_model');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(101)->result();
			$cekakses1 = $this->role_model->cekakses(102)->result();
			if ($cekakses != TRUE or $cekakses1 != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		//201510235005
		$this->load->library('user_agent');
	}

	function index()
	{
		$data['page'] = "v_maba_s1";
		$this->load->view('template', $data);
	}

	function saveSessS1()
	{
		$year = $this->input->post('tahun');
		$this->session->set_userdata( 'sess_year1',$year );
		redirect(base_url('master/validasi_maba/maba_s1'));
	}

	function saveSessS2()
	{
		$year = $this->input->post('tahun');
		$this->session->set_userdata( 'sess_year2',$year );
		redirect(base_url('master/validasi_maba/maba_s2'));
	}

	function maba_s1()
	{
		$data['tahun']= $this->session->userdata('sess_year1');
		$data['rows'] = $this->app_model->getmaba_new($this->session->userdata('sess_year1'),1)->result();
		$data['page'] = 'valid_maba';
		$this->load->view('template', $data);
	}

	function maba_s1_c()
	{
		$year = 2019;
		$data['tahun']= $year;
		$data['rows'] = $this->app_model->getmaba_new($year,1)->result();
		$data['page'] = 'valid_maba';
		$this->load->view('template', $data);
	}

	function index2()
	{
		$data['page'] = "v_maba_s2";
		$this->load->view('template', $data);
	}
	function maba_s2()
	{
		/*$data['koll'] = $this->db->query('SELECT * from tbl_form_camaba_copy a join tbl_jurusan_prodi b
											on a.`prodi`=b.`kd_prodi` join tbl_fakultas c
											on c.`kd_fakultas`=b.`kd_fakultas`
											where a.`status` >= 1 and a.`prodi` = "'.$id.'"')->result();*/
		//$data['rows'] = $this->app_model->getmabas2('2017')->result();
		$data['tahun']= $this->session->userdata('sess_year2');
		$data['rows'] = $this->app_model->getmaba_new($this->session->userdata('sess_year2'),2)->result();
		//var_dump($data);exit();
		$data['page'] = 'valid_maba_s2';
		$this->load->view('template', $data);
	}

	function validasi()
	{
		$logged = $this->session->userdata('sess_login');
        $pecah = explode(',', $logged['id_user_group']);
        $jmlh = count($pecah);
        for ($i=0; $i < $jmlh; $i++) { 
            $grup[] = $pecah[$i];
        }
        if ((in_array(11, $grup))) {
        	$total = count($this->input->post('maba', TRUE));

        	for ($i=0; $i < $total; $i++) { 
        		$datamhs[] = $this->input->post('maba['.$i.']', TRUE);
        	}

        	$newarr = [];
        	foreach ($datamhs as $key) {
        		if (!in_array($key, $newarr)) {
        			array_push($newarr, $key);
        		}
        	}

        	$removeDuplicateData = array_unique($newarr);

        	foreach ($removeDuplicateData as $val) {
        		$v = explode('mb', $val);
				$data['user_renkeu'] = $logged['userid'];
				$data['status'] = $v[0];
				$data['gelombang_du'] = 1;
				$data['tanggal_regis'] = date('Y-m-d');
				$this->createnpms1($v[1]);
				$this->db2->where('nomor_registrasi',$v[1]);
				$this->db2->update('tbl_form_pmb', $data);
        	}

        }else{
        	$total = count($this->input->post('baa', TRUE));
        	for ($i=0; $i < $total ; $i++) { 
				$v = explode('mb', $this->input->post('baa['.$i.']', TRUE));
				$data['user_baa'] = $logged['userid'];
				$data['status'] = $v[0];
				$data['tanggal_regis'] = date('Y-m-d');
				$this->db2->where('nomor_registrasi',$v[1]);
				$this->db2->update('tbl_form_pmb', $data);
				//$this->app_model->updatedata('tbl_form_pmb','nomor_registrasi',$v[1],$data);
			}
        }
        //var_dump($total);exit();
        echo "<script>alert('Sukses');
		document.location.href='".base_url()."master/validasi_maba/maba_s1';</script>";
	}

	function validasis2()
	{
		$logged = $this->session->userdata('sess_login');
        $pecah = explode(',', $logged['id_user_group']);
        $jmlh = count($pecah);
        for ($i=0; $i < $jmlh; $i++) { 
            $grup[] = $pecah[$i];
        }
        if ((in_array(11, $grup))) {
        	$total = count($this->input->post('maba', TRUE));

        	for ($i=0; $i < $total; $i++) { 
        		$datamhs[] = $this->input->post('maba['.$i.']', TRUE);
        	}

        	$newarr = array_unique($datamhs);

        	foreach ($newarr as $val) {
        		$v = explode('mb', $val);
				$data['user_renkeu'] = $logged['userid'];
				$data['status'] = $v[0];
				$data['gelombang_du'] = 1;
				$data['tanggal_regis'] = date('Y-m-d');
				$this->createnpms2($v[1]);
				$this->db2->where('nomor_registrasi',$v[1]);
				$this->db2->update('tbl_form_pmb', $data);
        	}

        }else{
        	$total = count($this->input->post('baa', TRUE));
        	for ($i=0; $i < $total ; $i++) { 
				$v = explode('mb', $this->input->post('baa['.$i.']', TRUE));
				$data['user_baa'] = $logged['userid'];
				$data['status'] = $v[0];
				$data['tanggal_regis'] = date('Y-m-d');
				$this->db2->where('nomor_registrasi',$v[1]);
				$this->db2->update('tbl_form_pmb', $data);
				//$this->app_model->updatedata('tbl_form_pmb','nomor_registrasi',$v[1],$data);
			}
        }
        echo "<script>alert('Sukses');
		document.location.href='".base_url()."master/validasi_maba/maba_s2';</script>";
	}

	function createnpms1($id_regis)
	{
		$year = $this->app_model->getdetail('tbl_tahunakademik','status',1,'kode','asc')->row()->tahun_akademik;
		$exp1 = explode(' - ', $year);
		$exp2 = explode('/', $exp1[0]);
		$fixx = '2019';

		$this->db2->where('nomor_registrasi',$id_regis);
		$this->db2->order_by('nomor_registrasi', 'asc');
		$data = $this->db2->get('tbl_form_pmb')->result();

		foreach ($data as $value) {
			$NIM = $this->setting_model->getnimnews1($value->prodi,$value->jenis_pmb)->row();

			if (is_null($NIM)) {
				
				if ($value->jenis_pmb == 'MB') {
					$kd = 5;
				} else {
					$kd = 7;
				}
				
				if ($value->prodi == '55201') {
					$npmprod = '1022'.$kd.'';
				} elseif ($value->prodi == '26201') {
					$npmprod = '1021'.$kd.'';
				} elseif ($value->prodi == '73201') {
					$npmprod = '1051'.$kd.'';
				} elseif ($value->prodi == '74201') {
					$npmprod = '1011'.$kd.'';
				} elseif ($value->prodi == '61201') {
					$npmprod = '1032'.$kd.'';
				} elseif ($value->prodi == '70201') {
					$npmprod = '1041'.$kd.'';
				} elseif ($value->prodi == '24201') {
					$npmprod = '1023'.$kd.'';
				} elseif ($value->prodi == '25201') {
					$npmprod = '1024'.$kd.'';
				} elseif ($value->prodi == '62201') {
					$npmprod = '1031'.$kd.'';
				} elseif ($value->prodi == '32201') {
					$npmprod = '1025'.$kd.'';
				} elseif ($value->prodi == '86206') {
					$npmprod = '1061'.$kd.'';
				} elseif ($value->prodi == '85202') {
					$npmprod = '1062'.$kd.'';
				}
				$npm = $fixx.$npmprod.'001';
				$data1['NIMHSMSMHS']= $npm;
			} else {
				$npm = substr($NIM->NIMHSMSMHS, -3,3)+1;
				if ($npm < 10) {
					$npmbaru = '00'.$npm;
				} elseif ($npm < 100) {
					$npmbaru = '0'.$npm;
				} else {
					$npmbaru = $npm;
				}

				$data1['NIMHSMSMHS']= substr($NIM->NIMHSMSMHS, 0,4).substr($NIM->NIMHSMSMHS, 4,5).$npmbaru;	
			}
			
			$data1['KDPTIMSMHS']= '31036';

			$data1['KDPSTMSMHS']= $value->prodi;

			$data1['NMMHSMSMHS']= strtoupper($value->nama);

			$data1['SHIFTMSMHS']= 'R';

			$data1['TPLHRMSMHS']= $value->tempat_lahir;

			$data1['TGLHRMSMHS']= $value->tgl_lahir;

			$data1['KDJEKMSMHS']= $value->kelamin;

			$data1['TAHUNMSMHS']= $fixx;

			$data1['SMAWLMSMHS']= $fixx.'1';

			$for_limit = $fixx+7;

			$data1['BTSTUMSMHS']= $for_limit.'1';

			$data1['ASSMAMSMHS']= '';

			$data1['TGMSKMSMHS']= date('Y-m-d');

			$data1['STMHSMSMHS']= 'A';

			$data1['FLAG_RENKEU']= 1;

			$data1['STPIDMSMHS']= '';

			$data1['SKSDIMSMHS']= '';

			$data1['NIKMSMHS']= trim($value->nik);

			$data1['NMIBUMSMHS']= strtoupper($value->nm_ibu);

			//var_dump($data1['NIMHSMSMHS']);exit();

			$insert = $this->app_model->insertdata('tbl_mahasiswa',$data1);

			$logged = $this->session->userdata('sess_login');
			$data3['briva_mhs'] = '70306'.substr($data1['NIMHSMSMHS'], 2);
			$data3['npm_mahasiswa'] = $data1['NIMHSMSMHS'];
			$data3['tahunajaran'] = getactyear();
			$data3['status'] = 1;
			$data3['transaksi_terakhir'] = date('Y-m-d');
			$data3['userid'] = $logged['userid'];
			$data3['semester'] = 1;
			$this->app_model->insertdata('tbl_sinkronisasi_renkeu',$data3);

			$data2['npm_baru']= $data1['NIMHSMSMHS'];

			//$this->app_model->updatedata('tbl_form_pmb','nomor_registrasi',$value->nomor_registrasi,$data2);
			
			$this->db2->where('nomor_registrasi',$value->nomor_registrasi);
			$this->db2->update('tbl_form_pmb', $data2);
		}
		//exit();

	}

	function createnpms2($id_regis)

	{
		
		$year = $this->app_model->getdetail('tbl_tahunakademik','status',1,'kode','asc')->row()->tahun_akademik;
		$exp1 = explode(' - ', $year);
		$exp2 = explode('/', $exp1[0]);
		$fixx = '2019';
		
		
		
		$actyear = getactyear();
		// $fixx = substr($actyear, 0,4);
		// $fixx = '2019';

		$this->db2->where('nomor_registrasi',$id_regis);
		$this->db2->order_by('nomor_registrasi', 'asc');
		$data = $this->db2->get('tbl_form_pmb')->result();

		foreach ($data as $value) {
			if (($value->gelombang) > 2) {
				$gelgel = 1;
			} else {
				$gelgel = 2;
			} 
			
			$NIM = $this->setting_model->getnimnews2($value->prodi,$value->jenis_pmb)->row();
			//var_dump($NIM);exit();
			if (count($NIM) == 0) {
				
				if ($value->jenis_pmb == 'MB') {
					$kd = 5;
				} else {
					$kd = 7;
				}

				if ($value->prodi == '61101') {
					$npmprod = '201'.$kd.$gelgel;
				} elseif ($value->prodi == '74101') {
					$npmprod = '202'.$kd.$gelgel;
				}
				//$npms2 = $this->app_model->getdetail('tbl_tahunakademik','status','1','kode','desc')->row()->kode;
				//$npm = $npms2.$npmprod.'001';
				$npm = $fixx.$npmprod.'001';
				$data1['NIMHSMSMHS']= $npm;
			} else {
				$npm = substr($NIM->NIMHSMSMHS, -3,3)+1;
				if ($npm < 10) {
					$npmbaru = '00'.$npm;
				} elseif ($npm < 100) {
					$npmbaru = '0'.$npm;
				} else {
					$npmbaru = $npm;
				}

				$data1['NIMHSMSMHS']= substr($NIM->NIMHSMSMHS, 0,4).substr($NIM->NIMHSMSMHS, 4,5).$npmbaru;
				// var_dump($data1['NIMHSMSMHS']);exit;
			}
			
			// $smt_awal = getactyear();
			$smt_awal = '20191';

			$data1['KDPTIMSMHS']= '031036';

			$data1['KDPSTMSMHS']= $value->prodi;

			$data1['NMMHSMSMHS']= strtoupper($value->nama);

			$data1['SHIFTMSMHS']= 'R';

			$data1['TPLHRMSMHS']= $value->tempat_lahir;

			$data1['TGLHRMSMHS']= $value->tgl_lahir;

			$data1['KDJEKMSMHS']= $value->kelamin;

			$data1['TAHUNMSMHS']= $fixx;

			$data1['SMAWLMSMHS']= $smt_awal;

			// $actyear = getactyear();
			if ($actyear%2 == 0) {
				$postfix = '2';
				$this->db->select('mulai');
				$this->db->from('tbl_kalender_dtl');
				$this->db->like('kd_kalender', $actyear, 'after');
				$this->db->like('kegiatan', 'Kuliah Perdana', 'BOTH');
				$youwillstart = $this->db->get()->row()->mulai;
				
			} else {
				$postfix = '1';
				$startstudy = $actyear+1;

				$this->db->select('mulai');
				$this->db->from('tbl_kalender_dtl');
				$this->db->like('kd_kalender', $startstudy, 'after');
				$this->db->like('kegiatan', 'Kuliah Perdana', 'BOTH');
				$youwillstart = $this->db->get()->row()->mulai;
			}

			$for_limit = $fixx + 4;

			$data1['BTSTUMSMHS']= $for_limit.$postfix;

			$data1['ASSMAMSMHS']= '';

			$data1['TGMSKMSMHS']= $youwillstart;

			$data1['NIKMSMHS']= trim($value->nik);

			$data1['STMHSMSMHS']= 'A';

			$data1['FLAG_RENKEU']= 1;

			$data1['STPIDMSMHS']= '';

			$data1['SKSDIMSMHS']= '';

			$data1['NMIBUMSMHS']= strtoupper($value->nm_ibu);

			//var_dump($data1['NIMHSMSMHS']);exit();

			$insert = $this->app_model->insertdata('tbl_mahasiswa',$data1);

			$logged = $this->session->userdata('sess_login');
			$data3['briva_mhs'] = '70306'.substr($data1['NIMHSMSMHS'], 2);
			$data3['npm_mahasiswa'] = $data1['NIMHSMSMHS'];
			$data3['tahunajaran'] = $actyear;
			$data3['status'] = 1;
			$data3['transaksi_terakhir'] = date('Y-m-d');
			$data3['userid'] = $logged['userid'];
			$data3['semester'] = 1;
			$this->app_model->insertdata('tbl_sinkronisasi_renkeu',$data3);

			$data2['npm_baru']= $data1['NIMHSMSMHS'];

			//$this->app_model->updatedata('tbl_form_pmb','nomor_registrasi',$value->nomor_registrasi,$data2);
			$this->db2->where('nomor_registrasi',$value->nomor_registrasi);
			$this->db2->update('tbl_form_pmb', $data2);

		}
		//exit();

	}

	function detils1($id)
	{
		$data['cek'] = $this->db2->query('SELECT * from tbl_form_pmb where nomor_registrasi = "'.$id.'"')->row();

		$a = array('74101','61101');
		$data['nomor_registrasi'] = $id;

		$this->db->select('*');
		$this->db->from('tbl_jurusan_prodi');
		$this->db->where_not_in('kd_prodi', $a);
		$data['rows'] = $this->db->get()->result();

		$data['page'] = "v_detil_new";
		$this->load->view('template', $data);
	}

	function detils2($id)
	{
		$data['cek'] = $this->db2->query('SELECT * from tbl_form_pmb where nomor_registrasi = "'.$id.'"')->row();
		$data['page'] = "v_detil_new2";
		$this->load->view('template', $data);
	}

	function simpan_form(){

		// $patt = $this->uploadbro($_FILES['userfile']);
		$id = $this->input->post('kd_regis');
		
		$nomor = $this->input->post('nomor_registrasi');
		$nama  = $this->input->post('nama');


			$datax = array(
				'foto'			=> 	$patt,
				'jenis_pmb'		=>	$this->input->post('jenis'),
				'kampus'		=>	$this->input->post('kampus'),
				'kelas'			=>	$this->input->post('kelas'),
				'nama'			=>	$nama,
				'kelamin'		=>	$this->input->post('jk'),
				'status_wn'		=>	$this->input->post('wn'),
				'kewaganegaraan'=>	$this->input->post('wn_txt'),
				'tempat_lahir'	=>	$this->input->post('tpt_lahir'),
				'tgl_lahir'		=>	$this->input->post('tgl_lahir'),
				'agama'			=>	$this->input->post('agama'),
				'status_nikah'	=>	$this->input->post('stsm'),
				'status_kerja'	=>	$this->input->post('stsb'),
				'perkerjaan'	=>	$this->input->post('stsb_txt'),
				'alamat'		=>	$this->input->post('alamat'),
				'kd_pos'		=>	$this->input->post('kdpos'),
				'tlp'			=>	$this->input->post('tlp'),
				'tlp2'			=>	$this->input->post('tlp2'),

				'npm_lama_readmisi'		=>	$this->input->post('npm_readmisi'),
				'tahun_masuk_readmisi'	=>	$this->input->post('thmasuk_readmisi'),
				'smtr_keluar_readmisi'	=>	$this->input->post('smtr_readmisi'),

				'asal_sch_maba'			=>	$this->input->post('asal_sch'),
				'kota_sch_maba'			=>	$this->input->post('kota_sch'),
				'jur_maba'				=>	$this->input->post('jur_sch'),
				'lulus_maba'			=>	$this->input->post('lulus_sch'),
				'jenis_sch_maba'		=> 	$this->input->post('jenis_sch_maba'),

				'asal_pts_konversi'		=>	$this->input->post('asal_pts'),
				'kota_pts_konversi'		=>	$this->input->post('kota_pts'),
				'prodi_pts_konversi'	=>	$this->input->post('prodi_pts'),
				'npm_pts_konversi'		=>	$this->input->post('npm_pts'),
				'tahun_lulus_konversi'	=>	$this->input->post('lulus_pts'),
				'smtr_lulus_konversi'	=>	$this->input->post('smtr_pts'),

				//'prodi'			=>	$this->input->post('prodi'),
				'nm_ayah'		=>	$this->input->post('nm_ayah'),
				'didik_ayah'	=>	$this->input->post('didik_ayah'),
				'workdad'		=>	$this->input->post('workdad'),
				'life_statdad'	=>	$this->input->post('life_statdad'),
				'nm_ibu'		=>	$this->input->post('nm_ibu'),
				'didik_ibu'		=>	$this->input->post('didik_ayah'),
				'workmom'		=>	$this->input->post('workdad'),
				'life_statmom'	=>	$this->input->post('life_statmom'),
				'tanggal_regis'	=>	date('Y-m-d'),
				'penghasilan'	=>	$this->input->post('gaji'),
				//'referensi'		=>	$this->input->post('refer')
				
			);

			$dataxxx = array(
					'NMMHSMSMHS'  => $nama
				);



		//var_dump($datax);exit();
		//$this->app_model->updatedata('tbl_form_camaba_copy','id_form',$id,$datax);
		//$this->uploadbro($_FILES['userfile']);
 		
 		$npmmb = $this->db->where('nomor_registrasi', $nomor)->get('tbl_form_camaba')->row();

 		$this->db->where('NIMHSMSMHS', $npmmb->npm_baru);
		$this->db->update('tbl_mahasiswa', $dataxxx);
		
		$this->db->where('nomor_registrasi', $id);
		$q=$this->db->update('tbl_form_camaba', $datax);

		if ($q == TRUE) {
			echo "<script>alert('Sukses');
				document.location.href='".base_url()."master/validasi_maba';</script>";	
		}else{
			echo "<script>alert('Gagal Update');
				document.location.href='".base_url()."master/validasi_maba';</script>";	
		}

		
	}

	function update_s2()
	{
		/*$this->load->helper('inflector');
		$nama = underscore($_FILES['userfile']['name']);
		$config['upload_path'] = './upload/imgpmb/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['file_name'] = $nama;
		$config['max_size'] = 1000000;
		$this->load->library('upload', $config);
		$patt = $config['upload_path'].basename($nama);*/
		//$patt = $this->uploadbro($_FILES['userfile']);

		$id = $this->input->post('ole');

		$namm = strtoupper($this->input->post('nama'));
		$ayah = strtoupper($this->input->post('nm_ayah'));
		$ibu = strtoupper($this->input->post('nm_ibu'));
		$kota = strtoupper($this->input->post('kota_asal'));

		$datas = array(
				'kampus'		=>	$this->input->post('kampus'),
				'kelas'			=>	'KR',
				'nama'			=>	$namm,
				'kelamin'		=>	$this->input->post('jk'),
				'negara'		=>	$this->input->post('wn'),
				'tempat_lahir'	=>	$this->input->post('tpt'),
				'tgl_lahir'		=>	$this->input->post('tgl'),
				'agama'			=>	$this->input->post('agama'),
				'status_nikah'	=>	$this->input->post('stsm'),
				'status_kerja'	=>	$this->input->post('stsb'),
				'alamat'		=>	$this->input->post('alamat'),
				'kd_pos'		=>	$this->input->post('kdpos'),
				'tlp'			=>	$this->input->post('tlp'),
				'npm_lama'		=>	$this->input->post('npm'),
				'thmsubj'		=>	$this->input->post('thmasuk'),
				'nm_univ'		=>	$this->input->post('nm_pt'),
				//'prodi'			=>	$this->input->post('prodi_lm'),
				'th_lulus'		=>	$this->input->post('thlulus'),
				'kota_asl_univ'	=>	$kota,
				'npm_nim'		=>	$this->input->post('npmnim'),
				//'opsi_prodi_s2'	=>	$this->input->post('prodi'),
				'pekerjaan'		=>	$this->input->post('pekerjaan'),
				'nm_ayah'		=>	$ayah,
				'nm_ibu'		=>	$ibu,
				'penghasilan'	=>	$this->input->post('gaji'),
				//'informasi_from'=>	$this->input->post('infrom'),
				'foto'			=>	$patt,
				'negara_wna'	=>	$this->input->post('jns_wn'),
				'tempat_kerja'	=>	$this->input->post('tpt_kerja'),
				'tanggal_regis'	=>	date('Y-m-d')
			);
		//var_dump($datas);exit();
		$this->app_model->updatedata('tbl_pmb_s2','ID_registrasi',$id,$datas);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."master/validasi_maba/index2';</script>";
	}

	function uploadbro($image){
		$this->load->helper('inflector');
		$nama = underscore($image['name']);
		$config = array(
		'file_name' => $nama,
		'upload_path' => "./upload/imgpmb/",
		'allowed_types' => "gif|jpg|png|jpeg|pdf|PNG|JPG|JPEG",
		'overwrite' => TRUE,
		'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
		'max_height' => "10000000",
		'max_width' => "10000000"
		);
		$this->load->library('upload', $config);
		//$this->upload->do_upload();
		
		if($this->upload->do_upload())
		{
			$data = array('upload_data' => $this->upload->data());
			$path = "./upload/imgpmb/".$nama;
			return $path;
		} else {
			$error = array('error' => $this->upload->display_errors());
			var_dump($error);
		}
	}

	function print_vs1($tahun){
		/* $data['rows'] = $this->db->query('SELECT maba.`nomor_registrasi`,maba.`npm_baru`,maba.`nama`,maba.`status`,maba.`gelombang`,maba.`kelas`,prd.`prodi`
											FROM
											  tbl_form_camaba maba
											 LEFT JOIN tbl_jurusan_prodi prd ON maba.`prodi` = prd.`kd_prodi`
											WHERE STATUS IS NOT NULL 
											ORDER BY maba.`gelombang` ASC')->result(); */
		$data['rows'] = $this->app_model->getmaba_new($tahun,1)->result();

		$this->load->view('print_validasi_maba', $data);
	}

	function print_vs2($tahun){
		$data['rows'] = $this->app_model->getmaba_new($tahun,2)->result();

		$this->load->view('print_validasi_maba', $data);
	}

	/**
	 * fetch all data from each new student using cURL
	 * @param [string] $nomor_reg [nomor regis as uniq key to seach new student's data]
	 * @return [string]
	 */
	function storeBioMaba($nomor_reg)
	{
		$initCurl = curl_init();
		curl_setopt($initCurl, CURLOPT_URL, 'http://172.16.1.5:802/api/v1/biodata');
		curl_setopt($initCurl, CURLOPT_HTTPHEADER, [
			'x-token' => 'Nzg3NTY2S2V5Rm9yUE1C',
			'client-key' => $nomor_reg
		]);
		$result = curl_exec($initCurl);
		curl_close($initCurl);

		return $result;
	}

}

/* End of file Validasi_maba.php */
/* Location: ./application/modules/master/controllers/Validasi_maba.php */