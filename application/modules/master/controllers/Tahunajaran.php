<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahunajaran extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(36)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{	
		$data['getData'] = $this->app_model->getdata('tbl_tahunajaran','tahunajaran','asc')->result();
		$data['page'] = 'master/tahunajaran_view';
		$this->load->view('template',$data);
	}

	function view_edit($id){
		$data['getData'] = $this->app_model->getdetail('tbl_tahunajaran','id_tahunajaran',$id,'tahunajaran','asc')->row();
		$this->load->view('master/tahunajaran_edit',$data);
	}

	function insert_TA(){

		$tahun = $this->input->post('tahun');

		$data = array(
			'tahunajaran' =>   $tahun 
			);

		$this->db->insert('tbl_tahunajaran', $data);

		redirect(base_url('master/tahunajaran'));
	}

	function update_TA(){
		$id 	= $this->input->post('id');
		$tahun  = $this->input->post('tahun');

		$data = array(
			'tahunajaran' =>   $tahun 
			);

		$this->db->where('id_tahunajaran',$id)
				 ->update('tbl_tahunajaran', $data);

		redirect(base_url('master/tahunajaran'));
	}

}

/* End of file tahunajaran.php */
/* Location: ./application/modules/master/controllers/tahunajaran.php */