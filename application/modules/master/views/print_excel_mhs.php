<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_mhs.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>NPM</th>
			<th>NAMA</th>
			<th>FAKULTAS</th>
			<th>JURUSAN</th>
			<th>ANGKATAN</th>
			<th>STATUS</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach ($mhs as $value) { $krs = $this->app_model->getdetail('tbl_verifikasi_krs','npm_mahasiswa',$value->NIMHSMSMHS,'kd_krs','asc')->result(); if ($krs == true) { ?>
		<tr>
			<td><?php echo number_format($no); ?></td>
			<td><?php echo $value->NIMHSMSMHS; ?></td>
			<td><?php echo $value->NMMHSMSMHS; ?></td>
			<td><?php echo $value->fakultas; ?></td>
			<td><?php echo $value->prodi; ?></td>
			<td><?php echo $value->TAHUNMSMHS; ?></td>
			<td>Aktif</td>
		</tr>
		<?php } else { continue; } ?>
		<?php $no++; } ?>
	</tbody>
</table>