<?php

//var_dump($detail_khs);exit();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();


$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',12); 



#$pdf->image('http://172.16.2.42:801/assets/logo.gif',10,10,20);
$pdf->image('http://172.16.2.42:801/assets/logo.gif',10,10,20);

$pdf->Ln(0);

$pdf->Cell(200,5,'FORM DAFTAR ULANG PROGRAM S1',0,3,'C');

$pdf->Ln(2);

$pdf->Cell(200,10,'UNIVERSITAS BHAYANGKARA',0,5,'C');

$pdf->Ln(0);

$pdf->Cell(200,10,'JAKARTA RAYA',0,1,'C');

$pdf->Ln(3);

$pdf->Cell(250,0,'',1,1,'L');


// $q=$this->db->query('SELECT a.`NIMHSMSMHS`,a.`NMMHSMSMHS`,a.`TAHUNMSMHS`,a.`SMAWLMSMHS`,b.`kd_prodi`,b.`prodi`,c.`fakultas`,d.`kd_krs`,d.`kd_matakuliah`,e.`id_pembimbing`,f.`nama`,f.`nid` 

// 					FROM tbl_mahasiswa a

// 					LEFT JOIN tbl_jurusan_prodi b ON a.`KDPSTMSMHS` = b.`kd_prodi`

// 					LEFT JOIN tbl_fakultas c ON c.`kd_fakultas` = b.`kd_fakultas`

// 					LEFT JOIN tbl_krs d ON d.`npm_mahasiswa` = a.`NIMHSMSMHS`

// 					LEFT JOIN tbl_verifikasi_krs e ON e.`kd_krs` = d.`kd_krs`

// 					LEFT JOIN tbl_karyawan f ON e.`id_pembimbing` = f.`nid`

// 					WHERE a.`NIMHSMSMHS` = '.$npm.' and e.tahunajaran = "20151"')->row();

// $logged = $this->session->userdata('sess_login');
// $prodi = $logged['userid'];
// $hitung_ips = $this->db->query('SELECT a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
// JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
// WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$prodi.'" AND NIMHSTRLNM = "'.$q->NIMHSMSMHS.'" and THSMSTRLNM = "'.substr($q->kd_krs, 12,5).'" ')->result();

// $st=0;
// $ht=0;
// foreach ($hitung_ips as $iso) {
// 	$h = 0;


// 	$h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
// 	$ht=  $ht + $h;

// 	$st=$st+$iso->sks_matakuliah;
// }

// $ips_nr = $ht/$st;
// $ipk = $this->app_model->get_ipk_mahasiswa($q->NIMHSMSMHS)->row()->ipk;

$pdf->ln(6);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(61.5,5,'No Pokok Mahasiswa',0,0);

$pdf->Cell(8.5,5,':',0,0,'C');

$pdf->Cell(60,5,$kampret->npm_baru,0,0,'L');



$pdf->ln(8);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(62,5,'Nama Lengkap Sesuai Ijazah',0,0);

$pdf->Cell(8,5,':',0,0,'C');

$pdf->Cell(60,5,$kampret->nama,0,0,'L');



$pdf->ln(8);

$pdf->Cell(65,5,'Nama Lengkap Sesuai Akte Kelahiran',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(60,5,$kampret->nama,0,0,'L');




$pdf->ln(8);

$pdf->Cell(65,5,'Tempat Tanggal lahir',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(60,5,$kampret->tempat_lahir.' , '.TanggalIndo($kampret->tgl_lahir),0,0,'L');



$pdf->ln(8);

$pdf->Cell(65,5,'Nama Ibu',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(60,5,$kampret->nm_ibu,0,0,'L');



$pdf->ln(8);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(65,5,'Asal Sekolah',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(5,5,'-',0,0,'L');

if($kampret->jenis_pmb == 'MB'){
	$pdf->Cell(42,5,'Nama Sekolah & Jurusan',0,0);
	$pdf->Cell(5,5,':',0,0,'L');
	$pdf->Cell(60,7,$kampret->asal_sch_maba.' - '.$kampret->jur_maba,0,0,'L');
} else {
	$pdf->Cell(42,5,'Nama Universitas & Jurusan',0,0);
	$pdf->Cell(5,5,':',0,0,'L');
	$pdf->Cell(60,7,$kampret->asal_pts_konversi.' - '.$kampret->prodi_pts_konversi,0,0,'L');
}



// $pdf->ln(5);

// $pdf->SetFont('Arial','',10); 

// $pdf->Cell(65,5,'Nama Sekolah & Jurusan',0,0);

// $pdf->Cell(5,5,':',0,0,'L');

// $pdf->Cell(60,5,'',0,0,'L');



$pdf->ln(8);

$pdf->Cell(70,5,'',0,0,'L');

$pdf->Cell(5,5,'-',0,0,'L');

$pdf->Cell(42,5,'Tahun Lulus',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(60,5,$kampret->lulus_maba,0,0,'L');



$pdf->ln(8);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(65,5,'Alamat',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->MultiCell(100,5,$kampret->alamat,0,'L');



$pdf->ln(8);

$pdf->Cell(65,5,'Agama',0,0);

$pdf->Cell(5,5,':',0,0,'L');

switch ($kampret->agama) {
	case 'ISL':
		$agamanya = 'ISLAM';
		break;
	case 'KTL':
		$agamanya = 'KATOLIK';
		break;
	case 'PRT':
		$agamanya = 'PROTESTAN';
		break;
	case 'HND':
		$agamanya = 'HINDU';
		break;
	case 'BDH':
		$agamanya = 'BUDHA';
		break;
	case 'OTH':
		$agamanya = 'LAIN LAIN';
		break;
}

$pdf->Cell(60,5,$agamanya,0,0,'L');



$pdf->ln(8);

$pdf->Cell(65,5,'Nomor Telepon',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(60,5,$kampret->tlp,0,0,'L');



$pdf->ln(8);

$pdf->Cell(65,5,'Pekerjaan Orang Tua',0,0);

$pdf->Cell(5,5,':',0,0,'L');

if ($kampret->workdad == 'WU') {
	$kalo = "WIRAUSAHA";
} elseif ($kampret->workdad == 'PN') {
	$kalo = "PEGAWAI NEGERI";
} elseif ($kampret->workdad == 'TP') {
	$kalo = "TNI / POLRI";
} elseif ($kampret->workdad == 'PS') {
	$kalo = "PEGAWAI SWASTA";
} elseif ($kampret->workdad == 'PSN') {
	$kalo = "PENSIUN";
} elseif ($kampret->workdad == 'TK') {
	$kalo = "TIDAK KERJA";
} elseif ($kampret->workdad == 'LL') {
	$kalo = "LAIN-LAIN";
}

$pdf->Cell(60,5,$kalo,0,0,'L');



$pdf->ln(8);

$pdf->Cell(65,5,'Kampus',0,0);

$pdf->Cell(5,5,':',0,0,'L');

if($kampret->kampus == 'bks') {
	$xx =  "Bekasi";
} else {
	$xx = "Jakarta";
}

$pdf->Cell(15,5,$xx,0,0,'L');

$pdf->Cell(60,5,'',0,0,'L');



$pdf->ln(8);

$pdf->Cell(65,5,'Kelas Pilihan',0,0);

$pdf->Cell(5,5,':',0,0,'L');

if($kampret->kelas == 'PG') { $kr =  "Pagi"; } elseif($kampret->kelas == 'SR') { $kr =  "Sore"; } else { $kr =  "Karyawan"; }

$pdf->Cell(15,5,$kr,0,0,'L');

$pdf->Cell(60,5,'',0,0,'L');



$pdf->ln(8);

$pdf->Cell(65,5,'Tanggal Pendaftaran',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(60,5,$kampret->tanggal_regis,0,0,'L');



// $pdf->ln(10);

// $pdf->SetFont('Arial','B',10); 

// $pdf->Cell(40,10,'Identitas Mahasiswa',0,0);

// $pdf->Cell(10,10,'',0,0,'C');

// $pdf->Cell(60,10,'',0,0,'C');

// $pdf->Cell(40,10,'Identitas Dosen PA',0,0);

// $pdf->Cell(10,10,'',0,0,'L');

// $pdf->Cell(60,10,'',0,0,'L');


// $pdf->ln(5);

// $pdf->SetFont('Arial','',10); 

// $pdf->Cell(40,10,'Nama Mahasiswa',0,0);

// $pdf->Cell(10,10,':',0,0,'C');

// $pdf->Cell(60,10,' ',0,0,'L');

// $pdf->Cell(40,10,'Nama Dosen PA',0,0);

// $pdf->Cell(5,5,':',0,0,'L');

// //$pdf->Cell(60,10,$q->nama,0,0,'L')MultiCell(60, 10, $q->nama, 0,'L');
// $pdf->MultiCell(50, 5,' ', 0,'L');


// $pdf->ln(7);

// $pdf->SetFont('Arial','',10); 

// $pdf->Cell(40,5,'NPM',0,0);

// $pdf->Cell(10,5,':',0,0,'C');

// $pdf->Cell(60,5,' ',0,0,'L');

// $pdf->Cell(40,5,'NIDN',0,0);

// $pdf->Cell(5,5,':',0,0,'L');

// $pdf->Cell(60,5,' ',0,0,'L');



// $pdf->SetFont('Arial','',10); 

// $pdf->Cell(40,5,'',0,0);

// $pdf->Cell(10,5,'',0,0,'C');

// $pdf->Cell(60,5,'',0,0,'C');

// $pdf->SetFont('Arial','',12);

// $pdf->Cell(40,5,'',0,0);

// $pdf->Cell(10,5,'',0,0,'C');

// $pdf->Cell(60,5,'',0,1,'L');


// $pdf->SetFont('Arial','',10); 

// $pdf->Cell(40,5,'IPS/IPK',0,0);

// $pdf->Cell(10,5,':',0,0,'C');


// $pdf->ln(10);


// $pdf->SetFont('Arial','',10); 

// $pdf->Cell(40,5,'SKS TERAMBIL',0,0);

// $pdf->Cell(10,5,':',0,0,'C');

// $pdf->Cell(60,5,'',0,0,'C');

// $pdf->Cell(40,5,'Jumlah SKS Lulus',0,0);

// $pdf->Cell(5,5,':',0,0,'C');

// $pdf->Cell(60,5,'',0,0,'L');


// $pdf->ln(10);

// $pdf->SetLeftMargin(10);

// $pdf->SetFont('Arial','',10); 

// $pdf->Cell(15,8,'NO',1,0,'C');

// $pdf->Cell(45,8,'KODE MATAKULIAH',1,0,'C');

// $pdf->Cell(100,8,'NAMA MATAKULIAH',1,0,'C');

// $pdf->Cell(15,8,'SKS',1,0,'C');

// $pdf->Cell(15,8,'NILAI',1,1,'C');



// $no=1;

// $total_sks=0;


// $no = 1; foreach ($krs as $row) {

// 	$pdf->Cell(15,8,number_format($no),1,0,'C');

// 	$pdf->Cell(45,8,$row->kd_matakuliah,1,0,'C');

// 	$pdf->SetFont('Arial','',8);

// 	$pdf->Cell(100,8,$row->nama_matakuliah,1,0,'L');
// 	//$pdf->MultiCell(50, 5, $q->nama, 0,'L');

// 	$pdf->SetFont('Arial','',10); 
// 	$pdf->Cell(15,8,$row->sks_matakuliah,1,0,'C');

	

// 	$adanilai = $this->db->query('SELECT count(*) as jml FROM tbl_transaksi_nilai WHERE NIMHSTRLNM  = '.$q->NIMHSMSMHS.' and KDKMKTRLNM ="'.$row->kd_matakuliah.'"')->row();

	
// 	if ($adanilai->jml >=1) {
// 		$que = $this->db->query('SELECT * FROM tbl_transaksi_nilai WHERE NIMHSTRLNM  = '.$q->NIMHSMSMHS.' and KDKMKTRLNM ="'.$row->kd_matakuliah.'"')->row();
// 		$pdf->Cell(15,8,$que->NLAKHTRLNM,1,1,'C');		
// 	}else {
// 		$pdf->Cell(15,8,'-',1,1,'C');
// 	}

	

// 	$total_sks = $total_sks + $row->sks_matakuliah;

// 	$no++;

// }


// $pdf->Cell(160,8,'Total',1,0,'C');

// $pdf->Cell(15,8,'',1,0,'C');

// $pdf->Cell(15,8,'',1,1,'C');


$pdf->ln(13);

$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',12);

date_default_timezone_set('Asia/Jakarta'); 

$pdf->Cell(170,5,'Jakarta,'.date('d-m-Y').'',0,0,'R');


$pdf->ln(8);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(145,5,'',0,0);

// $pdf->Cell(75,5,'',0,0,'C');

// $pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(30,5,'TTD',0,0);

$pdf->Cell(60,5,'',0,0,'L');

$pdf->Cell(10,5,'',0,1,'C');



$pdf->ln(20);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(10,5,'',0,0);

$pdf->Cell(5,5,'',0,0,'C');

$current_y = $pdf->GetY();
$current_x = $pdf->GetX();

$cell_width = 80;
$pdf->MultiCell($cell_width, 5,' ', 0,'L',false);
//$pdf->Cell(80,5,,0,0,'C');

$pdf->ln(5);

$x = 80;
$pdf->SetXY($current_x + $x, $current_y);

//$pdf->Cell(80,5,$q->NMMHSMSMHS,0,0,'C');

$pdf->Cell(30,5,'Nama',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->MultiCell(80, 5,$kampret->nama, 0,'L');
//$pdf->Cell(80,5,$q->nama,0,0,'L');

$pdf->SetFont('Arial','',12); 

$pdf->Cell(46,5,'',0,0);

$pdf->Cell(5,5,'',0,0,'C');

$pdf->Cell(44,5,' ',0,0,'L');

$pdf->Cell(30,5,'No. Registrasi',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(80,5,$kampret->nomor_registrasi,0,0,'L');





$pdf->Output('FORM_DAFTAR_ULANG.PDF','I');



?>

