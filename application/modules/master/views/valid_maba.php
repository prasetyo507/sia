<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>form/formcuti/looad/'+edc);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Daftar Validasi Mahasiswa Baru</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <div class="alert alert-danger">
                        <strong>Perhatian!</strong>
                        Jika nama mahasiswa tidak muncul di daftar, pastikan mahasiswa tersebut telah melengkapi berkas prasyarat yang harus diunggah!
                    </div>
                    <form action="<?php echo base_url(); ?>master/validasi_maba/validasi" method="post">
                        <a class="btn btn-success btn-large" href="<?php echo base_url(); ?>master/validasi_maba/print_vs1/<?php echo $tahun ?>"><i class="btn-icon-only icon-print">Print</i></a>
                        <input type="submit" class="btn btn-primary btn-large" value="Submit">
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>ID Registrasi</th>
                                    <th>NIM BARU</th>
                                    <th>Nama</th>
                                    <th>Pilihan</th>
                                    <th>Kelas</th>
                                    <?php 
                                    $logged = $this->session->userdata('sess_login');
                                    $pecah = explode(',', $logged['id_user_group']);
                                    $jmlh = count($pecah);
                                    for ($i=0; $i < $jmlh; $i++) { 
                                        $grup[] = $pecah[$i];
                                    }
                                    if ((in_array(11, $grup))) { ?>
                                        <th>Daftar Ulang</th>
                                    <?php } elseif ((in_array(10, $grup))) { ?>
                                        <th>Renkeu</th>
                                        <th>BAA</th>
                                        <th>Lihat</th>
                                        <th>Print</th>
                                    <?php } ?>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($rows as $value) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $value->nomor_registrasi; ?></td>
                                    <td><?php echo $value->npm_baru; ?></td>
                                    <td><?php echo $value->nama; ?></td>
                                    <td><?php echo get_jur($value->prodi); ?></td>

                                    <?php if (strtoupper($value->kelas) == 'PG') {
                                        $kelas = 'REG-PAGI';
                                    } elseif (strtoupper($value->kelas) == 'SR') {
                                        $kelas = 'REG-SORE';
                                    } else {
                                        $kelas = 'P2K';
                                    } ?>

                                    <td><?php echo $kelas; ?></td>
                                    <?php if ((in_array(11, $grup))) { ?>
                                        <?php if ($value->status > 2) { ?>
                                            <td><input type="checkbox" name="maba[]" value="3mb<?php echo $value->nomor_registrasi; ?>" checked disabled /></td>
                                        <?php } else { ?>
                                            <td><input type="checkbox" name="maba[]" value="3mb<?php echo $value->nomor_registrasi; ?>"/></td>
                                        <?php } ?>

                                    <?php } elseif ((in_array(10, $grup))) { ?>

                                        <?php if ($value->status == 3) { ?>
                                            <td><input type="checkbox" name="maba[]" value="3mb<?php echo $value->nomor_registrasi; ?>" checked disabled /></td>
                                            <td><input type="checkbox" name="baa[]" value="4mb<?php echo $value->nomor_registrasi; ?>"/></td>
                                        <?php } elseif ($value->status > 3) { ?>
                                            <td><input type="checkbox" name="maba[]" value="3mb<?php echo $value->nomor_registrasi; ?>" checked disabled /></td>
                                            <td><input type="checkbox" name="baa[]" value="4mb<?php echo $value->nomor_registrasi; ?>" checked disabled /></td>

                                        <?php } else { ?>
                                            <td><input type="checkbox" name="maba[]" value="3mb<?php echo $value->nomor_registrasi; ?>" disabled /></td>
                                            <td><input type="checkbox" name="baa[]" value="4mb<?php echo $value->nomor_registrasi; ?>" disabled /></td>
                                        <?php } ?>

                                        <?php 
                                        $logged = $this->session->userdata('sess_login');
                                        $pecah = explode(',', $logged['id_user_group']);
                                        $jmlh = count($pecah);
                                        for ($i=0; $i < $jmlh; $i++) { 
                                            $grup[] = $pecah[$i];
                                        }
                                        if ((in_array(11, $grup))) { ?>
                                            <td><a href="<?php echo base_url(); ?>master/validasi_maba/detils1/<?php echo $value->nomor_registrasi; ?>" class="btn btn-success btn-small"><i class="icon-check"></i></a></td>
                                        
                                        <?php } elseif ((in_array(10, $grup))) { ?>
                                            <td><a href="<?php echo base_url(); ?>master/validasi_maba/detils1/<?php echo $value->nomor_registrasi; ?>" class="btn btn-success btn-small"><i class="icon-check"></i></a></td>
                                            <td><a href="<?php echo base_url(); ?>master/print_hasil/print_hsl/<?php echo $value->nomor_registrasi; ?>" class="btn btn-primary btn-small"><i class="icon-print"></i></a></td>
                                        <?php } ?>
                                        
                                    <?php } ?>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>