<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pmb extends CI_Model {

	function __construct()
	{
		parent::__construct();

	}

	function amountOfFile($uidmaba,$uidprodi)
	{
		$this->dbreg = $this->load->database('regis', TRUE);

		$this->db->where('userid', $uidmaba);
		$fileamount = $this->db->get('tbl_file')->num_rows();

		$this->db->where('user_input', $uidmaba);
		$this->db->where('status_form', 1);
		$detailmaba = $this->db->get('tbl_form_pmb')->row();

		if ($detailmaba->program == '1') {
			if ($detailmaba->jenis_pmb == 'MB') {
				if ($fileamount < 6) {
					return '<button class="btn btn-danger" 
									onclick="alert(\'MOHON LENGKAPI BERKAS-BERKAS PRASYARAT ANDA!\')">
									<i class="icon icon-times"></i>
							</button>';
				} else {
					$jum = $this->db->where('userid',$log['userid'])->where('valid',1)->where('key_booking',$key)->get('tbl_file')->num_rows();
					if ($jum < 6) {
						return '<button class="btn btn-danger" 
									onclick="alert(\'BEBERAPA BERKAS ANDA BELUM/TIDAK TERVALIDASI!\')">
									<i class="icon icon-times"></i>
								</button>';
					} else {
						
					}
				}
			} else {
				if ($kvs1 != $fileamount) {
					echo "<script>alert('MOHON LENGKAPI BERKAS-BERKAS PRASYARAT ANDA!');history.go(-1);</script>"; exit();
				} else {
					$jum = $this->db->where('userid',$log['userid'])->where('valid',1)->where('key_booking',$key)->get('tbl_file')->num_rows();
					if ($jum < 8) {
						echo "<script>alert('BEBERAPA BERKAS ANDA BELUM/TIDAK TERVALIDASI!');history.go(-1);</script>"; exit();
					}
				}
			}
		} else {
			if ($detailmaba->jenis_pmb == 'MB') {
				if ($s2 != $fileamount) {
					echo "<script>alert('MOHON LENGKAPI BERKAS-BERKAS PRASYARAT ANDA!');history.go(-1);</script>"; exit();
				} else {
					$jum = $this->db->where('userid',$log['userid'])->where('valid',1)->where('key_booking',$key)->get('tbl_file')->num_rows();
					if ($jum < 7) {
						echo "<script>alert('BEBERAPA BERKAS ANDA BELUM/TIDAK TERVALIDASI!');history.go(-1);</script>"; exit();
					}
				}
			} else {
				if ($s2 != $fileamount) {
					echo "<script>alert('MOHON LENGKAPI BERKAS-BERKAS PRASYARAT ANDA!');history.go(-1);</script>"; exit();
				} else {
					$jum = $this->db->where('userid',$log['userid'])->where('valid',1)->where('key_booking',$key)->get('tbl_file')->num_rows();
					if ($jum < 7) {
						echo "<script>alert('BEBERAPA BERKAS ANDA BELUM/TIDAK TERVALIDASI!');history.go(-1);</script>"; exit();
					}
				}
			}
		}
	}

}

/* End of file Pmb.php */
/* Location: .//tmp/fz3temp-1/Pmb.php */