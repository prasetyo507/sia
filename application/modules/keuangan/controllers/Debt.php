<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debt extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			echo "<script>alert('Silahkan log-in kembali untuk memulai sesi!');</script>";
			redirect(base_url('auth/logout'),'refresh');
		} else {
			$this->sess = $this->session->userdata('sess_login');
		}
	}

	public function index()
	{
		$data['user'] = $this->app_model->getdata('tbl_jurusan_prodi','prodi','asc')->result();
		$data['year'] = $this->app_model->getdata('tbl_tahunakademik','kode','asc')->result();
		$data['page'] = 'vselect_year';
		$this->load->view('template', $data);
	}

	function havenotPay()
	{
		$this->load->model('temph_model');

		$prodi = $this->input->post('prodi');
		$angkt = $this->input->post('angkatan');
		$years = $this->input->post('tahunajaran');

		$data['year'] = $years;
		$data['prod'] = $prodi;

		$data['load'] = $this->temph_model->notPayButStudy($prodi,$angkt,$years);
		$this->load->view('excel_belum_du', $data);
	}

}

/* End of file Debt.php */
/* Location: .//tmp/fz3temp-1/Debt.php */