<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Form Data</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url()?>keuangan/indeks/update" method="post">
                <input type="hidden" name="id" value="<?php echo $getData->id_index; ?>">
                    <script type="text/javascript">

                    var format = function(num){
                    var str = num.toString().replace("$", ""), parts = false, output = [], i = 1, formatted = null;
                    if(str.indexOf(".") > 0) {
                        parts = str.split(".");
                        str = parts[0];
                    }
                    str = str.split("").reverse();
                    for(var j = 0, len = str.length; j < len; j++) {
                        if(str[j] != ",") {
                            output.push(str[j]);
                            if(i%3 == 0 && j < (len - 1)) {
                                output.push(",");
                            }
                            i++;
                        }
                    }
                    formatted = output.reverse().join("");
                    return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
                };

                $(function(){
                    $("#currency2").keyup(function(e){
                        $(this).val(format($(this).val()));
                    });
                });

                </script>
                <div class="modal-body" style="margin-left: -30px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Program Studi</label>
                        <div class="controls">
                            <select name="prodi" class="form-control span4" required>
                            <option value="<?php echo $getData->kd_prodi; ?>" selected><?php echo $getData->prodi; ?></option>
                            <?php foreach ($fakultas->result() as $key) {
                                echo "<option disabled><i><b> -- ".$key->fakultas." -- </b></i></option>";
                                $prodi = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$key->kd_fakultas,'kd_fakultas','asc')->result();
                                foreach ($prodi as $value) {
                                    echo "<option value='".$value->kd_prodi."'> ".$value->prodi." </option>";
                                }
                            } ?>
                            </select>
                        </div>
                    </div>
                    <?php switch ($getData->kelas) {
                        case 'PG':
                            $kls = 'PAGI';
                            break;
                        case 'SR':
                            $kls = 'SORE';
                            break;
                        case 'PK':
                            $kls = 'P2K';
                            break;
                        
                        default:
                            $kls = 'SEMUA';
                            break;
                    } ?>
                    <div class="control-group" id="">
                        <label class="control-label">Kelas</label>
                        <div class="controls">
                            <select name="kelas" class="form-control span4" required>
                                <option value="<?php echo $getData->kelas; ?>" selected><?php echo $kls; ?></option>
                                <option value="0">SEMUA</option>
                                <option value="PG">PAGI</option>
                                <option value="SR">SORE</option>
                                <option value="PK">P2K</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Kode Pembayaran</label>
                        <div class="controls">
                            <input type="text" class="span4" name="kode" id='kode' placeholder="Input Kode" class="form-control" value="<?php echo $getData->kode; ?>" style="text-transform:uppercase" required/>
                        </div>
                    </div>              
                    <div class="control-group" id="">
                        <label class="control-label">Pembayaran</label>
                        <div class="controls">
                            <input type="text" class="span4" name="keterangan" placeholder="Keterangan Pembayaran" class="form-control" value="<?php echo $getData->keterangan; ?>" required/>
                        </div>
                    </div>
                     <div class="control-group" id="">
                        <label class="control-label">Jumlah</label>
                        <div class="controls">
                            <input style="text-align: right" type="text" id="currency2" name="jumlah" placeholder="Jumlah Pembayaran" class="form-control span4" value="<?php echo number_format($getData->jumlah); ?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Tahun Ajaran</label>
                        <div class="controls">
                            <select name="ta" class="form-control span4" required>
                                <option value="<?php echo $getData->tahunajaran; ?>" selected><?php echo $getData->ta; ?></option>
                                <?php foreach ($ta->result() as $key2) {
                                    echo "<option value='".$key2->id_tahunajaran."'>".$key2->tahunajaran."</option>";
                                } ?>
                            </select>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>