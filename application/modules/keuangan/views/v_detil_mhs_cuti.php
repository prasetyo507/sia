<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>form/formcuti/looad/'+edc);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Detail Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url(); ?>keuangan/status_ujian/input_data" method="post">
                    <table>
                        <tr>
                            <td>Prodi</td>
                            <td>:</td>
                            <td><?php echo $prdi->prodi; ?></td>
                        </tr>
                        <tr>
                            <td>Tahun Akademik</td>
                            <td>:</td>
                            <td><?php echo substr($ta->tahun_akademik, 0,9); ?></td>
                        </tr>
                    </table>
                    <br>
                    <a class="btn btn-large btn-warning" href="<?php echo base_url(); ?>keuangan/report_jml/load"><< Kembali</a>
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>NPM</th>
                                    <th>Nama</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($what as $row) { ?>
                                <tr>
                                    <td><?php echo $no ?></td>
                                    <td><?php echo $row->NIMHSMSMHS; ?></td>
                                    <td><?php echo $row->NMMHSMSMHS; ?></td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>