<script>
function edit(idk){
$('#edit').load('<?php echo base_url();?>keuangan/indexpem/edit/'+idk);
}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<!--h3>Data Pembayaran <?php //echo $fakultas; ?> <?php //echo $prodi; ?> <?php //$angk = $this->app_model->getdetail('tbl_tahunajaran', 'id_tahunajaran', $angkatan, 'id_tahunajaran', 'ASC')->row(); echo $angk->tahunajaran; ?></h3-->
                <h3>Data Index Pembayaran</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a href="<?php echo base_url(); ?>keuangan/indexpem/back" class="btn btn-warning"> << Kembali </a>
                    <a data-toggle="modal" class="btn btn-success" href="#myModal"><i class="btn-icon-only icon-plus"> </i> Tambah Data</a>
                    <hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Jenis Pembayaran</th>
                                <th>Jumlah</th>
                                <?php if ($this->session->userdata('tipe_index') == 2) { ?>
                                <th>Prodi</th>
                                <?php } ?>
                                <th>Kelas</th>
                                <th width="80">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($rows as $isi) { ?>
	                        <tr>
	                        	<td><?php echo $no ?></td>
                                <td><?php echo $isi->jenis_pembayaran; ?></td>
                                <td style="text-align:right;"><?php echo number_format($isi->jumlah); ?></td>
                                <?php if ($this->session->userdata('tipe_index') == 2) { ?>
                                <td><?php echo $isi->prodi ?></td>
                                <?php } ?>
                                <?php if ($isi->kelas == 'RP'){ ?>
                                    <td>Reguler Pagi</td>
                                <?php }elseif ($isi->kelas == 'RS') { ?>
                                    <td>Reguler Sore</td>
                                <?php } elseif ($isi->kelas == 'NR') { ?>
                                    <td>Non Reguler</td>
                                <?php }else{ ?>
                                    <td></td>
                                <?php } ?>
                                    
                                <td class="td-actions">
                                    <a onclick="edit(<?php echo $isi->id_index; ?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="#"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++;} ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM TAMBAH DATA INDEKS PEMBAYARAN</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>keuangan/indexpem/savedata" method="post">
                <script type="text/javascript">

                    var format = function(num){
                    var str = num.toString().replace("$", ""), parts = false, output = [], i = 1, formatted = null;
                    if(str.indexOf(".") > 0) {
                        parts = str.split(".");
                        str = parts[0];
                    }
                    str = str.split("").reverse();
                    for(var j = 0, len = str.length; j < len; j++) {
                        if(str[j] != ",") {
                            output.push(str[j]);
                            if(i%3 == 0 && j < (len - 1)) {
                                output.push(",");
                            }
                            i++;
                        }
                    }
                    formatted = output.reverse().join("");
                    return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
                };

                $(function(){
                    $("#currency").keyup(function(e){
                        $(this).val(format($(this).val()));
                    });
                });

                $(function(){
                    $("#currency2").keyup(function(e){
                        $(this).val(format($(this).val()));
                    });
                });

                </script>   
                <?php if ($this->session->userdata('tipe_index') == 2) { ?>
                    <input type="hidden" name="jurusan" value="<?php echo $this->session->userdata('sess_jur'); ?>"/>
                    <input type="hidden" name="angkatan" value="<?php echo $this->session->userdata('sess_angk'); ?>"/>
                    <input type="hidden" name="tipe" value="2"/>
                <?php } else { ?>
                    <input type="hidden" name="tipe" value="1"/>
                    <input type="hidden" name="angkatan" value="<?php echo $this->session->userdata('sess_angk'); ?>"/>
                <?php } ?>
                <!-- <input type="hidden" name="jurusan" value="<?php //echo $kd_prodi; ?>"/>
                <input type="hidden" name="angkatan" value="<?php //echo $angkatan; ?>"/>
                <input type="hidden" name="kelas" value="<?php //echo $kelas; ?>"/> -->
                <div class="modal-body" style="margin-left: -30px;">  
					<div class="control-group" id="">
                        <label class="control-label">Jenis Pembayaran</label>
                        <div class="controls">
                            <select name="jenis" class="span4 form-control" required>
                                <option disabled selected>Pilih Jenis Pembayaran</option>
                                <?php foreach ($jenis as $row) { ?>
                                <option value="<?php echo $row->kd_jenis; ?>"><?php echo $row->jenis_pembayaran;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Angkatan</label>
                        <div class="controls">
                            <input type="text" name="angkatan" class="span2 form-control" value="<?php echo $this->session->userdata('sess_angk'); ?>"  disabled/>
                        </div>
                    </div>
                    <?php if ($this->session->userdata('tipe_index') == 2): ?>
                    <div class="control-group" id="">
                        <label class="control-label">Prodi</label>
                        <div class="controls">
                            <input type="text" name="angkatan" class="span2 form-control" value="<?php echo $this->session->userdata('prodi'); ?>"  disabled/>
                        </div>
                    </div>
                    <?php endif ?>
                    <div class="control-group" id="">
                        <label class="control-label">Jumlah</label>
                        <div class="controls">
                            <input type="text" name="jumlah" class="span2 form-control" id="currency" style="text-align:right;" required/>
                        </div>
                    </div>	
                    <div class="control-group">                                         
                        <label class="control-label" for="">Kelas</label>
                        <div class="controls">
                            <select name="kelas" class="span2" required>
                                <option disabled selected> -- Pilih kelas -- </option>
                                <option value="RP"> Reguler Pagi </option>
                                <option value="RS"> Reguler Sore </option>
                                <option value="NR"> Non-Reguler </option>
                            </select>
                        </div> <!-- /controls -->                
                    </div> <!-- /control-group -->			
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->