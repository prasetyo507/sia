<script type="text/javascript">

jQuery(document).ready(function($) {

    $('input[name^=npm]').autocomplete({

        source: '<?php echo base_url('keuangan/dispensasi/load_npm_autocomplete');?>',

        minLength: 4,

        select: function (evt, ui) {

            this.form.npm0.value = ui.item.value;

        }

    });

});

</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Daftar Mahasiswa Dispensasi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content"> 
				<div class="span11">
                        <form class="form-inline" action="<?php echo base_url(); ?>keuangan/dispensasi/add_dispensasi" method="post">
							<div class="form-group">
								<label class="sr-only">Mahasiswa : </label>
								<input id="npm0" name="npm" type="text" class="form-control mb-2 mr-sm-2" placeholder="Masukan NPM atau Nama" required> 
								<button type="submit" class="btn btn-primary" title="Tambah mahasiswa dispensasi" ><i class="icon-plus"></i></button>
							</div>
						</form> 
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Tahun Akademik</th>
                                    <th>Prodi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($users as $row) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->npm_mahasiswa; ?></td>
                                    <td><?php echo getNameMhs($row->npm_mahasiswa) ?></td>
                                    <td><?php echo $row->tahunajaran; ?></td>
                                    <td><?php echo get_jur($row->kd_jurusan) ?></td>
                                    <td>
										<a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-success btn-small" href="<?php echo base_url()?>keuangan/dispensasi/pelunasan/<?php echo $row->npm_mahasiswa; ?>/<?php echo $row->tahunajaran; ?>" title="Pelunasan Dispensasi"><i class="btn-icon-only icon-check"> </i></a>
									</td>
                                   
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                </div>                
            </div>
        </div>
    </div>
</div>

</div>

