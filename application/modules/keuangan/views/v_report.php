<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Data Status Mahasiswa</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>keuangan/report_jml/simpan_sesi">
                        <fieldset>
                      <script>
                              $(document).ready(function(){
                                $('#faks').change(function(){
                                  $.post('<?php echo base_url()?>keuangan/report_jml/get_jurusan/'+$(this).val(),{},function(get){
                                    $('#jurs').html(get);
                                  });
                                });
                              });
                              </script>

                              <div class="control-group">
                                <label class="control-label">Tahun Akademik</label>
                                <div class="controls">
                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">
                                    <option>--Pilih Tahun Akademik--</option>
                                    <?php foreach ($tahunajar as $row) { ?>
                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>  
                            <br/>
                              
                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" value="Cari"/>
                                <!-- <a class="btn btn-large btn-success" href="<?php echo base_url(); ?>keuangan/report_jml/load">Cari</a> -->
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>
          
        </div>
      </div>
    </div>
  </div>
</div>
