<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_mhs.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table id="example1" class="table table-bordered table-striped" border="1">
    <thead>
        <tr> 
            <th>No</th>
            <th>NPM</th>
            <th>Nama</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($hasil as $row) { ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $row->NIMHSMSMHS; ?></td>
            <td><?php echo $row->NMMHSMSMHS; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>