<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>form/formcuti/looad/'+edc);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Jumlah Status Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url(); ?>keuangan/status_ujian/input_data" method="post">
                        <!-- <a class="btn btn-success btn-large" href=""><i class="btn-icon-only icon-print"> Print</i></a> -->
                        <a class="btn btn-large btn-warning" href="<?php echo base_url(); ?>keuangan/report_jml/"><< Kembali</a>
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>Prodi</th>
                                    <th>Fakultas</th>
                                    <th>Aktif</th>
                                    <th>Cuti</th>
                                    <th>Non Aktif</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($query as $row) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->prodi; ?></td>
                                    <td><?php echo $row->fakultas; ?></td>
                                    <td><a href="<?php echo base_url(); ?>keuangan/report_jml/detil_jml_aktv/<?php echo $row->kd_prodi; ?>"><?php echo $row->jml; ?></a></td>

                                    <?php $qq = $this->db->query('SELECT COUNT(DISTINCT npm) AS jum FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa b ON a.`npm`=b.`NIMHSMSMHS`
                                                                WHERE a.`status`="C" AND a.`tahunajaran` = "'.$row->tahunajaran.'" and a.validate >= 1 AND b.`KDPSTMSMHS` = "'.$row->kd_prodi.'"
                                                                ORDER BY b.`KDPSTMSMHS` ASC')->row()->jum; ?>

                                    <?php
                                    $aa = substr($row->tahunajaran, 0,4);
                                    $bb = $aa+1;
                                    $start = studyStart($row->tahunajaran);

                                    $zz = $this->db->query("SELECT DISTINCT KDPSTMSMHS, COUNT(NIMHSMSMHS) as jum 
                                                            FROM tbl_mahasiswa WHERE NIMHSMSMHS NOT IN (
                                                                SELECT npm_mahasiswa FROM tbl_verifikasi_krs 
                                                                WHERE tahunajaran = ".$row->tahunajaran."
                                                            ) AND NIMHSMSMHS NOT IN (
                                                                SELECT npm FROM tbl_status_mahasiswa 
                                                                WHERE tahunajaran = ".$row->tahunajaran." AND validate = 1 
                                                                AND tahunajaran = ".$row->tahunajaran."
                                                            ) AND KDPSTMSMHS = ".$row->kd_prodi." 
                                                            AND BTSTUMSMHS >= ".$row->tahunajaran." AND STMHSMSMHS != 'L'
                                                            AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K' 
                                                            AND TAHUNMSMHS < ".$bb."
                                                            AND SMAWLMSMHS >= ".$start."
                                                            AND SMAWLMSMHS <= ".$row->tahunajaran."")->row()->jum;
                                     ?>

                                    <td><a href="<?php echo base_url(); ?>keuangan/report_jml/detil_jml_cuti/<?php echo $row->kd_prodi; ?>"><?php echo $qq; ?></a></td>
                                    <td><a href="<?php echo base_url(); ?>keuangan/report_jml/detil_jml_non/<?php echo $row->kd_prodi; ?>"><?php echo $zz; ?></a></td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>