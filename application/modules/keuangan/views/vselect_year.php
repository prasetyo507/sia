<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-money"></i>
        <h3>Piutang Mahasiswa</h3>
      </div> 

      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?= base_url(); ?>keuangan/debt/havenotPay">
            <fieldset>
              
              <div class="control-group">
                <label class="control-label">Prodi</label>
                <div class="controls">
                  <select class="form-control span6" name="prodi" id="" required>
                    <option disabled selected>--Pilih Prodi--</option>
                    <?php foreach ($user as $row) { ?>
                    <option value="<?php echo $row->kd_prodi;?>"><?php echo $row->prodi;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Tahun Akademik</label>
                <div class="controls">
                  <select class="form-control span6" name="tahunajaran" id="tahunajaran" required>
                    <option disabled selected>--Pilih Tahun Akademik--</option>
                    <?php foreach ($year as $row) { ?>
                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Angkatan</label>
                <div class="controls">
                  <select class="form-control span6" name="angkatan" id="" required>
                    <option disabled selected>--Pilih Angkatan--</option>
                    <?php for ($i = 2010; $i <= date('Y'); $i++) { ?>
                    <option value="<?= $i;?>"><?= $i;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-actions">
                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
              </div> <!-- /form-actions -->

            </fieldset>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>