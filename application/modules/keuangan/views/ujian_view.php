<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Data Keuangan Mahasiswa</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>keuangan/status_ujian/simpan_sesi">
                        <fieldset>
                              <script>
                                $(document).ready(function(){
                                  $('#faks').change(function(){
                                    $.post('<?php echo base_url()?>keuangan/status_ujian/get_jurusan/'+$(this).val(),{},function(get){
                                      $('#jurs').html(get);
                                    });
                                  });
                                });
                              </script>
                              <div class="control-group">
                                <label class="control-label">Fakultas</label>
                                <div class="controls">
                                  <select class="form-control span6" name="fakultas" id="faks">
                                    <option>--Pilih Fakultas--</option>
                                    <?php foreach ($fakultas as $row) { ?>
                                    <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                              

                              <div class="control-group">
                                <label class="control-label">Prodi</label>
                                <div class="controls">
                                  <select class="form-control span6" name="jurusan" id="jurs">
                                    <option>--Pilih Jurusan--</option>
                                  </select>
                                </div>
                              </div>

                              <div class="control-group">
                                <label class="control-label">Angkatan</label>
                                <div class="controls">
                                  <select class="form-control span6" name="angkatan">
                                    <option>--Pilih Angkatan--</option>
                                    <?php $tahun= date('Y'); $awal = $tahun-8; for ($i=$awal; $i <= $tahun; $i++) { 
                                      echo "<option value='".$i."'>".$i."</option>";
                                    }; ?>
                                  </select>
                                </div>
                              </div>

                              <div class="control-group">
                                <label class="control-label">Tahun Akademik</label>
                                <div class="controls">
                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">
                                    <option>--Pilih Tahun Akademik--</option>
                                    <?php foreach ($tahunajar as $row) { ?>
                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>  
                            <br/>
                              
                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>
          
        </div>
      </div>
    </div>
  </div>
</div>
