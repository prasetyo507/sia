              <div class="row">

                <div class="span12">                

                    <div class="widget ">

                      <div class="widget-header">

                        <i class="icon-list"></i>

                        <h3>Data Pengajuan Cuti</h3>

                    </div> <!-- /widget-header -->

                    

                    <div class="widget-content">

                      <div class="span11">

                        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>rekapitulasi/rekap_status/simpan_sesi">
                              <script>

                              $(document).ready(function(){

                                $('#faks').change(function(){

                                  $.post('<?php echo base_url()?>rekapitulasi/rekap_status/get_jurusan/'+$(this).val(),{},function(get){

                                    $('#jurs').html(get);

                                  });

                                });

                              });

                              </script>

                              <div class="control-group">

                                <label class="control-label">Fakultas</label>

                                <div class="controls">

                                  <select class="form-control span4" name="fakultas" id="faks">

                                    <option value="all">SEMUA FAKULTAS</option>

                                    <?php foreach ($fakultas as $row) { ?>

                                    <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div>

                              <div class="control-group">

                                <label class="control-label">Prodi</label>

                                <div class="controls">

                                  <select class="form-control span4" name="jur" id="jurs">

                                    <option>--Pilih Prodi--</option>
                                  
                                    <option value=""></option>
                                  
                                  </select>

                                </div>

                              </div>
                              
                              <div class="control-group">

                                <label class="control-label">Tahun Akademik</label>

                                <div class="controls">

                                  <select class="form-control span4" name="thnajar" id="tahunajaran">

                                    <option>--Pilih Tahun Akademik--</option>

                                    <?php foreach ($tahunajar as $row) { ?>

                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div> 

                              <div class="control-group">

                                <label class="control-label">Import</label>

                                <div class="controls">

                                  <select class="form-control span4" name="import" id="ppp">

                                    <option>--Pilih Jenis Import--</option>

                                    <option value="exc">Excel</option>
                                    <option value="csv">CSV</option>

                                  </select>

                                </div>

                              </div>   

                            <br/>

                            <div class="form-actions">
                            <button type="submit" class="btn btn-large btn-primary"><i class="icon icon-print"></i> Print</button>
                                
                               
                            </div> <!-- /form-actions -->

                    </form>
                </div>

      </div>

    </div>

  </div>

</div>