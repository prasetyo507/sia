<style>
    #dup label{
        margin-bottom: 0px;
    }
</style>

<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css"/>

<script type="text/javascript">
    $(document).ready(function(){
        $('#jam_masuk').timepicker();
        $('#jam_keluar').timepicker();
        $('#jam_masukedit').timepicker();
    });
</script>

<script>
    function edit(idk){
        $('#form_edit').load('<?php echo base_url();?>perkuliahan/jdl_kuliah_baa/edit_jadwal2/'+idk);
    }

    function dosen(idk,gabungan){
        console.log(gabungan)
        $('#edit1').load('<?php echo base_url();?>perkuliahan/jdl_kuliah_baa/load_dosen/'+idk+'/'+gabungan);
    }

    function seeAmount(id)
    {
        $.get('<?= base_url("perkuliahan/jdl_kuliah_baa/jumlahpeserta/") ?>'+id, function(response){
            $('#modaljumlah').modal('show');
            var data = JSON.parse(response);
            $('#titlejumlah').text(data.title);
            $('#dosen').val(data.nmdosen);
            $('#amount').val(data.jumlah);
        });
    }
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Jadwal MKDU</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <form action="<?php echo base_url('perkuliahan/jdl_kuliah_baa/view'); ?>" method="post">
                        <a data-toggle="modal" class="btn btn-primary" href="#new">
                            <i class="icon-plus"></i> New Data </a>
                        <a href="#" class="btn btn-success"><i class="icon-print"></i> Print Data </a>
                        <hr>
                        <table class="table table-bordered table-striped" id="example1">
                            <thead>
                                <tr> 
                                    <th>Kode MK</th>
                                    <th>Mata Kuliah</th>
                                    <th>Kelas</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Prodi</th>
                                    <th>Fakultas</th>
                                    <th>Hari/Waktu</th>
                                    <th>Dosen</th>
                                    <th>Ruangan</th>
                                    <th>Peserta</th>
                                    <th width="120">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($data as $row) { ?>
                                <tr>
                                    <td><?php echo $row->kd_matakuliah; ?></td>
                                    <td><?= get_nama_mk($row->kd_matakuliah,substr($row->kd_jadwal, 0,5)); ?></td>
                                    <td><?= $row->kelas ?></td>
                                    <td><?= $row->kd_tahunajaran; ?></td>
                                    <td><?= get_jur(substr($row->kd_jadwal, 0,5)); ?></td>
                                    <td><?= get_fak_byprodi(substr($row->kd_jadwal, 0,5)); ?></td>
                                    <td><?= notohari($row->hari).' / '.substr($row->waktu_mulai,0,5).'-'.substr($row->waktu_selesai,0,5); ?></td>
                                    <td><?= nama_dsn($row->kd_dosen) ?></td>
                                    <td><?= get_room($row->kd_ruangan) ?></td>
                                    <td>
                                       <button class="btn btn-primary btn-small" data-toggle="modal" data-target="#modaljumlah" onclick="seeAmount('<?= $row->id_jadwal ?>')"><i class="btn-icon-only icon-eye-open"></i></button> 
                                    </td>
                                    <td>
                                        <a data-toggle="modal" onclick="dosen(<?php echo $row->id_jadwal;?>, <?php echo $row->gabung ?>)" href="#editModal1"class="btn btn-success btn-small" ><i class="btn-icon-only icon-user"> </i></a>

                                        <a class="btn btn-primary btn-small" onclick="edit(<?php echo $row->id_jadwal;?>)" data-toggle="modal" href="#editModal" ><i class="btn-icon-only icon-pencil"></i></a>

                                        <a class="btn btn-warning btn-small" href="<?php echo base_url(); ?>akademik/ajar/cetak_absensi/<?php echo $row->id_jadwal; ?>" ><i class="btn-icon-only icon-print"></i></a>
                                        
										<a class="btn btn-danger btn-small" href="<?php echo base_url(); ?>perkuliahan/jdl_kuliah_baa/delete/<?php echo $row->id_jadwal; ?>" ><i class="btn-icon-only icon-remove"></i></a>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                            
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js') ?>" type="text/javascript"></script>
<!-- data table serverside processing -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#examplee').dataTable( {
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?= base_url(); ?>perkuliahan/jdl_kuliah_baa/serverside_loadmku",
                type: "POST"
            }
        });
    });
</script>

<div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM DATA</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>perkuliahan/jdl_kuliah_baa/save" method="post">
                <div class="modal-body">
                    <div class="control-group" id="">
                        <label class="control-label">Prodi</label>
                        <div class="controls">
                            <select name="prodi[]" class="span3" class="form-control" multiple>
                                <?php foreach ($rows as $isi) { ?>
                                    <option value="<?php echo $isi->kd_prodi; ?>"><?php echo $isi->kd_prodi; ?> - <?php echo $isi->prodi; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Kelompok Kelas</label>
                        <div class="controls">
                            <input type="radio" name="st_kelas" value="PG" required=""> PAGI &nbsp;&nbsp; 
                            <input type="radio" name="st_kelas" value="SR" required=""> SORE &nbsp;&nbsp;
                            <input type="radio" name="st_kelas" value="PK" required=""> P2K  &nbsp;&nbsp;
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Mata Kuliah</label>
                        <div class="controls">
                            <select name="mk" class="span3" class="form-control" id="mk">
                                <?php foreach ($matkul as $isi) { ?>
                                    <option value="<?php echo $isi->kd_matakuliah; ?>"><?php echo $isi->kd_matakuliah; ?> - <?php echo $isi->nama_matakuliah; ?> (<?php echo $isi->sks_matakuliah; ?> sks)</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Hari</label>
                        <div class="controls">
                            <select id="" name="hari" class="span2" class="form-control">
                                <option disabled>--Hari Kuliah--</option>
                                <option value="1">Senin</option>
                                <option value="2">Selasa</option>
                                <option value="3">Rabu</option>
                                <option value="4">Kamis</option>
                                <option value="5">Jum'at</option>
                                <option value="6">Sabtu</option>
                                <option value="7">Minggu</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Waktu</label>
                        <div class="controls">
                            <input type="text" class="form-control" placeholder="Input Jam Masuk" id="jam_masuk" name="jam_masuk">
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function(){
                            $('#gabung').change(function(){
                                $.post('<?php echo base_url();?>perkuliahan/Jdl_kuliah_baa/get_kelas_gabungan/'+$(this).val()+'/'+$('#mk').val(),{},function(get){
                                    $('#kelas_gab').html(get);
                                });
                            });
                        });
                    </script>

                    <div class="control-group" id="">
                        <label class="control-label">Kelas Gabungan</label>
                        <div class="controls">
                            <select id="gabung" name="gabung" class="span2" class="form-control">
                                <option>--Pilihan--</option>
                                <option value="1">Gabungan</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group" id="">
                        <label class="control-label">Kelas</label>
                        <div class="controls">
                            <select name="kelas_gab" id="kelas_gab" class="span2" class="form-control" required>
                                
                            </select>
                        </div>
                    </div>

                    <script type="text/javascript">

                        $(document).ready(function(){

                            $('#gedung').change(function(){

                                $.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_lantai/'+$(this).val(),{},function(get){

                                    $('#lantai').html(get);

                                });

                            });

                        });

                    </script>

                    <div class="control-group">                                         

                        <label class="control-label" for="gedung">Gedung</label>

                        <div class="controls">

                            <select class="form-control" name="gedung" id="gedung">

                                <option value="">--Pilih Gedung--</option>

                                <?php foreach ($gedung as $isi) {?>

                                    <option value="<?php echo $isi->id_gedung ?>"><?php echo $isi->gedung ?></option>   

                                <?php } ?>

                            </select>

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <script type="text/javascript">

                        $(document).ready(function(){

                            $('#lantai').change(function(){

                                $.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_ruangan/'+$(this).val(),{},function(get){

                                    $('#ruangan').html(get);

                                });

                            });

                        });

                    </script>

                    <div class="control-group">                                         

                        <label class="control-label" for="lantai">Lantai</label>

                        <div class="controls">

                            <select name="lantai" id="lantai">

                                <option value="">--Pilih Lantai--</option>

                            </select>

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    

                    <div class="control-group">                                         

                        <label class="control-label" for="ruangan">Ruangan</label>

                        <div class="controls">

                            <select name="ruangan" id="ruangan" required>

                                <option value="">--Pilih Ruangan--</option>

                            </select>

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" name="save" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="edit1">

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="form_edit">

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<!-- modal untuk mnampilkan jumlah peserta -->
<div class="modal fade" id="modaljumlah" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">
                <h3 id="titlejumlah"></h3>
            </div>
            <div class="modal-body">
                <div class="control-group">                                         
                    <label class="control-label" for="dosen">Nama Dosen</label>
                    <div class="controls">
                        <input type="text" class="form-control span5" id="dosen" readonly="">
                    </div> <!-- /controls -->               
                </div>
                <div class="control-group">                                         
                    <label class="control-label" for="amount">jumlah Peserta</label>
                    <div class="controls">
                        <input type="text" class="form-control span5" id="amount" readonly="">
                    </div> <!-- /controls -->               
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div>