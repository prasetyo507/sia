 <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">DATA PESERTA MAHASISWA PENGAJUAN SEMESTER PENDEK <?php echo $mk->nama_matakuliah.' ('.$mk->kd_matakuliah.')'; ?></h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/sp/submit_data" method="post">
  <div class="modal-body">
		<?php //var_dump($peserta_sp); ?>
    <!--div class="span11"-->
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                              <th width='50'>No</th>
                              <th width='80'>NPM</th>
                              <th>NAMA</th>
                              <th width='70'>ANGKATAN</th>
                            </tr>
                        </thead>
                        <tbody>   
                        <?php $no=1; foreach ($peserta_sp as $value) { ?>
                          <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $value->npm_mahasiswa; ?></td>
                            <td><?php echo $value->NMMHSMSMHS; ?></td>
                            <td><?php echo $value->TAHUNMSMHS; ?></td>
                            <input type="hidden" name="npm[]" value="<?php echo $value->kd_krs.$mk->kd_matakuliah; ?>"/>
                          </tr>
                        <?php $no++; } ?>
                        </tbody>
                    </table>
                <!--/div-->
  </div>
  <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <input type="submit" class="btn btn-primary" value="Submit"/>
  </div>
</form>

<script type="text/javascript">

    $(function() {

        $('#example2').dataTable({

            "bPaginate": false,

            "bLengthChange": true,

            "bFilter": true,

            "bSort": true,

            "bInfo": true,

            "bAutoWidth": true

        });

    });

</script>