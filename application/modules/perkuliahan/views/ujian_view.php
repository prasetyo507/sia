
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<h3>Data Jadwal Ujian</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <script>
                      $(document).ready(function(){
                        $('#faks').change(function(){
                          $.post('<?php echo base_url()?>perkuliahan/jadwalujian/get_jurusan/'+$(this).val(),{},function(get){
                            $('#jurs').html(get);
                          });
                        });
                      });
                      </script>
					<form class="form-horizontal" action="<?php echo base_url(); ?>perkuliahan/jadwalujian/view_jadwal" method="post">
                        <fieldset>
                            <div class="control-group">                                         
                                <label class="control-label" for="">Tahun Ajaran</label>
                                <div class="controls">
                                    <select name="tahunajaran" class="span6" required>
                                        <option> -- Pilih Tahun Ajaran -- </option>
                                        <?php foreach ($tahunajar as $row) { ?>
                                        <option value="<?php echo $row->id_tahunajaran;?>"><?php echo $row->tahunajaran;?></option>
                                        <?php } ?>
                                    </select>
                                </div> <!-- /controls -->               
                            </div> <!-- /control-group -->

                            <div class="control-group">                                         
                                <label class="control-label" for="">Fakultas</label>
                                <div class="controls">
                                    <select name="fakultas" class="span6" id="faks" required>
                                        <option disabled selected> -- Pilih Fakultas -- </option>
                                        <?php foreach ($fakultas as $row) { ?>
                                        <option value="<?php echo $row->kd_fakultas.'-'.$row->fakultas;?>"><?php echo $row->fakultas;?></option>
                                        <?php } ?>
                                    </select>
                                </div> <!-- /controls -->               
                            </div> <!-- /control-group -->

                            <div class="control-group">                                         
                                <label class="control-label" for="">Jurusan</label>
                                <div class="controls">
                                    <select name="jurusan" class="span6" id="jurs" required>
                                        <option disabled selected> -- Pilih Jurusan -- </option>
                                    </select>
                                </div> <!-- /controls -->               
                            </div> <!-- /control-group -->
                                <!--<div class="control-group">                                         
                                <label class="control-label" for="">Kelas</label>
                                <div class="controls">
                                    <select name="shift" class="span6" required>
                                        <option disabled selected> -- Pilih kelas -- </option>
                                        <option value="R"> Reguler - Pagi </option>
                                        <option value="RS"> Reguler - Sore </option>
                                        <option value="P2K"> P2K </option>
                                    </select>
                                </div>               
                            </div>  /control-group -->
                            
                            <br />
                              
                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>