<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>perkuliahan/sp/matkulsp/'+edc);
    }

    function myFunction(){
        window.location.reload();
    }

</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Semester Pendek</h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="span11">
                    <!--a class="btn btn-cancel btn" href="<?php //echo base_url(); ?>akademik/otorisasi"> Kembali</i></a>
                    <button type="submit" onClick="myFunction()" class="btn btn-primary btn" >Submit</button> >
                    <hr-->
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th width='50'>No</th>
                                <th width='150'>Kode MK</th>
                                <th>Mata Kuliah</th>
                                <th width='70'>SKS</th>
                                <th width='70'>Pengajuan</th>
                                <!--th width='70'>Peserta</th-->
                                <th>Status</th>
                                <th width='80'>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>   
                        <?php $no = 1; foreach ($matkul_sp as $value) { ?>
                        <?php $querydiview = $this->db->query('select count(npm_mahasiswa) as jumlah from tbl_krs where kd_matakuliah = "'.$value->kd_matakuliah.'" and SUBSTR(kd_krs,19,1) = 2 ')->row(); ?> 
                        <?php //$querydiview = $this->db->query('select count(npm_mahasiswa) as jumlah from tbl_krs where kd_matakuliah = "'.$value->kd_matakuliah.'" and SUBSTR(kd_krs,19,1) = 2 ')->row(); ?> 
                        <?php $querydiview2 = $this->db->query('select count(a.kd_jadwal) as jumlah from tbl_krs a join tbl_verifikasi_krs b on a.kd_krs = b.kd_krs where a.kd_matakuliah = "'.$value->kd_matakuliah.'" and SUBSTR(a.kd_krs,19,1) = 2 and a.kd_jadwal = 3 ')->row(); 
                            if ($querydiview2->jumlah == $querydiview->jumlah) {
                                $status = 'aktif';
                            } elseif ($querydiview2->jumlah == 0) {
                                $status = 'non-aktif';
                            } elseif (($querydiview->jumlah) > ($querydiview2->jumlah)) {
                                $status = 'aktif (submit)';
                            }
                            
                        ?> 
                            <tr> 
                                <td><?php echo $no;; ?></td>
                                <td><?php echo $value->kd_matakuliah; ?></td>
                                <td><?php echo $value->nama_matakuliah; ?></td>
                                <td><?php echo $value->sks_matakuliah; ?></td>
                                <td><?php echo $querydiview->jumlah; ?></td>
                                <td><?php echo $status; ?></td>
                                <td>
                                    <a data-toggle="modal" onclick="edit('<?php echo $value->kd_matakuliah;?>')" href="#editModal1" class="btn btn-primary btn-small"><i class="btn-icon-only icon-ok"> </i></a>
                                    <a href="#" class="btn btn-success btn-small"><i class="btn-icon-only icon-print"> </i></a>
                                </td>
                            </tr>
                        <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>