<style type="text/css">
	#circle { 
		width: 20px; 
		height: 20px; 
		background: #FFB200; 
		-moz-border-radius: 50px; 
		-webkit-border-radius: 50px; 
		border-radius: 50px; 
	}
</style>

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.css">
<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.js"></script>
<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">

<script>
	function edit(idk){
		$('#edit1').load('<?php echo base_url();?>perkuliahan/jdl_kuliah2/edit_jadwal_new/'+idk);
	}

	function edit2(idk){
		$('#edit2').load('<?php echo base_url();?>perkuliahan/jdl_kuliah/penugasan_new/'+idk);
	}

	function edit3(idk){
		$('#edit3').load('<?php echo base_url();?>perkuliahan/jdl_kuliah/edit_nama_kelas/'+idk);
	}
</script>


<script type="text/javascript">

	$(document).ready(function(){

		$('#jam_masuk').timepicker();

		$('#jam_keluar').timepicker();

		$('#jam_masukedit').timepicker();
	});

</script>



<script type="text/javascript">

jQuery(document).ready(function($) {

	$('input[name^=dosen]').autocomplete({

        source: '<?php echo base_url('perkuliahan/jdl_kuliah/getdosen');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.nik.value = ui.item.nik;

            this.form.dosen.value = ui.item.value;

        }

    });

});

</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>
  					Data Jadwal Perkuliahan <?php echo $this->session->userdata('tahunajaran').' - Semester'.$this->session->userdata('semester');?>
  				</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content" style="padding:30px;">
            	<div class="tabbable">
		            <ul class="nav nav-tabs">
		            	<?php
		            		echo '<li class="active">';
		            		echo '<a href="#tab'.$this->session->userdata('semester').'"';
		            		echo ' data-toggle="tab">Semester '.$this->session->userdata('semester').'</a>';
		            		echo '</li>';
		            	?>
		            </ul>

            		<br>
     
	              	<div class="tab-content">
	              	<?php for ($i = 1; $i < 2; $i++) {
	            		
	            		if ($i==1) { 
	            			echo '<div class="tab-pane active" id="tab'.$i.'">'; ?>
            				<div class="span11">

								<?php

								$user 	= $this->session->userdata('sess_login');
						        $pecah 	= explode(',', $user['id_user_group']);
						        $jmlh 	= count($pecah);
						        for ($i = 0; $i < $jmlh; $i++) { 
						            $grup[] = $pecah[$i];
						        }

								if ($user['id_user_group'] == 1 || $user['id_user_group'] == 10) { ?>

						            <a href="<?php echo base_url(); ?>perkuliahan/jdl_kuliah" class="btn btn-warning"> << Kembali</a>

							    <?php } ?>

							    <?php if ((count($aktif) == 1) and ($crud->create > 0))  { if ($user['id_user_group'] != 5) { ?> 
				                    
				                    <a data-toggle="modal" class="btn btn-success" href="#myModal">
				                    	<i class="btn-icon-only icon-plus"> </i> Tambah Data
				                    </a>

			                    <?php } } ?>

			                    <a href="<?php echo base_url(); ?>perkuliahan/jdl_kuliah/cetak_jadwal_all" class="btn btn-info">Print Jadwal</a>
			                    
			                    <br><hr>

								<table id='example1' class="table table-bordered table-striped">

				                	<thead>

				                        <tr> 

				                        	<th>No</th>
<!-- 
											<th  width="30">Semester</th> -->

											<th>Kelas</th>

											<th>Hari</th>

				                            <th>Kode MK</th>

				                            <th>Matakuliah</th>

											<th>SKS</th>

											<th>Waktu</th>

											<th>Ruang</th>

											<th>Kapasitas</th>

											<th>Peserta</th>

											<th>Dosen</th>

											<?php if ($user['id_user_group'] != 5) { ?>

				                            <th width="80">Aksi</th>

				                            <?php } ?>

				                        </tr>

				                    </thead>

				                    <tbody>
				                    	<?php $no=1; foreach ($jadwal as $isi)  { 
			                    			/*
			                    			$cek_kelas 	= $this->db->query('SELECT COUNT(z.`npm_mahasiswa`) as jml FROM tbl_krs z 
			                    											WHERE z.`kd_jadwal` = "'.$isi->kd_jadwal.'"')->row();
			                    			if ($isi->gabung > 0) {
			                    				$jml_peserta 	= $this->db->query('select count(distinct npm_mahasiswa) as jml from tbl_krs 
			                    													where kd_jadwal = "'.$isi->kd_jadwal.'" 
			                    													or kd_jadwal = "'.$isi->referensi.'" 
			                    													or kd_jadwal IN 
													(SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$isi->referensi.'")')->row()->jml;
			                    			} else {
			                    				$jml_peserta = $this->db->query('select count(distinct npm_mahasiswa) as jml from tbl_krs where kd_jadwal = "'.$isi->kd_jadwal.'" or kd_jadwal IN 
													(SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$isi->kd_jadwal.'")')->row()->jml;
			                    			}
			                    			*/

				                    		$jml_peserta 	= $this->db->query('SELECT count(distinct npm_mahasiswa) as jml from tbl_krs 
				                    											where kd_jadwal = "'.$isi->kd_jadwal.'"
				                    											or kd_jadwal in 
				                    												(SELECT kd_jadwal from tbl_jadwal_matkul where referensi = "'.$isi->kd_jadwal.'") 
				                    											')->row()->jml;

			                    			if (substr($isi->kuota, 0,2) == NULL ) {
			                    				$warna ='';

			                    			}elseif (substr($isi->kuota, 0,2) <= $jml_peserta) {
			                    				$warna = 'style="background:#FFB200;"';
			                    			
			                    			} else {
			                    				$warna ='';
			                    			}
				                    	
				                    	?>
				                    		
				                    	<tr >

			                           		<td <?php echo $warna; ?> ><?php echo $no; ?></td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->kelas; ?></td>

				                        	<td <?php echo $warna; ?>><?php echo notohari($isi->hari); ?></td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->kd_matakuliah; ?></td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->nama_matakuliah; ?></td>

				                        	<td <?php echo $warna; ?>><?php echo $isi->sks_matakuliah; ?></td>

				                        	<td <?php echo $warna; ?>>
				                        		<?= substr($isi->waktu_mulai,0,5).'-'.substr($isi->waktu_selesai,0,5); ?>
				                        	</td>

				                        	<?php if ($isi->kode_ruangan == NULL || $isi->kode_ruangan == '') { ?>
				                        		<td <?php echo $warna; ?>>-</td> 
				                        		<td <?php echo $warna; ?>>-</td> 

				                        	<?php }else{ ?>
				                        		<td <?php echo $warna; ?>><?php echo $isi->kode_ruangan; ?></td> 
				                        		<td <?php echo $warna; ?>><?php echo substr($isi->kuota, 0,2)?></td>

				                        	<?php }?>	                        
				                
				                        	<td <?php echo $warna; ?>><?php echo $jml_peserta; ?></td>
				                        	<td <?php echo $warna; ?>><?php echo $isi->nama; ?></td>

				                        	<?php if ($user['id_user_group'] != 5) { ?>
					                        	<?php if (($isi->audit_user != 'baa')) { ?>
					                        	<td <?php echo $warna; ?>>

					                        		<?php if ((count($aktif) == 1) and ($crud->edit > 0)) { ?>
					                        		<a 
					                        			class="btn btn-success btn-small" 
					                        			onclick="edit2(<?php echo $isi->id_jadwal;?>)" 
					                        			data-toggle="modal" 
					                        			href="#editModal2">
					                        			<i class="btn-icon-only icon-user"></i>
					                        		</a>

				                                    <a 
				                                    	class="btn btn-primary btn-small" 
				                                    	onclick="edit(<?php echo $isi->id_jadwal;?>)" 
				                                    	data-toggle="modal" href="#editModal1" >
				                                    	<i class="btn-icon-only icon-pencil"></i>
				                                    </a>
				                                    <?php } ?>

				                                    <?php if ($user['id_user_group'] == 19) { ?>
				                                    	<a onclick="return confirm('Apakah Anda Yakin?');" 
					                                    	class="btn btn-danger btn-small" 
					                                    	href="<?= base_url('perkuliahan/jdl_kuliah/delete/'.$isi->id_jadwal); ?>">
					                                    	<i class="btn-icon-only icon-remove"></i>
					                                    </a>
				                                    <?php } ?>
				                                    

				                                    <a 
				                                    	href="<?php echo base_url('akademik/absenDosen/event/'.$isi->id_jadwal); ?>" 
				                                    	target="_blank" 
				                                    	class="btn btn-info btn-small" 
				                                    	title="Berita Acara">
				                                    	<i class="icon icon-file"></i>
				                                    </a>
				                                    
				                                    <a 
				                                    	class="btn btn-warning btn-small" 
				                                    	href="<?php echo base_url(); ?>akademik/ajar/cetak_absensi/<?php echo $isi->id_jadwal; ?>" >
				                                    	<i class="btn-icon-only icon-print"></i>
				                                    </a>

												</td>

												<?php } else { ?>

												<td <?php echo $warna; ?>>

													<!-- <a class="btn btn-success btn-small" onclick="edit2(<?php //echo $isi->id_jadwal;?>)" data-toggle="modal" href="#editModal2"><i class="btn-icon-only icon-user"></i></a>
				                                    <a class="btn btn-primary btn-small" onclick="edit(<?php //echo $isi->id_jadwal;?>)" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-pencil"></i></a> -->
				                                    <?php if ($crud->edit > 0) { ?>
				                                    	<a 
				                                    		class="btn btn-primary btn-small" 
				                                    		onclick="edit3(<?php echo $isi->id_jadwal;?>)" 
				                                    		data-toggle="modal" href="#editModal3" >
				                                    		<i class="btn-icon-only icon-pencil"></i>
				                                    	</a>
				                                    <?php } ?>
				                                    
				                                    <a 
				                                    	href="<?php echo base_url('akademik/absenDosen/event/'.$isi->id_jadwal); ?>" 
				                                    	target="_blank" 
				                                    	class="btn btn-info btn-small" 
				                                    	title="Berita Acara">
				                                    	<i class="icon icon-file"></i>
				                                    </a>

				                                    <a 
				                                    	class="btn btn-warning btn-small" 
				                                    	href="<?php echo base_url(); ?>akademik/ajar/cetak_absensi/<?php echo $isi->id_jadwal; ?>" >
				                                    	<i class="btn-icon-only icon-print"></i>
				                                    </a>
												</td>
												<?php } ?>
											<?php } ?>
										</tr>
										<?php $no++; } ?>

										<?php } ?>
										<?php } ?>
				                    </tbody>

				               	</table>

							</div>
                <?php echo '</div>'; ?>      
			</div>  
		</div>
	</div> <!-- /widget-content -->



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Buat Jadwal Baru</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/jdl_kuliah/save_data" method="post">

                <div class="modal-body">

					<div class="control-group">

                      <label class="control-label">Jurusan</label>

                      <div class="controls">

                        <input type='hidden' class="form-control" name="jurusan" value="<?php echo $this->session->userdata('id_jurusan_prasyarat'); ?>" readonly >

                        <input type='text' class="form-control"  value="<?php echo $this->session->userdata('nama_jurusan_prasyarat'); ?>" readonly >

                      </div>

                    </div>

                    <script type="text/javascript">

                    	$(document).ready(function(){

							$('#semester').change(function(){

								$.post('<?php echo base_url();?>perkuliahan/jdl_kuliah2/get_detail_matakuliah_by_semester/'+$(this).val(),{},function(get){

									$('#ala_ala').html(get);

								});

							});

						});

                    </script>

                    <div class="control-group">

                      <label class="control-label">Semester</label>

                      <div class="controls">

                        <select id="semester" class="form-control" name="semester" required="">

                          <option>--Pilih Semester--</option>

                          <option value="1">1</option>

                          <option value="2">2</option>

                          <option value="3">3</option>

                          <option value="4">4</option>

                          <option value="5">5</option>

                          <option value="6">6</option>

                          <option value="7">7</option>

                          <option value="8">8</option>

                        </select>

                      </div>

                    </div>

                    <div class="control-group">
						<label class="control-label">Kelompok Kelas</label>
						<div class="controls">
							<input type="radio" name="st_kelas" value="PG" required=""> A &nbsp;&nbsp; 
							<input type="radio" name="st_kelas" value="SR" required=""> B &nbsp;&nbsp;
							<input type="radio" name="st_kelas" value="PK" required=""> C  &nbsp;&nbsp;
						</div>
					</div>

                    	<div class="control-group">											

							<label class="control-label" for="kelas">Nama Kelas</label>

							<div class="controls">

								<input type="text" class="form-control" placeholder="Input Kelas" id="kelas" name="kelas" required>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->

						<script type="text/javascript">

						$(document).ready(function() {

							$('#ala_ala').change(function() {

								$.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_kode_mk/'+$(this).val(),{},function(get){

									$('#kode_mk').val(get);

								});

							});

						});

						</script>



						<div class="control-group">											

							<label class="control-label" for="firstname">Hari Kuliah</label>

							<div class="controls">

								<!-- <input type="text" class="span2" name="nama_matakuliah"> -->

								<select class="form-control" name="hari_kuliah" id="hari_kuliah" required>

									<option disabled>--Hari Kuliah--</option>

									<option value="1">Senin</option>

									<option value="2">Selasa</option>

									<option value="3">Rabu</option>

									<option value="4">Kamis</option>

									<option value="5">Jum'at</option>

									<option value="6">Sabtu</option>

									<option value="7">Minggu</option>

								</select>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->	

                    

						<div class="control-group">											

							<label class="control-label" for="firstname">Nama Matakuliah</label>

							<div class="controls">

								<!-- <input type="text" class="span2" name="nama_matakuliah"> -->

								<select class="form-control" name="nama_matakuliah" id="ala_ala">

									<option>--Pilih MataKuliah--</option>

									

								</select>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->    

    					

    					<div class="control-group">										

							<label class="control-label" for="kode_mk">Kode MK</label>

							<div class="controls">

								<input type="text" class="span2" name="kd_matakuliah" placeholder="Kode Matakuliah" id="kode_mk" readonly>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->



						<script type="text/javascript">

                        	$(document).ready(function(){

								$('#gedung').change(function(){

									$.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_lantai/'+$(this).val(),{},function(get){

										$('#lantai').html(get);

									});

								});

							});

                    	</script>

						<div class="control-group">											

							<label class="control-label" for="gedung">Gedung</label>

							<div class="controls">

								<select class="form-control" name="gedung" id="gedung">

									<option value="">--Pilih Gedung--</option>

									<?php foreach ($gedung as $isi) {?>

										<option value="<?php echo $isi->id_gedung ?>"><?php echo $isi->gedung ?></option>	

									<?php } ?>

								</select>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->

						<script type="text/javascript">

                        	$(document).ready(function(){

								$('#lantai').change(function(){

									$.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_ruangan/'+$(this).val(),{},function(get){

										$('#ruangan').html(get);

									});

								});

							});

                    	</script>

						<div class="control-group">											

							<label class="control-label" for="lantai">Lantai</label>

							<div class="controls">

								<select name="lantai" id="lantai">

									<option value="">--Pilih Lantai--</option>

								</select>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->

						

						<div class="control-group">											

							<label class="control-label" for="ruangan">Ruangan</label>

							<div class="controls">

								<select name="ruangan" id="ruangan" required>

									<option value="">--Pilih Ruangan--</option>

								</select>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->

						

						<!-- <div class="control-group">											

							<label class="control-label" for="dosen">Dosen</label>

							<div class="controls">

								<input type="hidden" name="id_kary" id="id_kary">

								<input type="text" name="dosen" id="dosen">

							</div> 				

						</div>  -->



					

						<!-- <div class="control-group">											

							<label class="control-label" for="firstname">Perasyarat</label>

							<div class="controls">

							<select class="form-control" name="prasyarat_matakuliah[]" multiple>

								<?php //foreach($matakuliah as $row){ ?>

									<option value="<?php //echo $row->kd_matakuliah; ?>"><?php //echo $row->nama_matakuliah; ?></option>

								<?php //} ?>

							</select>												

							</div>				

						</div> -->

						<div class="control-group">											

							<label class="control-label" for="jam">Jam Masuk</label>

							<div class="controls">

								<input type="text" class="form-control" placeholder="Input Jam Masuk" id="jam_masuk" name="jam_masuk" required>

							</div> <!-- /controls -->				

						</div> <!-- /control-group -->

						<!-- <div class="control-group">											

							<label class="control-label" for="jam">Jam Keluar</label>

							<div class="controls">

								<input type="text" class="form-control" placeholder="Input Jam keluar" id="jam_keluar" name="jam_keluar">

							</div> 

						</div> -->

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <input type="submit" class="btn btn-primary" value="Save"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->



<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit1">            

        </div>
    </div>
</div>

<div class="modal fade" id="editModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit2">
        	
        </div>
    </div>
</div>

<div class="modal fade" id="editModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit3">
        	
        </div>
    </div>
</div>
