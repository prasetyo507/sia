<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>form/formcuti/looad/'+edc);
    }
</script>

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
                <i class="icon-user"></i>
          <h3>Daftar Peserta Semester Pendek</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
                    <form action="<?php echo base_url(); ?>perkuliahan/smt_pdk/update_status" method="post">
                        <input type="submit" class="btn btn-success" value="Submit">
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>NPM</th>
                                    <th>Nama</th>
                                    <th>Fakultas</th>
                                    <th>Jurusan</th>
                                    <th width="40">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($r as $row) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->npm_mahasiswa; ?></td>
                                    <td><?php echo $row->NMMHSMSMHS; ?></td>
                                    <td><?php echo $row->fakultas; ?></td>
                                    <td><?php echo $row->prodi; ?></td>
                                    <?php if (($row->kode_sink != "sp") ){ ?>
                                        <td><input type="checkbox" value="speek<?php echo $row->NIMHSMSMHS;?>" name="status[]" class="form-control"></td>
                                    <?php } else { ?>
                                        <td><input type="checkbox" value="speek<?php echo $row->NIMHSMSMHS;?>" name="status[]" class="form-control" checked disabled></td>
                                    <?php } ?>
                                    

                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>