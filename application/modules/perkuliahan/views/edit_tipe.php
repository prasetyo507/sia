<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM EDIT DATA TIPE MATA KULIAH</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>perkuliahan/kel_mk/update" method="post">
                <div class="modal-body" style="margin-left:-20px;">  
                <input type="hidden" name="id" value="<?php echo $getEdit->id_tipe; ?>"/>  
                    <div class="control-group" id="">
                        <label class="control-label">Kode Tipe</label>
                        <div class="controls">
                            <input type="text" class="span4" name="kode" placeholder="Kode Tipe" class="form-control" value="<?php echo $getEdit->kode_tipe; ?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Tipe Mata Kuliah</label>
                        <div class="controls">
                            <input type="text" class="span4" name="tipe" placeholder="Tipe Mata Kuliah" class="form-control" value="<?php echo $getEdit->tipe_mk; ?>" required/>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>