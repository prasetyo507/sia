<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Data Jadwal Perkuliahan</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>perkuliahan/jdl_kuliah/save_session">
                        <fieldset>                              

                              <div class="control-group">
                                <label class="control-label">Jurusan</label>
                                <div class="controls">
                                  <select class="form-control span6" name="jurusan" id="jurs">
                                    <option>--Pilih Jurusan--</option>
                                    <?php foreach ($prodi as $row) { ?>

				                      <option value="<?php echo $row->kd_prodi;?>"><?php echo $row->prodi;?></option>

				                      <?php } ?>
                                  </select>
                                </div>
                              </div>

                              <div class="control-group">
                                <label class="control-label">Tahun Akademik</label>
                                <div class="controls">
                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">
                                    <option>--Pilih Tahun Akademik--</option>
                                    <?php foreach ($tahunajar as $row) { ?>
                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>  
                            <br/>
                              
                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>
          
        </div>
      </div>
    </div>
  </div>
</div>
