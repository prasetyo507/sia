<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kalender extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(68)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$data['kale'] = $this->app_model->getdata('tbl_kalender','id_kalender','desc');
		$data['page'] = 'kalender_view';
		$this->load->view('template', $data);
	}

	function tambah()
	{
		$m = $this->input->post('mulai_keg');
		$q = explode('/', $m);
		$tahun = $q[2];
		$bulan = $q[1];
		$tgl = $q[0];
		$mulai2 = ''.$tahun.'-'.$bulan.'-'.$tgl.'';

		$w = $this->input->post('akhir_keg');
		$a = explode('/', $w);
		$th = $a[2];
		$bl = $a[1];
		$hr = $a[0];
		$mulai3 = ''.$th.'-'.$bl.'-'.$hr.'';
		// die($this->input->post('mulai_keg'));
		$data = array(
		'jns_kegiatan' 	 => $this->input->post('jk_keg'),
		'deskripsi'		 => $this->input->post('desk'),
		'mulai'			 => $mulai2,
		'akhir' 		 => $mulai3,
		'peserta'		 => $this->input->post('peserta')
		);

		//var_dump($data);die();

		$this->app_model->insertdata('tbl_kalender',$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."perkuliahan/Kalender';</script>";
	}

	function delete($id)
	{
		$this->app_model->deletedata('tbl_kalender','id_kalender',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."perkuliahan/Kalender';</script>";
	}

	function update()
	{
		// $x = $this->input->post('mulai_kegiatan');
		// $e = explode('/', $x);
		// $thn = $e[2];
		// $bln = $e[1];
		// $tggl = $e[0];
		// $mulai4 = ''.$thn.'-'.$bln.'-'.$tggl.'';

		// $l = $this->input->post('akhir_kegiatan');
		// $k = explode('/', $l);
		// $tahu = $k[2];
		// $bula = $k[1];
		// $tggal = $k[0];
		// $mulai5 = ''.$tahu.'-'.$bula.'-'.$tggal.'';

		$data = array(
		'jns_kegiatan' 	 => $this->input->post('jk_keg'),
		'deskripsi'		 => $this->input->post('desk'),
		'mulai'			 => $this->input->post('mulai_kegiatan'),
		'akhir' 		 => $this->input->post('akhir_kegiatan'),
		'peserta'		 => $this->input->post('peserta') 
		);

		$data['nder'] = $this->app_model->updatedata('tbl_kalender','id_kalender',$this->input->post('id_kalender'),$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."perkuliahan/Kalender';</script>";
	}

	function get_list_kal($edk)
	{
		$data['list'] = $this->db->query("select * from tbl_kalender where id_kalender = '$edk'")->row();
		$this->load->view('perkuliahan/edit_kalende', $data);
	}

	function get_desk($ddk)
	{
		$data['list'] = $this->db->query("select * from tbl_kalender where id_kalender = '$ddk'")->row();
		$this->load->view('perkuliahan/desk_kalender', $data);
	}

}

/* End of file  */
/* Location: ./application/controllers/ */