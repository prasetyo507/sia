<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwalujian extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(67)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		
		date_default_timezone_set("Asia/Jakarta"); 
		$this->load->library('user_agent');
	}

	function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
		$data['tahunajar']=$this->app_model->getdata('tbl_tahunajaran', 'id_tahunajaran', 'ASC')->result();
		$data['page'] = 'perkuliahan/ujian_view';
		$this->load->view('template',$data);
	}

	function get_jurusan($id)
	{
		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function view_jadwal()
	{
		$fak = explode('-', $this->input->post('fakultas', TRUE));
		$data['kd_fakultas'] = $fak[0];
		$data['fakultas'] = $fak[1];
		$jur = explode('-', $this->input->post('jurusan', TRUE));
		$data['kd_prodi'] = $jur[0];
		$data['prodi'] = $jur[1];
		$data['angkatan'] = $this->input->post('tahunajaran', TRUE);
		$data['kelas'] = $this->input->post('shift', TRUE);
		
		$data['jadwal_matkul'] = $this->app_model->get_jadwal($jur[0], $data['angkatan'])->result();
		$data['gedung'] = $this->app_model->getdata('tbl_gedung', 'id_gedung', 'ASC')->result();
		$data['jadwal'] = $this->app_model->get_jadwalujian()->result();
		$data['page'] = 'perkuliahan/ujian_lihat';
		$this->load->view('template',$data);
	}

	function edit($id)
	{
		$this->load->view('perkuliahan/ujian_edit');
	}
	
	function save_jadwal(){
		$mulai = strtotime($this->input->post('jam_masuk'));
		$sks = $this->input->post('sks_matakuliah');
		$tambahan = $sks * 40 * 60;
		
		$jam_mulai = date('H:i', $mulai); 
		$jam_berakhir = date('H:i', $mulai+$tambahan);
		
		$data['kd_jadwal_ujian'] = '';
		$data['kd_jadwal_matkul'] = $this->input->post('kd_jadwal');
		$data['hari_jadwal_ujian'] = $this->input->post('hari_ujian');
		$data['tanggal_jadwal_ujian'] = $this->input->post('tanggal_ujian');
		$data['mulai_jadwal_ujian'] = $jam_mulai;
		$data['akhir_jadwal_ujian'] = $jam_berakhir;
		$data['kode_ruangan'] = $this->input->post('ruangan_ujian');
		$data['kd_karyawan'] = $this->input->post('pengawas_ujian');
		
		$this->app_model->insertdata('tbl_jadwal_ujian', $data);
		//redirect($this->agent->referrer());
	}
	
	function cek_jam(){
		$mulai = strtotime($_POST['jam_masuk']);
		$sks = $_POST['sks_matakuliah'];
		$ruangan = $_POST['ruangan_ujian'];
		$tambahan = $sks * 40 * 60;
		$hari = $_POST['hari'];
		$tanggal = $_POST['tanggal'];
		
		$jam_mulai = date('H:i', $mulai); 
		$jam_berakhir = date('H:i', $mulai+$tambahan);
		echo $this->db->query('SELECT * FROM tbl_jadwal_ujian WHERE("'.$jam_mulai.'" BETWEEN mulai_jadwal_ujian AND akhir_jadwal_ujian) or ("'.$jam_berakhir.'" BETWEEN mulai_jadwal_ujian AND akhir_jadwal_ujian) AND kode_ruangan="'.$ruangan.'" AND hari_jadwal_ujian="'.$hari.'" AND tanggal_jadwal_ujian="'.$tanggal.'"')->num_rows();
	}
	
	function get_lantai($id){
		$data = $this->app_model->get_lantai($id);
		$list = "<option> -- </option>";
		foreach($data as $row){
		$list .= "<option value='".$row->id_lantai."'>".$row->lantai."</option>";
		}

		die($list);
	}

	function get_ruangan($id){
		$data = $this->app_model->get_ruang($id);
		$list = "<option> -- </option>";
		foreach($data as $row){
		$list .= "<option value='".$row->id_ruangan."'>".$row->kode_ruangan."</option>";
		}

		die($list);
	}
}

/* End of file Jadwalujian.php */
/* Location: .//C/Users/danum246/AppData/Local/Temp/fz3temp-1/Jadwalujian.php */