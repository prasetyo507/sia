<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sp extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('setting_model');
		if ($this->session->userdata('sess_login') == TRUE) {
		  $akses = $this->role_model->cekakses(80)->result();
		  $aktif = $this->setting_model->getaktivasi('sp')->result();
		  if (count($aktif) != 0) {
				if ($akses != TRUE) {
					echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
					//echo "gabisa";
				}
			} else {
				echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
				//echo "gabisa";
			}
		} else {
		  redirect('auth','refresh');
		}

		//$this->load->library('Cfpdf');
	}


	function index()
	{
		//by login
		$data['matkul_sp'] = $this->app_model->get_matkul_sp('55201')->result();
		$data['page'] = 'perkuliahan/v_sp';
		$this->load->view('template', $data);
	}

	function matkulsp($kode)
	{
		$data['mk'] = $this->app_model->getdetail('tbl_matakuliah','kd_matakuliah',$kode,'kd_matakuliah','asc')->row();
		$data['peserta_sp'] = $this->app_model->get_peserta_sp($kode,'55201')->result();
		//var_dump($data);
		$this->load->view('v_peserta_sp', $data);
	}

	function submit_data()
	{
		$jumlah = count($this->input->post('npm'));
		//var_dump($jumlah);
		for ($i=0; $i < $jumlah; $i++) { 
			$npm = substr($this->input->post('npm['.$i.']', TRUE),0,19);
			$mk = substr($this->input->post('npm['.$i.']', TRUE),19);
			$kodekrs = $this->db->query("select * from tbl_krs where kd_krs = '".$npm."' and kd_matakuliah = '".$mk."'")->row();
			$data['kd_jadwal'] = '3';
			$this->db->where('id_krs', $kodekrs->id_krs);
			$this->db->update('tbl_krs', $data);
			//var_dump($kodekrs);
		}
	}

}

/* End of file Sp.php */
/* Location: ./application/modules/perkuliahan/controllers/Sp.php */