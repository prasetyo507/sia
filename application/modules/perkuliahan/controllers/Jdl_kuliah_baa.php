<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jdl_kuliah_baa extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->output->cache(60);
        // ini_set('memory_limit', '1024M');
        $this->load->library('Cfpdf');
        // error_reporting(0);
        if ($this->session->userdata('sess_login') == TRUE) {

            $cekakses = $this->role_model->cekakses(136)->result();

            if ($cekakses != TRUE) {

                echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";

            }

        } else {

            redirect('auth','refresh');
        }
    }

	public function index()
	{

        $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

        $data['page']='jdl_kuliah_view3';

        $this->load->view('template', $data);		
	}

    public function view()
    {
        error_reporting(0);
        $yearacad = $this->session->userdata('ta');
        $this->load->model('perkuliahan/model_perkuliahan', 'mods');
        $data['rows']=$this->app_model->getdata('tbl_jurusan_prodi', 'kd_fakultas', 'ASC')->result();
        $data['gedung'] = $this->db->get('tbl_gedung')->result();
        $data['matkul']=$this->app_model->getdatamku()->result();
        $data['data'] = $this->mods->get_mku($yearacad)->result();
        $data['page'] = 'jdl_kuliah_view_baa';
        $this->load->view('template', $data);       
    }

	public function view2()
	{
		error_reporting(0);
        $yearacad = $this->session->userdata('ta');
        $this->load->model('perkuliahan/model_perkuliahan', 'mods');
        $data['rows']=$this->app_model->getdata('tbl_jurusan_prodi', 'kd_fakultas', 'ASC')->result();
        $data['gedung'] = $this->db->get('tbl_gedung')->result();
        $data['matkul']=$this->app_model->getdatamku()->result();
        $data['data'] = $this->mods->get_mku($yearacad)->result();
        $data['page'] = 'jdl_kuliah_view_baa2';
        $this->load->view('template', $data);		
	}

    function jumlahpeserta($idjdl)
    {
        // get detail jadwal
        $this->db->select('kd_matakuliah,kd_dosen,kd_jadwal');
        $this->db->where('id_jadwal', $idjdl);
        $detail = $this->db->get('tbl_jadwal_matkul')->row();

        // get amount of student
        $kdjdl = get_kd_jdl($idjdl);
        $this->db->select('npm_mahasiswa');
        $this->db->from('tbl_krs');
        $this->db->where('kd_jadwal', $kdjdl);
        $amout = $this->db->get()->num_rows();

        $data = [
            'title' => $detail->kd_matakuliah.' - '.get_nama_mk($detail->kd_matakuliah,substr($detail->kd_jadwal, 0,5)),
            'nmdosen' => nama_dsn($detail->kd_dosen),
            'jumlah' => $amout
        ];

        echo json_encode($data);
    }

	function sessdestroy()
	{
		$this->session->unset_userdata('sess_ringkasan');
        redirect('perkuliahan/jdl_kuliah_baa/view','refresh');
		
	}
    
    function serverside_loadmku()
    {
        $data = array();

        $load = $this->app_model->getdatajadwalmku($this->session->userdata('ta'))->result();

        foreach ($load as $val) {
            $row = array();
            $row[] = $val->kd_matakuliah;
            $row[] = get_nama_mk($val->kd_matakuliah,substr($val->kd_jadwal, 0,5));
            $row[] = $val->kd_tahunajaran;
            $row[] = get_jur(substr($val->kd_jadwal, 0,5));
            $row[] = get_fak_byprodi(substr($val->kd_jadwal, 0,5));
            $row[] = notohari($val->hari).' / '.substr($val->waktu_mulai,0,5).'-'.substr($val->waktu_selesai,0,5);
            $row[] = nama_dsn($val->kd_dosen);
            $row[] = $val->kode_ruangan;
            $row[] = get_amount_mhs($val->kd_jadwal);
            $row[] = 
                '<a data-toggle="modal" onclick="dosen('.$row->id_jadwal.')" href="#editModal1"class="btn btn-success btn-small" ><i class="btn-icon-only icon-user"> </i></a>'.
                '<a class="btn btn-primary btn-small" onclick="edit('.$row->id_jadwal.')" data-toggle="modal" href="#editModal" ><i class="btn-icon-only icon-pencil"></i></a>'.
                '<a class="btn btn-warning btn-small" href="'.base_url().'akademik/ajar/cetak_absensi/'.$row->id_jadwal.'" ><i class="btn-icon-only icon-print"></i></a>';
            $data[] = $row;
        }

        $columns = array(
                        "recordsTotal" => 10,
                        "recordsFiltered" => 20,
                        "data" => $data
                    );

        echo json_encode($columns);
    }

    public function save_session()
    {
        $ta = $this->input->post('tahunajaran', TRUE);
        $this->session->set_userdata('ta',$ta);
        redirect('perkuliahan/jdl_kuliah_baa/view','refresh');
    }

    function get_detail_matakuliah_by_semester($id){

        $user = $this->session->userdata('sess_login');

        //$data = $this->app_model->get_detail_matakuliah_by_semester($this->session->userdata('id_fakultas_prasyarat'), $this->session->userdata('id_jurusan_prasyarat'),$id);
        $kode = $user['userid'];//$this->session->userdata("id_jurusan_prasyarat");
        $data = $this->db->query('SELECT DISTINCT a.kd_matakuliah, a.nama_matakuliah, a.id_matakuliah FROM tbl_matakuliah a
            join tbl_kurikulum_matkul_new b on a.kd_matakuliah = b.kd_matakuliah
            WHERE a.kd_prodi = '.$kode.' AND b.semester_kd_matakuliah = '.$id.' AND b.`kd_kurikulum` LIKE "%'.$kode.'%" and (a.kd_matakuliah like "MKU-%" or a.kd_matakuliah like "MKDU-%")
            order by nama_matakuliah asc')->result();

        $list = "<option> -- </option>";

        foreach($data as $row){

        $list .= "<option value='".$row->id_matakuliah."'>".$row->kd_matakuliah." - ".$row->nama_matakuliah."</option>";

        }

        die($list);

    }

	public function save()
	{
		
        $n = 6; // jumlah karakter yang akan di bentuk.
        $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";
		foreach ($this->input->post('prodi') as $prodi) {
			$gpass = NULL;
            for ($i = 0; $i < $n; $i++) {
            	$rIdx = rand(1, strlen($chr));
            	$gpass .=substr($chr, $rIdx, 1);
            }

            $cc = $this->db->query('SELECT count(kd_jadwal) as jml FROM tbl_jadwal_matkul WHERE kd_jadwal LIKE "'.$prodi.'%" and kd_tahunajaran = '.$this->session->userdata('ta').'')->row();
			$hasil=$cc->jml;
            
            if($hasil==0){
                $hasilakhir="".$prodi."/".$gpass."/001";
            } elseif($hasil < 10){
                $hasilakhir="".$prodi."/".$gpass."/00".($hasil+1);
            } elseif($hasil < 100){
                $hasilakhir="".$prodi."/".$gpass."/0".($hasil+1);
            } elseif ($hasil < 1000){
                $hasilakhir="".$prodi."/".$gpass."/".($hasil+1);
            }elseif ($hasil < 10000){
                $hasilakhir="".$prodi."/".$gpass."/".($hasil+1);
            }

            $q = $this->db->where('kd_matakuliah', $this->input->post('mk', TRUE))->get('tbl_matakuliah')->row();
    		$w = ($q->sks_matakuliah)*50;
            $time2 = strtotime($this->input->post('jam_masuk', TRUE)) + $w*60;
        	$jumlah_time = date('H:i', $time2);

			$datax[] = array(
                'waktu_kelas'   => $this->input->post('st_kelas', TRUE),
                'kd_jadwal'     => $hasilakhir,
                'hari'          => $this->input->post('hari', TRUE),
                'kd_matakuliah' => $this->input->post('mk', TRUE),
                'kd_ruangan' 	=> $this->input->post('ruangan', TRUE),
                'waktu_mulai'   => $this->input->post('jam_masuk', TRUE),
                'waktu_selesai' => $jumlah_time,
                'kd_tahunajaran'=> $this->session->userdata('ta'),
                'audit_date'    => date('Y-m-d H:i:s'),
                'audit_user'    => 'baa',
                'gabung' 		=> $this->input->post('gabung'),
                'referensi' 	=> $this->input->post('kelas_gab')
			);
		}
		//var_dump($datax);exit();
		$this->db->insert_batch('tbl_jadwal_matkul',$datax);
        echo "<script>alert('Sukses');
		document.location.href='".base_url()."perkuliahan/jdl_kuliah_baa/view';</script>";
	}

    function load_dosen($idk,$gabungan)
    {

        $data['jadwal'] = $this->db->where('id_jadwal',$idk)

                                 ->get('tbl_jadwal_matkul')->row();

        $data['dosen']=$this->db->get('tbl_karyawan')->result();
        $data['gabungan'] = $gabungan;
        $this->load->view('penugasan_dosen_baa',$data);

    }

    function load_dosen_autocomplete()
    {

        $this->db->select("*");

        $this->db->from('tbl_karyawan');

        $this->db->like('nama', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

                            'id_kary'       => $row->id_kary,

                            'nid'           => $row->nid,

                            'value'         => $row->nama

                            );

        }
        echo json_encode($data);
    }

    function update_dosen(){

        $dosen = $this->input->post('kd_dosen');

        $id    = $this->input->post('id_jadwal');

        $gabungan = $this->input->post('gabungan', TRUE);

        $jadwal = $this->db->where('id_jadwal', $id)
                               ->get('tbl_jadwal_matkul')->row();

        if ($gabungan != 1) {
            $cekjumlahsks = $this->db->query('SELECT SUM(mk.`sks_matakuliah`) AS jumlah FROM tbl_jadwal_matkul jdl
                JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
                WHERE (mk.nama_matakuliah not like "tesis%" and mk.nama_matakuliah not like "skripsi%" and mk.nama_matakuliah not like "kerja praktek%" and mk.nama_matakuliah not like "magang%" and mk.nama_matakuliah not like "kuliah kerja%") and jdl.kd_tahunajaran = "'.$this->session->userdata('ta').'" and jdl.`kd_dosen` = "'.$dosen.'" AND SUBSTR(jdl.`kd_jadwal`,1,5) = mk.`kd_prodi` and gabung != 1')->row()->jumlah;

            if ($cekjumlahsks > 18) {
                echo "<script>alert('SKS dosen melebihi');history.go(-1);</script>";
                    exit();
            }
        }

        $jadwal_dosen = $this->db->query('SELECT * FROM tbl_jadwal_matkul jdl 
                                          WHERE jdl.`kd_tahunajaran` = "'.$this->session->userdata('ta').'" AND 
                                          jdl.`kd_dosen` = "'.$dosen.'" AND jdl.`hari` = '.$jadwal->hari.'')->result();

        /**
        $jadwal_masuk1 = $jadwal->waktu_mulai;
        $jadwal_keluar1= $jadwal->waktu_selesai;
         
        $msk1 = explode(":", $jadwal_masuk1);
        $menit_masuk1 = $msk1[0] * 60 + $msk1[1];

        $klr1 = explode(":", $jadwal_keluar1);
        $menit_keluar1 = $klr1[0] * 60 + $klr1[1];

        foreach ($jadwal_dosen as $key) {

            $jadwal_masuk2 = $key->waktu_mulai;
            $jadwal_keluar2= $key->waktu_selesai;
             
            $msk2 = explode(":", $jadwal_masuk2);
            $menit_masuk2 = $msk2[0] * 60 + $msk2[1];

            $klr2 = explode(":", $jadwal_keluar2);
            $menit_keluar2 = $klr2[0] * 60 + $klr2[1];

            if (($menit_masuk1 > $menit_masuk2 && $menit_masuk1 < $menit_keluar2) || ($menit_keluar1 > $menit_masuk2 && $menit_keluar1 < $menit_keluar2) || ($menit_masuk1 == $menit_masuk2)) {
                echo "<script>alert('JadwaL Dosen Bentrok');
                document.location.href='".base_url()."perkuliahan/jdl_kuliah_baa/view';</script>";
                exit();
            }   
        }
        */
        
        $data = array( 'kd_dosen' => $dosen );

        $this->db->where('id_jadwal',$id)->update('tbl_jadwal_matkul', $data);

        redirect('perkuliahan/jdl_kuliah_baa/view','refresh');
    }

    function edit_jadwal($id)
    {
        $data['gedung'] = $this->db->get('tbl_gedung')->result();

        $data['rows'] = $this->db->query('SELECT jdl.*,mk.`nama_matakuliah`,mk.`semester_matakuliah`,b.* FROM tbl_jadwal_matkul jdl
        									LEFT JOIN tbl_ruangan b on jdl.`kd_ruangan` = b.`id_ruangan` 
                                            JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
                                            WHERE id_jadwal = '.$id.'
                                            GROUP BY jdl.`kd_jadwal` ')->row();
        $prodi = substr($data['rows']->kd_jadwal, 0,5);

        $data['matkul']=$this->app_model->getdatamku_edit($prodi)->result();

        $this->load->view('v_jdl_kuliah_edit_baa', $data);

    }

    function edit_jadwal2($id)
    {
        $data['gedung'] = $this->db->get('tbl_gedung')->result();

        $data['rows'] = $this->db->query('SELECT jdl.*,mk.`nama_matakuliah`,mk.`semester_matakuliah`,b.* FROM tbl_jadwal_matkul jdl
                                            LEFT JOIN tbl_ruangan b on jdl.`kd_ruangan` = b.`id_ruangan` 
                                            JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
                                            WHERE id_jadwal = '.$id.'
                                            GROUP BY jdl.`kd_jadwal` ')->row();
        $prodi = substr($data['rows']->kd_jadwal, 0,5);

        $data['matkul']=$this->app_model->getdatamku_edit($prodi)->result();

        $this->load->view('v_jdl_kuliah_edit_baa2', $data);

    }

    function update_data_matakuliah()
    {

        $mk = $this->input->post('kd_matakuliah');
        $jam_masuk = $this->input->post('jam_masuk', TRUE);

        $q = $this->db->where('kd_matakuliah', $mk)->get('tbl_matakuliah')->row();
            $w = ($q->sks_matakuliah)*50;
            $time2 = strtotime($jam_masuk) + $w*60;
            $jumlah_time = date('H:i', $time2);

        $data = array('kd_matakuliah' => $mk, 
                      'hari' => $this->input->post('hari_kuliah'),
                      'waktu_mulai' => $jam_masuk,
                      'kd_ruangan' 	=> $this->input->post('ruangan', TRUE),
                      'waktu_selesai' => $jumlah_time,
                      'waktu_kelas' =>  $this->input->post('st_kelas'),
                      'gabung' =>  $this->input->post('gabung'),
                      'referensi' =>  $this->input->post('kelas_gab')
                      );


        $this->db->where('id_jadwal',$this->input->post('id_jadwal'))->update('tbl_jadwal_matkul', $data);

        echo "<script>alert('Sukses');
        document.location.href='".base_url()."perkuliahan/jdl_kuliah_baa/view';</script>";
    }

    function update_data_matakuliah2()
    {

        $mk = $this->input->post('kd_matakuliah');
        $jam_masuk = $this->input->post('jam_masuk', TRUE);

        $q = $this->db->where('kd_matakuliah', $mk)->get('tbl_matakuliah')->row();
            $w = ($q->sks_matakuliah)*50;
            $time2 = strtotime($jam_masuk) + $w*60;
            $jumlah_time = date('H:i', $time2);

        $data = array('kd_matakuliah' => $mk, 
                      'hari' => $this->input->post('hari_kuliah'),
                      'waktu_mulai' => $jam_masuk,
                      'kd_ruangan'  => $this->input->post('ruangan', TRUE),
                      'waktu_selesai' => $jumlah_time,
                      'waktu_kelas' =>  $this->input->post('st_kelas'),
                      'gabung' =>  $this->input->post('gabung'),
                      'referensi' =>  $this->input->post('kelas_gab')
                      );


        $this->db->where('id_jadwal',$this->input->post('id_jadwal'))->update('tbl_jadwal_matkul', $data);

        echo "<script>alert('Sukses');
        document.location.href='".base_url()."perkuliahan/jdl_kuliah_baa/view';</script>";
    }


    function get_lantai($id){

        $data = $this->app_model->get_lantai($id);

        $list = "<option> -- </option>";

        foreach($data as $row){

        $list .= "<option value='".$row->id_lantai."'>".$row->lantai."</option>";

        }



        die($list);

    }

    function get_kelas_gabungan($id,$mk)
    {
    	$this->load->model('monitoring_model');

        $tahunakademik = $this->session->userdata('ta');

    	$data = $this->monitoring_model->get_mk_gabung($mk,$tahunakademik);

		if ($id > 0) {
			$list = "<option disabled> Pilih Kelas </option>";
			foreach($data as $row){
				$list .= "<option value='".$row->kd_jadwal."'>".$row->kd_matakuliah.' - '.$row->kelas.' - '.$row->nama_matakuliah.' - '.$row->prodi.' - '.$row->id_jadwal."</option>";
			}
		} else {
			$list = "<option value=0> -- </option>";
		}

		echo $list;
    }
	
	function delete ($id)
	{
		$get_kdjadwal = $this->db->where('id_jadwal', $id)->get('tbl_jadwal_matkul')->row();

    	$isScheduleUsed = $this->db->where('kd_jadwal', $get_kdjadwal->kd_jadwal)->get('tbl_krs')->num_rows();

    	if ($isScheduleUsed > 0) { 
    		echo "<script>alert('Tidak dapat menghapus jadwal! Jadwal sudah digunakan oleh mahasiswa.');history.go(-1);</script>";
    		return;
    	}

        $this->db->query('delete from tbl_jadwal_matkul where id_jadwal = '.$id.'');

        redirect('perkuliahan/jdl_kuliah_baa/view','refresh');
	}
	

}

/* End of file Jdl_kuliah_baa.php */
/* Location: ./application/modules/perkuliahan/controllers/Jdl_kuliah_baa.php */