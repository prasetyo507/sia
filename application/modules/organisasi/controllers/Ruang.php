<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ruang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		//$data['lantai']=$this->app_model->getdata('tbl_lantai','id_lantai','asc')->result();
		//$data['x'] = $this->db->query('SELECT * from tbl_ruangan a join tbl_lantai b on a.`id_lantai`=b.`id_lantai` where id_lantai = "'.$this->uri->segment(4).'"')->result();
		$data['ruang']=$this->app_model->get_ruang();
		$data['page']="ruang_view";
		$this->load->view('template', $data);
	}

	function save_ruang()
	{
		$data=array(
		'kode_ruangan'	=> $this->input->post('kd_ruang'),
		'ruangan'		=> $this->input->post('ruang'),
		'id_lantai'		=> $this->input->post('lants'),
		'kuota'			=> $this->input->post('kuota'),
		'deskripsi'		=> $this->input->post('deskrip'),
		// 'id_gedung'		=> $this->input->post('building')
		);
		$this->app_model->insertdata('tbl_ruangan', $data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."organisasi/lantai/detil_lt/".$this->input->post('lants')."';</script>";
	}

	function update_ruang()
	{
		$data=array(
		'kode_ruangan'	=> $this->input->post('kd_ruangs'),
		'ruangan'		=> $this->input->post('ruangs'),
		'id_lantai'		=> $this->input->post('lantai'),
		'kuota'			=> $this->input->post('kuotas'),
		'deskripsi' 	=> $this->input->post('deskrips')
		);
		$this->app_model->updatedata('tbl_ruangan','id_ruangan',$this->input->post('hd_ruang'),$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."organisasi/ruang/detil_rg/".$this->input->post('lantai')."';</script>";
	}

	function load_ruang($edr)
	{
		$data['query'] = $this->db->query("select * from tbl_ruangan where id_ruangan = '$edr'")->row();
		//$data['lantais']=$this->app_model->getdata('tbl_lantai','id_lantai','asc')->result();
		//$data['lantais']=$this->app_model->get_lantai();;
		$data['lantais']=$this->db->query("SELECT * from tbl_lantai a
											join tbl_ruangan b on a.`id_lantai`=b.`id_lantai`
											where id_ruangan='$edr'")->result();
		//$data['hetdah']=$this->app_model->getdetail('tbl_gedung','id_gedung',$data['lantais'],'id_gedung','asc')->result();
		//var_dump($data['lantais']);die();
		$this->load->view('organisasi/edit_ruang_view', $data);
	}

	function del_ruang($id)
	{
		$this->app_model->deletedata('tbl_ruangan','id_ruangan',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."organisasi/lantai/detil_lt/".$this->uri->segment(4).";</script>";
	}

	function detil_rg($id)
	{
		$data['det'] =  $this->db->query('SELECT * from tbl_gedung a join tbl_lantai b on a.`id_gedung`=b.`id_gedung` where b.`id_lantai`="'.$id.'"')->row();
		$data['lantai']=$this->app_model->getdata('tbl_lantai','id_lantai','asc')->row($id);
		$data['x']=$this->app_model->getdetail('tbl_ruangan','id_ruangan',$id,'id_ruangan','asc')->result();
		$data['x']=$this->app_model->get_ruang($id);
		$data['page']="ruang_view";
		$this->load->view('template', $data);
	}

}
/* Location: ./application/controllers/ */