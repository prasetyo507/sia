            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Lantai</h4>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    $.post('<?php echo base_url()?>organisasi/lantai/load_lantai'+$(this).val(),{},function(get){
                        $("#ipt_lantai").html(get);
                    });
                });
            </script>
            <form class ='form-horizontal' action="<?php echo base_url();?>organisasi/lantai/update_lantai" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Lantai</label>
                        <div class="controls">
                            <input type="hidden" name="id_lantai" value="<?php echo $query->id_lantai;?>">
                            <input type="text" id="ipt_lantai" class="span4" name="lantai" placeholder="Input Gedung" class="form-control" value="<?php echo $query->lantai?>" required/>
                        </div>
                    </div> 
                    <div class="control-group" id="">
                        <label class="control-label">Gedung</label>
                        <div class="controls">
                            <select class="form-control" name="lants">
                                <option>--Pilih Gedung--</option>
                                <?php foreach ($gedung as $row) { ?>
                                    <option value="<?php echo $row->id_gedung;?>"><?php echo $row->gedung;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>              
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>