<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Detail Karyawan</h4>
</div>
<form class ='form-horizontal' action="<?php //echo base_url();?>organisasi/karyawan/edit" method="post" enctype="multipart/form-data">
    <div class="modal-body" style="margin-left: -60px;">  
        <div class="control-group" id="">
            <label class="control-label">NIP</label>
            <div class="controls">
                <input type="text" class="span4" name="nip" placeholder="Input NIP" class="form-control" value="<?php echo $wew->nip; ?>" required disabled=""/>
            </div>
        </div> 
        <div class="control-group" id="">
            <label class="control-label">Nama</label>
            <div class="controls">
                <input type="text" class="span4" name="nama" placeholder="Input Nama" class="form-control" value="<?php echo $wew->nama; ?>" required disabled=""/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Unit</label>
            <div class="controls">
                <input type="text" class="span4" name="unit" placeholder="Input Unit" class="form-control" value="<?php echo $wew->unit; ?>" required disabled=""/>
            </div>
        </div> 
        <div class="control-group" id="">
            <label class="control-label">Jabatan</label>
            <div class="controls">
                <input type="text" class="span4" name="jabatan" placeholder="Input Jabatan" class="form-control" value="<?php echo $wew->jabatan; ?>" required disabled=""/>
            </div>
        </div> 
        <div class="control-group" id="">
            <label class="control-label">Status</label>
            <div class="controls">
                <?php if ($wew->status == '1') { ?>
                    <input type="text" class="span4" name="jabatan" placeholder="Input Jabatan" class="form-control" value="PHL" required disabled=""/>
                <?php } else { ?>
                    <input type="text" class="span4" name="jabatan" placeholder="Input Jabatan" class="form-control" value="TETAP" required disabled=""/>
                <?php } ?>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">No. HP</label>
            <div class="controls">
                <input type="text" class="span4" name="hp" placeholder="Input Nomor HP" class="form-control" value="<?php echo $wew->no_hp; ?>" required/ disabled="">
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">No. KTP</label>
            <div class="controls">
                <input type="text" class="span4" name="ktp" placeholder="Input No. KTP" class="form-control" value="<?php echo $wew->no_ktp; ?>" required/ disabled="">
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">E-mail</label>
            <div class="controls">
                <input type="text" class="span4" name="email" placeholder="Input E-mail" class="form-control" value="<?php echo $wew->email; ?>" required/ disabled="">
            </div>
        </div><div class="control-group" id="">
            <label class="control-label">Alamat</label>
            <div class="controls">
                <textarea type="text" class="span4" name="alamat" placeholder="" class="form-control" value="" required disabled=""><?php echo $wew->alamat; ?></textarea>
            </div>
        </div>
        
        <div class="control-group" id="">
            <label class="control-label">Tanggal Masuk</label>
            <div class="controls">
                <input type="text" id="tgl" class="span4" name="thms" placeholder="" class="form-control" value="<?php echo $wew->th_masuk; ?>" required disabled=""/>
            </div>
        </div>
        <input type="hidden" name="iden" value="<?php echo $wew->id; ?>" placeholder=""> 
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
        
    </div>
</form>