<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Api extends CI_Controller {

	var $client_key;

	public function __construct() {
		parent::__construct();

		if ($this->input->get_request_header('app-key') !== APPKEYS) {
			$response = ['status' => 90, 'message' => 'app key not valid'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}

		$this->client_key = $this->input->get_request_header('client-key');
		
	}

	public function index() {
		echo "ini dia";
	}

	function cek_nomer($nomer) 
	{
		$query = "SELECT COUNT(*) AS jml , mhs.`NMMHSMSMHS` AS nama FROM tbl_bio_mhs bio
									JOIN tbl_mahasiswa mhs ON bio.`npm` = mhs.`NIMHSMSMHS` WHERE no_hp LIKE '%" . $nomer . "' LIMIT 1";

		$rs = $this->db->query($query)->row();

		if ($rs->jml >= 1) {
			$data['response'] = 10;
			$data['nama'] = $rs->nama;
		} elseif ($rs->jml = 0) {
			$data['response'] = 11;
		}

		echo json_encode($data);

	}

	/**
	* Get academic conselor
	* @return object
	*/
	function get_pa() 
	{
		$isPhoneExist = $this->isPhoneExist($this->client_key);

		// if phone number doesn't exist
		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();

		} else {

			// get data nama_dosenm nid, and tlp_dosen
			$this->db->select('a.nama as nama_dosen, a.nid, c.tlp as tlp_dosen');
			$this->db->from('tbl_karyawan a');
			$this->db->join('tbl_pa b', 'a.nid = b.kd_dosen');
			$this->db->join('tbl_biodata_dosen c', 'c.nid = a.nid', 'left');
			$this->db->where('b.npm_mahasiswa', $isPhoneExist->row()->npm);
			$getResponse = $this->db->get();

			// if data below not fount
			if (!$getResponse->result()) {
				$response = ['status' => 23, 'message' => 'data not found' ];
				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();

			} else {
				$dataResponse = [
					'nama_dosen' => $getResponse->row()->nama_dosen,
					'nid' => $getResponse->row()->nid,
					'tlp_dosen' => $getResponse->row()->tlp_dosen,
				];

				$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();
			}
		}

	}

	function get_khs2($semester) 
	{
		$this->load->library('Cfpdf');

		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs')->row();

		// if phone number doesn't exist
		if (!$isPhoneExist) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();

		} else {
			$nim = $isPhoneExist->npm;
			$prodi = get_mhs_jur($nim);
			$tas = $this->db->query("SELECT distinct kd_krs from tbl_krs where npm_mahasiswa = '" . $nim . "'
										and semester_krs = " . $semester . " ")->row();
			// if kd_krs not found
			if ($tas->kd_krs) {
				$data['ta'] = $tas;
				$data['krs'] = $this->db->query('SELECT distinct b.nama_matakuliah, b.sks_matakuliah, b.kd_matakuliah
													FROM tbl_krs a
													JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
													where a.`npm_mahasiswa` = ' . $nim . '
													and b.kd_prodi = ' . $prodi . '
													and a.semester_krs = ' . $semester . '')->result();
				$data['semester'] = $semester;
				$data['npm'] = $nim;

				// if data below already exists
				if (!file_exists(base_url() . 'downloaded/khs/KHS_' . $nim . '_' . substr($tas->kd_krs, 12, 5) . '.pdf')) {
					$this->load->view('api/print_khs', $data);
				}

				$this->output
					->set_status_header(200)
					->set_header('Content-Disposition: attachment; filename="KHS_' . $nim . '_' . substr($tas->kd_krs, 12, 5) . '.pdf"')
					->set_content_type('application/pdf')
					->set_output(file_get_contents(base_url() . 'downloaded/khs/KHS_' . $nim . '_' . substr($tas->kd_krs, 12, 5) . '.pdf'))
					->_display();
				exit();
			} else {
				$response = ['status' => 23, 'message' => 'KHS not found'];
				$this->output
					->set_status_header(400)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();
			}
		}
	}

	function get_khs() 
	{
		$semester = yearBefore();

		$this->load->library('Cfpdf');

		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs')->row();

		// if phone number doesn't exist
		if (!$isPhoneExist) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();

		} else {
			$nim = $isPhoneExist->npm;
			$prodi = get_mhs_jur($nim);
			$tas = $this->db->query("SELECT distinct kd_krs from tbl_krs where kd_krs like '" . $nim . $semester."%'")->row();
			// if kd_krs not found
			if ($tas->kd_krs) {
				$data['ta'] = $tas;
				$data['krs'] = $this->db->query("SELECT distinct b.nama_matakuliah, b.sks_matakuliah, b.kd_matakuliah
													FROM tbl_krs a
													JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
													where a.`npm_mahasiswa` = '" . $nim . "'
													and b.kd_prodi = '" . $prodi . "'
													and a.kd_krs like '" . $nim.$semester . "%'")->result();
				$data['semester'] = $semester;
				$data['npm'] = $nim;

				// if data below already exists
				if (!file_exists(base_url() . 'downloaded/khs/KHS_' . $nim . '_' . substr($tas->kd_krs, 12, 5) . '.pdf')) {
					$this->load->view('api/print_khs', $data);
				}

				$dataResponse = ['status' => 1, 'data' => base_url() . 'downloaded/khs/KHS_' . $nim . '_' . substr($tas->kd_krs, 12, 5) . '.pdf'];

				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf8')
					->set_output(json_encode($dataResponse))
					->_display();
				exit();
			} else {
				$response = ['status' => 23, 'message' => 'KHS not found'];
				$this->output
					->set_status_header(400)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();
			}
		}
	}

	function payment() 
	{
		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs');

		// if phone number doesn't exist
		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();

		} else {

			$activeAcademicYear = getactyear();

			// get payment in active year
			$this->db->select('tahunajaran, transaksi_terakhir as tgl_validasi, status, briva_mhs as briva, semester');
			$this->db->from('tbl_sinkronisasi_renkeu');
			$this->db->where('npm_mahasiswa', $isPhoneExist->row()->npm);
			$this->db->where('tahunajaran', $activeAcademicYear);
			$getResponse = $this->db->get();

			// if data below not fount
			if (!$getResponse->result()) {
				$response = ['status' => 23, 'message' => 'data not found' ];
				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();

			} else {

				switch ($getResponse->row()->status) {
				case '1':
					$status = 'Daftar ulang';
					break;

				case '2':
					$status = 'Telah melakukan pembayaran hingga UTS';
					break;

				case '3':
					$status = 'Telah melakukan pembayaran UTS susulan';
					break;

				case '4':
					$status = 'Telah melakukan pembayaran hingga UAS';
					break;

				case '5':
					$status = 'Cuti';
					break;
				}

				$dataResponse = [
					'tahunajaran' => get_thnajar($getResponse->row()->tahunajaran),
					'tgl_validasi' => $getResponse->row()->tgl_validasi,
					'status' => $status,
					'semester' => $getResponse->row()->semester,
				];
				$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();
			}
		}
	}

	function schedule($day) 
	{
		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs');

		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();

		} else {
			$activeAcademicYear = getactyear();

			$this->db->select('a.hari, a.waktu_mulai, a.waktu_selesai, a.kd_dosen as dosen, a.kd_ruangan as ruang, a.kd_matakuliah as matakuliah, a.kd_jadwal');
			$this->db->from('tbl_jadwal_matkul a');
			$this->db->join('tbl_krs b', 'a.kd_jadwal = b.kd_jadwal');
			$this->db->where('a.hari', $day);
			$this->db->like('b.kd_krs', $isPhoneExist->row()->npm . $activeAcademicYear, 'after');
			$getResponse = $this->db->get();

			if (!$getResponse->result()) {
				$response = ['status' => 23, 'message' => 'data is empty' ];
				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();

			} else {

				foreach ($getResponse->result() as $key) {
					$dataResponse[] = [
						'hari' => notohari($key->hari),
						'jam_masuk' => $key->waktu_mulai,
						'jam_keluar' => $key->waktu_selesai,
						'dosen' => nama_dsn($key->dosen),
						'ruang' => get_room($key->ruang),
						'matakuliah' => get_nama_mk($key->matakuliah, substr($key->kd_jadwal, 0, 5)),
					];
				}

				$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();

			}

		}

	}

	function briva() 
	{
		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs');

		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'message' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();

		} else {
			$this->db->where('NIMHSMSMHS', $isPhoneExist->row()->npm);
			$getResponse = $this->db->get('tbl_mahasiswa');

			if (!$getResponse->result()) {
				$response = ['status' => 23, 'message' => 'data is empty'];
				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();

			} else {
				$dataResponse = [
					'nomor_briva' => '70306' . substr($getResponse->row()->NIMHSMSMHS, 2),
					'nama' => $getResponse->row()->NMMHSMSMHS,
				];

				$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();
			}

		}

	}

	function examination($examptype) 
	{
		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs');

		if ($examptype === 1) {
			$tipeuji = 'UTS';
		} else {
			$tipeuji = 'UAS';
		}

		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'message' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();

		} else {
			$activeAcademicYear = getactyear();

			$this->db->like('kd_krs', $isPhoneExist->row()->npm . $activeAcademicYear, 'after');
			$listKrs = $this->db->get('tbl_krs')->result();

			foreach ($listKrs as $key) {
				$this->db->where('kd_jadwal', $key->kd_jadwal);
				$this->db->where('tipe_uji', $examptype);
				$dataExam = $this->db->get('tbl_jadwaluji')->result();

				foreach ($dataExam as $val) {
					$listExam[] = [
						'nama_matakuliah' => get_nama_mk(get_dtl_jadwal($val->kd_jadwal)['kd_matakuliah'], substr($val->kd_jadwal, 0, 5)),
						'tanggal_ujian' => $val->start_date,
						'jenis_ujian' => $tipeuji,
						'ruang' => get_room($val->kd_ruang),
					];
				}
			}

			if (empty($listExam)) {
				$response = [ 'status' => 23, 'message' => 'data is empty', ];
			} else {
				$response = ['status' => 1, 'message' => 'success', 'data' => $listExam];
			}

			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();

		}

	}

	/**
	 * Transcript
	 * @param   $phone [for validate phone number]
	 * @return  File Pdf
	 */
	public function transcript() {
		$isPhoneExist = $this->isPhoneExist($this->client_key);
		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'message' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();

		}

		$npm = $isPhoneExist->row()->npm;

		$data['head'] = $this->db->query('SELECT * from tbl_mahasiswa a join tbl_jurusan_prodi b
										on a.`KDPSTMSMHS`=b.`kd_prodi` join tbl_fakultas c
										on b.`kd_fakultas`=c.`kd_fakultas`
										where a.`NIMHSMSMHS` = "' . $npm . '"')->row();
		$data['q'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai nl
										WHERE nl.`NIMHSTRLNM` = "' . $npm . '"
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();
		$data['q_konversi'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM`  ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai_konversi nl
										WHERE nl.`NIMHSTRLNM` = "' . $npm . '"
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();

		$this->load->library('Cfpdf');

		$data['npm'] = $npm;
		$data['output'] = 'F';
		$filename = "transcript-" . $npm . ".pdf";

		if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename)) {
			$data['pathPdf'] = $_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename;
			$this->load->view('akademik/transkrip_pdf3', $data);
		}

		$response = ['status' => 1, 'data' => base_url() . 'downloaded/transkrip/' . $filename];
		$this->output
			->set_status_header(200)
			// ->set_header('Content-Disposition: attachment; filename="transcript-'.$npm.'.pdf"')
			->set_content_type('application/json', 'utf-8') // You could also use ".jpeg" which will have the full stop removed before looking in config/mimes.php
			// ->set_output(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename))
			->set_output(json_encode($response))
			->_display();
		exit();
	}

	public function get_krs()
	{
		$isPhoneExist = $this->isPhoneExist($this->client_key);

		if (!$isPhoneExist->result()) {
			# code...
		} else {
			# code...
		}
		
	}

	public function thesisSchedule()
	{
		$isPhoneExist = $this->isPhoneExist($this->client_key);

		// if phone number doesn't exist
		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();

		} else {

			// get data nama_dosenm nid, and tlp_dosen
			$this->db->select('*');
			$this->db->from('tbl_announcement');
			$this->db->where('category','jdl-skr');
			$getResponse = $this->db->get();

			// if data below not fount
			if (!$getResponse->result()) {
				$response = ['status' => 23, 'message' => 'data not found' ];
				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();

			} else {
				$dataResponse = [
					'category' => 'jadwal sidang skripsi',
					'tahunakademik' => $getResponse->row()->tahunakademik,
					'note' => $getResponse->row()->note,
				];

				$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();
			}
		}
	}

	/**
	 * Check Phone Number Exist
	 *  @param   $phone phone number
	 *  @return  Object
	 */
	public function isPhoneExist($phone = null) 
	{
		$this->db->where('no_hp', $phone);
		$isPhoneExist = $this->db->get('tbl_bio_mhs');
		return $isPhoneExist;
	}

}

/* End of file Api.php */
/* Location: ./application/controllers/Api.php */