<!DOCTYPE html>
<?php //var_dump($krs);die(); ?>
<html lang="en">

<head>

    <meta charset="utf-8">

              <?php $user = $this->session->userdata('sess_login');

              if ($user['user_type'] == 1) {

                $nama = $this->app_model->getdetail('tbl_karyawan','nid',$user['userid'],'nik','asc')->row(); $name = $nama->nama;

              } elseif($user['user_type'] == 3) {

                $nama = $this->app_model->getdetail('tbl_divisi','kd_divisi',$user['userid'],'kd_divisi','asc')->row(); $name = $nama->divisi;

              } elseif($user['user_type'] == 4) {

                $nama = $this->app_model->getdetail('tbl_karyawan_2','nip',$user['userid'],'nip','asc')->row(); $name = $nama->nama;

              } else {

                $nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$user['userid'],'NIMHSMSMHS','asc')->row(); $name = $nama->NMMHSMSMHS;

              }

               ?>
    <title>Siakad UBJ | <?php echo $name?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/logo.ico"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/pages/dashboard.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url();?>assets/css/pages/reports.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/bs_notif/bootstrap-notify.js"></script>

   

<style type="text/css">



</style>
    

</head>

<body>

<div class="navbar navbar-fixed-top">

  <div class="navbar-inner">

    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span

                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#"><img src="<?php echo base_url();?>assets/logo.png" style="width:40px;float:left;margin-top:-8px;margin-bottom:-13px">

            <span style="margin-left:10px;line-height:-15px">SISTEM INFORMASI AKADEMIK UBHARA JAYA</span></a>

      <div class="nav-collapse">

        <ul class="nav pull-right">

          <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size:16px">


              <i class="icon-user"></i> <?php echo $name; ?> <b class="caret"></b></a>

            <ul class="dropdown-menu">

              <li><a href="<?php echo base_url();?>extra/account">Ganti Password</a></li>

              <li><a href="<?php echo base_url();?>auth/logout">Logout</a></li>

            </ul>

          </li>

        </ul>

      </div>

      <!--/.nav-collapse --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /navbar-inner --> 

</div>

<!-- /navbar -->

<div class="subnavbar">

  <div class="subnavbar-inner">

    <div class="container">

      <ul class="mainnav">

        <li class="active" ><a href="<?php echo base_url();?>home"><i class="icon-home"></i><span >Dashboard</span> </a>

        <?php $q = $this->role_model->getparentmenu()->result(); foreach ($q as $menu) { if (($menu->url) == '-') { ?>

            <li class="dropdown active"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">

        <?php } elseif (($menu->url) == '#') { ?>

            <li class="active" ><a href="<?php echo $menu->url; ?>">

        <?php } else { ?>

            <li class="active" ><a href="<?php echo base_url();?><?php echo $menu->url; ?>">

        <?php } ?>

            <i class="<?php echo $menu->icon; ?>"></i><span ><?php echo $menu->menu; ?></span> </a>



        <?php if (($menu->url) == '-') { ?>

                <ul class="dropdown-menu">

                 <?php $qd = $this->role_model->getmenu($menu->id_menu)->result(); foreach ($qd as $row) { ?>

                     <?php if (($row->url) == '#') { ?>

                        <li><a href="<?php echo $row->url; ?>">

                    <?php } else { ?>

                        <li><a href="<?php echo base_url();?><?php echo $row->url; ?>">

                    <?php } ?>

                    <?php echo $row->menu; ?></a></li>

                    <?php } ?>

                  </ul>

                <?php } ?>

            </li>

        <?php } ?>

      </ul>

    </div>

    <!-- /container --> 

  </div>

  <!-- /subnavbar-inner --> 

</div>

<!-- /subnavbar -->

<div class="main">

  <div class="main-inner">

    <div class="container">

      <!-- load page -->

      <?php $this->load->view($page); ?>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

<!-- /main -->

<div class="footer">

  <div class="footer-inner">

    <div class="container">

      <div class="row">

        <div class="span12"> &copy; <?php date_default_timezone_set('Asia/Jakarta');echo date('Y'); ?> - <a target="_blank" href="http://ubharajaya.ac.id/">Universitas Bhayangkara Jakarta Raya</a>. </div>

        <!-- /span12 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /footer-inner --> 

</div>

<!-- /footer --> 

<!-- Le javascript

================================================== --> 



<!-- Placed at the end of the document so the pages load faster -->  

<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

<!-- script language="javascript" type="text/javascript" src="<?php //echo base_url();?>assets/js/full-calendar/fullcalendar.min.js"></script -->

<script type="text/javascript">

$(document).ready(function() {

        var date = new Date();

        var d = date.getDate();

        var m = date.getMonth();

        var y = date.getFullYear();

        var calendar = $('#calendar').fullCalendar({

          header: {

            left: 'prev,next today',

            center: 'title',

            right: 'month,agendaWeek,agendaDay'

          },

          selectable: true,

          selectHelper: true,

          select: function(start, end, allDay) {

            var title = prompt('Event Title:');

            if (title) {

              calendar.fullCalendar('renderEvent',

                {

                  title: title,

                  start: start,

                  end: end,

                  allDay: allDay

                },

                true // make the event "stick"

              );

            }

            calendar.fullCalendar('unselect');

          },

          editable: true,

          <?php $kalender = $this->app_model->getdata('tbl_kalender_dtl','mulai','asc')->result(); ?>

          events: [

            <?php foreach ($kalender as $value) { ?>

            {

              title: '<?php echo $value->jns_kegiatan; ?>',

              start: '<?php echo $value->mulai; ?>',

              end: '<?php echo $value->akhir; ?>'

            },

            <?php } ?>

          ]

        });

      });

</script>

<script src="<?php echo base_url();?>assets/js/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/js/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">

    $(function() {

        $("#example1").dataTable();

        $("#example3").dataTable();

        $("#example4").dataTable();

        $("#example5").dataTable();

        $('#example2').dataTable({

            "bPaginate": true,

            "bLengthChange": true,

            "bFilter": true,

            "bSort": true,

            "bInfo": true,

            "bAutoWidth": true

        });

    });

</script>
<?php
$user = $this->session->userdata('sess_login');

$pecah = explode(',', $user['id_user_group']);
$jmlh = count($pecah);
    
for ($i=0; $i < $jmlh; $i++) { 
  $grup[] = $pecah[$i];
}

if ( (in_array(6, $grup))) {


}elseif ((in_array(5, $grup))){

 
}

if(isset($krs)) {
  if ($krs->status == 1) {
    $stat = 'Telah di Setujui';
  }elseif ($krs->status == 2) {
    $stat = 'REVISI';
  }else{
    $stat = 'Pengajuan';
  }

  if($user['userid'] == '201210225045'){ ?>

    <script type="text/javascript">
      Push.create("Kartu Rencana Studi - <?php echo $user['user_type'] ?>",
        body: "<?php echo $krs->note ?>",
        icon: {x32: '<?php echo base_url();?>assets/logo.png'},
        timeout: 15000,
        onClick: function () {
            window.focus();
            this.close();
        }
    });
    </script><?php
  }elseif ($user['userid'] == '029809004') { ?>
    <script type="text/javascript">
      Push.create("Kartu Rencana Studi - <?php echo $stat ?>",
        body: "<?php echo $krs->note ?>",
        icon: {x32: '<?php echo base_url();?>assets/logo.png'},
        timeout: 15000,
        onClick: function () {
            window.focus();
            this.close();
        }
    });
    </script><?php
  }   
}
?>

<style type="text/css" !important>
@import url(http://fonts.googleapis.com/css?family=Old+Standard+TT:400,700);
[data-notify="container"][class*="alert-pastel-"] {
  background-color: rgb(255, 255, 238) !important;
  border-width: 0px !important;
  border-left: 15px solid rgb(255, 240, 106) !important;
  border-radius: 0px !important;
  box-shadow: 0px 0px 5px rgba(51, 51, 51, 0.3) !important;
  font-family: 'Old Standard TT', serif !important;
  letter-spacing: 1px !important;

}
[data-notify="container"].alert-pastel-info {
  border-left-color: rgb(255, 179, 40) !important;
}
[data-notify="container"].alert-pastel-danger {
  border-left-color: rgb(255, 103, 76) !important;
}
[data-notify="container"][class*="alert-pastel-"] > [data-notify="title"] {
  color: rgb(80, 80, 57) !important;
  display: block !important;
  font-weight: 700 !important;
  margin-bottom: 5px !important;
}
[data-notify="container"][class*="alert-pastel-"] > [data-notify="message"] {
  font-weight: 400 !important;
}
</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93675871-1', 'auto');
  ga('send', 'pageview'); 




  // $.notify({
  //   title: 'Application Installing',
  //   message: 'Developing nations social innovation shift globalization, invest safeguards life-expectancy positive social change. Gender care, new approaches empowerment diversity.'
  // },{
  //   type: 'pastel-info',
  //   delay: 5000,
  //   template: '<div data-notify="container" class="col-sm-3 alert alert-{0}" role="alert">' +
  //     '<span data-notify="title">{1}</span>' +
  //     '<span data-notify="message">{2}</span>' +
  //   '</div>'
  // });


</script>


</body>

</html>

