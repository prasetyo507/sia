
<div class="row">
<?php 
  $log = $this->session->userdata('sess_login');
  $akt = substr($log['userid'], 0,4);
  $actYear = getactyear();

  if ($notif == 'mhs') {
    $krs = $this->temph_model->notif_krs($log['userid'].$actYear)->row();
    $khs = $this->temph_model->notif_khs($log['userid'].$actYear)->row();
    if ($krs->status_verifikasi == 1 and is_null($krs->notif)) { 
     if ($akt != date('Y')) { ?>
        <div class="span6">
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Perhatian!</strong> KRS Anda telah disetujui oleh pembimbing akademik. 
            <a href="<?= base_url('home/notif_update_krs/'.$log['userid'].$actYear); ?>" onclick="" title=""><b>Lihat ...</b></a>
          </div>
        </div>
      <?php } 
      }
    if ($khs->flag_publikasi == 2 and is_null($khs->notif)) { ?>
      <div class="span6">
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>Perhatian!</strong> Nilai anda telah dapat dilihat. 
          <a href="<?= base_url('home/notif_update_khs/'.$log['userid'].'/'.$actYear); ?>" onclick="" title=""><b>Lihat ...</b></a>
        </div>
      </div>
  <?php } 
    
  } elseif ($notif == 'dpa') {
    $ajukrs = $this->temph_model->notif_for_pa($log['userid'],$actYear)->result();
    if (count($ajukrs) > 0) { ?>
      <div class="span6">
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>Perhatian!</strong> <?= count($ajukrs); ?> mahasiswa melakukan pengajuan KRS dan belum ditindak lanjuti. 
          <a href="<?= base_url('home/notif_pa/'.$actYear); ?>" onclick="" title=""><b>Lihat ...</b></a>
        </div>
      </div>
  <?php } } ?>

</div>
<div class="row">

  <div class="span8">

    <div class="widget widget-nopad">

      <div class="widget-header"> <i class="icon-list-alt"></i>

        <h3> Kalender Akademik Universitas Bhayangkara Jakarta Raya</h3>

      </div>

      <div class="widget-content">
        <div class="tabbable">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#formcontrols" data-toggle="tab">Kalender Akademik</a>
            </li>
            <li><a href="#pra" data-toggle="tab">Pra Perkuliahan</a></li>
            <li><a href="#kuliah" data-toggle="tab">Semester Perkuliahan</a></li>
            <li><a href="#bayar" data-toggle="tab">Pembayaran Perkuliahan</a></li>
          </ul>
          
            <div class="tab-content">
              <div class="tab-pane  active" id="formcontrols">
                <embed 
                  src="<?= base_url('calendar/old/kalender/') ?>" 
                  style="border: 0" 
                  width="750" 
                  height="670" 
                  frameborder="0" 
                  scrolling="no"/>
              </div>
              
              <div class="tab-pane" id="pra">
                <b><center>KEGIATAN PRA PERKULIAHAN</center></b><br>

                <table class="table table-bordered table-striped">
                  <thead>
                    <tr> 
                      <th>No</th>
                      <th>Kegiatan</th>
                      <th>Waktu Kegiatan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($calender1 as $isi) { 

                      if ($isi->mulai == $isi->ahir) {
                          $waktu = TanggalIndo($isi->mulai);
                      }else{
                          $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                      } ?>

                      <tr>
                          <td><?= $no ?></td>
                          <td><?= $isi->kegiatan ?></td>
                          <td><?= $waktu ?></td>
                      </tr>
                      <?php $no++; } ?>
                  </tbody>
                </table>
              </div>
              <div class="tab-pane" id="kuliah">
                <b><center>KEGIATAN PERKULIAHAN</center></b><br>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr> 
                        <th>No</th>
                        <th>Kegiatan</th>
                        <th>Waktu Kegiatan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      $no=1; 
                      foreach ($calender2 as $isi) { 

                        if ($isi->mulai == $isi->ahir) {
                            $waktu = TanggalIndo($isi->mulai);
                        }else{
                            $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                        } ?>

                        <tr>
                            <td><?= $no ?></td>
                            <td><?= $isi->kegiatan ?></td>
                            <td><?= $waktu ?></td>
                        </tr>
                    <?php $no++; } ?>
                  </tbody>
                </table>
              </div>
              <div class="tab-pane" id="bayar">
                <b><center>PEMBAYARAN PERKULIAHAN</center></b><br>
                <table class="table table-bordered table-striped">
                  <thead>
                      <tr> 
                          <th>No</th>
                          <th>Kegiatan</th>
                          <th>Waktu Kegiatan</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($calender3 as $isi) { 

                      if ($isi->mulai == $isi->ahir) {
                          $waktu = TanggalIndo($isi->mulai);
                      }else{
                          $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                      } ?>

                      <tr>
                          <td><?= $no ?></td>
                          <td><?= $isi->kegiatan ?></td>
                          <td><?= $waktu ?></td>
                      </tr>
                      <?php $no++; } ?>
                  </tbody>
                </table>  
              </div>
            </div> 
        </div>   
      </div>
    </div>
  </div>

  <div class="span4">
    <div class="widget widget-nopad">
      <div class="widget-header"> <i class="icon-list-alt"></i>
        <h3> Change Log SIA</h3>
      </div>

      <!-- /widget-header -->

      <div class="widget-content">
        <ul class="news-items">
          <li>
            
          </li>
        </ul>
      </div> 

    </div>
  </div>
</div>

<div class="modal fade" id="popcalendar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Kalender Akademik Pengisian KRS 2017/2018 - Ganjil</h4>
            </div>
            <form class ='form-horizontal' action="<?= base_url();?>organisasi/ruang/save_ruang" method="post" enctype="multipart/form-data">
                <div class="modal-body">  
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <td rowspan="2" style="text-align:center">NO</td>
                          <td rowspan="2" style="text-align:center">PRODI</td>
                          <td colspan="2" style="text-align:center">PERIODE</td>
                        </tr>
                        <tr>
                          <td style="text-align:center">I</td>
                          <td style="text-align:center">II</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Manajemen, Akuntansi, dan Psikologi</td>
                          <td>14-16 Agustus 2017</td>
                          <td>23-25 Agustus 2017</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Teknik Industri, Teknik Kimia, Teknik Perminyakan, Teknik Lingkungan, dan Ilmu Komunikasi</td>
                          <td>17-19 Agustus 2017</td>
                          <td>25-26 Agustus 2017</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>Magister Manajemen, Magister Ilmu Hukum, Ilmu Hukum, dan Teknik Informatika</td>
                          <td>20-22 Agustus 2017</td>
                          <td>26-27 Agustus 2017</td>
                        </tr>
                        <tr>
                          <td>4</td>
                          <td>Mahasiswa Baru 2017</td>
                          <td colspan="2">25-26 Agustus 2017/mengikuti jadwal PMB</td>
                        </tr>
                      </tbody>
                    </table>           
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="popbayaran" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Kegiatan Akademik 2017/2018 - Ganjil</h4>
            </div>
            <form class ='form-horizontal' action="<?= base_url();?>organisasi/ruang/save_ruang" method="post" enctype="multipart/form-data">
                <div class="modal-body">  
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <td rowspan="2" style="text-align:center">NO</td>
                          <td rowspan="2" style="text-align:center">Kegiatan Akademik</td>
                          <td colspan="2" style="text-align:center">PERIODE</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Pembayaran tahap V (syarat UAS) T.A 2017/2018 - Ganjil dan Pengajuan Form Dispensasi ke Prodi masing-masing (14 Desember 2017)</td>
                          <td>14 November - 20 Desember 2017</td>
                        </tr>
                      </tbody>
                    </table>           
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->