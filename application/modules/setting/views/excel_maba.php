<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=hasil_tes.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
tr {
	vertical-align: middle;
}
</style>

<table>
	<thead>
	    <tr> 
	        <th width="30" rowspan="2">No</th>
	        <th rowspan="2">NOMOR POKOK MAHASISWA (NPM)/NOMOR PENDAFTARAN</th>
	        <th rowspan="2">NAMA LENGKAP SESUAI IJASAH</th>
	        <th rowspan="2">NAMA LENGKAP SESUAI AKTE KELAHIRAN</th>
	        <th rowspan="2">NAMA IBU</th>
	        <th colspan="2">ASAL SEKOLAH</th>
	        <!-- <th rowspan="2">ALAMAT</th> -->
	        <th rowspan="2">AGAMA</th>
	        <th rowspan="2">NOMOR TELPON</th>
	        <th rowspan="2">PEKERJAAN ORANG TUA</th>
	        <th colspan="2">KAMPUS</th>
	        <th colspan="3">KELAS PILIHAN</th>
	        <th colspan="2">TANGGAL PENDAFTARAN</th>
	    </tr>
	    <tr>
	    	<th>NAMA SEKOLAH & JURUSAN</th>
	        <th>TAHUN LULUS</th>
	    	<th>JAKARTA</th>
	        <th>BEKASI</th>
	        <th>PAGI</th>
	        <th>SORE</th>
	        <th>P2K</th>
	        <th>TANGGAL</th>
	        <th>GELOMBANG</th>
	        <th>TANDA TANGAN</th>
	    </tr>
	</thead>
	<tbody>
	    <?php $no=1; foreach($koll as $key) { ?>
	    <tr>
	        <td align="center"><?php echo $no; ?></td>
	        <td align="center"><?php echo $key->npm_baru; ?></td>
	        <td><?php echo $key->nama; ?></td>
	        <td><?php echo $key->nama; ?></td>
	        <td><?php echo $key->nm_ibu; ?></td>
	        <td><?php echo $key->asal_sch_maba.' - '.$key->jur_maba; ?></td>
	        <td align="center"><?php echo $key->lulus_maba; ?></td>
	        <!-- <td><?php echo $key->alamat; ?></td> -->
	        <td><?php echo $key->agama; ?></td>
	        <td align="center"><?php echo $key->tlp; ?></td>
	        <td>
	        	<?php if ($key->workdad == 'PN') {
		        	echo $pkr = 'Pegawai Negeri';
		        	} elseif ($key->workdad == 'TP') {
		        		$pkr = "TNI /POLRI";
		        	} elseif ($key->workdad == 'PS') {
		        		$pkr = "Pegawai Swasta";
		        	} elseif ($key->workdad == 'WU') {
		        		$pkr = "Wirausaha";
		        	} elseif ($key->workdad == 'PE') {
		        		$pkr = "Pensiun";
		        	} elseif ($key->workdad == 'TK') {
		        		$pkr = "Tidak Bekerja";
		        	} elseif ($key->workdad == 'LL') {
		        		$pkr = "Lain-lain";
		        	} echo $pkr; ; 
	        	?>
	        </td>
	        <td align="center"><?php if($key->kampus == 'jkt') { echo 'v'; } else { echo " ";} ?></td>
	        <td align="center"><?php if($key->kampus == 'bks') { echo 'v'; } else { echo " ";} ?></td>
	        <td align="center"><?php if($key->kelas == 'PG') { echo 'v'; } else { echo " ";} ?></td>
	        <td align="center"><?php if($key->kelas == 'SR') { echo 'v'; } else { echo " ";} ?></td>
	        <td align="center"><?php if($key->kelas == 'KY') { echo 'v'; } else { echo " ";} ?></td>
	        <td><?php echo $key->tanggal_regis; ?></td>
	        <td><?php echo $key->gelombang; ?></td>
	        <td></td>
	    </tr>
	    <?php $no++; } ?>
	</tbody>
</table>