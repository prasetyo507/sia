<link href="<?php echo base_url(); ?>assets/js/bootstrap-toggle-master/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/bootstrap-toggle-master/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">
	$(function () {
   		$(".oplos").change(function() {
			if ($(this).attr('checked')) {
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url() ?>setting/turn_krs/pushdata/'+ 1 + '/' + $(this).val(),
					success: function () {
						$('#oplos').attr('checked','checked');
					}
				});
			} else {
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url() ?>setting/turn_krs/pushdata/'+ 0 + '/' + $(this).val(),
					success: function () {
						$('#oplos').removeAttr('checked','checked');
					}
				});
			}
		});
   	});
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-calendar"></i>
  				<h3>Jadwal Pengisisan KRS Tiap Prodi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<!--a data-toggle="modal" href="#myModal" class="btn btn-primary"> New Data </a><br><hr-->
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Prodi</th>
                                <th>Status</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($data->result() as $row) { ?>
	                        <tr>
	                        	<td><?php echo $no ;?></td>
                                <td><?php echo get_jur($row->kd_prodi); ?></td>
	                        	<td>
	                        		<input data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" class="oplos" type="checkbox" value="<?php echo $row->kd_prodi; ?>" <?php if ($row->status == 1) { echo "checked";}?> >
	                        	</td>
	                        </tr>
                            <?php $no++; } ?>   
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>