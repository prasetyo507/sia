<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sinkron_npm extends CI_Controller {

	public function index()
	{
		$data['page'] = "v_sinc_npm";
		$this->load->view('template', $data);
	}

}

/* End of file Sinkron_npm.php */
/* Location: ./application/modules/setting/controllers/Sinkron_npm.php */