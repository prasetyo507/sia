<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Turn_krs extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect(base_url('auth/logout'),'refresh');
		}
	}

	public function index()
	{
		$data['data'] = $this->app_model->getdata('tbl_jadwal_krs','kd_prodi','asc');
		$data['page'] = "v_setup_schedule_krs";
		$this->load->view('template', $data);
	}

	function pushdata($e,$f)
	{
		$data = array('status' => $e);
		$this->db->where('kd_prodi', $f);
		$this->db->update('tbl_jadwal_krs', $data);
	}

}

/* End of file Turn_krs.php */
/* Location: ./application/modules/setting/controllers/Turn_krs.php */