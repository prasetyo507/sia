<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Poin extends CI_Controller {

	/**
	 * Define user ID
	 * string $user
	 */
	private $user;
	
	function __construct()
	{
		parent::__construct();
		# redirect to auth if user have npo session
		(!$this->session->userdata('sess_login')) ? redirect('auth/logout','refresh') : NULL;

		# set up default time zone
		date_default_timezone_set('Asia/Jakarta');
		
		# set up user ID
		$user = $this->session->userdata('sess_login');
		$this->user = $user['userid'];

		# limit access. tis menu just for BAA and Admin
		if ($user['id_user_group'] != 1 && $user['id_user_group'] != 10) {
			echo "<script>alert('Akses tidak diizinkan!');history.go(-1);</script>";
			exit();	
			return;
		}
	}

	public function index()
	{
		$data['poin'] = $this->db->where('deleted_at')->get('tbl_index_nilai')->result();
		$data['page'] = 'v_poin';
		$this->load->view('template', $data);
	}

	/**
	 * Store new data of Mutu to database
	 * 
	 * @return void
	 */
	public function saveMutu()
	{
		# convert array from user input to a variable. You can find PupilateForm() in helper/mainhelper.php
		extract(PopulateForm());

		# $bobot, $bawah, $atas are must be a number
		if (!is_numeric($bobot) || !is_numeric($bawah) || !is_numeric($atas)) {
			echo "<script>alert('Mutu, Nilai bawah, dan Nilai atas harus berupa angka!');history.go(-1);</script>";
			return;
		}

		# check whether the request have a comma inside
		if (strpos($bobot, ',') || strpos($bawah, ',') || strpos($atas, ',')) {
			echo "<script>alert('Dilarang menggunakan koma (,) pada Mutu, Nilai bawah, dan Nilai atas!');history.go(-1);</script>";
			return;
		}

		# $bobot dont allow smaller than 0 and higher than 100
		if (str_replace(' ', '', $bobot) < 0.00 || str_replace(' ', '', $bobot) > 100) {
			echo "<script>alert('Bobot tidak boleh kurang dari 0 dan melebihi 100!');history.go(-1);</script>";
			return;
		}

		# $bawah dont allow smaller than 0 and higher than 100
		if (str_replace(' ', '', $bawah) < 0.00 || str_replace(' ', '', $bawah) > 100) {
			echo "<script>alert('Nilai bawah tidak boleh kurang dari 0 dan melebihi 100!');history.go(-1);</script>";
			return;
		}

		# $atas dont allow smaller than 0 and higher than 100
		if (str_replace(' ', '', $atas) < 0.00 || str_replace(' ', '', $atas) > 100) {
			echo "<script>alert('Nilai atas tidak boleh kurang dari 0 dan melebihi 100!');history.go(-1);</script>";
			return;
		}

		$request = [
			'nilai_huruf' => strtoupper($mutu),
			'nilai_mutu' => str_replace(' ', '', $bobot),
			'nilai_bawah' => str_replace(' ', '', $bawah),
			'nilai_atas' => str_replace(' ', '', $atas),
			'deskripsi' => $deskripsi,
			'description' => $description,
			'created_at' => date('Y-m-d H:i:s'),
			'created_by' => $this->user
		];

		$this->db->insert('tbl_index_nilai', $request);
		echo "<script>alert('Berhasil menyimpan data mutu!');history.go(-1);</script>";
		return;
	}

	/**
	 * Load data of mutu an show those on modal view
	 * @param int $id_mutu
	 * @return view modal
	 */
	public function loadEdit($id_mutu)
	{
		$data['mutu'] = $this->db->where('id', $id_mutu)->get('tbl_index_nilai')->row();
		$this->load->view('modal_edit_mutu', $data);
	}
	
	/**
	 * Update mutu
	 * @param int $id_mutu
	 * @return void
	 */
	public function updateMutu($id_mutu)
	{
		# convert array from user input to a variable. You can find PupilateForm() in helper/mainhelper.php
		extract(PopulateForm());

		# $bobot, $bawah, $atas are must be a number
		if (!is_numeric($bobot) || !is_numeric($bawah) || !is_numeric($atas)) {
			echo "<script>alert('Mutu, Nilai bawah, dan Nilai atas harus berupa angka!');history.go(-1);</script>";
			return;
		}

		# check whether the request have a comma inside
		if (strpos($bobot, ',') || strpos($bawah, ',') || strpos($atas, ',')) {
			echo "<script>alert('Dilarang menggunakan koma (,) pada Mutu, Nilai bawah, dan Nilai atas!');history.go(-1);</script>";
			return;
		}

		# $bobot dont allow smaller than 0 and higher than 100
		if (str_replace(' ', '', $bobot) < 0.00 || str_replace(' ', '', $bobot) > 100) {
			echo "<script>alert('Bobot tidak boleh kurang dari 0 dan melebihi 100!');history.go(-1);</script>";
			return;
		}

		# $bawah dont allow smaller than 0 and higher than 100
		if (str_replace(' ', '', $bawah) < 0.00 || str_replace(' ', '', $bawah) > 100) {
			echo "<script>alert('Nilai bawah tidak boleh kurang dari 0 dan melebihi 100!');history.go(-1);</script>";
			return;
		}

		# $atas dont allow smaller than 0 and higher than 100
		if (str_replace(' ', '', $atas) < 0.00 || str_replace(' ', '', $atas) > 100) {
			echo "<script>alert('Nilai atas tidak boleh kurang dari 0 dan melebihi 100!');history.go(-1);</script>";
			return;
		}

		$request = [
			'nilai_huruf' => strtoupper($mutu),
			'nilai_mutu' => str_replace(' ', '', $bobot),
			'nilai_bawah' => str_replace(' ', '', $bawah),
			'nilai_atas' => str_replace(' ', '', $atas),
			'deskripsi' => $deskripsi,
			'description' => $description,
			'updated_at' => date('Y-m-d H:i:s'),
			'updated_by' => $this->user
		];

		$this->db->where('id', $id_mutu);
		$this->db->update('tbl_index_nilai', $request);
		echo "<script>alert('Berhasil memperbarui data mutu!');history.go(-1);</script>";
		return;
	}

	/**
	 * Remove data Mutu by update its deleted_at (remove is just term, actually just update its deleted_at column)
	 * @param int $id_mutu
	 * @return void
	 */
	public function removeMutu($id_mutu)
	{
		$updatedColumn = [
			'deleted_at' => date('Y-m-d H:i:s'),
			'deleted_by' => $this->user
		];

		$this->db->where('id', $id_mutu);
		$this->db->update('tbl_index_nilai', $updatedColumn);
		echo "<script>alert('Berhasil menghapus data mutu!');history.go(-1);</script>";
		return;
	}
	
}

/* End of file Poin.php */
/* Location: .//tmp/fz3temp-1/Poin.php */