<div class="row">

  <div class="span12">                

      <div class="widget ">

        <div class="widget-header">

          <i class="icon-user"></i>

          <h3>Data Status Mahasiswa <?= get_thnajar(getactyear()); ?></h3>

      </div> <!-- /widget-header -->

      

      <div class="widget-content">
        <div class="tabbable">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#banyak" data-toggle="tab">Review Cuti</a></li>
              <li><a href="#satuan" data-toggle="tab">Review Non Aktif</a></li>
              <li><a href="#keluar" data-toggle="tab">Review Keluar</a></li>
              <li><a href="#lulus" data-toggle="tab">Review Lulus</a></li>
            </ul>           
            <br>         
              <div class="tab-content">
                <div class="tab-pane active" id="banyak">
                  <div class="span11">

                    <form method="post" class="form-horizontal" action="#">

                      <fieldset>
                        <a href="<?php echo base_url(); ?>sync_feed/status_mhs/sync_sts" target="blank" class="btn btn-primary"><i class="icon icon-refresh"></i> Sinkronisasi</a>
                        <!-- <a href="#" class="btn btn-primary"><i class="icon icon-refresh"></i> Sinkronisasi</a> -->
                        <hr>
                        <table id="tabel_kurikulum" class="table table-bordered table-striped">
                          <thead>
                            <tr> 
                              <th width="40">No</th>
                              <th>NPM</th>
                              <th>NAMA</th>
                              <th>IPK</th>
                              <th>SKS Total</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $no = 1; foreach ($cuti as $row) { ?>
                            <tr>
                              <td><?php echo $no; ?></td>
                              <td><?php echo $row->npm; ?></td>
                              <td><?php echo $row->NMMHSMSMHS; ?></td>
                              <?php $getipk = $this->monitoring_model->dapetip($row->npm,yearBefore())->row(); ?>
                              <td><?php if (count($getipk) > 0) { echo $getipk->NLIPKTRAKM; } else { echo '-'; } ?></td>
                              <td><?php if (count($getipk) > 0) { echo $getipk->SKSTTTRAKM; } else { echo '-'; } ?></td>
                            </tr>
                            <?php $no++; } ?>
                          </tbody>
                        </table>

                      </fieldset>
                  </form>
                    </div>
                </div>
                <div class="tab-pane" id="satuan">
                  <div class="span11">
                    <form class="form-horizontal" action="#">
                      <fieldset>
                      <a href="<?php echo base_url(); ?>sync_feed/status_mhs/sync_sts_non" target="_blank" class="btn btn-primary"><i class="icon icon-refresh"></i> Sinkronisasi</a>
                      <!-- <a href="#" class="btn btn-primary"><i class="icon icon-refresh"></i> Sinkronisasi</a> -->
                      <hr>
                      <table id="tabel_kurikulum" class="table table-bordered table-striped">
                          <thead>
                            <tr> 
                              <th width="40">No</th>
                              <th>NPM</th>
                              <th>NAMA</th>
                              <th>IPK</th>
                              <th>SKS Total</th>
                              <th width="30">Aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $no = 1; foreach ($non as $raw) { ?>
                            <tr>
                              <td><?php echo $no; ?></td>
                              <td><?php echo $raw->NIMHSMSMHS; ?></td>
                              <td><?php echo $raw->NMMHSMSMHS; ?></td>

                              <?php 
                                $getipk = $this->monitoring_model->dapetip($raw->NIMHSMSMHS, yearBefore())->row(); 
                              ?>

                              <td>
                                <?php if (count($getipk) > 0) { echo $getipk->NLIPKTRAKM; } else { echo '-'; } ?>
                              </td>
                              <td>
                                <?php if (count($getipk) > 0) { echo $getipk->SKSTTTRAKM; } else { echo '-'; } ?>
                              </td>
                              <td>
                                <a href="<?php echo base_url(); ?>sync_feed/status_mhs/update/<?php echo $raw->NIMHSMSMHS; ?>" class="btn btn-danger"><i class="icon icon-remove"></i></a>
                              </td>
                            </tr>
                            <?php $no++; } ?>
                          </tbody>
                        </table>
                      </fieldset>
                    </form>
                  </div>
                </div>
                 <div class="tab-pane" id="keluar">
                  <div class="span11">
                    <fieldset>
                       <!-- <a href="<?php echo base_url(); ?>sync_feed/status_mhs/sync_sts_out" target="blank" class="btn btn-primary"><i class="icon icon-refresh"></i> Sinkronisasi</a> -->
                        <hr>
                        <table id="tabel_kurikulum" class="table table-bordered table-striped">
                          <thead>
                            <tr> 
                              <th width="40">No</th>
                              <th>NPM</th>
                              <th>NAMA</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $no = 1; foreach ($keluar as $row) { ?>
                            <tr>
                              <td><?php echo $no; ?></td>
                              <td><?php echo $row->NIMHSMSMHS; ?></td>
                              <td><?php echo $row->NMMHSMSMHS; ?></td>
                            </tr>
                            <?php $no++; } ?>
                          </tbody>
                        </table>

                      </fieldset>
                  </div>
                  </div>
                <div class="tab-pane" id="lulus">
                  <div class="span11">
                      <fieldset>
                        <hr>
                        <table id="tabel_kurikulum" class="table table-bordered table-striped">
                          <thead>
                            <tr> 
                              <th width="40">No</th>
                              <th>NPM</th>
                              <th>NAMA</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $no = 1; foreach ($lulus as $row) { ?>
                            <tr>
                              <td><?php echo $no; ?></td>
                              <td><?php echo $row->NIMHSMSMHS; ?></td>
                              <td><?php echo $row->NMMHSMSMHS; ?></td>
                            </tr>
                            <?php $no++; } ?>
                          </tbody>
                        </table>

                      </fieldset>
                    </div>
                </div>
              </div>

        </div>
      </div>
        

      </div>

    </div>

  </div>

</div>
<script type="text/javascript">
jQuery(document).ready(function($) {

    $('input[name^=npm]').autocomplete({

        source: '<?php echo base_url('form/formcuti/load_mhs_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.npm.value = ui.item.value;
        }

    });

});</script>

