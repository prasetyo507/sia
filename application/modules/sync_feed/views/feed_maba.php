<script>
function edit(id){
$('#myModal1').load('<?php echo base_url();?>datas/mahasiswapmb/load_lengkap/'+id);
}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-list"></i>
  				<h3>Daftar Calon Mahasiswa S1</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
				<?php 
				// $logged = $this->session->userdata('sess_login');
				// $pecah = explode(',', $logged['id_user_group']);
				// $jmlh = count($pecah);
				// for ($i=0; $i < $jmlh; $i++) { 
				// 	$grup[] = $pecah[$i];
				// }
				// if ( (in_array(13, $grup))) { ?>
					<!-- <a data-toggle="modal" href="#myModal" class="btn btn-success"><i class="icon-excel"></i> Print Harian</a> 
					<a data-toggle="modal" href="#yuModal" class="btn btn-primary"><i class="icon-excel"></i> Print All</a>
					<a href="<?php echo base_url(); ?>datas/mahasiswapmb/printstatuss1" class="btn btn-warning"><i class="icon-excel"></i> Print Status Kelengkapan</a> -->
				<?php //}	?>
					<a href="<?php echo base_url(); ?>sync_feed/maba/sync_maba_feed" class="btn btn-success">Sinkronisasi</a><br>
                    <!-- <a href="<?php //echo base_url(); ?>datas/mahasiswapmb/printexcels1" class="btn btn-primary"><i class="icon-excel"></i> Print All</a> --><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr>
	                        	<th width="25">No</th>
	                        	<th>ID Registrasi</th>
                                <th>Nama</th>
                                <th>NPM</th>
                                <th width="140">Nomor Handphone</th>
                                <th width="120">Program Studi</th>
                                <th>Jenis</th>
                                <th>Kelamin</th>
                                <th>Nama Ibu</th>
                                <th>Status</th>
                                <th>Tempat lahir</th>
                                <th>Tanggal Lahir</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no=1; foreach ($look as $row) { ?>
	                        <tr>
                                <td><?php echo $no;?></td>
	                        	<td><?php echo $row->nomor_registrasi;?></td>
	                        	<td><?php echo $row->nama;?></td>
	                        	<td><?php echo $row->npm_baru;?></td>
	                        	<td><?php echo $row->tlp; ?></td>
	                        	<td><?php echo get_jur($row->prodi);?></td>
	                        	<?php if ($row->jenis_pmb == 'MB') {
	                        		$kls = 'Mahasiswa Baru';
	                        	} elseif ($row->jenis_pmb == 'KV') {
	                        		$kls = 'Konversi';
	                        	} elseif ($row->jenis_pmb == 'RM') {
	                        		$kls = 'Readmisi';
	                        	}
	                        	 ?>
	                        	<td><?php echo $kls; ?></td>
	                        	<?php if ($row->kelamin == 'P') {
	                        		$jklm = 'Perempuan';
	                        	} elseif ($row->kelamin == 'L') {
	                        		$jklm = 'Laki-Laki';
	                        	}
	                        	 ?>
	                        	<td><?php echo $jklm; ?></td>
	                        	<td><?php echo $row->nm_ibu;?></td>
	                        	<?php if ($row->status == 1) {
	                        		$st = 'Lulus Tes';
	                        	} elseif ($row->status > 1) {
	                        		$st = 'Daftar Ulang';
	                        	} else {
	                        		$st = 'Registrasi';
	                        	}
	                        	 ?>
	                        	<td><?php echo $st; ?></td>
	                        	<td><?php echo $row->tempat_lahir; ?></td>
	                        	<td><?php echo $row->tgl_lahir; ?></td>
	                        </tr>
                            <?php $no++; } ?>
							
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui.js"></script>
<script>
   $(document).ready(function() {
     $( "#tgl" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: "yy-mm-dd"
      });
   });
      
</script>

