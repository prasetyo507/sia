<script type="text/javascript">
function edit(idk){
        $('#edit_dropout').load('<?php echo base_url();?>data/Dropout/edit_dropout/'+idk);
    }
</script>


<script type="text/javascript">

$(document).ready(function($) {

    $('input[name^=mhs]').autocomplete({

        source: '<?php echo base_url('data/Dropout/load_mhs_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.mhs.value = ui.item.value;

            this.form.npm.value = ui.item.npm;
        }

    });

    $('#tgl_skep').datepicker({
			dateFormat: "yy-mm-dd",

			yearRange: "2016:2020",

			changeMonth: true,

			changeYear: true

			});

   
});

</script>


<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Sinkronisasi Data Lulusan</h3>
            </div> <!-- /widget-header -->
            <?php $sess = $this->session->userdata('sess_login'); ?>
            <div class="widget-content">
                
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
	                        <tr>
	                        	<th>No. SKEP</th>
	                            <th>NPM</th>
	                            <th>Nama Mahasiswa</th>
	                            <th>Alasan</th>
                                <?php if ($sess['id_user_group'] == 10) { ?>
                                    <th class="td-actions">Aksi</th>
                                <?php } ?>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php  foreach($rows as $row){

								if ($row->alasan = 1) {
									$res = 'Mengundurkan Diri';
								} elseif ($row->alasan == 2){
									$res = 'Dikeluarkan';
								} elseif ($row->alasan == 3) {
                                    $res = 'Wafat';
                                }

							?>
	                        <tr>
	                        	<td><?php echo "No. SKEP/".$row->skep."/TEST" ?></td>
	                        	<td><?php echo $row->NIMHSMSMHS ?></td>
	                            <td><?php echo $row->NMMHSMSMHS ?></td>
	                            <td><?php echo $res;?></td>
                                <?php if ($sess['id_user_group'] == 10) { ?>
                                <td class="td-actions">
                                    <!-- <a href="#editModal" data-toggle="modal" onclick="edit(<?php echo $row->NIMHSMSMHS;?>)"><button type="button" class="btn btn-primary btn-small edit"><i class="btn-icon-only icon-pencil"></i></button></a> -->    
                                    <a class="btn btn-primary btn-small" href="<?= base_url();?>sync_feed/Dropout/sync_do/<?= $row->NIMHSMSMHS;?>"><i class="icon icon-refresh"></i></a>
                                </td>
                                <?php } ?>
	                        </tr>
							<?php } ?>
	                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM DATA</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>sync_feed/dropout/saveout" method="post">
                <div class="modal-body">
                    <div class="control-group">                                         
                        <label class="control-label" for="jam">NPM <p style="color:red">*</p></label>
                        <div class="controls">
                            <input type="text" class="form-control" placeholder="Masukan NPM" id="jam_masuk" name="npm" required>
                        </div>
                    </div>
                    <div class="control-group">                                         
                        <label class="control-label" for="jam">Alasan</label>
                        <div class="controls">
                            <select class="form-group" name="alasan">
                                <option value="1">Mengundurkan diri</option>
                                <option value="2">Dikeluarkan</option>
                                <option value="3">Wafat</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">                                         
                        <label class="control-label" for="jam">SKEP <p style="color:red">*</p></label>
                        <div class="controls">
                            <input type="text" class="form-control" placeholder="Masukan No. SKEP" id="" name="skep" required>
                        </div>
                    </div>
                    <div class="control-group">                                         
                        <label class="control-label" for="jam">Tanggal SKEP <p style="color:red">*</p></label>
                        <div class="controls">
                            <input type="text" class="form-control" placeholder="Masukan Tanggal SKEP" id="tgl" name="tgl" required>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->