<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Semester</th>
	                        	<th>Total SKS</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($krs as $row) { $a = $this->app_model->getdetail('tbl_verifikasi_krs','kd_krs',$row->kd_krs,'npm_mahasiswa','asc')->row()->status_verifikasi; 
                            	  if ($a == NULL or $a !=0 ) { ?>
	                        <tr>
	                        	<td><?php echo $no;?></td>
	                        	<?php $sms = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$row->npm_mahasiswa,'NIMHSMSMHS','asc')->row(); ?>
	                        	<td> <?php $smtr = $this->db->query("select distinct semester_krs from tbl_krs where kd_krs = '".$row->kd_krs."'")->row()->semester_krs; echo $smtr; ?> </td>
	                        	<!-- <td><?php //echo $this->app_model->get_semester($sms->SMAWLMSMHS);?></td> -->
	                        	<?php
	                        	$q = $this->db->query("select distinct kd_matakuliah from tbl_krs where kd_krs = '".$row->kd_krs."'")->result();
	                        	$sumsks = 0;
	                        	foreach ($q as $value) {
	                        		$sksmk = $this->db->query("select distinct sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$value->kd_matakuliah."'")->row()->sks_matakuliah; 
	                        		$sumsks = $sumsks + $sksmk;
	                        	}
	                        	?>
                                <td><?php echo $sumsks; ?></td>
	                        	<td class="td-actions">
	                        	<?php if (is_null($slugcoy->slug_url)) {
	                        		$lanjut = $row->kd_krs;
	                        	} else {
	                        		$lanjut = $slugcoy->kd_krs;
	                        	}
	                        	 ?>
	                        	 <?php 
	                        	 	$logged = $this->session->userdata('sess_login');
	                        	 	$pecah = explode(',', $logged['id_user_group']);
									$jmlh = count($pecah);
									for ($i=0; $i < $jmlh; $i++) { 
										$grup[] = $pecah[$i];
									}
									if ((in_array(8, $grup)) or (in_array(10, $grup)) or (in_array(9, $grup)) or (in_array(1, $grup))) {
	                        	  ?>
									<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>akademik/viewkrs/viewview/<?php echo $row->kd_krs; ?>"><i class="btn-icon-only icon-ok"> </i></a>
								   <?php } else { ?>
									<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>akademik/viewkrs/view/<?php echo $lanjut; ?>"><i class="btn-icon-only icon-ok"> </i></a>
								   <?php } ?>
								</td>
	                        </tr>
	                        <?php $no++; }} ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>