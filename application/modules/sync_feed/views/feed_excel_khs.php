<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_khs.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table>
	<thead>
        <tr> 
        	<th>No</th>
        	<th>NIM</th>
        	<th>Nama MHS</th>
        	<th>Tahun Masuk</th>
        	<th>SKS 20152</th>
        	<th>IPS 20152</th>
        	<th>SKS Total</th>
        	<th>IPK 20151</th>
        	<th>IPK 20152</th>
        	
        </tr>
    </thead>
    <tbody>
    	<?php $no=1; foreach ($getData as $value) { ?>
		
        <tr>
        	<td><?php echo number_format($no); ?></td>
        	<td><?php echo $value->NIMHSMSMHS; ?></td>
        	<td><?php echo $value->NMMHSMSMHS; ?></td>
            <td><?php echo $value->TAHUNMSMHS; ?></td>

            <?php
            $logged = $this->session->userdata('sess_login');
            
            $hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah`
            								FROM tbl_transaksi_nilai a
            								JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
            								WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$logged['userid'].'"
            								AND NIMHSTRLNM = "'.$value->NIMHSMSMHS.'" and a.THSMSTRLNM = "20152"')->result();

            $jml_sks = $this->db->query('SELECT SUM(mk.`sks_matakuliah`) AS jml_sks FROM tbl_krs_feeder krs
											JOIN tbl_matakuliah mk ON krs.`kd_matakuliah` = mk.`kd_matakuliah`
											WHERE krs.`kd_krs` = "'.$value->kd_krs.'" AND mk.`kd_prodi` = "'.$logged['userid'].'"')->row()->jml_sks;

            $st=0;
            $ht=0;
            foreach ($hitung_ips as $iso) {
                $h = 0;

                $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
                $ht=  $ht + $h;

                $st=$st+$iso->sks_matakuliah;
            }

            $ips_nr = $ht/$st;
            
            ?>
            <td><?php echo $jml_sks; ?></td>
            <td><?php  echo number_format($ips_nr, 2); ?></td>

            <!-- get ipk total dan sks total dari feeder -->
            <?php
            $sks 	= $this->db->query("SELECT * from tbl_khs where npm_mahasiswa = '".$value->NIMHSMSMHS."' and tahunajaran = '20152'")->row();
            $sks2 	= $this->db->query("SELECT * from tbl_khs where npm_mahasiswa = '".$value->NIMHSMSMHS."' and tahunajaran = '20151'")->row();
            if (count($sks) > 0) {
            	$ipk = number_format($sks->ipk,2);
		        $sks_total = $sks->total_sks;
            } else {
            	$sks 		= $this->db->query("SELECT * from tbl_khs where npm_mahasiswa = '".$value->NIMHSMSMHS."' and tahunajaran = '20151'")->row();
            	$ipss 		= number_format($ips_nr, 2);
		        $skss 		= $st;
		        $awal 		= number_format(($sks->ipk*$sks->total_sks),2);
		        $nambah 	= number_format(($ipss*$skss),2);
		        $skstot 	= $sks->total_sks+$skss;
		        $ipk 		= number_format(($awal+$nambah)/($skss+$sks->total_sks),2);
		        $sks_total 	= $skstot;
            }
            
            ?>
            <td><?php echo $sks_total; ?></td>
            <td><?php echo number_format($sks2->ipk,2);?></td>
			<td><?php echo number_format($ipk, 2); ?></td>
			
            
        </tr>
        <?php $no++;} ?>
    </tbody>
</table>