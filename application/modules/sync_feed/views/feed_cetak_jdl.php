<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_maba.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 1px solid black;
}



th {
    background-color: blue;
    color: black;
}
</style>

<!-- <h4>JADWAL MATAKULIAH <?php echo $prodi; ?> TAHUNAJARAN </h4> -->

<table border="3">
    <thead>
        <tr> 

            <th>No</th>

            <th>Semester</th>

            <th>Kelas</th>

            <th>Kode MK</th>

            <th>Matakuliah</th>

            <th>SKS</th>

            <th>Peserta</th>

            <th>Dosen</th>

            <th>NID</th>

            <th>NIDN</th>

            <th>NUPN</th>

        </tr>
    </thead>
    <tbody>
        
        <?php $no=1; foreach ($rows->result() as $key) { 

            if ($no == 1) {
                $kosong = intval($key->semester_kd_matakuliah);
            }

            if ($kosong != $key->semester_kd_matakuliah){
                $kosong = intval($kosong+1);
                if ($kosong == $key->semester_kd_matakuliah){
                    echo "<tr><td style='background:#000000' colspan='11'></td></tr>";
                }else{
                    $kosong = intval($kosong+1);
                    echo "<tr><td style='background:#000000' colspan='11'></td></tr>";
                }
                ?>

                
            <?php } 
            

            $cek_kelas = $this->db->query('SELECT COUNT(z.`npm_mahasiswa`) as jml FROM tbl_krs z WHERE z.`kd_jadwal` = "'.$key->kd_jadwal.'"')->row();
            
            if (($key->nidn == NULL or $key->nidn == "") and ($key->nupn != NULL or $key->nupn != "")) {
                $warna ='';
            }elseif (($key->nidn != NULL or $key->nidn != "") and ($key->nupn == NULL or $key->nupn == "")){
                $warna ='';
            } elseif (($key->nidn == NULL or $key->nidn == "") and ($key->nupn == NULL or $key->nupn == "")) {
                $warna ='style="background:#FF6347"';
            }

        ?>
        <tr>
            <td <?php echo $warna; ?>><?php echo $no;; ?></td>

            <td <?php echo $warna; ?>><?php echo $key->semester_kd_matakuliah; ?></td> 

            <td <?php echo $warna; ?>><?php echo $key->kelas; ?></td>

            <td <?php echo $warna; ?>><?php echo $key->kd_matakuliah; ?></td>

            <td <?php echo $warna; ?>><?php echo $key->nama_matakuliah; ?></td>

            <td <?php echo $warna; ?>><?php echo $key->sks_matakuliah; ?></td>

            <?php  $jml_peserta = $this->db->query('select count(npm_mahasiswa) as jml from tbl_krs where kd_jadwal = "'.$key->kd_jadwal.'"')->row()->jml; ?>

            <td <?php echo $warna; ?>><?php echo $jml_peserta; ?></td>

            <td <?php echo $warna; ?>><?php echo $key->nama ?></td>

            <td <?php echo $warna; ?>><?php echo $key->nid; ?></td>

            <td <?php echo $warna; ?>><?php echo $key->nidn; ?></td>  

            <td <?php echo $warna; ?>><?php echo $key->nupn; ?></td>

        </tr>
        
        
           


            
        <?php   
        $no++; } ?>
         
    </tbody>
</table>