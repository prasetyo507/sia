<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var table = $('.table');
	
	var oTable = table.dataTable({
	});
});
</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kurikulum</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>sync_feed/kurikulum" class="btn btn-warning"> << Kembali </a>
					<a href="#help" data-toggle="modal" title="Bantuan">
						<i class="icon-question-sign icon-2x pull-right" style="color:#aaa;"></i>
					</a>
					<!-- <a data-toggle="modal" href="#edit_modal" class="btn btn-primary"> New Data </a> -->
					<center><h3>Kurikulum <?php echo $kurikulum; ?> </h3></center><br>
					<?php $tsksw = 0; $tsksp = 0; for($i=1; $i<=8; $i++){ ?>
					<h3>Semester <?php echo $i; ?></h3>
					<!-- <a href="" class="btn btn-primary"><i class="icon-refresh"></i> Sinkronisasi</a><br> -->
					<hr>

					<table class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Kode Mata Kuliah</th>
	                        	<th>Mata Kuliah</th>
	                        	<th>SKS</th>
                                <th>Semester</th>
                                <th>Prasyarat</th>
                                <th>Wajib</th>
	                            <th width="80">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach (${'data_table'.$i} as $row) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->kd_matakuliah; ?></td>
                                <td><?php echo $row->nama_matakuliah; ?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
                                <td><?php echo $row->semester_kd_matakuliah; ?></td>
                                <?php if ($row->prasyarat_matakuliah == '[]') {
                                	$pera = '';
                                } else {
                                	$pera = $row->prasyarat_matakuliah;
                                }
                                 ?>
                                <td><?php echo $pera; ?></td>
                                <?php if ($row->status_wajib == 1) { $tsksw = $tsksw+$row->sks_matakuliah; ?>
                                    <td><i class="icon-ok"></i></td>
                                <?php } else { $tsksp = $tsksp+$row->sks_matakuliah; ?>
                                    <td><u><i><b>PILIHAN</b></i></u></td>
                                <?php } ?>
	                        	<td class="td-actions">
									<!-- <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php //echo base_url().'perkuliahan/kurikulum/delte_matakuliah_kurikulum/'.$row->id_kurmat.'/'.$row->kd_kurikulum; ?>"><i class="btn-icon-only icon-remove"> </i></a> -->
									<a target="blank" href="<?php echo base_url(); ?>sync_feed/kurikulum/sync_mk/<?php echo $row->kd_matakuliah; ?>/<?php echo $row->kd_kurikulum; ?>" class="btn btn-primary btn-small"><i class="btn-icon-only icon-refresh"></i></a>
									<a target="blank" href="<?php echo base_url(); ?>sync_feed/kurikulum/sync_mk_kurikulum/<?php echo $row->kd_matakuliah; ?>/<?php echo $row->kd_kurikulum; ?>" class="btn btn-success btn-small"><i class="btn-icon-only icon-upload"></i></a>
								</td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
					<?php } ?>
					<u><center><h3>Total SKS Wajib : <?php echo $tsksw; ?>. Total SKS Pilihan : <?php echo $tsksp; ?></h3></center></u>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">TAMBAH MATAKULIAH</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/kurikulum/save_matakuliah" method="post">
                <div class="modal-body" style="margin-left: -30px;">  
					<input type="hidden" value="<?php echo $this->uri->segment(4); ?>" name="kd_kurikulum" class="span4" />
                    <div class="control-group" id="">
                        <label class="control-label">Matakuliah</label>
                        <div class="controls">
                            <select class="form-control span4" name="matakuliah" id="matakuliah" required>
                                    <option value="">--Pilih matakuliah--</option>
                                    <?php foreach ($data_matakuliah as $row) { ?>
                                    <option value="<?php echo $row->kd_matakuliah; ?>"><?php echo $row->kd_matakuliah; ?> - <?php echo $row->nama_matakuliah;?></option>
                                    <?php } ?>
                                  </select>
                        </div>
                    </div>
                    <div class="control-group">                                         
                        <label class="control-label" for="firstname">Semester</label>
                        <div class="controls">
                            <input type="number" min="1" max="8" id="semester_matakuliah" name="semester_matakuliah" required/>
                        </div> <!-- /controls -->               
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="help" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Bantuan</h4>
            </div>
			<div class="modal-body"> 
				<table width="100%">
					<tr>
						<td>
							<button class="btn btn-primary btn-small">
								<i class="btn-icon-only icon-refresh"></i>
							</button>
						</td>
						<td>:</td>
						<td>
							Sinkronisasi matakuliah ke feeder (Tahap Pertama)
						</td>
					</tr>
					<tr>
						<td>
							<button class="btn btn-success btn-small">
								<i class="btn-icon-only icon-upload"></i>
							</button>
						</td>
						<td>:</td>
						<td>
							Sinkronisasi distribusi matakuliah ke kurikulum terpilih (Tahap Kedua)
						</td>
					</tr>
				</table>
			</div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
