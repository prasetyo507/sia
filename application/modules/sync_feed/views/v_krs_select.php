<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data KHS Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
				<form method="post" class="form-horizontal" action="<?php echo base_url(); ?>sync_feed/krs/viewkrsprodi">
          <fieldset>
            <div class="control-group">
                <label class="control-label">Angkatan</label>
                <div class="controls">
                  <select class="form-control span6" name="tahun">
                    <option disabled selected>--Pilih Angkatan--</option>
                    <?php for ($i=2011; $i <= date('Y'); $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div> 
              <div class="control-group">
                <label class="control-label">Tahun Ajaran</label>
                <div class="controls">
                  <select class="form-control span6" name="ta">
                    <option disabled selected>--Pilih Tahun Ajaran--</option>
                    <?php foreach ($tahunakademik as $key) { ?>
                      <option value="<?= $key->kode ?>"><?= $key->tahun_akademik ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>              
            <br />
              
            <div class="form-actions">
                <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
            </div> <!-- /form-actions -->
        </fieldset>
        </form>
					
				</div>
			</div>
		</div>
	</div>
</div>
