<?php $sess = $this->session->userdata('sess_login'); ?>
<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-list"></i>

                <h3>Data Mahasiswa Konversi - <?php echo date('Y'); ?></h3>

            </div> <!-- /widget-header -->

            

            <div class="widget-content">
                <div class="span11">
                  <fieldset>
                    <table id="example1" class="table table-striped">
                      <thead>
                        <tr> 
                          <th width="40">No</th>
                          <th>NPM</th>
                          <th>NAMA</th>
                          <th>SKS Konversi</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; foreach ($load as $row) { ?>
                        <tr>
                          <td><?php echo $no; ?></td>
                          <td><?php echo $row->NIMHSMSMHS; ?></td>
                          <td><?php echo $row->NMMHSMSMHS; ?></td>
                          <?php $sks = $this->app_model->sum_sks_konversi($row->NIMHSMSMHS,$sess['userid'],$row->SMAWLMSMHS)->sks; ?>
                          <td><?php echo $sks; ?></td>
                          <td><a href="<?php echo base_url('sync_feed/konversi/save_session_mhs/'.$row->NIMHSMSMHS) ?>" title="lihat detail" class="btn btn-info"><i class="icon icon-eye-open"></i></a></td>
                        </tr>
                        <?php $no++; } ?>
                      </tbody>
                    </table>
                  </fieldset>
                </div>

              </div>
            </div>
          </div>  
        </div>  