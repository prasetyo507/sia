<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        ini_set('memory_limit', '1024M');
		// error_reporting(0);
	    if ($this->session->userdata('sess_login') == TRUE) {
            $cekakses = $this->role_model->cekakses(123)->result();
            if ($cekakses != TRUE) {
                echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
            }
        } else {
            redirect('auth','refresh');
        }
	}

	public function index()
	{
		$logged = $this->session->userdata('sess_login');
		$kd_sms = $this->db->query('SELECT * FROM tbl_jurusan_prodi 
                                    JOIN tbl_fakultas ON tbl_fakultas.`kd_fakultas` = tbl_jurusan_prodi.`kd_fakultas` 
                                    WHERE tbl_jurusan_prodi.`kd_prodi`="'.$logged['userid'].'"')->row();

		$this->session->set_userdata('id_fakultas_prasyarat', $kd_sms->kd_fakultas);
		$this->session->set_userdata('id_jurusan_prasyarat', $kd_sms->kd_prodi);

		$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'feed_kelas';
		$this->load->view('template', $data);
	}

	function get_jurusan($id)
    {
		$jrs = explode('-',$id);

        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();

		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Program Studi--</option>";

        foreach ($jurusan as $row) {

            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";

        }

        $out .= "</select>";

        echo $out;

	}

	function save_session_prodi(){

        
        $tahunajaran = $this->input->post('tahunajaran');

        $this->session->set_userdata('tahunajaran', $tahunajaran);        

        redirect(base_url('sync_feed/kelas/view_matakuliah'));

    }

    function view_matakuliah(){

        $data['tabs'] = $this->db->query('SELECT DISTINCT a.`semester_kd_matakuliah` FROM tbl_kurikulum_matkul_new a
                                        JOIN tbl_kurikulum_new b ON a.kd_kurikulum = b.kd_kurikulum
                                        WHERE a.`semester_kd_matakuliah` IS NOT NULL 
                                        AND b.status = "1"
                                        AND a.`semester_kd_matakuliah` != ""')->result();

		$data['page'] ='v_kelas_list';

		$this->load->view('template', $data);

	}

    function jmlPeserta($id)
    {
        $kdjdl = get_kd_jdl_feed($id);
        $this->db->select('count(distinct kd_krs) as jml');
        $this->db->from('tbl_krs_feeder');
        $this->db->where('kd_jadwal', $kdjdl);
        $data['jml'] = $this->db->get()->row()->jml;
        $this->load->view('jumlah_peserta_modal', $data);
    }


	function detail_mhs($id)
	{
        $this->db->where('id_jadwal', $id);
        $jadwal = $this->db->get('tbl_jadwal_matkul_feeder')->row();
     
        $this->session->set_userdata('kd_jadwal', $jadwal->kd_jadwal);
        $this->session->set_userdata('kd_matakuliah', $jadwal->kd_matakuliah);
        $this->session->set_userdata('id_jadwal', $id);
       
        $data['th'] = $this->session->userdata('tahunajaran');

        $this->db->distinct();
		$this->db->select('npm_mahasiswa,kd_jadwal');
		$this->db->from('tbl_krs_feeder');
		$this->db->where('kd_jadwal', $jadwal->kd_jadwal);
        $this->db->order_by('npm_mahasiswa', 'asc');
		$data['peserta'] = $this->db->get()->result();
        
		$data['nama_kelas'] = $jadwal->kelas;
		$data['jdl_mk'] = $jadwal->kd_jadwal;

		$data['page'] = "v_kelas_detail";
		$this->load->view('template', $data);
	}

	function load_mhs()
	{

        $this->db->distinct();

        $this->db->select('id_mhs,NIMHSMSMHS,NMMHSMSMHS');

        $this->db->from('tbl_mahasiswa');
        
        $this->db->like('NIMHSMSMHS', $_GET['term'], 'both');

        $this->db->or_like('NMMHSMSMHS', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

            				'id'	=> $row->id_mhs,

                            'nim'   => $row->NIMHSMSMHS,

                            'value' => $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS

                            );

        }

        echo json_encode($data);

    }

    function tambah_mhs(){
        $this->load->view('pindahan_kelas');
    }

    function edit_jadwal($id){
        $data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul_feeder mk
                                LEFT JOIN tbl_matakuliah a ON mk.`id_matakuliah`=a.`id_matakuliah`
                                left JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                WHERE mk.`id_jadwal` = '.$id.'')->row();
    
        $this->load->view('v_kelas_edit', $data);
    }

    function update_kelas()
    {
        $mhs = $this->input->post('student');
        $exp = explode('-', $mhs);
        $npm = trim($exp[0]);

        // var_dump($npm);exit();

        $q = $this->db->query("SELECT kd_krs FROM tbl_krs_feeder
                                WHERE kd_krs like '".$npm.$this->session->userdata('tahunajaran')."%'
                                AND npm_mahasiswa = '".$npm."' 
                                AND kd_matakuliah = '".$this->session->userdata('kd_matakuliah')."'
                                limit 1")->row();
        // var_dump($q);exit();
        // var_dump($npm.$this->session->userdata('tahunajaran').'---'.$this->session->userdata('kd_matakuliah').'--'.$this->session->userdata('kd_jadwal'));exit();

        $data = array('kd_jadwal' => $this->session->userdata('kd_jadwal'));

        $this->db->where('kd_krs', $q->kd_krs);
        $this->db->where('kd_matakuliah', $this->session->userdata('kd_matakuliah'));
        $this->db->update('tbl_krs_feeder', $data);

        $this->db->where('kd_krs', $q->kd_krs);
        $this->db->where('kd_matakuliah', $this->session->userdata('kd_matakuliah'));
        $this->db->update('tbl_nilai_detail_feeder', $data);

        echo "<script>history.go(-1);</script>";
    }

    function delete_jadwal($npm)
    {
        $object = array('kd_jadwal' => NULL );

        $this->db->where('kd_jadwal',$this->session->userdata('kd_jadwal'));
        $this->db->where('npm_mahasiswa',$npm);
        $this->db->update('tbl_krs_feeder', $object);

        redirect('sync_feed/kelas/detail_mhs/'.$this->session->userdata('id_jadwal'),'refresh');

    }

    function load_dosen($idk){

		$data['jadwal'] = $this->db->where('id_jadwal',$idk)

								 ->get('tbl_jadwal_matkul_feeder')->row();

		$data['dosen']=$this->db->get('tbl_karyawan')->result();

		$this->load->view('penugasan_dosen',$data);

	}

	function update_dosen(){

        $dosen = $this->input->post('kd_dosen');

        $id    = $this->input->post('id_jadwal');
        // var_dump($jadwal_dosen);die();

        $data = array( 'kd_dosen' => $dosen);

        $this->db->where('id_jadwal',$id)

                 ->update('tbl_jadwal_matkul_feeder', $data);

        redirect('sync_feed/kelas/view_matakuliah','refresh');

    }

    function update_data_matakuliah()
    {
        date_default_timezone_set("Asia/Jakarta"); 

        extract(PopulateForm());

        $q = $this->db->where('id_matakuliah', $nama_matakuliah)->get('tbl_matakuliah')->row();

        $ambil_kd = $this->db->query('select * from tbl_matakuliah where id_matakuliah = '.$nama_matakuliah.'')->row();

        $kd_mk = $ambil_kd->kd_matakuliah;

        $id_jur = $this->session->userdata('id_jurusan_prasyarat');

        $q_jur = $this->db->where('kd_prodi',$id_jur)->get('tbl_jurusan_prodi')->row();

        $data = array(

            'kelas'         => $kelas,

            'id_matakuliah' => $nama_matakuliah,

            'kd_matakuliah' => $kd_mk,

            'waktu_kelas'   => $st_kelas,

            );

        $this->db->where('id_jadwal', $id_jadwal)->update('tbl_jadwal_matkul_feeder',$data);

        redirect(base_url('sync_feed/kelas/view_matakuliah'));

    }

    function save_data()
    {
        date_default_timezone_set("Asia/Jakarta"); 

        extract(PopulateForm());

        $q=$this->db->where('id_matakuliah', $nama_matakuliah)->get('tbl_matakuliah')->row();

        $w = ($q->sks_matakuliah)*50;        

        $ambil_kd = $this->db->query('select * from tbl_matakuliah where id_matakuliah = '.$nama_matakuliah.'')->row();

        $kd_mk = $ambil_kd->kd_matakuliah;

            $id_jur = $this->session->userdata('id_jurusan_prasyarat');

            $q_jur = $this->db->where('kd_prodi',$id_jur)->get('tbl_jurusan_prodi')->row();

            $gpass = NULL;
             $n = 6; // jumlah karakter yang akan di bentuk.
             $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";
             
             for ($i = 0; $i < $n; $i++) {
             $rIdx = rand(1, strlen($chr));
             $gpass .=substr($chr, $rIdx, 1);
             }
             
            $cc = $this->db->like('kd_jadwal',$id_jur)->get('tbl_jadwal_matkul_feeder')->result();

            $hasil = count($cc);
            //var_dump($count);die();

            if($hasil==0) {

                $hasilakhir="".$q_jur->kd_prodi."/".$gpass."/001";

            } elseif($hasil < 10) {

                $hasilakhir="".$q_jur->kd_prodi."/".$gpass."/00".($hasil+1);

            } elseif($hasil < 100) {

                $hasilakhir="".$q_jur->kd_prodi."/".$gpass."/0".($hasil+1);

            } elseif ($hasil < 1000) {

                $hasilakhir="".$q_jur->kd_prodi."/".$gpass."/".($hasil+1);

            } elseif ($hasil >= 1000){

                $hasilakhir="".$q_jur->kd_prodi."/".$gpass."/".($hasil+1);

            }

            $session = $this->session->userdata('sess_login');

            $user = $session['username'];


            $data = array(

                'kelas'         => $kelas,

                'waktu_kelas'   => $st_kelas,

                'kd_jadwal'     => $hasilakhir,

                'id_matakuliah' => $nama_matakuliah,

                'kd_matakuliah' => $kd_mk,

                'kd_tahunajaran'=> $this->session->userdata('tahunajaran'),

                'audit_date'    => date('Y-m-d H:i:s'),

                'audit_user'    => $user

                );

            $this->db->insert('tbl_jadwal_matkul_feeder', $data);

            redirect('sync_feed/kelas/view_matakuliah','refresh');
    }

    function cetak_jadwal_all(){
        $logged = $this->session->userdata('sess_login');
        $userid = $logged['userid'];

        //$data['prodi'] = $this->db->query('select * from tbl_jurusan_prodi where kd_prodi = '.$this->session->userdata('id_jurusan_prasyarat').'')->row()->kd_prodi;

        $data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul_feeder mk
                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                JOIN tbl_kurikulum_matkul_new km ON mk.`kd_matakuliah` = km.`kd_matakuliah`
                                JOIN tbl_kurikulum_new kur ON kur.`kd_kurikulum` = km.`kd_kurikulum` 
                                WHERE mk.`kd_jadwal` LIKE "'.$userid.'%"
                                AND a.`kd_prodi` = "'.$userid.'"
                                AND kur.`kd_prodi` = "'.$userid.'"
                                AND mk.`kd_tahunajaran` LIKE "'.$this->session->userdata('tahunajaran').'%"
                                GROUP BY mk.`kd_jadwal`
                                ORDER BY semester_kd_matakuliah');

        $data['duplicate'] = $data['rows']->row();
        //$data['rows']      = $q->result();

        $this->load->view('feed_cetak_jdl', $data);

    }

    function sync_kelas_nilai($id)
    {
        $this->load->model('sync_feed/kelas_model', 'kelas');

        $logged = $this->session->userdata('sess_login');
        $years 	= $this->session->userdata('tahunajaran');

        $kdProdiFeeder = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$logged['userid'],'kd_prodi','asc')->row();
        
        $jadwal 	= $this->app_model->getdetail('tbl_jadwal_matkul_feeder','id_jadwal',$id,'id_jadwal','asc')->row();   
        
        $getnilai 	= $this->kelas->getNilaiFeeder($jadwal->kd_jadwal)->result();
  
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url    = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy  = $client->getProxy();
        $gettok = $proxy->GetToken(userfeeder, passwordfeeder);
        $token  = $gettok;
        
        $kodemk = str_replace('-', '', $jadwal->kd_matakuliah);
        $kelas  = str_replace(' ', '', $jadwal->kelas);

        $table1 = 'kelas_kuliah';
        $filter = "kode_mk = '".$kodemk."' and nm_kls = '".$kelas."'";
        $limit  = 100; // jumlah data yang diambill
        $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
        $result = $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);
        
        $before = yearBefore();

        foreach ($result['result'] as $value1) {
            if (($value1['id_smt'] == $before) && ($value1['id_sms'] == $kdProdiFeeder->id_sms)) {
                $id_kls = $value1['id_kls'];
            }
        }

        $table   = 'nilai';
        $records = [];
        foreach ($getnilai as $val) {
            
            $nilai_huruf = $this->kelas->getNilaiTransaksi($val->npm_mahasiswa,$jadwal->kd_matakuliah,$years)->row();    
                        
            $sp  = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".$val->npm_mahasiswa."%'");
            $key = ['id_reg_pd' => $sp['result']['id_reg_pd'], 'id_kls' => $id_kls];
            
            if ((is_null($nilai_huruf->nilai_akhir)) or (empty($nilai_huruf->nilai_akhir)) or ($nilai_huruf->nilai_akhir == 0) ) {
            	$data = array(
                    'nilai_angka'=>'0.00',
                    'nilai_huruf'=>'E',
                    'nilai_indeks'=>'0.00'
                );

            } elseif ($nilai_huruf->nilai_akhir > 100) {
                $data = array(
                    'nilai_angka'=>'0.00',
                    'nilai_huruf'=>'T',
                    'nilai_indeks'=>'0.00'
                );

            } else {
            	$data = array(
                    'nilai_angka'=>$nilai_huruf->nilai_akhir,
                    'nilai_huruf'=>$nilai_huruf->NLAKHTRLNM,
                    'nilai_indeks'=>$nilai_huruf->BOBOTTRLNM
                );
            }

            $records[] = array('key'=>$key, 'data'=>$data);
        }

        foreach ($records as $record) {
            $result = $proxy->UpdateRecord($token, $table, json_encode($record));
            var_dump($result);echo "<hr>";
        }
        $this->db->query("UPDATE tbl_jadwal_matkul_feeder set status_feeder = 1 where id_jadwal = ".$id."");
        $this->db->query("UPDATE tbl_krs_feeder set status_feeder = 1 where kd_jadwal = '".$jadwal->kd_jadwal."'");
    }

    function sync_kelas_add()
    {
        $logged 		= $this->session->userdata('sess_login');
        $kdProdiFeeder 	= $this->db->query('SELECT * FROM tbl_jurusan_prodi WHERE `kd_prodi`="'.$logged['userid'].'"')->row();
        $jadwal 		= $this->db->query("SELECT distinct kd_jadwal,kelas,kd_matakuliah from tbl_jadwal_matkul_feeder 
        									where kd_jadwal like '".$logged['userid']."%' 
        									and kd_tahunajaran = '".$this->session->userdata('tahunajaran')."'")->result();
        //var_dump($jadwal);exit();
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url 	= 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;
        $table 	= 'kelas_kuliah';

        foreach ($jadwal as $value) {
            $mk = $this->db->query("SELECT mk.sks_matakuliah from tbl_matakuliah mk
                                    JOIN tbl_kurikulum_matkul_new km ON mk.kd_matakuliah = km.kd_matakuliah
                                    JOIN tbl_kurikulum_new kr ON kr.kd_kurikulum = km.kd_kurikulum
            						where mk.kd_matakuliah = '".$value->kd_matakuliah."' 
            						and mk.kd_prodi = '".$logged['userid']."'
                                    and kr.status = 1")->row();

            $table1 = 'mata_kuliah';
            $filter = "kode_mk = '".str_replace('-', '', trim($value->kd_matakuliah))."'";
            $limit 	= 150; // jumlah data yang diambill
            $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
            $result2 = $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);

            foreach ($result2['result'] as $value1) {
                if ($value1['id_sms'] == $kdProdiFeeder->id_sms) {
                    $id_mk = $value1['id_mk'];
                }
            }
            
            $record['id_sms'] = $kdProdiFeeder->id_sms; // id prodi
            $record['id_smt'] = $this->session->userdata('tahunajaran');
            $record['nm_kls'] = substr(str_replace(' ', '', $value->kelas), 0,5); //nama kelas
            $record['sks_mk'] = $mk->sks_matakuliah; //sks mk
            $record['sks_tm'] = $mk->sks_matakuliah; //sks tatap muka
            $record['a_selenggara_pditt'] = '0';
            $record['a_pengguna_pditt'] = '0';
            $record['kuota_pditt'] = '0';
            $record['id_mk'] = $id_mk;

            $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
            echo "<pre>";
            print_r ($result1['result']);
            echo "</pre><hr>";
        }
        //$result = $proxy->InsertRecordset($token, $table,json_encode($records));
    }

    function sync_kelas_add2($id)
    {
        $logged = $this->session->userdata('sess_login');
        $q = $this->db->query('SELECT * FROM tbl_jurusan_prodi WHERE `kd_prodi`="'.$logged['userid'].'"')->row();
        $jadwal = $this->db->query("SELECT distinct kd_jadwal,kelas,kd_matakuliah from tbl_jadwal_matkul_feeder 
                                    where id_jadwal = ".$id." and kd_tahunajaran = '".yearBefore()."'")->result();
        //var_dump($jadwal);exit();
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);

        $token = $result;
        $table = 'kelas_kuliah';

        foreach ($jadwal as $value) {
            $mk = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah 
                                    where kd_matakuliah = '".$value->kd_matakuliah."' and kd_prodi = '".$logged['userid']."'")->row();

            $table1 = 'mata_kuliah';
            $filter = "kode_mk = '".str_replace('-', '', trim($value->kd_matakuliah))."'";
            $limit = 10; // jumlah data yang diambill
            $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
            $result2 = $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);
            //var_dump($result2);exit();
            foreach ($result2['result'] as $value1) {
                if ($value1['id_sms'] == $q->id_sms) {
                    $id_mk = $value1['id_mk'];
                }
            }
            //var_dump($id_mk);exit();
            $record['id_sms'] = $q->id_sms; // id prodi
            $record['id_smt'] = yearBefore();
            $record['nm_kls'] = substr(str_replace(' ', '', $value->kelas), 0,5); //nama kelas
            $record['sks_mk'] = $mk->sks_matakuliah; //sks mk
            $record['sks_tm'] = $mk->sks_matakuliah; //sks tatap muka
            $record['a_selenggara_pditt'] = '0';
            $record['a_pengguna_pditt'] = '0';
            $record['kuota_pditt'] = '0';
            $record['id_mk'] = $id_mk;

            $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
            var_dump($result1);echo "<hr>";
        }
        //$result = $proxy->InsertRecordset($token, $table,json_encode($records));
    }

    function sync_kelas_krs($id)
    {
        $thajar = getactyear();

        $logged = $this->session->userdata('sess_login');
        $q 		= $this->db->query('SELECT * FROM tbl_jurusan_prodi WHERE `kd_prodi`="'.$logged['userid'].'"')->row();
        //var_dump('SELECT * FROM tbl_jurusan_prodi WHERE `kd_prodi`="'.$logged['userid'].'"');exit();
        $jadwal = $this->app_model->getdetail('tbl_jadwal_matkul_feeder','id_jadwal',$id,'id_jadwal','asc')->row();   
        //var_dump($jadwal);
        $getnilai = $this->db->query("SELECT DISTINCT a.`npm_mahasiswa` FROM tbl_krs_feeder a 
                                    JOIN tbl_verifikasi_krs c ON a.`kd_krs` = c.`kd_krs`
                                    WHERE a.`kd_matakuliah` = '".$jadwal->kd_matakuliah."' AND c.`tahunajaran` = '".$jadwal->kd_tahunajaran."' AND a.kd_jadwal = '".$jadwal->kd_jadwal."'")->result();
        //var_dump($getnilai);exit();
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url 	= 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;

        $table1 	= 'kelas_kuliah';
        $filter 	= "kode_mk = '".str_replace('-', '', $jadwal->kd_matakuliah)."' and nm_kls = '".substr(str_replace(' ', '', $jadwal->kelas), 0,5)."'";
        $limit 		= 50; // jumlah data yang diambill
        $offset 	= 0; // offset dipakai untuk paging, contoh: bila $limit=20
        $order 		= 'id_smt desc';
        $result2 	= $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);
        // var_dump($result2['result']);exit();

        foreach ($result2['result'] as $value1) {
            if (($value1['id_smt'] == $thajar) and ($value1['id_sms'] == $q->id_sms)) {
                $id_kls = $value1['id_kls'];
            }
        }
        //var_dump($id_kls);exit();
        
        $table = 'nilai';
        $records = array();
        foreach ($getnilai as $value) {
            $sp2 = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".$value->npm_mahasiswa."%'");
            $record['id_reg_pd'] = $sp2['result']['id_reg_pd'];
            $record['id_kls'] = $id_kls;
            $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
            var_dump($result1);echo "<hr>";
        }
        //$this->db->query("update tbl_jadwal_matkul_feeder set status_feeder = 1 where id_jadwal = ".$id."");
        //$this->db->query("update tbl_krs_feeder set status_feeder = 1 where kd_jadwal = '".$jadwal->kd_jadwal."'");
    }

    function sync_ajar_dosen()
    {
        $yearBefore = yearBefore();
        
        $loginSession = $this->session->userdata('sess_login');

        $prodi = $this->db->query('SELECT * FROM tbl_jurusan_prodi WHERE kd_prodi="'.$loginSession['userid'].'"')->row();
        
        $jadwal = $this->db->query("SELECT kelas,kd_matakuliah,nidn,nupn,nid from tbl_jadwal_matkul_feeder a
                                    JOIN tbl_karyawan b ON a.`kd_dosen` = b.`nid`
                                    where kd_jadwal like '".$loginSession['userid']."%' 
                                    AND kd_tahunajaran = '".$this->session->userdata('tahunajaran')."' 
                                    AND (
                                            (b.`nidn` IS NOT NULL OR b.`nidn` != '') 
                                            OR (b.`nupn` IS NOT NULL OR b.`nupn` != '')
                                        )
                                    ORDER BY nidn ASC")->result();

        $this->load->library("Nusoap_lib");
        // $url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token = $result;
        $table = 'ajar_dosen';

        foreach ($jadwal as $value) {
            $mk = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah 
                                    where kd_matakuliah = '".$value->kd_matakuliah."' 
                                    AND kd_prodi = '".$loginSession['userid']."'")->row();

            if (!empty($value->nidn) && !is_null($value->nidn)) {
                $id_dosen = $value->nidn;
            } else {
                $id_dosen = $value->nupn;
            }
            // var_dump($id_dosen);exit();

            $record['id_jns_eval'] = '1';
           
            $id_sdm = $proxy->GetRecord($token, 'dosen', "nidn = '".trim($id_dosen)."'");
            
            // echo "<pre>";
            // var_dump($id_sdm['result']['id_sdm']);
            // echo "</pre>"; exit();

            $id_reg_ptk = $proxy->GetRecordSet($token, 'dosen_pt', "p.id_sdm = '".$id_sdm['result']['id_sdm']."'");;

            // echo "<pre>";
            // var_dump($id_reg_ptk['result'][0]['id_reg_ptk']);
            // echo "</pre>";  exit();

            $record['id_reg_ptk'] = $id_reg_ptk['result'][0]['id_reg_ptk'];

            if ($loginSession['userid'] == '74101') {
                $kelas = substr(trim(str_replace(' ', '', $value->kelas)), 0,5);
            } else {
                $kelas = trim(str_replace(' ', '', $value->kelas));
            }

            $table1     = 'kelas_kuliah';
            $filter     = "kode_mk = '".str_replace('-', '', $value->kd_matakuliah)."' and nm_kls = '".$kelas."'";
            $limit      = 100; // jumlah data yang diambill
            $offset     = 0; // offset dipakai untuk paging, contoh: bila $limit=20
            $result2    = $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);
            // var_dump($result2);exit();

            // echo "<pre>";
            // var_dump($result2);
            // echo "</pre>"; exit();

            foreach ($result2['result'] as $value1) {
                if (($value1['id_smt'] == $yearBefore) && ($value1['id_sms'] == $prodi->id_sms)) {
                    $id_kls = $value1['id_kls'];
                }
            }
            // var_dump($value->kd_matakuliah.' - '.$value->kelas.' - '.$id_kls.' - '.$record['id_reg_ptk']);exit();
            $record['id_kls']               = $id_kls;
            $record['sks_subst_tot']        = $mk->sks_matakuliah;
            $record['sks_tm_subst']         = $mk->sks_matakuliah;
            $record['sks_prak_subst']       = '0';
            $record['sks_prak_lap_subst']   = '0';
            $record['sks_sim_subst']        = '0';
            $record['jml_tm_renc']          = '16';
            $record['jml_tm_real']          = '16';

            // echo "<pre>";
            // print_r ($record);
            // echo "</pre>";  exit();

            $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
            var_dump($result1);echo "<hr>";
        }

        //$result = $proxy->InsertRecordset($token, $table,json_encode($records));
    }

    function sync_mhs_ipk()
    {
        $ses = $this->session->userdata('sess_login');

        $this->load->library("Nusoap_lib");
        // gunakan sandbox untuk coba-coba
        // $url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl';
        // gunakan live bila sudah yakin
        $url = 'http://172.16.0.58:8082/ws/live.php?wsdl';
        $client = new nusoap_client($url, true);
        $proxy  = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);

        $token = $result;
        $table = 'kuliah_mahasiswa';
        $input = count($this->input->post('mhs',TRUE));
        
        for ($i=1; $i <= $input ; $i++) {
            $mhs = $this->input->post('mhs['.$i.']', TRUE);
            $ips = $this->input->post('ips['.$i.']', TRUE);
            $ipk = $this->input->post('ipk['.$i.']', TRUE);
            $smt = $this->input->post('smt['.$i.']', TRUE);
            $sks = $this->input->post('sks['.$i.']', TRUE);

            if ($sks != 0) {
                #insert
                $sp = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".$mhs."%'");

                $record['id_smt']       = yearBefore();
                $record['id_reg_pd']    = $sp['result']['id_reg_pd'];
                $record['ips']          = $ips;
                $record['id_stat_mhs']  = 'A';
                $record['sks_smt']      = $smt;
                $record['ipk']          = $ipk;
                $record['sks_total']    = $sks;

                // if ($ses['userid'] == '55201') {
                // 	echo "<pre>";
                // 	var_dump($record);
                // 	echo "</pre>";
                // }

                $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
                var_dump($result1['result']);
                echo "<hr>";
            }
        }
    }

    function get_detail_matakuliah_by_semester_feeder($id)
    {
        $user = $this->session->userdata('sess_login');
        $kode = $user['userid'];

        if ($this->session->userdata('tahunajaran') > 20162) {
            $data   = $this->db->query('SELECT DISTINCT a.kd_matakuliah, a.nama_matakuliah, a.id_matakuliah FROM tbl_matakuliah a
                                        join tbl_kurikulum_matkul_new b on a.kd_matakuliah = b.kd_matakuliah
                                        WHERE a.kd_prodi = '.$kode.' 
                                        AND b.semester_kd_matakuliah = '.$id.' 
                                        AND b.`kd_kurikulum` LIKE "%'.$kode.'%"
                                        order by nama_matakuliah asc')->result();

        } else {
            $data   = $this->db->query('SELECT DISTINCT a.kd_matakuliah, a.nama_matakuliah, a.id_matakuliah FROM tbl_matakuliah a
                                        join tbl_kurikulum_matkul b on a.kd_matakuliah = b.kd_matakuliah
                                        WHERE a.kd_prodi = '.$kode.' 
                                        AND b.semester_kd_matakuliah = '.$id.' 
                                        AND b.`kd_kurikulum` LIKE "%'.$kode.'%"
                                        order by nama_matakuliah asc')->result();
        }
        
        $list = "<option> -- </option>";

        foreach($data as $row){
            $list .= "<option value='".$row->id_matakuliah."'>".$row->kd_matakuliah." - ".$row->nama_matakuliah."</option>";
        }

        die($list);

    }

}

/* End of file Kelas.php */
/* Location: ./application/modules/sync_feed/controllers/Kelas.php */