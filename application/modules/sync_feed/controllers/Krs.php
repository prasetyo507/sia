<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Krs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		ini_set('memory_limit', '2048M');
		$this->load->model('feeder_model');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(71)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$data['tahunakademik'] = $this->app_model->getdata('tbl_tahunakademik','kode','asc')->result();
		$data['page'] = 'v_krs_select';
		$this->load->view('template', $data); 
	}

	function viewkrsprodi()
	{
		set_time_limit(0);
		
		$actyear = getactyear();

		$yearbefore = yearBefore();

		$data['lastyear'] = $yearbefore;

		$data['activeyear'] = $actyear;

		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(8, $grup)) or (in_array(19, $grup))) {
			$prodi = $logged['userid'];
		} else {
			$prodi = $this->input->post('jurusan', TRUE);
		}
		$tahun = $this->input->post('tahun', TRUE);
		$this->session->set_userdata('angkatan',$tahun);

		$ta = $this->input->post('ta', TRUE);
		$this->session->set_userdata('ta',$ta);

		$data['chooseyear'] = $this->session->userdata('ta');
		$data['angkatan'] = $tahun;

		$this->db->distinct();
		$this->db->select('npm_mahasiswa,kd_krs,tahunajaran');
		$this->db->from('tbl_verifikasi_krs');
		$this->db->where('kd_jurusan', $prodi);
		$this->db->where('tahunajaran', $ta);
		$this->db->like('npm_mahasiswa', $this->session->userdata('angkatan'), 'after');
		$this->db->order_by('npm_mahasiswa', 'asc');
		$data['getData'] = $this->db->get()->result();

		if ($ta == $yearbefore) {
			$data['page'] = 'v_krs_list';
		} else {
			$data['page'] = 'v_krs_list2';
		}

		$this->load->view('template',$data);
	}

	function view_krs($id)
	{
		$data['pembimbing']=$this->app_model->get_pembimbing_krs($id)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa($id)->result();
		//var_dump($data['detail_krs']);die();

		$data['kd_krs'] = $id;
		$data['page'] = 'akademik/viewkrs_detail';
		$this->load->view('template',$data);
	}

	function getmhs()

    {

        $this->db->select("*");

        $this->db->from('tbl_krs');

        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.NIMHSMSMHS = tbl_krs.npm_mahasiswa');

        $this->db->where('jabatan_id', 8);

        //$this->db->join('obat_satuan', 'obat.satuan = obat_satuan.id');

        $this->db->like('nama', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {



            $data[] = array(

                            'nid' 	 => $row->nid,

                            'value'      => $row->nama,

                            );

        }



        echo json_encode($data);

    }

    function viewkrsmhs($id){
    	if (ctype_digit($id)) {
			$primary = $id;
		} else {
			$idkd = $this->app_model->getdetail('tbl_verifikasi_krs','slug_url',$id,'npm_mahasiswa','asc')->row();
			$primary = $idkd->npm_mahasiswa;
		}
		$data['slugcoy'] = $this->app_model->getdetail('tbl_verifikasi_krs','npm_mahasiswa',$primary,'npm_mahasiswa','asc')->row();
		$data['krs'] = $this->app_model->get_all_krs_mahasiswa($primary)->result();
		$data['npm'] = $primary;
		//$mhs = $this->app_model->get_detail('tbl_mahasiswa','NIMHSMSMHS',$id,'NIMHSMSMHS','asc')->row();
		//$data['smtr'] = $this->app_model->get_semester($mhs->SMAWLMSMHS);
		$data['page'] = 'v_krs_detailmhs';
		$this->load->view('template',$data);
    }

    function viewview($id)
    {
    	$getactyear = getactyear();

    	$logged = $this->session->userdata('sess_login');
    	$thn = substr($id, 12,5);
    	$nim = substr($id, 0,12);

    	$data['npm'] = $nim;


		//tab1
    	$data['pembimbing']=$this->app_model->get_pembimbing_krs($id)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa_feeder($id)->result();
		

		//tab2
		$aa = substr($thn, 0,4);
		$bb = substr($thn, -1);

		//$aa = $aa-1;
		
		if ($bb == 2) {
			$cc = $aa.'1';
		} else{
			$cc = ($aa-1).'2';
		}

		
		//print_r($cc);die();
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim,'NIMHSMSMHS','asc')->row();
		$data['semester'] = $a = $this->app_model->get_semester_khs($data['mhs']->SMAWLMSMHS,$id);
		$data['tahunakademik'] = $thn;
		if ($thn < '20151') {
			$data['detail_khs'] = $this->db->query("SELECT distinct b.kd_matakuliah,b.nama_matakuliah,b.sks_matakuliah from tbl_transaksi_nilai a join tbl_matakuliah_copy b on a.KDKMKTRLNM = b.kd_matakuliah 
													where a.THSMSTRLNM = '".$cc."' and b.tahunakademik = '".$cc."' and a.NIMHSTRLNM = '".$nim."' and b.kd_prodi = '".$data['mhs']->KDPSTMSMHS."'")->result();
			$data['kode'] = $this->db->query("SELECT distinct kd_krs from tbl_krs where kd_krs like '".$nim.$cc."%'")->row();
		} else {
			$data['detail_khs'] = $this->db->query("SELECT distinct a.*,b.nama_matakuliah,b.sks_matakuliah from tbl_krs a 
													join tbl_matakuliah b on a.kd_matakuliah = b.kd_matakuliah 
													where a.kd_krs like '".$nim.$cc."%' and b.kd_prodi = '".$data['mhs']->KDPSTMSMHS."'")->result();
			$data['kode'] = $this->db->query("SELECT distinct kd_krs from tbl_krs where kd_krs like '".$nim.$cc."%'")->row();
		}
		$logged = $this->session->userdata('sess_login');

		//view
		if ($thn == $getactyear) {
			$data['page'] = 'sync_feed/v_krs_smtr_detail';
		} else {
			$data['page'] = 'sync_feed/v_krs_smtr_detail2';
		}
		$data['kd_krs'] = $id;
		$data['ta_khs'] = $cc;
		
		$this->load->view('template',$data);
    }

    function get_jadwal(){
		//$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
		$kd_matakuliah = $_POST['kd_matakuliah'];

		$user = $this->session->userdata('sess_login');

		$prodi  = $user['username'];

		$mhs  = $this->app_model->get_jurusan_mhs($npm)->row();

		$data = $this->app_model->get_pilih_jadwal_feeder($kd_matakuliah,$prodi,$this->session->userdata('ta'))->result();
                
		$js = json_encode($data);
		echo $js;
	}

	function in()
	{
		// ini_set('memory_limit','64M');
		$tung = count($this->input->post('mhs'));
		// var_dump($tung);exit();
		for ($i=1; $i <= $tung; $i++) { 
			$npm[] 		= $this->input->post('mhs['.$i.']');
			$nim 		= $this->input->post('mhs['.$i.']');
			$ips 		= $this->input->post('ips['.$i.']');
			$totsks 	= $this->input->post('sks['.$i.']');
			$ipk 		= $this->input->post('ipk['.$i.']');
			//var_dump($this->session->userdata('ta'));exit();

			$datax[]	= [
							'npm_mahasiswa' => $nim, 
							'tahunajaran' 	=> $this->session->userdata('ta'), 
							'ips' 			=> $ips, 
							'total_sks' 	=> $totsks, 
							'ipk' 			=> $ipk
						];
		}

		$this->db->where_in('npm_mahasiswa', $npm);
		$this->db->where('tahunajaran', $this->session->userdata('ta'));
		$this->db->delete('tbl_khs');

		$this->db->insert_batch('tbl_khs',$datax);

		for ($x=1; $x < $tung; $x++) { 

			$npm 		= $this->input->post('mhs['.$x.']');
			$nim 		= $this->input->post('mhs['.$x.']');
			$ips 		= $this->input->post('ips['.$x.']');
			$totsks 	= $this->input->post('sks['.$x.']');
			$ipk 		= $this->input->post('ipk['.$x.']');

			$datas = [
				'SKSTTTRAKM' => $totsks,
				'NLIPKTRAKM' => $ipk 
			];

			$this->db->where('NIMHSTRAKM', $npm);
			$this->db->where('THSMSTRAKM', $this->session->userdata('ta'));
			$this->db->update('tbl_aktifitas_kuliah_mahasiswa', $datas);
		}
		
		echo "<script>alert('Berhasil');document.location.href='".base_url()."sync_feed/krs';</script>";
	}

	function cetak_khs()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(8, $grup))) {
			$prodi = $logged['userid'];
		} else {
			$prodi = $this->input->post('jurusan', TRUE);
		}
		// $tahun = $this->input->post('tahun', TRUE);
		// $this->session->set_userdata('tahunajaran',$tahun);
		// $ta = $this->input->post('ta', TRUE);
		// $this->session->set_userdata('ta',$ta);

		$data['angkatan'] = $tahun;
		$this->db->distinct();
		$this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,b.kd_krs,b.tahunajaran');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db->where('a.KDPSTMSMHS', $logged['userid']);
		$this->db->where('a.TAHUNMSMHS >', '2011');
		$this->db->where('a.TAHUNMSMHS <', '2016');
		$this->db->where('b.tahunajaran', $this->session->userdata('ta'));
		//$this->db->or_where('b.tahunajaran', 20161);
		$this->db->order_by('a.NIMHSMSMHS', 'asc');
		//$this->db->limit(5);
		$data['getData'] =  $this->db->get()->result();
		// if ($ta == '20152') {
		// 	$data['page'] = 'v_krs_list';
		// } else {
		// 	$data['page'] = 'v_krs_list2';
		// }

		//var_dump($data['getData']);die();
		$this->load->view('feed_excel_khs',$data);
	}

}

/* End of file Krs.php */
/* Location: ./application/controllers/Krs.php */