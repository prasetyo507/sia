<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen_ajar extends CI_Controller {

	public function index()
	{
		$this->db->where('kode >', 20152);
		$data['akademik'] = $this->db->get('tbl_tahunakademik')->result();
		$data['page'] = "v_selectyear_dosen";
        $this->load->view('template', $data);
	}

	function loadData()
	{
		$yearn        = $this->input->post('tahun', TRUE);
		$sesi         = $this->session->userdata('sess_login');
		$data['sess'] = $sesi['userid'];
		$data['q']    = $this->app_model->dosen_ajar_feed($data['sess'],$yearn);
		$data['page'] = "feed_dsn_ajar";
		$this->load->view('template', $data);
	}

	function dosen()
	{
		$this->load->library("Nusoap_lib");
		// gunakan sandbox untuk coba-coba
        // $url = 'http://172.16.2.194:8082/ws/sandbox.php?wsdl';

        // gunakan live bila sudah yakin
        $url = 'http://172.16.2.194:8082/ws/live.php?wsdl'; 

        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;
        $filter = "";
        $order 	= "";
        $limit 	= 3000; // jumlah data yang diambill
        $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
        $data['hasil'] = $proxy->GetListPenugasanDosen($token, $filter, $order, $limit,$offset);
        $data['page'] = "dosen_feeder";
		$this->load->view('template', $data);
	}

}

/* End of file Dosen_ajar.php */
/* Location: ./application/modules/sync_feed/controllers/Dosen_ajar.php */