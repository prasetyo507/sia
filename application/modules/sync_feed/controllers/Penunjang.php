<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penunjang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['page']='v_penunjang';
        $this->load->view('template', $data);
	}

}

/* End of file Penunjang.php */
/* Location: ./application/controllers/Penunjang.php */