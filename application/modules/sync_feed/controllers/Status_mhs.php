<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status_mhs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '2048M');
		$this->load->model('monitoring_model');
		error_reporting(0);
	    if ($this->session->userdata('sess_login') == TRUE) {
            $cekakses = $this->role_model->cekakses(123)->result();
            if ($cekakses != TRUE) {
                echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
            }
        } else {
            redirect('auth','refresh');
        }
	}

	public function index()
	{
		$actyear = getactyear();
		$stryear = studyStart($actyear); 		

		$logged = $this->session->userdata('sess_login');
		$data['cuti'] = $this->db->select('*')
								->from('tbl_status_mahasiswa a')
								->join('tbl_mahasiswa b','a.npm=b.NIMHSMSMHS')
								->where('a.status','C')
								->where('a.tahunajaran',$actyear)
								->where('b.KDPSTMSMHS',$logged['userid'])
								->where('a.validate',1)
								->get()->result();
	
		$data['non'] = $this->db->query("SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_mahasiswa WHERE 
										NIMHSMSMHS NOT IN (
										    SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = '".$actyear."'
										) AND NIMHSMSMHS NOT IN (
										    SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = '".$actyear."' AND validate = 1
										) 
										AND KDPSTMSMHS = '".$logged['userid']."' AND BTSTUMSMHS >= '".$actyear."' 
										AND STMHSMSMHS NOT IN ('L','D','C','CA','K') 
										AND SMAWLMSMHS >= '".$stryear."'
										AND TAHUNMSMHS != '2019'
										")->result();

		$data['keluar'] = $this->db->query("SELECT * FROM tbl_mahasiswa WHERE STMHSMSMHS IN ('K','D') AND KDPSTMSMHS = '".$logged['userid']."'")->result();
		$data['lulus'] = $this->db->query("SELECT * FROM tbl_mahasiswa WHERE STMHSMSMHS = 'L' AND KDPSTMSMHS = '".$logged['userid']."' AND YEAR(TGLLSMSMHS) > 2016")->result();
		$data['prodiuser'] = $logged['userid'];
		$data['page'] = "feed_sts_mhs";
		$this->load->view('template', $data);
	}

	function update($id)
	{
		$this->db->query("UPDATE tbl_mahasiswa set STMHSMSMHS = 'K' where NIMHSMSMHS = '".$id."'");
		echo "<script>alert('Berhasil!');
		document.location.href='".base_url()."sync_feed/status_mhs';</script>";
	}

	function sync_sts()
	{
		$actyear = $this->app_model->getdetail('tbl_tahunakademik','status',1,'kode','asc')->row()->kode;

		$logged = $this->session->userdata('sess_login');

		$cut = $this->db->select('*')
						->from('tbl_status_mahasiswa a')
						->join('tbl_mahasiswa b','a.npm=b.NIMHSMSMHS')
						->where('a.status','C')
						->where('a.tahunajaran', $actyear)
						->where('b.KDPSTMSMHS',$logged['userid'])
						->where('a.validate',1)
						->get()->result();
        
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);

        $token = $result;

        foreach ($cut as $val) {
        	$ipk = $this->monitoring_model->getskstotalmhs($val->npm,$logged['userid']);
        	$table1 = 'kuliah_mahasiswa';
	        $sp = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".$val->npm."%'");
	        //var_dump($result2);exit();
	        $record['id_smt'] 		= $actyear;
	        $record['id_reg_pd'] 	= $sp['result']['id_reg_pd'];
			$record['id_stat_mhs'] 	= 'C';
			$record['ips'] 			= '0';
			$record['sks_smt'] 		= '0';
			$record['ipk']			= $ipk['ipk']; //1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu
			$record['sks_total'] 	= $ipk['sks'];
			//$record['id_sp'] = $result2['result']['id_sp'];
			//var_dump($record);exit();
	        $result1 = $proxy->InsertRecord($token, $table1, json_encode($record));
	        var_dump($result1);echo "<hr>";	
        }
    }

    function sync_sts_non()
	{
		$actyear 	= getactyear();
		$logged 	= $this->session->userdata('sess_login');
		$start 		= studyStart($actyear);

		$cut = $this->db->query("SELECT DISTINCT NIMHSMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS NOT IN (
								    SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = '".$actyear."'
								) AND NIMHSMSMHS NOT IN (
								    SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = '".$actyear."' 
								    AND validate = 1
								) 
								AND KDPSTMSMHS = '".$logged['userid']."' 
								AND BTSTUMSMHS >= '".$actyear."' 
								AND STMHSMSMHS NOT IN ('L','D','K','C','CA')
								AND SMAWLMSMHS >= '".$start."'
								AND TAHUNMSMHS != '2019'
								")->result();
		//var_dump($cut);exit();
        
        $this->load->library("Nusoap_lib");

        // gunakan sandbox untuk coba-coba
        // $url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; 

        // gunakan live bila sudah yakin
        $url 	= 'http://172.16.0.58:8082/ws/live.php?wsdl';
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;
        $table1 = 'kuliah_mahasiswa';
        
        // if ($logged['userid'] == '70201') {
        // 	echo "<pre>";
        // 	print_r ($cut);exit();
        // 	echo "</pre>";
        // }

   //      if ($logged['userid'] == '70201') {
        	
   //      	$ipk 	= $this->monitoring_model->getskstotalmhs('201210415004',$logged['userid']);
   //      	$sp 	= $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".trim('201210415004')."%'");

   //      	$record['id_smt'] 		= $actyear;
	  //       $record['id_reg_pd'] 	= $sp['result']['id_reg_pd'];
			// $record['id_stat_mhs'] 	= 'N';
			// $record['ips'] 			= 0;
			// $record['sks_smt'] 		= 6;
			// $record['ipk']			= $ipk['ipk']; 
			// $record['sks_total'] 	= $ipk['sks'];
			// // 1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu
			// // $record['id_sp'] = $result2['result']['id_sp'];
			
			// // var_dump($record);exit();

	  //       $result1 = $proxy->InsertRecord($token, $table1, json_encode($record));
	  //       var_dump($result1);echo "<hr>";

   //      } else {
        	
        	foreach ($cut as $val) {
	        	$ipk 	= $this->monitoring_model->getskstotalmhs($val->NIMHSMSMHS,$logged['userid']);
	        	$sp 	= $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".trim($val->NIMHSMSMHS)."%'");
		        
		        /*
		        if ($logged['userid'] == '70201') {
		        	echo "<pre>";
			        print_r ($sp);exit();
			        echo "</pre>";
		        }
				*/

		        $record['id_smt'] 		= $actyear;
		        $record['id_reg_pd'] 	= $sp['result']['id_reg_pd'];
				$record['id_stat_mhs'] 	= 'N';
				$record['ips'] 			= 0;
				$record['sks_smt'] 		= 0;
				$record['ipk']			= $ipk['ipk']; 
				$record['sks_total'] 	= $ipk['sks'];
				// 1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu
				// $record['id_sp'] = $result2['result']['id_sp'];
				
				// if ($logged['userid'] == '70201') {
				// 	var_dump($record);exit();
				// }

		        $result1 = $proxy->InsertRecord($token, $table1, json_encode($record));
		        var_dump($result1);echo "<hr>";	
	        }	

        // }
        
        
        
        	
	}

	function sync_sts_out()
	{
		$actyear = $this->app_model->getdetail('tbl_tahunakademik','status',1,'kode','asc')->row()->kode;

		$logged = $this->session->userdata('sess_login');

		$out = $this->db->query("SELECT * FROM tbl_mahasiswa WHERE STMHSMSMHS = 'K' AND KDPSTMSMHS = '".$logged['userid']."'")->result();
		//var_dump($cut);exit();
        
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);

        $token = $result;

        foreach ($out as $val) {
        	$table1 = 'kuliah_mahasiswa';
	        $sp = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".trim($val->NIMHSMSMHS)."%'");
	        //var_dump($result2);exit();
	        $record['id_smt'] 		= $actyear;
	        $record['id_reg_pd'] 	= $sp['result']['id_reg_pd'];
			$record['id_stat_mhs'] 	= 'K';
			$record['ips'] 			= '0';
			$record['sks_smt'] 		= '0';
			$record['ipk']			= '0'; //1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu
			$record['sks_total'] 	= '0';
			//$record['id_sp'] = $result2['result']['id_sp'];
			//var_dump($record);exit();
	        $result1 = $proxy->InsertRecord($token, $table1, json_encode($record));
	        var_dump($result1);echo "<hr>";	
        }	
        	
	}

}

/* End of file Status_mhs.php */
/* Location: ./application/modules/sync_feed/controllers/Status_mhs.php */