<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maba extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        ini_set('memory_limit', '512M');
        $this->db2 = $this->load->database('regis',TRUE);
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(128)->result();
			if ($akses != TRUE) {
				redirect('home','refresh');
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$logged = $this->session->userdata('sess_login');
		$log = $logged['userid'];

        $max_angkatan   = $this->db->query("SELECT max(TAHUNMSMHS) as max from tbl_mahasiswa where KDPSTMSMHS = '".$log."' 
                                            order by TAHUNMSMHS desc limit 1 ")->row()->max;

        $new_angkatan   = $this->db->query("SELECT NIMHSMSMHS from tbl_mahasiswa where TAHUNMSMHS = '".$max_angkatan."'
                                            and KDPSTMSMHS = '".$log."'")->result();

        foreach ($new_angkatan as $key) {
            $data[] = $key->NIMHSMSMHS;
        }

        $this->db2->select('*');
        $this->db2->from('tbl_form_pmb');
        $this->db2->where('prodi', $log);
        $this->db2->where('npm_baru IS NOT NULL', NULL, FALSE);
        $this->db2->where('status > ', 2);
        $this->db2->where_in('npm_baru', $data);
        $this->db2->like('nomor_registrasi', substr($max_angkatan, 2,4), 'after');
        $this->db2->order_by('nomor_registrasi','asc');

        /*$getListMaba = "SELECT * from tbl_form_pmb 
                        where prodi = '".$log."' 
                        and npm_baru IS NOT NULL 
                        and status > 2 
                        and nomor_registrasi like '19%' 
                        order by nomor_registrasi asc";*/

        $data['look'] = $this->db2->get()->result();
        $data['page'] = "feed_maba";
		
		$this->load->view('template', $data);
	}

	function id_maba($id)
	{
		$data['query'] = $this->db->query("SELECT * from tbl_form_camaba where nomor_registrasi = '".$id."'")->row();
		$data['page'] = "feed_idmaba";
		$this->load->view('template', $data);
	}

	function inputdata()
	{
		if ($this->input->post('jk') == 'L') {
			$lamin = '1';
		} else {
			$lamin = '2';
		}

		$fak = $this->db->query("SELECT * from tbl_form_camaba where npm_baru = '".$this->input->post('npm')."'")->row();
		$jur = $fak->prodi;
		$haha= $this->db->query("SELECT * from tbl_jurusan_prodi where kd_prodi = '".$jur."'")->row();
		$faks= $haha->kd_fakultas;
		
		$data = array(
			'nomor_pokok' => $this->input->post('npm'),
			'nama' => $this->input->post('nama'),
			'jenis_kelamin' => $lamin,
			'alamat' => $this->input->post('alamat'),
			'rt' => $this->input->post('rt'),
			'rw' => $this->input->post('rw'),
			'kelurahan' => $this->input->post('kel'),
			'kecamatan' => $this->input->post('kec'),
			'provinsi' => $this->input->post('prov'),
			'handphone' => $this->input->post('hp'),
			'email' => $this->input->post('email'),
			'ayah' => $this->input->post('ayah'),
			'ibu' => $this->input->post('ibu'),
			'fakultas' => $faks,
			'jurusan' => $jur
			);
		//var_dump($data);exit();
		$this->db->insert('tbl_biodata', $data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."sync_feed/maba';</script>";
	}

	function sync_maba_feed()
    {
        $logged = $this->session->userdata('sess_login');

        $max_angkatan = $this->db->query("SELECT max(TAHUNMSMHS) as max from tbl_mahasiswa order by id_mhs desc limit 1")->row()->max;

        
        $cari   = $this->db->query("SELECT NIMHSMSMHS as npm from tbl_mahasiswa 
                                    where KDPSTMSMHS = '".$logged['userid']."' 
                                    and TAHUNMSMHS = '".$max_angkatan."' ")->result();
        
        $this->load->library("Nusoap_lib");
        // $url	= 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url 	= 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;
        $table 	= 'mahasiswa';

        foreach ($cari as $value) {

            $maba   = $this->db2->query("SELECT 
                                            nama,
                                            kelamin,
                                            nik,
                                            agama,
                                            nm_ayah,
                                            nm_ibu,
                                            tgl_lahir,
                                            tempat_lahir,
                                            tlp,
                                            alamat,
                                            npm_baru 
                                        from tbl_form_pmb 
                                        where npm_baru = '".$value->npm."' 
                                        and prodi = '".$logged['userid']."' 
                                        and (nik IS NOT NULL or nik != '') 
                                        and status > 2")->row();

            $smawl = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$maba->npm_baru,'NIMHSMSMHS','asc')->row()->SMAWLMSMHS;

            //1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu
        	switch ($maba->agama) {
        		case 'ISL':
        			$agm = '1';
        			break;
        		case 'HND':
        			$agm = '4';
        			break;
        		case 'KTL':
        			$agm = '3';
        			break;
        		case 'PRT':
        			$agm = '2';
        			break;
        		case 'BDH':
        			$agm = '5';
        			break;
        	}
        	
            //var_dump($maba);exit();
            // $table1 	= 'mahasiswa';
            // $filter 	= "nm_pd ilike '".$maba->nama."%'";
            // $limit 		= 5; // jumlah data yang diambill
            // $offset 	= 0; // offset dipakai untuk paging, contoh: bila $limit=20
            // $result2 	= $proxy->GetRecordset($token, $table1, $filter, $order, $limit, $offset);
            $alm 		= substr($maba->alamat, 0,60);
            $almt 		= strval($alm);

            $record['id_pd']                     = '';
            $record['nm_pd']                     = $maba->nama;
			$record['jk']                        = $maba->kelamin;
			$record['nisn']                      = '0';
			$record['nik']                       = str_replace(' ', '', $maba->nik);
			$record['id_agama']                  = $agm;
			$record['id_kk']                     = '0';
			$record['id_wil']                    = '022200';
			//$record['id_sp'] = $result2['result']['id_sp'];
			$record['nm_ayah']                   = $maba->nm_ayah;
			$record['nm_ibu_kandung']            = $maba->nm_ibu;
			$record['id_kebutuhan_khusus_ibu']   = 0;
			$record['id_kebutuhan_khusus_ayah']  = 0;
			$record['kewarganegaraan']           = 'ID';//ID = indonesia
			$record['a_terima_kps']              = 0;
			$record['ds_kel']                    = $almt;
			$record['tgl_lahir']                 = $maba->tgl_lahir;
			$record['tmpt_lahir']                = $maba->tempat_lahir;
			$record['no_hp']                     = $maba->tlp;
			$record['email']                     = '';

			//var_dump($record);exit();
            $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
            echo "<pre>";
            print_r ($result1['result']);
            echo "</pre>";

            
            $filter2 = "nik ilike '".str_replace(' ', '', $maba->nik)."%'";
            $dbb     = 'mahasiswa_pt';
            $limit   = 5;
            $search  = $proxy->GetRecord($token,'mahasiswa',$filter2);
            $sp      = $proxy->GetRecord($token, 'satuan_pendidikan', "nm_lemb ilike '%bhayangkara jakarta%'");
            $que = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$logged['userid'],'prodi','asc')->row();

        	// $record2['id_reg_pd'] = '';
            $record2['id_pd']            = $search['result']['id_pd'];
            $record2['nipd']             = trim($maba->npm_baru);
			$record2['id_sp']            = $sp['result']['id_sp'];
			$record2['id_sms']           = $que->id_sms;
			$record2['tgl_masuk_sp']     = '2019-08-26';
			$record2['a_pernah_paud']    = '0';
			$record2['a_pernah_tk']      = '0';
			$record2['tgl_create']       = date('Y-m-d');
			$record2['mulai_smt']        = $smawl;

            if ($logged['userid'] == '74101' or $logged['userid'] == '61101') {
                if (substr(trim($maba->npm_baru), 7,1) == 5) {
                    $record2['id_jns_daftar'] = 1;
                } else {
                    $record2['id_jns_daftar'] = 2;
                }
            } else {
                if (substr(trim($maba->npm_baru), 8,1) == 5) {
                    $record2['id_jns_daftar'] = 1;
                } else {
                    $record2['id_jns_daftar'] = 2;
                }
            }

			$hasil = $proxy->InsertRecord($token,$dbb,json_encode($record2));
			echo "<pre>";
            print_r ($hasil['result']);
            echo "</pre>";
            echo "<hr>";

            $this->db2->query("UPDATE tbl_form_pmb set status_feeder = 1 where npm_baru = '".$maba->npm_baru."'");

        }

        //$result = $proxy->InsertRecordset($token, $table,json_encode($records));
    }

    function sync_maba_feed_ikeh()
    {
        $logged = $this->session->userdata('sess_login');
        if ($logged['userid'] == '74101' or $logged['userid'] == '61101') {
        	$cari = $this->db->query("SELECT * from tbl_pmb_s2 a JOIN tbl_mahasiswa b on a.npm_baru=b.NIMHSMSMHS where b.KDPSTMSMHS = '".$logged['userid']."' and (a.nik is NULL or a.nik = '') and a.status > 1")->result();
        } else {
        	$cari = $this->db->query("SELECT * from tbl_form_camaba a JOIN tbl_mahasiswa b on a.npm_baru=b.NIMHSMSMHS where b.KDPSTMSMHS = '".$logged['userid']."' and (a.nik is NULL or a.nik = '') and a.status > 1")->result();
        }
        //var_dump($cari);exit();
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);

        $token = $result;
        $table = 'mahasiswa';

        foreach ($cari as $value) {
        	if ($logged['userid'] == '74101' or $logged['userid'] == '61101') {
        		$mk = $this->db->query("SELECT b.NMMHSMSMHS,a.kelamin,a.nik,a.agama,a.nm_ayah,a.nm_ibu,a.tgl_lahir,a.tempat_lahir,a.tlp,a.alamat,b.SMAWLMSMHS,b.NIMHSMSMHS from tbl_pmb_s2 a join tbl_mahasiswa b on a.npm_baru=b.NIMHSMSMHS where a.npm_baru = '".$value->NIMHSMSMHS."' and b.KDPSTMSMHS = '".$logged['userid']."' and (a.nik is NULL or a.nik = '') and a.status > 1")->row();
        		//$alm = substr($mk->alamat, 0,40);
        	} else {
        		$mk = $this->db->query("SELECT b.NMMHSMSMHS,a.kelamin,a.nik,a.agama,a.nm_ayah,a.nm_ibu,a.tgl_lahir,a.tempat_lahir,a.tlp,a.alamat,b.SMAWLMSMHS,b.NIMHSMSMHS from tbl_form_camaba a join tbl_mahasiswa b on a.npm_baru=b.NIMHSMSMHS where a.npm_baru = '".$value->NIMHSMSMHS."' and b.KDPSTMSMHS = '".$logged['userid']."' and (a.nik is NULL or a.nik = '') and a.status > 1")->row();
        		// $alm = substr($mk->alamat, 0,40);
        	}
        	switch ($mk->agama) {
        		case 'Islam':
        			$agm = '1';
        			break;
        		case 'Hindu':
        			$agm = '4';
        			break;
        		case 'Katolik':
        			$agm = '3';
        			break;
        		case 'Protestan':
        			$agm = '2';
        			break;
        		case 'Budha':
        			$agm = '5';
        			break;
        	}
        	
            // var_dump($mk);exit();
            $table1 = 'mahasiswa';
            $filter = "nm_pd ilike '".$mk->NMMHSMSMHS."%'";
            $limit = 5; // jumlah data yang diambill
            $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
            $result2 = $proxy->GetRecordset($token, $table1, $filter, $order, $limit, $offset);
            $alm = substr($mk->alamat, 0,60);
            $almt= strval($alm);

            $record['id_pd'] = '';
            $record['nm_pd'] = $mk->NMMHSMSMHS;
			$record['jk'] = $mk->kelamin;
			$record['nisn'] = '0';
			$record['nik'] = trim($mk->NIMHSMSMHS);
			$record['id_agama'] = $agm; //1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu
			$record['id_kk'] = '0';
			$record['id_wil'] = '022200';
			//$record['id_sp'] = $result2['result']['id_sp'];
			$record['nm_ayah'] = $mk->nm_ayah;
			$record['nm_ibu_kandung'] = $mk->nm_ibu;
			$record['id_kebutuhan_khusus_ibu'] = 0;
			$record['id_kebutuhan_khusus_ayah'] = 0;
			$record['kewarganegaraan'] = 'ID';//ID = indonesia
			$record['a_terima_kps'] = 0;
			$record['ds_kel'] = $almt;
			$record['tgl_lahir'] = $mk->tgl_lahir;
			$record['tmpt_lahir'] = $mk->tempat_lahir;
			$record['no_hp'] = substr(trim($mk->tlp), 0,12);
			$record['email'] = '';
			//var_dump($record);exit();
            $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
            var_dump($result1);echo "<hr>";

            
            $filter2 = "nik ilike '".$mk->NIMHSMSMHS."%'";
            $dbb = 'mahasiswa_pt';
            $limit = 5;
            $search = $proxy->GetRecord($token,'mahasiswa',$filter2);
            $sp = $proxy->GetRecord($token, 'satuan_pendidikan', "nm_lemb ilike '%bhayangkara jakarta%'");
            $que = $this->db->query("SELECT * from tbl_jurusan_prodi where kd_prodi = '".$logged['userid']."'")->row();
            //var_dump($search);exit();

        	// $record2['id_reg_pd'] = '';
            $record2['id_pd'] = $search['result']['id_pd'];
            $record2['nipd'] = trim($mk->NIMHSMSMHS);
			$record2['id_sp'] = $sp['result']['id_sp'];
			$record2['id_sms'] = $que->id_sms;
			$record2['tgl_masuk_sp'] = '2016-08-29';
			$record2['a_pernah_paud'] = '0'; //1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu
			$record2['a_pernah_tk'] = '0';
			$record2['tgl_create'] = date('Y-m-d');
			$record2['mulai_smt'] = $mk->SMAWLMSMHS;
			$record2['id_jns_daftar'] = 1;
			$hasil = $proxy->InsertRecord($token,$dbb,json_encode($record2));
			var_dump($hasil);echo "<hr>";
            if ($logged['userid'] == '74101' or $logged['userid'] == '61101') {
        		$this->db->query("update tbl_pmb_s2 set status_feeder = 1 where npm_baru = '".$mk->NIMHSMSMHS."'");
        	} else {
        		$this->db->query("update tbl_form_camaba set status_feeder = 1 where npm_baru = '".$mk->NIMHSMSMHS."'");	
        	}
            
        }

        //$result = $proxy->InsertRecordset($token, $table,json_encode($records));
    }

}

/* End of file Maba.php */
/* Location: ./application/modules/sync_feeder/controllers/Maba.php */