<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dropout extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('sess_login')) {
            redirect('auth/logout','refresh');
        } else {
            $this->sess = $this->session->userdata('sess_login');
        }
    }

	public function index()
	{
		$logged = $this->session->userdata('sess_login');
        $pecah = explode(',', $logged['id_user_group']);
        $jmlh = count($pecah);

        for ($i=0; $i < $jmlh; $i++) { 
            $grup[] = $pecah[$i];
        }
        
        if ((in_array(10, $grup))) { //baa
            $data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'sync_feed/v_do_sync_baa';

        } elseif ((in_array(9, $grup))) { // fakultas
            $data['jurusan']=$this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$logged['userid'],'prodi','asc')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'sync_feed/v_do_sync_faks';

        } elseif ((in_array(8, $grup) or in_array(19, $grup))) { // prodi
            $data['jurusan']=$this->db->where('kd_prodi',$logged['userid'])->get('tbl_jurusan_prodi')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'sync_feed/v_do_list_prodi';

        } elseif ((in_array(1, $grup))) { // admin
            $data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'sync_feed/v_do_sync_baa';
        }
		$this->load->view('template', $data);
	}

	function vw()
	{
		$jurusan = $this->input->post('jurusan');
        $tahun = $this->input->post('tahun');

        $this->session->set_userdata('tahun',$tahun);
        $this->session->set_userdata('jurusan',$jurusan);

        redirect(base_url().'sync_feed/Dropout/sync_data_do');
	}

	function sync_data_do()
	{
        if ($this->session->userdata('jurusan')) {
            $data['rows']   = $this->db->query("SELECT * FROM tbl_dropout do 
                                                JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = do.`npm_mahasiswa`
                                                WHERE mhs.`KDPSTMSMHS` = '".$this->session->userdata('jurusan')."' 
                                                AND do.`tahunajaran` = ".$this->session->userdata('tahun')."")->result();

            $data['page']='v_do_list';
            $this->load->view('template', $data);    
        }else{
            redirect(base_url().'sync_feed/Dropout','refresh');
        }
    }

    function syncDropOut()
    {
        ini_set('memori_limit', '512MB');
        // prepare webservice
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url        = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client     = new nusoap_client($url, true);
        $proxy      = $client->getProxy();
        $username   = userfeeder;
        $password   = passwordfeeder;
        $result     = $proxy->GetToken($username, $password);
        $token      = $result;
        $table      = 'mahasiswa_pt';

        // this session below was grab from /data/dropout/saveSessBaa
        $sessdrop = $this->session->userdata('sessforselectbaa');

        $prodi = $sessdrop['jurusan'];
        $thajr = $sessdrop['tahunajaran'];

        // get dropuot student
        $this->db->select('tgl_skep,npm_mahasiswa,alasan');
        $this->db->from('tbl_dropout');
        $this->db->where('tahunajaran', $thajr);
        $this->db->where('audit_user', $prodi);
        $mhs = $this->db->get()->result();

        /*
        echo "<pre>";
        print_r ($mhs);exit();
        echo "</pre>";
        */

        foreach ($mhs as $key) {

        	// deactivated user login
        	$this->db->where('userid', $key->npm_mahasiswa);
        	$this->db->update('tbl_user_login', ['status' => 0]);

            // dikeluarkan
            if ($key->alasan == '2') {
                $jenis_keluar = '3';

            // mengundurkan diri
            } elseif ($key->alasan == '1') {
                $jenis_keluar = '4';
            }

            $data = [
                'tgl_keluar' => $key->tgl_skep,
                'id_jns_keluar' => $jenis_keluar
            ];

            // get id reg pd from feeder
            $idstudent = $proxy->GetRecord($token, $table, "nipd ilike '".$key->npm_mahasiswa."%'");
            // var_dump($idstudent['result']['id_reg_pd']); exit();
            $idregstd  = ['id_reg_pd' => $idstudent['result']['id_reg_pd']];
            $records[] = ['key' => $idregstd, 'data' => $data];
        }

        // update status to dropout
        foreach ($records as $record) {
            $result = $proxy->UpdateRecord($token, $table, json_encode($record));
            echo "<pre>";
            print_r ($result['result']);
            echo "</pre><hr>";
        }
    }
}

/* End of file Dropout.php */
/* Location: ./application/modules/sync_feed/controllers/Dropout.php */