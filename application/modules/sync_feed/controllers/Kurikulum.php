<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kurikulum extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        error_reporting(0);
        if ($this->session->userdata('sess_login') == TRUE) {
            $cekakses = $this->role_model->cekakses(130)->result();
            if ($cekakses != TRUE) {
                echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
            }
        } else {
            redirect('auth','refresh');
        }
    }

	public function index()
	{
		$logged = $this->session->userdata('sess_login');
		$data['aktif'] = $this->db->query("SELECT * from tbl_cpanel where kode_cpanel = 'kurikulum'")->row();
		$data['panel'] = $this->db->query('select * from tbl_cpanel')->row();
		$data['tahunajaran']=$this->app_model->getdata('tbl_tahunajaran', 'id_tahunajaran', 'ASC')->result();
		$data['data_table']=$this->app_model->get_detail_kurikulum_prodi($logged['userid']);
		$data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
		$data['page'] = "feed_kurikulum";
		$this->load->view('template', $data);
	}

	function detail_kurikulum($id)
	{
		$data['kurikulum']=$id;
		$data['data_table1']=$this->app_model->get_matakuliah_kurikulum_new($id, 1);
		$data['data_table2']=$this->app_model->get_matakuliah_kurikulum_new($id, 2);
		$data['data_table3']=$this->app_model->get_matakuliah_kurikulum_new($id, 3);
		$data['data_table4']=$this->app_model->get_matakuliah_kurikulum_new($id, 4);
		$data['data_table5']=$this->app_model->get_matakuliah_kurikulum_new($id, 5);
		$data['data_table6']=$this->app_model->get_matakuliah_kurikulum_new($id, 6);
		$data['data_table7']=$this->app_model->get_matakuliah_kurikulum_new($id, 7);
		$data['data_table8']=$this->app_model->get_matakuliah_kurikulum_new($id, 8);
		$data['data_matakuliah']=$this->app_model->get_matakuliah_baru_new($id);
		$data['page'] = "feed_detail_kurikulum";
		$this->load->view('template', $data);
	}

	function sync_kurikulum($id)
	{

		$logged = $this->session->userdata('sess_login');

		$query 	= $this->db->query("SELECT 
										b.id_sms,
										a.kd_prodi,
										a.kurikulum,
										a.jml_sks 
									from tbl_kurikulum_new a 
									join tbl_jurusan_prodi b on a.kd_prodi = b.kd_prodi 
									where a.kd_prodi = '".$logged['userid']."' 
									and a.kd_kurikulum = '".$id."'")->row();
        
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url 	= 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);

        $token = $result;
        $table = 'kurikulum';

        $filter = "id_sms ilike '".$query->id_sms."%'";

        //die($filter);

        $limit 	= 5; // jumlah data yang diambill
        $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
        $result2 = $proxy->GetRecordset($token, $table, $filter, $order, $limit, $offset);

        //var_dump($result2);die();

        $tsksw = 0; $tsksp = 0;
        for ($i=1; $i < 9; $i++) { 
        	$hasil=$this->app_model->get_matakuliah_kurikulum_new($id, $i);
        	if ($row->status_wajib == 1) { 
        		$tsksw = $tsksw+$row->sks_matakuliah;
        	} else {
        		$tsksp = $tsksp+$row->sks_matakuliah;
        	}
        }

        if ($logged['userid'] == '74101' or $logged['userid'] == '61101') {
        	$lulus = '48';
        } else {
        	$lulus = '144';
        }
        

        $record['id_sms'] 			= $query->id_sms;
        $record['id_jenj_didik'] 	= 99;
		$record['id_smt'] 			= '20171';
		$record['nm_kurikulum_sp'] 	= $query->kurikulum;
		$record['jml_sem_normal'] 	= 8;
		$record['jml_sks_lulus']	= $lulus; //1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu
		$record['jml_sks_wajib'] 	= $tsksw;
		$record['jml_sks_pilihan'] 	= $tsksp;
		//$record['id_sp'] = $result2['result']['id_sp'];
		//var_dump($record);exit();
        $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
        var_dump($result1);echo "<hr>";		
    	
	}

	function sync_mk($kd,$kur)
	{
		$logged = $this->session->userdata('sess_login');

		$cari 	= $this->db->query("SELECT c.id_sms,a.kd_matakuliah,a.*,b.* from tbl_matakuliah a 
                                    join tbl_kurikulum_matkul_new b on a.kd_matakuliah = b.kd_matakuliah 
                                    join tbl_jurusan_prodi c on a.kd_prodi = c.kd_prodi
									where a.kd_prodi = '".$logged['userid']."' 
                                    and a.kd_matakuliah = '".$kd."' 
                                    and c.kd_prodi = '".$logged['userid']."'
									and b.kd_kurikulum = '".$kur."'")->row();
        //var_dump($cari);exit();
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token = $result;

        $table = 'mata_kuliah';

        if ($cari->status_wajib == 1) {
            $sts_mk = 'A';
        }else{
            $sts_mk = 'B';
        }

        $record['id_sms'] 			= $cari->id_sms;
        $record['id_jenj_didik'] 	= 99;
		$record['kode_mk'] 			= str_replace('-', '', trim($cari->kd_matakuliah));
		$record['jns_mk'] 			= $sts_mk;
		$record['sks_mk'] 			= number_format($cari->sks_matakuliah);
        $record['sks_tm']           = number_format($cari->sks_matakuliah);
        $record['sks_prak']         = number_format($cari->sks_praktikum);
        $record['sks_prak_lap']     = number_format($cari->sks_praklap);
        $record['sks_sim']          = number_format($cari->sks_simulasi);
		$record['a_sap']			= 1;
		$record['a_silabus'] 		= 1;
		$record['nm_mk'] 			= strtoupper($cari->nama_matakuliah);
		//$record['id_sp'] = $result2['result']['id_sp'];
		//var_dump($record);exit();
        $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
        var_dump($result1);echo "<hr>";
	}

	function sync_mk_kurikulum($idmk,$idkur)
	{
		$log 	= $this->session->userdata('sess_login');
		$user 	= $log['userid'];

		//getidmk sia & feeder
		$mk 	= $this->db->query("SELECT 
										sks_matakuliah,
										kd_matakuliah,
										nama_matakuliah,
										semester_matakuliah,
										id_sms 
									from tbl_matakuliah a 
									join tbl_jurusan_prodi b on a.kd_prodi = b.kd_prodi 
									where kd_matakuliah = '".$idmk."' 
									and a.kd_prodi = '".$user."'")->row();

		//getidkurikulum sia & feeder
		$kr 	= $this->db->query("SELECT kd_kurikulum,kurikulum from tbl_kurikulum_new where kd_kurikulum = '".$idkur."'")->row();
		
		$mkkr 	= $this->db->query("SELECT status_wajib from tbl_kurikulum_matkul_new 
									where kd_kurikulum = '".$idkur."' 
									and kd_matakuliah = '".$idmk."'")->row();
		
		// var_dump($mk->kd_matakuliah.'-'.$kr->kd_kurikulum);exit();
		//insert & sync to feeder

		$this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url 	= 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;

        $table1 = 'mata_kuliah';
        $filter = "kode_mk = '".str_replace('-', '', trim($mk->kd_matakuliah))."'";
        $limit 	= 10; // jumlah data yang diambill
        $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
        $result2 = $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);
        //var_dump($result2);exit();
        foreach ($result2['result'] as $value1) {
        	if ((substr($mk->kd_matakuliah, 0,4) == 'MKDU') or (substr($mk->kd_matakuliah, 0,3) == 'MKU')) {
        		$id_mk = $value1['id_mk'];

        	} else {
        		if ($value1['id_sms'] == $mk->id_sms) {
	                $id_mk = $value1['id_mk'];
	            }
        	}
        }

        $sp2 = $proxy->GetRecordset($token, 'kurikulum', "nm_kurikulum_sp ilike '%".$kr->kurikulum."%'", $order, 15,0);
        //var_dump($sp2);exit();
        foreach ($sp2['result'] as $value2) {
            if ($value2['id_sms'] == $mk->id_sms) {
                $kuri = $value2['id_kurikulum_sp'];
            }
        }
        //var_dump($kuri);exit();
        $table = 'mata_kuliah_kurikulum';

        $record['id_kurikulum_sp']	= $kuri;
        $record['id_mk'] 			= $id_mk;
		$record['smt'] 				= $mk->semester_matakuliah;
		$record['sks_mk'] 			= $mk->sks_matakuliah;
		$record['sks_tm'] 			= $mk->sks_matakuliah;
		$record['sks_prak']			= '';
		$record['sks_prak_lap'] 	= '';
		$record['sks_sim'] 			= '';
		$record['a_wajib'] 			= $mkkr->status_wajib;
		//var_dump($record);exit();
        $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
        var_dump($result1);echo "<hr>";
	}

}

/* End of file Kurikulum.php */
/* Location: ./application/modules/sync_feed/controllers/Kurikulum.php */