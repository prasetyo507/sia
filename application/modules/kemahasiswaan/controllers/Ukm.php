<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ukm extends CI_Controller {

	public function index()
	{
		$data['page'] = "v_ukm_mhs";
		$this->load->view('template', $data);
	}

}

/* End of file Ukm.php */
/* Location: ./application/modules/kemahasiswaan/controllers/Ukm.php */