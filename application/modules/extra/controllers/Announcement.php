<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('inflector');
		if (!$this->session->userdata('sess_login')) {
			redirect(base_url('auth/logout'));
		}
	}

	public function index()
	{
		$data['announcement'] = $this->db->get('tbl_announcement')->result();
		$data['tahunakademik'] = $this->db->get('tbl_tahunakademik')->result();
		$data['page'] = "v_announcement";
		$this->load->view('template', $data);
	}

	public function upload()
	{
		// get user id
		$user = $this->session->userdata('sess_login');
		$userid = $user['userid'];

		if ($_FILES['userfile']) {

			$sanitizeFileName = $this->security->sanitize_filename($_FILES['userfile']['name']);
			$fileName = rand(1, 10000000) . underscore(str_replace('/', '', $sanitizeFileName));
			
			// var_dump($_FILES['userfile']);exit();

			$isNameValid = explode('.', $fileName);
			if (count($isNameValid) > 2) {
				echo "<script>alert('FORMAT FILE MENCURIGAKAN!');history.go(-1)';</script>";
				exit();
			}

			$config['upload_path']          = './announcement/';
		    $config['allowed_types']        = 'gif|jpg|png|pdf';
		    $config['file_name']            = $fileName;
		    $config['overwrite']			= true;
		    $config['max_size']             = 2048; // 1MB

		    $this->load->library('upload', $config);

		    if (!$this->upload->do_upload('userfile')) {
		        
		        print_r($this->upload->display_errors());
		        exit();

		    } else {

		    	$data['tahunakademik'] = $this->input->post('tahunakademik');
		    	$data['prodi'] = $userid;
		    	$data['filename'] = $fileName;
		    	$data['pathfile'] = $config['upload_path'] . $fileName;
		    	$data['category'] = $this->input->post('category');
		    	$data['note'] = $this->input->post('note');
		    	$data['type'] = $_FILES['userfile']['type'];
		    	$this->db->insert('tbl_announcement', $data);

		    	echo "<script>alert('Berhasil!');history.go(-1);</script>";

		    }
		} else {
			echo "<script>alert('data tidak sesuai!');history.go(-1);</script>";
			exit();
		}
	}

	public function detail($id)
	{
		$this->db->where('id', $id);
		$data['announcement'] = $this->db->get('tbl_announcement')->row();
		$this->load->view('modal_announcement', $data);
	}

}

/* End of file Announcement.php */
/* Location: .//tmp/fz3temp-1/Announcement.php */