<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <title>Dashboard - Siakad UBJ</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/pages/dashboard.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url();?>assets/css/pages/reports.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->

    <script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script> 

</head>

<body>

<div class="navbar navbar-fixed-top">

  <div class="navbar-inner">

    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span

                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#"><img src="<?php echo base_url();?>assets/logo.png" style="width:40px;float:left;margin-top:-8px;margin-bottom:-13px">

            <span style="margin-left:10px;line-height:-15px">SISTEM INFORMASI AKADEMIK UBHARA JAYA</span></a>

      <div class="nav-collapse">

        <ul class="nav pull-right">

          <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size:16px">

              <?php $user = $this->session->userdata('sess_login');

              if ($user['user_type'] == 1) {

                $nama = $this->app_model->getdetail('tbl_karyawan','nid',$user['userid'],'nik','asc')->row(); $name = $nama->nama;

              } elseif($user['user_type'] == 3) {

                $nama = $this->app_model->getdetail('tbl_divisi','kd_divisi',$user['userid'],'kd_divisi','asc')->row(); $name = $nama->divisi;

              } else {

                $nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$user['userid'],'NIMHSMSMHS','asc')->row(); $name = $nama->NMMHSMSMHS;

              }

               ?>

              <i class="icon-user"></i> <?php echo $name; ?> <b class="caret"></b></a>

            <ul class="dropdown-menu">

              <li><a href="<?php echo base_url();?>extra/account">Ganti Password</a></li>

              <li><a href="<?php echo base_url();?>auth/logout">Logout</a></li>

            </ul>

          </li>

        </ul>

      </div>

      <!--/.nav-collapse --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /navbar-inner --> 

</div>

<!-- /navbar -->

<div class="subnavbar">
</div>

<!-- /subnavbar -->

<div class="main">

  <div class="main-inner">

    <div class="container">

      <!-- load page -->

      <script type="text/javascript">

        function checkPass()
        {
            var pass1 = document.getElementById('pass1');
            var pass2 = document.getElementById('pass2');
            var message = document.getElementById('confirmMessage');
            //Set the colors we will be using ...
            var goodColor = "#66cc66";
            var badColor = "#ff6666";
            if(pass1.value == pass2.value){
                pass2.style.backgroundColor = goodColor;
                message.style.color = goodColor;
                document.getElementById('save').disabled = false;
                message.innerHTML = "Passwords Match!"
            }else{
                pass2.style.backgroundColor = badColor;
                message.style.color = badColor;
                document.getElementById('save').disabled = true;
                message.innerHTML = "Passwords Do Not Match!"
            }
        }  

        </script>

        <?php $user = $this->session->userdata('sess_login'); ?>

        <div class="row">
          <div class="span12">                
              <div class="widget ">
                <div class="widget-header">
                  <i class="icon-user"></i>
                  <h3>Ubah Password</h3>
              </div> <!-- /widget-header -->
              
              <div class="widget-content">
                <div class="span11">
                  <b><center>Ubah Password Akun</center></b><br>
                  <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>extra/account/simpan">
                    <fieldset>
                      <div class="alert alert-info"><strong>Perhatian ! </strong>Ganti Password default anda untuk meningkatkan keamanan akun anda.</div>
                      <div class="control-group">                     
                        <label class="control-label">Password Lama: </label>
                        <div class="controls">
                          <input type="password" class="span3" name="old" placeholder="Lama" required>
                        </div> <!-- /controls -->       
                      </div> <!-- /control-group -->
                      <div class="control-group">                     
                        <label class="control-label">Password Baru: </label>
                        <div class="controls">
                          <input type="password" class="span3" name="new" placeholder="Baru" id="pass1" required>
                        </div> <!-- /controls -->       
                      </div> <!-- /control-group -->
                      <div class="control-group">                     
                        <label class="control-label">Ulangi *: </label>
                        <div class="controls">
                          <input type="password" class="span3" placeholder="Ketik Ulang" id="pass2" onkeyup="checkPass(); return false;" required>
                          <span id="confirmMessage" class="confirmMessage"></span>
                        </div> <!-- /controls -->       
                      </div> <!-- /control-group -->
                      <div class="form-actions">
                        <input type="submit" class="btn btn-primary" id="save" value="Ubah Password"/> 
                        <input type="reset" class="btn btn-warning" value="Reset"/>
                      </div> <!-- /form-actions -->
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

<!-- /main -->

<div class="footer">

  <div class="footer-inner">

    <div class="container">

      <div class="row">

        <div class="span12"> &copy; <?php date_default_timezone_set('Asia/Jakarta');echo date('Y'); ?> - <a target="_blank" href="http://ubharajaya.ac.id/">Universitas Bhayangkara Jakarta Raya</a>. <b><i>Last Login : <?php $cek = $this->app_model->getdetail('tbl_user_login','userid',$user['userid'],'userid','asc')->row()->last_login; echo $cek; ?></i></b></div>

        <!-- /span12 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /footer-inner --> 

</div>

<!-- /footer --> 

<!-- Le javascript

================================================== --> 

<!-- Placed at the end of the document so the pages load faster -->  

<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>assets/js/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/js/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">

    $(function() {

        $("#example1").dataTable();

        $("#example3").dataTable();

        $("#example4").dataTable();

        $("#example5").dataTable();

        $('#example2').dataTable({

            "bPaginate": true,

            "bLengthChange": true,

            "bFilter": true,

            "bSort": true,

            "bInfo": true,

            "bAutoWidth": true

        });

    });

</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93675871-1', 'auto');
  ga('send', 'pageview');

</script>

</body>

</html>

