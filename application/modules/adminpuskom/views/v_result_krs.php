<?php $user = $this->session->userdata('sess_mbalia'); ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Ubah Password</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>adminpuskom/board">Home</a> / <a href="<?php echo base_url(); ?>adminpuskom/hapus_krs">hapus krs</a> / <a href="#">krs</a>
					<b><center>Hapus KRS Bermasalah</center></b><br>
					<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>adminpuskom/cari_pw/cari">
						<fieldset>
							<div class="control-group">											
								<label class="control-label">NPM : </label>
								<div class="controls">
									<input type="text" class="span3" value="<?php echo $suit->npm_mahasiswa; ?>" placeholder="Masukan NPM" disabled="">
								</div> <!-- /controls -->				
							</div> <!-- /control-group -->
							<div class="control-group">											
								<label class="control-label">NAMA : </label>
								<div class="controls">
									<input type="text" class="span3" value="<?php echo $suit->NMMHSMSMHS; ?>" placeholder="Masukan NPM" disabled="">
								</div> <!-- /controls -->				
							</div>
							<br>
							<div class="row">
								<div class="span11">
				                    <table id="" class="table table-bordered table-striped">
				                        <thead>
				                            <tr> 
				                                <th>No</th>
				                                <th>NPM</th>
				                                <th>Kode Matakuliah</th>
				                                <th>Nama Matakuliah</th>
				                                <th>Semester</th>
				                                <th>Dosen</th>
				                                <th width="118">Aksi</th>
				                            </tr>
				                        </thead>
				                        <tbody>
				                            <?php $no=1; foreach ($saw as $row) { ?>
				                            <tr>
				                                <td><?php echo $no; ?></td>
				                                <td><?php echo $row->npm_mahasiswa; ?></td>
				                                <td><?php echo $row->kd_matakuliah;?></td>
				                                <td><?php echo $row->nama_matakuliah;?></td>
				                                <td><?php echo $row->semester_krs;?></td>
				                                <td><?php echo $row->nama; ?></td>
				                                <td class="td-actions">
				                                    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>adminpuskom/hapus_krs/delete/<?php echo $row->id_krs;?>"><i class="btn-icon-only icon-remove"> </i></a>
				                                </td>
				                            </tr>
				                            <?php $no++; } ?>
				                        </tbody>
				                    </table>
				                </div>
				            </div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>