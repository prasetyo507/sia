<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gantinama extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_mbalia') != TRUE) {
		 	redirect('adminpuskom/home','refresh');
		}
	}
	
	public function index()
	{
		$data['page'] = 'v_nama';
		$this->load->view('template', $data);
	}

	function load()
	{
		$gen = $this->input->post('jenis');
		$ih = $this->input->post('aww');
		$this->session->set_userdata('idn', $ih);
		if ($gen == 'mhs') {
			$data['kueh'] = $this->db->query('SELECT * from tbl_mahasiswa where NIMHSMSMHS = "'.$this->session->userdata('idn').'"')->row();
			if ($data['kueh'] == FALSE) {
				echo "<script>alert('Akun Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."adminpuskom/gantinama';</script>";
			} else {
				$data['page'] = 'v_nama_mhs';
				$this->load->view('template', $data);
			}

		} else {
			$data['kry'] = $this->db->query('SELECT * from tbl_karyawan where nid = "'.$this->session->userdata('idn').'"')->row();
			if ($data['kry'] == FALSE) {
				echo "<script>alert('Akun Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."adminpuskom/gantinama';</script>";
			} else {
				$data['page'] = 'v_nama_kry';
				$this->load->view('template', $data);
			}
			
		}
		
	}

	function ganti_mhs()
	{
		$this->db->query('UPDATE tbl_mahasiswa set NMMHSMSMHS = "'.$this->input->post('xxx').'" where NIMHSMSMHS = "'.$this->session->userdata('idn').'"');
		echo "<script>alert('Rubah Nama Berhasil !');document.location.href='".base_url()."adminpuskom/gantinama';</script>";
	}

	function ganti_kry()
	{
		$this->db->query('UPDATE tbl_karyawan set nama = "'.$this->input->post('www').'" where nid = "'.$this->session->userdata('idn').'"');
		echo "<script>alert('Rubah Nama Berhasil !');document.location.href='".base_url()."adminpuskom/gantinama';</script>";
	}

	function load_dosen_autocomplete(){

		$bro = $this->session->userdata('sess_login');

		$prodi   = $bro['userid'];

        $this->db->distinct();

        $this->db->select("a.id_kary,a.nid,a.nama");

        $this->db->from('tbl_karyawan a');

        $this->db->join('tbl_jadwal_matkul b', 'a.nid = b.kd_dosen');

        $this->db->like('b.kd_jadwal', $prodi, 'after');

        $this->db->like('a.nama', $_GET['term'], 'both');

        $this->db->or_like('a.nid', $_GET['term'], 'both');

        $sql  = $this->db->get();

        // $sql = $this->db->query("select distinct a.id_kary,a.nid,a.nama from tbl_karyawan a 
								// join tbl_jadwal_matkul b on a.nid = b.kd_dosen
								// where (nid LIKE '%".$_GET['term']."%' or nama like '%".$_GET['term']."%') and b.kd_jadwal like '".$prodi."%'");

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

                            // 'kode'       => $row->kd_obat,

                            // 'satuan'     => $row->nm_satuan,

                            // 'stok'       => $row->stok,

                            'id_kary'       => $row->id_kary,

                            'nid'           => $row->nid,

                            'value'         => $row->nid.' - '.$row->nama

                            // 'diskon'     => $row->diskon,

                            // 'label'      => $row->kd_obat . " " . $row->nm_obat,

                            );

        }

        echo json_encode($data);

    }

}

/* End of file Gantinama.php */
/* Location: ./application/modules/adminpuskom/controllers/Gantinama.php */