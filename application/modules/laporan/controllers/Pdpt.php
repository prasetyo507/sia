<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdpt extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		// error_reporting(0);
		// //$this->load->library('Cfpdf');
		// if ($this->session->userdata('sess_login') == TRUE) {
		// 	$cekakses = $this->role_model->cekakses(...)->result();
		// 	if ($cekakses != TRUE) {
		// 		echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
		// 	}
		// } else {
		// 	redirect('auth','refresh');
		// }
	}

	public function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
        $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'laporan/pdpt_select';
	    $this->load->view('template', $data);
	}	

	function get_jurusan($id){
		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function simpan_sesi()
	{
		$fakultas = $this->input->post('fakultas');
		$jurusan = $this->input->post('jurusan');
        $tahunajaran = $this->input->post('tahunajaran');
		$angkatan = $this->input->post('angkatan');  
		//var_dump($jurusan);exit();
        $this->session->set_userdata('tahunajaran', $tahunajaran);
		$this->session->set_userdata('jurusan', $jurusan);
		$this->session->set_userdata('fakultas', $fakultas);
		$this->session->set_userdata('angkatan', $angkatan);
		redirect(base_url('laporan/pdpt/load_laporan'));
	}

	function load_laporan()
	{
		$data['page'] = 'laporan/pdpt_view';
	    $this->load->view('template', $data);
	}
}

/* End of file Pdpt.php */
/* Location: ./application/modules/laporan/controllers/Pdpt.php */