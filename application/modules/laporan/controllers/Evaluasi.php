<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluasi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '512M');
		error_reporting(0);
		$logged = $this->session->userdata('sess_login');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(97)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}

		$this->edom = $this->load->database('eval', TRUE);
		
	}

	public function index()
	{
		$data['tahunajar'] 	= $this->app_model->getdata('tbl_tahunakademik','kode','asc')->result();

		$data['prodi'] 		= $this->app_model->getdata('tbl_jurusan_prodi','kd_prodi','asc')->result();

		$data['fakultas'] 	= $this->app_model->getdata('tbl_fakultas','kd_fakultas','asc')->result();
		//var_dump($data);exit();
		$data['page'] = 'laporan/evluasi_select';
		$this->load->view('template', $data);
	}

	function savesess()
	{
		$this->session->set_userdata('prod', $this->input->post('prodi', TRUE));
		$this->session->set_userdata('ta',$this->input->post('tahunajaran', TRUE));
		redirect(base_url('laporan/evaluasi/view'));
	}

	// new

	function savefaculty()
	{
		extract(PopulateForm());
		$this->session->set_userdata('evalforfac',$tahunajaran);
		redirect(base_url('laporan/evaluasi/listfaculty'));
	}

	function listfaculty()
	{
		$data['sesi'] 		= $this->session->userdata('evalforfac');
		$data['faculty'] 	= $this->app_model->getdata('tbl_fakultas','kd_fakultas','asc')->result();
		$data['page']		= "vlistprodieval";
		$this->load->view('template', $data);
	}

	function perProdi($id)
	{
		$data['arry'] = $id;
		$data['sesi'] = $this->session->userdata('evalforfac');
		$data['prod'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$id,'kd_prodi','asc')->result();
		$data['page'] = "vdetilprodieval";
		$this->load->view('template', $data);
	}

	function perPoin($id)
	{
		$data['sesi'] = $this->session->userdata('evalforfac');

		$this->edom = $this->load->database('eval', TRUE);
		$this->edom->distinct();
		$this->edom->select('topik,parameter,bobot,id_parameter');
		$this->edom->from('tbl_parameter a');
		$this->edom->join('tbl_topik_parameter b', 'a.kd_topik = b.kd_topik');
		$data['getData'] = $this->edom->get()->result();

		$data['prodi'] = $id;
		
		$this->session->set_userdata('idjadwal',$id);

		$data['page'] = 'laporan/vdetailpoinprodi';
		$this->load->view('template', $data);
	}

	function excelListFaculty()
	{
		$data['sesi'] 		= $this->session->userdata('evalforfac');
		$data['faculty'] 	= $this->app_model->getdata('tbl_fakultas','kd_fakultas','asc')->result();
		$this->load->view('export_list_prodi', $data);
	}

	function excelListProdi($id)
	{
		$data['arr']  = $id;
		$data['sesi'] = $this->session->userdata('evalforfac');
		$data['prod'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$id,'kd_prodi','asc')->result();
		$this->load->view('exportperprodi', $data);
	}

	function excelPoin($id)
	{
		$data['sesi'] = $this->session->userdata('evalforfac');

		$this->edom = $this->load->database('eval', TRUE);
		$this->edom->distinct();
		$this->edom->select('topik,parameter,bobot,id_parameter');
		$this->edom->from('tbl_parameter a');
		$this->edom->join('tbl_topik_parameter b', 'a.kd_topik = b.kd_topik');
		$data['getData'] = $this->edom->get()->result();

		$data['prodi'] = $id;
		
		$this->session->set_userdata('idjadwal',$id);

		$this->load->view('exportpoin', $data);
	}

	// end new

	function view()
	{
		$logs = $this->session->userdata('sess_login');
		if ($logs['userid'] == 'LPM') {
			$loging = $this->session->userdata('prod');
			$this->session->set_userdata('sesi_buat_viewdetil', $loging );
			$data['prodi'] = $loging;
		} else {
			$logxx = $this->session->userdata('sess_login');
			$loging= $logxx['userid'];
			$data['prodi'] = $logxx['userid'];
		}
		
		$sql = "SELECT DISTINCT b.kd_dosen,
											(SELECT SUM(mk.`sks_matakuliah`) AS sks FROM tbl_jadwal_matkul jdl
											JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
											WHERE jdl.`kd_dosen` = b.`kd_dosen` 
											AND jdl.kd_tahunajaran = '".$this->session->userdata('ta')."' 
											AND mk.kd_prodi = '".$loging."'
											AND jdl.kd_jadwal LIKE '".$loging."%' 
											AND (mk.`nama_matakuliah` NOT LIKE 'skripsi%' 
												AND mk.`nama_matakuliah` NOT LIKE '%tesis%' 
												AND mk.`nama_matakuliah` NOT LIKE '%kuliah kerja%'  
												AND mk.`nama_matakuliah` NOT LIKE '%kerja praktek%' 
												AND mk.`nama_matakuliah` NOT LIKE '%magang%')
											) AS sks 
				FROM tbl_jadwal_matkul b
				JOIN tbl_matakuliah d ON b.`kd_matakuliah` = d.`kd_matakuliah`
				WHERE b.`kd_jadwal` LIKE '".$loging."%' 
				AND b.`kd_tahunajaran` = '".$this->session->userdata('ta')."'
				AND d.kd_prodi = '".$loging."'
				AND (d.`nama_matakuliah` NOT LIKE 'skripsi%' 
					AND d.`nama_matakuliah` NOT LIKE '%tesis%' 
					AND d.`nama_matakuliah` NOT LIKE '%kuliah kerja%' 
					AND d.`nama_matakuliah` NOT LIKE '%kerja praktek%' 
					AND d.`nama_matakuliah` NOT LIKE '%magang%')";

		// $this->session->set_userdata('ta',$this->input->post('tahunajaran', TRUE));
		$data['getData'] = $this->db->query($sql)->result();

		// var_dump($loging);exit();

		$this->session->set_userdata('year', $this->session->userdata('ta'));
		$data['page'] = 'laporan/evluasi_view';
		$this->load->view('template', $data);
	}

	function view_detail($nid)
	{
		$data['kry'] = $this->db->query("select * from tbl_karyawan where nid = '".$nid."'")->row();	

		$this->session->set_userdata('nama_dosen',$data['kry']->nama);

		$logged = $this->session->userdata('sess_login');
		if ($logged['userid'] == 'LPM') {
			$logx = $this->session->userdata('sesi_buat_viewdetil');
		} else {
			$logx = $logged['userid'];
		}

		// var_dump($logx.'--------------'.$this->session->userdata('ta'));exit();

		$sql = "SELECT DISTINCT a.`kd_matakuliah`,b.`nama_matakuliah`,a.`kd_jadwal`,a.`id_jadwal`,a.`kelas`,
				b.`sks_matakuliah` 
				FROM tbl_jadwal_matkul a
				JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
				WHERE a.`kd_tahunajaran` = '".$this->session->userdata('ta')."' 
				AND a.`kd_jadwal` LIKE '".$logx."%' AND a.`kd_dosen` = '".$nid."' AND b.`kd_prodi` = '".$logx."'";

		$data['getData'] = $this->db->query($sql)->result();
		
		$this->session->set_userdata('dosen', $nid );
		$data['page'] = 'laporan/evluasi_detail';
		$this->load->view('template', $data);
	}

	function view_eval($id)
	{
		$data['kdjadwal'] = $this->db->query("SELECT kd_jadwal,kelas FROM tbl_jadwal_matkul WHERE id_jadwal = ".$id."")->row();

		switch ($this->session->userdata('ta')) {
			case '20171':
				# get data nilai
				$data['getData'] 	= $this->edom->query("SELECT AVG(c.nilai) as nilai,a.parameter from tbl_parameter a
													join tbl_nilai_parameter c on a.id_parameter = c.parameter_id
													where kd_jadwal = '".$data['kdjadwal']->kd_jadwal."' GROUP BY a.parameter")->result();

				#get data saran
				$data['getData2'] = $this->edom->query("SELECT saran FROM tbl_pengisian_kuisioner WHERE kd_jadwal = '".$data['kdjadwal']->kd_jadwal."'")->result();
				break;
			
			default:
				# get data nilai
				$data['getData'] 	= $this->edom->query("SELECT AVG(c.nilai) as nilai,a.parameter from tbl_parameter a
													join tbl_nilai_parameter_".$this->session->userdata('ta')." c on a.id_parameter = c.parameter_id
													where kd_jadwal = '".$data['kdjadwal']->kd_jadwal."' GROUP BY a.parameter")->result();

				#get data saran
				$data['getData2'] = $this->edom->query("SELECT saran FROM tbl_pengisian_kuisioner_".$this->session->userdata('ta')." 
														WHERE kd_jadwal = '".$data['kdjadwal']->kd_jadwal."'")->result();
				break;
		}

		$this->session->set_userdata('idjadwal',$id);
		$data['page'] = 'laporan/evluasi_detail_view';
		$this->load->view('template', $data);
	}

	function export_evl_dosen()
	{
		$logged = $this->session->userdata('sess_login');
		
		if ($logged['userid'] == 'LPM') {
			$loc = $this->session->userdata('sesi_buat_viewdetil');
		} else {
			$loc = $logged['userid'];
		}
		$data['prodi'] = $loc; 
		$sql = "SELECT DISTINCT 
					b.kd_dosen,
					(SELECT SUM(mk.`sks_matakuliah`) AS sks FROM tbl_jadwal_matkul jdl
					JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
					WHERE jdl.`kd_dosen` = b.`kd_dosen` 
					AND jdl.kd_tahunajaran = '".$this->session->userdata('ta')."' 
					AND mk.kd_prodi = '".$loc."'
					AND jdl.kd_jadwal LIKE '".$loc."%' 
					AND (mk.`nama_matakuliah` NOT LIKE 'skripsi%' 
						AND mk.`nama_matakuliah` NOT LIKE '%tesis%' 
						AND mk.`nama_matakuliah` NOT LIKE '%kuliah kerja%'  
						AND mk.`nama_matakuliah` NOT LIKE '%kerja praktek%' 
						AND mk.`nama_matakuliah` NOT LIKE '%magang%')
					) AS sks 
				FROM tbl_jadwal_matkul b
				JOIN tbl_matakuliah d ON b.`kd_matakuliah` = d.`kd_matakuliah`
				WHERE b.`kd_jadwal` LIKE '".$loc."%' 
				AND b.`kd_tahunajaran` = '".$this->session->userdata('ta')."'
				AND d.kd_prodi = '".$loc."' 
				AND (d.`nama_matakuliah` NOT LIKE 'skripsi%' 
					AND d.`nama_matakuliah` NOT LIKE '%tesis%' 
					AND d.`nama_matakuliah` NOT LIKE '%kuliah kerja%' 
					AND d.`nama_matakuliah` NOT LIKE '%kerja praktek%' 
					AND d.`nama_matakuliah` NOT LIKE '%magang%')";

		$data['catch'] = $this->db->query($sql)->result();
		$this->load->view('export_eval_dosen', $data);
	}

	function export_per_dosen()
	{
		$logged = $this->session->userdata('sess_login');

		if ($logged['userid'] == 'LPM') {
			$lok = $this->session->userdata('sesi_buat_viewdetil');
		} else {
			$lok = $logged['userid'];
		}

		$data['tahunakad'] = $this->session->userdata('ta');

		$data['prodi'] = $lok;
		$data['kry'] 	= $this->db->query("SELECT * from tbl_karyawan 
											where nid = '".$this->session->userdata('dosen')."'
											")->row();

		$data['eval'] 	= $this->db->query("SELECT DISTINCT 
												a.`kd_matakuliah`,
												b.`nama_matakuliah`,
												a.`kd_jadwal`,
												a.`id_jadwal`,
												a.`kelas`,
												b.`sks_matakuliah` 
											FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											WHERE a.`kd_tahunajaran` = '".$this->session->userdata('ta')."'
											AND a.`kd_jadwal` LIKE '".$lok."%' 
											AND a.`kd_dosen` = '".$this->session->userdata('dosen')."' 
											AND b.`kd_prodi` = '".$lok."'")->result();

		$this->load->view('eport_eval_per_dosen', $data);
	}

	function export_hasil()
	{
		$data['kdjadwal'] 	= $this->db->query("SELECT kd_jadwal,kelas FROM tbl_jadwal_matkul 
												WHERE id_jadwal = ".$this->session->userdata('idjadwal')."
												")->row();

		$this->edom = $this->load->database('eval', TRUE);
		$this->edom->distinct();
		$this->edom->select('topik,parameter,bobot,id_parameter');
		$this->edom->from('tbl_parameter a');
		$this->edom->join('tbl_topik_parameter b', 'a.kd_topik = b.kd_topik');
		
		$data['getData'] = $this->edom->get()->result();
		$this->load->view('export_hasil_eval', $data);
	}

	function export_saran()
	{
		$year = $this->session->userdata('ta');
		$this->edom = $this->load->database('eval', TRUE);
		$data['kdjadwal'] = $this->db->query("SELECT kd_jadwal,kelas FROM tbl_jadwal_matkul WHERE id_jadwal = ".$this->session->userdata('idjadwal')."")->row();
		if ($this->session->userdata('ta') == '20171') {
			$data['getData2'] = $this->edom->query("SELECT * FROM tbl_pengisian_kuisioner_20171 WHERE kd_jadwal = '".$data['kdjadwal']->kd_jadwal."'")->result();
		} else {
			$data['getData2'] = $this->edom->query("SELECT * FROM tbl_pengisian_kuisioner_".$year." WHERE kd_jadwal = '".$data['kdjadwal']->kd_jadwal."'")->result();
		}
		$this->load->view('export_saran', $data);
	}

	function export_eval()
	{
		$this->load->model('temph_model');
		$this->load->library('excel');
		$data['averageprodi'] = $this->edom->query('SELECT * from tbl_topik_parameter order by id_topik asc')->result();
		$this->load->view('excel_eval',$data);
	}

}

/* End of file Evaluasi.php */
/* Location: ./application/modules/laporan/controllers/Evaluasi.php */