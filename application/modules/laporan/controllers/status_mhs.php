<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jumlah_mhs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(71)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}

	}

	public function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();

        $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

        $data['chart_pie'] = $this->db->query('SELECT prd.`prodi`, COUNT(mhs.`KDPSTMSMHS`) AS jml FROM tbl_mahasiswa mhs
												JOIN tbl_verifikasi_krs krs ON mhs.`NIMHSMSMHS` = krs.`npm_mahasiswa`
												JOIN tbl_jurusan_prodi prd ON mhs.`KDPSTMSMHS` = prd.`kd_prodi`
												GROUP BY mhs.`KDPSTMSMHS`')->result();

		$data['page'] = 'jumlah_mhs_view';
		$this->load->view('template', $data);

		//$this->load->view('jumlah_mhs_view', $data);
	}

	function get_chart(){
		
	}

}

/* End of file jumlah_mhs.php */
/* Location: ./application/controllers/jumlah_mhs.php */