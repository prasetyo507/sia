<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Npm_maba extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(100)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-
					1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	function index()
	{
		$data['page'] = "npm_maba_view";
		$this->load->view('template', $data);
	}

}

/* End of file Npm_maba.php */
/* Location: ./application/modules/laporan/controllers/Npm_maba.php */