<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('excel');
		$this->load->model('setting_model');
	    error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(58)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$this->session->unset_userdata('tahun');
		$this->session->unset_userdata('jurusan');

		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		
		if ((in_array(10, $grup))) { //baa
			$data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

			$data['page'] = 'laporan/vlaporan_select_baa';

		} elseif ((in_array(9, $grup))) { // fakultas
			$data['jurusan']=$this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$logged['userid'],'prodi','asc')->result();
			$data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

			$data['page'] = 'laporan/vlaporan_select_fak';

		} elseif ((in_array(8, $grup))) { // prodi
			$data['jurusan']=$this->db->where('kd_prodi',$logged['userid'])->get('tbl_jurusan_prodi')->result();
			$data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

			$data['page'] = 'laporan/vlaporan_select_prodi';

		} elseif ((in_array(1, $grup))) { // admin
			$data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

			$data['page'] = 'laporan/vlaporan_select_baa';
		}

		$this->load->view('template',$data);
	}

	public function save_session(){
		$jur = $this->input->post('jurusan');
		$thn = $this->input->post('tahun');
		$lap = $this->input->post('laporan');

		if ($thn < 2015) {
			echo "<script>alert('Data Tidak Tersedia');
              				document.location.href='".base_url()."laporan';</script>";
		}else{
			$this->session->set_userdata('tahun',$thn);
			$this->session->set_userdata('jurusan',$jur);

			if ($lap == 1) {
				redirect(base_url().'laporan/rekap_jml_mhs','refresh');
			}elseif ($lap == 2) {
				redirect(base_url().'laporan/jml_kelas','refresh');
			}elseif ($lap == 3) {
				$this->beban_dosen();
			}elseif ($lap == 4) {
				redirect(base_url().'laporan/rekap_jml_mhs','refresh');
			}
		}
	}

	function rekap_jml_mhs(){
		$thn = $this->session->userdata('tahun');
		$jurus = $this->session->userdata('jurusan');


		//die($jur);

		$jurusan = explode('_', $jurus);

		$data['angkatan'] = substr($thn, 0,4)-7;
		$data['thn'] = $thn;
		$data['jur'] = $jurusan[0];
		$jur = $jurusan[0];



		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ($jur == 99) {
			if ((in_array(10, $grup))) { //baa
				$fak = $this->input->post('fakultas');
				$data['fak'] = $fak;

				$data['arr_jur'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$fak,'prodi','asc')->result();

				$this->load->view('laporan/print/beban_dosen_by_fakultas',$data);

			} elseif ((in_array(9, $grup))) { //fakultas
				$data['fak'] = $logged['userid'];
				
				$data['arr_jur'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$logged['userid'],'prodi','asc')->result();

				$this->load->view('laporan/print/beban_dosen_by_fakultas',$data);				

			}elseif ((in_array(1, $grup))) { //admin

				$fak = $this->input->post('fakultas');
				$data['fak'] = $fak;

				$data['arr_jur'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$fak,'prodi','asc')->result();

				$this->load->view('laporan/print/beban_dosen_by_fakultas',$data);
			}

		}else{

			if ((in_array(8, $grup))) { //prodi
				$data['jur'] = $logged['userid'];


				$data['aktif']=$this->db->query("SELECT krs.`kelas`,COUNT(krs.`npm_mahasiswa`) AS jml_mhs FROM tbl_mahasiswa mhs
												JOIN tbl_verifikasi_krs krs ON mhs.`NIMHSMSMHS` = krs.`npm_mahasiswa`
												WHERE krs.`tahunajaran` = '".$thn."' AND mhs.`KDPSTMSMHS` = '".$logged['userid']."'
												GROUP BY krs.`kelas`
												")->result();

				$data['cuti']=$this->db->query("SELECT mhs.`KDPSTMSMHS`,COUNT(mhs.`NIMHSMSMHS`) AS cuti FROM tbl_mahasiswa mhs
												WHERE mhs.`KDPSTMSMHS` = '55201' AND mhs.`STMHSMSMHS` = 'C'
												GROUP BY mhs.`KDPSTMSMHS`")->row()->cuti;

				$data['non']=$this->db->query("SELECT mhs.`KDPSTMSMHS`,COUNT(mhs.`NIMHSMSMHS`) AS non FROM tbl_mahasiswa mhs
												WHERE mhs.`KDPSTMSMHS` = '55201' AND mhs.`STMHSMSMHS` = 'N'
												GROUP BY mhs.`KDPSTMSMHS`")->row()->non;

				$this->load->view('laporan/print/phpexcel_mhs',$data);

			} else { // fakultas,baa,admin
				
				$data['aktif']=$this->db->query("SELECT krs.`kelas`,COUNT(krs.`npm_mahasiswa`) AS jml_mhs FROM tbl_mahasiswa mhs
												JOIN tbl_verifikasi_krs krs ON mhs.`NIMHSMSMHS` = krs.`npm_mahasiswa`
												WHERE krs.`tahunajaran` = '".$thn."' AND mhs.`KDPSTMSMHS` = '".$jur."'
												GROUP BY krs.`kelas`
												")->result();

				
				$data['cuti']=$this->db->query("SELECT mhs.`KDPSTMSMHS`,COUNT(mhs.`NIMHSMSMHS`) AS cuti FROM tbl_mahasiswa mhs
												WHERE mhs.`KDPSTMSMHS` = '".$jur."' AND mhs.`STMHSMSMHS` = 'C'
												GROUP BY mhs.`KDPSTMSMHS`")->row()->cuti;

				$thn_filter = substr($thn, 0,4);
				$aa = $thn_filter-1;
				$data['non']=$this->db->query("SELECT DISTINCT NIMHSMSMHS FROM tbl_mahasiswa 
												WHERE NIMHSMSMHS NOT IN (
												    SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = '".$thn."'
												) AND NIMHSMSMHS NOT IN (
												    SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = '".$thn."' AND validate = 1
												) AND KDPSTMSMHS = '".$jur."' AND BTSTUMSMHS >= ".$thn." AND STMHSMSMHS != 'L' 
													AND STMHSMSMHS != 'D' AND STMHSMSMHS != 'K' AND TAHUNMSMHS < ".$aa."")->row()->non;


				$this->load->view('laporan/print/phpexcel_mhs',$data);	
			}							
		}
		
	}

	function beban_dosen(){
		$thn = $this->session->userdata('tahun');
		$jurus = $this->session->userdata('jurusan');

		//die($jur);

		$jurusan = explode('_', $jurus);

		$data['thn'] = $thn;
		$data['jur'] = $jurusan[0];
		$jur = $jurusan[0];

		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		//var_dump($pecah);die();

		if ($jurusan[0] == 99) {
			if ((in_array(10, $grup))) { //baa
				$fak = $jurusan[1];
				$data['fak'] = $this->db->where('kd_fakultas', $fak)->get('tbl_jurusan_prodi')->row();
				$data['arr_jur'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$fak,'prodi','asc')->result();
				$data['title'] = $this->db->where('kd_fakultas', $fak)->get('tbl_fakultas')->row();
				//$this->load->view('laporan/print/beban_dosen_by_fakultas',$data);
				$this->load->view('laporan/print/phpexcel_bebanajar_fak',$data);

			} elseif ((in_array(9, $grup))) { // fakultas
				
				$data['arr_jur'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$logged['userid'],'prodi','asc')->result();

				$this->load->view('laporan/print/phpexcel_bebanajar_fak',$data);				

			}elseif ((in_array(1, $grup))) { // admin

				$fak = $jurusan[1];
				$data['fak'] = $this->db->where('kd_fakultas', $fak)->get('tbl_jurusan_prodi')->row();
				$data['arr_jur'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$fak,'prodi','asc')->result();
				$data['title'] = $this->db->where('kd_fakultas', $fak)->get('tbl_fakultas')->row();
				//$this->load->view('laporan/print/beban_dosen_by_fakultas',$data);
				$this->load->view('laporan/print/phpexcel_bebanajar_fak',$data);
			}

			
		}else{

			if ((in_array(8, $grup))) { //prodi
				$data['jur'] = $logged['userid'];

				$data['rows']=$this->db->query("SELECT jdl.`kd_jadwal`,kry.`nidn`,kry.`nama`,kry.`tetap`,jdl.`kd_dosen`,SUM(mk.`sks_matakuliah`) AS jml_sks, COUNT(jdl.`kd_dosen`) AS span 
											FROM tbl_jadwal_matkul jdl 
											JOIN tbl_karyawan kry ON kry.`nid` = jdl.`kd_dosen`
											JOIN tbl_matakuliah mk ON mk.`kd_matakuliah` = jdl.`kd_matakuliah`
											WHERE kd_tahunajaran = '".$thn."' AND mk.`kd_prodi` = '".$logged['userid']."' 
											AND SUBSTRING_INDEX(kd_jadwal, '/', 1) = '".$logged['userid']."' AND (kelas != '' OR kelas IS NOT NULL)
											GROUP BY jdl.`kd_dosen`
											ORDER BY kry.`nama` ASC
											")->result();

				$this->load->view('laporan/print/phpexcel_bebanajar',$data);

			} else { // fakultas,baa,admin
				
				$data['rows']=$this->db->query("SELECT jdl.`kd_jadwal`,kry.`nidn`,kry.`nama`,kry.`tetap`,jdl.`kd_dosen`,SUM(mk.`sks_matakuliah`) AS jml_sks, COUNT(jdl.`kd_dosen`) AS span 
											FROM tbl_jadwal_matkul jdl 
											JOIN tbl_karyawan kry ON kry.`nid` = jdl.`kd_dosen`
											JOIN tbl_matakuliah mk ON mk.`kd_matakuliah` = jdl.`kd_matakuliah`
											WHERE kd_tahunajaran = '".$thn."' AND mk.`kd_prodi` = '".$jur."' 
											AND SUBSTRING_INDEX(kd_jadwal, '/', 1) = '".$jur."' AND (kelas != '' OR kelas IS NOT NULL)
											GROUP BY jdl.`kd_dosen`
											ORDER BY kry.`nama` ASC
											")->result();

				$this->load->view('laporan/print/phpexcel_bebanajar',$data);	
			}							
		}	
	}

	function jml_kelas(){
		$thn = $this->session->userdata('tahun');
		$jurus = $this->session->userdata('jurusan');
		

		$jurusan = explode('_', $jurus);

		$data['thn'] = $thn;
		$data['jur'] = $jurusan[0];
		$jur = $jurusan[0];

		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		//var_dump($pecah);die();

		if ($jurusan[0] == 99) {
			if ((in_array(10, $grup))) { //baa				
				$fak = $jurusan[1];
				$data['arr_jur'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$fak,'prodi','asc')->result();
				$data['title'] = $this->db->where('kd_fakultas', $fak)->get('tbl_fakultas')->row();
				$this->load->view('laporan/print/phpexcel_jmlkelas_fak',$data);

			} elseif ((in_array(9, $grup))) { // fakultas
				
				$data['arr_jur'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$logged['userid'],'prodi','asc')->result();
				$data['title'] = $this->db->where('kd_fakultas', $logged['userid'])->get('tbl_fakultas')->row();

				$this->load->view('laporan/print/phpexcel_jmlkelas_fak',$data);				

			}elseif ((in_array(1, $grup))) { // admin
				$fak = $jurusan[1];
				$data['arr_jur'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$fak,'prodi','asc')->result();
				$data['title'] = $this->db->where('kd_fakultas', $fak)->get('tbl_fakultas')->row();
				$this->load->view('laporan/print/phpexcel_jmlkelas_fak',$data);
			}
		}else{
			if ((in_array(8, $grup))) { //prodi
				$data['jur'] = $logged['userid'];

				$data['rows'] = $this->db->query("SELECT jdl.`kd_jadwal`,kry.`nama`,jdl.`kd_dosen`,jdl.`kd_matakuliah`,mk.`nama_matakuliah`,
													mk.`sks_matakuliah`,jdl.`kelas`,jdl.`waktu_kelas`,kry.`tetap`
													FROM tbl_jadwal_matkul jdl 
													JOIN tbl_matakuliah mk ON mk.`kd_matakuliah` = jdl.`kd_matakuliah`
													LEFT JOIN tbl_karyawan kry ON kry.`nid` = jdl.`kd_dosen`
													WHERE kd_tahunajaran = '".$thn."' AND mk.`kd_prodi` = '".$logged['userid']."' 
													AND SUBSTRING_INDEX(jdl.`kd_jadwal`, '/', 1) = '".$logged['userid']."' AND (kelas != '' OR kelas IS NOT NULL)
													AND (kelas != '' OR kelas IS NOT NULL)
													ORDER BY jdl.`kelas`")->result();

				$data['sem'] = $this->db->query("SELECT mk.`semester_matakuliah` as semes FROM tbl_jadwal_matkul jdl
											JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah` 
											WHERE SUBSTRING_INDEX(jdl.`kd_jadwal`, '/', 1) = '".$logged['userid']."' AND jdl.`kd_tahunajaran` = '".$thn."' AND mk.`kd_prodi` = '".$logged['userid']."'
											AND (jdl.`kd_dosen` != '' OR jdl.`kd_dosen` IS NOT NULL) 
											GROUP BY mk.`semester_matakuliah`")->result();

				$this->load->view('laporan/print/phpexcel_jmlkelas',$data);

			} else { // fakultas,baa,admin
				
				$data['rows'] = $this->db->query("SELECT jdl.`kd_jadwal`,jdl.`kd_dosen`,kry.`nama`,jdl.`kd_matakuliah`,mk.`nama_matakuliah`,
											mk.`sks_matakuliah`,jdl.`kelas`,jdl.`waktu_kelas`,kry.`tetap`
											FROM tbl_jadwal_matkul jdl 
											JOIN tbl_matakuliah mk ON mk.`kd_matakuliah` = jdl.`kd_matakuliah`
											LEFT JOIN tbl_karyawan kry ON kry.`nid` = jdl.`kd_dosen`
											WHERE kd_tahunajaran = '".$thn."' AND mk.`kd_prodi` = '".$jur."' 
											AND SUBSTRING_INDEX(jdl.`kd_jadwal`, '/', 1) = '".$jur."' AND (kelas != '' OR kelas IS NOT NULL)
											AND (kelas != '' OR kelas IS NOT NULL)
											ORDER BY jdl.`kelas`")->result();

				$data['sem'] = $this->db->query("SELECT mk.`semester_matakuliah` as semes FROM tbl_jadwal_matkul jdl
											JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah` 
											WHERE SUBSTRING_INDEX(jdl.`kd_jadwal`, '/', 1) = '".$jur."' AND jdl.`kd_tahunajaran` = '".$thn."' AND mk.`kd_prodi` = '".$jur."'
											AND (jdl.`kd_dosen` != '' OR jdl.`kd_dosen` IS NOT NULL) 
											GROUP BY mk.`semester_matakuliah`")->result();

				//var_dump($data['rows']);die();

				$this->load->view('laporan/print/phpexcel_jmlkelas',$data);	
			}							
		}
	}

	function get_jurusan($id)
	{
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."_".$id."'>".$row->prodi. "</option>";
        }
        $out .= "<option value='99_".$id."'>SEMUA PROGRAM STUDI</option>";
        $out .= "</select>";

        echo $out;
	}

}

/* End of file Laporan.php */
/* Location: ./application/controllers/Laporan.php */