<?php 
$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

// set active sheet
$excel->setActiveSheetIndex(0);

// name the worksheet
$excel->getActiveSheet()->setTitle('JML PENGISIAN EDOM PER PRODI');

// title
$excel->getActiveSheet()->setCellValue('A1', 'DATA TOTAL MAHASISWA MENGISI EDOM PER-PROGRAM STUDI');

// HEADER
$excel->getActiveSheet()->setCellValue('A3', 'NO');
$excel->getActiveSheet()->setCellValue('B3', 'JENJANG');
$excel->getActiveSheet()->setCellValue('C3', 'KODE PROGRAM STUDI');
$excel->getActiveSheet()->setCellValue('D3', 'NAMA PROGRAM STUDI');
$excel->getActiveSheet()->setCellValue('E3', 'TOTAL PENGISIAN EDOM');

// content
$excel->getActiveSheet()->setCellValue('A4', 'content');
$excel->getActiveSheet()->setCellValue('B4', 'content');
$excel->getActiveSheet()->setCellValue('C4', 'content');
$excel->getActiveSheet()->setCellValue('D4', 'content');
$excel->getActiveSheet()->setCellValue('E4', 'content');

//border
$excel->getActiveSheet()->getStyle('A3:E3')->applyFromArray($BStyle);
$excel->getActiveSheet()->mergeCells('A1:E2');

//align
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    )
);

$excel->getActiveSheet()->getStyle("A1:E1")->applyFromArray($style);
$excel->getActiveSheet()->getStyle("A1:E1")->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle("A3:E3")->applyFromArray($style);
$excel->getActiveSheet()->getStyle("A3:E3")->getFont()->setBold(true);


// ========================================================= batas ======================================================================

$objsheet = $excel->createSheet(1);

// name worksheet
$objsheet->setTitle('JML SKS DOSEN');

// title
$objsheet->setCellValue('A1', 'DATA DOSEN NIDN UNIVERSITAS BHAYANGKARA JAKARTA RAYA');
$objsheet->setCellValue('A2', 'Update 16 Januari 2018 Jam 15.00 WIB');

// HEADER
$objsheet->setCellValue('A4', 'NO');
$objsheet->setCellValue('B4', 'NIDN');
$objsheet->setCellValue('C4', 'EMAIL UBJ @dsn.ubharajaya.ac.id');
$objsheet->setCellValue('D4', 'JABATAN FUNGSIONAL AKADEMIK');
$objsheet->setCellValue('E4', 'KODE PROGRAM STUDI');
$objsheet->setCellValue('F4', 'NAMA PROGRAM STUDI');
$objsheet->setCellValue('G4', 'NAMA DOSEN');
$objsheet->setCellValue('H4', 'JUMLAH SKS DIAJARKAN');
$objsheet->setCellValue('I4', 'NILAI EDOM');

// CONTENT
$objsheet->setCellValue('A5', 'content');
$objsheet->setCellValue('B5', 'content');
$objsheet->setCellValue('C5', 'content');
$objsheet->setCellValue('D5', 'content');
$objsheet->setCellValue('E5', 'content');
$objsheet->setCellValue('F5', 'content');
$objsheet->setCellValue('G5', 'content');
$objsheet->setCellValue('H5', 'content');
$objsheet->setCellValue('I5', 'content');

// titile style
$objsheet->mergeCells('A1:I1');
$objsheet->mergeCells('A2:I2');
$objsheet->getStyle("A1:I1")->getFont()->setBold(true);
$objsheet->getStyle("A2:I2")->getFont()->setBold(true);
$objsheet->getStyle("A1:I1")->applyFromArray($style);
$objsheet->getStyle("A2:I2")->applyFromArray($style);

// headder style
$objsheet->getStyle('A4:I4')->applyFromArray($BStyle);
$objsheet->getStyle("A4:I4")->applyFromArray($style);
$objsheet->getStyle("A4:I4")->getFont()->setBold(true);

// content style
$objsheet->getStyle('A5:I5')->applyFromArray($BStyle);


// ======================================================= batas ===============================================================

$objsheet2 = $excel->createSheet(2);

// name worksheet
$objsheet2->setTitle('NILAI RATA-RATA EDOM PER PRODI');

// title
$objsheet2->setCellValue('A1', 'NILAI RATA-RATA EDOM PER PRODI');
$objsheet2->setCellValue('A2', 'PERIODE GANJIL 2017/2018');

// HEADER
$objsheet2->setCellValue('A4', 'NO');
$objsheet2->setCellValue('B4', 'ASPEK');
$objsheet2->setCellValue('C4', '74101');
$objsheet2->setCellValue('D4', '61101');
$objsheet2->setCellValue('E4', '62201');
$objsheet2->setCellValue('F4', '61201');
$objsheet2->setCellValue('G4', '70201');
$objsheet2->setCellValue('H4', '73201');
$objsheet2->setCellValue('I4', '74201');
$objsheet2->setCellValue('J4', '55201');
$objsheet2->setCellValue('K4', '26201');
$objsheet2->setCellValue('L4', '24201');
$objsheet2->setCellValue('M4', '25201');
$objsheet2->setCellValue('N4', '32201');
$objsheet2->setCellValue('O4', '86206');
$objsheet2->setCellValue('P4', '85202');

$objsheet2->setCellValue('C5', get_jur('74101'));
$objsheet2->setCellValue('D5', get_jur('61101'));
$objsheet2->setCellValue('E5', get_jur('62201'));
$objsheet2->setCellValue('F5', get_jur('61201'));
$objsheet2->setCellValue('G5', get_jur('70201'));
$objsheet2->setCellValue('H5', get_jur('73201'));
$objsheet2->setCellValue('I5', get_jur('74201'));
$objsheet2->setCellValue('J5', get_jur('55201'));
$objsheet2->setCellValue('K5', get_jur('26201'));
$objsheet2->setCellValue('L5', get_jur('24201'));
$objsheet2->setCellValue('M5', get_jur('25201'));
$objsheet2->setCellValue('N5', get_jur('32201'));
$objsheet2->setCellValue('O5', get_jur('86206'));
$objsheet2->setCellValue('P5', get_jur('85202'));

// CONTENT
$no = 3;
$loc = '';
foreach ($averageprodi as $val) {

	// load params
	$param = $this->temph_model->getParamByTopic($val->kd_topik)->result();

	$loc = count($param)+$no;
	$inc = $loc+1;

	// topik params
	$objsheet2->setCellValue('A'.$loc, $val->topik);

	foreach ($param as $out) {
		
		$objsheet2->setCellValue('A'.$inc, $out->parameter);		

	}

}

// title style
$objsheet2->mergeCells('A1:I1');
$objsheet2->mergeCells('A2:I2');
$objsheet2->getStyle("A1:I1")->getFont()->setBold(true);
$objsheet2->getStyle("A2:I2")->getFont()->setBold(true);
$objsheet2->getStyle("A1:I1")->applyFromArray($style);
$objsheet2->getStyle("A2:I2")->applyFromArray($style);

// headder style
$objsheet2->mergeCells('A4:A5');
$objsheet2->mergeCells('B4:B5');
$objsheet2->getStyle('A4:P5')->applyFromArray($BStyle);
$objsheet2->getStyle("A4:P5")->applyFromArray($style);

// content style
$objsheet2->getStyle("A6")->getFont()->setBold(true);
$objsheet2->mergeCells('A6:P6');

$filename = 'LaporanEdom.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>