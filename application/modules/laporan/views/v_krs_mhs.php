<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Detail KRS Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>laporan/report_krs/exp" class="btn btn-success">Export Excel</i></a>
					<hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                            <th>NPM</th>
	                            <th>NAMA</th>
								<th>Dosen PA</th>
	                            <th>Semester</th>
	                            <th>Total SKS</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($quer as $row){?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->npm_mahasiswa; ?></td>
	                        	<td><?php echo $row->NMMHSMSMHS; ?></td>
								<?php $panya = $this->app_model->getdetail('tbl_karyawan','nid',$row->id_pembimbing,'nid','asc')->row()->nama; ?>
	                        	
								<?php if ($this->session->userdata('dpa')!=''){
									$panya = $this->app_model->getdetail('tbl_karyawan','nid',$this->session->userdata('dpa'),'nid','asc')->row()->nama;
									echo '<td>'.$panya.'</td>';
								} else {
									echo '<td>'.$panya.'</td>';
								}?>
	                        	
	                        	<td><?php echo $row->semester_krs; ?></td>
	                        	<td><?php echo $row->tot; ?></td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>