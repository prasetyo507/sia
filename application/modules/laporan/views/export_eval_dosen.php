<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=evaluasi_dosen.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
<table>
	<thead>
		<th colspan="5">Data Evaluasi Dosen <?php $tahunajar = $this->app_model->getdetail('tbl_tahunakademik','kode',$this->session->userdata('ta'),'kode','asc')->row()->tahun_akademik; echo $tahunajar; ?></th>
	</thead>
</table>
<table border="1">
	<thead>
		<tr>
			<th style="background:yellow;">NO</th>
			<th style="background:yellow;">NID</th>
			<th style="background:yellow;">NAMA</th>
			<th style="background:yellow;">NILAI KUMULATIF</th>
			<th style="background:yellow;">TOTAL SKS</th>
		</tr>
	</thead>
	<tbody>
	    <?php $no = 1; foreach ($catch as $value) { ?>
	    <tr>
	    	<td><?php echo number_format($no); ?></td>
	    	<td><?php echo $value->kd_dosen; ?></td>
	    	<td><?php echo nama_dsn($value->kd_dosen); ?></td>
	    	<?php 
	            $this->db2 = $this->load->database('eval', TRUE);
	            if ($this->session->userdata('year') < '20171') {
                	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM tbl_pengisian_kuisioner WHERE nid = '".$value->kd_dosen."' and kd_jadwal like '".$prodi."%' and tahunajaran = '".$this->session->userdata('year')."'")->row()->akhir;
                } else {
                	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM tbl_pengisian_kuisioner_".$this->session->userdata('year')." WHERE nid = '".$value->kd_dosen."' and kd_jadwal like '".$prodi."%' and tahunajaran = '".$this->session->userdata('year')."'")->row()->akhir;	
                } 
	    	?>
	    	<!-- nilai rata-rata -->
        	<?php if ($this->session->userdata('year') < '20172') {
        		$average = number_format(($rata2/20),2);
        	} else {
        		$average = ($rata2/20)*10;
        	}
        	 ?>
	    	<td><?php echo $average; ?></td>
	    	<td><?php echo $value->sks; ?></td>
	    </tr>
	    <?php $no++; } ?>
	</tbody>
</table>