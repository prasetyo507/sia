<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-group"></i>
  				<h3>Detail Status Mahasiswa</h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="span11">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#actv">Mahasiswa Aktif</a></li>
					    <li><a data-toggle="tab" href="#nact">Mahasiswa Non Aktif</a></li>
					    <li><a data-toggle="tab" href="#cuti">Mahasiswa Cuti</a></li>
					</ul>
					<div class="tab-content">

						<!-- aktiv -->
					    <div id="actv" class="tab-pane fade in active">
							<a class="btn btn-large btn-warning" href="<?php echo base_url(); ?>"><< Kembali</a>
	                        <hr>
	                        <table id="example1" class="table table-bordered table-striped">
	                            <thead>
	                                <tr> 
	                                    <th>No</th>
	                                    <th>NPM</th>
	                                    <th>Nama</th>
	                                    <th>SKS</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <?php $no=1; foreach($actv as $row) { ?>
	                                <tr>
	                                    <td><?php echo $no; ?></td>
	                                    <td><?php echo $row->NIMHSMSMHS; ?></td>
	                                    <td><?php echo $row->NMMHSMSMHS; ?></td>
	                                    <td><a target="_blank" href="<?php echo base_url()?>keuangan/report_jml/view_krs_mhs/<?php echo $row->kd_krs?>"><?php echo $row->tot; ?></a></td>
	                                </tr>
	                                <?php $no++; } ?>
	                            </tbody>
	                        </table>
						</div>

						<!-- non aktiv -->
					    <div id="nact" class="tab-pane fade">
									
						</div>

						<!-- cuti -->
						<div id="cuti" class="tab-pane fade">
									
						</div>
					</div>
			    </div>
			</div>

		</div>
	</div>
</div>