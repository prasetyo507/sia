<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Evaluasi Dosen <?php $tahunajar = $this->app_model->getdetail('tbl_tahunakademik','kode',$this->session->userdata('ta'),'kode','asc')->row()->tahun_akademik; echo $tahunajar; ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    
                    <a href="<?php echo base_url(); ?>laporan/evaluasi/export_evl_dosen" class="btn btn-success"><i class="btn-icon-only icon-print"> </i> Print Excel</a>
                    <br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>NID</th>
                                <th>NAMA</th>
                                <th>NILAI AKUMULATIF</th>
                                <th>TOTAL SKS</th>
	                            <th width="80">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($getData as $value) { ?>
	                        <tr>
	                        	<td><?php echo number_format($no); ?></td>
	                        	<td><?php echo $value->kd_dosen; ?></td>
	                        	<td><?php echo nama_dsn($value->kd_dosen); ?></td>
	                        	<?php 
		                            $this->db2 = $this->load->database('eval', TRUE);
		                            if ($this->session->userdata('year') < '20171') {
		                            	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM tbl_pengisian_kuisioner WHERE nid = '".$value->kd_dosen."' and kd_jadwal like '".$prodi."%' and tahunajaran = '".$this->session->userdata('year')."'")->row()->akhir;
		                            } else {
		                            	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM tbl_pengisian_kuisioner_".$this->session->userdata('year')." WHERE nid = '".$value->kd_dosen."' and kd_jadwal like '".$prodi."%' and tahunajaran = '".$this->session->userdata('year')."'")->row()->akhir;	
		                            } 
	                        	?>

	                        	<!-- nilai rata-rata -->
	                        	<?php if ($this->session->userdata('year') < '20172') {
	                        		$average = number_format(($rata2/20),2);
	                        	} else {
	                        		$average = ($rata2/20)*10;
	                        	}
	                        	 ?>
	                        	 
	                        	<td><?php echo number_format($average,2); ?></td>
	                        	<td><?php echo $value->sks; ?></td>
	                        	<td class="td-actions">
									<a class="btn btn-primary btn-small" target="_blank" href="<?php echo base_url(); ?>laporan/evaluasi/view_detail/<?php echo $value->kd_dosen; ?>" ><i class="btn-icon-only icon-ok"> </i></a>
									<!-- <a class="btn btn-success btn-small" href="#" ><i class="btn-icon-only icon-print"> </i></a> -->
								</td>
	                        </tr>
                            <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>