<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=evaluasi_prodi_".get_jur($prodi).".xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 2px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
<table>
	<thead>
		<th colspan="5">Data Evaluasi Prodi <?= get_jur($prodi); ?> Tahun Akademik <?= get_thajar($sesi); ?></th>
	</thead>
</table>
<table id="example1" class="table table-bordered table-striped">
	<thead>
        <tr> 
        	<th>No</th>
            <th>Parameter</th>
            <th>Nilai</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; foreach ($getData as $value) { ?>
        <tr>
        	<td><?php echo number_format($no); ?></td>
        	<td><?php echo $value->parameter; ?></td>
        	<?php 	
        		$this->db2 = $this->load->database('eval', TRUE); 
    			
    			if ($sesi < '20171') {
    				$avg = $this->db2->query("SELECT AVG(nilai) as nilaibro 
    										from tbl_nilai_parameter 
    										where parameter_id = ".$value->id_parameter." 
    										AND kd_jadwal like '".$prodi."%'")->row()->nilaibro;
    			} else {
    				$avg = $this->db2->query("SELECT AVG(nilai) as nilaibro 
    										from tbl_nilai_parameter_".$sesi."
    										where parameter_id = ".$value->id_parameter."
    										AND kd_jadwal like '".$prodi."%'")->row()->nilaibro;
    			}

    			if ($sesi < 20172) {
    				
    				$aveg = number_format(($avg/20),2);

    			} else {
			                        				
    				$aveg = number_format($avg,2);

    			}
    			
        	?>

        	<td><?= $aveg; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>