<?php 
ob_start();

class PDF extends FPDF
{
	function Footer()
    {
        // Go to 1.5 cm from bottom
	    $this->SetY(-35);
	    // Select Arial italic 8
	    $this->ln(1);
		$this->SetFont('Arial','',10);
		$this->Cell(135,0,'',0,0,'L');
		$this->Cell(87,0,'........... , .............................'.date('Y').'',0,1);

		$this->ln(8);
		$this->SetFont('Arial','',10);
		$this->Cell(135,0,'Pengawas :',0,1,'L');
		
		$this->ln(10);
		$this->SetFont('Arial','',10);
		$this->Cell(135,0,'1.',0,0,'L');
		$this->Cell(135,0,'(                                      )',0,1,'L');

		$this->ln(10);
		$this->SetFont('Arial','',10);
		$this->Cell(135,0,'2.',0,0,'L');
		$this->Cell(135,0,'(                                      )',0,1,'L');
    }
}


$pdf = new PDF("P","mm", "A4");
$pdf->AliasNbPages();

$pdf->AddPage();


$pdf->SetMargins(3, 3 ,0);

$pdf->SetFont('Arial','',18);

$pdf->Cell(200,5,'',0,0,'L');

$pdf->Ln();

$pdf->SetFont('Arial','',12); 
$pdf->image('http://172.16.2.42:801/assets/logo.gif',6,11,13);
$pdf->SetLeftMargin(20);
$pdf->Cell(200,5,'PENERIMAAN MAHASISWA BARU - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,1,'L');
$pdf->SetFont('Arial','',11);
$pdf->Cell(200,5,'GELOMBANG '.$gel.'',0,0,'L');
$pdf->SetLeftMargin(5);
$pdf->Ln(5);



$pdf->SetFont('Arial','',9);

$pdf->Cell(15,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Dharmawangsa III No.1,Kebayoran Baru, Jakarta Selatan ',0,0,'L');

$pdf->Ln(6);



$pdf->SetFont('Arial','',9);

$pdf->Cell(15,3,'KAMPUS II',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Raya Perjuangan, Bekasi Barat',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(290,0,'',1,0,'C');

$pdf->ln(5);

$pdf->SetLeftMargin(5);

$pdf->SetFont('Arial','',12);

$pdf->Cell(198,8,'DAFTAR HADIR PESERTA UJIAN PENERIMAAN MAHASISWA BARU GELOMBANG '.$gel.'',1,0,'C');

$pdf->ln(8);
$pdf->SetFont('Arial','B',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NO REGISTRASI','L,T,R,B',0,'C');

$pdf->Cell(60,10,'NAMA','L,T,R,B',0,'C');

$pdf->Cell(38,10,'PILIHAN PRODI','L,T,R,B',0,'C');

$pdf->Cell(22,10,'KELAS','L,T,R,B',0,'C');

$pdf->Cell(45,10,'KEHADIRAN','L,T,R,B',1,'C');

$no=1;
$noo=1;
foreach ($absenpmb as $row) {
	//$pdf->ln(10);

	$pdf->SetFont('Arial','',8);

	$pdf->Cell(8,10,$no,1,0,'C');
	
	$pdf->Cell(25,10,$row->noreg,1,0,'C');
	//$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$row->npm_mahasiswa,'NIMHSMSMHS','ASC')->row()->NMMHSMSMHS;
	$pdf->Cell(60,10,$row->nama,1,0,'L');
	$prd = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$row->prodi,'kd_prodi','ASC')->row()->prodi;
	$pdf->Cell(38,10,$prd,1,0,'L');
	if ($row->kelas == 'PG') {
		$kls = 'A';
	} elseif ($row->kelas == 'SR') {
		$kls = 'B';
	} else {
		$kls = 'C';
	}
	
	$pdf->Cell(22,10,$kls,1,0,'L');

	$r=$no%2;
	if ($r == 0) {
		$pdf->Cell(22.5,10,'',1,0,'L');
		$pdf->Cell(22.5,10,$no,1,1,'L');
	} elseif ($r == 1) {
		$pdf->Cell(22.5,10,$no,1,0,'L');
		$pdf->Cell(22.5,10,'',1,1,'L');
	}
	
	if ($noo == 20) {
		$noo = 0;
		$pdf->AliasNbPages();

		$pdf->AddPage();


		$pdf->SetMargins(3, 3 ,0);

		$pdf->SetFont('Arial','',18);

		$pdf->Cell(200,5,'',0,0,'L');

		$pdf->Ln();

		$pdf->SetFont('Arial','',12); 

		$pdf->image('http://172.16.2.42:801/assets/logo.gif',6,4,13);
		$pdf->SetLeftMargin(20);
		$pdf->Cell(200,5,'PENERIMAAN MAHASISWA BARU - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,1,'L');
		$pdf->SetFont('Arial','',11);
		$pdf->Cell(200,5,'GELOMBANG '.$gel.'',0,0,'L');
		$pdf->SetLeftMargin(5);
		$pdf->Ln(5);



		$pdf->SetFont('Arial','',9);

		$pdf->Cell(15,3,'KAMPUS I',0,0,'L');

		$pdf->Cell(3,3,' : ',0,0,'L');

		$pdf->Cell(140,3,'Jl. Dharmawangsa III No.1,Kebayoran Baru, Jakarta Selatan ',0,0,'L');

		$pdf->Ln(6);



		$pdf->SetFont('Arial','',9);

		$pdf->Cell(15,3,'KAMPUS II',0,0,'L');

		$pdf->Cell(3,3,' : ',0,0,'L');

		$pdf->Cell(140,3,'Jl. Raya Perjuangan, Bekasi Barat',0,0,'L');

		$pdf->Ln(4);

		$pdf->Cell(290,0,'',1,0,'C');

		$pdf->ln(5);

		$pdf->SetLeftMargin(5);

		$pdf->SetFont('Arial','',12);

		$pdf->Cell(198,8,'DAFTAR HADIR PESERTA UJIAN PENERIMAAN MAHASISWA BARU GELOMBANG '.$gel.'',1,0,'C');

		$pdf->ln(8);
		$pdf->SetFont('Arial','B',8);

		$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

		$pdf->Cell(25,10,'NO REGISTRASI','L,T,R,B',0,'C');

		$pdf->Cell(60,10,'NAMA','L,T,R,B',0,'C');

		$pdf->Cell(38,10,'PILIHAN PRODI','L,T,R,B',0,'C');

		$pdf->Cell(22,10,'KELAS','L,T,R,B',0,'C');

		$pdf->Cell(45,10,'KEHADIRAN','L,T,R,B',1,'C');

	}
$noo++;
$no++;

} 

$pdf->Output('ABSEN_PMB.PDF','I');

?>
