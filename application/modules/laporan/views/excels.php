<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_mhs.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border="1">
	<thead>
		<tr>
			<th rowspan="2">NPM</th>
			<th rowspan="2">NAMA</th>
			<th rowspan="2">FAKULTAS</th>
			<th rowspan="2">PRODI</th>
			<th text-align="center" colspan="14">SEMESTER</th>
		</tr>
		<tr>
			<?php for ($i=1; $i < 15; $i++) { 
				echo "<th>SMT ".$i."</th>";
			} ?>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($mhs as $value) { $b = 1; $lls = 1; $z = 0; $k = 0;?>
		<tr>
			<td><?php echo $value->NIMHSMSMHS; ?></td>
			<td><?php echo $value->NMMHSMSMHS; ?></td>
			<td><?php echo $value->fakultas; ?></td>
			<td><?php echo $value->prodi; ?></td>
			<?php for ($i=1; $i < 15; $i++) { 
				$angka = 0;
				if ($i > 2) {
					if ($i%2 == 0) {
						$angka = ($i/2)-1;
						$ta = (substr($value->NIMHSMSMHS, 0,4)+$angka).'2';
					} else {
						$angka = ($i-1)/2;
						$ta = (substr($value->NIMHSMSMHS, 0,4)+$angka).'1';
					}
				} else {
					$ta = substr($value->NIMHSMSMHS, 0,4).$i;
				}
				$cek =  $this->app_model->getsmsmhs($value->NIMHSMSMHS,$ta)->row();
				$tanow = $this->app_model->getdetail('tbl_tahunakademik','status',1,'kode','ASC')->row();
				//$cek2 =  $this->app_model->getsmsmhsnon($value->NIMHSMSMHS,$i)->row();
				//$cek3 =  $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$value->NIMHSMSMHS,'NIMHSMSMHS','ASC')->row();
				//var_dump($cek);exit();
				if (count($cek) > 0) {
					$q = $this->db->query("select distinct kd_matakuliah from tbl_krs where kd_krs like concat('".$value->NIMHSMSMHS."','".$ta."')")->result();
		        	$sumsks = 0;
		        	foreach ($q as $key) {
		        		$sksmk = $this->db->query("select distinct sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$key->kd_matakuliah."'")->row()->sks_matakuliah; 
		        		$sumsks = $sumsks + $sksmk;
		        	}
					echo "<td>".$sumsks."</td>";
				} else {
					$cek2 =  $this->app_model->getsmsmhsnon($value->NIMHSMSMHS,$ta)->row();
					if(count($cek2) > 0) {
						if ($cek2->status == 'C') {
							$akhir = 'CUTI';
						} elseif ($cek2->status == 'N') {
							$akhir = 'NON - AKTIF';
							$a = 1;
							$b = $b + $a;
						} elseif ($cek2->status == 'K') {
							$akhir = 'KELUAR';
							$k = 1;
						}
					} else {
						$akm = $this->db->query("select * from tbl_aktifitas_kuliah_mahasiswa where NIMHSTRAKM = '".$value->NIMHSMSMHS."' and THSMSTRAKM = '".$ta."'")->row();
						//var_dump($akm);exit();
						if (count($akm) > 0) {
							$akhir = substr($akm->SKSEMTRAKM, 0, 2);
						} else {
							$cek3 =  $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$value->NIMHSMSMHS,'NIMHSMSMHS','ASC')->row();
							if ($cek3->STMHSMSMHS == 'L') {
								$akhir = 'LULUS';
								$z = 1;
							} //elseif ($cek3->STMHSMSMHS == 'K') {
								//$akhir = 'KELUAR';
							//}
							if ($ta < $cek3->SMAWLMSMHS) {
								$akhir = "*";
							} elseif ($lls >= 2 or $k == 1) {
								$akhir = "-";
							} elseif ($b == 3) {
								$akhir = 'KELUAR';
								$k = 1;
							} elseif ($ta == $tanow->kode) {
								$akhir = "";
							}
							$lls = $lls+$z;
						}
						
					}
					
					echo "<td>".$akhir."</td>";
				}
				//exit();
				// } else {
				// 	if ($cek3->status == 'K') {
				// 		$akhir = 'KELUAR';
				// 	} else {
				// 		$akhir = 'NON - AKTIF';
				// 	}
				// }
			} ?>
		</tr>
	<?php } ?>
	</tbody>
</table>