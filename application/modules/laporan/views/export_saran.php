<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=evaluasi_saran_dosen_".$this->session->userdata('nama_dosen').".xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>


<table id="example3" class="table table-bordered table-striped">
	<thead>
		<tr>
			<td colspan="2" style="text-align:center;vertical-align:middle;background:#8FBC8B;">
				<h5>Data Evaluasi Dosen <?php echo $this->session->userdata('nama_dosen').' # '.$kdjadwal->kelas; ?> <?php $tahunajar = $this->app_model->getdetail('tbl_tahunakademik','kode',$this->session->userdata('ta'),'kode','asc')->row()->tahun_akademik; echo $tahunajar; ?></h5>
			</td>
		</tr>
	    <tr> 
	    	<th style="background:yellow">No</th>
	        <th style="background:yellow">Saran</th>
	    </tr>
	</thead>
	<tbody>
	    <?php $no = 1; foreach ($getData2 as $value) { ?>
	    <tr>
	    	<td><?php echo number_format($no); ?></td>
	    	<td><?php echo $value->saran; ?></td>
	    </tr>
	    <?php $no++; } ?>
	</tbody>
</table>