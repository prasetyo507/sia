<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_mhs.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border="1">
	<thead>
		<tr>
			<th rowspan="2">NPM</th>
			<th rowspan="2">NAMA</th>
			<th rowspan="2">FAKULTAS</th>
			<th rowspan="2">PRODI</th>
			<th text-align="center" colspan="16">SEMESTER</th>
		</tr>
		<tr>
			<?php for ($i=1; $i < 17; $i++) { 
				echo "<th>SMSTR ".$i."</th>";
			} ?>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($mhs as $value) { ?>
		<tr>
			<td><?php echo $value->NIMHSMSMHS; ?></td>
			<td><?php echo $value->NMMHSMSMHS; ?></td>
			<td><?php echo $value->fakultas; ?></td>
			<td><?php echo $value->prodi; ?></td>
			<?php for ($i=1; $i < 17; $i++) { 
				$cek = $this->db->query("select distinct semester_krs from tbl_krs where npm_mahasiswa = '".$value->NIMHSMSMHS."' and semester_krs = ".$id." ")->result();
				if ($cek == true) {
					echo "<td>A</td>";
				} else {
					echo "<td></td>";
				}
			} ?>
		</tr>
	<?php } ?>
	</tbody>
</table>