<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Laporan KRS Mahasiswa</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>laporan/report_krs/savesesi" method="post" enctype="multipart/form-data">
					<fieldset>

						<?php 
							$mari = $this->session->userdata('sess_login');
							$pecah = explode(',', $mari['id_user_group']);
							$jmlh = count($pecah);
							for ($i=0; $i < $jmlh; $i++) { 
								$grup[] = $pecah[$i];
							}
							if ((in_array(8, $grup))) { ?>
								<input type="hidden" name="pro" value="<?php echo $pr->kd_prodi; ?>" placeholder="">
							<?php } elseif ((in_array(9, $grup)) || (in_array(10, $grup)) || (in_array(14, $grup)) || (in_array(1, $grup))) { ?>
								<div class="control-group">
									<label class="control-label">Prodi</label>
									<div class="controls">
										<select class="form-control span4" name="pro" required/>
											<option disabled="" selected="">--Pilih Prodi--</option>
											<?php foreach ($pr as $key) { ?>
												<option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
											<?php } ?>									
										</select>
									</div>
								</div>
						<?php } ?>

						<div class="control-group">
							<label class="control-label">Tahun Akademik</label>
							<div class="controls">
								<select class="form-control span4" name="tahun" required/>
									<option disabled="" selected="">--Pilih Tahun--</option>
									<?php foreach ($ta as $key) { ?>
										<option value="<?php echo $key->kode; ?>"><?php echo $key->tahun_akademik; ?></option>
									<?php } ?>									
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Status</label>
							<div class="controls">
								<select class="form-control span4" name="tahun" required/>
									<option disabled="" selected="">--Pilih Status--</option>
									<option value="">Semua</option>
									<option value="">Pengajuan</option>
									<option value="">Revisi</option>
									<option value="">Verifikasi</option>
								</select>
							</div>
						</div>
						<script>
							function show1(){
							  document.getElementById('div1').style.display ='none';
							  document.getElementById('papnya').required =false;							  
							  document.getElementById('papnya').value ='';							  
							}
							function show2(){
							  document.getElementById('div1').style.display = 'block' ;							  
							  document.getElementById('papnya').required =true;							  
							}
						</script>
						  <script type="text/javascript">
							// <![CDATA[
							$(document).ready(function () {
								$(function () {
									$( "#papnya" ).autocomplete({
										source: function(request, response) {
											$.ajax({ 
												url: "<?php echo base_url('laporan/report_krs/autopa'); ?>",
												data: { kode: $("#papnya").val()},
												dataType: "json",
												type: "POST",
												success: function(data){
													response(data);
												}    
											});
										},


									});
								});
							});
							// ]]>
							</script>
						<div class="control-group">
							<label class="control-label">Pembimbing Akademik</label>
							<div class="controls">
								<input type="radio" name="papa" id="pap" value="0" onclick="show1();"> Semua
  								<input type="radio" name="papa" id="pap" value="1" onclick="show2();"> Pilih Dosen
							</div>
						</div>

						<div class="control-group" style="display: none;" id="div1">
							<label class="control-label">Dosen Pembimbing Akademik</label>
							<div class="controls">
								<input type="text" name="papnya" id="papnya"/>
							</div>
						</div>

						<div class="form-actions">
							<input class="btn btn-large btn-success" type="submit" value="Submit">
							<!-- <a class="btn btn-large btn-success" href="<?php echo base_url(); ?>pmb/data_maba/load">Submit</a> -->
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

