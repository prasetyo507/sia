<?php 

$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$shit=0;

foreach ($arr_jur as $val) {

	 $ts = $this->db->where('kd_prodi', $val->kd_prodi)->get('tbl_jurusan_prodi')->row();    
    
    //ACTIVE SHEET
	$excel->setActiveSheetIndex($shit);
	//name the worksheet
	$excel->getActiveSheet()->setTitle($ts->prodi);

	//border
	//$excel->getActiveSheet()->getStyle('B3:C4')->applyFromArray($BStyle);
	//$excel->getActiveSheet()->getStyle('E3:F4')->applyFromArray($BStyle);
	//$excel->getActiveSheet()->getStyle('H3:L4')->applyFromArray($BStyle);

	//tittle
	$excel->getActiveSheet()->setCellValue('C1', 'DATA BEBAN MENGAJAR DOSEN');
	$excel->getActiveSheet()->mergeCells('C1:E1');
	$excel->getActiveSheet()->setCellValue('C2', 'PROGRAM STUDI '.$ts->prodi);
	$excel->getActiveSheet()->mergeCells('C2:E2');

	$rows = $this->db->query("SELECT jdl.`kd_jadwal`,kry.`nidn`,kry.`nama`,kry.`tetap`,jdl.`kd_dosen`,
                                SUM(mk.`sks_matakuliah`) AS jml_sks, COUNT(jdl.`kd_dosen`) AS span 
                                FROM tbl_jadwal_matkul jdl 
                                JOIN tbl_karyawan kry ON kry.`nid` = jdl.`kd_dosen`
                                JOIN tbl_matakuliah mk ON mk.`kd_matakuliah` = jdl.`kd_matakuliah`
                                WHERE kd_tahunajaran = '".$thn."' AND mk.`kd_prodi` = '".$val->kd_prodi."' 
                                AND SUBSTRING_INDEX(kd_jadwal, '/', 1) = '".$val->kd_prodi."' AND (kelas != '' OR kelas IS NOT NULL)
                                AND (jdl.`kd_jadwal` != '' OR jdl.`kd_jadwal` IS NOT NULL)
                                GROUP BY jdl.`kd_dosen`
                                ORDER BY kry.`nama` ASC
                                ")->result();

	//header
	$h=4;
	$excel->getActiveSheet()->setCellValue('A'.$h, 'NO');
	$excel->getActiveSheet()->setCellValue('B'.$h, 'NID');
	$excel->getActiveSheet()->setCellValue('C'.$h, 'NIDN/NUPN');
	$excel->getActiveSheet()->setCellValue('D'.$h, 'STATUS PEGAWAI');
	$excel->getActiveSheet()->setCellValue('E'.$h, 'NAMA');
	$excel->getActiveSheet()->setCellValue('F'.$h, 'KODE MK');
	$excel->getActiveSheet()->setCellValue('G'.$h, 'MATAKULIAH');
	$excel->getActiveSheet()->setCellValue('H'.$h, 'SKS');
	$excel->getActiveSheet()->setCellValue('I'.$h, 'KELAS');
	$excel->getActiveSheet()->setCellValue('J'.$h, 'TOTAL SKS');

	//isi
	$a=5;
	$no=1;
	foreach ($rows as $isi) {

		 $util = $this->db->query("SELECT jdl.`kd_dosen`,jdl.`kd_matakuliah`,mk.`nama_matakuliah`,mk.`sks_matakuliah`,jdl.`kelas`
                                            FROM tbl_jadwal_matkul jdl 
                                            JOIN tbl_matakuliah mk ON mk.`kd_matakuliah` = jdl.`kd_matakuliah`
                                            WHERE kd_tahunajaran = '".$thn."' AND mk.`kd_prodi` = '".$val->kd_prodi."' 
                                            AND SUBSTRING_INDEX(kd_jadwal, '/', 1) = '".$val->kd_prodi."' AND (kelas != '' OR kelas IS NOT NULL)
                                            AND (kelas != '' OR kelas IS NOT NULL) AND jdl.`kd_dosen` = '".$isi->kd_dosen."' 
                                            ")->result();

		if($isi->nidn == NULL || $isi->nidn == ''){$nidn = '-';}else{$nidn = $isi->nidn;}

		if($isi->tetap == 1 ){$tetap = 'Tetap';}else{$tetap = 'Tidak Tetap';}

		$excel->getActiveSheet()->setCellValue('A'.$a, $no);
		$excel->getActiveSheet()->setCellValue('B'.$a, $isi->kd_dosen);
		$excel->getActiveSheet()->setCellValue('C'.$a, $nidn);
		$excel->getActiveSheet()->setCellValue('D'.$a, $tetap);
		$excel->getActiveSheet()->setCellValue('E'.$a, $isi->nama);

			$b = $a;
			foreach ($util as $key) {
				$excel->getActiveSheet()->setCellValue('F'.$b, $key->kd_matakuliah);
				$excel->getActiveSheet()->setCellValue('G'.$b, $key->nama_matakuliah);
				$excel->getActiveSheet()->setCellValue('H'.$b, $key->sks_matakuliah);
				$excel->getActiveSheet()->setCellValue('I'.$b, $key->kelas);

				$b++;
			}
			
			$excel->getActiveSheet()->setCellValue('J'.$a, $isi->jml_sks);

			$c=$b-1;
			$excel->getActiveSheet()->mergeCells('A'.$a.':A'.$c.'');
			$excel->getActiveSheet()->mergeCells('B'.$a.':B'.$c.'');
			$excel->getActiveSheet()->mergeCells('C'.$a.':C'.$c.'');
			$excel->getActiveSheet()->mergeCells('D'.$a.':D'.$c.'');
			$excel->getActiveSheet()->mergeCells('E'.$a.':E'.$c.'');
			$excel->getActiveSheet()->mergeCells('J'.$a.':J'.$c.'');

					

		$a=$b;
		$no++;
	}//end foreach dosen

	//align
	$style = array(
	    'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	    )
	);

	$excel->getActiveSheet()->getStyle("C1:E2")->applyFromArray($style);
	$excel->createSheet();

$shit++;
} // end foreach jurusan





$filename = 'Data_Beban_Mengajar_'.$title->fakultas.'.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>