<?php
$t = $this->db->where('kd_prodi', $jur)->get('tbl_jurusan_prodi')->row();

header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=beban_mengajar_dosen.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
/*table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}*/
</style>

<table>
    <tr>
        <td colspan="7">DATA BEBAN AJAR DOSEN <?php echo $t->prodi; ?></td>
    </tr>
    <tr>
        <td></td>
    </tr>
</table>
                    
<table border="1px">
	<thead>
        <tr> 
        	<th>No</th>
            <th>NID</th>
            <th>NIDN/NUPN</th>
            <th>NAMA</th>
            <th>BEBAN AJAR(sks)</th>
            <th>MATA KULIAH</th>
            <th>KELAS</th>
            
        </tr>        
    </thead>
    <tbody>
        <?php $no=1; 
            foreach  ($rows as $isi) {
                $util = $this->db->query("SELECT jdl.`kd_jadwal`,jdl.`kd_dosen`,kry.`nidn`,kry.`nama`,kry.`tetap`,
                                            jdl.`kd_matakuliah`,mk.`sks_matakuliah`
                                            FROM tbl_jadwal_matkul jdl 
                                            JOIN tbl_karyawan kry ON kry.`nid` = jdl.`kd_dosen`
                                            JOIN tbl_matakuliah mk ON mk.`kd_matakuliah` = jdl.`kd_matakuliah`
                                            WHERE kd_tahunajaran = '".$thn."' AND mk.`kd_prodi` = '".$jur."' 
                                            AND SUBSTRING_INDEX(kd_jadwal, '/', 1) = '".$jur."' AND (kelas != '' OR kelas IS NOT NULL)
                                            AND jdl.`kd_dosen` = '".$isi->kd_dosen."'
                                            ORDER BY kry.`nama` ASC 
                                            ")->result();
                ?>
            <tr>
                <td rowspan="<?php echo $isi->span;?>"><?php echo $no; ?></td>
                <td rowspan="<?php echo $isi->span;?>"><?php echo $isi->kd_dosen; ?></td>
                <td rowspan="<?php echo $isi->span;?>"><?php if($isi->nidn == NULL || $isi->nidn == ''){echo '-';}else{echo $isi->nidn;}?></td>
                <td rowspan="<?php echo $isi->span;?>"><?php echo $isi->nama; ?></td>
                <td rowspan="<?php echo $isi->span;?>"><?php echo $isi->jml_sks; ?></td>
            
                <?php $aa=1; foreach ($util as $key) { ?>
                    <?php if ($aa == 1){ ?>
                        <td><?php echo $key->kd_matakuliah.'-'.$key->nama_matakuliah.'('.$key->sks_matakuliah.')'; ?></td>
                        <td><?php echo $key->kelas; ?></td>
                    </tr>
                    <?php }else{ ?>
                    <tr>
                        <td><?php echo $key->kd_matakuliah.'-'.$key->nama_matakuliah.'('.$key->sks_matakuliah.')'; ?></td>
                        <td><?php echo $key->kelas; ?></td>
                    </tr>
                    <?php } ?>
                <?php $aa++; } ?>    
        <?php $no++; } ?>
    </tbody>
</table>