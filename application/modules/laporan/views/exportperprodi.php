<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=evaluasi_fakultas_".get_fak($arr).".xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 2px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
<table>
	<thead>
		<th colspan="5">Data Evaluasi Fakultas <?= get_fak($arr); ?> Tahun Akademik <?= get_thajar($sesi); ?></th>
	</thead>
</table>
<table id="example1" class="table table-bordered table-striped">
	<thead>
        <tr> 
        	<th>No</th>
            <th>Prodi</th>
            <th>NILAI AKUMULATIF</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; foreach ($prod as $value) { ?>
        <tr>
        	<td><?php echo number_format($no); ?></td>
        	<td><?php echo $value->prodi; ?></td>
        	<?php 
                $this->db2 = $this->load->database('eval', TRUE);
                if ($sesi < '20171') {
                	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM 
                								tbl_pengisian_kuisioner 
                								WHERE kd_jadwal like '".$value->kd_prodi."%' 
                								AND tahunajaran = '".$sesi."'")->row()->akhir;
                } else {
                	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM 
                								tbl_pengisian_kuisioner_".$sesi." 
                								WHERE kd_jadwal like '".$value->kd_prodi."%' 
                								AND tahunajaran = '".$sesi."'")->row()->akhir;	
                } 
        	?>

        	<!-- nilai rata-rata -->
        	<?php if ($sesi < '20172') {
        		$average = number_format(($rata2/20),2);
        	} else {
        		$average = number_format((($rata2/20)*10),2);
        	}
        	 ?>

        	<td><?php echo $average; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>