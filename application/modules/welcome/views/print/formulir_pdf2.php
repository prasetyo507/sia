<?php

//var_dump($detail_khs);exit();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();


$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','B',18); 





$pdf->Cell(200,7,'UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,3,'C');
$pdf->SetFont('Arial','',8);
$pdf->Cell(200,3,'Kampus I : Jl. Dharmawangsa I No.1 Kebayoran Baru, Jakarta 12140 - Tlp : (021) 7267655, 7231948 - Fax. : (021) 7267657',0,2,'C');
$pdf->Cell(200,3,'Kampus II : Jl. Perjuangan, Bekasi - Tlp : (021) 88955882 - Fax. : (021) 88955871',0,2,'C');
$pdf->Cell(200,3,'website : www.ubharajaya.ac.id',0,2,'C');


$pdf->SetFont('Arial','B',12); 
$pdf->ln(4);
$pdf->SetTextColor(255, 255, 255);
$pdf->Cell(200,4,'FORMULIR PENDAFTARAN CALON MAHASISWA BARU',1,0,'C',1);

$pdf->SetTextColor(0, 0, 0);
$pdf->ln(7);
$pdf->SetFont('Arial','',9); 
$pdf->Cell(100,4,'Nama Calon Mahasiswa : (Sesuai Ijazah / Akte Kelahiran) : ','L,R,T',0,'');
$pdf->Cell(50,4,'Jenis kelamin : ','L,R,T',0,'');
$pdf->Cell(50,4,'Kewarganegaraan: ','R,T',1,'');

$pdf->Cell(100,4,$row->nama,'L,',0,'');

if ($row->jk == 'L') {
	$jk = 1;
}elseif ($row->jk == 'P') {
	$jk = 21;
}

$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->image('http://172.16.2.42:801/assets/check24.png',$x+$jk,$y,3);
$pdf->Cell(20,4,'O Pria ','L,',0,'');
$pdf->Cell(30,4,'O Wanita ',',R',0,'');

if ($row->jk == 'L' || $row->jk == 'P') {
	$kwx = 1;
	$kwy = 0;
}
// elseif ($row->jk == 'P') {
// 	$kwx = 1;
// 	$kwy = 4;
//}
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->image('http://172.16.2.42:801/assets/check24.png',$x+$kwx,$y+$kwy,3);
$pdf->Cell(50,4,'O WNI ','R',1,'');
$pdf->Cell(100,4,'','L,R',0,'');
$pdf->Cell(50,4,'',',R',0,'');
$pdf->Cell(50,4,'O WNA, sebutkan : ... ','',0,'');
$pdf->Cell(50,4,'','L,R',1,'');

$pdf->Cell(25,5,'Tempat Lahir : ','L,T,B',0,'');
$pdf->Cell(75,5,$row->tpt_lahir,'T,B,R',0,'');
$pdf->Cell(25,5,'Tempat Lahir : ','L,T,B',0,'');
$pdf->Cell(75,5,$row->tgl_lahir,'T,B,R',1,'');

$agmx= 0;
$agmy= 0;
if ($row->agama == 'islam') {
	$agmx = 1;
}elseif ($row->agama == 'protestan') {
	$agmx = 26;
}elseif ($row->agama == 'katolik') {
	$agmx = 51;
}elseif ($row->agama == 'hindu') {
	$agmx = 1;
	$agmy = 4;
}elseif ($row->agama == 'budha') {
	$agmx = 26;
	$agmy = 4;
}

$pdf->Cell(100,4,'Agama: ','L,R,T',0,'');
$pdf->Cell(100,4,'Status: ','R',1,'');
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->image('http://172.16.2.42:801/assets/check24.png',$x+$agmx,$y+$agmy,3);
$pdf->Cell(25,4,'O Islam ','L',0,'');
$pdf->Cell(25,4,'O Katolik ','',0,'');
$pdf->Cell(50,4,'O Hindu ','',0,'');

if ($row->status == 'M') {
	$stx = 1;
	//$sty = 0;
}elseif ($row->status == 'BM') {
	$stx = 31;
	//$sty = 4;
}

$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->image('http://172.16.2.42:801/assets/check24.png',$x+$stx,$y,3);
$pdf->Cell(30,4,'O Menikah ','L',0,'');
$pdf->Cell(70,4,'O Belum Menikah ','R',1,1,'');

$pdf->Cell(25,4,'O Protestan','L',0,'');
$pdf->Cell(25,4,'O Budha ','',0,'');
$pdf->Cell(50,4,'O Lain-lain ','',0,'');


// if ($row->status == 1) {
// 	$krx = 1;
// 	//$sty = 0;
// }elseif ($row->kerja_cmahasiswa == 2) {
// 	$krx = 31;
// 	//$sty = 4;
// }

// $x = $pdf->GetX();
// $y = $pdf->GetY();
// $pdf->image('http://172.16.2.42:801/assets/check24.png',$x+$krx,$y,3);
$pdf->Cell(30,4,'O Belum Bekerja ','L',0,'');
$pdf->Cell(20,4,'O Bekerja di :',0,0,'');
// $pdf->Cell(50,4,$row->tmptkerja_cmahasiswa,'R',1,'');

$pdf->Cell(100,5,'Alamat: ','L,R,T',0,'');
$pdf->Cell(50,5,'Kode Pos (harus diisi) : '/*$row->kodepos_cmahasiswa*/,'L,R,T',0,'');
$pdf->Cell(50,5,'No. Telepon : '.$row->tlp.'','R,T',1,'');

$pdf->Cell(100,4,'Asal Sekolah: '.$row->asal_sekolah.'','L,R,T',0,'');
$pdf->Cell(50,4,'Jurusan : '/*$row->jurusan_cmahasiswa*/,'L,R,T',0,'');
$pdf->Cell(50,4,'Tahun Lulus : '.$row->th_lulus.'','R,T',1,'');

$pdf->Cell(100,5,'Kota Asal Sekolah: '/*$row->kota_asal_cmahasiswa*/,'L,R,T',0,'');
$pdf->Cell(50,5,'Nomor Ijazah : '/*$row->no_izasah_cmahasiswa*/,'L,R,T',0,'');
$pdf->Cell(50,5,'NEM (bila ada) : '/*$row->nilaiun_cmahasiswa*/,'R,T',1,'');

if ($row->pts_lain == 'Y') {
	$pkx = 41;
	//$sty = 0;
}elseif ($row->pts_lain == 'T') {
	$pkx = 57;
	//$sty = 4;
}
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->image('http://172.16.2.42:801/assets/check24.png',$x+$pkx,$y,3);
$pdf->Cell(100,4,'Pernah Kuliah di PTS Lain : O Pernah   O  Tidak ','L,R,T',0,'');
$pdf->Cell(50,4,'Kota PTS Asal  : '/*$row->kota_pts_cmahasiswa*/,'L,R,T',0,'');
$pdf->Cell(50,4,'Fakultas / Jurusan : '.$row->jur_lain.'','R,T',1,'');
$pdf->Cell(100,4,'Tahun Masuk / Lulus :'.$row->tm_tl.'','L,R',0,'');
$pdf->Cell(50,4,'','R',0,0,'');
$pdf->Cell(50,4,'','R',1,0,'');

		/** 
			62201     AKUNTANSI           1            S1       
	        73201     ILMU PSIKOLOGI      5            S1       
	        70201     ILMU KOMUNIKASI     4            S1       
	        25201     TEKNIK LINGKUNGAN   2            S1       
	        24201     TEKNIK KIMIA        2            S1       
	        55201     TEKNIK INFORMATIKA  2            S1       
	        26201     TEKNIK INDUSTRI     2            S1       
	        61201     MANAJEMEN           1            S1       
	        74201     ILMU HUKUM          3            S1       
	        74101     MAGISTER HUKUM      6            S2       
	        61101     MAGISTER MANAJEMEN  6            S2       
	        32201     TEKNIK PERMINYAKAN  2            S1 
	        **/


if ($row->pilih_jur == '62201') {
	$pkx = 41;
	$sty = 36;
}elseif ($row->pilih_jur == '73201') {
	$pkx = 41;
	$sty = 44;
}elseif ($row->pilih_jur == '70201') {
	$pkx = 41;
	$sty = 40;
}elseif ($row->pilih_jur == '25201') {
	$pkx = 41;
	$sty = 28;
}elseif ($row->pilih_jur == '24201') {
	$pkx = 41;
	$sty = 20;
}elseif ($row->pilih_jur == '55201') {
	$pkx = 41;
	$sty = 12;
}elseif ($row->pilih_jur == '26201') {
	$pkx = 41;
	$sty = 16;
}elseif ($row->pilih_jur == '61201') {
	$pkx = 41;
	$sty = 32;
}elseif ($row->pilih_jur == '74201') {
	$pkx = 41;
	$sty = 8;
}elseif ($row->pilih_jur == '74101') {
	$pkx = 101;
	$sty = 8;
}elseif ($row->pilih_jur == '61101') {
	$pkx = 101;
	$sty = 12;
}elseif ($row->pilih_jur == '32201') {
	$pkx = 41;
	$sty = 24;
}
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->image('http://172.16.2.42:801/assets/check24.png',$x+$pkx,$y+$sty,3);

$pdf->Cell(100,4,'Program Sarjana (S.1) ','L,R,T',0,'');
$pdf->Cell(100,4,'Program Sarjana (S.2) ','L,R,T',1,'');

$pdf->Cell(100,4,'Pilihan Program Studi ','L,R',0,'');
$pdf->Cell(100,4,'Pilihan Program Studi :','L,R',1,'');

$pdf->Cell(40,4,'Fakultas Hukum  ','L',0,'');
$pdf->Cell(60,4,'O Ilmu Hukum ','R',0,'');
$pdf->Cell(100,4,'O Magister Manajemen ','R',1,'');

$pdf->Cell(40,4,'Fakultas Teknik  ','L',0,'');
$pdf->Cell(60,4,'O Teknik Informatika ','R',0,'');
$pdf->Cell(100,4,'O Magister Hukum ','R',1,'');

$pdf->Cell(40,4,'','L',0,'');
$pdf->Cell(60,4,'O Teknik Industri','R',0,'');
$pdf->Cell(100,4,'','R',1,'');

$pdf->Cell(40,4,'','L',0,'');
$pdf->Cell(60,4,'O Teknik Kimia','R',0,'');
$pdf->Cell(100,4,'','R',1,'');


$pdf->Cell(40,4,'','L',0,'');
$pdf->Cell(60,4,'O Teknik Perminyakan','R',0,'');
$pdf->Cell(100,4,'','R',1,'');

$pdf->Cell(40,4,'','L',0,'');
$pdf->Cell(60,4,'O Teknik Lingkungan','R',0,'');
$pdf->Cell(100,4,'','R',1,'');

$pdf->Cell(40,4,'Fakultas Ekonomi  ','L',0,'');
$pdf->Cell(60,4,'O Manajemen ','R',0,'');
$pdf->Cell(100,4,'','R',1,'');

$pdf->Cell(40,4,'','L',0,'');
$pdf->Cell(60,4,'O Akuntansi','R',0,'');
$pdf->Cell(100,4,'','R',1,'');

$pdf->Cell(40,4,'Fakultas Ilmu Komunikasi  ','L',0,'');
$pdf->Cell(60,4,'O Ilmu Komunikasi ','R',0,'');
$pdf->Cell(100,4,'','R',1,'');

$pdf->Cell(40,4,'Fakultas Psikologi  ','L',0,'');
$pdf->Cell(60,4,'O Ilmu Psikologi ','R',0,'');
$pdf->Cell(100,4,'','R',1,'');


if ($row->lokasi_kamp == 'jkt') {
	$pkx = 41;
	//$sty = 0;
}elseif ($row->lokasi_kamp == 'bks') {
	$pkx = 71;
	//$sty = 4;
}
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->image('http://172.16.2.42:801/assets/check24.png',$x+$pkx,$y,3);
$pdf->Cell(40,4,'Pilihan Kampus : ','L,T',0,'');
$pdf->Cell(30,4,'O Jakarta ','T',0,'');
$pdf->Cell(30,4,'O Bekasi ','T,R',0,'');


if ($row->pilih_kls == 'pagi') {
	$pkx = 41;
	//$sty = 0;
}elseif ($row->pilih_kls == 'sore') {
	$pkx = 71;
	//$sty = 4;
}
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->image('http://172.16.2.42:801/assets/check24.png',$x+$pkx,$y,3);
$pdf->Cell(40,4,'Pilihan Kelas : ','L,T',0,'');
$pdf->Cell(30,4,'O Pagi ','T',0,'');
$pdf->Cell(30,4,'O Sore ','T,R',1,'');

$pdf->Cell(100,5,'Nama Ayah: '.$row->nm_ayah.'',1,0,'');
$pdf->Cell(100,5,'Nama Ibu: '.$row->nm_ibu.'',1,1,'');

$pdf->Cell(100,4,'Agama Ayah: ','L,R,T',0,'');
$pdf->Cell(100,4,'Agama Ibu: ','R',1,'');

$agmx= 0;
$agmy= 0;
if ($row->agama_ayah == 'islam') {
	$agmx = 1;
}elseif ($row->agama_ayah == 'protestan') {
	$agmx = 26;
}elseif ($row->agama_ayah == 'katolik') {
	$agmx = 51;
}elseif ($row->agama_ayah == 'hindu') {
	$agmx = 1;
	$agmy = 4;
}elseif ($row->agama_ayah == 'budha') {
	$agmx = 26;
	$agmy = 4;
}
$agmbx= 0;
$agmby= 0;
if ($row->agama_ibu == 'islam') {
	$agmbx = 101;
	//$agmby = 4;
}elseif ($row->agama_ibu == 'protestan') {
	$agmbx = 101;
	$agmby = 4;
}elseif ($row->agama_ibu == 'katolik') {
	$agmbx = 126;
	//$agmby = 4;
}elseif ($row->agama_ibu == 'hindu') {
	$agmbx = 151;
	//$agmby = 4;
}elseif ($row->agama_ibu == 'budha') {
	$agmbx = 126;
	$agmby = 4;
}
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->image('http://172.16.2.42:801/assets/check24.png',$x+$agmx,$y+$agmy,3);
$pdf->image('http://172.16.2.42:801/assets/check24.png',$x+$agmbx,$y+$agmby,3);
$pdf->Cell(25,4,'O Islam ','L',0,'');
$pdf->Cell(25,4,'O Katolik ','',0,'');
$pdf->Cell(50,4,'O Hindu ','',0,'');

$pdf->Cell(25,4,'O Islam','L',0,'');
$pdf->Cell(25,4,'O Katolik ','',0,'');
$pdf->Cell(50,4,'O Hindu ','R',1,'');

$pdf->Cell(25,4,'O Protestan','L',0,'');
$pdf->Cell(25,4,'O Budha ','',0,'');
$pdf->Cell(50,4,'O Lain-lain ','R',0,'');

$pdf->Cell(25,4,'O Protestan','L',0,'');
$pdf->Cell(25,4,'O Budha ','',0,'');
$pdf->Cell(50,4,'O Lain-lain ','R',1,'');

$pdf->Cell(200,5,'Alamat Orang Tua: ','L,R,T',1,'');

$pdf->Cell(85,5,'Kota : ','L,R,T',0,'');
$pdf->Cell(55,5,'Kode Pos (harus diisi) : ','L,R,T',0,'');
$pdf->Cell(60,5,'No. Telp : ','L,R,T',1,'');

$pdf->Cell(85,4,'Pendidikan Ayah : ','L,R,T',0,'');
$pdf->Cell(55,4,'Pekerjaan Ayah : ','L,R,T',0,'');
$pdf->Cell(60,4,'Status Ayah : ','L,R,T',1,'');

$pdf->Cell(30,4,'O Tidak Tamat SD ','L',0,'');
$pdf->Cell(55,4,'O Sarjana Muda ','R',0,'');
$pdf->Cell(30,4,'O Pegawai Negeri','L',0,'');
$pdf->Cell(25,5,'O Tidak Bekerja ','',0,'');
$pdf->Cell(60,5,'O Masih Hidup ','L,R',1,'');

$pdf->Cell(30,4,'O Tamat SD ','L',0,'');
$pdf->Cell(55,4,'O Sarjana ','R',0,'');
$pdf->Cell(30,4,'O  TNI / POLRI','L',0,'');
$pdf->Cell(25,4,'O Pensiun ','',0,'');
$pdf->Cell(60,4,'O Sudah Meninggal ','L,R',1,'');

$pdf->Cell(30,4,'O Tamat SLTP ','L',0,'');
$pdf->Cell(55,4,'O Pascasarjana ','R',0,'');
$pdf->Cell(30,4,'O  Pegawai Swasta','L',0,'');
$pdf->Cell(25,4,'O Lain - lain ','',0,'');
$pdf->Cell(60,4,'','L,R',1,'');

$pdf->Cell(30,4,'O Tamat SLTA ','L',0,'');
$pdf->Cell(55,4,'O Doktor ','R',0,'');
$pdf->Cell(30,4,'O Usaha Sendiri','L',0,'');
$pdf->Cell(25,4,'','',0,'');
$pdf->Cell(60,4,'','L,R',1,'');

$pdf->Cell(30,4,'O Diploma ','L',0,'');
$pdf->Cell(55,4,'','R',0,'');
$pdf->Cell(30,4,'','L',0,'');
$pdf->Cell(25,4,' ','',0,'');
$pdf->Cell(60,4,'','L,R',1,'');

$pdf->Cell(85,4,'Pendidikan Ibu : ','L,R,T',0,'');
$pdf->Cell(55,4,'Pekerjaan Ibu : ','L,R,T',0,'');
$pdf->Cell(60,4,'Status Ibu : ','L,R,T',1,'');

$pdf->Cell(30,4,'O Tidak Tamat SD ','L',0,'');
$pdf->Cell(55,4,'O Sarjana Muda ','R',0,'');
$pdf->Cell(30,4,'O Pegawai Negeri','L',0,'');
$pdf->Cell(25,4,'O Tidak Bekerja ','',0,'');
$pdf->Cell(60,4,'O Masih Hidup ','L,R',1,'');

$pdf->Cell(30,4,'O Tamat SD ','L',0,'');
$pdf->Cell(55,4,'O Sarjana ','R',0,'');
$pdf->Cell(30,4,'O  TNI / POLRI','L',0,'');
$pdf->Cell(25,4,'O Pensiun ','',0,'');
$pdf->Cell(60,4,'O Sudah Meninggal ','L,R',1,'');

$pdf->Cell(30,4,'O Tamat SLTP ','L',0,'');
$pdf->Cell(55,4,'O Pascasarjana ','R',0,'');
$pdf->Cell(30,4,'O  Pegawai Swasta','L',0,'');
$pdf->Cell(25,4,'O Lain - lain ','',0,'');
$pdf->Cell(60,4,'','L,R',1,'');

$pdf->Cell(30,4,'O Tamat SLTA ','L',0,'');
$pdf->Cell(55,4,'O Doktor ','R',0,'');
$pdf->Cell(30,4,'O Usaha Sendiri','L',0,'');
$pdf->Cell(25,4,'','',0,'');
$pdf->Cell(60,4,'','L,R',1,'');

$pdf->Cell(30,4,'O Diploma ','L',0,'');
$pdf->Cell(55,4,'','R',0,'');
$pdf->Cell(30,4,'','L',0,'');
$pdf->Cell(25,4,' ','',0,'');
$pdf->Cell(60,4,'','L,R',1,'');

$pdf->Cell(200,5,'PENGHASILAN ORANG TUA : ','L,R,T',1,'');

$pdf->Cell(100,4,'Referensi dari (Tulliskan jika ada) ? : ','L,R,T',0,'');
$pdf->Cell(100,4,$row->referensi,'L,R,T',1,'');
$pdf->Cell(100,4,' ','L,R',0,'');
$pdf->Cell(100,4,'Jakarta / Bekasi, ..................................... 20 ............... ','L,R',1,'C');
$pdf->Cell(100,4,'','L,R',0,'');
$pdf->Cell(100,4,'','L,R',0,'');
$pdf->Cell(100,4,' ','L,R',1,'');
$pdf->Cell(100,4,'','L,R',0,'');
$pdf->Cell(100,4,'','L,R',0,'');

$pdf->Cell(100,4,'( _______________________________________ ) ','L,R',1,'C');
$pdf->Cell(100,4,'','L,R',0,'');
$pdf->Cell(100,4,'Nama calon mahasiswa','L,R',1,'C');

$pdf->Cell(200,5,'','T',0,'');

//$pdf->Cell(200,4,'FORMULIR PENDAFTARAN CALON MAHASISWA BARU',1,0,'C',1);


$pdf->Output('FORMULIR.PDF','I');



?>

