<?php 
$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);


$excel->setActiveSheetIndex(0);
//name the worksheet
$excel->getActiveSheet()->setTitle('DATA MATAKULIAH SEMESTER PENDEK');
//header
$excel->getActiveSheet()->setCellValue('A1', 'DATA MATAKULIAH SEMESTER PENDEK - '.$ta);
$excel->getActiveSheet()->setCellValue('A2', 'PROGRAM STUDI '.get_jur($pro).'');

//HEADER
$excel->getActiveSheet()->setCellValue('A4', 'NO');
$excel->getActiveSheet()->setCellValue('B4', 'KODE MATAKULIAH');
$excel->getActiveSheet()->setCellValue('C4', 'NAMA MATAKULIAH');
$excel->getActiveSheet()->setCellValue('D4', 'DOSEN');
$excel->getActiveSheet()->setCellValue('E4', 'WAKTU');
$excel->getActiveSheet()->setCellValue('F4', 'PENDAFTAR');
$excel->getActiveSheet()->setCellValue('G4', 'VERIFIKASI');


//ISI DATABASE
//list matakuliah
$baris = 5;$no=1;foreach ($rows as $isi) {
	if ($isi->nama != '') {
		$dosen = $isi->nama;
	}else{
		$dosen = ' - ';
	}

	$bayar = $this->app_model->getbayarsp($isi->kd_jadwal);

	$excel->getActiveSheet()->setCellValue('A'.$baris.'', $no);
	$excel->getActiveSheet()->setCellValue('B'.$baris.'', $isi->kd_matakuliah);
	$excel->getActiveSheet()->setCellValue('C'.$baris.'', $isi->nama_matakuliah);
	$excel->getActiveSheet()->setCellValue('D'.$baris.'', $dosen);
	$excel->getActiveSheet()->setCellValue('E'.$baris.'', notohari($isi->hari).' / '.substr($isi->waktu_mulai, 0,-3).' - '.substr($isi->waktu_selesai,0,-3));
	$cp = $this->db->query('SELECT COUNT(kd_jadwal) AS jml_mhs FROM tbl_krs_sp WHERE kd_jadwal = "'.$isi->kd_jadwal.'"')->row();
	$excel->getActiveSheet()->setCellValue('F'.$baris.'', $cp->jml_mhs);
	//$bayar = $this->app_model->getbayarsp($isi->kd_matakuliah);
	$excel->getActiveSheet()->setCellValue('G'.$baris.'', $bayar);

	$baris++;
	$no++;
}
$baris--;
//border
$excel->getActiveSheet()->getStyle('A4:G'.$baris.'')->applyFromArray($BStyle);
$excel->getActiveSheet()->mergeCells('A1:G1');
$excel->getActiveSheet()->mergeCells('A2:G2');



//align
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
);

$excel->getActiveSheet()->getStyle("A1:G2")->applyFromArray($style);
$excel->getActiveSheet()->getStyle("A1:G2")->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle("A4:G".$baris."")->applyFromArray($style);
//$excel->getDefaultStyle()->applyFromArray($style);

$filename = 'Form_Nilai_.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>