<?php
//var_dump($rows);die();
error_reporting(0);

define('FPDF_FONTPATH','font/');
// require('fpdf.php');

class PDF extends FPDF
{
    //Page header
    function Header()
    {
        $this->SetMargins(3, 5 ,0);
        $this->SetFont('Arial','',11); 

        $this->image('http://172.16.2.42:801/assets/ubhara.png',30,50,90);
        $this->image('http://172.16.2.42:801/assets/logo.gif',15,6,13);

        $this->Cell(140,5,'KARTU UJIAN AKHIR SEMESTER',0,2,'C');

        $this->Cell(140,5,'UNIVERSITAS BHAYANGKARA',0,2,'C');

        $this->Cell(140,5,'JAKARTA RAYA',0,1,'C');

        $this->Ln(1);

        $this->Cell(142,0,'',1,1,'C');
    }

    //Page footer
    function Footer()
    {

        $this->ln(11);

        $this->SetFont('Arial','B',8);
        $this->Cell(98,5,'',0,0,'C');

        $this->Cell(30,5,'JAKARTA , '.date('d-m-Y'),0,0,'C');
        $this->ln();
        $this->Cell(98,5,'',0,0,'C');

        $this->Cell(30,5,'KEPALA BIRO ADMINISTRASI AKADEMIK',0,0,'C');
        $x = $this->GetX();
        $y = $this->GetY();

        $this->ln(20);


        $this->SetFont('Arial','U',8);

        $this->Cell(98,5,'',0,0,'C');

        $this->Cell(30,5,'ROULY G RATNA S, ST., MM',0,0,'C');
        $this->ln(3);

        $this->SetFont('Arial','',8); 

        $this->Cell(98,5,'',0,0,'C');

        //$pdf->Cell(10,5,'',0,0,'C');

        $this->Cell(30,5,'0607108',0,0,'C');


        $this->image('http://172.16.2.42:801/assets/ttd_baa.png',($x-35),$y,30);
    }

}

$pdf = new PDF("P","mm", "A5");
//Instanciation of inherited class
foreach ($rows as $isi) {
    
    //$qqq=$this->db->query('SELECT * FROM tbl_verifikasi_krs WHERE npm_mahasiswa LIKE "%'.$isi->NIMHSMSMHS.'%" AND tahunajaran = '.$this->session->userdata('tahunajaran').'')->row();

    $qr = $isi->kd_krs_verifikasi;

    $nurfan = $this->app_model->get_detail_krs_mahasiswa($isi->kd_krs)->result();

$pdf->SetMargins(3, 5 ,0);
$pdf->AliasNbPages();
$pdf->AddPage();

$tahunajar=substr($isi->kd_krs, 12,4);

$a=substr($isi->kd_krs, 16,1);

        if ($a == 1) {
            $b = 'Ganjil';
        } else {
            $b = 'Genap';
        }

//ob_end_flush();

$q=$this->db->query('SELECT a.`NIMHSMSMHS`,a.`NMMHSMSMHS`,a.`TAHUNMSMHS`,a.`SMAWLMSMHS`,b.`kd_prodi`,b.`prodi`,c.`fakultas`,d.`kd_krs`,d.`kd_matakuliah`,e.`id_pembimbing` 

                    FROM tbl_mahasiswa a

                    JOIN tbl_jurusan_prodi b ON a.`KDPSTMSMHS` = b.`kd_prodi`

                    JOIN tbl_fakultas c ON c.`kd_fakultas` = b.`kd_fakultas`

                    JOIN tbl_krs d ON d.`npm_mahasiswa` = a.`NIMHSMSMHS`

                    JOIN tbl_verifikasi_krs e ON e.`kd_krs` = d.`kd_krs`

                    WHERE e.kd_krs = "'.$isi->kd_krs.'"')->row();

$smtr = $this->db->query("select distinct semester_krs from tbl_krs where kd_krs = '".$isi->kd_krs."'")->row()->semester_krs;


$pdf->SetFont('Arial','',8); 

$pdf->Cell(25,5,'Fakultas',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,$q->fakultas,0,0,'L');

$pdf->Cell(25,5,'Semester',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,$smtr,0,0,'L');



$pdf->ln(4);


$pdf->Cell(25,5,'Program Studi',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,$q->prodi,0,0,'L');

$pdf->Cell(25,5,'Tahun Akademik',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,''.$tahunajar.'/'.$b.'',0,0,'L');

//substr($ta,0,4).'/'.$gg

$pdf->ln(5);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(25,5,'Identitas Mahasiswa',0,0);

$pdf->Cell(2,5,'',0,0,'C');

$pdf->Cell(45,5,'',0,0,'C');

$pdf->Cell(25,5,'Identitas Dosen PA',0,0);

$pdf->Cell(2,5,'',0,0,'L');

$pdf->Cell(45,5,'',0,0,'L');



$pdf->ln(4);
$pdf->SetFont('Arial','',8);
$pdf->Cell(25,5,'Nama Mahasiswa',0,0);

$pdf->Cell(2,5,':',0,0,'C');

//$pdf->Cell(60,10,$q->NMMHSMSMHS,0,0,'L');

$current_y = $pdf->GetY();
$current_x = $pdf->GetX();

$cell_width = 45;
$pdf->MultiCell($cell_width, 5, $q->NMMHSMSMHS, 0,'L',false);

$pdf->ln(5);

$x = 45;
$pdf->SetXY($current_x + $x, $current_y);

//$pdf->MultiCell(50, 5, $q->NMMHSMSMHS, 0,'L',false);

$pdf->Cell(25,5,'Nama Dosen PA',0,0);

$pdf->Cell(2,5,':',0,0,'C');

//$pdf->Cell(60,10,$q->nama,0,0,'L')MultiCell(60, 10, $q->nama, 0,'L');
$pdf->MultiCell(45, 5, get_nm_pa($q->id_pembimbing), 0,'L');

 $pdf->ln(5);

$pdf->Cell(25,5,'NPM',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,$q->NIMHSMSMHS,0,0,'L');

$pdf->Cell(25,5,'NID',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,$q->id_pembimbing,0,0,'L');


$pdf->ln(7);

$pdf->SetLeftMargin(4);

$pdf->SetFont('Arial','',7); 

$pdf->Cell(5,5,'NO',1,0,'C');

$pdf->Cell(18,5,'KODE MK',1,0,'C');

$pdf->Cell(83,5,'NAMA MATAKULIAH',1,0,'C');

$pdf->Cell(26,5,'PARAF PENGAWAS',1,0,'C');

$pdf->Cell(10,5,'KET',1,1,'C');



$no=1;
$nooo=0;
$total_sks=0;
// $tt=count($mk_krs);

foreach ($nurfan as $isi) {


    $bisaujiankah = $this->app_model->bisa_ujiankah($isi->kd_jadwal,$isi->npm_mahasiswa);
    if ($bisaujiankah > 74.99) {
       
    
    // if ( $no == 0) {
    //  $no++;
    // } else {
    $pdf->SetFont('Arial','',6);
        $pdf->Cell(5,5,$no,1,0,'C');

    $pdf->Cell(18,5,$isi->kd_matakuliah,1,0,'C');



    //$mk = $this->db->query('SELECT * from tbl_matakuliah where kd_matakuliah = "'.$isi->kd_matakuliah.'" AND kd_prodi = "'.$isi->kd_prodi.'"')->row();


    // $this->db->select('*');
    // $this->db->distinct('nama_matakuliah');
    // $this->db->from('tbl_matakuliah');
    // $this->db->where('kd_matakuliah', $isi->kd_matakuliah);
    // $this->db->where('kd_prodi', $isi->kd_prodi);
    // $mk=$this->db->get()->result();

    //var_dump($mk);

    //$hitung_mk = strlen($mk->nama_matakuliah);

        // if ($hitung_mk >= 38) {
        //  $pdf->SetFont('Arial','',8); 
        // }elseif ($hitung_mk >= 42) {
        //  $pdf->SetFont('Arial','',7); 
        // }    

    //foreach ($mk as $mk) {
    $pdf->SetFont('Arial','',6);
    $aaa = 0;
    //foreach ($mk as $isi) {
    
        $pdf->Cell(83,5,$isi->nama_matakuliah,1,0,'L');
    //$pdf->MultiCell(50, 5, $q->nama, 0,'L');

    $pdf->SetFont('Arial','',6); 
    //$pdf->Cell(6,5,$mk->sks_matakuliah,1,0,'C');

    $q=$no%2;

    if ($q == 0) {
        $pdf->Cell(13,5,'',1,0,'L');
        $pdf->Cell(13,5,$no,1,0,'L');
    }elseif ($q == 1) {
        $pdf->Cell(13,5,$no,1,0,'L');
        $pdf->Cell(13,5,'',1,0,'L');
    }

    

    $pdf->Cell(10,5,'',1,1,'C');

    $total_sks = $total_sks+$isi->sks_matakuliah;
    
    $no++;    
    
    
    }

    
    //}
}



$pdf->ln(8);


$pdf->SetFont('Arial','B',8);

$pdf->Cell(70,5,'PERHATIAN',0,0);
$pdf->ln(3);
$pdf->SetFont('Arial','',8);
$pdf->Cell(70,5,'Pada saat ujian peserta harus',0,0);
$pdf->ln(3);
$pdf->Cell(4,5,'1.',0,0);
$pdf->Cell(50,5,'Mahasiswa harus membawa kartu ujian pada saat ujian',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'2.',0,0);
$pdf->Cell(50,5,'Kartu ini tidak boleh rusak atau hilang',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'3.',0,0);
$pdf->Cell(50,5,'Jika kartu ini rusak/hilang harap lapor ke Fakultas / BAA',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'4.',0,0);
$pdf->Cell(50,5,'Membawa Perlengkapan ujian : kartu peserta ujian, alat tulis, kartu mahasiswa',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'5.',0,0);
$pdf->Cell(50,5,'Mengenakan pakaian yang telah di tentukan, yaitu :',0,0,'L');

$pdf->ln(3);
$pdf->Cell(7,5,'',0,0);
$pdf->Cell(10,5,'pria',0,0);
$pdf->Cell(2,5,':',0,0,'C');
$pdf->Cell(50,5,'Kemeja putih lengan panjang, celana panjang hitam (bukan bahan jeans)',0,0,'L');

$pdf->ln(3);
$pdf->Cell(7,5,'',0,0);
$pdf->Cell(10,5,'wanita',0,0);
$pdf->Cell(2,5,':',0,0,'C');
$pdf->Cell(50,5,'rok hitam (bukan bahan jeans), dan sepatu tertutup',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'6.',0,0);
$pdf->Cell(50,5,'Dilarang bekerja sama dan menyontek atau membuka buku (kecuali open book)',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'7.',0,0);
$pdf->Cell(50,5,'Datang dan meninggalkan ruang ujian sesuai jadwal ujian',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'8.',0,0);
$pdf->Cell(50,5,'Mengumpulkan berkas ujian kepada pengawas bila telah selesai',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'9.',0,0);
$pdf->Cell(50,5,'Mengisi data absensi dan lembar jawaban secara cermat dan benar',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'10.',0,0);
$pdf->Cell(50,5,'Menjaga ketertiban dan ketenangan selama ujian berlangsung',0,0,'L');
date_default_timezone_set('Asia/Jakarta'); 
// if (is_null($qr)) {
//     //$pdf->image(base_url().'/QRImage/'.$qr.'.png',65,190,15);
// } else {
//     $pdf->image(base_url().'QRImage/'.$qr.'.png',65,190,15);
// }
//$pdf->Cell(170,5,'Jakarta,'.date('d-m-Y').'',0,0,'R');

//$this->load->view('footer.php');

}//enforeach
$pdf->Output('KartuUAS'.'_'.date('ymd_his').'.PDF','I');
?> 