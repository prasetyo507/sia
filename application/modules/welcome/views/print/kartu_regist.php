<?php
//var_dump($rows);die();

define('FPDF_FONTPATH','font/');
require('fpdf.php');

class PDF extends FPDF
{
//Page header
function Header()
{
$this->SetMargins(3, 5 ,0);
$this->SetFont('Arial','',8); 

$this->image('http://172.16.2.42:801/assets/ubhara.png',16,33,73);
$this->image('http://172.16.2.42:801/assets/logo.gif',5,6,13);

$this->Cell(100,5,'KARTU REGISTRASI CALON MAHASISWA BARU',0,2,'C');

$this->Cell(100,5,'UNIVERSITAS BHAYANGKARA',0,2,'C');

$this->Cell(100,5,'JAKARTA RAYA',0,1,'C');

$this->Ln(1);

$this->Cell(100,0,'',1,1,'C');
}

//Page footer
function Footer()
{

    // $this->ln(11);

    // $this->SetFont('Arial','B',8);
    // $this->Cell(98,5,'',0,0,'C');

    // $this->Cell(30,5,'JAKARTA , '.date('d-m-Y'),0,0,'C');
    // $this->ln();
    // $this->Cell(98,5,'',0,0,'C');

    // $this->Cell(30,5,'KEPALA BIRO ADMINISTRASI AKADEMIK',0,0,'C');
    // $x = $this->GetX();
    // $y = $this->GetY();

    // $this->ln(20);


    // $this->SetFont('Arial','U',8); 

    // $this->Cell(98,5,'',0,0,'C');

    // $this->Cell(30,5,'ROULY G RATNA S, ST., MM',0,0,'C');
    // $this->ln(3);

    // $this->SetFont('Arial','',8); 

    // $this->Cell(98,5,'',0,0,'C');

    // //$pdf->Cell(10,5,'',0,0,'C');

    // $this->Cell(30,5,'0607108',0,0,'C');


    // $this->image('http://172.16.2.42:801/assets/ttd_baa.png',($x-35),$y,30);
}

}

$pdf = new PDF("P","mm", array(105,148));
//Instanciation of inherited class


$pdf->SetMargins(3, 5 ,0);
$pdf->AliasNbPages();
$pdf->AddPage();


// $pdf->ln(4);
// $pdf->SetFont('Arial','',8);
// $pdf->Cell(12,5,'Nama',0,0);

// $pdf->Cell(2,5,':',0,0,'C');

// //$pdf->Cell(60,10,$q->NMMHSMSMHS,0,0,'L');

// $current_y = $pdf->GetY();
// $current_x = $pdf->GetX();

// $cell_width = 45;
// $pdf->MultiCell($cell_width, 5, $camaba->nama, 0,'L',false);

// $pdf->ln(5);

// $x = 45;
// $pdf->SetXY($current_x + $x, $current_y);

// //$pdf->MultiCell(50, 5, $q->NMMHSMSMHS, 0,'L',false);

// $pdf->Cell(25,5,'ID Registrasi',0,0);

// $pdf->Cell(2,5,':',0,0,'C');

// //$pdf->Cell(60,10,$q->nama,0,0,'L')MultiCell(60, 10, $q->nama, 0,'L');
// $pdf->MultiCell(45, 5, $camaba->ID_registrasi, 0,'L');

  $pdf->ln(5);

$pdf->Cell(20,5,'ID Registrasi',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,$camaba->ID_registrasi,0,1,'L');

$pdf->Cell(20,5,'Nama',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,$camaba->email,0,1,'L');

$pdf->Cell(20,5,'Telepon',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,$camaba->tlp,0,1,'L');

$pdf->Cell(20,5,'Email',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,$camaba->email,0,1,'L');

$pdf->Cell(20,5,'Asal Sekolah',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,$camaba->asal_skl,0,1,'L');

$pdf->Cell(20,5,'Pilihan Prodi',0,0);

$pdf->Cell(2,5,':',0,0,'C');

$pdf->Cell(45,5,$camaba->prodi,0,0,'L');

$pdf->ln(10);

$pdf->ln(5);
$pdf->SetFont('Arial','B',8);

$pdf->Cell(28,5,'',0,0,'C');
$pdf->Cell(15,5,'Username',0,0);
$pdf->Cell(5,5,':',0,0,'C');
$pdf->Cell(25,5,$user,0,1,'L');

$pdf->Cell(28,5,'',0,0,'C');
$pdf->Cell(15,5,'Password',0,0);
$pdf->Cell(5,5,':',0,0,'C');
$pdf->Cell(25,5,$pass,0,0,'L');

$pdf->ln(12);

$pdf->SetFont('Arial','B',8);

$pdf->Cell(70,5,'LANGKAH YANG HARUS DILAKUKAN',0,0);
$pdf->ln(3);
$pdf->SetFont('Arial','',8);
$pdf->Cell(70,5,'',0,0);
$pdf->ln(3);
$pdf->Cell(4,5,'1.',0,0);
$pdf->Cell(50,5,'Calon mahasiswa baru login pada website sia.ubharajaya.ac.id. ',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'2.',0,0);
$pdf->Cell(50,5,'Mengisi formulir online yang telah di sediakan pada akun anda. ',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'3.',0,0);
$pdf->Cell(50,5,'Print formulir online yang telah anda isi.',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'4.',0,0);
$pdf->Cell(50,5,'Menyerahkan formulir pendaftaran ke bagian pemasaran untuk',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'',0,0);
$pdf->Cell(50,5,'pengambilan kartu ujian penerimaan mahasiswa baru.',0,0,'L');


$pdf->ln(3);
$pdf->Cell(4,5,'5.',0,0);
$pdf->Cell(50,5,'Bagi peserta yang telah lulus tes penerimaan, harus melakukan ',0,0,'L');

$pdf->ln(3);
$pdf->Cell(4,5,'',0,0);
$pdf->Cell(50,5,'verifikasi data ulang di Biro Administrasi & Akademik.',0,0,'L');


date_default_timezone_set('Asia/Jakarta'); 
if ($qr == '') {
    //$pdf->image(base_url().'QRImage/'.$qr.'.png',65,190,15);
} else {
    $pdf->image(base_url().'QRImage/'.$qr.'.png',46,120,15);
}

//$pdf->Cell(170,5,'Jakarta,'.date('d-m-Y').'',0,0,'R');

//$this->load->view('footer.php');

$pdf->Output('Kartu_Registrasi_'.$camaba->ID_registrasi.''.'_'.date('ymd_his').'.PDF','I');
?> 