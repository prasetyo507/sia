<?php
//die($kd_krs);
//var_dump($kdver);die();
require('rotation.php');

// define('FPDF_FONTPATH','font/');
// require('fpdf.php');


class PDF extends PDF_Rotate


//class PDF extends FPDF
{
	//Page header
	function Header()
	{
	$this->SetMargins(7, 5 ,0);
	$this->SetFont('Arial','',11); 

	$this->image('http://172.16.2.42:801/assets/ubhara.png',50,50,90);
	$this->image('http://172.16.2.42:801/assets/logo.gif',15,6,13);

	$this->Cell(200,5,'KARTU RENCANA STUDI',0,2,'C');

	$this->Cell(200,5,'UNIVERSITAS BHAYANGKARA',0,2,'C');

	$this->Cell(200,5,'JAKARTA RAYA',0,1,'C');

	$this->Ln(1);

	$this->Cell(250,0,'',1,1,'C');
	
	
	}

	//Page footer
	function Footer()
	{
        //$kdver = $this->db->query('SELECT kd_krs_verifikasi from tbl_verifikasi_krs WHERE npm_mahasiswa = "'.$npm.'"')->row();
		// $this->SetFont('Arial','B',20);
		// $this->SetTextColor(158,158,158);
        //$this->image(base_url().'QRImage/'.$qr.'.png',65,180,20);
		//$this->RotatedText(8,200,base_url().'QRImage/'.$qr.'.png',0);
	}

	function RotatedText($x, $y, $txt, $angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle,$x,$y);
		$this->Text($x,$y,$txt);
		$this->Rotate(0);
	}

}

$pdf = new PDF("P","mm", "A4");
foreach ($mahasiswa as $key) {

    $kd_krs = $key->kd_krs;
    $npm = substr($key->kd_krs, 0,12);
    $ta  = substr($key->kd_krs, 12,4);
    $kdver = $this->db->query('SELECT kd_krs_verifikasi from tbl_verifikasi_krs WHERE kd_krs = "'.$key->kd_krs.'" order by kd_krs_verifikasi desc limit 1')->row();
    $footer = $this->db->select('npm_mahasiswa,id_pembimbing')
                            ->from('tbl_verifikasi_krs')
                            ->like('npm_mahasiswa', $npm,'both')
                            ->get()->row();
    
    $a=substr($key->kd_krs, 16,1);

    if ($a == 1) {
        $b = 'Ganjil';
    } else {
        $b = 'Genap';
    }

    $data['gg']  = $b;

    $qr = $kdver->kd_krs_verifikasi;
    //Instanciation of inherited class
        
        $mhs=$this->db->query('SELECT e.`kd_krs_verifikasi`,a.`NIMHSMSMHS`,a.`NMMHSMSMHS`,a.`TAHUNMSMHS`,a.`SMAWLMSMHS`,b.`kd_prodi`,b.`prodi`,c.`fakultas`,d.`kd_krs`,d.`kd_matakuliah`,e.`id_pembimbing`,f.`nama`,f.`nid` 

                        FROM tbl_mahasiswa a

                        JOIN tbl_jurusan_prodi b ON a.`KDPSTMSMHS` = b.`kd_prodi`

                        JOIN tbl_fakultas c ON c.`kd_fakultas` = b.`kd_fakultas`

                        JOIN tbl_krs d ON d.`npm_mahasiswa` = a.`NIMHSMSMHS`

                        JOIN tbl_verifikasi_krs e ON e.`kd_krs` = d.`kd_krs`

                        JOIN tbl_karyawan f ON e.`id_pembimbing` = f.`nid`

                        WHERE e.`kd_krs` = "'.$kd_krs.'" ')->row();

    	// $mhs = $this->db->like('NIMHSMSMHS', $npm)
    	// 				->get('tbl_mahasiswa')->row();

    	$dosen = $this->db->like('nid', $mhs->id_pembimbing)
    					->get('tbl_karyawan')->row();

        
        $qqq=$this->db->query('SELECT * FROM tbl_verifikasi_krs WHERE kd_krs LIKE "%'.$kd_krs.'%" and tahunajaran = "'.$ta.'"')->row();

        $nurfan = $this->app_model->get_detail_krs_mahasiswa($kd_krs)->result();



    $pdf->SetMargins(7, 5 ,0);
    $pdf->AliasNbPages();
    $pdf->AddPage();

    $tahunajar=substr($kd_krs, 12,4);

    $a=substr($kd_krs, 16,1);

            if ($a == 1) {
                $b = 'Ganjil';
            } else {
                $b = 'Genap';
            }

    //ob_end_flush();



    $smtr = $this->db->query("select distinct semester_krs from tbl_krs where kd_krs = '".$kd_krs."'")->row()->semester_krs;

    // $nilai = 0;

    // $nilai2 = 0;

    // $nilai3 = 0;

    // $jml = count($ipk);

    // foreach ($ipk as $key => $value) {

    // $nilai = $ipk->BOBOTTRLNM * $ipk->sks_matakuliah;

    // $nilai2 = $nilai2 + $nilai;

    // }



    // $nilai3=$nilai2/$jml;


    $pdf->SetFont('Arial','',8); 

    $pdf->Cell(40,5,'Fakultas',0,0);

    $pdf->Cell(5,5,':',0,0,'C');

    $pdf->Cell(60,5,$mhs->fakultas,0,0,'L');

    $pdf->Cell(40,5,'Semester',0,0);

    $pdf->Cell(5,5,':',0,0,'C');

    $pdf->Cell(60,5,$smtr,0,0,'L');

    $pdf->ln(4);



    $pdf->Cell(40,5,'Program Studi',0,0);

    $pdf->Cell(5,5,':',0,0,'C');

    $pdf->Cell(60,5,$mhs->prodi,0,0,'L');

    $pdf->Cell(40,5,'Tahun Akademik',0,0);

    $pdf->Cell(5,5,':',0,0,'C');

    $pdf->Cell(60,5,''.$tahunajar.'/'.$b.'',0,0,'L');

    //substr($ta,0,4).'/'.$gg


    $pdf->ln(5);
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(40,5,'Identitas Mahasiswa',0,0);

    $pdf->Cell(5,5,'',0,0,'C');

    $pdf->Cell(60,5,'',0,0,'C');

    $pdf->Cell(40,5,'Identitas Dosen PA',0,0);

    $pdf->Cell(5,5,'',0,0,'L');

    $pdf->Cell(60,5,'',0,0,'L');



    $pdf->ln(4);
    $pdf->SetFont('Arial','',8);
    $pdf->Cell(40,5,'Nama Mahasiswa',0,0);

    $pdf->Cell(5,5,':',0,0,'C');

    //$pdf->Cell(60,10,$q->NMMHSMSMHS,0,0,'L');

    $current_y = $pdf->GetY();
    $current_x = $pdf->GetX();

    $cell_width = 60;
    $pdf->MultiCell($cell_width, 5, $mhs->NMMHSMSMHS, 0,'L',false);

    $pdf->ln(5);

    $x = 60;
    $pdf->SetXY($current_x + $x, $current_y);

    //$pdf->MultiCell(50, 5, $q->NMMHSMSMHS, 0,'L',false);

    $pdf->Cell(40,5,'Nama Dosen PA',0,0);

    $pdf->Cell(5,5,':',0,0,'C');

    //$pdf->Cell(60,10,$q->nama,0,0,'L')MultiCell(60, 10, $q->nama, 0,'L');
    $pdf->MultiCell(60, 5, $mhs->nama, 0,'L');

    $pdf->ln(5);

    $pdf->Cell(40,5,'NPM',0,0);

    $pdf->Cell(5,5,':',0,0,'C');

    $pdf->Cell(60,5,$mhs->NIMHSMSMHS,0,0,'L');

    $pdf->Cell(40,5,'NID',0,0);

    $pdf->Cell(5,5,':',0,0,'C');

    $pdf->Cell(60,5,$mhs->nid,0,0,'L');


    $pdf->ln(7);

    $pdf->SetLeftMargin(7);

    $pdf->SetFont('Arial','',10); 

    $pdf->Cell(10,5,'NO',1,0,'C');

    $pdf->Cell(20,5,'KODE MK',1,0,'C');

    $pdf->Cell(50,5,'NAMA MATAKULIAH',1,0,'C');

    $pdf->Cell(50,5,'DOSEN',1,0,'C');

    $pdf->Cell(50,5,'KELAS',1,0,'C');

    $pdf->Cell(10,5,'SKS',1,1,'C');


    $no=1;
    $nooo=0;
    $total_sks=0;

    foreach ($nurfan as $isi) {
    	$pdf->SetFont('Arial','',9); 


        // if ( $no == 0) {
        //  $no++;
        // } else {
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(10,10,$no,1,0,'C');

        $pdf->Cell(20,10,$isi->kd_matakuliah,1,0,'C');

        //$pdf->SetFont('Arial','',10);
        $aaa = 0;
        $current_y = $pdf->GetY();
        $current_x = $pdf->GetX();

        $cell_width = 50;
        $nama = $isi->nama_matakuliah;
        if (strlen($nama) > 25) {
            $hegh = 5;
        } else {
            $hegh = 10;
        }
        
        $pdf->MultiCell($cell_width, $hegh, $nama, 'T.R','L',false);

        //$pdf->ln(5);

        $x = 50;
        $pdf->SetXY($current_x + $x, $current_y);

        $current_y = $pdf->GetY();
        $current_x = $pdf->GetX();

        $cell_width = 50;
        $nama = $isi->nama;
        if (strlen($nama) > 30) {
            $hegh = 5;
        } else {
            $hegh = 10;
        }
        
        $pdf->MultiCell($cell_width, $hegh, $nama, 'T.R','L',false);

        //$pdf->ln(5);

        $x = 50;
        $pdf->SetXY($current_x + $x, $current_y);

        $current_y = $pdf->GetY();
        $current_x = $pdf->GetX();

        $cell_width = 50;
        $nama = $isi->kelas;
        if (strlen($nama) > 30) {
            $hegh = 5;
        } else {
            $hegh = 10;
        }
        
        $pdf->MultiCell($cell_width, $hegh, $nama, 'T.R','L',false);

        //$pdf->ln(5);

        $x = 50;
        $pdf->SetXY($current_x + $x, $current_y);
        //$pdf->Cell(100,10,$isi->nama_matakuliah,1,0,'L');
        $pdf->SetFont('Arial','',9); 

        $pdf->Cell(10,10,$isi->sks_matakuliah,1,1,'C');

        $total_sks = $total_sks+$isi->sks_matakuliah;
        
        $no++;    
        
    }
    $pdf->Cell(180,8,'Total SKS',1,0,'C');

    $pdf->Cell(10,8,$total_sks,1,1,'C');


    $pdf->ln(10);

    $pdf->SetFont('Arial','',8); 

    $pdf->Cell(20,5,'',0,0,'C');

    $pdf->Cell(40,5,'',0,0,'C');

    $pdf->Cell(70,5,'',0,0,'C');

    $pdf->Cell(40,5,date("d-m-Y"),0,0,'C');

    $pdf->ln();

    $pdf->SetFont('Arial','',8); 

    $pdf->Cell(20,5,'',0,0,'C');

    $pdf->Cell(40,5,$mhs->nama,0,0,'C');

    $pdf->Cell(70,5,'',0,0,'C');

    $pdf->Cell(40,5,$mhs->NMMHSMSMHS,0,0,'C');




    $pdf->ln(20);

    $pdf->SetFont('Arial','',8); 

    $pdf->Cell(20,5,'',0,0,'C');

    $pdf->Cell(40,5,$mhs->nid,0,0,'C');

    $pdf->Cell(70,5,'',0,0,'C');

    $pdf->Cell(40,5,$mhs->NIMHSMSMHS,0,0,'C');


    $pdf->ln(70);

    //$pdf->image('http://172.16.2.42:801/assets/'.$kdver.'.png',105,6,13);
    // for ($i=0; $i < 5; $i++) { 
    // 	$pdf->Cell(60,5,'b1679a17fe76a967680e63d1e2549611',0,0,'C');
    // }
    $pdf->SetMargins(100, 300 ,0);
    if ($mhs->kd_krs_verifikasi != '') {
        //$pdf->image(base_url().'QRImage/'.$mhs->kd_krs_verifikasi.'.png',90,200,20);
        $pdf->SetFont('Arial','I',8);
        //$pdf->Cell(190,5,$mhs->kd_krs_verifikasi,'I',0,'C');
    }



    date_default_timezone_set('Asia/Jakarta'); 

}
$pdf->Output('KRS_20151_PSIKOLOGI_2016.PDF','I');

?> 