<!DOCTYPE html>
<html>
<head>
	<title>Halaman kepepet</title>
</head>
<body>
	<h3>Update Verifikasi KRS</h3>
	<form action="<?= base_url('welcome/test_function/updateverif'); ?>" method="post">
		<input type="text" name="npm" placeholder="NPM nya">
		<select name="status">
			<option>--Pilih Status--</option>
			<option value="1">Lunas</option>
			<option value="10">FORM 14</option>
		</select>
		<input type="text" name="tahun" placeholder="tahun ajaran">
		<input type="submit" value="Serang!">
	</form>
	<br>
	<br>
	<hr>
	<h3>Update Kelas Mahasiswa</h3>
	<form action="<?= base_url('welcome/test_function/updatekelas'); ?>" method="post">
		<input type="text" name="npm" placeholder="NPM nya">
		<select name="kelas">
			<option>--Pilih Kelas--</option>
			<option value="PG">PAGI</option>
			<option value="SR">SORE</option>
			<option value="PK">P2k</option>
		</select>
		<input type="submit" value="Serang!">
	</form>
	<br>
	<br>
	<hr>
	<h3>Hapus KRS double</h3>
	<form action="<?= base_url('welcome/test_function/hapuskrsdobel') ?>" method="post">
		<input type="text" name="npm" placeholder="masukin npm nya">
		<input type="submit" value="go!">
	</form>
	<br>
	<br>
	<hr>
	<h3>Hapus Verifikasi KRS double</h3>
	<form action="<?= base_url('welcome/test_function/hapusverif') ?>" method="post">
		<input type="text" name="npm" placeholder="masukin npm nya">
		<input type="submit" value="go!">
	</form>
	<br>
	<br>
	<hr>
	<h3>Tambah MK Konversi</h3>
	<form action="<?= base_url('welcome/test_function/tambahkonversimk') ?>" method="post">
		<input type="text" name="baru" placeholder="masukin kode baru">
		<input type="text" name="lama" placeholder="masukin kode lama">
		<input type="text" name="prodi" placeholder="masukin kode prodi">
		<input type="submit" value="go!">
	</form>

</body>
</html>