<?php 
$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
//border
$excel->getActiveSheet()->getStyle('B3:C4')->applyFromArray($BStyle);
$excel->getActiveSheet()->getStyle('E3:F4')->applyFromArray($BStyle);

$excel->setActiveSheetIndex(0);
//name the worksheet
$excel->getActiveSheet()->setTitle('Data Nilai ...');
//header
$excel->getActiveSheet()->setCellValue('B1', 'HASIL EVALUASI BELAJAR MENGAJAR');
$getta = $this->app_model->getdetail('tbl_tahunakademik','kode',$rows->kd_tahunajaran,'kode','asc')->row();
$excel->getActiveSheet()->setCellValue('B2', ''.$prodi->prodi.' '.$getta->tahun_akademik);
$excel->getActiveSheet()->setCellValue('B3', 'Mata Kuliah');
$excel->getActiveSheet()->setCellValue('C3', $rows->nama_matakuliah);
$excel->getActiveSheet()->setCellValue('B4', 'NID / Nama');
$excel->getActiveSheet()->setCellValue('C4', $rows->kd_dosen.' / '.$rows->nama);
$excel->getActiveSheet()->setCellValue('E3', 'Jumlah MHS');
$excel->getActiveSheet()->setCellValue('F3', $jmlh->jumlah);
$excel->getActiveSheet()->setCellValue('E4', 'Semester / Kelas');
$excel->getActiveSheet()->setCellValue('F4', $rows->semester_matakuliah.' / '.$rows->kelas);

//isi mahasiswa
$excel->getActiveSheet()->setCellValue('A6', 'NO');
$excel->getActiveSheet()->setCellValue('B6', 'NPM');
$excel->getActiveSheet()->setCellValue('C6', 'NAMA');
$excel->getActiveSheet()->setCellValue('D6', 'NILAI AKHIR');
$excel->getActiveSheet()->setCellValue('E6', 'NILAI HURUF');

//ISI DATABASE
//mahasiswa
$xx = 8;$no=1;foreach ($ping as $key) {
	$excel->getActiveSheet()->setCellValue('A'.$xx.'', $no);
	$excel->getActiveSheet()->setCellValue('B'.$xx.'', $key->NIMHSMSMHS);
	$excel->getActiveSheet()->setCellValue('C'.$xx.'', $key->NMMHSMSMHS);

	//getdata

	$nilai = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,10,$rows->kd_matakuliah)->row()->nilai;

	//NILAI AKHIR
	//$logged = $this->session->userdata('sess_login');
	//if (($logged['userid'] == '74101') or ($logged['userid'] == '61101')) {
		$hrf = '=IF(D'.$xx.'>100,"T",IF(D'.$xx.'>=80,"A",IF(D'.$xx.'>=76,"A-",IF(D'.$xx.'>=72,"B+",IF(D'.$xx.'>=68,"B",IF(D'.$xx.'>=64,"B-",IF(D'.$xx.'>=60,"C+",IF(D'.$xx.'>=56,"C",IF(D'.$xx.'>=45,"D","E")))))))))';
	//}else{
		//$hrf= '=IF(D'.$xx.'>100,"T",IF(D'.$xx.'>=80,"A",IF(D'.$xx.'>=65,"B",IF(D'.$xx.'>=55,"C",IF(D'.$xx.'>=45,"D","E")))))';
	//}
	$excel->getActiveSheet()->setCellValue('D'.$xx.'', $nilai)
	                      ->setCellValue('E'.$xx.'',$hrf);
	                      
	$xx++;$no++;
}
$xw = $xx - 1;
$excel->getActiveSheet()->getStyle('A6:E'.$xw.'')->applyFromArray($BStyle);
$xy = $xx + 1;
//footer
$excel->getActiveSheet()->setCellValue('B'.$xy.'', 'Keterangan Nilai');

//$logged = $this->session->userdata('sess_login');
//if (($logged['userid'] == '61101') or ($logged['userid'] == '74101')) {
	$a = $xy+1; 
	$b = $xy+2;
	$c = $xy+3;
	$d = $xy+4;
	$e = $xy+5;
	$excel->getActiveSheet()->setCellValue('B'.$a.'', '80-100');
	$excel->getActiveSheet()->setCellValue('B'.$b.'', '76-79.99');
	$excel->getActiveSheet()->setCellValue('B'.$c.'', '72-75.99');
	$excel->getActiveSheet()->setCellValue('B'.$d.'', '68-71.99');
	$excel->getActiveSheet()->setCellValue('D'.$a.'', '64-67.99');
	$excel->getActiveSheet()->setCellValue('D'.$b.'', '60-63.99');
	$excel->getActiveSheet()->setCellValue('D'.$c.'', '56-59.99');
	$excel->getActiveSheet()->setCellValue('D'.$d.'', '45-55.99');
	$excel->getActiveSheet()->setCellValue('D'.$e.'', '0-39.99');
	$excel->getActiveSheet()->setCellValue('C'.$a.'', 'A');
	$excel->getActiveSheet()->setCellValue('C'.$b.'', 'A-');
	$excel->getActiveSheet()->setCellValue('C'.$c.'', 'B+');
	$excel->getActiveSheet()->setCellValue('C'.$d.'', 'B');
	$excel->getActiveSheet()->setCellValue('E'.$a.'', 'B-');
	$excel->getActiveSheet()->setCellValue('E'.$b.'', 'C+');
	$excel->getActiveSheet()->setCellValue('E'.$c.'', 'C');
	$excel->getActiveSheet()->setCellValue('E'.$d.'', 'D');
	$excel->getActiveSheet()->setCellValue('E'.$e.'', 'E');
	$excel->getActiveSheet()->getStyle('B'.$xy.':E'.$e.'')->applyFromArray($BStyle);
	$excel->getActiveSheet()->setCellValue('L'.$xy.'', 'Jakarta,'.date('d-m-Y').'');
	$excel->getActiveSheet()->setCellValue('L'.$a.'', 'Dosen Yang Bersangkutan');
	$excel->getActiveSheet()->setCellValue('L'.$d.'', $rows->nama);
// }else{
// 	$a = $xy+1; 
// 	$b = $xy+2;
// 	$c = $xy+3;
// 	$d = $xy+4;
// 	$excel->getActiveSheet()->setCellValue('B'.$a.'', '80-100');
// 	$excel->getActiveSheet()->setCellValue('B'.$b.'', '65-79.99');
// 	$excel->getActiveSheet()->setCellValue('B'.$c.'', '55-64.99');
// 	$excel->getActiveSheet()->setCellValue('D'.$a.'', '45-54.99');
// 	$excel->getActiveSheet()->setCellValue('D'.$b.'', '0-44.99');
// 	$excel->getActiveSheet()->setCellValue('C'.$a.'', 'A');
// 	$excel->getActiveSheet()->setCellValue('C'.$b.'', 'B');
// 	$excel->getActiveSheet()->setCellValue('C'.$c.'', 'C');
// 	$excel->getActiveSheet()->setCellValue('E'.$a.'', 'D');
// 	$excel->getActiveSheet()->setCellValue('E'.$b.'', 'E');
// 	$excel->getActiveSheet()->getStyle('B'.$xy.':E'.$c.'')->applyFromArray($BStyle);
// 	$excel->getActiveSheet()->setCellValue('G'.$xy.'', 'Jakarta,'.date('d-m-Y').'');
// 	$excel->getActiveSheet()->setCellValue('G'.$a.'', 'Dosen Yang Bersangkutan');
// 	$excel->getActiveSheet()->setCellValue('G'.$d.'', $rows->nama);
// }
$e = $xy+7;
$excel->getActiveSheet()->setCellValue('B'.$e.'', '*Harap Memasukkan Nilai Maksimal 2 Angka Di Belakang Koma');
//merge cell
$excel->getActiveSheet()->mergeCells('B'.$xy.':E'.$xy.'');
$excel->getActiveSheet()->mergeCells('G'.$xy.':I'.$xy.'');
$excel->getActiveSheet()->mergeCells('G'.$a.':I'.$a.'');
$excel->getActiveSheet()->mergeCells('G'.$d.':I'.$d.'');
$excel->getActiveSheet()->mergeCells('B1:E1');
$excel->getActiveSheet()->mergeCells('B2:E2');
$excel->getActiveSheet()->mergeCells('H3:H4');
$excel->getActiveSheet()->mergeCells('A6:A7');
$excel->getActiveSheet()->mergeCells('B6:B7');
$excel->getActiveSheet()->mergeCells('C6:C7');
$excel->getActiveSheet()->mergeCells('D6:D7');
$excel->getActiveSheet()->mergeCells('E6:E7');
//change the font size
$excel->getActiveSheet()->getStyle('B1:B2')->getFont()->setSize(12);
$excel->getActiveSheet()->getStyle()->getFont()->setSize(11);

//align
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
);

$excel->getActiveSheet()->getStyle("B1:B2")->applyFromArray($style);
$excel->getActiveSheet()->getStyle("A6:N7")->applyFromArray($style);
//$excel->getDefaultStyle()->applyFromArray($style);

$filename = 'Form_Nilai_'.str_replace(' ', '_', $rows->nama_matakuliah)."_".str_replace(' ', '_', $rows->kelas).'.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>