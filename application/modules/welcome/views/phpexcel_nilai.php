<?php 
$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
//border
$excel->getActiveSheet()->getStyle('B3:C4')->applyFromArray($BStyle);
$excel->getActiveSheet()->getStyle('E3:F4')->applyFromArray($BStyle);
$excel->getActiveSheet()->getStyle('H3:L4')->applyFromArray($BStyle);

$excel->setActiveSheetIndex(0);
//name the worksheet
$excel->getActiveSheet()->setTitle('Data Nilai ...');
//header
$excel->getActiveSheet()->setCellValue('E1', 'HASIL EVALUASI BELAJAR MENGAJAR');
$getta = $this->app_model->getdetail('tbl_tahunakademik','kode',$rows->kd_tahunajaran,'kode','asc')->row();
$excel->getActiveSheet()->setCellValue('E2', ''.$prodi->prodi.' '.$getta->tahun_akademik);
$excel->getActiveSheet()->setCellValue('B3', 'Mata Kuliah');
$excel->getActiveSheet()->setCellValue('C3', $rows->nama_matakuliah);
$excel->getActiveSheet()->setCellValue('B4', 'NID / Nama');
$excel->getActiveSheet()->setCellValue('C4', $rows->kd_dosen.' / '.$rows->nama);
$excel->getActiveSheet()->setCellValue('E3', 'Jumlah MHS');
$excel->getActiveSheet()->setCellValue('F3', $jmlh->jumlah);
$excel->getActiveSheet()->setCellValue('E4', 'Semester / Kelas');
$excel->getActiveSheet()->setCellValue('F4', $rows->semester_kd_matakuliah.' / '.$rows->kelas);
$excel->getActiveSheet()->setCellValue('H3', 'Bobot Nilai');
$excel->getActiveSheet()->setCellValue('I3', 'ABSENSI');
$excel->getActiveSheet()->setCellValue('J3', 'TUGAS');
$excel->getActiveSheet()->setCellValue('K3', 'UTS');
$excel->getActiveSheet()->setCellValue('L3', 'UAS');
$excel->getActiveSheet()->setCellValue('I4', '10%');
$excel->getActiveSheet()->setCellValue('J4', '20%');
$excel->getActiveSheet()->setCellValue('K4', '30%');
$excel->getActiveSheet()->setCellValue('L4', '40%');

//isi mahasiswa
$excel->getActiveSheet()->setCellValue('A6', 'NO');
$excel->getActiveSheet()->setCellValue('B6', 'NPM');
$excel->getActiveSheet()->setCellValue('C6', 'NAMA');
$excel->getActiveSheet()->setCellValue('D6', 'KEHADIRAN');
$excel->getActiveSheet()->setCellValue('D7', 'DOSEN');
$excel->getActiveSheet()->setCellValue('E7', 'MHS');
$excel->getActiveSheet()->setCellValue('F6', 'NILAI TUGAS');
$excel->getActiveSheet()->setCellValue('F7', 'TUGAS 1');
$excel->getActiveSheet()->setCellValue('G7', 'TUGAS 2');
$excel->getActiveSheet()->setCellValue('H7', 'TUGAS 3');
$excel->getActiveSheet()->setCellValue('I7', 'TUGAS 4');
$excel->getActiveSheet()->setCellValue('J7', 'TUGAS 5');
$excel->getActiveSheet()->setCellValue('K6', 'ABSEN (%)');
$excel->getActiveSheet()->setCellValue('L6', 'RATA-RATA TUGAS');
$excel->getActiveSheet()->setCellValue('M6', 'UTS');
$excel->getActiveSheet()->setCellValue('N6', 'UAS');
$excel->getActiveSheet()->setCellValue('O6', 'NILAI AKHIR');
$excel->getActiveSheet()->setCellValue('P6', 'NILAI HURUF');

//ISI DATABASE
//mahasiswa
$xx = 8;
$no = 1;
foreach ($ping as $key) {

	$excel->getActiveSheet()->setCellValue('A'.$xx.'', $no);
	$excel->getActiveSheet()->setCellValue('B'.$xx.'', $key->NIMHSMSMHS);
	$excel->getActiveSheet()->setCellValue('C'.$xx.'', $key->NMMHSMSMHS);

	//getdata
	//$absendosen = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs where kd_jadwal = '".$rows->kd_jadwal."'")->row();
	$absenmhs 	= $this->db->query("SELECT COUNT(npm_mahasiswa) as dua FROM tbl_absensi_mhs_new_20171 
									where kd_jadwal = '".$rows->kd_jadwal."' and npm_mahasiswa = '".$key->NIMHSMSMHS."' 
									and (kehadiran IS NULL or kehadiran = 'H')")->row();
	// $tugas = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,1,$rows->kd_matakuliah)->row()->nilai;
	// $tugas1 = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,5,$rows->kd_matakuliah)->row()->nilai;
	// $tugas2 = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,6,$rows->kd_matakuliah)->row()->nilai;
	// $tugas3 = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,7,$rows->kd_matakuliah)->row()->nilai;
	// $tugas4 = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,8,$rows->kd_matakuliah)->row()->nilai;
	// $tugas5 = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,9,$rows->kd_matakuliah)->row()->nilai;
	// $uts = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,3,$rows->kd_matakuliah)->row()->nilai;
	// $uas = $this->app_model->getnilai($rows->kd_jadwal,$key->NIMHSMSMHS,4,$rows->kd_matakuliah)->row()->nilai;

	//nilai
	$qqq 	= $this->db->query("SELECT distinct tipe,nilai from tbl_nilai_detail 
								where npm_mahasiswa = '".str_replace(' ', '', $key->NIMHSMSMHS)."' 
								and kd_jadwal = '".$rows->kd_jadwal."' ")->result();

	$tugas 	= 0;
	$uts 	= 0;
	$uas 	= 0;
	$tugas1 = 0;
	$tugas2 = 0;
	$tugas3 = 0;
	$tugas4 = 0;
	$tugas5 = 0;

	foreach ($qqq as $key) {
		switch ($key->tipe) {
			case '1':
				$tugas = $key->nilai;
				break;
			case '3':
				$uts = $key->nilai;
				break;
			case '4':
				$uas = $key->nilai;
				break;
			case '5':
				$tugas1 = $key->nilai;
				break;
			case '6':
				$tugas2 = $key->nilai;
				break;
			case '7':
				$tugas3 = $key->nilai;
				break;
			case '8':
				$tugas4 = $key->nilai;
				break;
			case '9':
				$tugas5 = $key->nilai;
				break;
		}
	}

	//ABSENSI
	$excel->getActiveSheet()->setCellValue('D'.$xx.'', $absendosen->satu)
	                      ->setCellValue('E'.$xx.'', $absenmhs->dua)
	                      ->setCellValue('K'.$xx.'', '=(E'.$xx.'/D'.$xx.')*100');
	//TUGAS
	if (is_null($tugas1) or $tugas1 == '') {
		$tugas1 = '-';
	 	//$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	} elseif ($tugas1 == 0) {
		$tugas1 = "";
	}

	if (is_null($tugas2) or $tugas2 == '') {
		$tugas2 = '-';
		//$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	} elseif ($tugas2 == 0) {
		$tugas2 = "";
	}

	if (is_null($tugas3) or $tugas3 == '') {
		$tugas3 = '-';
		//$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	} elseif ($tugas3 == 0) {
		$tugas3 = "";
	}

	if (is_null($tugas4) or $tugas4 == '') {
		$tugas4 = '-';
		//$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	} elseif ($tugas4 == 0) {
		$tugas4 = "";
	}

	if (is_null($tugas5) or $tugas5 == '') {
		$tugas5 = '-';
		//$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	} elseif ($tugas5 == 0) {
		$tugas5 = "";
	}

	if (is_null($tugas) or $tugas == '') {
		$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')';
	} else {
	 	$tugasakhir = '=AVERAGE(F'.$xx.':J'.$xx.')'; //$tugas;
	}
	
	 
	$excel->getActiveSheet()->setCellValue('F'.$xx.'', $tugas1)
	                      ->setCellValue('G'.$xx.'', $tugas2)
	                      ->setCellValue('H'.$xx.'', $tugas3)
	                      ->setCellValue('I'.$xx.'', $tugas4)
	                      ->setCellValue('J'.$xx.'', $tugas5)
	                      ->setCellValue('L'.$xx.'', $tugasakhir);

	//NILAI AKHIR
	//$logged = $this->session->userdata('sess_login');
	//if (($logged['userid'] == '74101') or ($logged['userid'] == '61101')) {
		$hrf = '=IF(O'.$xx.'>=80,"A",IF(O'.$xx.'>=76,"A-",IF(O'.$xx.'>=72,"B+",IF(O'.$xx.'>=68,"B",IF(O'.$xx.'>=64,"B-",IF(O'.$xx.'>=60,"C+",IF(O'.$xx.'>=56,"C",IF(O'.$xx.'>=45,"D","E"))))))))';
	//}else{
		//$hrf= '=IF(O'.$xx.'>=80,"A",IF(O'.$xx.'>=65,"B",IF(O'.$xx.'>=55,"C",IF(O'.$xx.'>=45,"D","E"))))';
	//}
	$excel->getActiveSheet()->setCellValue('M'.$xx.'', $uts)
	                      ->setCellValue('N'.$xx.'', $uas)
	                      ->setCellValue('O'.$xx.'', '=(0.1*K'.$xx.')+(0.2*L'.$xx.')+(0.3*M'.$xx.')+(0.4*N'.$xx.')')
	                      ->setCellValue('P'.$xx.'',$hrf);
	                      
	$xx++;$no++;
}
$xw = $xx - 1;
$excel->getActiveSheet()->getStyle('A6:P'.$xw.'')->applyFromArray($BStyle);
$xy = $xx + 1;
//footer
$excel->getActiveSheet()->setCellValue('B'.$xy.'', 'Keterangan Nilai');

//$logged = $this->session->userdata('sess_login');
//if (($logged['userid'] == '74101')) {
	$a = $xy+1; 
	$b = $xy+2;
	$c = $xy+3;
	$d = $xy+4;
	$f = $xy+5;
	$excel->getActiveSheet()->setCellValue('B'.$a.'', '80-100');
	$excel->getActiveSheet()->setCellValue('B'.$b.'', '76-79.99');
	$excel->getActiveSheet()->setCellValue('B'.$c.'', '72-75.99');
	$excel->getActiveSheet()->setCellValue('B'.$d.'', '68-71.99');
	$excel->getActiveSheet()->setCellValue('D'.$a.'', '64-67.99');
	$excel->getActiveSheet()->setCellValue('D'.$b.'', '60-63.99');
	$excel->getActiveSheet()->setCellValue('D'.$c.'', '56-59.99');
	$excel->getActiveSheet()->setCellValue('D'.$d.'', '45-55.99');
	$excel->getActiveSheet()->setCellValue('D'.$f.'', '0-44.99');
	$excel->getActiveSheet()->setCellValue('C'.$a.'', 'A');
	$excel->getActiveSheet()->setCellValue('C'.$b.'', 'A-');
	$excel->getActiveSheet()->setCellValue('C'.$c.'', 'B+');
	$excel->getActiveSheet()->setCellValue('C'.$d.'', 'B');
	$excel->getActiveSheet()->setCellValue('E'.$a.'', 'B-');
	$excel->getActiveSheet()->setCellValue('E'.$b.'', 'C+');
	$excel->getActiveSheet()->setCellValue('E'.$c.'', 'C');
	$excel->getActiveSheet()->setCellValue('E'.$d.'', 'D');
	$excel->getActiveSheet()->setCellValue('E'.$f.'', 'E');
	$excel->getActiveSheet()->getStyle('B'.$xy.':E'.$f.'')->applyFromArray($BStyle);
	$excel->getActiveSheet()->setCellValue('L'.$xy.'', 'Jakarta,'.date('d-m-Y').'');
	$excel->getActiveSheet()->setCellValue('L'.$a.'', 'Dosen Yang Bersangkutan');
	$excel->getActiveSheet()->setCellValue('L'.$d.'', $rows->nama);
//}else{
	// $a = $xy+1; 
	// $b = $xy+2;
	// $c = $xy+3;
	// $d = $xy+4;
	// $excel->getActiveSheet()->setCellValue('B'.$a.'', '80-100');
	// $excel->getActiveSheet()->setCellValue('B'.$b.'', '65-79.99');
	// $excel->getActiveSheet()->setCellValue('B'.$c.'', '55-64.99');
	// $excel->getActiveSheet()->setCellValue('D'.$a.'', '45-54.99');
	// $excel->getActiveSheet()->setCellValue('D'.$b.'', '0-44.99');
	// $excel->getActiveSheet()->setCellValue('C'.$a.'', 'A');
	// $excel->getActiveSheet()->setCellValue('C'.$b.'', 'B');
	// $excel->getActiveSheet()->setCellValue('C'.$c.'', 'C');
	// $excel->getActiveSheet()->setCellValue('E'.$a.'', 'D');
	// $excel->getActiveSheet()->setCellValue('E'.$b.'', 'E');
	// $excel->getActiveSheet()->getStyle('B'.$xy.':E'.$c.'')->applyFromArray($BStyle);
	// $excel->getActiveSheet()->setCellValue('L'.$xy.'', 'Jakarta,'.date('d-m-Y').'');
	// $excel->getActiveSheet()->setCellValue('L'.$a.'', 'Dosen Yang Bersangkutan');
	// $excel->getActiveSheet()->setCellValue('L'.$d.'', $rows->nama);
//}
$e = $xy+6;
$excel->getActiveSheet()->setCellValue('B'.$e.'', '*Harap Memasukkan Nilai Maksimal 2 Angka Di Belakang Koma');
//merge cell
$excel->getActiveSheet()->mergeCells('B'.$xy.':E'.$xy.'');
$excel->getActiveSheet()->mergeCells('L'.$xy.':N'.$xy.'');
$excel->getActiveSheet()->mergeCells('L'.$a.':N'.$a.'');
$excel->getActiveSheet()->mergeCells('L'.$d.':N'.$d.'');
$excel->getActiveSheet()->mergeCells('E1:H1');
$excel->getActiveSheet()->mergeCells('E2:H2');
$excel->getActiveSheet()->mergeCells('H3:H4');
$excel->getActiveSheet()->mergeCells('A6:A7');
$excel->getActiveSheet()->mergeCells('B6:B7');
$excel->getActiveSheet()->mergeCells('C6:C7');
$excel->getActiveSheet()->mergeCells('D6:E6');
$excel->getActiveSheet()->mergeCells('M6:M7');
$excel->getActiveSheet()->mergeCells('N6:N7');
$excel->getActiveSheet()->mergeCells('O6:O7');
$excel->getActiveSheet()->mergeCells('P6:P7');
$excel->getActiveSheet()->mergeCells('K6:K7');
$excel->getActiveSheet()->mergeCells('L6:L7');
$excel->getActiveSheet()->mergeCells('F6:J6');
//change the font size
$excel->getActiveSheet()->getStyle('E1:E2')->getFont()->setSize(12);
$excel->getActiveSheet()->getStyle()->getFont()->setSize(11);

//align
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
);

$excel->getActiveSheet()->getStyle("E1:E2")->applyFromArray($style);
$excel->getActiveSheet()->getStyle("A6:N7")->applyFromArray($style);
//$excel->getDefaultStyle()->applyFromArray($style);

$filename = 'Form_Nilai_'.str_replace(' ', '_', $rows->nama_matakuliah)."_".str_replace(' ', '_', $rows->kelas).'.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>