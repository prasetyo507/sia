<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_function extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Cfpdf');
		//error_reporting(0);
	}

	public function index()
	{
		//$data['npm'] = $logged = $this->session->userdata('sess_login');

		$id = '201310115139201516';
		$data['npm'] = substr($id, 0,12);
		$data['ta']  = substr($id, 12,4);
		
		$a=substr($id, 16,1);

		if ($a == 1) {
			$b = 'Ganjil';
		} else {
			$b = 'Genap';
		}
		

		$data['gg']  = $b;

		$this->load->view('welcome/print/test_krs_old',$data);
	}

	function insertLostKrs()
	{
		$npm = ["201720252003","201720252003","201720252004","201720252005","201720252006","201720252007","201720252008"];

		$data = [];
		foreach ($npm as $key) {
			$data[] = [
				'npm_mahasiswa' => $key,
				'kd_matakuliah' => [
					'MHK-6436',
					'MHK-6321',
					'MHK-6320',
					'MHK-5210',
					'MHK-5209',
					'MHK-5208',
					'MHK-5206'
				],
				'semester_krs' => '2',
				'kd_krs' => $key.'1',
				'kd_jadwal' => [
					'74101/dRldd4/0010',
					'74101/Qhgnj8/011',
					'74101/ozhgmP/009',
					'74101/EZ6qNv/007',
					'74101/jFveDe/008',
					'74101/if7LCt/012',
					'74101/13QEf9/013'
				]
			];
		}

		echo "<pre>";
		print_r ($data);
		echo "</pre>";exit();
	}

	function tex($yr)
	{
		$act = getactyear();

		$selisih = $act - $yr;
		
		if ($selisih == 1) {
			echo $selisih/0.5;
		} elseif ($selisih == 0) {
			echo 1;
		} else {
			$awal = substr($selisih, 0,1);
			$akhir = substr($selisih, 1,1);

			if ($akhir == 0) {
				$end = 1;
			} else {
				$end = $akhir/0.5;
			}
			

			$res = ($awal/0.5)+($end);
			echo $res;
		}
	}

	function resetsmt($akt,$prodi)
	{
		$get = $this->db->select('npm_mahasiswa,kd_krs')
		                ->from('tbl_verifikasi_krs')
						->where('kd_jurusan', $prodi)
		                ->where('tahunajaran', '20182')
						->like('npm_mahasiswa',$akt,'after')
		                ->get()->result();

		foreach ($get as $key) {
			$smt = $this->db->select('SMAWLMSMHS')
			                ->from('tbl_mahasiswa')
			                ->where('NIMHSMSMHS', $key->npm_mahasiswa)
			                ->get()->row()->SMAWLMSMHS;

			$getsmt = $this->app_model->get_semester($smt);
			$set = ['semester_krs' => $getsmt];

			$this->db->where('kd_krs', $key->kd_krs);
			$this->db->update('tbl_krs', $set);
		}
	}

	function loadgbr()
	{
		imagecreatefromgif(base_url('assets/logo.gif'));
	}

	function import_to_jdl_feeder()
	{
		$actyear 	= getactyear();
		$get 		= $this->db->query("SELECT * from tbl_jadwal_matkul where kd_tahunajaran = '".$actyear."'")->result();
		$me = [];
		foreach ($get as $key) {
			$me[] = [
				'kd_jadwal' => $key->kd_jadwal,
				'id_matakuliah' => $key->id_matakuliah,
				'kd_matakuliah' => $key->kd_matakuliah,
				'kd_dosen' => $key->kd_dosen,
				'kd_ruangan' => $key->kd_ruangan,
				'hari' => $key->hari,
				'waktu_mulai' => $key->waktu_mulai,
				'waktu_selesai' => $key->waktu_selesai,
				'kelas' => $key->kelas,
				'waktu_kelas' => $key->waktu_kelas,
				'kd_tahunajaran' => $key->kd_tahunajaran,
				'audit_date' => $key->audit_date,
				'audit_user' => $key->audit_user
			];
		}
		// var_dump($data);exit();

		$this->db->insert_batch('tbl_jadwal_matkul_feeder', $me);
	}

	function import_to_krs_feeder()
	{
		ini_set('memory_limit', '512M');
		$actyear = getactyear();
		$get = $this->db->query("SELECT * from tbl_krs where kd_krs like CONCAT (npm_mahasiswa,'".$actyear."%')")->result();

		// $you = [];
		foreach ($get as $key) {
			$you[] = [
				'npm_mahasiswa' => $key->npm_mahasiswa,
				'kd_matakuliah' => $key->kd_matakuliah,
				'semester_krs' => $key->semester_krs,
				'kd_krs' => $key->kd_krs,
				'kd_jadwal' => $key->kd_jadwal
			];
		}
		/*echo "<pre>";
        var_dump($you);exit();
        echo "</pre>";*/
		$this->db->insert_batch('tbl_krs_feeder', $you);
		echo "berhasil";
	}

	function move_absen($prodi)
	{
		ini_set('memory_limit', '1024M');
		$act = getactyear();
		$get = $this->db->query("SELECT a.*,b.kd_tahunajaran from tbl_absensi_mhs_new_20171 a join tbl_jadwal_matkul b on  a.kd_jadwal = b.kd_jadwal where b.kd_tahunajaran = '".$act."' AND a.kd_jadwal LIKE '".$prodi."%'")->result();

		$loop = [];
		foreach ($get as $key) {
			$loop[] = [
                 'kd_jadwal' => $key->kd_jadwal,
                 'npm_mahasiswa' => $key->npm_mahasiswa,
                 'tanggal' => $key->tanggal,
                 'pertemuan' => $key->pertemuan,
                 'kehadiran' => $key->kehadiran,
                 'sblm_update' => $key->sblm_update,
                 'user_update' => $key->user_update,
                 'transaksi' => md5($key->kd_jadwal.$key->pertemuan)
			];
		}

		$this->db->insert_batch('tbl_absensi_mhs_new_20181', $loop);
	}

	function transaksi_absen($prodi)
	{
		$get = $this->db->query("SELECT distinct transaksi from tbl_absensi_mhs_new_20172 where kd_jadwal like '".$prodi."%'")->result();
		foreach ($get as $key) {
			$detail = $this->db->query("SELECT distinct transaksi,kd_jadwal from  tbl_absensi_mhs_new_20172 where transaksi = '".$key->transaksi."'")->row();
			$sakit = $this->db->query("SELECT count(npm_mahasiswa) as sakit from tbl_absensi_mhs_new_20172 where transaksi = '".$key->transaksi."' and kehadiran = 'S'")->row()->sakit;
			$ijin = $this->db->query("SELECT count(npm_mahasiswa) as ijin from tbl_absensi_mhs_new_20172 where transaksi = '".$key->transaksi."' and kehadiran = 'I'")->row()->ijin;
			$alfa = $this->db->query("SELECT count(npm_mahasiswa) as alfa from tbl_absensi_mhs_new_20172 where transaksi = '".$key->transaksi."' and kehadiran = 'A'")->row()->alfa;
			$hadir = $this->db->query("SELECT count(npm_mahasiswa) as hadir from tbl_absensi_mhs_new_20172 where transaksi = '".$key->transaksi."' and kehadiran = 'H'")->row()->hadir;


			$obj = ['kd_transaksi' => $detail->transaksi,'kd_jadwal' => $detail->kd_jadwal,'sakit' => $sakit,'alfa' => $alfa,'ijin' => $ijin, 'hadir' => $hadir, 'timestamp' => date('Y-m-d')];
			$this->db->insert('tbl_transaksi_absen_20172', $obj);
		}
	}

	function testqrcode()
	{
		$this->load->library('ciqrcode');

		//$params['data'] = 'This is a text to encode become QR Code';
		$params['data'] = 'http://203.77.248.54/sia/welcome/welcome/cekkodeverifikasi/94c55bdc4a694d81d8f21ed60690fd87';
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'QRImage/tes.png';
		$this->ciqrcode->generate($params);

		echo '<img src="'.base_url().'QRImage/tes.png" />';
	}

	function testslugci()
	{
		error_reporting(0);
		//$mhs = $this->app_model->getdata('tbl_verifikasi_krs','npm_mahasiswa','asc')->result();
		//foreach ($mhs as $value) {
			$nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS','201410325031','NIMHSMSMHS','asc')->row();
			$slug = url_title($nama->NMMHSMSMHS, '_', TRUE);
			$data['slug_url'] = $slug;
			$this->app_model->updatedata('tbl_verifikasi_krs','npm_mahasiswa','201410325031',$data);
			echo $slug;
			echo "<br/>";
		//}
	}

	function printkhsall(){
		$logged = $this->session->userdata('sess_login');
		$prodi = $logged['userid'];
		// $data['ips'] = $this->app_model->get_ips_mahasiswa($npm, $id)->row()->ips;
		// $data['ipk'] = $this->app_model->get_ipk_mahasiswa($npm)->row()->ipk;
		//var_dump($data['ipk']);exit();
		
		$data['semester'] = $id;
	// $data['mahasiswa'] = $this->db->query('SELECT DISTINCT npm_mahasiswa as halaman FROM tbl_krs a
	// 	JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` in ("201210415050","201210415018","201210415085")')->result();
		$data['mahasiswa'] = $this->db->query('SELECT DISTINCT npm_mahasiswa as halaman FROM tbl_krs a
		JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS` WHERE b.`KDPSTMSMHS` = 73201 and TAHUNMSMHS = 2012  order by b.`NIMHSMSMHS`  ')->result();
		// $data['krs'] = $this->db->query('SELECT * FROM tbl_krs a 
		// JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` where a.`npm_mahasiswa` = "'.$npm.'" and kd_prodi = '.$prodi.'')->result();
		$data['detail_khs'] = $this->app_model->get_detail_khs_mahasiswa($npm, $id)->result();
		$this->load->view('welcome/print/khs_pdf_all',$data);
	}

	function printkhsall2(){
		$logged = $this->session->userdata('sess_login');
		$prodi = $logged['userid'];
		// $data['ips'] = $this->app_model->get_ips_mahasiswa($npm, $id)->row()->ips;
		// $data['ipk'] = $this->app_model->get_ipk_mahasiswa($npm)->row()->ipk;
		//var_dump($data['ipk']);exit();
		
		$data['semester'] = $id;
	// $data['mahasiswa'] = $this->db->query('SELECT DISTINCT npm_mahasiswa as halaman FROM tbl_krs a
	// 	JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` in ("201210415050","201210415018","201210415085")')->result();
		$data['mahasiswa'] = $this->db->query('SELECT DISTINCT npm_mahasiswa as halaman FROM tbl_krs a
		JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS` WHERE b.`KDPSTMSMHS` = 73201 and TAHUNMSMHS = 2014  order by b.`NIMHSMSMHS`')->result();
		// $data['krs'] = $this->db->query('SELECT * FROM tbl_krs a 
		// JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` where a.`npm_mahasiswa` = "'.$npm.'" and kd_prodi = '.$prodi.'')->result();
		$data['detail_khs'] = $this->app_model->get_detail_khs_mahasiswa($npm, $id)->result();
		$this->load->view('welcome/print/khs_pdf_all',$data);
	}

	function testphpexcel()
	{
		$data['ping'] = $this->db->query('SELECT * FROM tbl_mahasiswa a
										JOIN tbl_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
										JOIN tbl_jadwal_matkul c ON b.`kd_jadwal` = c.`kd_jadwal`
										WHERE c.`id_jadwal` = 3801 ORDER BY a.`NMMHSMSMHS` asc')->result();
		$this->load->library('excel');
		$this->load->view('welcome/phpexcel_nilai',$data);
	}

	function insertips($prodi)
	{
		// die('ini');exit();
		$before = yearBefore();
		$mhs 	= $this->db->query("SELECT DISTINCT a.kd_krs,b.npm_mahasiswa FROM tbl_verifikasi_krs a 
									JOIN tbl_krs b ON a.kd_krs = b.kd_krs 
									WHERE a.kd_jurusan = '".$prodi."' 
									AND b.kd_krs LIKE CONCAT(b.npm_mahasiswa,'".$before."%')
									AND b.npm_mahasiswa like '2016%'")->result();
		foreach ($mhs as $key) {
			$count 	= $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,
										a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
	                    				JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
	                    				WHERE a.`kd_transaksi_nilai` IS NOT NULL 
	                    				AND kd_prodi = "'.$prodi.'" AND NIMHSTRLNM = "'.$key->npm_mahasiswa.'" 
	                    				AND THSMSTRLNM = "'.substr($key->kd_krs, 12,5).'" ')->result();

	        $st = 0;
	        $ht = 0;
	        foreach ($count as $iso) {
	            $h = 0;

	            $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
	            $ht= $ht + $h;

	            $st= $st+$iso->sks_matakuliah;
	        }

	        $ips_nr = $ht/$st;
	        /*
	        $data['kd_krs'] = $key->kd_krs;
	        $data['tahunajaran'] = substr($key->kd_krs, 12,5);
	        $data['npm_mahasiswa'] = $key->npm_mahasiswa;
	        $data['ips'] = number_format($ips_nr,2);
	        $data['semester'] = $this->db->query("SELECT distinct semester_krs from tbl_krs where kd_krs = '".$data['kd_krs']."'")->row()->semester_krs;
	        $data['sks'] = $st;*/
	        //$data['last_transact'] = date('Y-m-d');
	        //var_dump($data);exit();
	        // $this->app_model->insertdata('tbl_ips',$data);
	        $this->db->distinct();
	        $this->db->select('semester_krs');
	        $this->db->from('tbl_krs');
	        $this->db->where('kd_krs', $key->kd_krs);
	        $smt = $this->db->get()->row()->semester_krs;

	        $object[] = [
	        	'kd_krs' => $key->kd_krs,
	        	'tahunajaran' => substr($key->kd_krs, 12,5),
	        	'npm_mahasiswa' => $key->npm_mahasiswa,
	        	'ips' => number_format($ips_nr,2),
	        	'semester' => $smt,
	        	'sks' => $st
	        ];
	    }
	    
	    // echo "<pre>";
	    // print_r ($object);exit();
	    // echo "</pre>";
	    
	    $this->db->insert_batch('tbl_ips_copy', $object);
	}

	function insertkhs($prodi)
	{
		//die('ini');exit();
		$mhsprodi = $this->db->query("SELECT DISTINCT ips,npm_mahasiswa,sks FROM tbl_ips a
										join tbl_mahasiswa b on a.npm_mahasiswa = b.NIMHSMSMHS
										WHERE b.KDPSTMSMHS = '".$prodi."' AND a.kd_krs LIKE CONCAT(a.npm_mahasiswa,'20161%') and a.tahunajaran = '20161' order by npm_mahasiswa desc")->result();
		foreach ($mhsprodi as $key) {
			$getipk = $this->db->query("select * from tbl_khs where tahunajaran = '20152' and npm_mahasiswa = '".$key->npm_mahasiswa."'")->row();
			//var_dump($getipk);exit();

			if ((substr($key->npm_mahasiswa, 0,4) == '2016') and ($getipk == NULL)) {
				//die('disana');
				$data['tahunajaran'] = '20161';
		        $data['npm_mahasiswa'] = $key->npm_mahasiswa;
		        $data['ips'] = $key->ips;
		        $data['total_sks'] = $key->sks;
		        $data['ipk'] = $key->ips;
			} else {
				//die('disini');
				$data['tahunajaran'] = '20161';
		        $data['npm_mahasiswa'] = $key->npm_mahasiswa;
		        $data['ips'] = $key->ips;
		        $totsks = $getipk->total_sks+$key->sks;
		        $data['total_sks'] = $totsks;
		        $ipk = ($getipk->total_sks*$getipk->ipk+$key->ips*$key->sks)/$totsks;
		        $data['ipk'] = number_format($ipk,2);
			}
			
	        //var_dump($data);exit();
	        $this->app_model->insertdata('tbl_khs',$data);
	    }
	}

	function generatePa()
	{
		$gett = $this->db->query("SELECT * from tbl_verifikasi_krs where tahunajaran = '20171' and npm_mahasiswa NOT IN (SELECT distinct npm_mahasiswa from tbl_pa)")->result();



		foreach ($gett as $val) {
			$data[] = array(
				'kd_dosen' => $val->id_pembimbing,
				'npm_mahasiswa'	=> $val->npm_mahasiswa,
				'kelas_mhs'	=> $val->kelas,
				'audit_user'	=> $val->kd_jurusan,
				'audit_time'	=> date('Y-m-d H:i:s')
			);

			// var_dump($data);exit();

		$this->db->insert_batch('tbl_pa', $data);
		}
	}

	// ini yg dibawah kalo lagi kedesek aja makenya

	function kepepet()
	{
		$this->load->view('kepepet_view');
	}

	function updatekelas()
	{
		$data = array(
			'kelas' => $this->input->post('kelas', TRUE)
		);
		$this->db->where('npm_mahasiswa', $this->input->post('npm', TRUE));
		$this->db->where('tahunajaran', '20172');
		$this->db->update('tbl_verifikasi_krs', $data);
	}

	function updateverif()
	{
		$data = array(
			'status_verifikasi' => $this->input->post('status', TRUE)
		);
		$this->db->where('npm_mahasiswa', $this->input->post('npm', TRUE));
		$this->db->where('tahunajaran', $this->input->post('tahun', TRUE));
		$this->db->update('tbl_verifikasi_krs', $data);
	}

	function hapuskrsdobel()
	{
		$this->db->like('kd_krs', $this->input->post('npm', TRUE).'20172', 'AFTER');
		$this->db->order_by('kd_matakuliah', 'asc');
		$data['q'] = $this->db->get('tbl_krs')->result();

		$this->load->view('halamanmagils', $data);
	}

	function deletekrs($kd,$id)
	{
		$this->db->where('id_krs', $id);
		$this->db->where('kd_krs', $kd);
		$this->db->delete('tbl_krs');

		echo '<script>history.go(-1);</script>';
	}

	function hapusverif($id)
	{
		$this->db->like('kd_krs', $id.'20172', 'AFTER');
		$data['q'] = $this->db->get('tbl_verifikasi_krs')->result();

		$this->load->view('halamanmagils2', $data);
	}

	function deleteverifkrs($kd,$id)
	{
		$this->db->where('id_verifikasi', $id);
		$this->db->where('kd_krs', $kd);
		$this->db->delete('tbl_verifikasi_krs');

		echo '<script>history.go(-1);</script>';
	}

	function tambahkonversimk()
	{
		$new = $this->input->post('baru', TRUE);
		$old = $this->input->post('lama', TRUE);
		$pro = $this->input->post('prodi', TRUE);
		$this->db->where('kd_baru', $new);
		$this->db->where('kd_lama', $old);
		$this->db->where('kd_prodi', $pro);
		$cuy = $this->db->get('tbl_konversi_matkul_temp')->result();

		if (count($cuy) > 0) {
			echo '<script>alert("udah ada!");history.go(-1);</script>';
		} else {
			$data = array(
				'kd_baru'	=> $new,
				'kd_lama'	=> $old,
				'kd_prodi'	=> $pro
			);
			$this->db->insert('tbl_konversi_matkul_temp', $data);
			echo '<script>history.go(-1);</script>';
		}
	}

	function dataTableTest()
	{
		$this->load->view('testdatatable');
	}

	function dataTableServerSide()
	{
		$data = array();

		$hilm = $this->app_model->getdetail('tbl_krs','npm_mahasiswa','201210225054','npm_mahasiswa','asc');

		$no = 1;
		foreach ($hilm->result() as $val) {
				$no++;
				$row = array();
	            $row[] = $val->id_krs;
	            $row[] = $val->kd_krs;
	            $row[] = $val->npm_mahasiswa;
	            $row[] = $val->kd_matakuliah;
	            $row[] = $val->kd_jadwal;
	            $row[] =
	            	'<button>haha</button>'.
	            	'<button>haha</button>'.
	            	'<button>haha</button>';
	 			$data[] = $row;
		}

		$columns = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $hilm->num_rows(),
                    "recordsFiltered" => $hilm->num_rows(),
                    'data' => $data
                );

		echo json_encode($columns);
		    
	}

	function insertkhsilang($uid,$year,$akt)
	{
		$foo = $this->db->query("SELECT npm_mahasiswa,kd_krs from tbl_verifikasi_krs where kd_jurusan = '".$uid."' 
								and tahunajaran = '".$year."' and npm_mahasiswa like '".$akt."%'")->result();

		foreach ($foo as $key) {
			$hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` 
											FROM tbl_transaksi_nilai a
						                    JOIN tbl_matakuliah b 
						                    ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
						                    WHERE a.`kd_transaksi_nilai` IS NOT NULL 
						                    AND kd_prodi = "'.$uid.'" 
						                    AND NIMHSTRLNM = "'.$key->npm_mahasiswa.'" 
						                    AND a.THSMSTRLNM = "'.$year.'"')->result();
			$param = 0;
            $const = 0;
            foreach ($hitung_ips as $iso) {
                $h = 0;

                $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
                $const = $const + $h;

                $param = $param + $iso->sks_matakuliah;
            }
			$ips = $const / $param;

			$jml_sks = $this->db->query('SELECT SUM(mk.`sks_matakuliah`) AS jml_sks FROM tbl_krs krs
										JOIN tbl_matakuliah mk 
										ON krs.`kd_matakuliah` = mk.`kd_matakuliah`
										WHERE krs.`kd_krs` = "'.$key->kd_krs.'" 
										AND mk.`kd_prodi` = "'.$uid.'"')->row()->jml_sks;

			$tahu = substr($year, 0,4);
			$subs = substr($year, 4,1);
			if ($subs == 1) {
				$akad = ($tahu-1).'2';
				$tabl = 'tbl_khs_dump';
			} else {
				$akad = $tahu.'1';
				$tabl = 'tbl_khs';
			}
			
			$sks = $this->db->query("SELECT * from tbl_khs where npm_mahasiswa = '".$key->npm_mahasiswa."' and tahunajaran = '".$akad."'")->row();

        	$ipstry		= number_format($ips, 2);
	        $skss 		= $jml_sks;
	        $awal 		= number_format(($sks->ipk*$sks->total_sks),2);
	        $nambah 	= number_format(($ipstry*$skss),2);
	        $skstot 	= $sks->total_sks+$skss;
	        $ipk 		= number_format(($awal+$nambah)/($skss+$sks->total_sks),2);
	        $sks_total 	= $skstot;

	        $arr = array(
	        	'npm_mahasiswa' => $key->npm_mahasiswa,
	        	'tahunajaran'	=> $year,
	        	'ips'			=> $ips,
	        	'total_sks'		=> $sks_total,
	        	'ipk'			=> $ipk 
	        );

	        $this->db->insert('tbl_khs', $arr);
		}
		
	}

	function printkhsall3($akt,$year){
		$logged = $this->session->userdata('sess_login');
		$prodi = $logged['userid'];
		$data['prodi'] = $logged['userid'];
		$data['akademik'] = $year;
		// $data['ips'] = $this->app_model->get_ips_mahasiswa($npm, $id)->row()->ips;
		// $data['ipk'] = $this->app_model->get_ipk_mahasiswa($npm)->row()->ipk;
		//var_dump($data['ipk']);exit();
		
		//$data['semester'] = $id;
		$data['prodi'] = $prodi;
	// $data['mahasiswa'] = $this->db->query('SELECT DISTINCT npm_mahasiswa as halaman FROM tbl_krs a
	// 	JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` in ("201210415050","201210415018","201210415085")')->result();
		$data['mahasiswa'] = $this->db->query('SELECT DISTINCT npm_mahasiswa as halaman FROM tbl_verifikasi_krs a
												JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS` WHERE b.`KDPSTMSMHS` = "'.$prodi.'"
												and TAHUNMSMHS = "'.$akt.'" AND a.`tahunajaran` = "'.$year.'" order by b.`NIMHSMSMHS`')->result();
		// $data['krs'] = $this->db->query('SELECT * FROM tbl_krs a 
		// JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` where a.`npm_mahasiswa` = "'.$npm.'" and kd_prodi = '.$prodi.'')->result();
		//$data['detail_khs'] = $this->app_model->get_detail_khs_mahasiswa($npm, $id)->result();
		$this->load->view('welcome/print_khs_all',$data);
	}

	function updateujian()
	{
		$data = $this->db->query("SELECT kd_jadwal,id_jadwal from tbl_jadwal_matkul where kd_tahunajaran  = '20181'")->result();
		foreach ($data as $key) {
			$this->db->query("UPDATE tbl_jadwaluji set kd_jadwal = '".$key->kd_jadwal."' where id_jadwal = '".$key->id_jadwal."' and kd_jadwal IS NULL");
		}
	}

	function apiSms()
	{
		if ($this->session->userdata('sess_login')) {
			$curl = curl_init();
				curl_setopt_array($curl, array(
			    CURLOPT_RETURNTRANSFER => 1,
			    CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
			    CURLOPT_POST => true,
			    CURLOPT_POSTFIELDS => array(
			        'user' => 'ubharajaya2_api',
			        'password' => 'U6psYjL',
			        'SMSText' => 'Selamat sore pak, ini testing sms blast via SIA. Salam, Hilmawan',
			        'GSM' => '62811912212'
			    )
			));
			$resp = curl_exec($curl);
			if (!$resp) {
			    die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
			} else {
			    header('Content-type: text/xml'); /*if you want to output to be an xml*/
			    echo $resp;
			}
			curl_close($curl);
		} else {
			echo "login dulu coy";
		}
	}

	function updateTglMsMhs($prodi)
	{
		$this->load->library("Nusoap_lib");
		$data = $this->db->query("SELECT NIMHSMSMHS from tbl_mahasiswa where NIMHSMSMHS like '2018%' and KDPSTMSMHS = '".$prodi."'")->result();

		$url 	= 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;
        $table 	= 'mahasiswa_pt';

		foreach ($data as $keys) {
			$fx = $proxy->GetRecordset($token, $table, "nipd ilike '%".$keys->NIMHSMSMHS."%'", 'nipd asc', 1);

			foreach ($fx['result'] as $val) {
				// data which will update
				$record['tgl_masuk_sp'] = '2018-08-27';

				// key for update
				$key = ['id_reg_pd' => $val['id_reg_pd']];
				
				// bulk of both
				$rec[] = ['key' => $key, 'data' => $record];
			}

			foreach ($rec as $recs) {
	    		$result1 = $proxy->UpdateRecord($token, $table, json_encode($recs));
	    		var_dump($result1);echo "<hr>";
	    	}
		}
	}

	// update tgl masuk mhs
	function updateMsk()
	{
		$this->load->library("Nusoap_lib");

		$url 	= 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;
        $table 	= 'mahasiswa_pt';

		$fx = $proxy->GetRecordset($token, $table, "nipd ilike '2018%' and id_sms = 'a26699d1-68f5-43cd-9eea-183d02932b8a'", 'nipd asc', 1);

		var_dump($fx);exit();

		foreach ($fx['result'] as $val) {
			// data which will update
			$record['tgl_masuk_sp'] = '2018-08-27';

			// key for update
			$key = ['id_reg_pd' => $val['id_reg_pd']];
			
			// bulk of both
			$rec[] = ['key' => $key, 'data' => $record];
		}

		foreach ($rec as $recs) {
    		$result1 = $proxy->UpdateRecord($token, $table, json_encode($recs));
    		var_dump($result1);echo "<hr>";
    	}
	}

	function updateTglUas($prodi)
	{
		$dates = $this->db->query("SELECT id_jadwal,end_date from tbl_jadwaluji where prodi = '".$prodi."' and tipe_uji = '2' and tahunakademik = '20181'")->result();

		foreach ($dates as $key) {
			date_default_timezone_set('Asia/Jakarta');
	        $date = new DateTime($key->end_date);
	        $date->add(new DateInterval('P1D'));
	        $fixend	= $date->format('Y-m-d');

	        $this->db->where('id_jadwal', $key->id_jadwal);
	        $this->db->where('tipe_uji', 2);
	        $this->db->update('tbl_jadwaluji', ['end_date' => $fixend]);
		}
	}

	function selectmax()
	{
		$arr = [
			'55201/2Ejg8a/344','70201/bDf5qz/195','73201/eTWrcq/138','26201/gOqq0m/164','24201/o0z5Vg/074','55201/gqxqs/307','26201/MkSXqr/156','55201/HrUFyE/070','55201/UQvZqk/252','55201/k5CNuG/324','55201/dBCcBR/182','55201/rR2dE/059','26201/53P6b/181','26201/GyKeN/215','55201/M0rp6t/057','74201/JYitmq/195','70201/g1Zah3/224','25201/iFb65x/048','74201/SYWqHc/113','74201/7ulTbq/210','74201/apSRT1/218','74201/QDlE21/229','70201/pcZ1mP/114','61201/3bLq6D/328','70201/D6brxX/201','70201/XKb2vx/190','62201/2V26Kf/157','62201/cECDbV/160','62201/viXIzU/125','62201/oGzjO/127','62201/SMoL5F/145','32201/yR48Mi/014','26201/vQ2NFX/143','26201/kPGhzg/201','26201/DxU4az/263','26201/6t9xzU/269','25201/0HLyny/043','32201/7x8V84/019','74201/eSbGnY/076','74201/FtrQO/077','70201/BQxyhC/113','70201/FuQgab/074','55201/dVEc4k/064','24201/ZI5TUR/036','55201/4M8CRH/061','26201/4jlW0j/186','26201/XIMoFR/177','26201/dqvpBm/020','73201/s4z2IE/030','73201/t64Tl3/047','70201/qmvYqV/063','85202/Rly15k/015','61201/JPsluf/068','61201/zaFJiz/066','62201/0QHs5c/158','74201/Xhbztk/132','26201/SaPdGC/252','26201/u2Vcld/236','61201/Wim2Ma/212','61201/q35jRd/337','26201/GNUZNv/017','62201/oONXV/126','55201/RQfI9G/063','55201/qrMsEb/081','62201/YhRNj7/155','74201/O7UZH8/247','70201/JHJz4r/129','55201/clJvpq/197','55201/GxqNOn/347','55201/uzDak/349','55201/f3Dle2/360','73201/i0LzWH/031','55201/qtU0qh/251','70201/Pr9k32/078','70201/hscuOg/088','70201/Mdf8qe/207','55201/9Uizm/370','74201/HFpcD9/074','74201/r612lZ/304','74201/6LaEBt/200','55201/SgNHGt/179','55201/F53G8C/372','74201/k8ZXYE/057','55201/I1RSq9/360','55201/p8U6j4/225','55201/MmV9UU/095','55201/MlLxC6/118','26201/OYVfTY/255','70201/Whza4S/144','55201/3zgKZX/048','55201/r5Ip7J/060','55201/yWJ72T/343','70201/qcNxop/218','24201/NGyIVn/044','55201/qQZhh/150','55201/3p1NjR/305','70201/ytOm8s/212','73201/q58xCV/168','70201/u29qIq/210','55201/GdiXUg/366','73201/7LeqEZ/087','55201/IqmEh5/264','62201/kGjGX/043','85202/qDpTCI/013','62201/Vfvv6Y/021','55201/OhLEpk/051','55201/CbRvyb/159','55201/QVzLeS/190','26201/2RCNQW/122','73201/xfIyG6/160','74201/vQKTg/015','70201/0di6R/115','26201/dLToCg/062','70201/Srgszj/032','55201/tTCsCe/348','61201/iV8V8H/073','55201/WOVE1V/335','73201/3U9CkV/071','55201/mrWDbX/335','25201/utSszd/022','70201/X8nbr4/181','26201/VPzrpr/022','61201/OTtb9N/035','61201/89PRrP/200','73201/UzQGRr/036','55201/HJ3rJR/315','85202/9WiUr/012','55201/tor8z0/049','85202/9vydfE/009','85202/iyzcZH/008','85202/7i1JjM/007','26201/byjxk1/024','55201/t0I0jC/047','24201/z2EZYo/013','70201/XTnKrd/006','55201/nfP8NS/341','55201/Rr5MDq/121','70201/DpROmr/045','73201/r7yjCs/161','74201/J0fHtg/046','74201/COg6PV/234','74201/Pv3PI2/105','55201/EFn8fM/304','55201/qWcSZW/138','61201/IcOkB3/233','62201/UZKpb/001','62201/YDyWyo/004','62201/CSIUzj/130','70201/5snys4/213','73201/97ulYb/029','74201/3gu2fF/151','26201/a1hokz/260','73201/45vpBP/043','26201/9OULlK/203','74201/M8jZ1z/165','73201/URqqvR/044','62201/a14c1k/134','61201/gZzJqq/205','62201/sudLmd/135','55201/RQuqMa/185','26201/E81dVi/038','26201/qpPtXE/213','70201/7rc40W/155','70201/gp7p1l/168','70201/lB3zTc/118','74201/rjkoCh/233','74201/d4hgqT/269','24201/vkQ7uP/060','26201/N6q504/040','32201/BPLDe5/013','26201/HYF0q4/067','70201/KfebQy/036','26201/1HgfSx/082','70201/a8ttWT/215','70201/8n6NYF/150','70201/eSJxB/016','26201/aNJ3Kk/117','61201/qmIqi/065','26201/Iqc5N0/120','61201/XEkRcl/091','26201/WaDYFG/214','61201/PHuW62/125','26201/p3u0UG/267','73201/TmdGTm/048','73201/7Q31B/041','73201/6XIYma/033','70201/gBC80m/184','70201/X2uxCl/023','62201/lELHN2/022','62201/MdYOqN/128','55201/qk6z6q/188','70201/Y95JMF/075','70201/QgN2nJ/103','55201/NZ2YBa/338','55201/BgI7Vm/355','55201/T07gmM/181','61201/VHbvLp/221','61201/yrvGyt/171','61201/IifuKq/327','61201/FUbmvJ/172','55201/5TyMRb/371','61201/1diZiX/346','70201/qcJNSX/214','26201/lExvXN/200','86206/rf17EZ/035','70201/B7XDj/211','26201/P37vpu/034','73201/O2vtPP/038','73201/HguUdT/050','70201/bPpQF3/117','86206/OMBY5q/034','24201/naLzLW/049','26201/1mbq6e/262','61201/PWvnTq/124','61201/xMyezm/132','73201/iyR4fl/062','26201/IP0nF/229','61201/z0ny6R/196','55201/iNqrWy/326','26201/NaJiJ5/257','62201/Ktj5Yv/171','61201/EhK4Ce/001','62201/vYVZQV/132','26201/SlqNNp/270','61201/GuYbE7/004','55201/yx5LUf/151','55201/qXscPi/222','55201/gMWGuM/139'
		];

		// $const = ',';
		foreach ($arr as $key) {
			$query = $this->db->query("SELECT max(id_file) as id from tbl_file_upload where kode = '".$key."' and waktu like '2019%'")->result();
			foreach ($query as $val) {
				$const[] = $val->id;
			}
		}

		$res = $const;

		foreach ($res as $out) {
			$file = APPPATH.str_replace('./','',$out->path);
			die($file);

			if (file_exists($file)) {
			    header('Content-Description: File Transfer');
			    header('Content-Type: application/octet-stream');
			    header('Content-Disposition: attachment; filename="'.basename($file).'"');
			    header('Expires: 0');
			    header('Cache-Control: must-revalidate');
			    header('Pragma: public');
			    header('Content-Length: ' . filesize($file));
			    readfile($file);
			    exit;
			}
		}
	}

	function updateOpenClass()
	{
		$data 	= $this->db->query("SELECT a.*, b.kd_jadwal,b.kd_tahunajaran,b.open FROM tbl_krs_sp a
									JOIN tbl_jadwal_matkul_sp b
									ON a.kd_jadwal = b.kd_jadwal
									JOIN tbl_sinkronisasi_renkeu c
									ON c.npm_mahasiswa = a.npm_mahasiswa
									WHERE b.open  = '1'
									AND c.tahunajaran = '20183'
									AND b.kd_tahunajaran = '20183'")->result();

		$object = ['flag_open' => 1];

		foreach ($data as $key) {
			$this->db->where('kd_jadwal', $key->kd_jadwal);
			$this->db->where('npm_mahasiswa', $key->npm_mahasiswa);
			$this->db->update('tbl_krs_sp', $object);
		}
	}

	function changeMkduForMhss()
	{
		if ($this->session->userdata('sess_schedule')) {
			$this->session->unset_userdata('sess_schedule');
		}
		$data['page'] = "v_changemkdumhs";
		$this->load->view('template', $data);
	}

	function autocomplete_mkdu()
	{
		$this->db->select('a.id_jadwal, a.kd_matakuliah, a.kd_dosen, a.kelas, a.kd_jadwal, b.nama_matakuliah, b.kd_prodi');
		$this->db->from('tbl_jadwal_matkul a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');
		$this->db->where('a.kd_dosen <> ""');
		$this->db->where('a.kd_dosen IS NOT NULL');
		$this->db->where('a.kd_tahunajaran', '20182');
		$this->db->where('b.kd_prodi = SUBSTRING(a.kd_jadwal,1,5)');
		$this->db->group_start();
		$this->db->like('a.kd_matakuliah', $_GET['term'], 'BOTH');
		$this->db->or_like('b.nama_matakuliah', $_GET['term'], 'BOTH');
		$this->db->or_like('a.kelas', $_GET['term'], 'BOTH');
		$this->db->group_end();
		$sql = $this->db->get()->result();

		$data = [];

		foreach ($sql as $key) {
			$data[] = [
				'value' => $key->kd_matakuliah.' - '.$key->nama_matakuliah.' - '.nama_dsn($key->kd_dosen).' - '.$key->kelas.' - '.get_jur($key->kd_prodi).' - '.$key->id_jadwal
			];
		}

		echo json_encode($data);

	}

	function create_sess_schedule()
	{
		$schedulebefore = $this->input->post('jdl_before');
		$scheduleafter = $this->input->post('jdl_after');

		$exp = explode(' - ', $schedulebefore);

		$amountofarray = count($exp);
		$getarray_idjdl = $amountofarray-1;

		$kd_mk = $exp[0];
		$kelas = $exp[3];
		$jadwal = $exp[$getarray_idjdl];
		$dosen = $exp[2];

		$exp2 = explode(' - ', $scheduleafter);

		$amountofarray2 = count($exp2);
		$getarray_idjdl2 = $amountofarray2-1;

		$kd_mk2 = $exp2[0];
		$kelas2 = $exp2[3];
		$jadwal2 = $exp2[$getarray_idjdl2];
		$dosen2 = $exp2[2];

		$array = array(
			'before_kode' => $kd_mk,
			'before_kelas' => $kelas,
			'before_jadwal' => $jadwal,
			'before_dosen' => $dosen,
			'after_kode' => $kd_mk2,
			'after_kelas' => $kelas2,
			'after_jadwal' => $jadwal2,
			'after_dosen' => $dosen2
		);
		
		$this->session->set_userdata('sess_schedule', $array);
		redirect(base_url('welcome/test_function/loadliststudent'));
	}

	function loadliststudent()
	{
		$sess_schedule = $this->session->userdata('sess_schedule');

		$id_jadwal = $sess_schedule['before_jadwal'];
		$kd_jdl = get_kd_jdl($id_jadwal);
		$prodi = substr($kd_jdl, 0,5);

		$id_jadwal2 = $sess_schedule['after_jadwal'];
		$kd_jdl2 = get_kd_jdl($id_jadwal2);
		$prodi2 = substr($kd_jdl2, 0,5);

		// var_dump($sess_schedule['before_kode'].'=='.$prodi.'<<<<>>>>'.$sess_schedule['after_kode'].'=='.$prodi2);
		// exit();

		$data['kd_mk'] = $sess_schedule['before_kode'];
		$data['kelas'] = $sess_schedule['before_kelas'];
		$data['nm_mk'] = get_nama_mk($sess_schedule['before_kode'],$prodi);
		$data['dosen'] = $sess_schedule['before_dosen'];

		$data['kd_mk2'] = $sess_schedule['after_kode'];
		$data['kelas2'] = $sess_schedule['after_kelas'];
		$data['nm_mk2'] = get_nama_mk($sess_schedule['after_kode'],$prodi2);
		$data['dosen2'] = $sess_schedule['after_dosen'];

		// get schedule detail
		$data['mhs'] = $this->db->query("SELECT * from tbl_krs where kd_jadwal = '".$kd_jdl."'")->result();
		$data['page'] = "v_listmhsmkdu";
		$this->load->view('template', $data);
	}

	function updateJadwalMkdu()
	{
		error_reporting(0);
		$npm = $this->input->post('npm[]');
		$sess  = $this->session->userdata('sess_schedule');
		$kd_jdl_bf = get_kd_jdl($sess['before_jadwal']);
		$kd_jdl_af = get_kd_jdl($sess['after_jadwal']);

		for ($i=0; $i < count($npm); $i++) {
			$object = ['kd_jadwal' => $kd_jdl_af]; 
			$this->db->like('kd_krs', $npm[$i], 'after');
			$this->db->where('kd_jadwal', $kd_jdl_bf);
			$this->db->update('tbl_krs', $object);
		}

		echo "<script>alert('Berhasil!');</script>";
		redirect(base_url('welcome/test_function/changeMkduForMhs'),'refresh');
	}

	function backmkdu()
	{
		$this->session->unset_userdata('sess_schedule');
		redirect(base_url('welcome/test_function/changeMkduForMhs'),'refresh');
	}

	// fungsi insert ke tbl_aktifitas_kuliah_mahasiswa

	function insertLastSksToAktifitasKuliahMhs($year, $prodi)
	{
		$krs = $this->db->query("SELECT kd_krs,npm_mahasiswa,kd_jurusan,tahunajaran from tbl_verifikasi_krs where tahunajaran = '".$year."' and kd_jurusan = '".$prodi."' ")->result();

		foreach ($krs as $key) {

			// get kd_mk from krs in current academicyear
			$kd_mk = $this->db->query("SELECT kd_matakuliah, npm_mahasiswa from tbl_krs where kd_krs = '".$key->kd_krs."'")->result();
			$sksConst = 0;
			foreach ($kd_mk as $val) {
				
				$sks = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$val->kd_matakuliah."' and kd_prodi = '".$prodi."'")->row();
				// var_dump($sks);exit();
				$sksConst = $sksConst + $sks->sks_matakuliah;
			}

			$data[] = [
				'THSMSTRAKM' => $year,
				'KDPTITRAKM' => 031036,
				'KDJENTRAKM' => 'C',
				'KDPSTTRAKM' => $prodi,
				'NIMHSTRAKM' => $key->npm_mahasiswa,
				'SKSEMTRAKM' => $sksConst
			];
		}

		$this->db->insert_batch('tbl_aktifitas_kuliah_mahasiswa_copy', $data);
	}

	function updateToInsertAllSksToKuliahMhs($year, $prodi)
	{
		$krs = $this->db->query("SELECT kd_krs,npm_mahasiswa,kd_jurusan,tahunajaran from tbl_verifikasi_krs where tahunajaran = '".$year."' and kd_jurusan = '".$prodi."' ")->result();

		foreach ($krs as $key) {

			$kd_mk2 = $this->db->query("SELECT distinct kd_matakuliah from tbl_krs where npm_mahasiswa = '".$key->npm_mahasiswa."'")->result();
			$sksConst2 = 0;
			foreach ($kd_mk2 as $val2) {
				
				$sks2 = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$val2->kd_matakuliah."' and kd_prodi = '".$prodi."'")->row();
				$sksConst2 = $sksConst2 + $sks2->sks_matakuliah;
			}

			$object = [
				'THSMSTRAKM' => $year,
				'KDPTITRAKM' => 031036,
				'KDJENTRAKM' => 'C',
				'KDPSTTRAKM' => $prodi,
				'NIMHSTRAKM' => $key->npm_mahasiswa,
				'SKSTTTRAKM' => $sksConst2
			];

			$this->db->where('NIMHSTRAKM', $key->npm_mahasiswa);
			$this->db->where('THSMSTRAKM', $year);
			$this->db->update('tbl_aktifitas_kuliah_mahasiswa_copy', $object);
		}
	}

	function insertIpsToAktifitasMhs()
	{
		$year = '20171';
		$prodi = '73201';
		// select KRS in choosed academicyear, group by department
		$krs = $this->db->query("SELECT kd_krs,npm_mahasiswa,kd_jurusan,tahunajaran from tbl_verifikasi_krs where tahunajaran = '".$year."' and kd_jurusan = '".$prodi."'")->result();

		foreach ($krs as $key) {

			// get kd_mk from krs in current academicyear
			$kd_mk = $this->db->query("SELECT kd_matakuliah, npm_mahasiswa from tbl_krs where kd_krs = '".$key->kd_krs."'")->result();
			$sksConst = 0;
			foreach ($kd_mk as $val) {
				
				$sks = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$val->kd_matakuliah."' and kd_prodi = '".$prodi."'")->row();
				// var_dump($sks);exit();
				$sksConst = $sksConst + $sks->sks_matakuliah;
			}
			$data['SKSSEMTRAKM'] = $sksConst;

			// get ips
			$this->load->model('krs_model');
			$ips = $this->krs_model->countips($prodi, $key->npm_mahasiswa, $year);
			$ipsk = $this->krs_model->countipk($prodi, $key->npm_mahasiswa);
						
			$valConst = 0;
			foreach ($kd_mk as $xxx) {
				$ipk = 0;
				
				$bobot = $this->db->query("SELECT a.sks_matakuliah, b.BOBOTTRLNM, b.NLAKHTRLNM from tbl_matakuliah a join tbl_transaksi_nilai b on a.kd_matakuliah = b.KDKMKTRLNM where a.kd_matakuliah = '".$xxx->kd_matakuliah."' and a.kd_prodi = '".$prodi."' and b.NIMHSTRLNM = '".$xxx->npm_mahasiswa."' ")->row();
				
				$weight = $bobot->sks_matakuliah * $bobot->BOBOTTRLNM;

				if ($bobot->NLAKHTRLNM == 'T') {
					$valConst = $valConst;
				} else {
					$valConst = $valConst + $weight;
				}
				$ipk = $ipk + $ipsk;
									
			}
			$npm = $key->npm_mahasiswa;
			$object = [
				'THSMSTRAKM' => $year,
				'KDPTITRAKM' => 031036,
				'KDJENTRAKM' => 'C',
				'KDPSTTRAKM' => $prodi,
				'NIMHSTRAKM' => $npm,
				'SKSEMTRAKM' => $sksConst,
				'NLIPSTRAKM' => $ips,
				'SKSTTTRAKM' => $sksConst2,
				'NLIPKTRAKM' => $ipk
			];

			$this->db->where('NIMHSTRAKM', $key->npm_mahasiswa);
			$this->db->where('THSMSTRAKM', $year);
			$this->db->update('tbl_aktifitas_kuliah_mahasiswa_copy', $object);
		}
		
	}

	// end of fungsi insert ke tbl_aktifitas_kuliah_mahasiswa

	function aktifitasKuliah($year='20171', $prodi='73201')
	{
		$data = [];
		// select KRS in choosed academicyear, group by department
		$krs = $this->db->query("SELECT kd_krs,npm_mahasiswa,kd_jurusan,tahunajaran from tbl_verifikasi_krs where tahunajaran = '".$year."' and kd_jurusan = '".$prodi."'")->result();

		foreach ($krs as $key) {

			// get kd_mk from krs in current academicyear
			$kd_mk = $this->db->query("SELECT kd_matakuliah, npm_mahasiswa from tbl_krs where kd_krs = '".$key->kd_krs."'")->result();
			$sksConst = 0;
			foreach ($kd_mk as $val) {
				
				$sks = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$val->kd_matakuliah."' and kd_prodi = '".$prodi."'")->row();
				// var_dump($sks);exit();
				$sksConst = $sksConst + $sks->sks_matakuliah;
			}
			// $data['SKSSEMTRAKM'] = $sksConst;

			// get ips
			$this->load->model('krs_model');
			$ips = $this->krs_model->countips($prodi, $key->npm_mahasiswa, $year);
			$ipsk = $this->krs_model->countipk($prodi, $key->npm_mahasiswa);
			// $data['NLIPSTRAKM'] = $ips;
			// sks total
			$kd_mk2 = $this->db->query("SELECT distinct kd_matakuliah from tbl_krs where npm_mahasiswa = '".$key->npm_mahasiswa."'")->result();
			$sksConst2 = 0;
			foreach ($kd_mk2 as $val2) {
				
				$sks2 = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$val2->kd_matakuliah."' and kd_prodi = '".$prodi."'")->row();
				$sksConst2 = $sksConst2 + $sks2->sks_matakuliah;
			}
			// $data['SKSTTTRAKM'] = $sksConst2;

			// ipk
			
			$valConst = 0;
			foreach ($kd_mk as $xxx) {
				$ipk = 0;
				
				$bobot = $this->db->query("SELECT a.sks_matakuliah, b.BOBOTTRLNM, b.NLAKHTRLNM from tbl_matakuliah a join tbl_transaksi_nilai b on a.kd_matakuliah = b.KDKMKTRLNM where a.kd_matakuliah = '".$xxx->kd_matakuliah."' and a.kd_prodi = '".$prodi."' and b.NIMHSTRLNM = '".$xxx->npm_mahasiswa."' ")->row();
				
				$weight = $bobot->sks_matakuliah * $bobot->BOBOTTRLNM;

				if ($bobot->NLAKHTRLNM == 'T') {
					$valConst = $valConst;
				} else {
					$valConst = $valConst + $weight;
				}
				$ipk = $ipk + $ipsk;
									
			}
			$npm=$key->npm_mahasiswa;
			$data[] = [
				'THSMSTRAKM' => $year,
				'KDPTITRAKM' => 031036,
				'KDJENTRAKM' => 'C',
				'KDPSTTRAKM' => $prodi,
				'NIMHSTRAKM' => $npm,
				'SKSEMTRAKM' => $sksConst,
				'NLIPSTRAKM' => $ips,
				'SKSTTTRAKM' => $sksConst2,
				'NLIPKTRAKM' => $ipk
			];
		}
		// $dt['value'] = $data;
		// echo "<pre>";
		// print_r($dt);
		// echo "</pre>";
		// exit;
		$this->db->insert_batch('tbl_aktifitas_kuliah_mahasiswa_copy', $data);
		// $this->load->view('excel_aktifitas', $dt);

	}

	function insertToKhs($nim,$year,$prodi)
	{

		$kd_mk2 = $this->db->query("SELECT distinct kd_matakuliah from tbl_krs where npm_mahasiswa = '".$nim."' and SUBSTRING(kd_krs,1,17) <= '".$nim.$year."' ")->result();

		// sks total
		$sksConst = 0;
		foreach ($kd_mk2 as $val) {
			$sks = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$val->kd_matakuliah."' and kd_prodi = '".$prodi."'")->row();
			$sksConst = $sksConst + $sks->sks_matakuliah;
		}

		// ips
		$this->load->model('krs_model');
		$ips = $this->krs_model->countips($prodi, $nim, $year);

		// ipk
		
		$weight2 = 0;
		$totalsks = 0;
		foreach ($kd_mk2 as $xxx) {
			$ipk = 0;
			
			$bobot = $this->db->query("SELECT a.sks_matakuliah, b.BOBOTTRLNM, b.NLAKHTRLNM from tbl_matakuliah a join tbl_transaksi_nilai b on a.kd_matakuliah = b.KDKMKTRLNM where a.kd_matakuliah = '".$xxx->kd_matakuliah."' and a.kd_prodi = '".$prodi."' and b.NIMHSTRLNM = '".$nim."' and b.NLAKHTRLNM <> 'T' ")->row();
			
			$weight = $bobot->sks_matakuliah * $bobot->BOBOTTRLNM;
			$weight2 = $weight2 + $weight;

			$totalsks = $totalsks + $bobot->sks_matakuliah;
			
			$ipk = $ipk + ($weight2 / $totalsks);					
		}

		$data = [
			'npm_mahasiswa' => $nim,
			'tahunajaran' => $year,
			'ips' => number_format($ips, 2),
			'total_sks' => $sksConst,
			'ipk' => number_format($ipk, 2)
		];

		$this->app_model->insertdata('tbl_khs', $data);

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// exit();
	}

	function changeNimS2()
	{
		$arr = [
			"201820252016",
			"201820252017",
			"201820252018",
			"201820252019",
			"201820252020",
			"201820252021",
			"201820252022",
			"201820252023",
			"201820252024",
			"201820252025"
		];

		foreach ($arr as $key) {
			$exp = explode('252', $key);
			// var_dump($exp[1]);exit();
			// $nim = $exp[0].'252'.$exp[1];
			if ($exp[1] == "016") {

				$nim = $exp[0].'252001';

			} elseif ($exp[1] == "017") {

				$nim = $exp[0].'252002';

			} elseif ($exp[1] == "018") {

				$nim = $exp[0]."252003";

			} elseif ($exp[1] == "019") {

				$nim = $exp[0]."252004";

			} elseif ($exp[1] == "020") {

				$nim = $exp[0]."252005";

			} elseif ($exp[1] == "021") {

				$nim = $exp[0]."252006";

			} elseif ($exp[1] == "022") {

				$nim = $exp[0]."252007";

			} elseif ($exp[1] == "023") {

				$nim = $exp[0]."252008";

			} elseif ($exp[1] == "024") {

				$nim = $exp[0]."252009";

			} elseif ($exp[1] == "025") {

				$nim = $exp[0]."252010";

			}
			

			// update in tbl_mahasiswa
			$obj = ['NIMHSMSMHS' => $nim];
			$this->db->where('NIMHSMSMHS', $key);
			$this->db->update('tbl_mahasiswa', $obj);

			// update in tbl_sinkronisasi_renkeu
			$npm = ['npm_mahasiswa' => $nim];
			$this->db->where('npm_mahasiswa', $key);
			$this->db->update('tbl_sinkronisasi_renkeu', $npm);

			// update in tbl_krs
			$const = '2018';
			for ($i=1; $i < 3; $i++) { 
				$this->db->like('kd_krs', $key.$const.$i, 'after');
				$kdkrs = $this->db->get('tbl_krs');

				// if data available
				if ($kdkrs->num_rows() > 0) {
					// new kd krs
					$exp2 = substr($kdkrs->row()->kd_krs, 12, 6);
					$kode_krs = $nim.$exp2;

					// new npm
					// $exp3 = explode('252', $kdkrs->row()->npm_mahasiswa);
					// $npm_mhs = $exp3[0].'252'.$exp[1];

					// new data
					$data = ['npm_mahasiswa' => $nim, 'kd_krs' => $kode_krs];

					// tbl_krs
					$this->db->where('kd_krs', $kdkrs->row()->kd_krs);
					$this->db->update('tbl_krs', $data);

					// update tbl_verifikasi_krs
					$this->db->where('kd_krs', $kdkrs->row()->kd_krs);
					$this->db->update('tbl_verifikasi_krs', $data);
				}
			}

			// update in tbl_pa
			$object = ['npm_mahasiswa' => $nim];
			$this->db->where('npm_mahasiswa', $key);
			$this->db->update('tbl_pa', $object);

			// update in tbl_absen
			$npm2 = ['npm_mahasiswa' => $nim];
			$this->db->where('npm_mahasiswa', $key);
			$this->db->update('tbl_absensi_mhs_new_20171', $npm2);

			// update in tbl_userlogin
			$userid = [
				'username' => $nim,
				'password' => sha1(md5($nim).key),
				'password_plain' => $nim,
				'userid' => $nim
			];
			$this->db->where('userid', $key);
			$this->db->update('tbl_user_login', $userid);
			
		}
	}

	function removemhs()
	{
		$key = [
			"201910415459"
		];
		// remove from tbl_mahasiswa
		$this->db->where_in('NIMHSMSMHS', $key);
		$this->db->delete('tbl_mahasiswa');

		// remove from tbl_sinkronisasi_renkeu
		$this->db->where_in('npm_mahasiswa', $key);
		$this->db->delete('tbl_sinkronisasi_renkeu');

		// remove from tbl_krs
		$this->db->where_in('npm_mahasiswa', $key);
		$this->db->delete('tbl_krs');

		// remove from tbl_krs_feeder
		$this->db->where_in('npm_mahasiswa', $key);
		$this->db->delete('tbl_krs_feeder');

		// remove from tbl_verifikasi_krs
		$this->db->where_in('npm_mahasiswa', $key);
		$this->db->delete('tbl_verifikasi_krs');

		// remove from tbl_absensi_mhs_new_20171
		$this->db->where_in('npm_mahasiswa', $key);
		$this->db->delete('tbl_absensi_mhs_new_20171');

		// remove from tbl_pa
		$this->db->where_in('npm_mahasiswa', $key);
		$this->db->delete('tbl_pa');

		// remove from tbl_nilai_detail
		$this->db->where_in('npm_mahasiswa', $key);
		$this->db->delete('tbl_nilai_detail');

		// remove from tbl_transaksi_nilai
		$this->db->where_in('NIMHSTRLNM', $key);
		$this->db->delete('tbl_transaksi_nilai');
		

		echo "Berhasil";
	}

	function generatemhspemutihan()
	{
		$npm = "200810115024";

		// insert to tbl_user_login
		// $data['username'] = $npm;
		// $data['password'] = sha1(md5($npm).key);
		// $data['password_plain'] = $npm;
		// $data['userid'] = $npm;
		// $data['id_user_group'] = '5';
		// $data['status'] = '1';
		// $data['user_type'] = '2';
		// $this->db->insert('tbl_user_login', $data);

		// insert tbl_krs
		// get transaksi nilai
		$transaksinilai = $this->db->query("SELECT * from tbl_transaksi_nilai where NIMHSTRLNM = '".$npm."' order by THSMSTRLNM asc")->result();

		foreach ($transaksinilai as $key) {
			$data1[] = [
				'npm_mahasiswa' => $npm,
				// 'kd_matakuliah' => $key->KDKMKTRLNM,
				// 'semester_krs' => $this->app_model->get_semester_khs(20081,$key->THSMSTRLNM),
				'kd_krs' => $npm.$key->THSMSTRLNM.'1',
				'kd_jurusan' => '70201',
				'tahunajaran' => $key->THSMSTRLNM,
				'status_verifikasi' => 1
			];
		}
		// $this->db->insert_batch('tbl_verifikasi_krs', $data1);
		// echo "<pre>";
		// print_r ($data1);
		// echo "</pre>";exit();
		$smt = 1;
		foreach ($transaksinilai as $row) { 
			$data[] = [
				'briva_mhs' => '703060810115024',
				'npm_mahasiswa' => $npm,
				'status' => 4,
				'transaksi_terakhir' => date('Y-m-d'),
				'userid' => 'RENKEU',
				'semester' => $smt,
				'tahunajaran' => $row->THSMSTRLNM
			];
			$smt++;
		}
		$this->db->insert_batch('tbl_sinkronisasi_renkeu', $data);


	}

	function deactivateUser()
	{
		$do = $this->db->get('tbl_dropout')->result();

		$this->db->where('STMHSMSMHS', 'L');
		$this->db->where('NIMHSMSMHS IN (SELECT userid from tbl_user_login where id_user_group = "5") ');
		$graduate = $this->db->get('tbl_mahasiswa')->result();

		foreach ($do as $val) {
			$arr1[] = $val->npm_mahasiswa;
		}

		foreach ($graduate as $key) {
			$arr2[] = $key->NIMHSMSMHS;
		}

		$merge = array_merge($arr1,$arr2);
		$fix = array_unique($merge);

		echo "<pre>";
		print_r ($fix);
		echo "</pre>";

		// foreach ($fix as $keys => $value) {
		// 	$this->db->where('userid', $value);
		// 	$this->db->update('tbl_user_login_copy', ['status' => 0]);
		// }
	}

	function moveJadwalRealToJadwalFeeder($tahunakademik)
	{
		$this->db->where("kd_tahunajaran", $tahunakademik);
		$getjadwal = $this->db->get("tbl_jadwal_matkul")->result();

		foreach ($getjadwal as $key) {
			$data[] = [
				'kd_jadwal' => $key->kd_jadwal,
				'id_matakuliah' => $key->id_matakuliah,
				'kd_matakuliah' => $key->kd_matakuliah,
				'kd_dosen' => $key->kd_dosen,
				'kd_ruangan' => $key->kd_ruangan,
				'hari' => $key->hari,
				'waktu_mulai' => $key->waktu_mulai,
				'waktu_selesai' => $key->waktu_selesai,
				'kelas' => $key->kelas,
				'waktu_kelas' => $key->waktu_kelas,
				'kd_tahunajaran' => $key->kd_tahunajaran,
				'audit_date' => $key->audit_date,
				'audit_user' => $key->audit_user,
				'status_feeder' => 0
			];
		}

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// exit();

		$this->db->insert_batch('tbl_jadwal_matkul_feeder', $data);
	}

	function getValueRange($poin)
	{	
		$this->db->select('nilai_bawah, nilai_atas, nilai_huruf');
		$this->db->where('deleted_at');
		$data = $this->db->get('tbl_index_nilai')->result();

		foreach ($data as $key) {
			if (in_array($poin, range($key->nilai_bawah, $key->nilai_atas, 0.1))) {
				$nilai = $key->nilai_huruf;
			}
		}

		echo $nilai;
	}
}

/* End of file test_function.php */
/* Location: ./application/controllers/test_function.php */