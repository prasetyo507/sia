<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menyeh extends CI_Controller {

	public function index()
	{
		$prodi = '74101';
		$ta = '20151';
		$x=0;
		$getmhs = $this->db->query("SELECT DISTINCT kd_krs FROM tbl_krs a
									JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS`
									WHERE kd_krs LIKE CONCAT(npm_mahasiswa,'".$ta."%') AND b.`KDPSTMSMHS` = '".$prodi."' AND b.NIMHSMSMHS like '2015%'
									AND b.`NIMHSMSMHS` NOT IN (SELECT NIMHSTRAKM FROM tbl_aktifitas_kuliah_mahasiswa WHERE THSMSTRAKM = '".$ta."')
									ORDER BY b.`NIMHSMSMHS` ASC")->result();
		foreach ($getmhs as $value) {
			$x++;
			//ips
			$hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
										JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
										JOIN tbl_krs c ON a.`NIMHSTRLNM` = c.`npm_mahasiswa`
										WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$prodi.'" AND NIMHSTRLNM = "'.substr($value->kd_krs, 0,12).'" and a.THSMSTRLNM ="'.$ta.'" 
										AND c.`kd_krs` LIKE CONCAT(npm_mahasiswa,"'.$ta.'%")')->result();

			//try
			$q = $this->db->query("select distinct kd_matakuliah from tbl_krs where kd_krs = '".$value->kd_krs."'")->result();
        	$sumsks = 0;
        	foreach ($q as $val) {
        		$sksmk = $this->db->query("select distinct sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$val->kd_matakuliah."' AND kd_prodi = ".$prodi."")->row()->sks_matakuliah; 
        		$sumsks = $sumsks + $sksmk;
        	}
        	//--try

			$sts=0;
			$hts=0;
			foreach ($hitung_ips as $iso) {
				$hs = 0;
				$hs = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
				$hts=  $hts + $hs;
				$sts=$sts+$iso->sks_matakuliah;
			}
			$ips = number_format($hts/$sts,2);


			//ipk
			$tskss=0;
			$tnl=0;
			if (substr($value->kd_krs, 0,4) > 2014) {
			    $ipk = $this->app_model->getipkmhs(substr($value->kd_krs, 0,12),$prodi);
			    $hitung_ipk = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
										JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
										WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$prodi.'" AND NIMHSTRLNM = "'.substr($value->kd_krs, 0,12).'" and a.THSMSTRLNM <="'.$ta.'" ')->result();

				$st=0;
				$ht=0;
				foreach ($hitung_ipk as $iso) {
					$nk = ($iso->sks_matakuliah*$iso->BOBOTTRLNM);
					$tskss=$tskss+$iso->sks_matakuliah;
					$tnl = $tnl+$nk;
				}
				$ipk = number_format($tnl/$tskss,2);
			} else {
				$q = $this->db->query('SELECT nl.`KDKMKTRLNM`,
							IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
								(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
							IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
								(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
							MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,nl.`THSMSTRLNM`,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai nl 
							WHERE nl.`NIMHSTRLNM` = "'.substr($value->kd_krs, 0,12).'" AND nl.`THSMSTRLNM` <= "'.$ta.'"
							GROUP BY nl.`KDKMKTRLNM` ORDER BY nl.`THSMSTRLNM` ASC 
							')->result();
				foreach ($q as $row) {
					$nk = ($row->sks_matakuliah*$row->BOBOTTRLNM);
					$tskss=$tskss+$row->sks_matakuliah;
					$tnl = $tnl+$nk;
				}
				$ipk = number_format($tnl/$tskss,2);
			}

			$data['THSMSTRAKM'] = $ta;
			$data['KDPTITRAKM'] = '031036';
			$data['KDJENTRAKM'] = 'C';
			$data['KDPSTTRAKM'] = $prodi;
			$data['NIMHSTRAKM'] = substr($value->kd_krs, 0,12);
			$data['SKSEMTRAKM'] = $sumsks;
			$data['NLIPSTRAKM'] = $ips;
			$data['SKSTTTRAKM'] = $tskss;
			$data['NLIPKTRAKM'] = $ipk;

			$vi[$x] = $data['NIMHSTRAKM'];

			$this->app_model->insertdata('tbl_aktifitas_kuliah_mahasiswa',$data);
			$ak = $this->db->query("SELECT * FROM tbl_aktifitas_kuliah_mahasiswa ak WHERE ak.`THSMSTRAKM` = 20161 AND ak.`KDPSTTRAKM` = 70201")->result();

			$c_vi=count($vi);
			$ak = count($ak);

			echo $vi[1].'--'.$vi[$c_vi].'--'.$ak;
		}
	}
}

/* End of file Menyeh.php */
/* Location: ./application/modules/welcome/controllers/Menyeh.php */