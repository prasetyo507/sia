<?php 
	
	$client = new SoapClient("http://simlitabmas.dikti.go.id/ws_pt/ws.svc?wsdl");
	
	// 1 
	/*  
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999","nidn"=>"0022120150");
    $webService = $client->get_biodata_dosen($params);  
    $Hasil = $webService->get_biodata_dosenResult; 
	*/	
	
	/* 2 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999","tgl_updated"=>"");
    $webService = $client->get_jml_record_biodata_dosen($params);  
    $Hasil = $webService->get_jml_record_biodata_dosenResult; 
	*/
	
	/* 3 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999","tgl_updated"=>"",
    "jml_record"=>"20","offset"=>"0");
    $webService = $client->get_daftar_biodata_dosen($params);  
    $Hasil = $webService->get_daftar_biodata_dosenResult; 
	*/
	
	/* 4 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999","tgl_updated"=>"");
    $webService = $client-> get_jml_record_komentar_rekomendasi_penilaian($params);  
    $Hasil = $webService-> get_jml_record_komentar_rekomendasi_penilaianResult; 
	*/
	
	/* 5  
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "jml_record"=>"20","offset"=>"0");
    $webService = $client->get_daftar_komentar_rekomendasi_penilaian($params);  
    $Hasil = $webService->get_daftar_komentar_rekomendasi_penilaianResult; 
	*/
	
	/* 6 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client->get_jml_record_mitra_usulan_penelitian($params);  
    $Hasil = $webService->get_jml_record_mitra_usulan_penelitianResult; 
	*/
	
	/* 7 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client->get_daftar_mitra_usulan_penelitian($params);  
    $Hasil = $webService->get_daftar_mitra_usulan_penelitianResult; 
	*/
	
	/* 8 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client->get_jml_record_info_tambahan_usulan_penelitian($params);  
    $Hasil = $webService->get_jml_record_info_tambahan_usulan_penelitianResult; 
	*/
	
	/* 9
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client-> get_daftar_info_tambahan_usulan_penelitian($params);  
    $Hasil = $webService-> get_daftar_info_tambahan_usulan_penelitianResult; 
	*/
	
	/* 10 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client-> get_jml_record_personil_penelitian($params);  
    $Hasil = $webService-> get_jml_record_personil_penelitianResult; 
	*/
	/* 11  
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client-> get_daftar_personil_penelitian($params);  
    $Hasil = $webService-> get_daftar_personil_penelitianResult; 
	*/
	/* 12
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client->get_jml_record_mitra_ibm($params);  
    $Hasil = $webService->get_jml_record_mitra_ibmResult; 
	 */
	 
	/* 13
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client->get_daftar_mitra_ibm($params);  
    $Hasil = $webService->get_daftar_mitra_ibmResult; 
	*/
	 
	/* 14 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client->get_jml_atribut_ibw_ibwcsr($params);  
    $Hasil = $webService->get_jml_atribut_ibw_ibwcsrResult; 
	*/
	
	/* 15 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client-> get_daftar_atribut_ibw_ibwcsr ($params);  
    $Hasil = $webService-> get_daftar_atribut_ibw_ibwcsrResult; 
	*/
	 
	/* 16 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client->  get_jml_record_daftar_catatan_harian_pelaksanaan_kegiatan  ($params);  
    $Hasil = $webService->  get_jml_record_daftar_catatan_harian_pelaksanaan_kegiatanResult; 
	*/
	 
	/* 17 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client->get_daftar_catatan_harian_pelaksanaan_kegiatan   ($params);  
    $Hasil = $webService->get_daftar_catatan_harian_pelaksanaan_kegiatanResult; 
	*/
	 
	/* 18 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client->get_daftar_hasil_penilaian_usulan_per_komponen    ($params);  
    $Hasil = $webService->get_daftar_hasil_penilaian_usulan_per_komponenResult; 
	*/
	 
	/* 19 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client->get_jml_record_daftar_hasil_penilaian_usulan_per_komponen($params);  
    $Hasil = $webService->get_jml_record_daftar_hasil_penilaian_usulan_per_komponenResult; 
	*/
	/* 20 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client-> get_pt_jml_record_program_studi ($params);  
    $Hasil = $webService-> get_pt_jml_record_program_studiResult; 
	*/
	
	/* 21 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client->get_pt_daftar_program_studi($params);  
    $Hasil = $webService->get_pt_daftar_program_studiResult; 
	*/
	
	/* 22 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client->get_pt_jml_record_usulan_penelitian($params);  
    $Hasil = $webService->get_pt_jml_record_usulan_penelitianResult; 
	*/
	 
	/* 23 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client->get_pt_daftar_usulan_penelitian($params);  
    $Hasil = $webService->get_pt_daftar_usulan_penelitianResult; 
	*/
	 
	/* 24 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client-> get_pt_jml_record_usulan_penelitian_disertasi_doktor ($params);  
    $Hasil = $webService-> get_pt_jml_record_usulan_penelitian_disertasi_doktorResult; 
	*/
	 
	/* 25 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client->  get_pt_daftar_usulan_penelitian_disertasi_doktor  ($params);  
    $Hasil = $webService->  get_pt_daftar_usulan_penelitian_disertasi_doktorResult; 
	*/
	
	 
	/* 26 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client->   get_pt_jml_record_ukm_ibpe   ($params);  
    $Hasil = $webService->   get_pt_jml_record_ukm_ibpeResult; 
	*/
	 
	/* 27 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client->get_pt_daftar_ukm_ibpe($params);  
    $Hasil = $webService->get_pt_daftar_ukm_ibpeResult; 
	*/
	 
	/* 28 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"");
    $webService = $client-> get_pt_jml_record_atribut_hilink ($params);  
    $Hasil = $webService-> get_pt_jml_record_atribut_hilinkResult; 
	*/
	
	
	/* 29 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client->  get_pt_daftar_atribut_hilink  ($params);  
    $Hasil = $webService->  get_pt_daftar_atribut_hilinkResult; 
	*/
	
	/* 30 
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client->  get_pt_jml_record_atribut_kknppm  ($params);  
    $Hasil = $webService->  get_pt_jml_record_atribut_kknppmResult; 
	*/
	
	/* 31 */
    $params = array("keyword"=>"3f291e16-f084-4f61-b80b-223c5138d557","kode_perguruan_tinggi"=>"999999",
    "tgl_updated"=>"","jml_record"=>"20","offset"=>"0");
    $webService = $client->get_pt_daftar_atribut_kknppm($params);  
    $Hasil = $webService->get_pt_daftar_atribut_kknppmResult; 
	
	
    //echo "Encoded data = ".$Hasil;
	echo PHP_EOL.PHP_EOL;
	
	$hasil64 = base64_decode($Hasil);
	echo "<br/><br/>Decoded 64 = <br/>".$hasil64."<br>";
	echo PHP_EOL.PHP_EOL;
	
	$dta =  json_decode($hasil64);
	echo "<br>Data JSON = ".$dta->{'data'};
	echo "<br> Result = ".$dta->{'result'}."<br>";
	
echo "=======================================================<br>";
	if(!strpos($dta->{'result'},"False")){
	
	echo PHP_EOL.PHP_EOL;
	$dataDetail = json_decode($dta->{'data'});
	
	echo PHP_EOL.PHP_EOL;
	echo "Akses data: ".PHP_EOL;
	
	echo '<br/><br/><br/>';	
		
	$arrKolom = array();
	$arrData = array();	
		$i=0; $j=0; $k=0;
	foreach($dataDetail as $key=>$value)
	{
		foreach($value as $keyval=>$val)
		{
			echo $keyval." = ".$value->$keyval."<br>";
			$arrData[$k] = $value->$keyval;
			$k++;
			if($i==0){
				$arrKolom[$j] = $keyval;
				$j++;
			}
	  }	  
	  $i++;
	  echo '<br/>';
	}

	echo "<br/> jml kolom=".count($arrKolom);
	//echo "<br/> jml data=".(count($arrData));
    $jmlBaris = count($arrData)/count($arrKolom);
    echo "<br/> jml baris=".$jmlBaris."<br>";
	echo "<table border=1 ><tr bgcolor='#cccccc' align='center'><td>No</td>";
	for($jmlKolom=0; $jmlKolom<count($arrKolom); $jmlKolom++)
	{
		echo "<td>".$arrKolom[$jmlKolom]."</td>";
	}
	echo "</tr>";
	$indek=0;
	for($jb=0; $jb < $jmlBaris; $jb++)
	{
		echo "<tr><td>".($jb+1)."</td>";
		for($jmlKolom=0; $jmlKolom<count($arrKolom); $jmlKolom++)
			{
				echo "<td>".$arrData[$indek]."&nbsp;</td>";
				$indek++;
			}
		echo "</tr>";
	}
	echo "</table>";

}
?> 
