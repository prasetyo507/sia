<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_table extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		// 
		// $url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
		// // $url = 'http://localhost:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
		// $client = new nusoap_client($url, true);
		// $proxy = $client->getProxy();
		// $result = $proxy->GetToken(userfeeder, passwordfeeder);
		// //var_dump($result);exit();
		// $token = $result;
	}

	public function index()
	{
		//die('adad');exit();
		$this->load->library("Nusoap_lib");
		//var_dump($this->load->library("Nusoap_lib"));exit();
		//$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
		$url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
		$client = new nusoap_client($url, true);
		$proxy = $client->getProxy();
		//var_dump($proxy);exit();
		//$username = userfeeder;
        //$password = passwordfeeder;
		$result = $proxy->GetToken(userfeeder, passwordfeeder);
		//var_dump($result);exit();
		$token = $result;
		$data['getData'] = $proxy->ListTable($token);
		//var_dump($data);exit();
		$this->load->view('list_view', $data);
	}

	function column($table)
	{
		$this->load->library("Nusoap_lib");
		$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
		// $url = 'http://localhost:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
		$client = new nusoap_client($url, true);
		$proxy = $client->getProxy();
		$result = $proxy->GetToken(userfeeder, passwordfeeder);
		//var_dump($result);exit();
		$token = $result;
		$data['getData'] = $proxy->GetDictionary($token, $table);
		$data['table'] = $table;
		//var_dump($data);exit();
		$this->load->view('table_view', $data);
	}

	function search()
	{
		$this->load->library("Nusoap_lib");
		//$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
		$url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
		$client = new nusoap_client($url, true);
		$proxy = $client->getProxy();
		$result = $proxy->GetToken(userfeeder, passwordfeeder);
		//var_dump($result);exit();
		$token = $result;
		$table1 = $this->input->post('table', TRUE);
        $filter = $this->input->post('filter', TRUE);
        $order = $this->input->post('order', TRUE);
        $limit = 100; // jumlah data yang diambill
        $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
        $result2 = $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);
        
		var_dump($result2['result']);
	}

	function sync_mhs_ipk()
    {
    	$q = $this->db->query('SELECT DISTINCT kd_krs FROM tbl_verifikasi_krs WHERE tahunajaran = "20152" and kd_jurusan = "32201" order by npm_mahasiswa asc')->result();
    	//var_dump($q);exit();
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $username = '031036';
        $password = 'ubhara-031036';
        $result = $proxy->GetToken($username, $password);

        $token = $result;
        $table = 'kuliah_mahasiswa';
        //$input = count($this->input->post('mhs',TRUE));
        //var_dump($input);exit();
        foreach ($q as $value) {
        	$mhs = substr(trim($value->kd_krs), 0,12);
            $table1 = 'kuliah_mahasiswa';
            $filter = "nipd ilike '%".$mhs."%'";
            $limit = 10; // jumlah data yang diambill
            $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
            $result2 = $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);
            //var_dump($result2);exit();
            foreach ($result2['result'] as $value1) {
                if ($value1['id_smt'] == '20151') {
                	//echo $value1['ips'];exit();
                	//var_dump($value1);exit();
                	$datax[] = array('npm_mahasiswa' => $mhs, 'tahunajaran' => '20151', 'ips' => $value1['ips'], 'total_sks' => $value1['sks_total'], 'ipk' => $value1['ipk']);
                }
                //var_dump($datax);exit();
            }
            
        }
        $this->db->insert_batch('tbl_khs',$datax);
    }

}

/* End of file List_table.php */
/* Location: ./application/modules/feeder/controllers/List_table.php */