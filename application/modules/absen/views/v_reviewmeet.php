<script type="text/javascript">
	$(document).ready(function(){
		$('#table-container').load('<?php echo base_url('absen/absenDosen/loadMeet/'.$idjdl.'/1') ?>');
	});

	function loadMeeting(meet)
	{
		$('#table-container').load('<?php echo base_url('absen/absenDosen/loadMeet/'.$idjdl.'/') ?>'+meet);
	}

	function loadEdit(transaction)
	{
		$('#absen').load('<?php echo base_url('absen/absenDosen/editData/') ?>'+transaction);
	}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file"></i>
  				<?php 
  				$kdjdl = get_kd_jdl($idjdl);
  				$prodi = substr($kdjdl, 0,5) ?>
  				<h3><?= get_nama_mk(get_dtl_jadwal($kdjdl)['kd_matakuliah'],$prodi) ?> - <?= get_dtl_jadwal($kdjdl)['kelas'] ?></h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="span11">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#1" onclick="loadMeeting(1)">Ke - 1</a></li>
					    <?php for ($i=2; $i < 17; $i++) { ?>
					    	<li>
					    		<a data-toggle="tab" href="#<?= $i ?>" onclick="loadMeeting(<?= $i ?>)">Ke - <?= $i ?></a>
					    	</li>
					    <?php } ?>
				  	</ul>

				  	<div class="tab-content" id="table-container">

				 	</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="absen">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->