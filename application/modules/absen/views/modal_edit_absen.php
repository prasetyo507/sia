<style type="text/css">
    .veralign {
        vertical-align: middle;
    }
</style>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?php echo $nama_mk.' - '.$kelas.' - pertemuan '.$meeting ?></h4>
</div>

<form action="<?= base_url('absen/absenDosen/updateAbsen') ?>" method="post">
    
    <div class="modal-body">
        <table class="table table-stripped table-bordered">
        	<thead>
        		<tr>
        			<th rowspan="2" class="veralign">No</th>
        			<th rowspan="2" class="veralign">Nama</th>
        			<th colspan="4" class="veralign">Status Kehadiran</th>
        		</tr>
                <tr>
                    <th>H</th>
                    <th>I</th>
                    <th>S</th>
                    <th>A</th>
                </tr>
        	</thead>
        	<tbody>
    			<?php $no = 1; foreach ($list->result() as $key) { ?>
        			<tr>
    	    			<td><?php echo $no; ?></td>
    	    			<td><?php echo get_nm_mhs($key->npm_mahasiswa); ?></td>
                        <td>
                            <input type="radio" name="status[][<?= $key->npm_mahasiswa ?>]" value="H" <?php if ($key->kehadiran == 'H') { echo "checked"; } ?>>
                        </td>
                        <td>
                            <input type="radio" name="status[][<?= $key->npm_mahasiswa ?>]" value="I" <?php if ($key->kehadiran == 'I') { echo "checked"; } ?>>
                        </td>
                        <td>
                            <input type="radio" name="status[][<?= $key->npm_mahasiswa ?>]" value="S" <?php if ($key->kehadiran == 'S') { echo "checked"; } ?>>
                        </td>
                        <td>
                            <input type="radio" name="status[][<?= $key->npm_mahasiswa ?>]" value="A" <?php if ($key->kehadiran == 'A') { echo "checked"; } ?>>
                        </td>
    	    		</tr>
                    <input type="hidden" name="user[]" value="<?= $key->npm_mahasiswa; ?>">
                    <input type="hidden" name="kdtrk[]" value="<?= $transaks; ?>">
        		<?php $no++; } ?>
        	</tbody>
        </table>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Update</button>
    </div>
</form>