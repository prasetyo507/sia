<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbsenDosen extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			echo "<script>alert('Silahkan login kembali untuk memulai sesi!');</script>";
			redirect(base_url('auth/logout'),'refresh');
		}
		$this->actyear = getactyear();
		$this->user = $this->session->userdata('sess_login');
		$this->load->model('absen/absen_model', 'absen');
	}

	public function index()
	{
		$tgl = strtotime(date('Y-m-d'));
		$day = dayToNumber(date('l', $tgl));

		$data['listajar'] = $this->absen->jadwalajar($this->user['userid'],$this->actyear,$day);
		$data['page'] = "v_listajardosen";
		$this->load->view('template', $data);
	}

	function todaylist($idjdl)
	{
		$kdjdl = get_kd_jdl($idjdl);

		$isopen= $this->role_model->cek_abs($kdjdl)->row();

		if ($isopen->open != 1) {

			echo "<script>alert('Jadwal Anda Belum Dapat Diakses. Harap Hubungi Prodi!');history.go(-1);</script>";

		} else { 

			$data['kelas'] 	= $this->app_model->getdetail('tbl_krs','kd_jadwal',$kdjdl,'npm_mahasiswa','asc')->result();
			$tanggal 		= $this->absen->getLastMeeting($kdjdl)->row();
			$data['meet'] 	= $tanggal->pertemuan+1;
			$data['onup'] 	= get_dtl_jadwal($kdjdl);
			$data['max'] 	= $this->absen->amountMemberClass($kdjdl);
			$data['id'] 	= $idjdl;

			$data['page'] = "v_detailpeserta";
			$this->load->view('template', $data);
		}
	}

	function saveMeeting($idjdl)
	{
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('hilman_model', 'hilm');

		$curentTime 	= date('H:i:s');
		$jumlahabsen 	= $this->input->post('jumlah', TRUE);
		$actyear		= getactyear();

		$kode 	= $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$idjdl,'id_jadwal','asc')->row();
		$cek 	= $this->absen->getLastMeeting($kode->kd_jadwal);

		$meet 	= $cek->row()->pertemuan+1;

		// var_dump($meet);exit();
		
		if ($cek->num_rows() > 0) {
			if ($cek->row()->tanggal == $this->input->post('tgl',TRUE)) {

				// var_dump($cek->row()->tanggal.'---'.$this->input->post('tgl',TRUE));
				echo "<script>alert('Absensi Sudah Dilakukan Pada Tanggal Tersebut');history.go(-1);</script>";

			} else {

				$a = 1;
				for ($i=0; $i < $jumlahabsen-1; $i++) { 
					$hasil = $this->input->post('absen'.$a.'[0]', TRUE);
					$pecah = explode('-', $hasil);
					$datax[] = array(
						'npm_mahasiswa' => $pecah[1],
						'kd_jadwal' => $kode->kd_jadwal,
						'tanggal' => $this->input->post('tgl',TRUE),
						'pertemuan' => $meet,
						'kehadiran' => $pecah[0],
						'transaksi' => md5($kode->kd_jadwal.$meet),
						'created_at' => date('Y-m-d H:i:s')
						);
					if (($cek->row()->pertemuan+1) > 16) {
						echo "<script>alert('Absen Melebihi Jumlah 16 Pertemuan');
						document.location.href='".base_url()."absensi/list_abs/';</script>";
						exit();
					}
					$a++;

				}
				
				// echo "<pre>";
				// var_dump($datax);exit();
				// echo "</pre>";
				$this->db->insert_batch('tbl_absensi_mhs_new_'.$actyear, $datax);

				// cek sks mk
				$pro = substr($kode->kd_jadwal, 0,5);
				$arr = ['kd_matakuliah' => $kode->kd_matakuliah, 'kd_prodi' => $pro];
				$sks = $this->hilm->moreWhere($arr, 'tbl_matakuliah')->row()->sks_matakuliah;
				
				if ($sks == '2') {
                    $date = new DateTime($curentTime)	;
                    $date->add(new DateInterval('PT90M'));
                    $fixtime = $date->format('H:i:s');
				} else {
                    $date = new DateTime($curentTime)	;
                    $date->add(new DateInterval('PT135M'));
					$fixtime = $date->format('H:i:s');
				}

				$das = array(
					'kd_jadwal'	=> $kode->kd_jadwal,
					'pertemuan'	=> $cek->row()->pertemuan+1,
					'tgl'		=> $this->input->post('tgl',TRUE),
					'materi'	=> $this->input->post('bahas', TRUE),
					'jam_masuk'	=> date('H:i:s'),
					'jam_keluar'=> $fixtime,
					'kd_trk' 	=> md5($kode->kd_jadwal.$meet),
					'created_at'=> date('Y-m-d H:i:s')
					);
				// var_dump($das);exit();
				$this->db->insert('tbl_materi_abs_'.$actyear, $das);

				echo "<script>alert('Sukses');</script>";
				redirect(base_url('absen/absenDosen'),'refresh');
				
			}

		} else {
			$a = 1;
			for ($i=0; $i < $jumlahabsen-1; $i++) { 
				$hasil = $this->input->post('absen'.$a.'[0]', TRUE);
				$pecah = explode('-', $hasil);
				$datax[] = array(
					'npm_mahasiswa' => $pecah[1],
					'kd_jadwal' => $kode->kd_jadwal,
					'tanggal' => $this->input->post('tgl',TRUE),
					'pertemuan' => 1,
					'kehadiran' => $pecah[0],
					'transaksi' => md5($kode->kd_jadwal.'1')
					);
				$a++;
			}

			echo "<pre>";
			var_dump($datax);exit();
			echo "</pre>";

			$this->db->insert_batch('tbl_absensi_mhs_new_'.$actyear, $datax);

			// cek sks mk
			$pro = substr($kode->kd_jadwal, 0,5);
			$arr = ['kd_matakuliah' => $kode->kd_matakuliah, 'kd_prodi' => $pro];
			$sks = $this->hilm->moreWhere($arr, 'tbl_matakuliah')->row()->sks_matakuliah;

			if ($sks == '2') {
                $date = new DateTime($curentTime)	;
                $date->add(new DateInterval('PT90M'));
			} else {
                $date = new DateTime($curentTime)	;
                $date->add(new DateInterval('PT135M'));
				$fixtime = $date->format('H:i:s');
			}
			
			$das = array(
					'kd_jadwal'	=> $kode->kd_jadwal,
					'pertemuan'	=> 1,
					'tgl'		=> $this->input->post('tgl',TRUE),
					'materi'	=> $this->input->post('bahas', TRUE),
					'jam_masuk'	=> date('H:i:s'),
					'jam_keluar'=> $fixtime,
					'kd_trk' 	=> md5($kode->kd_jadwal.'1'),
					'created_at'=> date('Y-m-d H:i:s')
					);
			var_dump($das);exit();
			$this->db->insert('tbl_materi_abs', $das);

			echo "<script>alert('Sukses');</script>";
			redirect(base_url('absen/absenDosen'),'refresh');
		}
	}

	function review($idjdl)
	{
		$data['idjdl'] = $idjdl;
		$data['page'] = "v_reviewmeet";
		$this->load->view('template', $data);
	}

	function loadMeet($idjdl,$meet)
	{
		$act  = getactyear();
		$kdjdl= get_kd_jdl($idjdl);
		$load = $this->absen->reviewAbsen($idjdl,$meet)->result();

		// get tanggal pertemuan
		foreach ($load as $val) {
			$tgl = $val->tanggal;
		}

		// get transaksi absen
		$kdtrk = md5($kdjdl.$meet);
		$transaksi = $this->app_model->getdetail('tbl_transaksi_absen_'.$act, 'kd_transaksi', $kdtrk,'id','asc');

		// get materi absen
		$materi = $this->app_model->getdetail('tbl_materi_abs_'.$act, 'kd_trk', $kdtrk,'id','asc');

		// for table content
		$compulate = '';

		foreach ($load as $key) {
				
			$center = '<tr>';
			$center .= '<td>'.$key->npm_mahasiswa.'</td>';
			$center .= '<td>'.get_nm_mhs($key->npm_mahasiswa).'</td>';
			$center .= '<td>'.$key->kehadiran.'</td>';
			$center .= '</tr>';

			$compulate = $center.$compulate;
		}
		// end of table content

		// set active class tab panel
		if ($meet == 1) {
			$clsact = 'in active';
		} else {
			$clsact = 'active in';
		}
		
		// generate table for content
		$list =  '<div id="'.$meet.'" class="tab-pane fade '.$clsact.'">';

		// button just appear if presence havent validate
		if (!$transaksi->result() && $materi->num_rows() > 0) {
			$list .= '<a class="btn btn-warning" href="#edit" data-toggle="modal" onclick="loadEdit(\''.$kdtrk.'\')"><i class="icon icon-pencil"></i> Rubah data</a><hr>';
		}

		// header akan muncul jika telah dilakukan validasi oleh mahasiswa
		if (count($transaksi->result()) > 0) {
			
			// header
			$list .= '<hr>';
			$list .= '<table>';
			$list .= '<tr>';
			$list .= '<td><b>Pertemuan</b></td>';
			$list .= '<td>:</td>';
			$list .= '<td style="padding-right:40px">'.$meet.'</td>';
			$list .= '<td><b>Tanggal</b></td>';
			$list .= '<td>:</td>';
			$list .= '<td>'.TanggalIndo($tgl).'</td>';
			$list .= '</tr>';
			$list .= '<tr>';
			$list .= '<td><b>Jam Mulai</b></td>';
			$list .= '<td>:</td>';
			$list .= '<td style="padding-right:40px">'.get_dtl_jadwal($kdjdl)['waktu_mulai'].'</td>';
			$list .= '<td><b>Jam Selesai</b></td>';
			$list .= '<td>:</td>';
			$list .= '<td>'.get_dtl_jadwal($kdjdl)['waktu_selesai'].'</td>';
			$list .= '</tr>';
			$list .= '<tr>';
			$list .= '<td><b>Jumlah Peserta Kelas</b></td>';
			$list .= '<td>:</td>';
			$list .= '<td style="padding-right:40px">'.count($load).'</td>';
			$list .= '<td><b>Waktu Validasi Mahasiswa</b></td>';
			$list .= '<td>:</td>';
			$list .= '<td>'.$transaksi->row()->timestamp.'</td>';
			$list .= '</tr>';
			$list .= '<tr>';
			$list .= '<td><b>Materi Pengajaran</b></td>';
			$list .= '<td>:</td>';
			$list .= '<td style="padding-right:40px">'.$materi->row()->materi.'</td>';
			$list .= '<td><b>Validator</b></td>';
			$list .= '<td>:</td>';
			$list .= '<td>'.get_nm_mhs($transaksi->row()->audit_mhs).' - '.$transaksi->row()->audit_mhs.'</td>';
			$list .= '</tr>';
			$list .= '</table>';
			$list .= '<br>';
			// header end

		}
		
		$list .= '<table id="example1" class="table table-bordered table-striped">';
		$list .= '<thead>';
		$list .= '<tr> ';
		$list .= '<th>NPM</th>';
		$list .= '<th>Nama Mahasiswa</th>';
		$list .= '<th>Status</th>';
		$list .= '</tr>';
		$list .= '</thead>';
		$list .= '<tbody>';
		$list .= $compulate;
		$list .= '</tbody>';
		$list .= '</table>';
		$list .= '</div>';
		// end of generate content

		echo $list;
		
	}

	function editData($transc)
	{
		$data['list'] = $this->absen->reviewAbsen('','',$transc);
		foreach ($data['list']->result() as $key) {
			$kdjdl = $key->kd_jadwal;
			$meets = $key->pertemuan;
		}
		$prod = substr($kdjdl, 0,5);
		$kdmk = get_dtl_jadwal($kdjdl)['kd_matakuliah'];
		$data['nama_mk'] = get_nama_mk($kdmk,$prod);
		$data['kelas'] = get_dtl_jadwal($kdjdl)['kelas'];
		$data['meeting'] = $meets;
		$data['transaks'] = $transc;
		$this->load->view('modal_edit_absen', $data);
	}

	function updateAbsen()
	{
		$act = getactyear();
		extract(PopulateForm());

		$sts = xss_clean($status);
		$uid = xss_clean($user);
		$trk = xss_clean($kdtrk);
		
		for ($i = 0; $i < count($sts); $i++) {
			$data = array('kehadiran' => $sts[$i][$uid[$i]]);
			$this->db->where('npm_mahasiswa', $uid[$i]);
			$this->db->where('transaksi', $trk[$i]);
			$this->db->update('tbl_absensi_mhs_new_'.$act, $data);
		}
		echo "<script>alert('Berhasil!');history.go(-1)</script>";
	}

}

/* End of file AbsenDosen.php */
/* Location: .//tmp/fz3temp-1/AbsenDosen.php */