<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absen_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function loadListJadwal($kdkrs,$day)
	{
		$this->db->select('b.*');
		$this->db->from('tbl_krs a');
		$this->db->join('tbl_jadwal_matkul b', 'a.kd_jadwal = b.kd_jadwal');
		$this->db->where('b.hari', $day);
		$this->db->like('a.kd_krs', $kdkrs, 'after');
		return $this->db->get();
	}

	function loadPresence($kdjdl)
	{
		$actyear = getactyear();

		// $this->db->where('kd_jadwal', $kdjdl);
		// $this->db->order_by('pertemuan', 'desc');
		// $keyTransaction = $this->db->get('tbl_absensi_mhs_new_'.$actyear, 1)->row();

		$this->db->where('tanggal', date('Y-m-d'));
		// $this->db->where('transaksi', $keyTransaction->transaksi);
		return $this->db->get('tbl_absensi_mhs_new_'.$actyear);
	}

	function validAbsen($trk)
	{
		$log = $this->session->userdata('sess_login');
		$act = getactyear();

		$this->db->where('transaksi', $trk);
		$this->db->where('kehadiran', 'H');
		$h = $this->db->get('tbl_absensi_mhs_new_'.$act);

		$this->db->where('transaksi', $trk);
		$this->db->where('kehadiran', 'I');
		$i = $this->db->get('tbl_absensi_mhs_new_'.$act);

		$this->db->where('transaksi', $trk);
		$this->db->where('kehadiran', 'S');
		$s = $this->db->get('tbl_absensi_mhs_new_'.$act);

		$this->db->where('transaksi', $trk);
		$this->db->where('kehadiran', 'A');
		$a = $this->db->get('tbl_absensi_mhs_new_'.$act);

		foreach ($h->result() as $val) {
			$jadwal = $val->kd_jadwal;
		}

		// var_dump($jadwal);exit();

		$lecture = get_dtl_jadwal($jadwal)['kd_dosen'];

		$data = [
			'kd_transaksi'	=> $trk,
			'kd_jadwal'		=> $jadwal,
			'alfa' 			=> $a->num_rows(),
			'hadir' 		=> $h->num_rows(),
			'ijin' 			=> $i->num_rows(),
			'sakit' 		=> $s->num_rows(),
			'audit_dosen'	=> $lecture,
			'audit_mhs'		=> $log['userid'],
			'timestamp'		=> date('Y-m-d H:i:s')
		];

		$this->db->insert('tbl_transaksi_absen_'.getactyear(), $data);
		echo "<script>alert('Berhasil!');history.go(-1);</script>";
	}

	function jadwalajar($uid,$year,$day)
	{
		$this->db->where('kd_dosen', $uid);
		$this->db->where('kd_tahunajaran', $year);
		$this->db->where('hari', $day);
		return $this->db->get('tbl_jadwal_matkul');
	}

	function getLastMeeting($kdjdl)
	{
		$act = getactyear();
		$tbl = 'tbl_absensi_mhs_new_'.$act;

		$this->db->distinct();
		$this->db->select('tanggal,max(pertemuan) AS pertemuan');
		$this->db->from($tbl);
		$this->db->where('kd_jadwal', $kdjdl);
		$this->db->order_by('id_absen', 'desc');
		$this->db->limit(1);
		return $this->db->get();
	}

	function amountMemberClass($kdjdl)
	{
		$this->db->where('kd_jadwal', $kdjdl);
		return $this->db->get('tbl_krs')->num_rows();
	}

	function reviewAbsen($jdl,$meet,$transaction = NULL)
	{		
		$actyr = getactyear();

		if (!is_null($transaction)) {
			$this->db->where('transaksi', $transaction);
		} else {
			$kdjdl = get_kd_jdl($jdl);

			$this->db->where('kd_jadwal', $kdjdl);
			$this->db->where('pertemuan', $meet);	
		}

		$this->db->order_by('npm_mahasiswa', 'asc');
		return $this->db->get('tbl_absensi_mhs_new_'.$actyr);
	}

}

/* End of file Absen_model.php */
/* Location: ./application/models/Absen_model.php */