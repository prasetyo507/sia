<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Iuran extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(72)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		if ($this->session->userdata('sess_jur') == TRUE){
			$data['mahasiswa'] = $this->app_model->getdetail('tbl_mahasiswa', 'KDPSTMSMHS',$this->session->userdata('sess_jur'), 'NIMHSMSMHS', 'ASC')->result();
			$data['page']='data/iuran_detail';
		} else {
			$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunajaran', 'id_tahunajaran', 'ASC')->result();
			$data['page']='data/iuran_view';
		}
		$this->load->view('template', $data);
	}

	function back()
	{
		$this->session->unset_userdata('sess_jur');
		$this->session->unset_userdata('sess_angk');
		redirect('data/iuran','refresh');
	}

	function caridata()
	{
		$fak = explode('-', $this->input->post('fakultas', TRUE));
		$data['kd_fakultas'] = $fak[0];
		$data['fakultas'] = $fak[1];
		$jur = explode('-', $this->input->post('jurusan', TRUE));
		$data['kd_prodi'] = $jur[0];
		$data['prodi'] = $jur[1];
		$data['angkatan'] = $this->input->post('tahunajaran', TRUE);
		$this->session->set_userdata('sess_jur',$jur[0]);
		$data['mahasiswa'] = $this->app_model->getdetail('tbl_mahasiswa', 'KDPSTMSMHS', $jur[0], 'NIMHSMSMHS', 'ASC')->result();
		$data['page']='data/iuran_detail';
		$this->load->view('template', $data);
	}

	function get_jurusan($id){
		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function load_status_bayar($npm){

		$data['']=$this->db->get('tbl_karyawan')->result();
		
		$this->load->view('penugasan_dosen',$data);
	}

	function insert_nr(){
		$this->db->select('*');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_index_pembayaran b', 'a.KDPSTMSMHS = b.kd_prodi', 'left');
		//$this->db->where('a.id_mhs', 2);
		$q=$this->db->get()->result();

		//print_r($q);

		foreach ($q as $isi) {
			$ambil = substr($isi->NIMHSMSMHS, 2);
			$briva = '70306'.$ambil;
			$balance = (0-($isi->jumlah));

			if ($isi->kd_jenis == 'BPP') {
				for ($smtr=1; $smtr < 2; $smtr++) { 
					for ($i=1; $i < 7 ; $i++) {

						$data = array(
							'nim' 		=> $isi->NIMHSMSMHS,
							'briva' 	=> $briva,
							'keterangan'=> $isi->kd_jenis.' ANG-'.$i,
							'debit'		=> 0,
							'credit'	=> $isi->jumlah,
							'balance'	=> $balance,
							'smtr'		=> $smtr,
							'type'		=> 1,
							'flag'		=> 3,
							'idx'		=> $i+2
							);
						$this->db->insert('tbl_rekap_pembayaran_mhs', $data);
					}
				}
			}elseif ($isi->kd_jenis == 'PRAK') {
				for ($smtr=1; $smtr < 2; $smtr++) { 
					$data = array(
							'nim' 		=> $isi->NIMHSMSMHS,
							'briva' 	=> $briva,
							'keterangan'=> $isi->kd_jenis,
							'debit'		=> 0,
							'credit'	=> $isi->jumlah,
							'balance'	=> $balance,
							'smtr'		=> $smtr,
							'type'		=> 1,
							'flag'		=> 2,
							'idx'		=> 2
							);
					$this->db->insert('tbl_rekap_pembayaran_mhs', $data);
				}
			}elseif ($isi->kd_jenis == 'KMHS') {
				for ($smtr=1; $smtr < 2; $smtr++) { 
					$data = array(
							'nim' 		=> $isi->NIMHSMSMHS,
							'briva' 	=> $briva,
							'keterangan'=> $isi->kd_jenis,
							'debit'		=> 0,
							'credit'	=> $isi->jumlah,
							'balance'	=> $balance,
							'smtr'		=> $smtr,
							'type'		=> 1,
							'flag'		=> 1,
							'idx'		=> 1
							);
					$this->db->insert('tbl_rekap_pembayaran_mhs', $data);
				}
			}

			

			
		}


	}

}

/* End of file Iuran.php */		
/* Location: .//C/Users/danum246/AppData/Local/Temp/fz3temp-1/Iuran.php */