<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Biodata extends CI_Controller {



	public function __construct()

	{

		parent::__construct();

		if ($this->session->userdata('sess_login') == TRUE) {

			$cekakses = $this->role_model->cekakses(40)->result();

			if ($cekakses != TRUE) {

				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";

			}

		} else {

			redirect('auth','refresh');

		}

	}



	public function index()

	{

		$user = $this->session->userdata('sess_login');

		$cek_user = $user['user_type'];

		$userid = $user['userid'];



		if ($cek_user == 1) {

			$data['nopok'] = $this->db->query('select * from tbl_karyawan where nik = '.$userid.'')->row();

			$data['rows']=$this->db->query('select * from tbl_biodata where nomor_pokok = '.$data['nopok']->nid.'')->result();
			$count=count($data['rows']);

			if ($count >= 0) {
					$data['isi']=1;								
				}else{
					$data['isi']=0;
				}

		} elseif ($cek_user == 2) {

			$data['nopok'] = $this->db->query('select * from tbl_mahasiswa where NIMHSMSMHS = '.$userid.'')->row();

			$data['rows']=$this->db->query('select * from tbl_biodata where nomor_pokok = '.$data['nopok']->nid.'')->result();
			$count=count($data['rows']);

			if ($count >= 0) {
					$data['isi']=1;								
				}else{
					$data['isi']=0;
				}

		}

		$data['provinsi'] = $this->db->query('select * from tbl_info_lokasi order by lokasi_nama asc');

		$data['fakultas'] = $this->db->get('tbl_fakultas')->result();

		$data['TA'] = $this->db->get('tbl_tahunajaran')->result();

		$data['jabatan'] = $this->db->get('tbl_jabatan')->result();

		$data['kampus'] = $this->db->get('tbl_kampus')->result();
		

		$data['cek'] = $user['user_type'];

		$data['page'] = 'data/biodata_form';

		$this->load->view('template',$data);

	}



	function get_jurusan($id){



        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();

		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";

        foreach ($jurusan as $row) {

            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";

        }

        $out .= "</select>";

        echo $out;

	}



	function get_divisi($id){



        $jurusan = $this->app_model->getdetail('tbl_divisi', 'kd_divisi', $id, 'id_divisi', 'ASC')->result();

		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";

        foreach ($jurusan as $row) {

            $out .= "<option value='".$row->kd_divisi."-".$row->divisi."'>".$row->divisi. "</option>";

        }

        $out .= "</select>";

        echo $out;

	}



	function save_data(){

		//die('kaskus');



		$nopok = ''.$this->input->post('nim').$this->input->post('nik').'';



		$data = array(

			'nomor_pokok' => $nopok,

			'nama' => $this->input->post('nama'),

			'jenis_kelamin' => $this->input->post('jekel'),

			'tempat_lahir' => $this->input->post('tempat_lahir'),

			'tanggal_lahir' => $this->input->post('tanggal_lahir'),

			'nama_sekolah_asal' => $this->input->post('nama_sekolah_asal'),

			'alamat_sekolah_asal' => $this->input->post('alamat_sekolah_asal'),

			'nama_jalan' => $this->input->post('jalan'),

			'rt' => $this->input->post('rt'),

			'rw' => $this->input->post('rw'),

			'kelurahan' => $this->input->post('kel'),

			'kecamatan' => $this->input->post('kec'),

			'kabupaten' => $this->input->post('kab'),

			'kota' => $this->input->post('kota'),

			'provinsi' => $this->input->post('provinsi'),

			'kode_pos' => $this->input->post('kdpos'),

			'telepon' => $this->input->post('tlp'),

			'handphone' => $this->input->post('hp'),

			'email' => $this->input->post('email'),

			'ayah' => $this->input->post('ayah'),

			'ibu' => $this->input->post('ibu'),

			'alamat_ortu' => $this->input->post('alamat_ortu'),

			'fakultas' => $this->input->post('faks'),

			'jurusan' => $this->input->post('jurusan'),

			'kelas' => $this->input->post('kelas'),

			'kampus' => $this->input->post('kampus'),

			'semester' => $this->input->post('semester'),

			'tahun_akademik' => $this->input->post('ta'),

			'tgl_bayar' => $this->input->post('tgl_bayar'),

			'jumlah_tagihan' => $this->input->post('jumlah_tagihan'),

			'cicilan' => $this->input->post('cicilan'),

			'no_briva' => $this->input->post('briva_no'),

			'jabatan' => $this->input->post('jbtn'),

			'divisi' => $this->input->post('divisi'),

			'bio_type' => $this->input->post('bio_type')

			);



		//var_dump($data);die();



		$cek = $this->db->query('select * from tbl_biodata where nomor_pokok = "'.$nopok.'"')->result();

		$cek_count = count($cek);



		if ($cek_count == 0) {

			$query =  $this->db->insert('tbl_biodata', $data);

		}else{

			$query =  $this->db->where('nomor_pokok',$nopok)

							   ->update('tbl_biodata', $data);

		}



		if ($query) {

			echo '<script>alert("Tersimpan"); window.location.href = "'.base_url().'data/biodata/";</script>';

		}

	}



}



/* End of file Biodata.php */

/* Location: ./application/controllers/Biodata.php */