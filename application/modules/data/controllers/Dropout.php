<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dropout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('setting_model');
        error_reporting(0);
        if ($this->session->userdata('sess_login') == TRUE) {
            $cekakses = $this->role_model->cekakses(147)->result();
            if ($cekakses != TRUE) {
                echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
            }
        } else {
            redirect('auth','refresh');
        }
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('data/do_model', 'do');
        $this->load->library('cart');
	}

	public function form()
	{
		$data['page']='v_lulusan_form';
        $this->load->view('template', $data);
	}

    function index(){
        $this->session->unset_userdata('tahun');
        $this->session->unset_userdata('jurusan');

        $logged = $this->session->userdata('sess_login');
        $pecah = explode(',', $logged['id_user_group']);
        $jmlh = count($pecah);

        for ($i=0; $i < $jmlh; $i++) { 
            $grup[] = $pecah[$i];
        }
        
        if ((in_array(10, $grup))) { //baa
            $data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'data/v_do_select_baa';

        } elseif ((in_array(9, $grup))) { // fakultas
            $data['jurusan']=$this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$logged['userid'],'prodi','asc')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'data/v_do_select_fak';

        } elseif ((in_array(8, $grup))) { // prodi
            $data['jurusan']    = $this->db->where('kd_prodi',$logged['userid'])->get('tbl_jurusan_prodi')->result();
            $data['akademik']   = $this->db->get('tbl_tahunakademik')->result();

            $data['list'] = $this->do->listSkep($logged['userid'])->result();

            //$data['page'] = 'data/v_do_select_prodi';
            $data['page'] = 'data/v_do_home';

        } elseif ((in_array(1, $grup))) { // admin
            $data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'data/v_do_select_baa';
        }

        $this->load->view('template', $data);   
    }

    function get_jurusan($id)
	{
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."_".$id."'>".$row->prodi. "</option>";
        }
        // $out .= "<option value='99_".$id."'>SEMUA PROGRAM STUDI</option>";
        $out .= "</select>";

        echo $out;
	}

    function get_filter_do($key){
        $logged = $this->session->userdata('sess_login');


        if ($key == '1') {
            $rows = $this->db->get('tbl_tahunakademik')->result();

            $out = "<select class='form-control' name='filter' id='fl'><option>--Pilih Filter--</option>";
            foreach ($rows as $row) {
                $out .= "<option value='".$row->kode."'>".$row->tahun_akademik. "</option>";
            }
        } elseif ($key == '2') {
            $rows = $this->db->query('SELECT mhs.`TAHUNMSMHS` FROM tbl_mahasiswa mhs WHERE mhs.`KDPSTMSMHS` = '.$logged["userid"].'
                                      GROUP BY mhs.`TAHUNMSMHS`')->result();

            $out = "<select class='form-control' name='filter' id='fl'><option>--Pilih Filter--</option>";
            foreach ($rows as $row) {
                $out .= "<option value='".$row->TAHUNMSMHS."'>".$row->TAHUNMSMHS. "</option>";
            }
        }

        

        $out .= "</select>";
        echo $out;   
    }

    function view(){

        $jf = $this->input->post('jfilter');
        $jur= $this->input->post('jurusan');
        $fil= $this->input->post('filter');

        $this->session->set_userdata('jurusan',$jur);
        $this->session->set_userdata('filter',$jf);

        if ($jf == 1) {
            $this->session->set_userdata('tahunajaran',$fil);                   
        } elseif ($jf == 2) {
            $this->session->set_userdata('angkatan',$fil);
        }

        redirect(base_url().'data/Dropout/data_do','refresh');
    }

    function saveSessBaa()
    {
        extract(PopulateForm());
        $array = array(
            'tahunajaran' => $tahun,
            'jurusan' => $jurusan
        );
        
        $this->session->set_userdata('sessforselectbaa', $array);
        redirect(base_url('data/dropout/viewDoBaa'));
    }

    function viewDoBaa()
    {
        $sesslogbaa = $this->session->userdata('sessforselectbaa');
        $data['rows'] = $this->do->getDataByTa($sesslogbaa['jurusan'],$sesslogbaa['tahunajaran']);
        $data['page'] = 'v_do_view';
        $this->load->view('template', $data);        
    }

    function data_do(){
        if ($this->session->userdata('filter') == 1) {
            /*
            $data['rows'] = $this->db->from('tbl_dropout_mhs do_mhs')
                                     ->join('tbl_dropout_skep do_skep', 'do_skep.no_skep = do_mhs.no_skep', 'left')
                                     ->join('tbl_dropout_skep skep', 'do_skep. = mhs.column', 'left')
                                     ->where('mhs.KDPSTMSMHS', $this->session->userdata('jurusan'))
                                     ->where('mhs.TAHUNMSMHS', $this->session->userdata('tahunajaran'))
                                     ->get()->result();
            */
            $data['rows'] = $this->do->getDataByTa($this->session->userdata('jurusan'),$this->session->userdata('tahunajaran'));

            $data['page']='v_do_view';

        } elseif ($this->session->userdata('filter') == 2) {
            /*
            $data['rows'] = $this->db->from('tbl_mahasiswa mhs')
                                     ->where('mhs.KDPSTMSMHS', $this->session->userdata('jurusan'))
                                     ->where('mhs.TAHUNMSMHS', $this->session->userdata('angkatan'))
                                     ->get()->result();
            */
            $data['rows'] = $this->do->getDataByAkt($this->session->userdata('jurusan'),$this->session->userdata('angkatan'));

            $data['page']='v_data_do_angk';
        }

        // var_dump($data['rows']);die($this->session->userdata('jurusan').$this->session->userdata('angkatan'));

        $this->load->view('template', $data);        
        
    }

    function data_do_old(){

        if ($this->session->userdata('jurusan')) {
            $data['rows'] = $this->db->query("SELECT * FROM tbl_dropout do 
                                            JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = do.`npm_mahasiswa`
                                            WHERE mhs.`KDPSTMSMHS` = '".$this->session->userdata('jurusan')."' AND do.`tahunajaran` = ".$this->session->userdata('tahun')."")->result();

            $data['page']='v_do_view';
            $this->load->view('template', $data);    
        }else{
            redirect(base_url().'data/Dropout','refresh');
        }

        
    }

    function form_skep_do(){
        $this->session->unset_userdata('drop');
        $this->session->set_userdata('drop',array());

        $data['ta'] = $this->db->get('tbl_tahunakademik')->result();

        $data['page']='v_do_form';
        $this->load->view('template', $data);
    }

    function add()
    {
    	$logged = $this->session->userdata('sess_login');

    	extract(PopulateForm());

        for ($i=0; $i < count($nim); $i++) { 
            
            // insert to tbl_dropout
            $data_do[] = [
                        'skep' => $skep[$i],
                        'tgl_skep' => $tgl[$i],
                        'npm_mahasiswa' => $nim[$i],
                        'alasan' => $alasan[$i],
                        'tahunajaran' => $thajar[$i],
                        'audit_user' => $logged['userid'],
                        'audit_time' =>  date('Y-m-d H:i:s'),
                        'key_skep' => md5($skep[$i])
                        ];

            // insert to tbl_status_mahasiswa
            $get = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim[$i],'NIMHSMSMHS','asc')->row()->SMAWLMSMHS;
            $smt = $this->app_model->get_semester($get);

            $mhs_do[] = [
                        'npm' => $nim[$i], 
                        'status' => 'D', 
                        'tahunajaran' =>  $thajar[$i], 
                        'tanggal' => date('Y-m-d'),
                        'validate' => 1,
                        'semester' => $smt
                    ];

            // update tbl_mahasiswa
            $up_mhs = array('STMHSMSMHS' => 'D');
            $this->db->where('NIMHSMSMHS', $nim[$i]);
            $this->db->update('tbl_mahasiswa', $up_mhs);

        }

        $this->db->insert_batch('tbl_dropout', $data_do);
    	
    	$this->db->insert_batch('tbl_status_mahasiswa', $mhs_do);

    	redirect(base_url('data/dropout'),'refresh');
    }

    function delete($id)
    {
    	$this->db->where('npm_mahasiswa', $id);
    	$this->db->delete('tbl_dropout');

    	$this->db->where('npm', $id);
        $this->db->where('status', 'K');
    	$this->db->delete('tbl_status_mahasiswa');

    	redirect(base_url('data/dropout/view'),'refresh');
    }

    function edit_dropout($id)
    {
    	$data['row']=$this->db->where('npm_mahasiswa', $id)
    				->get('tbl_dropout')->row();

    	$this->load->view('v_do_edit', $data);
    }


    function load_mhs_autocomplete()
    {
        $logged = $this->session->userdata('sess_login');

        $not_st = array('K','L');    

        $prodi = $this->session->userdata('jurusan');

        $this->db->distinct();

        $this->db->select("a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS");

        $this->db->from('tbl_mahasiswa a');

        $this->db->where_not_in('a.STMHSMSMHS',$not_st);

        $this->db->where('a.KDPSTMSMHS', $logged['userid']);

        $this->db->like('a.NMMHSMSMHS', $_GET['term'], 'both');

        $this->db->or_like('a.NIMHSMSMHS', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

                    'nama'          => $row->NMMHSMSMHS,

                    'value'         => $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS,

                    'tahun_masuk'   => $row->TAHUNMSMHS,

                    'nim'          => $row->NMMHSMSMHS,

                    );
        }

        echo json_encode($data);

    }

    function outtemporary()
    {
        extract(PopulateForm());

        $mhs = explode(' - ', $npm_mhs);
        $nim = $mhs[0];

        // make session for foreach
        $ses    = $this->session->userdata('drop');
        $cp     = count($ses);

        if ($cp == 0) {
            $data = array(
                    $cp => array(
                        'noskep' => $no_skep, 
                        'tgskep' => $tgl_skep,
                        'thajar' => $ta_skep, 
                        'nama' => $mhs[1], 
                        'nim' => $nim, 
                        'smawl' => $tm_mhs,
                        'alasan' => $alasan
                    )
                );
            $this->session->set_userdata('jml_array',$cp);     

            $arr = $ses + $data;
            $count = $cp+1;
            $this->session->unset_userdata('drop');
            $this->session->set_userdata('drop',$arr);
            $this->session->set_userdata('jml_array',$count);

         } else {
            $i_arr = $this->session->userdata('jml_array');
            $data = array(
                $i_arr => array(
                    'noskep' => $no_skep, 
                    'tgskep' => $tgl_skep,
                    'thajar' => $ta_skep, 
                    'nama' => $mhs[1], 
                    'nim' => $nim, 
                    'smawl' => $tm_mhs,
                    'alasan' => $alasan
                )
            );     

            $arr = $ses + $data;
            $count = $i_arr+1;
            $this->session->unset_userdata('drop');
            $this->session->unset_userdata('jml_array');
            
            $this->session->set_userdata('drop',$arr);
            $this->session->set_userdata('jml_array',$count);
         }
    }

    function loadTable()
    {
        $data['team'] = $this->cart->contents();
        
        $this->load->view('temptableout',$data); 
    }

    function deleteList($id){
        $arr = $this->session->userdata('drop');

        unset($arr[$id]);
        
        $this->session->unset_userdata('drop');
        $this->session->set_userdata('drop',$arr);
    }

    function loaddetil($id)
    {
        $data['years'] = $this->db->get('tbl_tahunakademik')->result();

        $this->load->model('hilman_model','hil');
        $uid = $this->session->userdata('sess_login');

        $arr = ['key_skep' => $id, 'audit_user' => $uid['userid']];
        $data['dats'] = $this->hil->moreWhere($arr, 'tbl_dropout');
        // $data['dats'] = $this->app_model->getdetail('tbl_dropout','key_skep',$id,'key_skep','asc');

        $this->load->view('contentmodeldo', $data);
    }

    function addFromEdit($key)
    {
        $data['get'] = $this->app_model->getdetail('tbl_dropout','key_skep',$key,'id','asc')->row();
        $this->load->view('addmodal_do', $data);
    }

    function saveFromEdit()
    {
        $logged = $this->session->userdata('sess_login');
        extract(PopulateForm());

        $exp = explode(' - ', $nim);
        $npm = $exp[0];

        // insert to tbl_dropout
        $data_do = [
                    'skep'          => $skep,
                    'tgl_skep'      => $tgl,
                    'npm_mahasiswa' => $npm,
                    'alasan'        => $alasan,
                    'tahunajaran'   => $thajar,
                    'audit_user'    => $logged['userid'],
                    'audit_time'    => date('Y-m-d H:i:s'),
                    'key_skep'      => $keyskep
                ];

        $this->app_model->insertdata('tbl_dropout', $data_do);

        // insert to tbl_status_mahasiswa
        $get = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'NIMHSMSMHS','asc')->row()->SMAWLMSMHS;
        $smt = $this->app_model->get_semester($get);

        $mhs_do = [
                'npm'           => $npm, 
                'status'        => 'D', 
                'tahunajaran'   => $thajar, 
                'tanggal'       => date('Y-m-d'),
                'validate'      => 1,
                'semester'      => $smt
            ];

        $this->app_model->insertdata('tbl_status_mahasiswa',$mhs_do);

        // update tbl_mahasiswa
        $up_mhs = array('STMHSMSMHS' => 'D');
        $this->db->where('NIMHSMSMHS', $npm);
        $this->db->update('tbl_mahasiswa', $up_mhs);

        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }

    function removeFromList($nim,$tahunajar)
    {
        $this->db->where('npm_mahasiswa', $nim);
        $this->db->where('tahunajaran', $tahunajar);
        $this->db->delete('tbl_dropout');

        $this->db->where('npm', $nim);
        $this->db->where('tahunajaran', $tahunajar);
        $this->db->delete('tbl_status_mahasiswa');

        $this->db->where('NIMHSMSMHS', $nim);
        $this->db->update('tbl_mahasiswa', ['STMHSMSMHS' => 'A']);        
    }

    function updateDataDo()
    {
        extract(PopulateForm());

        $this->db->where('skep', $idskep);
        $this->db->update('tbl_dropout', ['skep' => $noskep, 'tahunajaran' => $tahunajar, 'tgl_skep' => $tgl_skep]);

        echo "<script>alert('Berhasil rubah data!');history.go(-1);</script>";
    }

}

/* End of file Dropout.php */
/* Location: ./application/controllers/Dropout.php */