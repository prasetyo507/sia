<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_maba_s1.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr> 
            <th>No</th>
            <th>ID Registrasi</th>
            <th>Nama</th>
            <th>Kelas</th>
            <th>Program Pilihan</th>
            <th>Kampus</th>
            <th>Uang Pendaftaran</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($look as $row) { $a = $no;?>
        
        <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $row->nomor_registrasi;?></td>
            <td><?php echo $row->nama;?></td>
            <?php if ($row->kelas == 'sr') {
                $kls = 'SORE';
            } elseif ($row->kelas == 'ky') {
                $kls = 'KARYAWAN';
            } else {
                $kls = 'PAGI';
            }
             ?>
            <td><?php echo $kls; ?></td>
            <td><?php echo $row->prodi;?></td>
            <?php if ($row->kampus == 'bks') {
                $kps = 'BEKASI';
            } else {
                $kps = 'JAKARTA';
            }
             ?>
            <td><?php echo $kps; ?></td>
            <td><?php $uang = 250000; echo $uang;?></td>
        </tr>
        <?php $no++; } ?>
        <tr>
            <td colspan="6" >Total</td>
            <td><?php echo $uang*$a; ?></td>
        </tr>
        
    </tbody>
</table>