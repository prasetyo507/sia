<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Publikasi Nilai</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>data/data_publikasi" class="btn btn-primary"> Kembali </a><br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr>
	                        	<th>No</th>
                                <th>Kode MK</th>
                                <th>Mata Kuliah</th>
                                <th>Dosen</th>
                                <th>Kelas</th>
                                <th>Last Update</th>
                                <th>Last Publish</th>
	                            <!-- <th width="120">Aksi</th> -->
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($getData as $row){?>
	                        <tr>
	                        	<?php
	                        		  $time1 = strtotime($row->audit_date);
									  $audit = date("d-m-Y H:i", $time1);
									  $time2 = strtotime($row->publis_date);
									  $publis = date("d-m-Y H:i", $time2);
								?>
	                        	<td <?php if ($time2 <= $time1) {echo 'style="background:#FFB200;"';} ?>><?php echo number_format($no); ?></td>
	                        	<td <?php if ($time2 <= $time1) {echo 'style="background:#FFB200;"';} ?>><?php echo $row->kd_matakuliah; ?></td>
	                        	<td <?php if ($time2 <= $time1) {echo 'style="background:#FFB200;"';} ?>><?php echo $row->nama_matakuliah; ?></td>
	                        	<td <?php if ($time2 <= $time1) {echo 'style="background:#FFB200;"';} ?>><?php echo $row->nama; ?></td>
	                        	<td <?php if ($time2 <= $time1) {echo 'style="background:#FFB200;"';} ?>><?php echo $row->kelas; ?></td>
	                        	<td <?php if ($time2 <= $time1) {echo 'style="background:#FFB200;"';} ?>><?php echo $audit; ?></td>
	                        	<td <?php if ($time2 <= $time1) {echo 'style="background:#FFB200;"';} ?>><?php echo $publis; ?></td>
	                        	<!-- <td class="td-actions">
									<a onclick="edit(<?php //echo $row->id_divisi;?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
									<a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php //echo base_url();?>data/divisi/deletedata/<?php //echo $row->id_divisi;?>"><i class="btn-icon-only icon-remove"> </i></a>
								</td> -->
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>