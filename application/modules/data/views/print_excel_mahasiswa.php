<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_mahasiswa.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border='1'>
	<thead>
        <tr> 
        	<th>No</th>
            <th>NIM</th>
        	<th>Mahasiswa</th>
            <th>Angkatan</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; foreach ($getData as $row) { ?>
        <tr>
        	<td><?php echo number_format($no); ?></td>
            <td><?php echo $row->NIMHSMSMHS; ?></td>
            <td><?php echo $row->NMMHSMSMHS; ?></td>
            <td><?php echo $row->TAHUNMSMHS; ?></td>
            <?php switch ($row->STMHSMSMHS) {
                                    case 'A':
                                        $stts = 'AKTIF';
                                        break;
                                    case 'L':
                                        $stts = 'LULUS';
                                        break;
                                    case 'K':
                                        $stts = 'KELUAR';
                                        break;
                                    case 'C':
                                        $stts = 'CUTI';
                                        break;
                                } ?>
            <td><?php echo $stts; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>