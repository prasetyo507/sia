<?php // var_dump($row);die(); ?>

                    <script>
                       $(document).ready(function(){
                        $.post('<?php echo base_url();?>data/karyawan/get_listjab/'+$(this).val(),{},function(get){
                               $('#jabatans').html(get);
                            });
                        });
                    </script>
<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Data Karyawan</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>data/karyawan/update_karyawan" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -30px;">    
                    <div class="control-group" id="">
                        <label class="control-label">Tipe Identitas</label>
                        <div class="controls">
                            <input type="radio" name="tipe" value="1" required/> NIK
                            <input type="radio" name="tipe" value="2" required/> NIDN
                            <input type="radio" name="tipe" value="3" required/> KTP
                            <input type="radio" name="tipe" value="4" required/> SIM
                            <input type="radio" name="tipe" value="5" required/> Paspor
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Nomor Identitas</label>
                        <div class="controls">
                            <input type="text" class="span4" name="nik" placeholder="Input Nomor Identitas" class="form-control" value="<?php echo $row->nik;?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Nama</label>
                        <div class="controls">
                            <input type="text" class="span4" name="nama" placeholder="Input Nama" class="form-control" value="<?php echo $row->nama;?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Jenis Kelamin</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="jk" value="P" <?php if($row->jns_kel=='P'){ echo 'checked'; } ?>/>
                                Pria
                            </label>
                            <label class="radio inline">
                                <input type="radio" name="jk" value="W" <?php if($row->jns_kel=='W'){ echo 'checked'; } ?>/>
                                Wanita
                            </label>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Telepon</label>
                        <div class="controls">
                            +62 ( <input type="text" class="span3" name="telepon" id="telepon_e" placeholder="Input Telepon" class="form-control" value="<?php //$dat = explode('+62',$row->hp); 
                                                                                                                                                                echo $row->hp; ?>" required/> ) 
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Email</label>
                        <div class="controls">
                            <input type="text" class="span4" name="email" placeholder="Input Email" class="form-control" value="<?php echo $row->email;?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Alamat</label>
                        <div class="controls">
                            <textarea class="span4" name="alamat" placeholder="Input Alamat" class="form-control" value="" required><?php echo $row->alamat;?></textarea>
                        </div>
                    </div>
                    <!-- <div class="control-group" id="">
                        <label class="control-label">Foto</label>
                        <div class="controls">
                            <input type="file" class="span4" name="foto" class="form-control"/>
                        </div>
                    </div> -->

                    <script>
                    $(document).ready(function(){
                        $('#lembaga1').change(function(){
                            $.post('<?php echo base_url();?>data/karyawan/get_listjab/'+$(this).val(),{},function(get){
                               //alert($(this).val());
                               $('#jabatan1').html(get);
                            });
                        });
                    });
                    </script>


                    <div class="control-group" id="">
                        <label class="control-label">Divisi</label>
                        <div class="controls">
                            <select class="span4" name="lembaga" id="lembaga1" class="form-control" required>
                                <option value="<?php echo $row->kd_divisi; ?>"><?php echo $row->divisi ?></option>
                                <option> -- </option>
                                <?php $no=1; foreach ($divisi as $isi) { ?>
                                    <option value="<?php echo $isi->kd_divisi; ?>"><?php echo $isi->divisi ?></option>
                                <?php $no++; } ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group" id="">
                        <label class="control-label">Jabatan</label>
                        <div class="controls">
                            <select class="span4" name="jabatan" id="jabatan1" class="form-control" value="" required>
                               
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">User Grup</label>
                        <div class="controls">
                             <select name="" multiple>
                                <option value="">Group 1</option>
                                <option value="">Group 2</option>
                                <option value="">Group 3</option>
                            </select> 
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Status</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="status" value="1" <?php if ($row->status==1){ echo 'checked'; };?> />
                                Aktif
                            </label>
                            <label class="radio inline">
                                <input type="radio" name="status" value="0" <?php if ($row->status==0){ echo 'checked'; };?> />
                                Tidak Aktif
                            </label>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>