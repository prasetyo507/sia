<script type="text/javascript">
	$(function () {
		var chart = $('#sekolah').highcharts({

		    chart: {
		        type: 'column'
		    },

		    title: {
		    	<?php  
	        				$nm = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$asal,'kd_prodi','asc')->row();
	        				$text = "'Prodi ".$nm->prodi."'"; ?>
		        text: <?php echo $text; ?> 
		    },

		    subtitle: {
		        text: 'Source: SIA UBHARAJAYA'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: [
	            <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
	            	echo $i.",";
	            } ?>
	            ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [{
		        name: 'SMA',
		        data: [
		        <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
		        		$daftar = $this->ig_model->getjumlahmabasklprodi($i,'SMA',$asal); echo $daftar.","; 
		        	}?>
		         ]
		    }, {
		        name: 'SMK',
		        data: [
		        <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
		        		$daftar = $this->ig_model->getjumlahmabasklprodi($i,'SMK',$asal); echo $daftar.","; 
		        	}?>
		         ]
		    }, {
		        name: 'MA',
		        data: [
		        <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
		        		$daftar = $this->ig_model->getjumlahmabasklprodi($i,'MA',$asal); echo $daftar.","; 
		        	}?>
		         ]
		    }, {
		        name: 'LAIN-LAIN',
		        data: [
		        <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
		        		$daftar = $this->ig_model->getjumlahmabasklprodi($i,'LAIN',$asal); echo $daftar.","; 
		        	}?>
		         ]
		    }],
		});
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id='sekolah' style='min-width: 300px; height: 400px; max-width: 1000px; margin: 0 auto'></div>