<?php $logged = $this->session->userdata('sess_login'); ?>

<script type="text/javascript">
	function getPage(tag,rule) {
		$('#'+tag).load('<?= base_url() ?>infografis/loadPage/'+rule);
	}
</script>

<!-- jumlah mahasiswa total per tahun -->
<script type="text/javascript">
	$(function () {
	    $('#jumlahmhs').highcharts({
	        chart: {
	            type: 'column'
	        },

	        title: {
	            text: <?= $textjmlmhs; ?> 
	        },

	        subtitle: {
	            text: 'Source : SIA UBHARAJAYA'
	        },

	        xAxis: {
	            categories: [
	                <?php foreach ($jmlmhs as $oz) {
	            			echo $oz->TAHUNMSMHS.',';
            		} ?>
	            ],
	            crosshair: true
	        },

	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Total'
	            }
	        },

	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },

	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },

	        series: [{
	            name: 'Jumlah Mahasiswa',
	            data: [
	            	<?php foreach ($jmlmhs as $oz) {
	            			echo $oz->jml.',';
	            	} ?>
	            ],
	            dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
	        }],
	    });
	});
</script>

<!--

<script type="text/javascript">
	//status mahasiswa pie-chart
	$(function () {

		// Create the chart
		$('#stsmhs-pie').highcharts({
		    chart: {
		        type: 'pie'
		    },
		    title: {
		        text: 'Status Mahasiswa UBHARAJAYA'
		    },
		    subtitle: {
		        text: 'Sumber: <a href="http://sia.ubharajaya.ac.id" target="_blank">sia.ubharajaya.com</a>'
		    },
		    plotOptions: {
		        series: {
		            dataLabels: {
		                enabled: true,
		                format: '{point.name}: {point.y:.f} Mahasiswa'
		            }
		        }
		    },

		    tooltip: {
		        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
		        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
		    },

		    "series": [
		        {
		            "name": "Browsers",
		            "colorByPoint": true,
		            "data": [
		            	<?php foreach ($takd as $val) { ?>
		            		<?php 
			            		if ( (in_array(8, $grup)) or (in_array(9, $grup))) {
			        				$akt = $this->info_model->getstatusaktif($logged['userid'],$val->kode);
			        				$cut = $this->info_model->getstatuscuti($logged['userid'],$val->kode);
			        				$non = $this->info_model->getstatusnonaktif($logged['userid'],$val->kode);
			        			}else{
			        				$akt = $this->info_model->getstatusaktif(0,$val->kode);
			        				$cut = $this->info_model->getstatuscuti(0,$val->kode);
			        				$non = $this->info_model->getstatusnonaktif(0,$val->kode);
			        			}
			        			$sum = $akt+$cut+$non;
			        		?>
		            		{
			                    "name": "<?= $val->tahun_akademik; ?>",
			                    "y": <?= $sum; ?>,
			                    "drilldown": "<?= $val->kode; ?>"
			                },
		            	<?php } ?>
		            ]
		        }
		    ],
		    "drilldown": {
		        "series": [
		        	<?php foreach ($takd as $key) { ?>
		        		
		        		// here highchart begin
		        		{
			                "name": "<?= $key->tahun_akademik; ?>",
			                "id": "<?= $key->kode; ?>",
			                "data": [
			                    [
			                        "AKTIF",
			                        <?php 
					            		if ( (in_array(8, $grup)) or (in_array(9, $grup))) {
					        				$act = $this->info_model->getstatusaktif($logged['userid'],$key->kode);
					        			}else{
					        				$act = $this->info_model->getstatusaktif(0,$key->kode);
					        			}
					        			echo $act;
					        		?>
			                    ],
			                    [
			                        "CUTI",
			                        <?php 
					            		if ( (in_array(8, $grup)) or (in_array(9, $grup))) {
					        				$cuts = $this->info_model->getstatuscuti($logged['userid'],$key->kode);
					        			}else{
					        				$cuts = $this->info_model->getstatuscuti(0,$key->kode);
					        			}
					        			echo $cuts;
					        		?>
			                    ],
			                    [
			                        "NON AKTIF",
			                        <?php 
					            		if ( (in_array(8, $grup)) or (in_array(9, $grup))) {
					        				$nonn = $this->info_model->getstatusnonaktif($logged['userid'],$key->kode);
					        			}else{
					        				$nonn = $this->info_model->getstatusnonaktif(0,$key->kode);
					        			}
					        			echo $nonn;
					        		?>
			                    ]
			                ]
			            },
			            // highchart end

		        	<?php } ?>
		        ]
		    }
		});
	});
</script>

-->


<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-graph"></i>
  				<h3>INFOGRAFIS</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#home1">Jumlah Mahasiswa</a></li>
					    <li><a data-toggle="tab" href="#home2" onclick="getPage('home2','STMHS')">Status Mahasiswa</a></li>
					    <li><a data-toggle="tab" href="#home3" onclick="getPage('home3','JMLLS')">Jumlah Lulusan</a></li>
					    <li><a data-toggle="tab" href="#home4" onclick="getPage('home4','IPKGRD')">Rata-rata IPK Lulusan</a></li>
					    <li><a data-toggle="tab" href="#home5" onclick="getPage('home5','IPKACT')">Rata-Rata IPK Mahasiswa Aktif</a></li>
					    <li><a data-toggle="tab" href="#home6" onclick="getPage('home6','STSLEC')">Status Dosen</a></li>
					    <li><a data-toggle="tab" href="#home7" onclick="getPage('home7','STSPMB')">PMB</a></li>
					    <li><a data-toggle="tab" href="#home8" onclick="getPage('home8','AKREDT')">Akreditasi</a></li>
					</ul>
					<div class="tab-content">
					    <div id="home1" class="tab-pane fade in active">
					    	<div id='jumlahmhs' style='min-width: 300px; height: 400px; max-width: 1000px; margin: 0 auto'></div>
					    	<?php 
					    		if ($logged['id_user_group'] != 8 and $logged['id_user_group'] != 9) { ?>
					    			<hr>
							    	<center>
							    		<h3>Jumlah Mahasiswa Per Prodi</h3>
							    		<small>*pilih prodi pada <i>select box</i> untuk menampilkan statistik</small>
							    	</center>
							    	<br>
							    	<script>
				                        $(document).ready(function(){
				                            $('#fk').change(function(){
				                                $.post('<?= base_url()?>infografis/ig02/sess/'+$(this).val(),{},function(get){
				                                    $('#show').html(get);
				                                });
				                            });
				                        }); 
				                    </script>
							    	<div class="pull-right">
				                        <div class="form-group" style="float:left;margin-right">
				                            <select class="form-control" name="prod" id="fk">
				                                <option disabled="" selected="">-- Pilih Prodi --</option>
				                                <?php foreach ($pro as $key) { ?>
				                                <option value="<?= $key->kd_prodi; ?>"><?= $key->prodi; ?></option>
				                                <?php } ?>
				                            </select>
				                        </div>
				                    </div>
				                    <br>
				                    <div id="show" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
					    	<?php } ?>
						</div>
						<div id="home2" class="tab-pane fade in">
								
						</div>
						<div id="home3" class="tab-pane fade in">
											
						</div>
						<div id="home4" class="tab-pane fade in">
							
						</div>
						<div id="home5" class="tab-pane fade in">
											
						</div>
						<div id="home6" class="tab-pane fade in">
							
						</div>
						<div id="home7" class="tab-pane fade in">
							
						</div>
						<div id="home8" class="tab-pane fade in">
												
						</div>
					</div>
			    </div>
			</div>

		</div>
	</div>
</div>