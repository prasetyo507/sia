<?php 
	$log = $this->session->userdata('sess_login');

	/** define user group */
	$pecah 	= explode(',', $log['id_user_group']);
	$jmlh 	= count($pecah);
	for ($i = 0; $i < $jmlh; $i++) { 
		$grup[] = $pecah[$i];
	}
 ?>

<!--- PMB -->
<script type="text/javascript">
	$(function () {
	    $('#pmb').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: '<?= $textpmb; ?>' 
	        },

	        subtitle: {
	            text: 'Source: SIA UBHARAJAYA'
	        },

	        xAxis: {
	            categories: [
	            	<?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { echo $i.","; } ?>
	            ],
	            crosshair: true
	        },

	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Total'
	            }
	        },

	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },

	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },

	        series: [{
		        name: 'Total pendaftar',
		        data: [
		        	<?= $reguser; ?>
		         ]
		    }, {
		        name: 'Peserta tes',
		        data: [
		        	<?= $tesuser; ?>
		         ]
		    }, {
		        name: 'Lulus tes',
		        data: [
		        	<?= $grduser; ?>
		         ]
		    }, {
		        name: 'Daftar ulang',
		        data: [
		        	<?= $reregis; ?>
		         ]
		    }],
	    });
	});
</script>

<!-- kategori asal sekolah -->
<script type="text/javascript">
	$(function () {
		var chart = $('#asl_skl').highcharts({

		    chart: {
		        type: 'column'
		    },

		    title: {
		        text: 'Kategori Sekolah Asal'
		    },

		    subtitle: {
		        text: 'Source: SIA UBHARAJAYA'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: [
	            	<?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { echo $i.","; } ?>
	            ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [{
		        name: 'SMA',
		        data: [
		        	<?= $sma; ?>
		         ]
		    }, {
		        name: 'SMK',
		        data: [
		       		<?= $smk; ?>
		         ]
		    }, {
		        name: 'MA',
		        data: [
		        	<?= $maa; ?>
		         ]
		    }, {
		        name: 'LAIN-LAIN',
		        data: [
		        	<?= $oth; ?>
		         ]
		    }],
		});
	});
</script>

<!-- penghasilan orang tua -->
<script type="text/javascript">
	$(function () {
		var chart = $('#penghasilan').highcharts({

		    chart: {
		        type: 'column'
		    },

		    title: {
		        text: 'Penghasilan Orang Tua'
		    },

		    subtitle: {
		        text: 'Source: SIA UBHARAJAYA'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: [
	            <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
	            	echo $i.",";
	            } ?>
	            ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [
		    <?php for ($i=1; $i < 5; $i++) {
		    	switch ($i) {
		    		case '1':
		    			$hasilan = 'Rp 1,000,000 - 2,000,000';
		    			break;

		    		case '2':
		    			$hasilan = 'Rp 2,100,000 - 4,000,000';
		    			break;

		    		case '3':
		    			$hasilan = 'Rp 4,100,000 - 5,999,000';
		    			break;

		    		case '4':
		    			$hasilan = '>= Rp 6,000,000';
		    			break;
		    	}
		    	echo "{name: '".$hasilan."',";
		    	echo "data: [";
		    	$tahun = date('Y'); for ($a=2016; $a <= $tahun; $a++) {
		    		if ( (in_array(8, $grup)) or (in_array(9, $grup))) {
		        		$daftar = $this->info_model->getjumlahmabapenghasilanprodi($a,$i,$log['userid']); echo $daftar.","; 
		        	} else {
		        		$daftar = $this->info_model->getjumlahmabapenghasilan($a,$i); echo $daftar.",";
		        	}
		    	}
		    	echo "]},";
		    } ?>
		    ],
		});
	});
</script>

<!--- /PMB -->

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id='pmb' style='min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto'></div>
<hr>
<?php 
	if ($log['id_user_group'] != 8 && $log['id_user_group'] != 9)  { ?>
    	<center>
    		<h3>Jumlah Pendaftar Mahasiswa Baru Per Prodi</h3>
    		<small>*pilih prodi pada <i>select box</i> untuk menampilkan statistik</small>
    	</center>
    	<br>
    	<script>
            $(document).ready(function(){
                $('#fk7').change(function(){
                    $.post('<?= base_url()?>infografis/ig02/jm_mabaprodi/'+$(this).val(),{},function(get){
                        $('#showjmlmaba').html(get);
                    });
                });
            }); 
        </script>
    	<div class="pull-right">
            <div class="form-group" style="float:left;margin-right">
                <select class="form-control" name="prod" id="fk7">
                    <option disabled="" selected="">-- Pilih Prodi --</option>
                    <?php foreach ($pro as $key) { ?>
                    <option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <br>
        <div id="showjmlmaba" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
        <hr>
<?php } ?>
<div id='asl_skl' style='min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto'></div>
<hr>
<?php 
	if ($log['id_user_group'] != 8 && $log['id_user_group'] != 9) { ?>
    	<center>
    		<h3>Jumlah Kategori Sekolah Mahasiswa Baru Per Prodi</h3>
    		<small>*pilih prodi pada <i>select box</i> untuk menampilkan statistik</small>
    	</center>
    	<br>
    	<script>
            $(document).ready(function(){
                $('#fk8').change(function(){
                    $.post('<?php echo base_url()?>infografis/ig02/asalsekolah/'+$(this).val(),{},function(get){
                        $('#showktgskl').html(get);
                    });
                });
            }); 
        </script>
    	<div class="pull-right">
            <div class="form-group" style="float:left;margin-right">
                <select class="form-control" name="prod" id="fk8">
                    <option disabled="" selected="">-- Pilih Prodi --</option>
                    <?php foreach ($pro as $key) { ?>
                    <option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <br>
        <div id="showktgskl" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
        <hr>
<?php } ?>
<div id='penghasilan' style='min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto'></div>
<?php 
	if ($log['id_user_group'] != 8 && $log['id_user_group'] != 9) { ?>
		<hr>
    	<center>
    		<h3>Jumlah Penghasilan Orang Tua Mahasiswa Baru Per Prodi</h3>
    		<small>*pilih prodi pada <i>select box</i> untuk menampilkan statistik</small>
    	</center>
    	<br>
    	<script>
            $(document).ready(function(){
                $('#fk9').change(function(){
                    $.post('<?php echo base_url()?>infografis/ig02/loadgaji/'+$(this).val(),{},function(get){
                        $('#showgaji').html(get);
                    });
                });
            }); 
        </script>
    	<div class="pull-right">
            <div class="form-group" style="float:left;margin-right">
                <select class="form-control" name="prod" id="fk9">
                    <option disabled="" selected="">-- Pilih Prodi --</option>
                    <?php foreach ($pro as $key) { ?>
                    <option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <br>
        <div id="showgaji" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
        <hr>
<?php } ?>