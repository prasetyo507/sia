<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pemasaran extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('info_model');
	}

	public function index()
	{
		//akreditas
		$data['akre'] = $this->db->query("SELECT * from tbl_jurusan_prodi a join tbl_fakultas b on a.kd_fakultas = b.kd_fakultas order by a.kd_fakultas asc")->result();

		/*dosen*/
		$data['getdosen'] = $this->info_model->getdosen();

		/*lulusan*/
		//$data['lulusan'] = $this->info_model->getlulusan(0);

		//jml mhs
		$data['pro'] = $this->db->query("SELECT * from tbl_jurusan_prodi")->result();

		$data['ta'] = $this->db->query("SELECT * from tbl_tahunakademik")->result();

		$data['page'] = 'v_pemasaran_sekolah_asal';
		$this->load->view('template',$data);
	}

}

/* End of file Pemasaran.php */
/* Location: ./application/controllers/Pemasaran.php */