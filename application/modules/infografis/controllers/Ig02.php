<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ig02 extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('ig_model');
		$this->load->model('info_model');
	}

	public function index()
	{
		//akreditas
		$data['akre'] = $this->db->query("SELECT * from tbl_jurusan_prodi a join tbl_fakultas b on a.kd_fakultas = b.kd_fakultas order by a.kd_fakultas asc")->result();

		/*dosen*/
		$data['getdosen'] = $this->ig_model->getdosen();

		/*lulusan*/
		//$data['lulusan'] = $this->info_model->getlulusan(0);

		//status mhs
		$data['q'] = $this->db->query("SELECT kode from tbl_tahunakademik where status = 1")->row();
		$data['min'] = $this->db->query("SELECT min(tahunajaran) as tmin,max(tahunajaran) as tmax from tbl_status_mahasiswa")->row();
		// var_dump($data['min']->tmin.'-'.$data['min']->tmax);exit();

		//jml mhs
		$data['pro'] = $this->db->query("SELECT * from tbl_jurusan_prodi")->result();

		$data['page'] = "v_reps";
		$this->load->view('template', $data);
	}

	function sess($prod)
	{
		$this->session->set_userdata('pro',$prod);
		redirect('infografis/ig02/ganti');
	}

	function ganti()
	{
		$data['qry'] = $this->ig_model->load_change($this->session->userdata('pro'));
		$this->load->view('load_faks', $data);
	}

	function sesi_jml_sts($id)
	{
		$this->session->set_userdata('sesijmlsts',$id);
		redirect('infografis/ig02/jmlsts');
	}

	function jmlsts()
	{
		// die($this->session->userdata('sesijmlsts'));
		$data['stat'] = $this->session->userdata('sesijmlsts');
		$this->load->view('load_jml_sts', $data);
	}

	function jmlulus($id)
	{
		// die($id);
		$data['lulus'] = $id;
		$this->load->view('jml_lulus', $data);
	}

	function masastd($id)
	{
		$data['std'] = $id;
		$this->load->view('load_avg_std', $data);
	}

	function ipklulusprodi($id)
	{
		$data['ipk'] = $id;
		$this->load->view('load_ipkprodi', $data);
	}

	function ipkaktifprodi($id)
	{
		$data['ipkon'] = $id;
		$this->load->view('load_ipk_on', $data);
	}

	function dosen($id)
	{
		$data['dsn'] = $id;
		$this->load->view('load_dosen', $data);
	}

	function jm_mabaprodi($id)
	{
		$data['maba'] = $id;
		$this->load->view('load_maba', $data);
	}

	function asalsekolah($id)
	{
		$data['asal'] = $id;
		$this->load->view('load_skl', $data);
	}

	function loadgaji($id)
	{
		$data['gaji'] = $id;
		$this->load->view('load_gaji', $data);
	}

}

/* End of file Ig02.php */
/* Location: ./application/modules/infografis/controllers/Ig02.php */